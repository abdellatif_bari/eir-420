# Introduction

This document aims at providing guidelines on how to organize project files (source code, configuration suites, test suites, documentation)

## Tools

The main tools used to organize an Opencell project are:

* **VSA**: financial management tool
* **Assembla**: production management tool
* **Git**: file storage
* **Postman**: configuration and testing tool

Of course, we'll also may have to use:

* a good text editor (Sublime Text, Atom, Notepad++, ...)
* a development environment (Eclipse, NetBeans, ...). That can be useful when writing scripts.
* an Office Suite (Microsoft Office, GSuite, ...)
* a tool to manipulate the Git repository (Git Extensions for command line on Windows, Source Tree for a good GUI, ...)

## Project team

### Contract manager 

* In charge of negotiating the contractual aspects of the project (Contract, QAP, ...).
* Part of the Steering Committee.

### Project manager

* Runs the project, responsible for the planning and delivery.
* Reports to the Chief Operating Officer.
* Takes part in the Steering Committee and Project Committee.
* Is present at most workshops.

### Functional expert

* Responsible for the functional architecture of the solution.
* Writes functional specifications.
* Attends functional/business workshops.
* Can attend Project Committee.

### Technical expert

* Responsible for the technical architecture of the solution.
* Writes technical documentation (operation manual, ...).
* Helps with setting up the development and testing environments.
* Helps tuning the production environments.
* Attends technical workshops.
* Can attend Project Committee.

### Lead developer (Dev lead)

* Assists the Project Manager in following the development progress.
* Writes technical specification.
* Assigns tasks to developers.
* Reviews tickets estimates.
* Reviews developments.
* Builds delivery packages.
* Can attend Project Committee and some workshops.

On sizeable project there can be several Lead Developer, they are then called **Stream leaders**.

### Developer

* In charge of completing the task assigned by the Lead Developer.
* Estimates workload for assigned tasks.
* Performs unit testing.

### Test manager

* Responsible for functional tests and integration tests.
* Writes test plans and User Manual.
* Assigns tests tasks testers.
* Reports tests coverage and progress to the Project Manager.
* Tests delivery packages
* Can attend Project Committee.

### Tester

* Writes test cases according to the test plan.
* Creates test datasets.
* Performing tests and provide a first level of bug analysis.

# VSA

https://opencell.vsactivity.com/

VSA is tool we use to manage project budget, financial status, resource allocation and planning.

This tools is used by Project Managers to assign resources to the project, follow invoicing and payments.
It is used by all the project team as a time-sheet declaration tool, and absence requests. 

# Assembla

## About Assembla

https://opencell.assembla.com/

Assembla is our standard tool to follow project production and client's support requests.

It is the duty of the **Project Manager** and **Lead Developer** to ensure that Assembla tickets are up-to-date.

### Project space 
Each project will have its dedicated Assembla space.

Project spaces must not contain core or portal development tasks. If the project requires evolution of the Opencell Product, tickets must be opened in 
the dedicated Product spaces (core, portal, ...). In that case, project and product tickets must be linked together using the Assembla built-in feature.

#### Permissions

Project manager, Lead Developer and Test Manager will be *Owners* of the project space.

Other Opencell team members will be *Members*.

### Support tool

Assembla is also the tool used by our clients to open support requests.

Client's users must be declared as *Watchers*. 

They will have limited access to Assembla features; especially, they won't have access to the *Tickets* tool, but rather to the more restrictive 
*Support* tool.

## Milestones

Assembla milestone are used to follow up project milestone.

Milestones due date corresponds to planned delivery dates, usually triggering contractual invoicing.

### Milestones naming

Milestones names are always prefixed with the Opencell version (excluding patch number), then follows the project phase, and at last, optionally, the 
milestone name.

`<Opencell version>-<project phase>-<milestone>`

ex: `5.2-SETUP-SPRINT1`

#### Project phases

We define 5 project phase:

`INIT > SETUP > TEST > SUPPORT > UPGRADE`

##### INIT 
Initial phase when most the workshops and specifications take place

Deliverable usually are:

* Signed Contract and Annexes (QAP, NDA, ...)
* Functional specifications
* Technical architecture document
* Development and Test environments

##### SETUP
Main production phase when the project is developed.

Deliverables are:

* Opencell installation package
* Opencell configuration file (Postman collection)
* Installation manual
* Test plans
* Test cases execution reports
* Operation manual
* User manual
* Delivery note

##### TEST
Phase during which the client performs his acceptance tests, and during which bugs are fixed.

Deliverable is:

* Acceptance note signed by the client

##### SUPPORT
Phase during which the Opencell provides support for the project.
It usually lasts as long as the Client pays for the Opencell EE Licence.

Support request, bug reports, and change requests can be be handles as part of a support milestone

Deliverables are:

* Updated Opencell configuration file (Postman collection)
* Opencell patch files (Postman collection)
* Updated Installation manual
* Updated Test plans
* Updated Test cases execution reports
* Updated Operation manual
* Updated User manual
* Delivery notes

##### UPGRADE
Usually this phase prepares for an upgrade of the Opencell version.

This doesn't concern patch updates.
Usually we try to link upgrades with major change requests.

Deliverable are:

* Opencell installation package
* Updated Installation manual
* Updated Operation manual
* Delivery note

### Special milestones

We can define the following 3 backlog milestones.

#### Backlog milestone

* Named like `5.2-SETUP-BACKLOG`
* It contains tickets that have been agreed upon but not yet scheduled into a sprint/batch.

#### Bug milestone

* Named like `5.2-TEST-BUG`
* It contains tickets that report a bug and are waiting for scheduling.

#### Pending milestone

* Named like `5.2-SUPPORT-PENDING`
* It contains change request tickets being discussed, not agreed upon, not scheduled.

## Tickets

### Project tasks

Created by the Opencell project team.

#### Epic

These are created by the Project Manager during the INIT phase, and correspond to the major domains/functionalities involved in the project:

* Environments (technical architecture, environments setup, ...)
* Project management
* Mediation
* Rating (catalogue, pricing, ...)
* Billing (cycles, templates, ...)
* Accounting
* Reports
* GUI
* ...

No estimates or worked hours should be directly entered on the epics.

#### Stories

Written by the Functional and technical experts, they usually are assigned to the Lead Developer.

These are the specifications (users stories) and must be aggregated into the corresponding specification documents.

They explain the functionalities required, expected results, processes and high level algorithms.

Stories are linked to a parent Epic.
Estimate and work hours correspond to the time estimated and spent for writing the tickets.

#### Tasks

Written by the Leader Developer and assigned to the Developers.

These are the technical specifications that describe tasks that must be performed in order to implement the parent Story.

Estimates and worked hours are filled by the developer.

### Bug tickets

1. Created by the Client and assigned to the Project Manager. Or created by the Test team and assigned directly to the Lead Developer.
2. Bug ticket are always created in the Backlog 
2. The Lead Developer dispatches the ticket to a Developer.
3. The developer, then sent to the Test Leader who dispatches it to a Tester, 
4. For a Client ticket, the tester sends the ticket to the Project Manager who will schedule the delivery of the fix.

### Change requests

Created by the Client, to request a new feature or a change in an existing feature, assigned to the Project Manager.

The change request is to be parked into the "PENDING" milestone while it's being discussed between the Project Manager and the Client.

Upon agreement it will be moved to the "BACKLOG" milestone awaiting for scheduling.

### Support requests

Support request are opened by the Client and assigned to the Project Manager.

It can cover different kind of requests such as:

* question/advice,
* help with environment configuration,
* help on product usage,
* ...

The Project Manager will evaluate whether the request is included in the EE Licence support or should be billed as Professional Services.

### Using tags

Tags can be assigned to tickets. That feature can be used to facilitate various reporting.

#### Financial reporting

The idea is to create a tag for each order form received from the client, allowing to check which ticket is covered by which order.

#### Nature of the tasks

This would be to evaluate the repartition between the different types of tickets. 
For example:

* spec
* dev
* bug
* change
* support

# Project files (Git)

## About Git

https://en.wikipedia.org/wiki/Git
https://gitextensions.github.io/
https://www.sourcetreeapp.com/

Git is mainly a version control tool for source code, but can be used a file repository and facilitates packaging for delivery.

Assembla spaces contain at least one Git repository.

Project files will be stored on the Project's Assembla space Git repository.

## Git file tree

Below is a non-limiting example of project file tree.

From Project's repo root dir:

* `README.md`: Markdown file contain general informations about the project
* `/data`: Folder to store project's data files
    - `/database`: contains database dumps
    - `/images`: contains images, logos
    - `/test`: contains test data files 
* `/delivery`: Folder containing delivery packages organized into folder named after the delivery package prefix (see dedicated section ***Delivery 
packages*** below for details)
* `/documentation`: Folder contains latest published version of project deliverable documentation. These documents can be arranged into folders by 
document types or by functional streams as best suited for the project (see dedicated section ***Documentation*** below for details)
* `/jasper`: Folder containing the Jasper templates used in the project, arranged into sub-folders (one per template)
* `/postman`: Folder containing the Postman files for the project (see dedicated section ***Postman files*** below for details)
    - `/environments`: contains Postman environment files, for different Opencell instances used on the projects (local, dev, test, prod, ...)
    - `/patches`: contains Postman patch collections
    - `/test`: contains Postman test collections
    - `Opencell_<PROJECT-CODE>_Setup.postman_collection.json`: the full Postman configuration collection for the project
        + `<PROJECT-CODE>` is the Assembla space code, like `Acme-153`
* `/scripts`: Folder containing the source code for Opencell scripts developed for the project. Usually organized into folders, by category.
    - `/beanio`: XML files for beanio file format descriptions
    - `/filters`: XML files for Opencell filters
    - `/invoicing`: scripts for RT dispatch scripts
    - `/jobs`: scripts for "scripting" and "filtered" jobs ()
    - `/mediation`: used by the various mediation jobs types
    - `/notifications`: scripts triggered by notifications
    - `/rating`: rating script used in price plans
    - `/xml`: scripts for XML generation
    - ...


## Postman files

https://www.getpostman.com/

Postman is a great tool for execute HTTP requests especially Rest API calls.
These calls are organized into Collections which can be exported into a Json file.

Opencell configuration/customization can be done using API, so we decided to using API calls collections as a means to configure Opencell instances.

This section give a few tip on how to organize project Postman files.

### Postman project file types

There are 2 types of Postman files:

* **Collections**: they contain a *collection* of API calls. These are divided into 3 mains categories:
    - *Setup*: complete setup file for setting up an environment from scratch.
    - *Test*: contains tests data and tests suites.
    - *Patch*: patch files containing only changes from the previous project release.
* **Environment**: they contain environment variables, used store environment specific configuration (URL, user, password, ...)

### Environment files

The project should have one Postman environment file for each environment related to the project:

* Development instance
* Test platform
* Acceptance platform
* Production platform
* ...

#### Variables

Postman environments mainly contains the following variables:

* `opencell.base`: base URL for Opencell application (`https://host:port/opencell`)
* `opencell.url`: Opencell rest API URL (`{{opencell.base}}/api/rest`)
* `opencell.username`: Opencell user password (`opencell.admin`)
* `opencell.password`: Opencell user unencoded password (`MTM5ZmIyM2IwM2UyNDQ3YjI1MjA4ZTJm`)
* `proxy.auth`: If using the Postman proxy configuration, this stores the proxy basic auth string (`Basic b3BledcNlbGw6MHBlbmNlISE=`)

#### Environment naming

Environment name, as visible in Postman should look like:
`[<PROJECT-CODE>][<access>] <env name>` 

* `<PROJECT-CODE>` is the Assembla space code, like `Acme-153`
* `<access>` should be one of 
    - `internal`: Opencell environment
    - `external`: Client environment. Opencell may have access only for bug investigations.
* `<env name>` is the name of the environment

A Client's test environment could be labelled:
`[Acme-153][external] Acceptance` 

#### Environment files naming

Environment files should be named
`<PROJECT-CODE>_<access>_<env name>.postman_environment.json`

For example:
`Acme-153_external_Acceptance.postman_environment.json`

### Setup collection

The Setup collection contains all API calls needed to configure the project in a new environment.

There should be only **one setup collection per project**. Unless the project can be split into separate sub-projects, in which case we will usually 
have a "common" setup collection, and a setup collection per sub-project.

#### Setup collection naming

Collection name, as visible in Postman should look like:
`<PROJECT-CODE> Setup` 

Where `<PROJECT-CODE>` is the Assembla space code, like `Acme-153`

The collection name would look like `Acme-153 Setup`.

#### Setup collection file naming

Collection file should be named like this
`Opencell_<PROJECT-CODE>_Setup.postman_collection.json`

For example:
`Opencell_Acme-153_Setup.postman_collection.json`

#### Setup collection organization

The project Setup collection should, as much as possible, follow the structure of our ***[Opencell common 
configuration](https://opencell.assembla.com/spaces/meveo-enterprise/git/source/master/opencell-suites/postman/Opencell%20common%20configuration.postman_collection.json 
"Opencell common configuration")***

#### Setup Collection variables

Some collection level variables can be used to share project specific data, such as:

* `project.code`: project code, as mentioned before (`Acme-153`)
* `project.type`: project collection type (`Setup`)
* `project.version`: project version, name of the last version (`20181126_1`)
* `project.description`: project description, like `Wholesale billing system for Acme Inc.`
* `template.code`: a code, used in every entity codes, to allow running several versions on the project on the same server.

<span style="font-size:150%;color:red">**&#x26a0;**</span> A *createOrUpdate* request on a Provider CustomField will have to be added to the Patch 
collection in order to track which patches have been installed.

```json
POST /customFieldTemplate/createOrUpdate
{
    "code": "CF_PROV_PROVIDER_VERSION_{{$timestamp}}",
    "description": "{{project.code}} {{project.type}} {{project.version}}",
    "fieldType": "STRING",
    "appliesTo": "PROVIDER",
    "defaultValue": "{{timestamp.iso}}",
    "storageType": "SINGLE",
    "valueRequired": false,
    "versionable": false,
    "allowEdit": false,
    "hideOnNew": false,
    "cacheValue": false,
    "guiPosition": "tab:Project versions:999"
}
```

### Patch collection

A Patch collection contains all operations required to update the environment since last delivery.

There will be several Patch collections, typical 1 for each delivery after the initial delivery.

#### Patch collection naming

Collection name, as visible in Postman should look like:
`<PROJECT-CODE> Patch <YYYYMMDD>_<N>`

Where:

* `<PROJECT-CODE>` is the Assembla space code, like `Acme-153`
* `<YYYYMMDD>` is the date of release of the patch
* `<N>` is a counter, in case several patches should be delivered on the same day (to fix a delivery package error). Should always be `1`.

A Client's test environment could be labelled:
`Acme-153 Patch 20181125_1` 

#### Patch collection file naming

Patch collection files should be named like this: 
`<YYYYMMDD>_Opencell_<PROJECT-CODE>_Patch_v<N>.postman_collection.json`

For example:
`20181125_Acme-153_Patch_v1.postman_collection.json`

#### Patch collection organization

Patch collection often contains patches for several tickets (bug or enhancements). 

Each ticket will have its own folder. This folder will contain (with no set order) API calls to:

* **update** modified entities
* **delete** obsolete entities
* **create** new entities
* create, execute and delete **migration Java scripts** (if complex operation are required to migrate existing entities)

<span style="font-size:150%;color:red">**&#x26a0;**</span> The Lead Developer and Test Manager in charge of the delivery must make sure that there's not 
unwanted interactions between the different ticket fixes.

#### Patch collection variables

The patch collection will contain the same variables as the Setup collectionwill contain the same variables as the Setup collection, with a variation 
for the `project.version`:

* `project.type`: project collection type (`Patch`)

<span style="font-size:150%;color:red">**&#x26a0;**</span> The patch collection will always contain a the same CustomFieldTemplate creation request as 
mentioned for the Setup collection.

#### Patch collection under construction

While a patch collection is being constructed (fixes and improvements are still to be included), the date part in name and file name will be replaced 
with `00000000`, and the counter will always be `0`.

For example:
`Acme-153 Patch 00000000_0` 
`00000000_Acme-153_Patch_v0.postman_collection.json`

### Test collection

The Test collections contain series of API calls allowing to test the project configuration and specific features present in the Setup collection.

Usually one Test collection will contain all the tests, but several collections could be created is needed.

Also a one "Patch Test" collection must be provided for each patch. Patch test collection contains only tests for the features delivered in the patch. 
This test must be in the global test collection too.

#### Test collection naming

Collection name, as visible in Postman should look like:
`<PROJECT-CODE> [<Patch code>] Test` 

Where `<PROJECT-CODE>` is the Assembla space code, like `Acme-153`

Collection file should be named like this: 
`Acme-153 Test` 

#### Test collection file naming

Collection file should be named like this: 
`Opencell_<PROJECT-CODE>_Test.postman_collection.json`

For example:
`Opencell_Acme-153_Test.postman_collection.json`

#### Test collection organization

The test collection will contain multiple tests (hopefully enough to cover all the project's requirements), organized according to the project 
specification document plan.

Each test must be standalone and contain 3 sub-folders:

* `dataset`: creates the data required to test the feature
* `test`: executes the actions for the test
* `cleanup`: cleans as much as it is possible of the created data 

Test results will be tested in the request "test scripts" in Postman (see below ***Postman scripting***).

### Postman scripting

https://www.getpostman.com/docs/v6/postman/scripts/postman_sandbox

Postman has strong scripting capabilities that can prove useful.

There are 2 kinds of scripts:

* **Pre-request** scripts: executed before the API call
* **Test** scripts: executed after the API call (usually to the result of the call)

***Note***: scripts written in the folders (or collection) level, will be executed before the scripts in children folders or requests.

#### Pre-request scripts

Can be used to prepare some data for the requests (by setting some variables).

<span style="font-size:150%;color:red">**&#x26a0;**</span> **All** our Postman collection should include the following script set as pre-request script 
at collection level and allows parsing request name to global variables:
```javascript
pm.globals.set("timestamp.iso", (new Date()).toISOString());
pm.globals.set("request.label", pm.info.requestName);

var parts = pm.info.requestName.split("|");

for(let i=0; i<parts.length; i++) {
    pm.globals.set("request.label."+i, parts[i]);
}
```

#### Test scripts

Typically used to check the result of the request, or store the result in variables.

<span style="font-size:150%;color:red">**&#x26a0;**</span> **All** API requests in any collection must have a test script to check the good execution of 
the request.

For Opencell GET requests, the following code this could be enough:
```javascript
var jsonData = JSON.parse(responseBody);
tests["is.success"] = jsonData.actionStatus.status === "SUCCESS";
```

For Opencell POST/PUT request, it's usually something like this:
```javascript
var jsonData = JSON.parse(responseBody);
tests["is.success"] = jsonData.status === "SUCCESS";
```

For ScriptInstance POST/PUT, you can add the following line to check for compilation errors:
```javascript
tests["is.compiled"] = jsonData.compilationErrors.length === 0;
```

### Opencell scripts and XML in Postman

Json format doesn't allow for string containing newlines, or things like that, so Opencell ScriptInstance code cannot be pasted as is into a postman 
request; it must encoded by escaping forbidden character.

This also applies to XML filters and file descriptions (beanio).

These are 3 ways for transforming the Java source file into a Json-friendly string

#### Opencell GUI and API calls
*The most compatible way*

* After having written your script in your favourite text editor/IDE
* Copy the content
* Paste it into the target ScriptInstance in Opencell Admin GUI, and save
* Using Postman to GET the ScriptInstance
* Copy the already encoded script code
* Paste it in the POST ScriptInstance request you're trying to write

#### Online converted
*The lazy ways*

Use an online converter, such as: https://www.freeformatter.com/json-escape.html

#### Shell scripting
*The hardcore way (and personal favourite)*

This will require a Linux like shell (Linux, MacOSX, Cygwin, Windows' Ubuntu shell).

Create a shell script like this:

```bash
cat >json_string <<EOF
#!/bin/bash
sed 's/\\/\\\\/g' | sed ':a;N;$!ba;s/\n/\\n/g' | sed 's/\t/\\t/g' | sed 's/\r/\\r/g' | sed 's/"/\\"/g'
EOF
chmod +x json_string
```

Then you can either convert the java source file using redirection:

```bash
json_string <MyScript.java >MyScript.java.txt
```

Or just:

* Run the shell script,
* Paste java source code,
* Send EOF character on an empty line (`CTRL-D`),
* The encrypted line will be printed in console output.

## Documentation

Project documentation is usually composed of a set of Microsoft Office documents. Some come for the client (mostly requirements), others from Opencell 
(Specifications, Operation manual, User manual).

As usual documents are edited and stored on the Opencell Google Drive.

But in order to prepare for delivery the deliverable documents will be copied to the `documentation` in the git repository.

# Delivery packages

Delivery packages are prepared with the other files stored in the Git. A package should not contains anything not from the Git.

## Package contents and schedule

Package contents and schedule are a decided between the Project Manager and the Client.

The package usually contains:

* Updated **Setup** postman collection 
* Updated **Test** postman collection
* **Patch** postman collection 
* **Patch Test** postman collection
* Test data files as needed
* Updated deliverable **documentation**

Along with the delivery package, a delivery note will be provided, containing the list of delivered features (bug/features tickets, documents, ...).

## Delivery packages naming

Delivery packages will be stored in a folder in the `/delivery` folder of the Git repository.

Depending on the project methodology (V-cycle, Agile), the packages can be named differently.

In order to facilitate sequential application of patches, packages will be named like using this convention: 
`<YYYYMMDD>_Opencell_<PROJECT-CODE>_<DELIVERY_NAME>_v<N>`

* `<YYYYMMDD>` is the date of delivery
* `<PROJECT-CODE>` is the Assembla project code (`Acme-153`)
* `<DELIVERY_NAME>` identifies the delivery; for example:
    - `INCREMENT-1`
    - `BATCH-WINTER18`
    - `SPRINT-42`
    - `HOTFIX`
* `<N>` is a counter in case of multiple deliveries in the day (for fixing a problem in the delivery package). Should always be 1.

## Delivery packages file names

* Delivery sub-folder: `/<YYYYMMDD>_Opencell_<PROJECT-CODE>_<DELIVERY_NAME>_v<N>`
    - Examples: `/201810215_Opencell_Acme-153_INCREMENT-1_v1`, `/20171120_Opencell_Acme-153_SPRINT-7_v1`, `/20181120_Opencell_Acme-153_HOTFIX_v1`
* Delivery files: 
    - `<YYYYMMDD>_Opencell_<PROJECT-CODE>_<DELIVERY_NAME>_Notes_v<N>.pdf`: delivery notes describing the content of the delivery
    - `<YYYYMMDD>_Opencell_<PROJECT-CODE>_<DELIVERY_NAME>_Package_v<N>.zip`: if the delivery contains a number of files, they will be zipped

