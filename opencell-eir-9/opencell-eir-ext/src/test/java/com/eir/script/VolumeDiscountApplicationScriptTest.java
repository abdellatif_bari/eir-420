package com.eir.script;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.commons.collections.MapUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.meveo.model.admin.Seller;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.BillingRun;
import org.meveo.model.billing.ChargeInstance;
import org.meveo.model.billing.InvoiceSubCategory;
import org.meveo.model.billing.OneShotChargeInstance;
import org.meveo.model.billing.RecurringChargeInstance;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.billing.Tax;
import org.meveo.model.billing.UserAccount;
import org.meveo.model.billing.WalletInstance;
import org.meveo.model.billing.WalletOperation;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.OfferTemplateCategory;
import org.meveo.model.catalog.OneShotChargeTemplate;
import org.meveo.model.catalog.OneShotChargeTemplateTypeEnum;
import org.meveo.model.catalog.RecurringChargeTemplate;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.crm.Customer;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.model.tax.TaxClass;
import org.meveo.service.billing.impl.BillingRunService;
import org.meveo.service.billing.impl.RatingService;
import org.meveo.service.billing.impl.WalletOperationService;
import org.meveo.service.catalog.impl.OneShotChargeTemplateService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.script.RunTimeLogger;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

@RunWith(MockitoJUnitRunner.class)
public class VolumeDiscountApplicationScriptTest {

    @Spy
    @InjectMocks
    private VolumeDiscountApplicationScript volumeDiscountScript;

    @Mock
    private CustomTableService customTableService;
    
    @Mock
    RatingService ratingService;
    
    @Mock
    private OneShotChargeTemplateService chargeTemplateService;

    @Mock
    private WalletOperationService walletOperationService;

    @Mock
    private EntityManager entityManager;
    
    @Mock
    private BillingRunService billingRunService;

    /**
     * A logger to replace with when running script in test mode (from GUI), so logs can be returned/visible to the end user
     */
    protected RunTimeLogger logTest = new RunTimeLogger(this.getClass());
    
    @Test
    public void test_execute_Volume_Discount_Application() {

        WalletOperation wo = createWO("subscription", "DATA", true);

        String[] columns = new String[] { "id", "profile_code", "customer_account", "billing_account", "charge_type", "migration_code", "valid_from", "valid_to", "discount_type",
                "percent", "description", "band_from", "accounting_code", "invoice_sub_cat", "aggregate_type" };
        Object[] values = new Object[] { "82", "10", "20222", "20223", "O", "DSL", "2020-04-07 00:00:00", "2020-04-07 00:00:00", "TR", "20.000000000000", "DSL", "20", new BigInteger("-1"), new BigInteger("5"),
                "2" };
        List<Map<String, Object>> volumes = constructListOfMap(columns, values);

        wo.setQuantity(new BigDecimal(20));
        wo.setDescription("VolumeDiscountTest");
        wo.setUnitAmountWithoutTax(new BigDecimal(20));
        wo.setUnitAmountWithTax(new BigDecimal(25));

        doNothing().when(volumeDiscountScript).executeAggregateSql(any());
        doNothing().when(ratingService).calculateAmounts(any(), any(), any());

        List<Object[]> wallets = new ArrayList<Object[]>();
        wallets.add(new Object[] { new BigInteger("257"), new BigInteger("2"), new BigDecimal("3") });
        doReturn(wallets).when(volumeDiscountScript).getWalletOperationsForDiscount(any(), any(),any(),any(), any(Date.class));
        //doReturn(BigDecimal.TEN).when(volumeDiscountScript).getDiscountPercent(any(), any(), any());
        doReturn(volumes).when(volumeDiscountScript).getActiveVolumeDiscounts(any(Date.class));

        when(walletOperationService.findById(any())).thenReturn(wo);
        when(chargeTemplateService.findByCode(any())).thenReturn(new OneShotChargeTemplate());
        BillingRun br = new BillingRun();
        br.setInvoiceDate(new Date());
        when(billingRunService.findById(any())).thenReturn(br);

        Map<String, Object> aggSql = new HashMap<String, Object>();
        aggSql.put("aggregate_sql",
            "select billing_subscription.id, row_number() over (partition by customer_account_id, eir_sub_association.assoc_circ_id order by eir_sub_association.assoc_start_date)  from billing_subscription  join billing_user_account on billing_subscription.user_account_id =billing_user_account.id  join billing_billing_account on billing_user_account.billing_account_id = billing_billing_account.id and billing_billing_account.id = :ba_id and customer_account_id = :ca_id join eir_sub_association on billing_subscription.id =eir_sub_association.subscription_id and eir_sub_association.assoc_end='A' and billing_subscription.id in (select subscription_id from billing_service_instance  join cat_service_template on billing_service_instance.service_template_id = cat_service_template.id and cat_service_template.cf_values like :migration_code");
        when(customTableService.findById(eq("EIR_OR_VOL_DISCOUNT_AGGR"), any())).thenReturn(aggSql);
        
        when(walletOperationService.getEntityManager()).thenReturn(entityManager);

        when(entityManager.getReference(eq(InvoiceSubCategory.class), any())).thenAnswer(new Answer<InvoiceSubCategory>() {
            public InvoiceSubCategory answer(InvocationOnMock invocation) throws Throwable {
                InvoiceSubCategory entity = new InvoiceSubCategory();
                entity.setId((Long) invocation.getArguments()[1]);
                return entity;
            }
        });
        
        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { "BR_ID", "2" });
        volumeDiscountScript.execute(initContext);

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(20));
        assertThat(wo.getDescription()).isEqualTo("VolumeDiscountTest");
        verify(walletOperationService, times(1)).create(any());
}

    private List<Map<String, Object>> constructListOfMap(String[] columns, Object[] values) {
        int size = columns.length;
        List<Map<String, Object>> list = new ArrayList<Map<String,Object>>();
        
        Map<String, Object> map = new HashMap<String, Object>();
        for (int i= 0; i<size; i++) {
            map.put(columns[i], values[i]);
        }
        list.add(map);     
        return list;
    }
    
    private WalletOperation createWO(String chargeType, String offerType, boolean setExtraInfo) {
        WalletOperation wo = new WalletOperation();

        Customer customer = new Customer();
        CustomerAccount ca = new CustomerAccount();
        ca.setCustomer(customer);

        BillingAccount ba = new BillingAccount();
        ba.setCustomerAccount(ca);

        UserAccount ua = new UserAccount();
        ua.setBillingAccount(ba);

        WalletInstance wallet = new WalletInstance();
        wallet.setUserAccount(ua);

        wo.setWallet(wallet);

        wo.setSeller(new Seller());

        ChargeInstance chargeInstance = null;
        if (chargeType.equals("subscription") || chargeType.equals("other")) {
            chargeInstance = new OneShotChargeInstance();
            OneShotChargeTemplate chargeTemplate = new OneShotChargeTemplate();
            chargeTemplate.setOneShotChargeTemplateType(chargeType.equals("subscription") ? OneShotChargeTemplateTypeEnum.SUBSCRIPTION : OneShotChargeTemplateTypeEnum.OTHER);
            chargeInstance.setChargeTemplate(chargeTemplate);

        } else if (chargeType.equals("recurring")) {
            chargeInstance = new RecurringChargeInstance();
            RecurringChargeTemplate chargeTemplate = new RecurringChargeTemplate();
            chargeInstance.setChargeTemplate(chargeTemplate);
        }

        wo.setChargeInstance(chargeInstance);

        OfferTemplateCategory offerCategory = new OfferTemplateCategory();
        offerCategory.setCfValue("offerType", offerType);

        OfferTemplate offer = mock(OfferTemplate.class);
       // when(offer.getOfferTemplateCategories()).thenReturn(Arrays.asList(offerCategory));

        ServiceTemplate serviceTemplate = new ServiceTemplate();
        serviceTemplate.setId(5L);
        ServiceInstance serviceInstance = new ServiceInstance();
        serviceInstance.setServiceTemplate(serviceTemplate);
        
        chargeInstance.setCfValue("SERVICE_PARAMETERS", MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|d||" }));
        wo.setServiceInstance(serviceInstance);
        wo.getChargeInstance().setServiceInstance(serviceInstance);

        wo.setOfferTemplate(offer);

        if (setExtraInfo) {
            TaxClass taxClass = new TaxClass();
            taxClass.setId(5L);
            wo.setTaxClass(taxClass);

            Tax tax = new Tax();
            tax.setId(4L);
            tax.setPercent(new BigDecimal(15));
            wo.setTax(tax);
            wo.setTaxPercent(new BigDecimal(15));
        }

        return wo;
    }

    
}
