package com.eir.script;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.IntStream;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.meveo.admin.exception.BusinessException;
import org.meveo.api.dto.billing.TimePeriodDto;
import org.meveo.api.dto.billing.UsageRatingDto;
import org.meveo.api.dto.billing.UsageRatingDto.RoundingMode;
import org.meveo.model.billing.Tax;
import org.meveo.model.billing.WalletInstance;
import org.meveo.model.billing.WalletOperation;
import org.meveo.model.rating.EDR;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.billing.impl.AccountingCodeService;
import org.meveo.service.billing.impl.RatingService;
import org.meveo.service.catalog.impl.InvoiceSubCategoryService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.tax.TaxClassService;
import org.meveo.service.tax.TaxMappingService;
import org.meveo.service.tax.TaxMappingService.TaxInfo;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UsageRatingScriptTest {

    @Spy
    @InjectMocks
    private UsageRatingScript ratingScript;

    @Mock
    private CustomTableService customTableService;

    @Mock
    private InvoiceSubCategoryService invoiceSubCategoryService;

    @Mock
    private TaxClassService taxClassService;

    @Mock
    private AccountingCodeService accountingCodeService;
    
    @Mock
    private TaxMappingService taxMappingService;

    @Mock
    private RatingService ratingService;

    @Before
    public void setUp() {           
        populateCustomTable("EIR_U_TARIFF_PLAN_MAP", new Object[][] {
            {"id","customer_id","rate_plan_id","valid_from","valid_to"},
            { 1L, 1L, 1L, "12/12/2019","12/12/2020"}
        });
        populateCustomTable("EIR_U_TARIFF", new Object[][] {
            {"id","rate_plan_id","traffic_class_id","min_charge","connection_charge","time_period_id","usage_charge","unit","duration_rounding","min_duration"},
            {1L, 1L, 1L,new BigDecimal(1.3),new BigDecimal(1.5),1L,new BigDecimal(0.4),60L,30L,20L},
            {2L, 1L, 1L,new BigDecimal(1.4),null,2L,new BigDecimal(0.2),60L,20L,20L},
            {3L, 1L, 1L,new BigDecimal(1.5),null,3L,new BigDecimal(0.3),60L,30L,null}
        });         
        when(invoiceSubCategoryService.findById(any())).thenReturn(null);
        when(taxClassService.findById(any())).thenReturn(null);
        when(accountingCodeService.findById(any())).thenReturn(null);
        
        TaxInfo taxInfo = taxMappingService.new TaxInfo();
        taxInfo.tax = new Tax();
        taxInfo.tax.setPercent(new BigDecimal(20));  
        when(taxMappingService.determineTax(any(),any(),any(),any(),any(),any(Boolean.class),any(Boolean.class))).thenReturn(taxInfo);
    }
    @Test
    public void determineTrafficClass_WhenManyTariffRecordsFound_ShouldFindBestMatch() {
        populateCustomTable("EIR_U_TRAFFIC_CLASS", new Object[][] {
            {"id", "code", "description", "fee_code", "call_type", "service_category", "number_prefix", "valid_from", "valid_to"},
            { new BigInteger("1"), "tc_1", "traffic class 1", "fc_1", "INBOUND", "cat1", "44", "2019-12-11", "2019-12-10" },
            { new BigInteger("2"), "tc_2", "traffic class 1", "fc_1", "INBOUND", "cat1", "446", "2019-12-11", "2020-12-10" },
            { new BigInteger("3"), "tc_3", "traffic class 1", "fc_2", "OUTBOUND", "cat2", "4", "2020-12-11", "2021-12-10" }

        });
        UsageRatingDto usageRatingDto = new UsageRatingDto();
        EDR edr = new EDR();
        edr.setParameter3("fc_4");//fee_code
        edr.setParameter2("INBOUND");//call_tpye
        edr.setParameter7("cat1");//service_category
        edr.setParameter1("44662103366"); //b_number
        edr.setParameter1("44662103366"); //b_number
        edr.setEventDate(DateUtils.newDate(2020, java.util.Calendar.JUNE, 9, 0, 0, 0));
        ratingScript.determineTrafficClass(usageRatingDto, edr);
        assertThat(usageRatingDto.getTrafficClassId()).isEqualTo(2);
    }
    @Test
    public void determineTrafficClass_WhenNoRecordIsFound_ShouldThrowException() {
        populateCustomTable("EIR_U_TRAFFIC_CLASS", new Object[][] {
            {"id", "code", "description", "fee_code", "call_type", "service_category", "number_prefix", "valid_from", "valid_to"}
        });
        UsageRatingDto usageRatingDto = new UsageRatingDto();
        EDR edr = new EDR();
        edr.setParameter3("fc_4");//fee_code
        edr.setParameter2("INBOUND");//call_tpye
        edr.setParameter7("cat1");//service_category
        edr.setParameter1("44662103366"); //b_number
        edr.setEventDate(DateUtils.newDate(2020, java.util.Calendar.JUNE, 9, 0, 0, 0));
        
        assertThatExceptionOfType(BusinessException.class).isThrownBy(() -> {ratingScript.determineTrafficClass(usageRatingDto, edr);});
    }

    @Test
    public void determineTrafficClass_WhenOneRecordIsFound_And_NumberPrefixNotEmptyOrSame_ShouldThrowException() {
        populateCustomTable("EIR_U_TRAFFIC_CLASS", new Object[][] {
            {"id", "code", "description", "fee_code", "call_type", "service_category", "number_prefix", "valid_from", "valid_to"} ,
            { new BigInteger("1"), "tc_1", "traffic class 1", "fc_1", "INBOUND", "cat1", "33", "2019-12-11", "2019-12-10" }
        });
        UsageRatingDto usageRatingDto = new UsageRatingDto();
        EDR edr = new EDR();
        edr.setParameter3("fc_4");//fee_code
        edr.setParameter2("INBOUND");//call_tpye
        edr.setParameter7("cat1");//service_category
        edr.setParameter1("44662103366"); //b_number
        edr.setEventDate(DateUtils.newDate(2020, java.util.Calendar.JUNE, 9, 0, 0, 0));
        
        assertThatExceptionOfType(BusinessException.class).isThrownBy(() -> {ratingScript.determineTrafficClass(usageRatingDto, edr);});
    }

    @SuppressWarnings("static-access")
    @Test
    public void identifyRatePlan_WhenRatePlanIsFound_ShoudReturnSuccess() {
        populateCustomTable("EIR_U_TARIFF_PLAN", new Object[][] {
            {"id","code","description","currency_id","time_band","split","rounding_mode","rounding"},
            { new BigInteger("1"), "rp_1", "rate plan 1", "1", "1", "true", "UP", "4"}
        });

        UsageRatingDto usageRatingDto = new UsageRatingDto();
        usageRatingDto.setTrafficClassId(2L);
        usageRatingDto.setCustomerId(1L);
        usageRatingDto.setEventDate(DateUtils.newDate(2020, java.util.Calendar.JUNE, 9, 0, 0, 0));
        ratingScript.identifyRatePlan(usageRatingDto);
        assertThat(usageRatingDto.getTariffPlanId()).isEqualTo(1L);
        assertThat(usageRatingDto.getTimeBandId()).isEqualTo(1L);
        assertThat(usageRatingDto.getRounding()).isEqualTo(4L);
        assertThat(usageRatingDto.getRoundingMode()).isEqualTo(usageRatingDto.getRoundingMode().UP);
        assertThat(usageRatingDto.isSplit()).isEqualTo(true);
    }
    
    @Test
    public void identifyRatePlan_WhenNoRatePlanIsFound_ShoudThrowException() {
        populateCustomTable("EIR_U_TARIFF_PLAN", new Object[][] {
            {"id","code","description","currency_id","time_band","split","rounding_mode","rounding"}
        });

        UsageRatingDto usageRatingDto = new UsageRatingDto();
        usageRatingDto.setTrafficClassId(2L);
        usageRatingDto.setCustomerId(1L);
        usageRatingDto.setEventDate(DateUtils.newDate(2020, java.util.Calendar.JUNE, 9, 0, 0, 0));
        
        assertThatExceptionOfType(BusinessException.class).isThrownBy(() -> {ratingScript.identifyRatePlan(usageRatingDto);});
    }

    @Test
    public void calculateTotalPrice_WhenRoundingUP_And_RoundingIsThree_ShouldReturnSuccess() {
        UsageRatingDto usageRatingDto = new UsageRatingDto();
        usageRatingDto.setInputQuantity(new BigDecimal(65000));
        usageRatingDto.setRounding(3L);
        usageRatingDto.setRoundingMode(RoundingMode.UP);
        usageRatingDto.setEventDate(DateUtils.newDate(2020, java.util.Calendar.JUNE, 9, 0, 0, 0));
        usageRatingDto.setMinCharge(new BigDecimal(1.0));
        usageRatingDto.setConnectionCharge(new BigDecimal(1.5));
        usageRatingDto.setMinDuration(30L);      
        usageRatingDto.setTariffPlanId(1L);
        usageRatingDto.setCustomerId(1L);
        usageRatingDto.setTrafficClassId(1L);     
        usageRatingDto.setTimePeriodPerCalendar(Map.of(1L, 1L, 2L, 2L));
        usageRatingDto.setTimePeriods(constructTimePeriods());
        ratingScript.calculateCallCharge(usageRatingDto);
        assertThat(usageRatingDto.getMinCharge()).isEqualByComparingTo("1.301");
        assertThat(usageRatingDto.getTimePeriods().get(0).getTimePeriodAmount()).isEqualByComparingTo("0.601");
        assertThat(usageRatingDto.getTimePeriods().get(1).getTimePeriodAmount()).isEqualByComparingTo("0.067");
        assertThat(usageRatingDto.getMinCharge()).isEqualByComparingTo("1.301");
        assertThat(usageRatingDto.getTotalCharge()).isEqualByComparingTo("2.318");
    }
 
    @SuppressWarnings("unchecked")
    @Test
    public void calculateTotalPrice_And_StoreData_WhenRoundingDOWN_And_RoundingIsTwo_ShouldReturnSuccess() {
        UsageRatingDto usageRatingDto = new UsageRatingDto();
        usageRatingDto.setInputQuantity(new BigDecimal(20000));
        usageRatingDto.setRounding(2L);
        usageRatingDto.getRoundingMode();
        usageRatingDto.setRoundingMode(RoundingMode.DOWN);
        usageRatingDto.setEventDate(DateUtils.newDate(2020, java.util.Calendar.JUNE, 9, 0, 0, 0));
        usageRatingDto.setMinCharge(new BigDecimal(1.0));
        usageRatingDto.setConnectionCharge(new BigDecimal(1.5));
        usageRatingDto.setMinDuration(30L);      
        usageRatingDto.setTariffPlanId(1L);
        usageRatingDto.setCustomerId(1L);
        usageRatingDto.setTrafficClassId(1L);     
        usageRatingDto.setTimePeriodPerCalendar(Map.of(1L, 1L, 2L, 2L));
        usageRatingDto.setTimePeriods(constructTimePeriods());
        ratingScript.calculateCallCharge(usageRatingDto);
        assertThat(usageRatingDto.getMinCharge()).isEqualByComparingTo("1.30");
        assertThat(usageRatingDto.getTimePeriods().get(0).getTimePeriodAmount()).isEqualByComparingTo("0.60");
        assertThat(usageRatingDto.getTimePeriods().get(1).getTimePeriodAmount()).isEqualByComparingTo("0.06");
        assertThat(usageRatingDto.getMinCharge()).isEqualByComparingTo("1.30");
        assertThat(usageRatingDto.getTotalCharge()).isEqualByComparingTo("2.30");
        WalletOperation wo = new WalletOperation();
        wo.setWallet(new WalletInstance());
        ratingScript.applyTax(usageRatingDto, wo);
        ratingScript.storeData(usageRatingDto, wo);
        Map<String,String> callInformation = (Map<String,String>) wo.getCfValue("CALL_INFORMATION");
        String[] values = callInformation.values().iterator().next().split(Pattern.quote("|"));
        assertThat(values[0]).isEqualTo("1.30");
        assertThat(values[1]).isEqualTo("TP_3");
        assertThat(values[4]).isEqualTo("0.60");
        assertThat(values[8]).isEqualTo("0.06");
        assertThat(values[12]).isEqualTo("0.14");
    }

    private List<TimePeriodDto> constructTimePeriods() {
        List<TimePeriodDto> timePeriods = new ArrayList<>();
        TimePeriodDto timePeriod1 = new TimePeriodDto();
        timePeriod1.setTimePeriodId(1L);
        timePeriod1.setTimePeriodCode("TP_1");
        timePeriod1.setCallDuration(65L);
        timePeriods.add(timePeriod1);
        
        TimePeriodDto timePeriod2 = new TimePeriodDto();
        timePeriod2.setTimePeriodId(2L);
        timePeriod1.setTimePeriodCode("TP_2");
        timePeriod2.setCallDuration(55L);
        timePeriods.add(timePeriod2);

        TimePeriodDto timePeriod3 = new TimePeriodDto();
        timePeriod3.setTimePeriodId(3L);
        timePeriod1.setTimePeriodCode("TP_3");
        timePeriod3.setCallDuration(25L);
        timePeriods.add(timePeriod3);

        return timePeriods;
    }
    private void populateCustomTable(String tableName, Object[][] data) {
        Object[] columns = data[0];
        List<Map<String, Object>> dataList = new ArrayList<Map<String, Object>>(data.length - 1);
        Map<String, Object> items = new HashMap<>();
        for (Object[] row : data) {
            if(columns.equals(row)) {
                continue;
            }
            items = populateData(columns, row);
            dataList.add(items);
        }
        when(customTableService.list(eq(tableName), any())).thenReturn(dataList);
    }
    private Map<String, Object> populateData(Object[] columns, Object[] row) {  
        if(row.length == 0) {
            return Collections.emptyMap();
        }
        Map<String, Object> data = new HashMap<>();      
        IntStream.range(0, columns.length).forEach(idx -> data.put((String) columns[idx], row[idx]));
        return data;
    }
}
