package com.eir.script;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doAnswer;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;

import org.apache.commons.collections.MapUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.meveo.model.DatePeriod;
import org.meveo.model.admin.Seller;
import org.meveo.model.billing.AccountingCode;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.ChargeApplicationModeEnum;
import org.meveo.model.billing.ChargeInstance;
import org.meveo.model.billing.InstanceStatusEnum;
import org.meveo.model.billing.InvoiceSubCategory;
import org.meveo.model.billing.OneShotChargeInstance;
import org.meveo.model.billing.RecurringChargeInstance;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.billing.SubscriptionChargeInstance;
import org.meveo.model.billing.Tax;
import org.meveo.model.billing.TerminationChargeInstance;
import org.meveo.model.billing.UserAccount;
import org.meveo.model.billing.WalletInstance;
import org.meveo.model.billing.WalletOperation;
import org.meveo.model.catalog.CalendarYearly;
import org.meveo.model.catalog.DayInYear;
import org.meveo.model.catalog.MonthEnum;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.OfferTemplateCategory;
import org.meveo.model.catalog.OneShotChargeTemplate;
import org.meveo.model.catalog.OneShotChargeTemplateTypeEnum;
import org.meveo.model.catalog.RecurringChargeTemplate;
import org.meveo.model.catalog.RoundingModeEnum;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.crm.Customer;
import org.meveo.model.crm.EntityReferenceWrapper;
import org.meveo.model.crm.Provider;
import org.meveo.model.crm.custom.CustomFieldMapKeyEnum;
import org.meveo.model.crm.custom.CustomFieldMatrixColumn;
import org.meveo.model.crm.custom.CustomFieldMatrixColumn.CustomFieldColumnUseEnum;
import org.meveo.model.crm.custom.CustomFieldStorageTypeEnum;
import org.meveo.model.crm.custom.CustomFieldTypeEnum;
import org.meveo.model.customEntities.CustomEntityInstance;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.model.shared.DateUtils;
import org.meveo.model.tax.TaxClass;
import org.meveo.service.billing.impl.RatingService;
import org.meveo.service.billing.impl.WalletOperationService;
import org.meveo.service.catalog.impl.CalendarService;
import org.meveo.service.catalog.impl.InvoiceSubCategoryService;
import org.meveo.service.catalog.impl.OfferTemplateService;
import org.meveo.service.catalog.impl.ServiceTemplateService;
import org.meveo.service.crm.impl.CustomFieldTemplateService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.script.Script;
import org.meveo.service.tax.TaxClassService;
import org.meveo.service.tax.TaxMappingService;
import org.meveo.service.tax.TaxMappingService.TaxInfo;
import org.meveo.util.ApplicationProvider;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import com.eir.commons.enums.ChargeTypeEnum;
import com.eir.commons.enums.TransformationRulesEnum;
import com.eir.commons.enums.customfields.ChargeInstanceCFsEnum;
import com.eir.commons.enums.customfields.ServiceInstanceCFsEnum;
import com.eir.commons.enums.customfields.ServiceParametersCFEnum;
import com.eir.commons.enums.customfields.ServiceTemplateCFsEnum;
import com.eir.commons.enums.customtables.ValidTariffCustomTableEnum;
import com.eir.script.OneShotAndRecurringRatingScript.ServiceRatingParameterGroup;
import com.eir.script.OneShotAndRecurringRatingScript.TariffRecordGroup;

@RunWith(MockitoJUnitRunner.class)
public class OneShotAndRecurringRatingScriptTest {

    private static String GNP_PRODUCT_SET = "-1";

    @Spy
    @InjectMocks
    private OneShotAndRecurringRatingScript ratingScript;

    @Mock
    private CustomTableService customTableService;

    @Mock
    private CustomFieldTemplateService customFieldTemplateService;

    @Mock
    private TaxClassService taxClassService;

    @Mock
    private InvoiceSubCategoryService invoiceSubCategoryService;

    @Mock
    private TaxMappingService taxMappingService;

    @Mock
    private RatingService ratingService;

    @Mock
    private WalletOperationService walletOperationService;

    @Mock
    private ServiceTemplateService serviceTemplateService;

    @Mock
    private CalendarService calendarService;

    @Mock
    private OfferTemplateService offerTemplateService;

    @Mock
    private EntityManager entityManager;

    @Mock
    @ApplicationProvider
    protected Provider appProvider;

    @SuppressWarnings("unchecked")
    @Before
    public void setUp() {

        when(customTableService.list(eq("EIR_OR_TARIFF_PLAN_MAP"), any())).thenReturn(Arrays.asList(MapUtils.putAll(new HashMap<String, Object>(), new Object[] { "tariff_plan_id", 15L })));

        CustomFieldTemplate serviceParamCFT = new CustomFieldTemplate();
        serviceParamCFT.setFieldType(CustomFieldTypeEnum.MULTI_VALUE);
        serviceParamCFT.setStorageType(CustomFieldStorageTypeEnum.MATRIX);

        List<CustomFieldMatrixColumn> matrixColumns = new ArrayList<CustomFieldMatrixColumn>();

        CustomFieldMatrixColumn matrixColumn = new CustomFieldMatrixColumn(ServiceParametersCFEnum.ITEM_ID.getLabel(), "Item id");
        matrixColumn.setColumnUse(CustomFieldColumnUseEnum.USE_KEY);
        matrixColumn.setPosition(1);
        matrixColumn.setKeyType(CustomFieldMapKeyEnum.STRING);
        matrixColumns.add(matrixColumn);

        matrixColumn = new CustomFieldMatrixColumn(ServiceParametersCFEnum.QUANTITY.getLabel(), "Quantity");
        matrixColumn.setColumnUse(CustomFieldColumnUseEnum.USE_VALUE);
        matrixColumn.setPosition(1);
        matrixColumn.setKeyType(CustomFieldMapKeyEnum.STRING);
        matrixColumns.add(matrixColumn);

        matrixColumn = new CustomFieldMatrixColumn("one", "First field");
        matrixColumn.setColumnUse(CustomFieldColumnUseEnum.USE_VALUE);
        matrixColumn.setPosition(2);
        matrixColumn.setKeyType(CustomFieldMapKeyEnum.STRING);
        matrixColumns.add(matrixColumn);

        matrixColumn = new CustomFieldMatrixColumn("two", "Second field");
        matrixColumn.setColumnUse(CustomFieldColumnUseEnum.USE_VALUE);
        matrixColumn.setPosition(3);
        matrixColumn.setKeyType(CustomFieldMapKeyEnum.STRING);
        matrixColumns.add(matrixColumn);

        matrixColumn = new CustomFieldMatrixColumn(ServiceParametersCFEnum.SLA.getLabel(), "SLA field");
        matrixColumn.setColumnUse(CustomFieldColumnUseEnum.USE_VALUE);
        matrixColumn.setPosition(4);
        matrixColumn.setKeyType(CustomFieldMapKeyEnum.STRING);
        matrixColumns.add(matrixColumn);

        matrixColumn = new CustomFieldMatrixColumn(ServiceParametersCFEnum.AMT.getLabel(), "AMT");
        matrixColumn.setColumnUse(CustomFieldColumnUseEnum.USE_VALUE);
        matrixColumn.setPosition(5);
        matrixColumn.setKeyType(CustomFieldMapKeyEnum.STRING);
        matrixColumns.add(matrixColumn);

        matrixColumn = new CustomFieldMatrixColumn(ServiceParametersCFEnum.AMTOOC.getLabel(), "AMTOOC");
        matrixColumn.setColumnUse(CustomFieldColumnUseEnum.USE_VALUE);
        matrixColumn.setPosition(6);
        matrixColumn.setKeyType(CustomFieldMapKeyEnum.STRING);
        matrixColumns.add(matrixColumn);

        matrixColumn = new CustomFieldMatrixColumn(ServiceParametersCFEnum.DISTANCE.getLabel(), "Distance");
        matrixColumn.setColumnUse(CustomFieldColumnUseEnum.USE_VALUE);
        matrixColumn.setPosition(7);
        matrixColumn.setKeyType(CustomFieldMapKeyEnum.STRING);
        matrixColumns.add(matrixColumn);

        serviceParamCFT.setMatrixColumns(matrixColumns);

        when(customFieldTemplateService.findByCodeAndAppliesTo(eq(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name()), any(ServiceInstance.class))).thenReturn(serviceParamCFT);

        when(customTableService.listAsObjects(eq("product_set"), any())).thenReturn(Arrays.asList(new BigInteger(GNP_PRODUCT_SET)));

        Tax tax = new Tax();
        tax.setPercent(new BigDecimal(10));
        TaxInfo taxInfo = new TaxMappingService().new TaxInfo();
        taxInfo.tax = tax;
        taxInfo.taxClass = new TaxClass();

        when(taxClassService.findById(any())).thenAnswer(new Answer<TaxClass>() {
            public TaxClass answer(InvocationOnMock invocation) throws Throwable {
                TaxClass entity = new TaxClass();
                entity.setId((Long) invocation.getArguments()[0]);
                return entity;
            }
        });

        when(calendarService.findById(any())).thenAnswer(new Answer<CalendarYearly>() {
            public CalendarYearly answer(InvocationOnMock invocation) throws Throwable {

                CalendarYearly calendar = new CalendarYearly();

                List<DayInYear> days = new ArrayList<DayInYear>();

                for (int i = 1; i <= 12; i = i + ((Long) invocation.getArguments()[0]).intValue()) {
                    DayInYear day = new DayInYear();
                    day.setMonth(MonthEnum.getValue(i));
                    day.setDay(1);
                    days.add(day);
                }
                calendar.setDays(days);
                calendar.setId((Long) invocation.getArguments()[0]);
                calendar.setCode("Calendar_" + invocation.getArguments()[0]);
                return calendar;
            }
        });

        when(taxMappingService.determineTax(any(), any(), any(), any(), any(), anyBoolean(), anyBoolean())).thenReturn(taxInfo);
        // doCallRealMethod().when(ratingService).calculateAmounts(any(), any(), any());

        // List<OfferTemplate> offerTemplates = Arrays.asList(offerTemplateService.findByCode(any()));
        // when(offerTemplateService.findByServiceTemplate(any(ServiceTemplate.class))).thenReturn(offerTemplates);

        when(ratingService.getEntityManager()).thenReturn(entityManager);
        doAnswer(new Answer<Void>() {
            public Void answer(InvocationOnMock invocation) {
                WalletOperation wo = (WalletOperation) invocation.getArguments()[0];
                BigDecimal unitAmountWithoutTax = (BigDecimal) invocation.getArguments()[1];

                wo.setUnitAmountWithoutTax(unitAmountWithoutTax);
                wo.setUnitAmountWithTax((BigDecimal) invocation.getArguments()[2]);

                if (wo.getQuantity() != null && unitAmountWithoutTax != null) {
                    wo.setAmountWithoutTax(wo.getQuantity().multiply(unitAmountWithoutTax));
                }
                return null;
            }
        }).when(ratingService).calculateAmounts(any(), any(), any());

        when(entityManager.getReference(eq(InvoiceSubCategory.class), any())).thenAnswer(new Answer<InvoiceSubCategory>() {
            public InvoiceSubCategory answer(InvocationOnMock invocation) throws Throwable {
                InvoiceSubCategory entity = new InvoiceSubCategory();
                entity.setId((Long) invocation.getArguments()[1]);
                return entity;
            }
        });

        when(entityManager.getReference(eq(AccountingCode.class), any())).thenAnswer(new Answer<AccountingCode>() {
            public AccountingCode answer(InvocationOnMock invocation) throws Throwable {
                AccountingCode entity = new AccountingCode();
                entity.setId((Long) invocation.getArguments()[1]);
                return entity;
            }
        });

        when(appProvider.getRoundingMode()).thenReturn(RoundingModeEnum.NEAREST);
        when(appProvider.getRounding()).thenReturn(6);
        when(appProvider.isEntreprise()).thenReturn(true);

        ratingScript.init(null);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_getMatchingTariffRecords_Data_subscription() {

        WalletOperation wo = createWO("subscription", "DATA", true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-17", null, null, 0d);

        Object[][] tariffs = new Object[][] { { "2020-01-15", "2020-01-20", "O", 25d, "one", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" },
                { "2020-01-15", "2020-03-25", "O", 28d, "two", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "1", "val" },
                { "2020-01-01", "2020-02-25", "O", 28d, "three", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "1", "val" },
                { "2020-01-15", "2020-01-20", "O", 28d, "four", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "4", "%" },
                { "2020-01-15", "2020-03-25", "O", 28d, "five", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "4", "%" },
                { "2020-01-01", "2020-02-25", "O", 28d, "six", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "4", "%" } };
        when(customTableService.list(eq("EIR_OR_TARIFF"), any())).thenReturn(constructTariffs(TransformationRulesEnum.DATA.name(), tariffs));

        Map<String, Object> serviceParams = MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { ServiceParametersCFEnum.ACTION_QUALIFIER.getLabel(), "one", ServiceParametersCFEnum.PRICE_PLAN.getLabel(), "two", ServiceParametersCFEnum.RATING_SCHEME.getLabel(), "three" });

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.getMatchingTariffRecords(wo.getWallet().getUserAccount().getBillingAccount().getCustomerAccount().getCustomer(), wo.getOfferTemplate(),
            wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.getChargeType(wo.getChargeInstance()), new BigDecimal(5), wo.getOperationDate(), null, serviceParams, false);

        assertThat(tariffRatingGroups.size()).isEqualTo(1); // For one shot only one period will be matched. For non-distance only one record will be matched per period.
        assertThat(tariffRatingGroups.get(0).tariffRecords.size()).isEqualTo(1);
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(0).description).isEqualTo("one");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_getMatchingTariffRecords_Data_recurring_1() {

        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-13", "2020-01-13", "2020-02-28", 0d);

        Object[][] tariffs = new Object[][] { { "2020-01-01", null, "R", 25d, "one", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val2" },
                { "2019-01-01", null, "R", 28d, "two", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "2", "val3" },
                { "2020-01-01", null, "R", 29d, "three", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "3", "val2" } };
        when(customTableService.list(eq("EIR_OR_TARIFF"), any())).thenReturn(constructTariffs(TransformationRulesEnum.DATA.name(), tariffs));

        Map<String, Object> serviceParams = MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { ServiceParametersCFEnum.ACTION_QUALIFIER.getLabel(), "one", ServiceParametersCFEnum.PRICE_PLAN.getLabel(), "two", ServiceParametersCFEnum.RATING_SCHEME.getLabel(), "three" });

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.getMatchingTariffRecords(wo.getWallet().getUserAccount().getBillingAccount().getCustomerAccount().getCustomer(), wo.getOfferTemplate(),
            wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.getChargeType(wo.getChargeInstance()), new BigDecimal(5), wo.getOperationDate(), new DatePeriod(wo.getStartDate(), wo.getEndDate()), serviceParams,
            false);

        assertThat(tariffRatingGroups.size()).isEqualTo(1); // For recurring multiple periods will be matched. For non-distance only one record will be matched per period.

        assertThat(tariffRatingGroups.get(0).tariffRecords.size()).isEqualTo(1);
        assertThat(tariffRatingGroups.get(0).validity).isEqualTo(getPeriod("2020-01-01", null));
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(0).description).isEqualTo("one");

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_getMatchingTariffRecords_Data_recurring_2() {

        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-13", "2020-01-13", "2020-02-28", 0d);

        Object[][] tariffs = new Object[][] { { "2020-01-15", null, "R", 25d, "one", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val2" },
                { "2020-01-15", "2020-03-25", "R", 28d, "two", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "1", "val3" },
                { "2020-01-01", null, "R", 28d, "three", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "2", "val" },
                { "2020-01-01", "2020-01-17", "R", 28d, "six", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "3", "%" } };
        when(customTableService.list(eq("EIR_OR_TARIFF"), any())).thenReturn(constructTariffs(TransformationRulesEnum.DATA.name(), tariffs));

        Map<String, Object> serviceParams = MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { ServiceParametersCFEnum.ACTION_QUALIFIER.getLabel(), "one", ServiceParametersCFEnum.PRICE_PLAN.getLabel(), "two", ServiceParametersCFEnum.RATING_SCHEME.getLabel(), "three" });

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.getMatchingTariffRecords(wo.getWallet().getUserAccount().getBillingAccount().getCustomerAccount().getCustomer(), wo.getOfferTemplate(),
            wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.getChargeType(wo.getChargeInstance()), new BigDecimal(5), wo.getOperationDate(), new DatePeriod(wo.getStartDate(), wo.getEndDate()), serviceParams,
            false);

        assertThat(tariffRatingGroups.size()).isEqualTo(2); // For recurring multiple periods will be matched. For non-distance only one record will be matched per period.

        assertThat(tariffRatingGroups.get(0).tariffRecords.size()).isEqualTo(1);
        assertThat(tariffRatingGroups.get(0).validity).isEqualTo(getPeriod("2020-01-01", "2020-01-15"));
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(0).description).isEqualTo("three");

        assertThat(tariffRatingGroups.get(1).tariffRecords.size()).isEqualTo(1);
        assertThat(tariffRatingGroups.get(1).validity).isEqualTo(getPeriod("2020-01-15", null));
        assertThat(tariffRatingGroups.get(1).tariffRecords.get(0).description).isEqualTo("one");

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_getMatchingTariffRecords_Data_recurring_3() {

        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-13", "2020-01-13", "2020-02-28", 0d);

        Object[][] tariffs = new Object[][] { { "2020-01-15", "2020-01-20", "R", 25d, "one", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val2" },
                { "2020-01-15", "2020-03-25", "R", 28d, "two", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "1", "val3" },
                { "2020-01-01", null, "R", 28d, "three", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "2", "val" },
                { "2020-01-01", "2020-01-17", "R", 28d, "six", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "3", "%" } };
        when(customTableService.list(eq("EIR_OR_TARIFF"), any())).thenReturn(constructTariffs(TransformationRulesEnum.DATA.name(), tariffs));

        Map<String, Object> serviceParams = MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { ServiceParametersCFEnum.ACTION_QUALIFIER.getLabel(), "one", ServiceParametersCFEnum.PRICE_PLAN.getLabel(), "two", ServiceParametersCFEnum.RATING_SCHEME.getLabel(), "three" });

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.getMatchingTariffRecords(wo.getWallet().getUserAccount().getBillingAccount().getCustomerAccount().getCustomer(), wo.getOfferTemplate(),
            wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.getChargeType(wo.getChargeInstance()), new BigDecimal(5), wo.getOperationDate(), new DatePeriod(wo.getStartDate(), wo.getEndDate()), serviceParams,
            false);

        assertThat(tariffRatingGroups.size()).isEqualTo(3); // For recurring multiple periods will be matched. For non-distance only one record will be matched per period.

        assertThat(tariffRatingGroups.get(0).tariffRecords.size()).isEqualTo(1);
        assertThat(tariffRatingGroups.get(0).validity).isEqualTo(getPeriod("2020-01-01", "2020-01-15"));
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(0).description).isEqualTo("three");

        assertThat(tariffRatingGroups.get(1).tariffRecords.size()).isEqualTo(1);
        assertThat(tariffRatingGroups.get(1).validity).isEqualTo(getPeriod("2020-01-15", "2020-01-20"));
        assertThat(tariffRatingGroups.get(1).tariffRecords.get(0).description).isEqualTo("one");

        assertThat(tariffRatingGroups.get(2).tariffRecords.size()).isEqualTo(1);
        assertThat(tariffRatingGroups.get(2).validity).isEqualTo(getPeriod("2020-01-20", "2020-03-25"));
        assertThat(tariffRatingGroups.get(2).tariffRecords.get(0).description).isEqualTo("two");

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_getMatchingTariffRecords_Data_subscription_distance() {

        WalletOperation wo = createWO("subscription", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-17", null, null, 0d);

        Object[][] tariffs = new Object[][] { { "2020-01-15", "2020-02-20", "O", 25d, "one", 1L, 5L, 6L, 1, 3, null, null, null, null, 1L, "1", "val" },
                { "2020-01-15", "2020-02-20", "O", 25d, "one", 1L, 5L, 6L, 3, 8, null, null, null, null, 1L, "1", "val" },
                { "2020-01-15", "2020-02-20", "O", 25d, "one", 1L, 5L, 6L, 8, null, null, null, null, null, 1L, "1", "val" },
                { "2020-01-15", "2020-03-25", "O", 28d, "two", 2L, 5L, 10L, 1, 4, null, null, null, null, 1L, "1", "val" },
                { "2020-01-15", "2020-03-25", "O", 28d, "two", 2L, 5L, 10L, 4, 9, null, null, null, null, 1L, "1", "val" },
                { "2020-01-15", "2020-03-25", "O", 28d, "two", 2L, 5L, 10L, 9, null, null, null, null, null, 1L, "1", "val" },
                { "2020-01-01", "2020-01-17", "O", 28d, "three", 2L, 5L, 10L, 1, 5, null, null, null, null, 1L, "1", "val" },
                { "2020-01-01", "2020-01-17", "O", 28d, "three", 2L, 5L, 10L, 5, 10, null, null, null, null, 1L, "1", "val" },
                { "2020-01-01", "2020-01-17", "O", 28d, "three", 2L, 5L, 10L, 10, null, null, null, null, null, 1L, "1", "val" },
                { "2020-01-15", "2020-02-20", "O", 25d, "four", 1L, 5L, 6L, 1, 3, null, null, null, null, 1L, "4", "%" },
                { "2020-01-15", "2020-02-20", "O", 25d, "four", 1L, 5L, 6L, 3, 8, null, null, null, null, 1L, "4", "%" },
                { "2020-01-15", "2020-02-20", "O", 25d, "four", 1L, 5L, 6L, 8, null, null, null, null, null, 1L, "4", "%" },
                { "2020-01-15", "2020-03-25", "O", 28d, "five", 2L, 5L, 10L, 1, 4, null, null, null, null, 1L, "4", "%" },
                { "2020-01-15", "2020-03-25", "O", 28d, "five", 2L, 5L, 10L, 4, 9, null, null, null, null, 1L, "4", "%" },
                { "2020-01-15", "2020-03-25", "O", 28d, "five", 2L, 5L, 10L, 9, null, null, null, null, null, 1L, "4", "%" },
                { "2020-01-01", "2020-01-17", "O", 28d, "six", 2L, 5L, 10L, 1, 5, null, null, null, null, 1L, "4", "%" },
                { "2020-01-01", "2020-01-17", "O", 28d, "six", 2L, 5L, 10L, 5, 10, null, null, null, null, 1L, "4", "%" },
                { "2020-01-01", "2020-01-17", "O", 28d, "six", 2L, 5L, 10L, 10, null, null, null, null, null, 1L, "4", "%" } };
        when(customTableService.list(eq("EIR_OR_TARIFF"), any())).thenReturn(constructTariffs(TransformationRulesEnum.DATA.name(), tariffs));

        Map<String, Object> serviceParams = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { ServiceParametersCFEnum.ACTION_QUALIFIER.getLabel(), "one", ServiceParametersCFEnum.PRICE_PLAN.getLabel(), "two",
                ServiceParametersCFEnum.RATING_SCHEME.getLabel(), "three", ServiceParametersCFEnum.DISTANCE.getLabel(), "15" });

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.getMatchingTariffRecords(wo.getWallet().getUserAccount().getBillingAccount().getCustomerAccount().getCustomer(), wo.getOfferTemplate(),
            wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.getChargeType(wo.getChargeInstance()), new BigDecimal(5), wo.getOperationDate(), null, serviceParams, false);

        assertThat(tariffRatingGroups.size()).isEqualTo(1); // For one shot only one period will be matched. For distance multiple records will be matched per period
        assertThat(tariffRatingGroups.get(0).tariffRecords.size()).isEqualTo(3);
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(0).description).isEqualTo("one");
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(0).distanceFrom).isEqualTo(1);
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(1).description).isEqualTo("one");
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(1).distanceFrom).isEqualTo(3);
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(2).description).isEqualTo("one");
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(2).distanceFrom).isEqualTo(8);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_getMatchingTariffRecords_Data_recurring_distance() {

        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-13", "2020-01-13", "2020-02-28", 0d);

        Object[][] tariffs = new Object[][] { { "2020-01-15", "2020-02-20", "R", 25d, "one", 1L, 5L, 6L, 1, 3, null, null, null, null, 1L, "1", "val" },
                { "2020-01-15", "2020-02-20", "R", 25d, "one", 1L, 5L, 6L, 3, 8, null, null, null, null, 1L, "1", "val" },
                { "2020-01-15", "2020-02-20", "R", 25d, "one", 1L, 5L, 6L, 8, null, null, null, null, null, 1L, "1", "val" },
                { "2020-01-15", "2020-03-25", "R", 28d, "two", 2L, 5L, 10L, 1, 4, null, null, null, null, 1L, "1", "val" },
                { "2020-01-15", "2020-03-25", "R", 28d, "two", 2L, 5L, 10L, 4, 9, null, null, null, null, 1L, "1", "val" },
                { "2020-01-15", "2020-03-25", "R", 28d, "two", 2L, 5L, 10L, 9, null, null, null, null, null, 1L, "1", "val" },
                { "2020-01-01", "2020-01-17", "R", 28d, "three", 2L, 5L, 10L, 1, 5, null, null, null, null, 1L, "1", "val" },
                { "2020-01-01", "2020-01-17", "R", 28d, "three", 2L, 5L, 10L, 5, 10, null, null, null, null, 1L, "1", "val" },
                { "2020-01-01", "2020-01-17", "R", 28d, "three", 2L, 5L, 10L, 10, null, null, null, null, null, 1L, "1", "val" },
                { "2020-01-15", "2020-02-20", "R", 25d, "four", 1L, 5L, 6L, 1, 3, null, null, null, null, 1L, "4", "%" },
                { "2020-01-15", "2020-02-20", "R", 25d, "four", 1L, 5L, 6L, 3, 8, null, null, null, null, 1L, "4", "%" },
                { "2020-01-15", "2020-02-20", "R", 25d, "four", 1L, 5L, 6L, 8, null, null, null, null, null, 1L, "4", "%" },
                { "2020-01-15", "2020-03-25", "R", 28d, "five", 2L, 5L, 10L, 1, 4, null, null, null, null, 1L, "4", "%" },
                { "2020-01-15", "2020-03-25", "R", 28d, "five", 2L, 5L, 10L, 4, 9, null, null, null, null, 1L, "4", "%" },
                { "2020-01-15", "2020-03-25", "R", 28d, "five", 2L, 5L, 10L, 9, null, null, null, null, null, 1L, "4", "%" },
                { "2020-01-01", "2020-01-17", "R", 28d, "six", 2L, 5L, 10L, 1, 5, null, null, null, null, 1L, "4", "%" },
                { "2020-01-01", "2020-01-17", "R", 28d, "six", 2L, 5L, 10L, 5, 10, null, null, null, null, 1L, "4", "%" },
                { "2020-01-01", "2020-01-17", "R", 28d, "six", 2L, 5L, 10L, 10, null, null, null, null, null, 1L, "4", "%" } };
        when(customTableService.list(eq("EIR_OR_TARIFF"), any())).thenReturn(constructTariffs(TransformationRulesEnum.DATA.name(), tariffs));

        Map<String, Object> serviceParams = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { ServiceParametersCFEnum.ACTION_QUALIFIER.getLabel(), "one", ServiceParametersCFEnum.PRICE_PLAN.getLabel(), "two",
                ServiceParametersCFEnum.RATING_SCHEME.getLabel(), "three", ServiceParametersCFEnum.DISTANCE.getLabel(), "5" });

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.getMatchingTariffRecords(wo.getWallet().getUserAccount().getBillingAccount().getCustomerAccount().getCustomer(), wo.getOfferTemplate(),
            wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.getChargeType(wo.getChargeInstance()), new BigDecimal(5), wo.getOperationDate(), new DatePeriod(wo.getStartDate(), wo.getEndDate()), serviceParams,
            false);

        assertThat(tariffRatingGroups.size()).isEqualTo(3); // For quantity or distance multiple periods and records will be matched

        assertThat(tariffRatingGroups.get(0).tariffRecords.size()).isEqualTo(3);
        assertThat(tariffRatingGroups.get(0).validity).isEqualTo(getPeriod("2020-01-01", "2020-01-15"));
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(0).description).isEqualTo("three");
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(0).distanceFrom).isEqualTo(1);
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(1).description).isEqualTo("three");
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(1).distanceFrom).isEqualTo(5);
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(2).description).isEqualTo("three");
        assertThat(tariffRatingGroups.get(0).tariffRecords.get(2).distanceFrom).isEqualTo(10);

        assertThat(tariffRatingGroups.get(1).tariffRecords.size()).isEqualTo(3);
        assertThat(tariffRatingGroups.get(1).validity).isEqualTo(getPeriod("2020-01-15", "2020-02-20"));
        assertThat(tariffRatingGroups.get(1).tariffRecords.get(0).description).isEqualTo("one");
        assertThat(tariffRatingGroups.get(1).tariffRecords.get(0).distanceFrom).isEqualTo(1);
        assertThat(tariffRatingGroups.get(1).tariffRecords.get(1).description).isEqualTo("one");
        assertThat(tariffRatingGroups.get(1).tariffRecords.get(1).distanceFrom).isEqualTo(3);
        assertThat(tariffRatingGroups.get(1).tariffRecords.get(2).description).isEqualTo("one");
        assertThat(tariffRatingGroups.get(1).tariffRecords.get(2).distanceFrom).isEqualTo(8);

        assertThat(tariffRatingGroups.get(2).tariffRecords.size()).isEqualTo(3);
        assertThat(tariffRatingGroups.get(2).validity).isEqualTo(getPeriod("2020-02-20", "2020-03-25"));
        assertThat(tariffRatingGroups.get(2).tariffRecords.get(0).description).isEqualTo("two");
        assertThat(tariffRatingGroups.get(2).tariffRecords.get(0).distanceFrom).isEqualTo(1);
        assertThat(tariffRatingGroups.get(2).tariffRecords.get(1).description).isEqualTo("two");
        assertThat(tariffRatingGroups.get(2).tariffRecords.get(1).distanceFrom).isEqualTo(4);
        assertThat(tariffRatingGroups.get(2).tariffRecords.get(2).description).isEqualTo("two");
        assertThat(tariffRatingGroups.get(2).tariffRecords.get(2).distanceFrom).isEqualTo(9);
    }

    @Test
    public void test_determineRatingGroups_oneshot() {

        // Start and end date should be irrelevant
        WalletOperation wo = createWO("subscription", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-13", "2025-01-13", "2025-01-14", 20d);

        List<ServiceRatingParameterGroup> ratingGroups = ratingScript.determineRatingGroups(wo, true);

        assertThat(ratingGroups.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).quantity).isEqualTo(wo.getQuantity());
        assertThat(ratingGroups.get(0).accumulatedQuantity).isEqualTo(wo.getQuantity());
        assertThat(ratingGroups.get(0).parameters.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).parameters.get(0).period.getFrom()).isEqualTo(wo.getOperationDate());
        assertThat(ratingGroups.get(0).parameters.get(0).period.getTo()).isNull();
        assertThat(ratingGroups.get(0).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("d");
    }

    @Test
    public void test_determineRatingGroups_oneshot_other() {

        // Start and end date should be irrelevant
        WalletOperation wo = createWO("other", TransformationRulesEnum.DATA.name(), false, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-13", "2025-01-13", "2025-01-14", 20d);

        List<ServiceRatingParameterGroup> ratingGroups = ratingScript.determineRatingGroups(wo, true);

        assertThat(ratingGroups.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).quantity).isEqualTo(wo.getQuantity());
        assertThat(ratingGroups.get(0).accumulatedQuantity).isEqualTo(wo.getQuantity());
        assertThat(ratingGroups.get(0).parameters.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).parameters.get(0).period.getFrom()).isEqualTo(wo.getOperationDate());
        assertThat(ratingGroups.get(0).parameters.get(0).period.getTo()).isNull();
        assertThat(ratingGroups.get(0).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("d");
    }

    @Test
    public void test_determineRatingGroups_oneshot_termination() {

        // Start and end date should be irrelevant
        WalletOperation wo = createWO("termination", TransformationRulesEnum.DATA.name(), true, false, false, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-03-25", "2025-01-13", "2025-01-14", 20d);

        List<ServiceRatingParameterGroup> ratingGroups = ratingScript.determineRatingGroups(wo, true);

        assertThat(ratingGroups.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).quantity).isEqualTo(wo.getQuantity());
        assertThat(ratingGroups.get(0).accumulatedQuantity).isEqualTo(wo.getQuantity());
        assertThat(ratingGroups.get(0).parameters.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).parameters.get(0).period.getFrom()).isEqualTo(wo.getOperationDate());
        assertThat(ratingGroups.get(0).parameters.get(0).period.getTo()).isNull();
        assertThat(ratingGroups.get(0).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("g");
    }

    @Test
    public void test_determineRatingGroups_recurring_rating_period_within_5serviceParameters_over_serviceParameterLimit() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2019-10-01", "2020-04-28", 20d);

        List<ServiceRatingParameterGroup> ratingGroups = ratingScript.determineRatingGroups(wo, false);

        assertThat(ratingGroups.size()).isEqualTo(3);
        assertThat(ratingGroups.get(0).quantity).isEqualTo(new BigDecimal(5));
        assertThat(ratingGroups.get(0).accumulatedQuantity).isEqualTo(new BigDecimal(5));
        assertThat(ratingGroups.get(0).parameters.size()).isEqualTo(4);
        assertThat(ratingGroups.get(0).parameters.get(0).period).isEqualTo(getPeriod("2019-12-15", "2020-01-10"));
        assertThat(ratingGroups.get(0).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("c");
        assertThat(ratingGroups.get(0).parameters.get(1).period).isEqualTo(getPeriod("2020-01-10", "2020-02-10"));
        assertThat(ratingGroups.get(0).parameters.get(1).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("d");
        assertThat(ratingGroups.get(0).parameters.get(2).period).isEqualTo(getPeriod("2020-02-10", "2020-02-25"));
        assertThat(ratingGroups.get(0).parameters.get(2).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("f");
        assertThat(ratingGroups.get(0).parameters.get(3).period).isEqualTo(getPeriod("2020-02-25", "2020-03-25"));
        assertThat(ratingGroups.get(0).parameters.get(3).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("g");

        assertThat(ratingGroups.get(1).quantity).isEqualTo(new BigDecimal(10));
        assertThat(ratingGroups.get(1).accumulatedQuantity).isEqualTo(new BigDecimal(15));
        assertThat(ratingGroups.get(1).parameters.size()).isEqualTo(3);
        assertThat(ratingGroups.get(1).parameters.get(0).period).isEqualTo(getPeriod("2020-01-20", "2020-02-10"));
        assertThat(ratingGroups.get(1).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("d");
        assertThat(ratingGroups.get(1).parameters.get(1).period).isEqualTo(getPeriod("2020-02-10", "2020-02-25"));
        assertThat(ratingGroups.get(1).parameters.get(1).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("f");
        assertThat(ratingGroups.get(1).parameters.get(2).period).isEqualTo(getPeriod("2020-02-25", "2020-03-25"));
        assertThat(ratingGroups.get(1).parameters.get(2).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("g");

        assertThat(ratingGroups.get(2).quantity).isEqualTo(new BigDecimal(1));
        assertThat(ratingGroups.get(2).accumulatedQuantity).isEqualTo(new BigDecimal(16));
        assertThat(ratingGroups.get(2).parameters.size()).isEqualTo(1);
        assertThat(ratingGroups.get(2).parameters.get(0).period).isEqualTo(getPeriod("2020-02-25", "2020-03-25"));
        assertThat(ratingGroups.get(2).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("g");

    }

    @Test
    public void test_determineRatingGroups_recurring_rating_period_within_5serviceParameters() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-01", "2020-02-28", 20d);

        List<ServiceRatingParameterGroup> ratingGroups = ratingScript.determineRatingGroups(wo, false);

        assertThat(ratingGroups.size()).isEqualTo(3);
        assertThat(ratingGroups.get(0).quantity).isEqualTo(new BigDecimal(5));
        assertThat(ratingGroups.get(0).accumulatedQuantity).isEqualTo(new BigDecimal(5));
        assertThat(ratingGroups.get(0).parameters.size()).isEqualTo(4);
        assertThat(ratingGroups.get(0).parameters.get(0).period).isEqualTo(getPeriod("2020-01-01", "2020-01-10"));
        assertThat(ratingGroups.get(0).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("c");
        assertThat(ratingGroups.get(0).parameters.get(1).period).isEqualTo(getPeriod("2020-01-10", "2020-02-10"));
        assertThat(ratingGroups.get(0).parameters.get(1).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("d");
        assertThat(ratingGroups.get(0).parameters.get(2).period).isEqualTo(getPeriod("2020-02-10", "2020-02-25"));
        assertThat(ratingGroups.get(0).parameters.get(2).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("f");
        assertThat(ratingGroups.get(0).parameters.get(3).period).isEqualTo(getPeriod("2020-02-25", "2020-02-28"));
        assertThat(ratingGroups.get(0).parameters.get(3).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("g");

        assertThat(ratingGroups.get(1).quantity).isEqualTo(new BigDecimal(10));
        assertThat(ratingGroups.get(1).accumulatedQuantity).isEqualTo(new BigDecimal(15));
        assertThat(ratingGroups.get(1).parameters.size()).isEqualTo(3);
        assertThat(ratingGroups.get(1).parameters.get(0).period).isEqualTo(getPeriod("2020-01-20", "2020-02-10"));
        assertThat(ratingGroups.get(1).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("d");
        assertThat(ratingGroups.get(1).parameters.get(1).period).isEqualTo(getPeriod("2020-02-10", "2020-02-25"));
        assertThat(ratingGroups.get(1).parameters.get(1).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("f");
        assertThat(ratingGroups.get(1).parameters.get(2).period).isEqualTo(getPeriod("2020-02-25", "2020-02-28"));
        assertThat(ratingGroups.get(1).parameters.get(2).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("g");

        assertThat(ratingGroups.get(2).quantity).isEqualTo(new BigDecimal(1));
        assertThat(ratingGroups.get(2).accumulatedQuantity).isEqualTo(new BigDecimal(16));
        assertThat(ratingGroups.get(2).parameters.size()).isEqualTo(1);
        assertThat(ratingGroups.get(2).parameters.get(0).period).isEqualTo(getPeriod("2020-02-25", "2020-02-28"));
        assertThat(ratingGroups.get(2).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("g");

    }

    @Test
    public void test_determineRatingGroups_recurring_rating_period_within_3serviceParameters() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-15", "2020-02-21", 20d);

        List<ServiceRatingParameterGroup> ratingGroups = ratingScript.determineRatingGroups(wo, false);

        assertThat(ratingGroups.size()).isEqualTo(2);
        assertThat(ratingGroups.get(0).quantity).isEqualTo(new BigDecimal(5));
        assertThat(ratingGroups.get(0).accumulatedQuantity).isEqualTo(new BigDecimal(5));
        assertThat(ratingGroups.get(0).parameters.size()).isEqualTo(2);
        assertThat(ratingGroups.get(0).parameters.get(0).period).isEqualTo(getPeriod("2020-01-15", "2020-02-10"));
        assertThat(ratingGroups.get(0).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("d");
        assertThat(ratingGroups.get(0).parameters.get(1).period).isEqualTo(getPeriod("2020-02-10", "2020-02-21"));
        assertThat(ratingGroups.get(0).parameters.get(1).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("f");

        assertThat(ratingGroups.get(1).quantity).isEqualTo(new BigDecimal(10));
        assertThat(ratingGroups.get(1).accumulatedQuantity).isEqualTo(new BigDecimal(15));
        assertThat(ratingGroups.get(1).parameters.size()).isEqualTo(2);
        assertThat(ratingGroups.get(1).parameters.get(0).period).isEqualTo(getPeriod("2020-01-20", "2020-02-10"));
        assertThat(ratingGroups.get(1).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("d");
        assertThat(ratingGroups.get(1).parameters.get(1).period).isEqualTo(getPeriod("2020-02-10", "2020-02-21"));
        assertThat(ratingGroups.get(1).parameters.get(1).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("f");

    }

    @Test
    public void test_determineRatingGroups_recurring_rating_period_within_3serviceParameters_quantityZero() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-02-11", "2020-02-28", 20d);

        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-02-10", "2020-02-25"), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "0|a|b|f|||" }));

        List<ServiceRatingParameterGroup> ratingGroups = ratingScript.determineRatingGroups(wo, false);

        assertThat(ratingGroups.size()).isEqualTo(2);
        assertThat(ratingGroups.get(0).quantity).isEqualTo(new BigDecimal(0));
        assertThat(ratingGroups.get(0).accumulatedQuantity).isEqualTo(new BigDecimal(0));
        assertThat(ratingGroups.get(0).parameters.size()).isEqualTo(2);
        assertThat(ratingGroups.get(0).parameters.get(0).period).isEqualTo(getPeriod("2020-02-11", "2020-02-25"));
        assertThat(ratingGroups.get(0).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("f");
        assertThat(ratingGroups.get(0).parameters.get(1).period).isEqualTo(getPeriod("2020-02-25", "2020-02-28"));
        assertThat(ratingGroups.get(0).parameters.get(1).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("g");

        assertThat(ratingGroups.get(1).quantity).isEqualTo(new BigDecimal(16));
        assertThat(ratingGroups.get(1).accumulatedQuantity).isEqualTo(new BigDecimal(16));
        assertThat(ratingGroups.get(1).parameters.size()).isEqualTo(1);
        assertThat(ratingGroups.get(1).parameters.get(0).period).isEqualTo(getPeriod("2020-02-25", "2020-02-28"));
        assertThat(ratingGroups.get(1).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("g");

    }

    @Test
    public void test_determineRatingGroups_recurring_rating_period_within_1serviceParameters() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-02-15", "2020-02-21", 20d);

        List<ServiceRatingParameterGroup> ratingGroups = ratingScript.determineRatingGroups(wo, false);

        assertThat(ratingGroups.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).quantity).isEqualTo(new BigDecimal(15));
        assertThat(ratingGroups.get(0).parameters.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).parameters.get(0).period).isEqualTo(getPeriod("2020-02-15", "2020-02-21"));
        assertThat(ratingGroups.get(0).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("f");
    }

    @Test
    public void test_determineRatingGroups_recurring_rating_period_within_1serviceParameters_noEndDate() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-05-15", "2020-05-30", 20d);

        List<ServiceRatingParameterGroup> ratingGroups = ratingScript.determineRatingGroups(wo, false);

        assertThat(ratingGroups.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).quantity).isEqualTo(new BigDecimal(16));
        assertThat(ratingGroups.get(0).parameters.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).parameters.get(0).period).isEqualTo(getPeriod("2020-05-15", "2020-05-30"));
        assertThat(ratingGroups.get(0).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("h");
    }

    @Test
    public void test_determineRatingGroups_recurring_rating_period_within_2serviceParameters_noEndDate() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-03-15", "2020-05-30", 20d);

        List<ServiceRatingParameterGroup> ratingGroups = ratingScript.determineRatingGroups(wo, false);

        assertThat(ratingGroups.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).quantity).isEqualTo(new BigDecimal(16));
        assertThat(ratingGroups.get(0).parameters.size()).isEqualTo(2);

        assertThat(ratingGroups.get(0).parameters.get(0).period).isEqualTo(getPeriod("2020-03-15", "2020-05-01")); // Should be 2020-03-25, but as there is a gap, it takes the next
        // period start value as end value
        assertThat(ratingGroups.get(0).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("g");
        assertThat(ratingGroups.get(0).parameters.get(1).period).isEqualTo(getPeriod("2020-05-01", "2020-05-30"));
        assertThat(ratingGroups.get(0).parameters.get(1).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("h");

    }

    @Test
    public void test_determineRatingGroups_recurring_rating_period_reimburse_1serviceParameters_beforeTerminationDate() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, false, ChargeApplicationModeEnum.REIMBURSMENT, "2025-01-13", "2020-03-27", "2020-04-25", 20d);

        List<ServiceRatingParameterGroup> ratingGroups = ratingScript.determineRatingGroups(wo, false);

        assertThat(ratingGroups.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).quantity).isEqualTo(new BigDecimal(16));
        assertThat(ratingGroups.get(0).parameters.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).parameters.get(0).period).isEqualTo(getPeriod("2020-03-27", "2020-04-25"));
        assertThat(ratingGroups.get(0).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("e");
    }

    @Test
    public void test_determineRatingGroups_recurring_rating_period_reimburse_1serviceParameters_beforeTerminationDate_endsOnTerminationDate() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, false, ChargeApplicationModeEnum.REIMBURSMENT, "2025-01-13", "2020-04-15", "2020-05-01", 20d);

        List<ServiceRatingParameterGroup> ratingGroups = ratingScript.determineRatingGroups(wo, false);

        assertThat(ratingGroups.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).quantity).isEqualTo(new BigDecimal(16));
        assertThat(ratingGroups.get(0).parameters.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).parameters.get(0).period).isEqualTo(getPeriod("2020-04-15", "2020-05-01"));
        assertThat(ratingGroups.get(0).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("e");
    }

    @Test
    public void test_determineRatingGroups_recurring_rating_period_reimburse_1serviceParameters_startingTerminationDate() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, false, ChargeApplicationModeEnum.REIMBURSMENT, "2025-01-13", "2020-05-01", "2020-06-01", 20d);

        List<ServiceRatingParameterGroup> ratingGroups = ratingScript.determineRatingGroups(wo, false);

        assertThat(ratingGroups.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).quantity).isEqualTo(new BigDecimal(16));
        assertThat(ratingGroups.get(0).parameters.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).parameters.get(0).period).isEqualTo(getPeriod("2020-05-01", "2020-06-01"));
        assertThat(ratingGroups.get(0).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("e");
    }

    @Test
    public void test_determineRatingGroups_recurring_rating_period_reimburse_1serviceParameters_pastTerminationDate() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, false, ChargeApplicationModeEnum.REIMBURSMENT, "2025-01-13", "2020-06-01", "2020-07-01", 20d);

        List<ServiceRatingParameterGroup> ratingGroups = ratingScript.determineRatingGroups(wo, false);

        assertThat(ratingGroups.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).quantity).isEqualTo(new BigDecimal(16));
        assertThat(ratingGroups.get(0).parameters.size()).isEqualTo(1);
        assertThat(ratingGroups.get(0).parameters.get(0).period).isEqualTo(getPeriod("2020-06-01", "2020-07-01"));
        assertThat(ratingGroups.get(0).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("e");
    }

    @Test
    public void test_determineRatingGroups_recurring_rating_period_reimburse_6serviceParameters() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, false, ChargeApplicationModeEnum.REIMBURSMENT, "2025-01-13", "2020-01-05", "2020-06-01", 20d);

        List<ServiceRatingParameterGroup> ratingGroups = ratingScript.determineRatingGroups(wo, false);

        assertThat(ratingGroups.size()).isEqualTo(3);

        assertThat(ratingGroups.get(0).quantity).isEqualTo(new BigDecimal(5));
        assertThat(ratingGroups.get(0).accumulatedQuantity).isEqualTo(new BigDecimal(5));
        assertThat(ratingGroups.get(0).parameters.size()).isEqualTo(5);
        assertThat(ratingGroups.get(0).parameters.get(0).period).isEqualTo(getPeriod("2020-01-05", "2020-01-10"));
        assertThat(ratingGroups.get(0).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("c");
        assertThat(ratingGroups.get(0).parameters.get(1).period).isEqualTo(getPeriod("2020-01-10", "2020-02-10"));
        assertThat(ratingGroups.get(0).parameters.get(1).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("d");
        assertThat(ratingGroups.get(0).parameters.get(2).period).isEqualTo(getPeriod("2020-02-10", "2020-02-25"));
        assertThat(ratingGroups.get(0).parameters.get(2).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("f");
        assertThat(ratingGroups.get(0).parameters.get(3).period).isEqualTo(getPeriod("2020-02-25", "2020-03-25"));
        assertThat(ratingGroups.get(0).parameters.get(3).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("g");
        assertThat(ratingGroups.get(0).parameters.get(4).period).isEqualTo(getPeriod("2020-03-25", "2020-06-01"));
        assertThat(ratingGroups.get(0).parameters.get(4).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("e");

        assertThat(ratingGroups.get(1).quantity).isEqualTo(new BigDecimal(10));
        assertThat(ratingGroups.get(1).accumulatedQuantity).isEqualTo(new BigDecimal(15));
        assertThat(ratingGroups.get(1).parameters.size()).isEqualTo(4);
        assertThat(ratingGroups.get(1).parameters.get(0).period).isEqualTo(getPeriod("2020-01-20", "2020-02-10"));
        assertThat(ratingGroups.get(1).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("d");
        assertThat(ratingGroups.get(1).parameters.get(1).period).isEqualTo(getPeriod("2020-02-10", "2020-02-25"));
        assertThat(ratingGroups.get(1).parameters.get(1).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("f");
        assertThat(ratingGroups.get(1).parameters.get(2).period).isEqualTo(getPeriod("2020-02-25", "2020-03-25"));
        assertThat(ratingGroups.get(1).parameters.get(2).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("g");
        assertThat(ratingGroups.get(1).parameters.get(3).period).isEqualTo(getPeriod("2020-03-25", "2020-06-01"));
        assertThat(ratingGroups.get(1).parameters.get(3).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("e");

        assertThat(ratingGroups.get(2).quantity).isEqualTo(new BigDecimal(1));
        assertThat(ratingGroups.get(2).accumulatedQuantity).isEqualTo(new BigDecimal(16));
        assertThat(ratingGroups.get(2).parameters.size()).isEqualTo(2);
        assertThat(ratingGroups.get(2).parameters.get(0).period).isEqualTo(getPeriod("2020-02-25", "2020-03-25"));
        assertThat(ratingGroups.get(2).parameters.get(0).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("g");
        assertThat(ratingGroups.get(2).parameters.get(1).period).isEqualTo(getPeriod("2020-03-25", "2020-06-01"));
        assertThat(ratingGroups.get(2).parameters.get(1).parameters.get(ServiceParametersCFEnum.SLA.getLabel())).isEqualTo("e");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_rateOneShotCharge() {

        // Start and end date should be irrelevant
        WalletOperation wo = createWO("subscription", TransformationRulesEnum.MISC.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-13", "2025-01-13", "2025-01-14", 20d);

        doNothing().when(ratingScript).applyFlatDiscounts(any());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-01-20", "O", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" },
                { "2020-01-20", "2020-10-25", "O", 28d, "second-1", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.MISC.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, true, false, wo.getServiceInstance().getCode(), wo.getOperationDate(), null);

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        when(serviceTemplateService.findByCode("Misc")).thenReturn(wo.getServiceInstance().getServiceTemplate());

        Map<String, String> cfValues = (Map<String, String>) wo.getServiceInstance().getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), wo.getOperationDate());
        Map<String, Object> serviceParams = ratingScript.getServiceParametersCFT().deserializeMultiValue(cfValues.values().iterator().next(), null);

        wo = ratingScript.rateOneShotCharge(wo.getWallet().getUserAccount(), wo.getSeller(), wo.getOfferTemplate(), "Misc", (OneShotChargeTemplate) wo.getChargeInstance().getChargeTemplate(), serviceParams,
            wo.getQuantity(), wo.getOperationDate());

        verify(walletOperationService, times(0)).create(any());

        assertThat(wo.getQuantity().setScale(2, RoundingMode.HALF_UP)).isEqualTo(BigDecimal.valueOf(20).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getUnitAmountWithoutTax()).isEqualTo(new BigDecimal(25)); // flat amount $25
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_subscription() {

        // Start and end date should be irrelevant
        WalletOperation wo = createWO("subscription", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-13", "2025-01-13", "2025-01-14", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-01-20", "O", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" },
                { "2020-01-20", "2020-10-25", "O", 28d, "second-1", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, true, false, wo.getServiceInstance().getCode(), wo.getOperationDate(), null);

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        verify(walletOperationService, times(0)).create(any());

        assertThat(wo.getQuantity().setScale(2, RoundingMode.HALF_UP)).isEqualTo(BigDecimal.valueOf(20).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getUnitAmountWithoutTax()).isEqualTo(new BigDecimal(25)); // flat amount $25
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Misc_other() {

        // Start and end date should be irrelevant
        WalletOperation wo = createWO("other", TransformationRulesEnum.DATA.name(), false, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-13", "2025-01-13", "2025-01-14", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());

        OfferTemplate miscOffer = new OfferTemplate();
        miscOffer.setCode("MISC offer");

        when(offerTemplateService.findByServiceTemplate(any())).thenReturn(Arrays.asList(miscOffer));

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-01-20", "O", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" },
                { "2020-01-20", "2020-10-25", "O", 28d, "second-1", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, true, false, "Misc", wo.getOperationDate(), null);

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ServiceTemplate serviceTemplate = new ServiceTemplate();
        serviceTemplate.setId(5L);

        doReturn(serviceTemplate).when(ratingScript).getServiceTemplateFromChargeInstanceCF(any());

        ratingScript.execute(initContext);

        verify(walletOperationService, times(0)).create(any());

        assertThat(wo.getQuantity().setScale(2, RoundingMode.HALF_UP)).isEqualTo(BigDecimal.valueOf(20).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getUnitAmountWithoutTax()).isEqualTo(new BigDecimal(25)); // flat amount $25
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getOfferCode()).isEqualTo(miscOffer.getCode());

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurring_1quantity_over_1serviceParameter_noEndDate() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-05-15", "2020-05-25", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-08-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        verify(walletOperationService, times(0)).create(any());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(16));
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(25).setScale(2, RoundingMode.HALF_UP)); // 10/10 days at $25
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurring_1quantity_over_2serviceParameters() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-01", "2020-01-15", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-02-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(5));
        // 9/14 days at $25
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(16.07).setScale(2, RoundingMode.HALF_UP)); // 9/14 days and 5/14 at $25
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-01-01");
        assertThat(wo.getEndDate()).isEqualTo("2020-01-10");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);

        WalletOperation wo2 = argument.getAllValues().get(0);
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(5));
        // 5/14 days at $25
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(8.93d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getDescription()).isEqualTo("first-1");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-10");
        assertThat(wo2.getEndDate()).isEqualTo("2020-01-15");

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurring_2quantities_over_3ServiceParameters() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-01", "2020-02-05", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-02-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(2)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(5));
        // 9/35 days at $25
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(6.43d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-01-01");
        assertThat(wo.getEndDate()).isEqualTo("2020-01-10");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);

        WalletOperation wo2 = argument.getAllValues().get(0);
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(5));
        // 26/35 days at $25
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(18.57d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getDescription()).isEqualTo("first-1");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-10");
        assertThat(wo2.getEndDate()).isEqualTo("2020-02-05");

        WalletOperation wo3 = argument.getAllValues().get(1);
        assertThat(wo3.getQuantity()).isEqualTo(new BigDecimal(10));
        // 16/35 days at $25
        assertThat(wo3.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(11.43d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo3.getDescription()).isEqualTo("first-1");
        assertThat(wo3.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo3.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo3.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo3.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo3.getStartDate()).isEqualTo("2020-01-20");
        assertThat(wo3.getEndDate()).isEqualTo("2020-02-05");

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurring_2quantities_over_3ServiceParameters_and_2Tariffs() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-01", "2020-02-05", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[] tariff1 = new Object[] { "2019-01-01", "2020-01-15", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" };
        Object[] tariff2 = new Object[] { "2020-01-15", "2020-02-20", "R", 5d, "second-1", 2L, 7L, 8L, null, null, null, null, null, null, 1L, "1", "val" };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), new Object[][] { tariff1 });
        List<TariffRecordGroup> tariffRatingGroups1 = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), new Object[][] { tariff2 });
        List<TariffRecordGroup> tariffRatingGroups2 = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), new Object[][] { tariff1, tariff2 });
        List<TariffRecordGroup> tariffRatingGroupsAll = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroupsAll).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());
        doReturn(tariffRatingGroups1).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-01-01", "2020-01-10")), any(), anyBoolean());
        doReturn(tariffRatingGroups2).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-01-20", "2020-02-05")), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(3)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(5));
        // 9/35 at $25
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(6.43d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-01-01");
        assertThat(wo.getEndDate()).isEqualTo("2020-01-10");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(5d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(5.5d);

        WalletOperation wo2 = argument.getAllValues().get(0);
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(5));
        // 5/35 days at $25
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(3.57d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getDescription()).isEqualTo("first-1");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-10");
        assertThat(wo2.getEndDate()).isEqualTo("2020-01-15");

        WalletOperation wo3 = argument.getAllValues().get(1);
        assertThat(wo3.getQuantity()).isEqualTo(new BigDecimal(5));
        // 21/35 days at $5
        assertThat(wo3.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(3d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo3.getDescription()).isEqualTo("second-1");
        assertThat(wo3.getTaxClass().getId()).isEqualTo(2L);
        assertThat(wo3.getInvoiceSubCategory().getId()).isEqualTo(7L);
        assertThat(wo3.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo3.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo3.getStartDate()).isEqualTo("2020-01-15");
        assertThat(wo3.getEndDate()).isEqualTo("2020-02-05");

        WalletOperation wo4 = argument.getAllValues().get(2);
        assertThat(wo4.getQuantity()).isEqualTo(new BigDecimal(10));
        // 16/35 at $5
        assertThat(wo4.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(2.29d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo4.getDescription()).isEqualTo("second-1");
        assertThat(wo4.getTaxClass().getId()).isEqualTo(2L);
        assertThat(wo4.getInvoiceSubCategory().getId()).isEqualTo(7L);
        assertThat(wo4.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo4.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo4.getStartDate()).isEqualTo("2020-01-20");
        assertThat(wo4.getEndDate()).isEqualTo("2020-02-05");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurring_2quantities_over_2serviceParameters_with_overridenAmountInWo() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-10", "2020-02-10", 20d);

        wo.setUnitAmountWithoutTax(new BigDecimal(100));

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-02-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(5));
        // 31/31 days at $100
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(100).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-01-10");
        assertThat(wo.getEndDate()).isEqualTo("2020-02-10");

        WalletOperation wo2 = argument.getAllValues().get(0);
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(10));
        // 21/31 days at $100
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(67.74d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getDescription()).isEqualTo("first-1");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-20");
        assertThat(wo2.getEndDate()).isEqualTo("2020-02-10");

        assertThat((Double) wo2.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(100d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(110d);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurring_with_overridenAmountInServiceParameter_356_days() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-01", "2020-04-01", 20d);

        wo.getServiceInstance().removeCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());
        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-01", "2021-10-01"), 1,
            MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|d|100|40|" }));

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2021-10-01", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(0)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(5));
        assertThat(wo.getUnitAmountWithoutTax()).isNull();
        // 3/12 month * 100
        assertThat(wo.getAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(25d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-01-01");
        assertThat(wo.getEndDate()).isEqualTo("2020-04-01");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurring_with_overridenAmountInServiceParameter_356_days_prorated() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-10", "2020-04-01", 20d);
        wo.setFullRatingPeriod(getPeriod("2020-01-01", "2020-04-01"));
        wo.getServiceInstance().removeCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());
        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-01", "2021-10-01"), 1,
            MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|d|100|40|" }));

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2021-10-01", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(0)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(5));
        assertThat(wo.getUnitAmountWithoutTax()).isNull();
        // 82/91 days * 3/12 month * 100
        assertThat(wo.getAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(22.53d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-01-10");
        assertThat(wo.getEndDate()).isEqualTo("2020-04-01");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurring_with_overridenAmountInServiceParameter_356_days_prorated_2serviceParameters() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-10", "2020-04-01", 20d);
        wo.setFullRatingPeriod(getPeriod("2020-01-01", "2020-04-01"));
        wo.getServiceInstance().removeCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());
        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-01", "2020-01-30"), 1,
            MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|d|100|40|" }));
        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-30", "2021-10-01"), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|d|||" }));

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2021-10-01", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(5));
        assertThat(wo.getUnitAmountWithoutTax()).isNull();
        // 82/91 days * 1/12 month * 100
        assertThat(wo.getAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(7.51d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-01-10");
        assertThat(wo.getEndDate()).isEqualTo("2020-01-30");

        WalletOperation wo2 = argument.getAllValues().get(0);
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(5));
        // 62/91 days at $25
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(17.03d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getDescription()).isEqualTo("first-1");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-30");
        assertThat(wo2.getEndDate()).isEqualTo("2020-04-01");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurring_with_overridenAmountInServiceParameter_355_days() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-04-01", "2020-07-01", 20d);

        wo.getServiceInstance().removeCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());
        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-01", "2021-10-01"), 1,
            MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|d|100|40|" }));

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2021-10-01", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(0)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(5));
        assertThat(wo.getUnitAmountWithoutTax()).isNull();
        // 3/12 month * 100
        assertThat(wo.getAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(25d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-04-01");
        assertThat(wo.getEndDate()).isEqualTo("2020-07-01");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_subscription_with_overridenAmountInServiceParameter() {

        // Start/end dates and quantity should be irrelevant
        WalletOperation wo = createWO("subscription", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-15", "2025-01-10", "2025-02-10", 20);

        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-10", "2020-01-20"), 1,
            MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|d|30|40|" }));

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2021-02-20", "O", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, true, false, wo.getServiceInstance().getCode(), wo.getOperationDate(), null);

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(0)).create(argument.capture());

        // For OS, quantity is taken from WO.quantity
        assertThat(wo.getQuantity().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(20d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getUnitAmountWithoutTax()).isNull(); // .setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(0d).setScale(2, RoundingMode.HALF_UP)); // Only fixed
                                                           // price
        // $40 fixed price from service parameters
        assertThat(wo.getAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(40d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getOperationDate()).isEqualTo("2020-01-15");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(40d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(44d);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurring_2quantities_over_2serviceParameters_with_quantityBasedService() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-10", "2020-02-10", 20d);

        wo.getServiceInstance().getServiceTemplate().setCfValue(ServiceTemplateCFsEnum.PRODUCT_SET.name(), new EntityReferenceWrapper(CustomEntityInstance.class.getName(), "Product_set", GNP_PRODUCT_SET));

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-02-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, 1, 20, 4, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(4)); // quantity cut-of at 4
        assertThat(wo.getInputQuantity()).isEqualTo(new BigDecimal(5));
        // 31/31 days at $25
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(25d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-01-10");
        assertThat(wo.getEndDate()).isEqualTo("2020-02-10");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);

        WalletOperation wo2 = argument.getAllValues().get(0);
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(4)); // quantity cut-of at 4
        assertThat(wo2.getInputQuantity()).isEqualTo(new BigDecimal(10));
        // 21/31 days at $25
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(16.94d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getDescription()).isEqualTo("first-1");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-20");
        assertThat(wo2.getEndDate()).isEqualTo("2020-02-10");

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurring_2quantities_over_2serviceParameters_withDistance() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-10", "2020-02-10", 20d);

        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-10", "2020-01-20"), 1,
            MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|d|||505" }));
        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-20", "2020-02-10"), 1,
            MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "15|a|b|d|||260" }));

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-01-20", "R", 125d, "first-1", 1L, 5L, 6L, 0, null, null, null, null, null, 1L, "1", "val" },
                { "2019-01-01", "2020-01-20", "R", 25d, "first-1", 1L, 5L, 6L, 1, 300, 300, null, null, null, 1L, "1", "val" },
                { "2019-01-01", "2020-01-20", "R", 10d, "first-1", 1L, 5L, 6L, 301, 800, 100, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups1 = ratingScript.convertToTariffRecordGroups(tariffLines, false, true, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        tariffs = new Object[][] { { "2019-01-20", "2020-02-25", "R", 5d, "second-1", 1L, 5L, 6L, 0, null, null, null, null, null, 1L, "1", "val" },
                { "2019-01-20", "2020-02-25", "R", 1.5d, "second-1", 1L, 5L, 6L, 1, 300, 3, null, null, null, 1L, "1", "val" } };

        tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups2 = ratingScript.convertToTariffRecordGroups(tariffLines, false, true, wo.getChargeInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups1).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-01-10", "2020-01-20")), any(), anyBoolean());
        doReturn(tariffRatingGroups2).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-01-20", "2020-02-10")), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(2)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(5));
        // 10/31 days at $125+25+roundToNext(205/100)*10
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(58.06d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-01-10");
        assertThat(wo.getEndDate()).isEqualTo("2020-01-20");

        WalletOperation wo2 = argument.getAllValues().get(0);
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(5));
        // 21/31 days at $5+260/3*1.5
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(91.79d).setScale(2, RoundingMode.HALF_UP)); // 21/31 days at
        assertThat(wo2.getDescription()).isEqualTo("second-1");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-20");
        assertThat(wo2.getEndDate()).isEqualTo("2020-02-10");

        WalletOperation wo3 = argument.getAllValues().get(1);
        assertThat(wo3.getQuantity()).isEqualTo(new BigDecimal(10));
        // 21/31 days at $5+260/3*1.5
        assertThat(wo3.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(91.79d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo3.getDescription()).isEqualTo("second-1");
        assertThat(wo3.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo3.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo3.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo3.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo3.getStartDate()).isEqualTo("2020-01-20");
        assertThat(wo3.getEndDate()).isEqualTo("2020-02-10");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(135.5d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(149.05d);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_reimburse_recurring_1quantity_over_1serviceParameter_woStartsBeforeTerminationDate_woEndsPastTerminationDate() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, false, ChargeApplicationModeEnum.REIMBURSMENT, "2025-01-13", "2020-03-15", "2020-05-25", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-08-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(-16));
        // 10/71 days at $25
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(3.52).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-03-15");
        assertThat(wo.getEndDate()).isEqualTo("2020-03-25");

        WalletOperation wo2 = argument.getAllValues().get(0);
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(-16));
        // 61/71 days at $25
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(21.48).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getDescription()).isEqualTo("first-1");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-03-25");
        assertThat(wo2.getEndDate()).isEqualTo("2020-05-25");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_reimburse_recurring_2quantities_over_2serviceParameter_woStartsBeforeTerminationDate_woEndsPastTerminationDate() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, false, ChargeApplicationModeEnum.REIMBURSMENT, "2025-01-13", "2020-02-15", "2020-05-25", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-08-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(4)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(-15));
        // 10/100 days at $25
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(2.5).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-02-15");
        assertThat(wo.getEndDate()).isEqualTo("2020-02-25");

        WalletOperation wo2 = argument.getAllValues().get(0);
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(-15));
        // 29/100 days at $25
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(7.25).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getDescription()).isEqualTo("first-1");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-02-25");
        assertThat(wo2.getEndDate()).isEqualTo("2020-03-25");

        WalletOperation wo3 = argument.getAllValues().get(1);
        assertThat(wo3.getQuantity()).isEqualTo(new BigDecimal(-15));
        // 61/100 days at $25
        assertThat(wo3.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(15.25).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo3.getDescription()).isEqualTo("first-1");
        assertThat(wo3.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo3.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo3.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo3.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo3.getStartDate()).isEqualTo("2020-03-25");
        assertThat(wo3.getEndDate()).isEqualTo("2020-05-25");

        WalletOperation wo4 = argument.getAllValues().get(2);
        assertThat(wo4.getQuantity()).isEqualTo(new BigDecimal(-1));
        // 29/100 days at $25
        assertThat(wo4.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(7.25).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo4.getDescription()).isEqualTo("first-1");
        assertThat(wo4.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo4.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo4.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo4.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo4.getStartDate()).isEqualTo("2020-02-25");
        assertThat(wo4.getEndDate()).isEqualTo("2020-03-25");

        WalletOperation wo5 = argument.getAllValues().get(3);
        assertThat(wo5.getQuantity()).isEqualTo(new BigDecimal(-1));
        // 61/100 days at $25
        assertThat(wo5.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(15.25).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo5.getDescription()).isEqualTo("first-1");
        assertThat(wo5.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo5.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo5.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo5.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo5.getStartDate()).isEqualTo("2020-03-25");
        assertThat(wo5.getEndDate()).isEqualTo("2020-05-25");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_reimburse_recurring_2quantities_over_3serviceParameters_and_2_tariffs_woStartsBeforeTerminationDate_woEndsBeforeTerminationDate() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, false, ChargeApplicationModeEnum.REIMBURSMENT, "2025-01-13", "2020-01-01", "2020-02-05", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[] tariff1 = new Object[] { "2019-01-01", "2020-01-15", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" };
        Object[] tariff2 = new Object[] { "2020-01-15", "2020-02-20", "R", 5d, "second-1", 2L, 7L, 8L, null, null, null, null, null, null, 1L, "1", "val" };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), new Object[][] { tariff1 });
        List<TariffRecordGroup> tariffRatingGroups1 = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), new Object[][] { tariff2 });
        List<TariffRecordGroup> tariffRatingGroups2 = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), new Object[][] { tariff1, tariff2 });
        List<TariffRecordGroup> tariffRatingGroupsAll = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroupsAll).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());
        doReturn(tariffRatingGroups1).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-01-01", "2020-01-10")), any(), anyBoolean());
        doReturn(tariffRatingGroups2).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-01-20", "2020-02-05")), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(3)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(-5));
        // 9/35 days at $25
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(6.43).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-01-01");
        assertThat(wo.getEndDate()).isEqualTo("2020-01-10");

        WalletOperation wo2 = argument.getAllValues().get(0);
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(-5));
        // 5/35 days at $25
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(3.57d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-10");
        assertThat(wo2.getEndDate()).isEqualTo("2020-01-15");

        WalletOperation wo3 = argument.getAllValues().get(1);
        assertThat(wo3.getQuantity()).isEqualTo(new BigDecimal(-5));
        // 21/35 days at $5
        assertThat(wo3.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(3d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo3.getDescription()).isEqualTo("second-1");
        assertThat(wo3.getTaxClass().getId()).isEqualTo(2L);
        assertThat(wo3.getInvoiceSubCategory().getId()).isEqualTo(7L);
        assertThat(wo3.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo3.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo3.getStartDate()).isEqualTo("2020-01-15");
        assertThat(wo3.getEndDate()).isEqualTo("2020-02-05");

        WalletOperation wo4 = argument.getAllValues().get(2);
        assertThat(wo4.getQuantity()).isEqualTo(new BigDecimal(-10));
        // 16/35 days at $5
        assertThat(wo4.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(2.29d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo4.getDescription()).isEqualTo("second-1");
        assertThat(wo4.getTaxClass().getId()).isEqualTo(2L);
        assertThat(wo4.getInvoiceSubCategory().getId()).isEqualTo(7L);
        assertThat(wo4.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo4.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo4.getStartDate()).isEqualTo("2020-01-20");
        assertThat(wo4.getEndDate()).isEqualTo("2020-02-05");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(5d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(5.5d);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_reimburse_recurring_1quantity_over_1serviceParameter_woStartsOnTerminationDate_woEndsPastTerminationDate() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, false, ChargeApplicationModeEnum.REIMBURSMENT, "2025-01-13", "2020-05-01", "2020-06-01", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-08-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        verify(walletOperationService, times(0)).create(any());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(-16));
        // 31/31 days at $25 (period that should have ended on 03/25 is extended to no end
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(25).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-05-01");
        assertThat(wo.getEndDate()).isEqualTo("2020-06-01");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_reimburse_recurring_1quantity_over_1serviceParameter_woStartsPastTerminationDate_woEndsPastTerminationDate() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, false, ChargeApplicationModeEnum.REIMBURSMENT, "2025-01-13", "2020-06-01", "2020-07-01", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-08-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        verify(walletOperationService, times(0)).create(any());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(-16));
        // 30/30 days at $25 (period that should have ended on 03/25 is extended to no end
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(25).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-06-01");
        assertThat(wo.getEndDate()).isEqualTo("2020-07-01");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_reimburse_recurring_2quantities_reduction_over_2serviceParameters_with_overridenAmountInServiceParameter() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, false, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-10", "2020-02-10", 20d);

        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2019-12-15", "2020-01-10"), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|c|||" }));
        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-10", "2020-01-20"), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|d|||" }));
        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-20", "2020-02-10"), 1,
            MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "3|a|b|d|30|40|" }));
        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-02-10", "2020-02-25"), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "3|a|b|f|||" }));

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-02-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        // 10/31 days at $25
        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(5));
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(8.06d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-01-10");
        assertThat(wo.getEndDate()).isEqualTo("2020-01-20");

        WalletOperation wo2 = argument.getAllValues().get(0);
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(3));
        // 21/31 days * 1/12month at fixed price of $30
        assertThat(wo2.getUnitAmountWithoutTax()).isNull();
        assertThat(wo2.getAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(1.69d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getDescription()).isEqualTo("first-1");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-20");
        assertThat(wo2.getEndDate()).isEqualTo("2020-02-10");

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(2.5d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(2.75d);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurringCalendarChange_1quantity_over_1serviceParameter_calendarChangeFromPeriodStart_noFullRatingPeriodValue() {

        // Operation date determine that its apply in advance charge.
        // Quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-05-03", "2020-05-03", "2020-05-25", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2020-05-03", null, "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 2L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(0)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(16));
        // 59/61 days at $25
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(24.18).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getAccountingCode().getId()).isEqualTo(6L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-05-03");
        assertThat(wo.getEndDate()).isEqualTo("2020-07-01");
        assertThat(wo.getOperationDate()).isEqualTo("2020-05-03");
        assertThat(wo.getFullRatingPeriod()).isEqualTo(getPeriod("2020-05-01", "2020-07-01"));
        assertThat(((RecurringChargeInstance) wo.getChargeInstance()).getCalendar().getId().longValue() == 2L);

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurringCalendarChange_1quantity_over_1serviceParameter_calendarChangeFromPeriodStart_noFullRatingPeriodValue_endOfPeriod() {

        // Operation date determine that its apply end of period charge.
        // Quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-05-25", "2020-05-03", "2020-05-25", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2020-05-03", null, "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 2L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(0)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(16));
        // 59/61 days at $25
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(24.18).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getAccountingCode().getId()).isEqualTo(6L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-05-03");
        assertThat(wo.getEndDate()).isEqualTo("2020-07-01");
        assertThat(wo.getOperationDate()).isEqualTo("2020-07-01");
        assertThat(wo.getFullRatingPeriod()).isEqualTo(getPeriod("2020-05-01", "2020-07-01"));
        assertThat(((RecurringChargeInstance) wo.getChargeInstance()).getCalendar().getId().longValue() == 2L);

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurringCalendarChange_1quantity_over_1serviceParameter_calendarChangeFromPeriodStart_wFullRatingPeriodValue() {

        // Operation date determine that its apply in advance charge.
        // Quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-05-03", "2020-05-03", "2020-05-25", 20d);
        wo.setFullRatingPeriod(getPeriod("2020-05-02", "2020-05-31")); // this will be ignored as proration will take new calendar for prorata calculation

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2020-05-03", null, "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 2L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(0)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(16));
        // 59/61 days at $25
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(24.18).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getAccountingCode().getId()).isEqualTo(6L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-05-03");
        assertThat(wo.getEndDate()).isEqualTo("2020-07-01");
        assertThat(wo.getOperationDate()).isEqualTo("2020-05-03");
        assertThat(wo.getFullRatingPeriod()).isEqualTo(getPeriod("2020-05-01", "2020-07-01"));
        assertThat(((RecurringChargeInstance) wo.getChargeInstance()).getCalendar().getId().longValue() == 2L);

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurringCalendarChange_1quantity_over_1serviceParameter_calendarChangeFromPeriodStart_wFullRatingPeriodValue_endOfPeriod() {

        // Operation date determine that its apply end of period charge.
        // Quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-05-25", "2020-05-03", "2020-05-25", 20d);
        wo.setFullRatingPeriod(getPeriod("2020-05-02", "2020-05-31")); // this will be ignored as proration will take new calendar for prorata calculation

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2020-05-03", null, "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 2L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(0)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(16));
        // 59/61 days at $25
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(24.18).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getAccountingCode().getId()).isEqualTo(6L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-05-03");
        assertThat(wo.getEndDate()).isEqualTo("2020-07-01");
        assertThat(wo.getOperationDate()).isEqualTo("2020-07-01");
        assertThat(wo.getFullRatingPeriod()).isEqualTo(getPeriod("2020-05-01", "2020-07-01"));
        assertThat(((RecurringChargeInstance) wo.getChargeInstance()).getCalendar().getId().longValue() == 2L);

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(25d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(27.5d);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurringCalendarChange_1quantity_over_1serviceParameter_1calendarChangeMidPeriod_noFullRatingPeriodValue() {

        // Operation date determine that its apply in advance charge.
        // Quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-05-03", "2020-05-03", "2020-05-25", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2020-01-01", "2020-05-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" },
                { "2020-05-20", null, "R", 250d, "second-1", 2L, 7L, 8L, null, null, null, null, null, null, 2L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-05-03", "2020-05-25")), any(), anyBoolean());
        doReturn(tariffRatingGroups.subList(0, 1)).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-05-03", "2020-05-20")), any(), anyBoolean());
        doReturn(tariffRatingGroups.subList(1, 2)).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-05-20", "2020-07-01")), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(16));
        // 17/22 days at $25
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(19.32).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getAccountingCode().getId()).isEqualTo(6L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-05-03");
        assertThat(wo.getEndDate()).isEqualTo("2020-05-20");
        assertThat(wo.getOperationDate()).isEqualTo("2020-05-03");
        assertThat(wo.getFullRatingPeriod()).isEqualTo(getPeriod("2020-05-03", "2020-05-25"));
        assertThat(((RecurringChargeInstance) wo.getChargeInstance()).getCalendar().getId().longValue() == 2L);

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(16));
        // 42/61 days at $250
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(172.13d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getDescription()).isEqualTo("second-1");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(2L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(7L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(8L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-05-20");
        assertThat(wo2.getEndDate()).isEqualTo("2020-07-01");
        assertThat(wo2.getOperationDate()).isEqualTo("2020-05-03");
        assertThat(wo2.getFullRatingPeriod()).isEqualTo(getPeriod("2020-05-01", "2020-07-01"));

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(250d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(275d);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurringCalendarChange_1quantity_over_1serviceParameter_1calendarChangeMidPeriod_noFullRatingPeriodValue_endOfPeriod() {

        // Operation date determine that its apply end of period charge.
        // Quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-05-25", "2020-05-03", "2020-05-25", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2020-01-01", "2020-05-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" },
                { "2020-05-20", null, "R", 250d, "second-1", 2L, 7L, 8L, null, null, null, null, null, null, 2L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-05-03", "2020-05-25")), any(), anyBoolean());
        doReturn(tariffRatingGroups.subList(0, 1)).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-05-03", "2020-05-20")), any(), anyBoolean());
        doReturn(tariffRatingGroups.subList(1, 2)).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-05-20", "2020-07-01")), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(16));
        // 17/22 days at $25
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(19.32).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getAccountingCode().getId()).isEqualTo(6L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-05-03");
        assertThat(wo.getEndDate()).isEqualTo("2020-05-20");
        assertThat(wo.getOperationDate()).isEqualTo("2020-05-20");
        assertThat(wo.getFullRatingPeriod()).isEqualTo(getPeriod("2020-05-03", "2020-05-25"));
        assertThat(((RecurringChargeInstance) wo.getChargeInstance()).getCalendar().getId().longValue() == 2L);

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(16));
        // 42/61 days at $250
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(172.13d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getDescription()).isEqualTo("second-1");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(2L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(7L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(8L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-05-20");
        assertThat(wo2.getEndDate()).isEqualTo("2020-07-01");
        assertThat(wo2.getOperationDate()).isEqualTo("2020-07-01");
        assertThat(wo2.getFullRatingPeriod()).isEqualTo(getPeriod("2020-05-01", "2020-07-01"));

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(250d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(275d);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurringCalendarChange_1quantity_over_1serviceParameter_1calendarChangeMidPeriod_wFullRatingPeriodValue() {

        // Operation date determine that its apply in advance charge.
        // Quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-05-03", "2020-05-03", "2020-05-25", 20d);
        wo.setFullRatingPeriod(getPeriod("2020-05-02", "2020-05-30")); // Will be used to prorate the first WO

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2020-01-01", "2020-05-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" },
                { "2020-05-20", null, "R", 250d, "second-1", 2L, 7L, 8L, null, null, null, null, null, null, 2L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-05-03", "2020-05-25")), any(), anyBoolean());
        doReturn(tariffRatingGroups.subList(0, 1)).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-05-03", "2020-05-20")), any(), anyBoolean());
        doReturn(tariffRatingGroups.subList(1, 2)).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-05-20", "2020-07-01")), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(16));
        // 14/28 days at $25
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(15.18).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getAccountingCode().getId()).isEqualTo(6L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-05-03");
        assertThat(wo.getEndDate()).isEqualTo("2020-05-20");
        assertThat(wo.getOperationDate()).isEqualTo("2020-05-03");
        assertThat(wo.getFullRatingPeriod()).isEqualTo(getPeriod("2020-05-02", "2020-05-30"));
        assertThat(((RecurringChargeInstance) wo.getChargeInstance()).getCalendar().getId().longValue() == 2L);

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(16));
        // 42/61 days at $250
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(172.13d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getDescription()).isEqualTo("second-1");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(2L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(7L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(8L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-05-20");
        assertThat(wo2.getEndDate()).isEqualTo("2020-07-01");
        assertThat(wo2.getOperationDate()).isEqualTo("2020-05-03");
        assertThat(wo2.getFullRatingPeriod()).isEqualTo(getPeriod("2020-05-01", "2020-07-01"));

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(250d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(275d);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurringCalendarChange_1quantity_over_1serviceParameter_1calendarChangeMidPeriod_wFullRatingPeriodValue_endOfPeriod() {

        // Operation date determine that its apply end of period charge.
        // Quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-05-25", "2020-05-03", "2020-05-25", 20d);
        wo.setFullRatingPeriod(getPeriod("2020-05-02", "2020-05-30")); // Will be used to prorate the first WO

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2020-01-01", "2020-05-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" },
                { "2020-05-20", null, "R", 250d, "second-1", 2L, 7L, 8L, null, null, null, null, null, null, 2L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-05-03", "2020-05-25")), any(), anyBoolean());
        doReturn(tariffRatingGroups.subList(0, 1)).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-05-03", "2020-05-20")), any(), anyBoolean());
        doReturn(tariffRatingGroups.subList(1, 2)).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-05-20", "2020-07-01")), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(16));
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(15.18).setScale(2, RoundingMode.HALF_UP)); // 14/28 days at $25
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getAccountingCode().getId()).isEqualTo(6L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-05-03");
        assertThat(wo.getEndDate()).isEqualTo("2020-05-20");
        assertThat(wo.getOperationDate()).isEqualTo("2020-05-20");
        assertThat(wo.getFullRatingPeriod()).isEqualTo(getPeriod("2020-05-02", "2020-05-30"));
        assertThat(((RecurringChargeInstance) wo.getChargeInstance()).getCalendar().getId().longValue() == 2L);

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(16));
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(172.13d).setScale(2, RoundingMode.HALF_UP)); // 42/61 days at $250
        assertThat(wo2.getDescription()).isEqualTo("second-1");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(2L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(7L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(8L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-05-20");
        assertThat(wo2.getEndDate()).isEqualTo("2020-07-01");
        assertThat(wo2.getOperationDate()).isEqualTo("2020-07-01");
        assertThat(wo2.getFullRatingPeriod()).isEqualTo(getPeriod("2020-05-01", "2020-07-01"));

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(250d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(275d);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_execute_Data_recurringCalendarChange_1quantity_over_2serviceParameter_1calendarChangeMidPeriod_noFullRatingPeriodValue() {

        // Operation date determine that its apply in advance charge.
        // Quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, false, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-03-05", "2020-03-05", "2020-05-25", 20d);

        Map<String, Object> initContext = MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo });

        doNothing().when(ratingScript).applyFlatDiscounts(any());
        doReturn(null).when(ratingScript).applySLASurcharge(any(), anyBoolean());

        Object[][] tariffs = new Object[][] { { "2020-01-01", "2020-03-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" },
                { "2020-03-20", null, "R", 250d, "second-1", 2L, 7L, 8L, null, null, null, null, null, null, 2L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        // doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-03-05", "2020-05-25")),
        // any(), anyBoolean());
        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-03-05", "2020-03-25")), any(), anyBoolean());
        doReturn(tariffRatingGroups.subList(0, 1)).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-03-05", "2020-03-20")), any(), anyBoolean());
        doReturn(tariffRatingGroups.subList(1, 2)).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-03-20", "2020-03-25")), any(), anyBoolean());
        doReturn(tariffRatingGroups.subList(1, 2)).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-03-25", "2020-05-01")), any(), anyBoolean());
        // doReturn(tariffRatingGroups.subList(1, 2)).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-03-25",
        // "2020-05-25")), any(), anyBoolean());
        // doReturn(tariffRatingGroups.subList(1, 2)).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-03-25",
        // "2020-07-01")), any(), anyBoolean());
        // doReturn(tariffRatingGroups.subList(1, 2)).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-05-01",
        // "2020-05-25")), any(), anyBoolean());
        doReturn(tariffRatingGroups.subList(1, 2)).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), eq(getPeriod("2020-05-01", "2020-07-01")), any(), anyBoolean());

        ratingScript.execute(initContext);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(3)).create(argument.capture());

        assertThat(wo.getQuantity()).isEqualTo(new BigDecimal(16));
        // 15/81 days at $25. 81 was original charge calendar period 03/05 - 05/25
        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(4.63).setScale(2, RoundingMode.HALF_UP)); // 15/81 days at $25
        assertThat(wo.getDescription()).isEqualTo("first-1");
        assertThat(wo.getTaxClass().getId()).isEqualTo(1L);
        assertThat(wo.getInvoiceSubCategory().getId()).isEqualTo(5L);
        assertThat(wo.getAccountingCode().getId()).isEqualTo(6L);
        assertThat(wo.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo.getStartDate()).isEqualTo("2020-03-05");
        assertThat(wo.getEndDate()).isEqualTo("2020-03-20");
        assertThat(wo.getOperationDate()).isEqualTo("2020-03-05");
        assertThat(wo.getFullRatingPeriod()).isEqualTo(getPeriod("2020-03-05", "2020-05-25"));

        WalletOperation wo2 = argument.getAllValues().get(0);
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(16));
        // 5/61 days at $250. New period is used 03/01-05/01
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(20.49d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getDescription()).isEqualTo("second-1");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(2L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(7L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(8L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo2.getStartDate()).isEqualTo("2020-03-20");
        assertThat(wo2.getEndDate()).isEqualTo("2020-03-25");
        assertThat(wo2.getOperationDate()).isEqualTo("2020-03-05");
        assertThat(wo2.getFullRatingPeriod()).isEqualTo(getPeriod("2020-03-01", "2020-05-01"));

        WalletOperation wo3 = argument.getAllValues().get(1);
        assertThat(wo3.getQuantity()).isEqualTo(new BigDecimal(16));
        // 37/61 days at $250. New period is used 03/01-05/01
        assertThat(wo3.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(151.64d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo3.getDescription()).isEqualTo("second-1");
        assertThat(wo3.getTaxClass().getId()).isEqualTo(2L);
        assertThat(wo3.getInvoiceSubCategory().getId()).isEqualTo(7L);
        assertThat(wo3.getAccountingCode().getId()).isEqualTo(8L);
        assertThat(wo3.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo3.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo3.getStartDate()).isEqualTo("2020-03-25");
        assertThat(wo3.getEndDate()).isEqualTo("2020-05-01");
        assertThat(wo3.getOperationDate()).isEqualTo("2020-03-05");
        assertThat(wo3.getFullRatingPeriod()).isEqualTo(getPeriod("2020-03-01", "2020-05-01"));

        WalletOperation wo4 = argument.getAllValues().get(2);
        assertThat(wo4.getQuantity()).isEqualTo(new BigDecimal(16));
        // 61/61 days at $250
        assertThat(wo4.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(250d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo4.getDescription()).isEqualTo("second-1");
        assertThat(wo4.getTaxClass().getId()).isEqualTo(2L);
        assertThat(wo4.getInvoiceSubCategory().getId()).isEqualTo(7L);
        assertThat(wo4.getAccountingCode().getId()).isEqualTo(8L);
        assertThat(wo4.getTaxPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo4.getTax().getPercent()).isEqualTo(new BigDecimal(10));
        assertThat(wo4.getStartDate()).isEqualTo("2020-05-01");
        assertThat(wo4.getEndDate()).isEqualTo("2020-07-01");
        assertThat(wo4.getOperationDate()).isEqualTo("2020-05-01");
        assertThat(wo4.getFullRatingPeriod()).isEqualTo(getPeriod("2020-05-01", "2020-07-01"));

        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name())).isEqualTo(250d);
        assertThat((Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name())).isEqualTo(275d);
        assertThat(((RecurringChargeInstance) wo4.getChargeInstance()).getCalendar().getId().longValue() == 2L);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_estimate_Data_subscription() {

        // Start and end date should be irrelevant
        WalletOperation wo = createWO("subscription", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-13", "2025-01-13", "2025-01-14", 20d);

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-01-20", "O", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" },
                { "2020-01-20", "2020-10-25", "O", 28d, "second-1", 2L, 5L, 10L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, true, false, wo.getServiceInstance().getCode(), wo.getOperationDate(), null);

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        Map<String, String> cfValues = (Map<String, String>) wo.getServiceInstance().getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), wo.getOperationDate());
        Map<String, Object> serviceParams = ratingScript.getServiceParametersCFT().deserializeMultiValue(cfValues.values().iterator().next(), null);

        BigDecimal amountWithoutTax = ratingScript.estimateChargeAmount(null, wo.getOfferTemplate(), wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.O, serviceParams, wo.getQuantity(), wo.getOperationDate(),
            java.util.Calendar.YEAR);

        assertThat(amountWithoutTax.setScale(6, RoundingMode.HALF_UP)).isEqualTo(BigDecimal.valueOf(500).setScale(6, RoundingMode.HALF_UP));

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_estimate_Data_subscription_with_overridenAmountInServiceParameter() {

        // Start/end dates and quantity should be irrelevant
        WalletOperation wo = createWO("subscription", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-15", "2025-01-10", "2025-02-10", 20);

        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-10", "2020-01-20"), 1,
            MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|d|30|40|" }));

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2021-02-20", "O", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, true, false, wo.getServiceInstance().getCode(), wo.getOperationDate(), null);

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        Map<String, String> cfValues = (Map<String, String>) wo.getServiceInstance().getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), wo.getOperationDate());
        Map<String, Object> serviceParams = ratingScript.getServiceParametersCFT().deserializeMultiValue(cfValues.values().iterator().next(), null);

        BigDecimal amountWithoutTax = ratingScript.estimateChargeAmount(null, wo.getOfferTemplate(), wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.O, serviceParams, wo.getQuantity(), wo.getOperationDate(),
            java.util.Calendar.YEAR);

        assertThat(amountWithoutTax.setScale(6, RoundingMode.HALF_UP)).isEqualTo(BigDecimal.valueOf(40).setScale(6, RoundingMode.HALF_UP));

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_estimate_Data_recurring() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-05-15", "2020-05-25", 20d);

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-08-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        Map<String, String> cfValues = (Map<String, String>) wo.getServiceInstance().getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), wo.getStartDate());
        Map<String, Object> serviceParams = ratingScript.getServiceParametersCFT().deserializeMultiValue(cfValues.values().iterator().next(), null);

        BigDecimal amountWithoutTax = ratingScript.estimateChargeAmount(null, wo.getOfferTemplate(), wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.R, serviceParams, wo.getQuantity(), wo.getStartDate(),
            java.util.Calendar.MONTH);

        assertThat(amountWithoutTax.setScale(6, RoundingMode.HALF_UP)).isEqualTo(BigDecimal.valueOf(500).setScale(6, RoundingMode.HALF_UP));

        amountWithoutTax = ratingScript.estimateChargeAmount(null, wo.getOfferTemplate(), wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.R, serviceParams, wo.getQuantity(), wo.getStartDate(),
            java.util.Calendar.YEAR);

        assertThat(amountWithoutTax.setScale(6, RoundingMode.HALF_UP)).isEqualTo(BigDecimal.valueOf(6000).setScale(6, RoundingMode.HALF_UP));

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_estimate_Data_recurring_with_overridenAmountInServiceParameter() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-10", "2020-02-10", 20d);

        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-10", "2020-01-20"), 1,
            MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|d|30|40|" }));

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-02-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        Map<String, String> cfValues = (Map<String, String>) wo.getServiceInstance().getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), wo.getStartDate());
        Map<String, Object> serviceParams = ratingScript.getServiceParametersCFT().deserializeMultiValue(cfValues.values().iterator().next(), null);

        BigDecimal amountWithoutTax = ratingScript.estimateChargeAmount(null, wo.getOfferTemplate(), wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.R, serviceParams, wo.getQuantity(), wo.getStartDate(),
            java.util.Calendar.MONTH);
        // 31/366 days at $30 - but calculation prorates based on month, so its 1/12 month at $30
        assertThat(amountWithoutTax.setScale(6, RoundingMode.HALF_UP)).isEqualTo(BigDecimal.valueOf(2.50).setScale(6, RoundingMode.HALF_UP));

        amountWithoutTax = ratingScript.estimateChargeAmount(null, wo.getOfferTemplate(), wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.R, serviceParams, wo.getQuantity(), wo.getStartDate(),
            java.util.Calendar.YEAR);

        assertThat(amountWithoutTax.setScale(6, RoundingMode.HALF_UP)).isEqualTo(BigDecimal.valueOf(30).setScale(6, RoundingMode.HALF_UP));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_estimate_Data_recurring_with_overridenAmountInServiceParameter2() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-01", "2020-04-01", 20d);

        wo.getServiceInstance().removeCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());
        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-01", "2021-10-01"), 1,
            MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|d|100|40|" }));

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2021-10-01", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        Map<String, String> cfValues = (Map<String, String>) wo.getServiceInstance().getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), wo.getStartDate());
        Map<String, Object> serviceParams = ratingScript.getServiceParametersCFT().deserializeMultiValue(cfValues.values().iterator().next(), null);

        BigDecimal amountWithoutTax = ratingScript.estimateChargeAmount(null, wo.getOfferTemplate(), wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.R, serviceParams, wo.getQuantity(), wo.getStartDate(),
            java.util.Calendar.MONTH);
        // 1/12 month at $100
        assertThat(amountWithoutTax.setScale(6, RoundingMode.HALF_UP)).isEqualTo(BigDecimal.valueOf(8.333333).setScale(6, RoundingMode.HALF_UP));

        amountWithoutTax = ratingScript.estimateChargeAmount(null, wo.getOfferTemplate(), wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.R, serviceParams, wo.getQuantity(), wo.getStartDate(),
            java.util.Calendar.YEAR);

        assertThat(amountWithoutTax.setScale(6, RoundingMode.HALF_UP)).isEqualTo(BigDecimal.valueOf(100).setScale(6, RoundingMode.HALF_UP));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_estimate_Data_recurring_with_quantityBasedService() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-10", "2020-02-10", 20d);

        wo.getServiceInstance().getServiceTemplate().setCfValue(ServiceTemplateCFsEnum.PRODUCT_SET.name(), new EntityReferenceWrapper(CustomEntityInstance.class.getName(), "Product_set", GNP_PRODUCT_SET));

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-02-20", "R", 25d, "first-1", 1L, 5L, 6L, null, null, null, 1, 20, 4, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, false, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        Map<String, String> cfValues = (Map<String, String>) wo.getServiceInstance().getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), wo.getStartDate());
        Map<String, Object> serviceParams = ratingScript.getServiceParametersCFT().deserializeMultiValue(cfValues.values().iterator().next(), null);

        BigDecimal amountWithoutTax = ratingScript.estimateChargeAmount(null, wo.getOfferTemplate(), wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.R, serviceParams, wo.getQuantity(), wo.getStartDate(),
            java.util.Calendar.YEAR);

        // Quantity cut off at 4
        assertThat(amountWithoutTax.setScale(6, RoundingMode.HALF_UP)).isEqualTo(BigDecimal.valueOf(1200).setScale(6, RoundingMode.HALF_UP));

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_estimate_Data_recurring_withDistance() {

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, false, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2020-01-10", "2020-02-10", 20d);

        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-10", "2020-01-20"), 1,
            MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|d|||505" }));
        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-20", "2020-02-10"), 1,
            MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "15|a|b|d|||260" }));

        Object[][] tariffs = new Object[][] { { "2019-01-01", "2020-01-20", "R", 125d, "first-1", 1L, 5L, 6L, 0, null, null, null, null, null, 1L, "1", "val" },
                { "2019-01-01", "2020-01-20", "R", 25d, "first-1", 1L, 5L, 6L, 1, 300, 300, null, null, null, 1L, "1", "val" },
                { "2019-01-01", "2020-01-20", "R", 10d, "first-1", 1L, 5L, 6L, 301, 800, 100, null, null, null, 1L, "1", "val" } };

//        tariffs = new Object[][] { { "2019-01-20", "2020-02-25", "R", 5d, "second-1", 1L, 5L, 6L, 0, null, null, null, null, null, 1L, "1", "val" },
//                { "2019-01-20", "2020-02-25", "R", 1.5d, "second-1", 1L, 5L, 6L, 1, 300, 3, null, null, null, 1L, "1", "val" } };

        List<Map<String, Object>> tariffLines = constructTariffs(TransformationRulesEnum.DATA.name(), tariffs);

        List<TariffRecordGroup> tariffRatingGroups = ratingScript.convertToTariffRecordGroups(tariffLines, false, true, wo.getServiceInstance().getCode(), wo.getOperationDate(),
            new DatePeriod(wo.getStartDate(), wo.getEndDate()));

        doReturn(tariffRatingGroups).when(ratingScript).getMatchingTariffRecords(any(), any(), any(), any(), any(), any(), any(), any(), anyBoolean());

        Map<String, String> cfValues = (Map<String, String>) wo.getServiceInstance().getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), wo.getStartDate());
        Map<String, Object> serviceParams = ratingScript.getServiceParametersCFT().deserializeMultiValue(cfValues.values().iterator().next(), null);

        BigDecimal amountWithoutTax = ratingScript.estimateChargeAmount(null, wo.getOfferTemplate(), wo.getServiceInstance().getServiceTemplate(), ChargeTypeEnum.R, serviceParams, wo.getQuantity(), wo.getStartDate(),
            java.util.Calendar.YEAR);

        // at $125+25+roundToNext(205/100)*$10
        assertThat(amountWithoutTax.setScale(6, RoundingMode.HALF_UP)).isEqualTo(BigDecimal.valueOf(43200).setScale(6, RoundingMode.HALF_UP));
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameter_and_1config() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-11", "2020-01-11", "2020-01-15", 20d);

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));
        // WO at 10%
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(50d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getEndDate()).isEqualTo("2020-01-15");
        assertThat(wo2.getCode()).isEqualTo("one");
        assertThat(wo2.getDescription()).isEqualTo("first surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

    }

    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameter_and_1config_terminatedPastDate() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-11", "2020-01-11", "2020-01-15", 20d);
        wo.getChargeInstance().setStatus(InstanceStatusEnum.TERMINATED);
        wo.getChargeInstance().setTerminationDate(DateUtils.parseDateWithPattern("2020-01-10", DateUtils.DATE_PATTERN));
        ((RecurringChargeInstance) wo.getChargeInstance()).setChargeToDateOnTermination(wo.getChargeInstance().getTerminationDate());

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(0)).create(argument.capture());

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameter_and_1config_terminatedMidPeriod() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-11", "2020-01-11", "2020-01-15", 20d);
        wo.getChargeInstance().setStatus(InstanceStatusEnum.TERMINATED);
        wo.getChargeInstance().setTerminationDate(DateUtils.parseDateWithPattern("2020-01-13", DateUtils.DATE_PATTERN));
        ((RecurringChargeInstance) wo.getChargeInstance()).setChargeToDateOnTermination(wo.getChargeInstance().getTerminationDate());

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));
        // WO at 10% - only to termination date
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(25d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getEndDate()).isEqualTo("2020-01-13");
        assertThat(wo2.getCode()).isEqualTo("one");
        assertThat(wo2.getDescription()).isEqualTo("first surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameter_and_1config_terminatedEndPeriod() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-11", "2020-01-11", "2020-01-15", 20d);
        wo.getChargeInstance().setStatus(InstanceStatusEnum.TERMINATED);
        wo.getChargeInstance().setTerminationDate(DateUtils.parseDateWithPattern("2020-01-15", DateUtils.DATE_PATTERN));
        ((RecurringChargeInstance) wo.getChargeInstance()).setChargeToDateOnTermination(wo.getChargeInstance().getTerminationDate());

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));
        // WO at 10% - only to termination date
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(50d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getEndDate()).isEqualTo("2020-01-15");
        assertThat(wo2.getCode()).isEqualTo("one");
        assertThat(wo2.getDescription()).isEqualTo("first surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameter_and_1config_terminatedPastDate_Reimbursement() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.REIMBURSMENT, "2020-01-11", "2020-01-11", "2020-01-15", 20d);
        wo.getChargeInstance().setStatus(InstanceStatusEnum.TERMINATED);
        wo.getChargeInstance().setTerminationDate(DateUtils.parseDateWithPattern("2020-01-10", DateUtils.DATE_PATTERN));
        ((RecurringChargeInstance) wo.getChargeInstance()).setChargeToDateOnTermination(wo.getChargeInstance().getTerminationDate());

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));
        // WO at 10% - only to termination date
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(50d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2020-01-10");
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getEndDate()).isEqualTo("2020-01-15");
        assertThat(wo2.getCode()).isEqualTo("one");
        assertThat(wo2.getDescription()).isEqualTo("first surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameter_and_1config_terminatedStartPeriod_Reimbursement() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.REIMBURSMENT, "2020-01-11", "2020-01-11", "2020-01-15", 20d);
        wo.getChargeInstance().setStatus(InstanceStatusEnum.TERMINATED);
        wo.getChargeInstance().setTerminationDate(DateUtils.parseDateWithPattern("2020-01-11", DateUtils.DATE_PATTERN));
        ((RecurringChargeInstance) wo.getChargeInstance()).setChargeToDateOnTermination(wo.getChargeInstance().getTerminationDate());

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));
        // WO at 10% - only to termination date
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(50d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getEndDate()).isEqualTo("2020-01-15");
        assertThat(wo2.getCode()).isEqualTo("one");
        assertThat(wo2.getDescription()).isEqualTo("first surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameter_and_1config_terminatedMidPeriod_Reimbursement() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.REIMBURSMENT, "2020-01-11", "2020-01-11", "2020-01-15", 20d);
        wo.getChargeInstance().setStatus(InstanceStatusEnum.TERMINATED);
        wo.getChargeInstance().setTerminationDate(DateUtils.parseDateWithPattern("2020-01-13", DateUtils.DATE_PATTERN));
        ((RecurringChargeInstance) wo.getChargeInstance()).setChargeToDateOnTermination(wo.getChargeInstance().getTerminationDate());

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));
        // WO at 10% - only to termination date
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(50d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getEndDate()).isEqualTo("2020-01-15");
        assertThat(wo2.getCode()).isEqualTo("one");
        assertThat(wo2.getDescription()).isEqualTo("first surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameter_and_1config_terminatedFutureDate() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-11", "2020-01-11", "2020-01-15", 20d);
        wo.getChargeInstance().setStatus(InstanceStatusEnum.TERMINATED);
        wo.getChargeInstance().setTerminationDate(DateUtils.parseDateWithPattern("2020-01-25", DateUtils.DATE_PATTERN));
        ((RecurringChargeInstance) wo.getChargeInstance()).setChargeToDateOnTermination(wo.getChargeInstance().getTerminationDate());

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));
        // WO at 10% - only to termination date
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(50d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getEndDate()).isEqualTo("2020-01-15");
        assertThat(wo2.getCode()).isEqualTo("one");
        assertThat(wo2.getDescription()).isEqualTo("first surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_2SlaParameter_and_1config() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-05", "2020-01-05", "2020-01-15", 20d);

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        // HERE test should return 2 sla configs - matching SLA code c (before 01-10) and d (after 01-10)
        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        // WO 5 days at 10% and 5 days at 10%
        verify(walletOperationService, times(2)).create(argument.capture());

        WalletOperation wo2 = argument.getAllValues().get(0);
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));
        // WO 5 days at 10%
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(25).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2020-01-05");
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-05");
        assertThat(wo2.getEndDate()).isEqualTo("2020-01-10");
        assertThat(wo2.getCode()).isEqualTo("one");
        assertThat(wo2.getDescription()).isEqualTo("first surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

        WalletOperation wo3 = argument.getAllValues().get(1);
        assertThat(wo3.getQuantity()).isEqualTo(new BigDecimal(1));
        // WO 5 days at 10%
        assertThat(wo3.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(25).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo3.getOperationDate()).isEqualTo("2020-01-10");
        assertThat(wo3.getStartDate()).isEqualTo("2020-01-10");
        assertThat(wo3.getEndDate()).isEqualTo("2020-01-15");
        assertThat(wo3.getCode()).isEqualTo("one");
        assertThat(wo3.getDescription()).isEqualTo("first surcharge");
        assertThat(wo3.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo3.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo3.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo3.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo3.getTax().getId()).isEqualTo(4L);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_5SlaParameter_with_gap_and_1config() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-01", "2020-01-01", "2020-04-01", 20d);

        wo.setAmountWithoutTax(new BigDecimal(500));

        // Make a gap
        wo.getServiceInstance().setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-02-10", "2020-02-25"), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "15|a|b||||" }));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        // HERE test should return 2 sla configs - matching SLA code g (before 03-25) and h (after 05-01)
        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-05-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);

        // WO 9 days at 10% and 31 days at 10% with a 15 day gap in between without SLA plus 29 days at 10% (total 91 days)
        verify(walletOperationService, times(3)).create(argument.capture());

        WalletOperation wo2 = argument.getAllValues().get(0);
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));
        // WO 9 days at 10% (total 91 days)
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(4.95d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2020-01-01");
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-01");
        assertThat(wo2.getEndDate()).isEqualTo("2020-01-10");
        assertThat(wo2.getCode()).isEqualTo("one");
        assertThat(wo2.getDescription()).isEqualTo("first surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

        WalletOperation wo3 = argument.getAllValues().get(1);
        assertThat(wo3.getQuantity()).isEqualTo(new BigDecimal(1));
        // WO 31 days at 10% (total 91 days)
        assertThat(wo3.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(17.03d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo3.getOperationDate()).isEqualTo("2020-01-10");
        assertThat(wo3.getStartDate()).isEqualTo("2020-01-10");
        assertThat(wo3.getEndDate()).isEqualTo("2020-02-10");
        assertThat(wo3.getCode()).isEqualTo("one");
        assertThat(wo3.getDescription()).isEqualTo("first surcharge");
        assertThat(wo3.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo3.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo3.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo3.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo3.getTax().getId()).isEqualTo(4L);

        WalletOperation wo4 = argument.getAllValues().get(2);
        assertThat(wo4.getQuantity()).isEqualTo(new BigDecimal(1));
        // WO 29 days at 10% (total 91 days)
        assertThat(wo4.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(15.93d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo4.getOperationDate()).isEqualTo("2020-02-25");
        assertThat(wo4.getStartDate()).isEqualTo("2020-02-25");
        assertThat(wo4.getEndDate()).isEqualTo("2020-03-25");
        assertThat(wo4.getCode()).isEqualTo("one");
        assertThat(wo4.getDescription()).isEqualTo("first surcharge");
        assertThat(wo4.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo4.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo4.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo4.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo4.getTax().getId()).isEqualTo(4L);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_2wo_over_1slaParameter_and_1config() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-11", "2020-01-11", "2020-01-15", 20d);

        wo.setAmountWithoutTax(new BigDecimal(500));
        wos.add(wo);

        // Operation date and quantity should be irrelevant
        wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-11", "2020-01-15", "2020-01-20", 15d);

        wo.setAmountWithoutTax(new BigDecimal(300));
        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(80d).setScale(2, RoundingMode.HALF_UP)); // Both WO at 10%
        assertThat(wo2.getOperationDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getEndDate()).isEqualTo("2020-01-20");
        assertThat(wo2.getCode()).isEqualTo("one");
        assertThat(wo2.getDescription()).isEqualTo("first surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameter_and_2configs() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-11", "2020-01-11", "2020-01-15", 20d);

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "two", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-13", DateUtils.DATE_PATTERN) }));
        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "two", "description", "second surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(1d), "accounting_code", 8L, "invoice_sub_cat", 2L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-13", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));
        // WO with 2 days at 10% and 2 days at 1%
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(27.5d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getEndDate()).isEqualTo("2020-01-15");
        assertThat(wo2.getCode()).isEqualTo("two");
        assertThat(wo2.getDescription()).isEqualTo("second surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(8L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(2L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameter_and_3configs_only2ConfigsApply() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-11", "2020-01-11", "2020-01-15", 20d);

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "two", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2021-01-13", DateUtils.DATE_PATTERN) }));
        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "two", "description", "second surcharge", OneShotAndRecurringRatingScript.SLA_SERVICE_ID, 15L, OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(1d), "accounting_code", 8L,
                    "invoice_sub_cat", 2L, OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-13", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));
        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "two", "description", "third surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 8L, "invoice_sub_cat", 2L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-13", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));
        // WO with 2 days at 10% and 2 days at 1%
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(27.5d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getStartDate()).isEqualTo("2020-01-11");
        assertThat(wo2.getEndDate()).isEqualTo("2020-01-15");
        assertThat(wo2.getCode()).isEqualTo("two");
        assertThat(wo2.getDescription()).isEqualTo("second surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(8L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(2L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameter_and_2configs_reimbursement() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.REIMBURSMENT, "2020-04-11", "2020-04-11", "2020-05-15", 20d);

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-04-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-04-23", DateUtils.DATE_PATTERN) }));
        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "two", "description", "second surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(1d), "accounting_code", 8L, "invoice_sub_cat", 2L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-04-23", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-08-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));
        // WO with 2 days at 10% and 2 days at 1%
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(20.88d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2020-04-11");
        assertThat(wo2.getStartDate()).isEqualTo("2020-04-11");
        assertThat(wo2.getEndDate()).isEqualTo("2020-05-15");
        assertThat(wo2.getCode()).isEqualTo("two");
        assertThat(wo2.getDescription()).isEqualTo("second surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(8L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(2L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameters_and_1config_serviceParamStartsLater() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2019-12-01", "2019-12-01", "2019-12-29", 20d);

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2019-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));

        // Service parameter is "2019-12-15", "2020-01-10". WO 14 days at nothing and 14 days at 10%
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(25d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2019-12-15");
        assertThat(wo2.getStartDate()).isEqualTo("2019-12-15");
        assertThat(wo2.getEndDate()).isEqualTo("2019-12-29");
        assertThat(wo2.getCode()).isEqualTo("one");
        assertThat(wo2.getDescription()).isEqualTo("first surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameters_and_1config_serviceParamStartsLater_and_SLAConfigsStartsEvenLater() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2019-12-01", "2019-12-01", "2019-12-29", 20d);

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2019-12-17", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));

        // Service parameter is "2019-12-15", "2020-01-10", BUT SLA config starts at 2019-12-17 WO 16 days at nothing and 12 days at 10%
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(21.43d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2019-12-15");
        assertThat(wo2.getStartDate()).isEqualTo("2019-12-15");
        assertThat(wo2.getEndDate()).isEqualTo("2019-12-29");
        assertThat(wo2.getCode()).isEqualTo("one");
        assertThat(wo2.getDescription()).isEqualTo("first surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameters_and_1config_serviceParamEndsEarlier() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-03-20", "2020-03-20", "2020-03-30", 20d);

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2019-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-04-30", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));
        // Service parameters are "2020-02-25", "2020-03-25". WO 5 days at 10% and 5 days at nothing
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(25d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2020-03-20");
        assertThat(wo2.getStartDate()).isEqualTo("2020-03-20");
        assertThat(wo2.getEndDate()).isEqualTo("2020-03-25");
        assertThat(wo2.getCode()).isEqualTo("one");
        assertThat(wo2.getDescription()).isEqualTo("first surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameters_and_1config_serviceParamEndsEarlier_and_SLAConfigEndEvenEarlier() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-03-20", "2020-03-20", "2020-03-30", 20d);

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2019-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-03-22", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));
        // Service parameters are "2020-02-25", "2020-03-25", but SLA config ends on 2020-03-22. WO 2 days at 10% and 8 days at nothing
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(10d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2020-03-20");
        assertThat(wo2.getStartDate()).isEqualTo("2020-03-20");
        assertThat(wo2.getEndDate()).isEqualTo("2020-03-25");
        assertThat(wo2.getCode()).isEqualTo("one");
        assertThat(wo2.getDescription()).isEqualTo("first surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applySLASurcharge_1wo_over_1slaParameters_and_1config_serviceParamStartsLater_and_SLAConfigsStartsAndEndsInBetween() {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();

        // Operation date and quantity should be irrelevant
        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2019-12-01", "2019-12-01", "2019-12-29", 20d);

        wo.setAmountWithoutTax(new BigDecimal(500));

        wos.add(wo);

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "one", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2019-12-17", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2019-12-25", DateUtils.DATE_PATTERN) }));

        when(customTableService.list(eq("eir_or_sla"), any())).thenReturn(slaConfigs);

        ratingScript.applySLASurcharge(wos, true);

        ArgumentCaptor<WalletOperation> argument = ArgumentCaptor.forClass(WalletOperation.class);
        verify(walletOperationService, times(1)).create(argument.capture());

        WalletOperation wo2 = argument.getValue();
        assertThat(wo2.getQuantity()).isEqualTo(new BigDecimal(1));

        // Service parameter is "2019-12-15", "2020-01-10", BUT SLA config starts at 2019-12-17 and ends on 2019-12-25. WO 20 days at nothing and 8 days at 10%
        assertThat(wo2.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(14.29d).setScale(2, RoundingMode.HALF_UP));
        assertThat(wo2.getOperationDate()).isEqualTo("2019-12-15");
        assertThat(wo2.getStartDate()).isEqualTo("2019-12-15");
        assertThat(wo2.getEndDate()).isEqualTo("2019-12-29");
        assertThat(wo2.getCode()).isEqualTo("one");
        assertThat(wo2.getDescription()).isEqualTo("first surcharge");
        assertThat(wo2.getTaxClass().getId()).isEqualTo(5L);
        assertThat(wo2.getAccountingCode().getId()).isEqualTo(7L);
        assertThat(wo2.getInvoiceSubCategory().getId()).isEqualTo(9L);
        assertThat(wo2.getTaxPercent()).isEqualTo(new BigDecimal(15));
        assertThat(wo2.getTax().getId()).isEqualTo(4L);

    }

    private List<Map<String, Object>> constructTariffs(String offerType, Object[][] tariffs) {

        List<Map<String, Object>> tariffList = new ArrayList<Map<String, Object>>(tariffs.length);

        for (Object[] tariff : tariffs) {

            Map<String, Object> tariffRecord = new HashMap<String, Object>();

            tariffRecord.put(ValidTariffCustomTableEnum.ID.getLabel(), new BigInteger("5"));
            tariffRecord.put(ValidTariffCustomTableEnum.VALID_FROM.getLabel(), DateUtils.parseDateWithPattern((String) tariff[0], DateUtils.DATE_PATTERN));
            tariffRecord.put(ValidTariffCustomTableEnum.VALID_TO.getLabel(), DateUtils.parseDateWithPattern((String) tariff[1], DateUtils.DATE_PATTERN));
            tariffRecord.put(ValidTariffCustomTableEnum.CHARGE_TYPE.getLabel(), tariff[2]);
            tariffRecord.put(ValidTariffCustomTableEnum.PRICE.getLabel(), new BigDecimal((double) tariff[3]));
            tariffRecord.put(ValidTariffCustomTableEnum.DESCRIPTION.getLabel(), tariff[4]);
            tariffRecord.put(ValidTariffCustomTableEnum.TAX_CLASS.getLabel(), new BigInteger(((Long) tariff[5]).toString()));
            tariffRecord.put(ValidTariffCustomTableEnum.INVOICE_SUBCAT_ID.getLabel(), new BigInteger(((Long) tariff[6]).toString()));
            tariffRecord.put(ValidTariffCustomTableEnum.ACCOUNTING_CODE_ID.getLabel(), new BigInteger(((Long) tariff[7]).toString()));
            tariffRecord.put(ValidTariffCustomTableEnum.DISTANCE_FROM.getLabel(), tariff[8] != null ? new BigInteger(((Integer) tariff[8]).toString()) : null);
            tariffRecord.put(ValidTariffCustomTableEnum.DISTANCE_TO.getLabel(), tariff[9] != null ? new BigInteger(((Integer) tariff[9]).toString()) : null);
            tariffRecord.put(ValidTariffCustomTableEnum.DISTANCE_UNIT.getLabel(), tariff[10] != null ? new BigInteger(((Integer) tariff[10]).toString()) : null);
            tariffRecord.put(ValidTariffCustomTableEnum.QUANTITY_FROM.getLabel(), tariff[11] != null ? new BigInteger(((Integer) tariff[11]).toString()) : null);
            tariffRecord.put(ValidTariffCustomTableEnum.QUANTITY_TO.getLabel(), tariff[12] != null ? new BigInteger(((Integer) tariff[12]).toString()) : null);
            tariffRecord.put(ValidTariffCustomTableEnum.MAX_RATED_QUANTITY.getLabel(), tariff[13] != null ? new BigInteger(((Integer) tariff[13]).toString()) : null);
            tariffRecord.put(ValidTariffCustomTableEnum.REC_CALENDAR.getLabel(), tariff[14] != null ? new BigInteger(((Long) tariff[14]).toString()) : null);
            tariffRecord.put(ValidTariffCustomTableEnum.PRIORITY.getLabel(), tariff[15] != null ? tariff[15] : null);
            tariffRecord.put(ValidTariffCustomTableEnum.TARIFF_PLAN_ID.getLabel(), new BigInteger("5"));

//            // Add offer type specific fields
//            String[] fields = OneShotAndRecurringRatingScript.getTariffSearchFields(offerType, null);
//
//            for (int i = 0; (i < tariff.length - 16) && i < fields.length; i++) {
//                tariffRecord.put(fields[i], tariff[14 + i]);
//            }

            tariffList.add(tariffRecord);

        }

        return tariffList;
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_normalizeSLAConfig_largeSpecific_shortGeneric_config() {

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "two", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_SERVICE_ID, 15L, OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L,
                    "invoice_sub_cat", 9L, OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2021-01-13", DateUtils.DATE_PATTERN) }));
        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "two", "description", "second surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(1d), "accounting_code", 8L, "invoice_sub_cat", 2L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-13", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-03-30", DateUtils.DATE_PATTERN) }));

        List<Map<String, Object>> slaConfigsNormalized = ratingScript.normalizeSLAConfig(slaConfigs, getPeriod("2020-01-01", "2020-02-25"));

        assertThat(slaConfigsNormalized.size()).isEqualTo(1);

        Map<String, Object> slaConfig = slaConfigsNormalized.get(0);
        assertThat(slaConfig.get("description")).isEqualTo("first surcharge");
        assertThat((Date) slaConfig.get(OneShotAndRecurringRatingScript.SLA_VALID_FROM)).isEqualTo("2020-01-01");
        assertThat((Date) slaConfig.get(OneShotAndRecurringRatingScript.SLA_VALID_TO)).isEqualTo("2021-01-13");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_normalizeSLAConfig_largeGeneric_shortSpecific_config() {

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "two", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2021-01-13", DateUtils.DATE_PATTERN) }));
        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "two", "description", "second surcharge", OneShotAndRecurringRatingScript.SLA_SERVICE_ID, 15L, OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(1d), "accounting_code", 8L,
                    "invoice_sub_cat", 2L, OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-13", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-03-30", DateUtils.DATE_PATTERN) }));

        List<Map<String, Object>> slaConfigsNormalized = ratingScript.normalizeSLAConfig(slaConfigs, getPeriod("2020-01-01", "2020-02-25"));

        assertThat(slaConfigsNormalized.size()).isEqualTo(2);

        Map<String, Object> slaConfig = slaConfigsNormalized.get(0);
        assertThat(slaConfig.get("description")).isEqualTo("first surcharge");
        assertThat((Date) slaConfig.get(OneShotAndRecurringRatingScript.SLA_VALID_FROM)).isEqualTo("2020-01-01");
        assertThat((Date) slaConfig.get(OneShotAndRecurringRatingScript.SLA_VALID_TO)).isEqualTo("2020-01-13");

        slaConfig = slaConfigsNormalized.get(1);
        assertThat(slaConfig.get("description")).isEqualTo("second surcharge");
        assertThat((Date) slaConfig.get(OneShotAndRecurringRatingScript.SLA_VALID_FROM)).isEqualTo("2020-01-13");
        assertThat((Date) slaConfig.get(OneShotAndRecurringRatingScript.SLA_VALID_TO)).isEqualTo("2020-03-30");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_normalizeSLAConfig_3configs() {

        List<Map<String, Object>> slaConfigs = new ArrayList<>();

        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "two", "description", "first surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(10d), "accounting_code", 7L, "invoice_sub_cat", 9L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2021-01-13", DateUtils.DATE_PATTERN) }));
        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "two", "description", "second surcharge", OneShotAndRecurringRatingScript.SLA_SERVICE_ID, 15L, OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(1d), "accounting_code", 8L,
                    "invoice_sub_cat", 2L, OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-13", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-03-30", DateUtils.DATE_PATTERN) }));
        slaConfigs.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "two", "description", "third surcharge", OneShotAndRecurringRatingScript.SLA_PERCENT, new BigDecimal(1d), "accounting_code", 8L, "invoice_sub_cat", 2L,
                    OneShotAndRecurringRatingScript.SLA_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-13", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.SLA_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-03-30", DateUtils.DATE_PATTERN) }));

        List<Map<String, Object>> slaConfigsNormalized = ratingScript.normalizeSLAConfig(slaConfigs, getPeriod("2020-01-01", "2020-02-25"));

        assertThat(slaConfigsNormalized.size()).isEqualTo(2);

        Map<String, Object> slaConfig = slaConfigsNormalized.get(0);
        assertThat(slaConfig.get("description")).isEqualTo("first surcharge");
        assertThat((Date) slaConfig.get(OneShotAndRecurringRatingScript.SLA_VALID_FROM)).isEqualTo("2020-01-01");
        assertThat((Date) slaConfig.get(OneShotAndRecurringRatingScript.SLA_VALID_TO)).isEqualTo("2020-01-13");

        slaConfig = slaConfigsNormalized.get(1);
        assertThat(slaConfig.get("description")).isEqualTo("second surcharge");
        assertThat((Date) slaConfig.get(OneShotAndRecurringRatingScript.SLA_VALID_FROM)).isEqualTo("2020-01-13");
        assertThat((Date) slaConfig.get(OneShotAndRecurringRatingScript.SLA_VALID_TO)).isEqualTo("2020-03-30");
    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applyFlatDiscount_many_discounts() {

        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2025-01-13", "2019-12-01", "2019-12-29", 20d);

        wo.setUnitAmountWithoutTax(new BigDecimal(500));
        wo.setUnitAmountWithTax(new BigDecimal(600));

        List<Map<String, Object>> flatDiscounts = new ArrayList<>();
        flatDiscounts.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "1", "customer_account", "355", "billing_account", "442", "charge_type", "R", OneShotAndRecurringRatingScript.DISCOUNT_PERCENT, new BigDecimal(10d), "product_set", "", "migration_code",
                    "DSL", OneShotAndRecurringRatingScript.DISCOUNT_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.DISCOUNT_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));
        flatDiscounts.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "2", "customer_account", "355", "billing_account", "", "charge_type", "R", OneShotAndRecurringRatingScript.DISCOUNT_PERCENT, new BigDecimal(5d), "product_set", "7", "migration_code",
                    "DSL", OneShotAndRecurringRatingScript.DISCOUNT_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.DISCOUNT_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));

        ratingScript.applyMatchingDiscounts(wo, flatDiscounts);

        assertThat(wo.getUnitAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(427.5d).setScale(2, RoundingMode.HALF_UP));

    }

    @Test
    public void test_applyFlatDiscount_no_discounts() {

        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-13", "2019-12-01", "2019-12-29", 20d);

        wo.setAmountWithoutTax(new BigDecimal(500));
        wo.setAmountWithTax(new BigDecimal(600));

        List<Map<String, Object>> flatDiscounts = new ArrayList<>();

        ratingScript.applyMatchingDiscounts(wo, flatDiscounts);

        assertThat(wo.getAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(500d).setScale(2, RoundingMode.HALF_UP));

    }

    @SuppressWarnings("unchecked")
    @Test
    public void test_applyFlatDiscount_inferior_egal_zero_discounts() {

        WalletOperation wo = createWO("recurring", TransformationRulesEnum.DATA.name(), true, true, true, ChargeApplicationModeEnum.SUBSCRIPTION, "2020-01-13", "2019-12-01", "2019-12-29", 20d);

        wo.setAmountWithoutTax(new BigDecimal(500));
        wo.setAmountWithTax(new BigDecimal(600));

        List<Map<String, Object>> flatDiscounts = new ArrayList<>();
        flatDiscounts.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "1", "customer_account", "355", "billing_account", "442", "charge_type", "R", OneShotAndRecurringRatingScript.DISCOUNT_PERCENT, new BigDecimal(0d), "product_set", "", "migration_code",
                    "DSL", OneShotAndRecurringRatingScript.DISCOUNT_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.DISCOUNT_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));
        flatDiscounts.add(MapUtils.putAll(new HashMap<String, Object>(),
            new Object[] { "code", "2", "customer_account", "355", "billing_account", "", "charge_type", "R", OneShotAndRecurringRatingScript.DISCOUNT_PERCENT, new BigDecimal(-2d), "product_set", "7", "migration_code",
                    "DSL", OneShotAndRecurringRatingScript.DISCOUNT_VALID_FROM, DateUtils.parseDateWithPattern("2020-01-01", DateUtils.DATE_PATTERN), OneShotAndRecurringRatingScript.DISCOUNT_VALID_TO,
                    DateUtils.parseDateWithPattern("2020-01-30", DateUtils.DATE_PATTERN) }));

        ratingScript.applyMatchingDiscounts(wo, flatDiscounts);

        assertThat(wo.getAmountWithoutTax().setScale(2, RoundingMode.HALF_UP)).isEqualTo(new BigDecimal(500d).setScale(2, RoundingMode.HALF_UP));

    }

    /**
     * Create wallet operation
     *
     * @param chargeType Charge type: subscription, recurring, other
     * @param offerType Offer type: Data, Misc
     * @param servicePresent Is CF value present on service instance (true) or a charge instance (false)
     * @param setExtraInfo True to set tax class, tax and tax percent values
     * @param addSpaceInParameters True to add a break in service parameters. Used to test functionality when last service parameter are end dated. Applies to non-reimbursement charges.
     * @param chargeMode Charge mode
     * @param operationDate Operation date
     * @param startDate Start date field value
     * @param endDate End date field value
     * @param quantity Quantity field value
     * @return A Wallet operation instance
     */
    private WalletOperation createWO(String chargeType, String offerType, boolean servicePresent, boolean setExtraInfo, boolean addSpaceInParameters, ChargeApplicationModeEnum chargeMode, String operationDate,
            String startDate, String endDate, double quantity) {
        WalletOperation wo = new WalletOperation();

        Customer customer = new Customer();
        CustomerAccount ca = new CustomerAccount();
        ca.setCustomer(customer);

        BillingAccount ba = new BillingAccount();
        ba.setCustomerAccount(ca);

        UserAccount ua = new UserAccount();
        ua.setBillingAccount(ba);

        WalletInstance wallet = new WalletInstance();
        wallet.setUserAccount(ua);

        wo.setWallet(wallet);
        wo.setChargeMode(chargeMode);
        wo.setSeller(new Seller());
        wo.setUserAccount(ua);
        wo.setBillingAccount(ba);

        if (operationDate != null) {
            wo.setOperationDate(DateUtils.parseDateWithPattern(operationDate, DateUtils.DATE_PATTERN));
        }
        if (startDate != null) {
            wo.setStartDate(DateUtils.parseDateWithPattern(startDate, DateUtils.DATE_PATTERN));
        }
        if (endDate != null) {
            wo.setEndDate(DateUtils.parseDateWithPattern(endDate, DateUtils.DATE_PATTERN));
        }
        wo.setQuantity(BigDecimal.valueOf(quantity));

        ChargeInstance chargeInstance = null;
        if (chargeType.equals("subscription")) {
            chargeInstance = new SubscriptionChargeInstance();
            OneShotChargeTemplate chargeTemplate = new OneShotChargeTemplate();
            chargeTemplate.setOneShotChargeTemplateType(OneShotChargeTemplateTypeEnum.SUBSCRIPTION);
            chargeInstance.setChargeTemplate(chargeTemplate);

        } else if (chargeType.equals("termination")) {
            chargeInstance = new TerminationChargeInstance();
            OneShotChargeTemplate chargeTemplate = new OneShotChargeTemplate();
            chargeTemplate.setOneShotChargeTemplateType(OneShotChargeTemplateTypeEnum.TERMINATION);
            chargeInstance.setChargeTemplate(chargeTemplate);

        } else if (chargeType.equals("other")) {
            chargeInstance = new OneShotChargeInstance();
            OneShotChargeTemplate chargeTemplate = new OneShotChargeTemplate();
            chargeTemplate.setOneShotChargeTemplateType(OneShotChargeTemplateTypeEnum.OTHER);
            chargeInstance.setChargeTemplate(chargeTemplate);

        } else if (chargeType.equals("recurring")) {
            chargeInstance = new RecurringChargeInstance();
            RecurringChargeTemplate chargeTemplate = new RecurringChargeTemplate();
            chargeInstance.setChargeTemplate(chargeTemplate);

            ((RecurringChargeInstance) chargeInstance).setCalendar(calendarService.findById(1L));
        }

        wo.setChargeInstance(chargeInstance);

        OfferTemplateCategory offerCategory = new OfferTemplateCategory();
        offerCategory.setCfValue("offerType", offerType);

        OfferTemplate offer = mock(OfferTemplate.class);
        when(offer.getOfferTemplateCategories()).thenReturn(Arrays.asList(offerCategory));

        ServiceTemplate serviceTemplate = new ServiceTemplate();
        serviceTemplate.setId(5L);
        ServiceInstance serviceInstance = new ServiceInstance();
        serviceInstance.setServiceTemplate(serviceTemplate);

        if (servicePresent) {
            serviceInstance.setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2019-12-15", "2020-01-10"), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|c|||" }));
            serviceInstance.setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-10", "2020-01-20"), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|d|||" }));
            serviceInstance.setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-01-20", "2020-02-10"), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "15|a|b|d|||" }));
            serviceInstance.setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-02-10", "2020-02-25"), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "15|a|b|f|||" }));
            serviceInstance.setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-02-25", "2020-03-25"), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "16|a|b|g|||" }));

            // In case of reimbursement, the last service parameter indicates termination parameters and has start and end date match
            if (chargeMode == ChargeApplicationModeEnum.REIMBURSMENT) {
                serviceInstance.setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-03-25", "2020-05-01"), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "16|a|b|e|||" }));
                serviceInstance.setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-05-01", "2020-05-01"), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "16|a|b|l|||" }));

                // The period 03/25-05/01 is explicitly missing for non-reimbursement charges to test functionality when last service parameter are end dated.
            } else if (addSpaceInParameters) {
                serviceInstance.setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-05-01", null), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "16|a|b|h|||" }));

                // At termination charge processing service will already be terminated
            } else if (chargeType.equals("termination")) {
                serviceInstance.setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-03-25", "2020-03-25"), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "16|a|b|e|||" }));

            } else {
                serviceInstance.setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), getPeriod("2020-03-25", null), 1, MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "16|a|b|h|||" }));
            }

        } else {
            chargeInstance.setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), MapUtils.putAll(new HashMap<String, String>(), new String[] { "1", "5|a|b|d|||" }));
        }

        if (servicePresent) {
            wo.setServiceInstance(serviceInstance);
            wo.getChargeInstance().setServiceInstance(serviceInstance);
        }

        wo.setOfferTemplate(offer);

        if (setExtraInfo) {
            TaxClass taxClass = new TaxClass();
            taxClass.setId(5L);
            wo.setTaxClass(taxClass);

            Tax tax = new Tax();
            tax.setId(4L);
            tax.setPercent(new BigDecimal(15));
            wo.setTax(tax);
            wo.setTaxPercent(new BigDecimal(15));
        }

        return wo;
    }

    private DatePeriod getPeriod(String date1, String date2) {
        return new DatePeriod(date1 != null ? DateUtils.parseDateWithPattern(date1, DateUtils.DATE_PATTERN) : null, date2 != null ? DateUtils.parseDateWithPattern(date2, DateUtils.DATE_PATTERN) : null);
    }

}
