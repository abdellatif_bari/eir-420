package com.eir.script;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.meveo.admin.exception.BusinessException;
import org.meveo.api.commons.ErrorEnum;
import org.meveo.api.dto.subscriber.ServiceChargesRequestDto;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.Subscription;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.catalog.impl.ServiceTemplateService;
import org.meveo.service.custom.CustomTableService;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

import com.eir.service.subscriber.SubscriberApiService;

@RunWith(MockitoJUnitRunner.class)
public class SubscriberApiServiceTest {

    @Spy
    @InjectMocks
    private SubscriberApiService subscriberApiService;

    @Mock
    private CustomTableService customTableService;

    @Mock
    private BillingAccountService billingAccountService;

    @Mock
    private SubscriptionService subscriptionService;

    @Mock
    private ServiceTemplateService serviceTemplateService;
    @Before
    public void setUp() {   
        BillingAccount billingAccount = new BillingAccount();
        billingAccount.setCode("TEST_BA");
        ServiceTemplate serviceTemplate = new ServiceTemplate();
        serviceTemplate.setCode("TEST_ST");
        Subscription subscription1 = new Subscription();
        subscription1.setCode("TEST_SUB1");

        List<Subscription> subscriptions = Arrays.asList(subscription1);
        when(billingAccountService.findByCode(any())).thenReturn(billingAccount);
        when(serviceTemplateService.findByCode(any())).thenReturn(serviceTemplate);
        when(subscriptionService.findByCodeLike(any())).thenReturn(subscriptions);
    }
    
    @Test
    public void validateFields_WhenOrderingSystemKO_ShouldThrowException() {
        ServiceChargesRequestDto serviceChargesRequestDto = new ServiceChargesRequestDto();
        serviceChargesRequestDto.setAccountNumber("203215422");
        serviceChargesRequestDto.setAction("C");
        serviceChargesRequestDto.setBillCode("ADSLMVP");
        serviceChargesRequestDto.setChargeQueryId("1234");
        serviceChargesRequestDto.setOrderingSystem("UGA");
        serviceChargesRequestDto.setOrderId("21700811");
        serviceChargesRequestDto.setSubscriberId("01-5433211");
        serviceChargesRequestDto.setTransactionId("1345435345");
        assertThatExceptionOfType(BusinessException.class).isThrownBy(() -> {subscriberApiService.validateFields(serviceChargesRequestDto);});
        assertThat(serviceChargesRequestDto.getErrorCode()).isEqualTo(ErrorEnum.BAD_REQUEST_INVALID_VALUE);
    }
    
    @Test
    public void validateFields_WhenActionKO_ShouldThrowException() {
        ServiceChargesRequestDto serviceChargesRequestDto = new ServiceChargesRequestDto();
        serviceChargesRequestDto.setAccountNumber("203215422");
        serviceChargesRequestDto.setAction("CA");
        serviceChargesRequestDto.setBillCode("ADSLMVP");
        serviceChargesRequestDto.setChargeQueryId("1234");
        serviceChargesRequestDto.setOrderingSystem("UG");
        serviceChargesRequestDto.setOrderId("21700811");
        serviceChargesRequestDto.setSubscriberId("01-5433211");
        serviceChargesRequestDto.setTransactionId("1345435345");
        assertThatExceptionOfType(BusinessException.class).isThrownBy(() -> {subscriberApiService.validateFields(serviceChargesRequestDto);});
        assertThat(serviceChargesRequestDto.getErrorCode()).isEqualTo(ErrorEnum.BAD_REQUEST_INVALID_VALUE);
    }

    @Test
    public void validateFields_WhenBillCodeKO_ShouldThrowException() {
        when(serviceTemplateService.findByCode(any())).thenReturn(null);
        ServiceChargesRequestDto serviceChargesRequestDto = new ServiceChargesRequestDto();
        serviceChargesRequestDto.setAccountNumber("203215422");
        serviceChargesRequestDto.setAction("C");
        serviceChargesRequestDto.setBillCode("ADSLMVPA");
        serviceChargesRequestDto.setChargeQueryId("1234");
        serviceChargesRequestDto.setOrderingSystem("UG");
        serviceChargesRequestDto.setOrderId("21700811");
        serviceChargesRequestDto.setSubscriberId("01-5433211");
        serviceChargesRequestDto.setTransactionId("1345435345");
        assertThatExceptionOfType(BusinessException.class).isThrownBy(() -> {subscriberApiService.validateFields(serviceChargesRequestDto);});
        assertThat(serviceChargesRequestDto.getErrorCode()).isEqualTo(ErrorEnum.GENERIC_API_EXCEPTION);
    }
    
    @Test
    public void retrieveSubscription_WhenUGOK_ShouldReturnSuccess() {
        ServiceChargesRequestDto serviceChargesRequestDto = new ServiceChargesRequestDto();
        serviceChargesRequestDto.setAccountNumber("203215422");
        serviceChargesRequestDto.setAction("C");
        serviceChargesRequestDto.setBillCode("ADSLMVPA");
        serviceChargesRequestDto.setChargeQueryId("1234");
        serviceChargesRequestDto.setOrderingSystem("UG");
        serviceChargesRequestDto.setOrderId("21700811");
        serviceChargesRequestDto.setSubscriberId("01-5433211");
        serviceChargesRequestDto.setTransactionId("134è5435345");
        Subscription subscription = subscriberApiService.retrieveSubscription(serviceChargesRequestDto);
        assertThat(subscription.getCode()).isEqualTo("TEST_SUB1");
    }

}
