package org.meveo.api.rest.eir.tax;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.meveo.api.dto.response.PagingAndFiltering;
import org.meveo.api.rest.IBaseRs;

/**
 * REST Web service interface for tax mapping.
 *
 * @author Hatim OUDAD
 */
@Path("/tax")
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface TaxRs extends IBaseRs {

    /**
     * Activate tax mapping in a staging table
     *
     * @param filename The name of a tax mapping import file
     * @return Action response
     */
    @POST
    @Path("/taxMapping/activate/{fileId}")
    Response activateTaxMapping(@PathParam("fileId") Long fileId);

    /**
     * Cancel tax mapping in a staging table
     *
     * @param filename The name of a tax mapping import file
     * @return Action response
     */
    @POST
    @Path("/taxMapping/cancel/{fileId}")
    Response cancelTaxMapping(@PathParam("fileId") Long fileId);

    /**
     * Export tax mapping
     *
     * @param pagingAndFiltering Search and paging configuration
     * @return Action response
     */
    @POST
    @Path("/taxMapping/export")
    Response exportTaxMapping(PagingAndFiltering pagingAndFiltering);
    
    /**
     * 
     * @param fileId
     * @return
     */
    @POST
    @Path("/taxMapping/validate/{fileId}")
	Response validateTaxMapping(@PathParam("fileId") Long fileId);

}