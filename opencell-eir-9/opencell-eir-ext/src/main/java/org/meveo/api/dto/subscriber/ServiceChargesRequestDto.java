package org.meveo.api.dto.subscriber;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.meveo.api.dto.BaseDto;

/**
 * Service charges Request DTO
 *
 * @author Mohammed Amine TAZI
 */
@XmlRootElement(name = "OC_Charge_Query_Request")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceChargesRequestDto extends BaseDto {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -8202679774333065304L;

    /**
     * Unique for charge request order in the custom table  
     */
    @XmlElement(name = "Transaction_Id", required = true)
    private String transactionId;
    
    /**
     * Identifier for upstream ordering system
     */
    @XmlElement(name = "Ordering_System", required = true)
    private String orderingSystem;

    /**
     * charge Order identifier
     */
    @XmlElement(name = "Charge_Query_Id", required = true)
    private String chargeQueryId;

    /**
     * Ordering System Identifier for a related service Order
     */
    @XmlElement(name = "Order_Id", required = true)
    private String orderId;
    
    /**
     * Required Financial Query Action
     */
    @XmlElement(name = "Action", required = true)
    private String action;
    
    /**
     * Billing account number
     */
    @XmlElement(name = "Account_Number", required = true)
    private String accountNumber;
    
    /**
     * Circuit Id, or concatenation of STD + Tel_Num    
     */
    @XmlElement(name = "Subscriber_Id", required = true)
    private String subscriberId;
    
    /**
     * Service charge item being queried.    
     */
    @XmlElement(name = "Bill_Code", required = true)
    private String billCode;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOrderingSystem() {
        return orderingSystem;
    }

    public void setOrderingSystem(String orderingSystem) {
        this.orderingSystem = orderingSystem;
    }

    public String getChargeQueryId() {
        return chargeQueryId;
    }

    public void setChargeQueryId(String chargeQueryId) {
        this.chargeQueryId = chargeQueryId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public String getSubscriberId() {
        return subscriberId;
    }

    public void setSubscriberId(String subscriberId) {
        this.subscriberId = subscriberId;
    }

    public String getBillCode() {
        return billCode;
    }

    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }
}
