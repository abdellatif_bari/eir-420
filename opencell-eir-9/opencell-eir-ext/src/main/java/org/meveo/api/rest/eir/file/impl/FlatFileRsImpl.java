package org.meveo.api.rest.eir.file.impl;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.core.Response;

import org.meveo.api.dto.bi.FlatFileDto;
import org.meveo.api.dto.flatfile.FlatFilesDto;
import org.meveo.api.dto.response.GenericSearchResponse;
import org.meveo.api.dto.response.PagingAndFiltering;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.eir.EirRs;
import org.meveo.api.rest.eir.file.FlatFileRs;

import com.eir.api.file.FlatFileApi;

/**
 * REST Web service implementation for managing {@link org.meveo.model.bi.FlatFile} resource.
 *
 * @author Abdellatif BARI
 */
@RequestScoped
@Interceptors({ WsRestApiInterceptor.class })
public class FlatFileRsImpl extends EirRs implements FlatFileRs {

    @Inject
    private FlatFileApi flatFileApi;

    /**
     * Reprocess a flat file order
     *
     * @param flatFilesDto the flat file Dto.
     * @return Reprocessing response
     */
    @Override
    public Response reprocess(FlatFilesDto flatFilesDto) {
        Response response = null;
        try {
            flatFileApi.reprocess(flatFilesDto);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    @Override
    public Response getFilesInStagingTable(PagingAndFiltering pagingAndFiltering) {
        Response response = null;
        try {

            List<FlatFileDto> files = flatFileApi.getFilesInStagingTable(pagingAndFiltering);

            GenericSearchResponse<FlatFileDto> searchResponse = new GenericSearchResponse<FlatFileDto>(files, pagingAndFiltering);
            response = buildSuccessResponse(searchResponse);
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }
}
