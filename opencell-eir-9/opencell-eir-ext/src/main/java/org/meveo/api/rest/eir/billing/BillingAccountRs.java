package org.meveo.api.rest.eir.billing;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.meveo.api.dto.account.BillingAccountDto;
import org.meveo.api.dto.billing.BillingAccountDebtorStatementDto;
import org.meveo.api.rest.IBaseRs;

/**
 * REST Web service interface for managing {@link org.meveo.model.billing.BillingAccount} ressource.
 *
 * @author Abdellatif BARI
 */
@Path("/billingAccount")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public interface BillingAccountRs extends IBaseRs {

    /**
     * Create a new billing account.
     *
     * @param billingAccountDto Billing account Dto
     * @return Billing account response
     */
    @POST
    @Path("/create")
    Response create(BillingAccountDto billingAccountDto);

    /**
     * Get a billing account sequence number.
     *
     * @return Billing account sequence number response.
     */
    @GET
    @Path("/sequenceNumber")
    Response getSequenceNumber();

    /**
     * List BillingAccount filter by customerAccountCode.
     *
     * @param customerAccountCode Customer account code
     * @return list of billing account
     */
    @GET
    @Path("/Billing_Account_Details/Customer_Account")
    Response listByCustomerAccount(@QueryParam("customerAccountCode") String customerAccountCode);
    
    @POST
    @Path("/exportDebtorStatement")
    Response exportDebtorStatement(BillingAccountDebtorStatementDto billingAccountDebtorStatementDto);

}