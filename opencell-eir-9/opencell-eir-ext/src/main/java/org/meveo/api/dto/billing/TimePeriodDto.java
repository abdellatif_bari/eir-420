package org.meveo.api.dto.billing;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * Time Period Data Transfer Object
 *
 * @author Mohammed Amine TAZI
 */
public class TimePeriodDto implements Serializable {

    private static final long serialVersionUID = 754167742574408331L;
    
    private Long timePeriodId;
    private String timePeriodCode;
    private Long calendarId;
    private Date timePeriodStart;
    private Date timePeriodEnd;
    private BigDecimal timePeriodAmount;
    private Long callDuration;
    public Long getTimePeriodId() {
        return timePeriodId;
    }
    public void setTimePeriodId(Long timePeriodId) {
        this.timePeriodId = timePeriodId;
    }
    
    public String getTimePeriodCode() {
        return timePeriodCode;
    }
    
    public void setTimePeriodCode(String timePeriodCode) {
        this.timePeriodCode = timePeriodCode;
    }
    
    public Long getCalendarId() {
        return calendarId;
    }
    public void setCalendarId(Long calendarId) {
        this.calendarId = calendarId;
    }
    public Date getTimePeriodStart() {
        return timePeriodStart;
    }
    public void setTimePeriodStart(Date timePeriodStart) {
        this.timePeriodStart = timePeriodStart;
    }
    public Date getTimePeriodEnd() {
        return timePeriodEnd;
    }
    public void setTimePeriodEnd(Date timePeriodEnd) {
        this.timePeriodEnd = timePeriodEnd;
    }
    public BigDecimal getTimePeriodAmount() {
        return timePeriodAmount;
    }
    public void setTimePeriodAmount(BigDecimal timePeriodAmount) {
        this.timePeriodAmount = timePeriodAmount;
    }
    public Long getCallDuration() {
        return callDuration;
    }
    public void setCallDuration(Long callDuration) {
        this.callDuration = callDuration;
    }
    
    public void calculateCallDuration() {
        callDuration = (timePeriodEnd.getTime() - timePeriodStart.getTime())/1000;
    }
}
