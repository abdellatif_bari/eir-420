package org.meveo.api.rest.eir.customer;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.meveo.api.rest.IBaseRs;

/**
 * REST Web service interface to manage the export of customer balances.
 *
 * @author Houssine ZNIBAR
 */
@Path("/customer")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public interface CustomerRs extends IBaseRs {
    
    /**
     * Export balances.
     *
     * @param customerCode the customer code
     * @return the response
     */
    @POST
    @Path("/exportBalances/{customerCode}")
    Response exportBalances(@PathParam("customerCode") String customerCode);
}