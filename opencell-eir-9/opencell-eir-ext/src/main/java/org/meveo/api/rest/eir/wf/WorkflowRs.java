package org.meveo.api.rest.eir.wf;

import org.meveo.api.dto.wf.WorkflowResponseDto;
import org.meveo.api.rest.IBaseRs;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;


/**
 * REST Web service implementation for managing {@link org.meveo.model.wf.Workflow} ressource.
 *
 * @author Abdellatif BARI
 */
@Path("/workflow")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public interface WorkflowRs extends IBaseRs {

    /**
     * Find a workflow with a given code
     *
     * @param code The workflow's code
     * @return
     */
    @GET
    @Path("/")
    WorkflowResponseDto find(@QueryParam("code") String code);
}