package org.meveo.api.dto.order;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Building Dto
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class BuildingDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7749431909837534109L;

    /**
     * The building number.
     */
    @XmlElement(name = "Building_Number")
    private String buildingNumber;

    /**
     * The from building number.
     */
    @XmlElement(name = "From_Building_Number")
    private String fromBuildingNumber;

    /**
     * The to building number.
     */
    @XmlElement(name = "To_Building_Number")
    private String toBuildingNumber;

    /**
     * The building name.
     */
    @XmlElement(name = "Building_Name")
    private String buildingName;

    /**
     * Gets the buildingNumber
     *
     * @return the buildingNumber
     */
    public String getBuildingNumber() {
        return buildingNumber;
    }

    /**
     * Sets the buildingNumber.
     *
     * @param buildingNumber the new buildingNumber
     */
    public void setBuildingNumber(String buildingNumber) {
        this.buildingNumber = buildingNumber;
    }

    /**
     * Gets the fromBuildingNumber
     *
     * @return the fromBuildingNumber
     */
    public String getFromBuildingNumber() {
        return fromBuildingNumber;
    }

    /**
     * Sets the fromBuildingNumber.
     *
     * @param fromBuildingNumber the new fromBuildingNumber
     */
    public void setFromBuildingNumber(String fromBuildingNumber) {
        this.fromBuildingNumber = fromBuildingNumber;
    }

    /**
     * Gets the toBuildingNumber
     *
     * @return the toBuildingNumber
     */
    public String getToBuildingNumber() {
        return toBuildingNumber;
    }

    /**
     * Sets the toBuildingNumber.
     *
     * @param toBuildingNumber the new toBuildingNumber
     */
    public void setToBuildingNumber(String toBuildingNumber) {
        this.toBuildingNumber = toBuildingNumber;
    }

    /**
     * Gets the buildingName
     *
     * @return the buildingName
     */
    public String getBuildingName() {
        return buildingName;
    }

    /**
     * Sets the buildingName.
     *
     * @param buildingName the new buildingName
     */
    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }
}
