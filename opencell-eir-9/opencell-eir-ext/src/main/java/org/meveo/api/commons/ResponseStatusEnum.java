package org.meveo.api.commons;

/**
 * Determine the response status of the EIR API web service.
 *
 * @author Abdellatif BARI
 */
public enum ResponseStatusEnum {
    /**
     * Request is ok. No error found.
     */
    SUCCESS,

    /**
     * Request failed. See ActionStatus.errorCode for an error code.
     */
    FAILURE
}
