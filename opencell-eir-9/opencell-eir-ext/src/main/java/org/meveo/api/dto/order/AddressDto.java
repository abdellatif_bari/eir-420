package org.meveo.api.dto.order;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Address Dto
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class AddressDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 8458141691439476869L;

    /**
     * The unit.
     */
    @XmlElement(name = "Unit")
    private UnitDto unit;

    /**
     * The building.
     */
    @XmlElement(name = "Building")
    private BuildingDto building;

    /**
     * The street.
     */
    @XmlElement(name = "Street")
    private StreetDto street;

    /**
     * The county.
     */
    @XmlElement(name = "County")
    private CountyDto county;

    /**
     * Gets the unit
     *
     * @return the unit
     */
    public UnitDto getUnit() {
        return unit;
    }

    /**
     * Sets the unit.
     *
     * @param unit the new unit
     */
    public void setUnit(UnitDto unit) {
        this.unit = unit;
    }

    /**
     * Gets the building
     *
     * @return the building
     */
    public BuildingDto getBuilding() {
        return building;
    }

    /**
     * Sets the building.
     *
     * @param building the new building
     */
    public void setBuilding(BuildingDto building) {
        this.building = building;
    }

    /**
     * Gets the street
     *
     * @return the street
     */
    public StreetDto getStreet() {
        return street;
    }

    /**
     * Sets the street.
     *
     * @param street the new street
     */
    public void setStreet(StreetDto street) {
        this.street = street;
    }

    /**
     * Gets the county
     *
     * @return the county
     */
    public CountyDto getCounty() {
        return county;
    }

    /**
     * Sets the county.
     *
     * @param county the new county
     */
    public void setCounty(CountyDto county) {
        this.county = county;
    }

}
