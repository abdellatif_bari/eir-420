package org.meveo.api.dto.subscriber;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.meveo.api.dto.BaseDto;
import org.meveo.api.dto.order.AttributeDto;
import org.meveo.api.jaxb.DateTimeAdapter;
import org.meveo.commons.utils.StringUtils;
/**
 * 
 * @author Hatim OUDAD
 *
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class QuoteDetailDto extends BaseDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	 /**
     * The operator id.
     */
    @XmlElement(name = "Operator_Id", required = true)
    private String operatorId;
    
    /**
     * The bill code.
     */
    @XmlElement(name = "Bill_Code", required = true)
    private String billCode;
    
    /**
     * Required Financial Query Action
     */
    @XmlElement(name = "Action", required = true)
    private String action;
    
    /**
     * Required quote type
     */
    @XmlElement(name = "Quote_Type", required = true)
    private String quoteType;
    
    /**
     * The master account number.
     */
    @XmlElement(name = "Master_Account_Number")
    private String masterAccountNumber;
    
    /**
     * The action qualifier.
     */
    @XmlElement(name = "Action_Qualifier")
    private String billActionQualifier;
    
    /**
     * The quantity.
     */
    @XmlElement(name = "Quantity", required = true)
    private String billQuantity;
    
    /**
     * The effect date.
     */
    @XmlElement(name = "Effect_Date", required = true)
    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    Date billEffectDate;
    
    /**
     * The attributes list.
     */
    @XmlElement(name = "Attribute")
    private List<QuoteAttributeDto> attributes = new ArrayList<>();
    
    @XmlTransient
    private Map<String, QuoteAttributeDto> attributesMap;

	public String getOperatorId() {
		return operatorId;
	}

	public void setOperatorId(String operatorId) {
		this.operatorId = operatorId;
	}

	public String getBillCode() {
		return billCode;
	}

	public void setBillCode(String billCode) {
		this.billCode = billCode;
	}

	public String getAction() {
		return action;
	}

	public void setAction(String action) {
		this.action = action;
	}

	public String getQuoteType() {
		return quoteType;
	}

	public void setQuoteType(String quoteType) {
		this.quoteType = quoteType;
	}

	public String getMasterAccountNumber() {
		return masterAccountNumber;
	}

	public void setMasterAccountNumber(String masterAccountNumber) {
		this.masterAccountNumber = masterAccountNumber;
	}

	public String getBillActionQualifier() {
		return billActionQualifier;
	}

	public void setBillActionQualifier(String billActionQualifier) {
		this.billActionQualifier = billActionQualifier;
	}

	public String getBillQuantity() {
		return billQuantity;
	}

	public void setBillQuantity(String billQuantity) {
		this.billQuantity = billQuantity;
	}

	public Date getBillEffectDate() {
		return billEffectDate;
	}

	public void setBillEffectDate(Date billEffectDate) {
		this.billEffectDate = billEffectDate;
	}

	public List<QuoteAttributeDto> getAttributes() {
		return attributes;
	}

	public void setAttributes(List<QuoteAttributeDto> attributes) {
		this.attributes = attributes;
	}
	
	public Map<String, QuoteAttributeDto> getAttributesMap() {
		return attributesMap;
	}

	public void setAttributesMap(Map<String, QuoteAttributeDto> attributesMap) {
		this.attributesMap = attributesMap;
	}

	/**
     * Get the order line attribute by code
     *
     * @param code the attribute code
     * @return the order line attribute by code
     */
    private QuoteAttributeDto getAttributeDto(String code) {
        if (attributesMap == null) {
            attributesMap = new HashMap<>();
            for (QuoteAttributeDto attributeDto : attributes) {
                if (attributeDto != null && !StringUtils.isBlank(attributeDto.getCode())) {
                    attributesMap.put(attributeDto.getCode(), attributeDto);
                }
            }
        }
        return attributesMap.get(code);
    }

    /**
     * Get the order line attribute by code
     *
     * @param code the attribute code
     * @return the order line attribute by code
     */
    public String getAttribute(String code) {
        QuoteAttributeDto attributeDto = getAttributeDto(code);
        if (attributeDto != null) {
            return attributeDto.getValue();
        }
        return null;
    }

  
}
