package org.meveo.api.dto.subscriber;

import java.io.Serializable;
import java.util.HashMap;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.meveo.api.dto.BaseDto;
import org.meveo.api.dto.order.AttributeDto;
import org.meveo.commons.utils.StringUtils;

/**
 * Attribute Dto
 *
 * @author Hatim OUDAD
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class QuoteAttributeDto extends BaseDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1735352520076029445L;

    /**
     * The code.
     */
    @XmlElement(name = "Code", required = true)
    private String code;

    /**
     * The value.
     */
    @XmlElement(name = "Value", required = true)
    private String value;

    /**
     * The unit.
     */
    @XmlElement(name = "Unit")
    private String unit;



    /**
     * Gets the code
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the value
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the unit
     *
     * @return the unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets the unit.
     *
     * @param unit the new unit
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }
  
}
