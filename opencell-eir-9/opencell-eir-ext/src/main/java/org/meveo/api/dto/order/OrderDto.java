package org.meveo.api.dto.order;

import com.eir.commons.enums.OrderProcessingTypeEnum;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.meveo.api.commons.ErrorEnum;
import org.meveo.model.crm.custom.CustomFieldValue;
import org.meveo.model.order.OrderStatusEnum;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Order Dto (Element tag names use InitCaps with underscores)
 *
 * @author Abdellatif BARI
 */
@XmlRootElement(name = "Wholesale_Billing")
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderDto extends ReleaseOrderDto {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -2510086889557547061L;

    /**
     * The ref order id.
     */
    @XmlElement(name = "Ref_Order_Id", required = true)
    private String refOrderCode;

    /**
     * The transaction id.
     */
    @XmlElement(name = "Transaction_Id", required = true)
    private String transactionId;

    /**
     * The ordering status.
     */
    @XmlTransient
    @JsonProperty("orderStatus")
    private OrderStatusEnum status = OrderStatusEnum.IN_CREATION;

    /**
     * The status message.
     */
    @XmlElement(name = "Status_Message")
    private String statusMessage;

    /**
     * The bill items.
     */
    @XmlElement(name = "Bill_Item")
    private List<OrderLineDto> orderLines = new ArrayList<>();

    @XmlTransient
    private Long orderId;
    @XmlTransient
    private ComplimentaryOrderDto complimentaryOrderDto;
    @XmlTransient
    private Set<String> subscriptionCodes = new HashSet<>();
    @XmlTransient
    private Set<String> refSubscriptionCodes = new HashSet<>();
    @XmlTransient
    private String dataDiscrepancies = "";
    @XmlTransient
    private String originalXmlOrder;
    @XmlTransient
    private OrderProcessingTypeEnum processingType;
    @XmlTransient
    private String releaseDescription;
    @XmlTransient
    private Set<String> provideMainSubscriptionCodes = new HashSet<>();
    @XmlTransient
    private Map<Long, OrderLineDto> serviceInstances = new LinkedHashMap();
    @XmlTransient
    private Map<String, String> serviceOfferTemplates = new LinkedHashMap();
    @XmlTransient
    private String heldBy;

    /**
     * each entry of this map will be composite from the code of entity as key and the technical id as value or the reverse
     */
    @XmlTransient
    private Map<String, Object> entities = new HashMap<>();

    // order details text included in CF : ORDER_DETAILS
    public Map<String, String> getOrderDetails() {
        Map<String, String> orderDetails = new HashMap<>();
        orderDetails.put(getOrderCode(), getOrderingSystem() + CustomFieldValue.MATRIX_KEY_SEPARATOR + refOrderCode + CustomFieldValue.MATRIX_KEY_SEPARATOR + transactionId);
        return orderDetails;
    }

    /**
     * Set order error
     *
     * @param errorEnum    The error enum
     * @param orderStatus  The order status
     * @param errorMessage The error message
     */
    public void setError(ErrorEnum errorEnum, OrderStatusEnum orderStatus, String errorMessage) {
        setErrorCode(errorEnum);
        setStatus(orderStatus);
        setErrorMessage(errorMessage);
    }

    /**
     * Gets the refOrderCode
     *
     * @return the refOrderCode
     */
    public String getRefOrderCode() {
        return refOrderCode;
    }

    /**
     * Sets the refOrderCode.
     *
     * @param refOrderCode the refOrderCode
     */
    public void setRefOrderCode(String refOrderCode) {
        this.refOrderCode = refOrderCode;
    }

    /**
     * Gets the transactionId
     *
     * @return the transactionId
     */
    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Sets the transactionId.
     *
     * @param transactionId the transactionId
     */
    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    /**
     * Gets the statusMessage
     *
     * @return the statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * Sets the statusMessage.
     *
     * @param statusMessage the statusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    /**
     * Gets the status
     *
     * @return the status
     */
    public OrderStatusEnum getStatus() {
        return status;
    }

    /**
     * Sets the status.
     *
     * @param status the status
     */
    public void setStatus(OrderStatusEnum status) {
        this.status = status;
    }

    /**
     * Gets the orderLines
     *
     * @return the orderLines
     */
    public List<OrderLineDto> getOrderLines() {
        return orderLines;
    }

    /**
     * Sets the orderLines.
     *
     * @param orderLines the orderLines
     */
    public void setOrderLines(List<OrderLineDto> orderLines) {
        this.orderLines = orderLines;
    }

    /**
     * Gets the orderId
     *
     * @return the orderId
     */
    public Long getOrderId() {
        return orderId;
    }

    /**
     * Sets the orderId.
     *
     * @param orderId the orderId
     */
    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    /**
     * Gets the complimentaryOrderDto
     *
     * @return the complimentaryOrderDto
     */
    public ComplimentaryOrderDto getComplimentaryOrderDto() {
        return complimentaryOrderDto;
    }

    /**
     * Sets the complimentaryOrderDto.
     *
     * @param complimentaryOrderDto the complimentaryOrderDto
     */
    public void setComplimentaryOrderDto(ComplimentaryOrderDto complimentaryOrderDto) {
        this.complimentaryOrderDto = complimentaryOrderDto;
    }

    /**
     * Gets the subscriptionCodes
     *
     * @return the subscriptionCodes
     */
    @Override
    public Set<String> getSubscriptionCodes() {
        return subscriptionCodes;
    }

    /**
     * Sets the subscriptionCodes.
     *
     * @param subscriptionCodes the subscriptionCodes
     */
    @Override
    public void setSubscriptionCodes(Set<String> subscriptionCodes) {
        this.subscriptionCodes = subscriptionCodes;
    }

    /**
     * Gets the refSubscriptionCodes
     *
     * @return the refSubscriptionCodes
     */
    public Set<String> getRefSubscriptionCodes() {
        return refSubscriptionCodes;
    }

    /**
     * Sets the refSubscriptionCodes.
     *
     * @param refSubscriptionCodes the refSubscriptionCodes
     */
    public void setRefSubscriptionCodes(Set<String> refSubscriptionCodes) {
        this.refSubscriptionCodes = refSubscriptionCodes;
    }

    /**
     * Gets the dataDiscrepancies
     *
     * @return the dataDiscrepancies
     */
    public String getDataDiscrepancies() {
        return dataDiscrepancies;
    }

    /**
     * Sets the dataDiscrepancies.
     *
     * @param dataDiscrepancies the dataDiscrepancies
     */
    public void setDataDiscrepancies(String dataDiscrepancies) {
        this.dataDiscrepancies = dataDiscrepancies;
    }

    /**
     * Gets the originalXmlOrder
     *
     * @return the originalXmlOrder
     */
    public String getOriginalXmlOrder() {
        return originalXmlOrder;
    }

    /**
     * Sets the originalXmlOrder.
     *
     * @param originalXmlOrder the originalXmlOrder
     */
    public void setOriginalXmlOrder(String originalXmlOrder) {
        this.originalXmlOrder = originalXmlOrder;
    }

    /**
     * Gets the processingType
     *
     * @return the processingType
     */
    @Override
    public OrderProcessingTypeEnum getProcessingType() {
        return processingType;
    }

    /**
     * Sets the processingType.
     *
     * @param processingType the processingType
     */
    @Override
    public void setProcessingType(OrderProcessingTypeEnum processingType) {
        this.processingType = processingType;
    }

    /**
     * Gets the releaseDescription
     *
     * @return the releaseDescription
     */
    public String getReleaseDescription() {
        return releaseDescription;
    }

    /**
     * Sets the releaseDescription.
     *
     * @param releaseDescription the releaseDescription
     */
    public void setReleaseDescription(String releaseDescription) {
        this.releaseDescription = releaseDescription;
    }

    /**
     * Gets the provideMainSubscriptionCodes
     *
     * @return the provideMainSubscriptionCodes
     */
    public Set<String> getProvideMainSubscriptionCodes() {
        return provideMainSubscriptionCodes;
    }

    /**
     * Sets the provideMainSubscriptionCodes.
     *
     * @param provideMainSubscriptionCodes the provideMainSubscriptionCodes
     */
    public void setProvideMainSubscriptionCodes(Set<String> provideMainSubscriptionCodes) {
        this.provideMainSubscriptionCodes = provideMainSubscriptionCodes;
    }

    /**
     * Gets the serviceInstances
     *
     * @return the serviceInstances
     */
    public Map<Long, OrderLineDto> getServiceInstances() {
        return serviceInstances;
    }

    /**
     * Sets the serviceInstances.
     *
     * @param serviceInstances the serviceInstances
     */
    public void setServiceInstances(Map<Long, OrderLineDto> serviceInstances) {
        this.serviceInstances = serviceInstances;
    }

    /**
     * Gets the serviceOfferTemplates
     *
     * @return the serviceOfferTemplates
     */
    public Map<String, String> getServiceOfferTemplates() {
        return serviceOfferTemplates;
    }

    /**
     * Sets the serviceOfferTemplates.
     *
     * @param serviceOfferTemplates the serviceOfferTemplates
     */
    public void setServiceOfferTemplates(Map<String, String> serviceOfferTemplates) {
        this.serviceOfferTemplates = serviceOfferTemplates;
    }

    /**
     * Gets the heldBy
     *
     * @return the heldBy
     */
    public String getHeldBy() {
        return heldBy;
    }

    /**
     * Sets the heldBy.
     *
     * @param heldBy the heldBy
     */
    public void setHeldBy(String heldBy) {
        this.heldBy = heldBy;
    }

    /**
     * Gets the entities
     *
     * @return the entities
     */
    public Map<String, Object> getEntities() {
        return entities;
    }

    /**
     * Sets the entities.
     *
     * @param entities the entities
     */
    public void setEntities(Map<String, Object> entities) {
        this.entities = entities;
    }
}
