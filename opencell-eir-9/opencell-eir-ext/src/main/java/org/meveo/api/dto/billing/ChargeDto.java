package org.meveo.api.dto.billing;

import java.io.Serializable;
import java.util.Date;

/**
 * Charge Dto
 *
 * @author Abdellatif BARI
 */
public class ChargeDto implements Serializable {


    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7462249564740703699L;

    private String subscriberPrefix = null;
    private String serviceId = null;
    private String billCode = null;
    private Date transDate = null;
    private String transDateTo = null;
    private String billText = null;
    private String taxClass = null;
    private String amount = null;
    private String chargeReference = null;
    private String quantity = null;
    private String notes = null;

    /**
     * Gets the subscriberPrefix
     *
     * @return the subscriberPrefix
     */
    public String getSubscriberPrefix() {
        return subscriberPrefix;
    }

    /**
     * Sets the subscriberPrefix.
     *
     * @param subscriberPrefix the subscriberPrefix
     */
    public void setSubscriberPrefix(String subscriberPrefix) {
        this.subscriberPrefix = subscriberPrefix;
    }

    /**
     * Gets the serviceId
     *
     * @return the serviceId
     */
    public String getServiceId() {
        return serviceId;
    }

    /**
     * Sets the serviceId.
     *
     * @param serviceId the serviceId
     */
    public void setServiceId(String serviceId) {
        this.serviceId = serviceId;
    }

    /**
     * Gets the billCode
     *
     * @return the billCode
     */
    public String getBillCode() {
        return billCode;
    }

    /**
     * Sets the billCode.
     *
     * @param billCode the billCode
     */
    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    /**
     * Gets the transDate
     *
     * @return the transDate
     */
    public Date getTransDate() {
        return transDate;
    }

    /**
     * Sets the transDate.
     *
     * @param transDate the transDate
     */
    public void setTransDate(Date transDate) {
        this.transDate = transDate;
    }

    /**
     * Gets the transDateTo
     *
     * @return the transDateTo
     */
    public String getTransDateTo() {
        return transDateTo;
    }

    /**
     * Sets the transDateTo.
     *
     * @param transDateTo the transDateTo
     */
    public void setTransDateTo(String transDateTo) {
        this.transDateTo = transDateTo;
    }

    /**
     * Gets the billText
     *
     * @return the billText
     */
    public String getBillText() {
        return billText;
    }

    /**
     * Sets the billText.
     *
     * @param billText the billText
     */
    public void setBillText(String billText) {
        this.billText = billText;
    }

    /**
     * Gets the taxClass
     *
     * @return the taxClass
     */
    public String getTaxClass() {
        return taxClass;
    }

    /**
     * Sets the taxClass.
     *
     * @param taxClass the taxClass
     */
    public void setTaxClass(String taxClass) {
        this.taxClass = taxClass;
    }

    /**
     * Gets the amount
     *
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Sets the amount.
     *
     * @param amount the amount
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * Gets the chargeReference
     *
     * @return the chargeReference
     */
    public String getChargeReference() {
        return chargeReference;
    }

    /**
     * Sets the chargeReference.
     *
     * @param chargeReference the chargeReference
     */
    public void setChargeReference(String chargeReference) {
        this.chargeReference = chargeReference;
    }

    /**
     * Gets the quantity
     *
     * @return the quantity
     */
    public String getQuantity() {
        return quantity;
    }

    /**
     * Sets the quantity.
     *
     * @param quantity the quantity
     */
    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }
    
    
}
