package org.meveo.api.dto.flatfile;

import org.meveo.model.admin.Seller;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.BillingCycle;
import org.meveo.model.billing.UserAccount;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.crm.Customer;
import org.meveo.model.payments.CustomerAccount;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;

/**
 * Billing account record Dto
 *
 * @author Abdellatif BARI
 */
public class BillingAccountRecordDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -9114517267234440420L;

    private String customerAccountCode;
    private String billingAccountCode;
    private String billingAccountName;
    private String billingAddress1;
    private String billingAddress2;
    private String billingAddress3;
    private String billingCity;
    private String billingPostalCode;
    private String serviceType;
    private String billingTaxCategory;
    private String billingCycleCode;
    private String subscriptionCode;
    private Date startDate;
    private String billCode;
    private String amount;

    private OfferTemplate offerTemplate;
    private Seller seller;
    private BillingCycle billingCycle;
    private Customer customer;
    private CustomerAccount customerAccount;
    private BillingAccount billingAccount;
    private UserAccount userAccount;
    private ServiceTemplate serviceTemplate;
    private StringBuffer errorMessage;

    /**
     * Gets the customerAccountCode
     *
     * @return the customerAccountCode
     */
    public String getCustomerAccountCode() {
        return customerAccountCode;
    }

    /**
     * Sets the customerAccountCode.
     *
     * @param customerAccountCode the customerAccountCode
     */
    public void setCustomerAccountCode(String customerAccountCode) {
        this.customerAccountCode = customerAccountCode;
    }

    /**
     * Gets the billingAccountCode
     *
     * @return the billingAccountCode
     */
    public String getBillingAccountCode() {
        return billingAccountCode;
    }

    /**
     * Sets the billingAccountCode.
     *
     * @param billingAccountCode the billingAccountCode
     */
    public void setBillingAccountCode(String billingAccountCode) {
        this.billingAccountCode = billingAccountCode;
    }

    /**
     * Gets the billingAccountName
     *
     * @return the billingAccountName
     */
    public String getBillingAccountName() {
        return billingAccountName;
    }

    /**
     * Sets the billingAccountName.
     *
     * @param billingAccountName the billingAccountName
     */
    public void setBillingAccountName(String billingAccountName) {
        this.billingAccountName = billingAccountName;
    }

    /**
     * Gets the billingAddress1
     *
     * @return the billingAddress1
     */
    public String getBillingAddress1() {
        return billingAddress1;
    }

    /**
     * Sets the billingAddress1.
     *
     * @param billingAddress1 the billingAddress1
     */
    public void setBillingAddress1(String billingAddress1) {
        this.billingAddress1 = billingAddress1;
    }

    /**
     * Gets the billingAddress2
     *
     * @return the billingAddress2
     */
    public String getBillingAddress2() {
        return billingAddress2;
    }

    /**
     * Sets the billingAddress2.
     *
     * @param billingAddress2 the billingAddress2
     */
    public void setBillingAddress2(String billingAddress2) {
        this.billingAddress2 = billingAddress2;
    }

    /**
     * Gets the billingAddress3
     *
     * @return the billingAddress3
     */
    public String getBillingAddress3() {
        return billingAddress3;
    }

    /**
     * Sets the billingAddress3.
     *
     * @param billingAddress3 the billingAddress3
     */
    public void setBillingAddress3(String billingAddress3) {
        this.billingAddress3 = billingAddress3;
    }

    /**
     * Gets the billingCity
     *
     * @return the billingCity
     */
    public String getBillingCity() {
        return billingCity;
    }

    /**
     * Sets the billingCity.
     *
     * @param billingCity the billingCity
     */
    public void setBillingCity(String billingCity) {
        this.billingCity = billingCity;
    }

    /**
     * Gets the billingPostalCode
     *
     * @return the billingPostalCode
     */
    public String getBillingPostalCode() {
        return billingPostalCode;
    }

    /**
     * Sets the billingPostalCode.
     *
     * @param billingPostalCode the billingPostalCode
     */
    public void setBillingPostalCode(String billingPostalCode) {
        this.billingPostalCode = billingPostalCode;
    }

    /**
     * Gets the serviceType
     *
     * @return the serviceType
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * Sets the serviceType.
     *
     * @param serviceType the serviceType
     */
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    /**
     * Gets the billingTaxCategory
     *
     * @return the billingTaxCategory
     */
    public String getBillingTaxCategory() {
        return billingTaxCategory;
    }

    /**
     * Sets the billingTaxCategory.
     *
     * @param billingTaxCategory the billingTaxCategory
     */
    public void setBillingTaxCategory(String billingTaxCategory) {
        this.billingTaxCategory = billingTaxCategory;
    }

    /**
     * Gets the billingCycleCode
     *
     * @return the billingCycleCode
     */
    public String getBillingCycleCode() {
        return billingCycleCode;
    }

    /**
     * Sets the billingCycleCode.
     *
     * @param billingCycleCode the billingCycleCode
     */
    public void setBillingCycleCode(String billingCycleCode) {
        this.billingCycleCode = billingCycleCode;
    }

    /**
     * Gets the subscriptionCode
     *
     * @return the subscriptionCode
     */
    public String getSubscriptionCode() {
        return subscriptionCode;
    }

    /**
     * Sets the subscriptionCode.
     *
     * @param subscriptionCode the subscriptionCode
     */
    public void setSubscriptionCode(String subscriptionCode) {
        this.subscriptionCode = subscriptionCode;
    }

    /**
     * Gets the startDate
     *
     * @return the startDate
     */
    public Date getStartDate() {
        return startDate;
    }

    /**
     * Sets the startDate.
     *
     * @param startDate the startDate
     */
    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    /**
     * Gets the billCode
     *
     * @return the billCode
     */
    public String getBillCode() {
        return billCode;
    }

    /**
     * Sets the billCode.
     *
     * @param billCode the billCode
     */
    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    /**
     * Gets the amount
     *
     * @return the amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * Sets the amount.
     *
     * @param amount the amount
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * Gets the offerTemplate
     *
     * @return the offerTemplate
     */
    public OfferTemplate getOfferTemplate() {
        return offerTemplate;
    }

    /**
     * Sets the offerTemplate.
     *
     * @param offerTemplate the offerTemplate
     */
    public void setOfferTemplate(OfferTemplate offerTemplate) {
        this.offerTemplate = offerTemplate;
    }

    /**
     * Gets the seller
     *
     * @return the seller
     */
    public Seller getSeller() {
        return seller;
    }

    /**
     * Sets the seller.
     *
     * @param seller the seller
     */
    public void setSeller(Seller seller) {
        this.seller = seller;
    }

    /**
     * Gets the billingCycle
     *
     * @return the billingCycle
     */
    public BillingCycle getBillingCycle() {
        return billingCycle;
    }

    /**
     * Sets the billingCycle.
     *
     * @param billingCycle the billingCycle
     */
    public void setBillingCycle(BillingCycle billingCycle) {
        this.billingCycle = billingCycle;
    }

    /**
     * Gets the customer
     *
     * @return the customer
     */
    public Customer getCustomer() {
        return customer;
    }

    /**
     * Sets the customer.
     *
     * @param customer the customer
     */
    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    /**
     * Gets the customerAccount
     *
     * @return the customerAccount
     */
    public CustomerAccount getCustomerAccount() {
        return customerAccount;
    }

    /**
     * Sets the customerAccount.
     *
     * @param customerAccount the customerAccount
     */
    public void setCustomerAccount(CustomerAccount customerAccount) {
        this.customerAccount = customerAccount;
    }

    /**
     * Gets the billingAccount
     *
     * @return the billingAccount
     */
    public BillingAccount getBillingAccount() {
        return billingAccount;
    }

    /**
     * Sets the billingAccount.
     *
     * @param billingAccount the billingAccount
     */
    public void setBillingAccount(BillingAccount billingAccount) {
        this.billingAccount = billingAccount;
    }

    /**
     * Gets the userAccount
     *
     * @return the userAccount
     */
    public UserAccount getUserAccount() {
        return userAccount;
    }

    /**
     * Sets the userAccount.
     *
     * @param userAccount the userAccount
     */
    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    /**
     * Gets the serviceTemplate
     *
     * @return the serviceTemplate
     */
    public ServiceTemplate getServiceTemplate() {
        return serviceTemplate;
    }

    /**
     * Sets the serviceTemplate.
     *
     * @param serviceTemplate the serviceTemplate
     */
    public void setServiceTemplate(ServiceTemplate serviceTemplate) {
        this.serviceTemplate = serviceTemplate;
    }

    /**
     * Gets the errorMessage
     *
     * @return the errorMessage
     */
    public StringBuffer getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the errorMessage.
     *
     * @param errorMessage the errorMessage
     */
    public void setErrorMessage(StringBuffer errorMessage) {
        this.errorMessage = errorMessage;
    }
}
