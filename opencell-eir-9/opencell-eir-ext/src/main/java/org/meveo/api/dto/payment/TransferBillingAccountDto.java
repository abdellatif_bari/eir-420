package org.meveo.api.dto.payment;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Transfer billing account DTO
 *
 * @author Abdellatif BARI
 */
public class TransferBillingAccountDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 8718364492136154925L;

    /**
     * The source billing account.
     */
    private String fromBillingAccountNumber;

    /**
     * The recipient billing account.
     */
    private String toBillingAccountNumber;

    /**
     * The amount.
     */
    private BigDecimal amount;

    /**
     * The operation reason.
     */
    private String operationReason;

    /**
     * Gets the fromBillingAccountNumber
     *
     * @return the fromBillingAccountNumber
     */
    public String getFromBillingAccountNumber() {
        return fromBillingAccountNumber;
    }

    /**
     * Sets the fromBillingAccountNumber.
     *
     * @param fromBillingAccountNumber the fromBillingAccountNumber
     */
    public void setFromBillingAccountNumber(String fromBillingAccountNumber) {
        this.fromBillingAccountNumber = fromBillingAccountNumber;
    }

    /**
     * Gets the toBillingAccountNumber
     *
     * @return the toBillingAccountNumber
     */
    public String getToBillingAccountNumber() {
        return toBillingAccountNumber;
    }

    /**
     * Sets the toBillingAccountNumber.
     *
     * @param toBillingAccountNumber the toBillingAccountNumber
     */
    public void setToBillingAccountNumber(String toBillingAccountNumber) {
        this.toBillingAccountNumber = toBillingAccountNumber;
    }

    /**
     * Gets the amount
     *
     * @return the amount
     */
    public BigDecimal getAmount() {
        return amount;
    }

    /**
     * Sets the amount.
     *
     * @param amount the new amount
     */
    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    /**
     * Gets the operationReason
     *
     * @return the operationReason
     */
    public String getOperationReason() {
        return operationReason;
    }

    /**
     * Sets the operationReason.
     *
     * @param operationReason the operationReason
     */
    public void setOperationReason(String operationReason) {
        this.operationReason = operationReason;
    }
}
