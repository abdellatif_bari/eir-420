package org.meveo.api.rest.eir.payment.impl;

import com.eir.api.payment.PaymentApi;
import org.meveo.api.dto.payment.ReverseAccountOperationDto;
import org.meveo.api.dto.payment.TransferBillingAccountDto;
import org.meveo.api.dto.payment.TransferBillingAccountsDto;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.eir.EirRs;
import org.meveo.api.rest.eir.payment.PaymentRs;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.core.Response;

/**
 * REST Web service implementation for managing payment ressources.
 *
 * @author Abdellatif BARI
 */
@RequestScoped
@Interceptors({WsRestApiInterceptor.class})
public class PaymentRsImpl extends EirRs implements PaymentRs {

    @Inject
    private PaymentApi paymentApi;

    /**
     * Transfer account operation.
     *
     * @param transferBillingAccountsDto the transfer billing accounts Dto
     * @return the transfer account response
     */
    @Override
    public Response transferAccountOperation(TransferBillingAccountsDto transferBillingAccountsDto) {
        Response response = null;
        try {
            paymentApi.transferAccountOperation(transferBillingAccountsDto);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    /**
     * Transfer amount.
     *
     * @param transferBillingAccountDto the transfer billing account Dto
     * @return the transfer account response
     */
    @Override
    public Response transferAmount(TransferBillingAccountDto transferBillingAccountDto) {
        Response response = null;
        try {
            paymentApi.transferAmount(transferBillingAccountDto);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    /**
     * Reverse payment.
     *
     * @param reverseAccountOperationDto the reverse account operation Dto
     * @return the reverse payment response
     */
    @Override
    public Response reversePayment(ReverseAccountOperationDto reverseAccountOperationDto) {
        Response response = null;
        try {
            paymentApi.reversePayment(reverseAccountOperationDto);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

}
