package org.meveo.api.dto.billing;

import org.meveo.api.commons.Utils;
import org.meveo.model.shared.DateUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Usage Rating Data Transfer Object
 *
 * @author Mohammed Amine TAZI
 */
public class UsageRatingDto implements Serializable {

    private static final long serialVersionUID = 2088496470330285925L;

    private Long trafficClassId;
    private String trafficClassCode;
    private String trafficClassDescription;
    private Long tariffPlanId;
    private Long timeBandId;
    private Long customerId;
    private Date eventDate;
    private Date initDate;
    private BigDecimal inputQuantity;
    private BigDecimal quantity;
    private Map<Long, Long> callDurationPerTimePeriod;
    private Long rounding;
    private BigDecimal minCharge;
    private BigDecimal connectionCharge;
    private BigDecimal totalCharge;
    private Long minDuration;
    private boolean split;
    private RoundingMode roundingMode;

    public enum RoundingMode {UP, DOWN, NONE}

    ;
    private Long invoiceSubCategoryId;
    private Long accountingCodeId;
    private Long taxClassId;
    private List<TimePeriodDto> timePeriods;
    private Map<Long, Long> timePeriodPerCalendar;

    public final static String SEPARATOR = "|";

    public Long getTrafficClassId() {
        return trafficClassId;
    }

    public void setTrafficClassId(Long trafficClassId) {
        this.trafficClassId = trafficClassId;
    }

    public String getTrafficClassCode() {
        return trafficClassCode;
    }

    public void setTrafficClassCode(String trafficClassCode) {
        this.trafficClassCode = trafficClassCode;
    }

    public String getTrafficClassDescription() {
        return trafficClassDescription;
    }
    
    public void setTrafficClassDescription(String trafficClassDescription) {
        this.trafficClassDescription = trafficClassDescription;
    }
    
    public Long getTariffPlanId() {
        return tariffPlanId;
    }

    public void setTariffPlanId(Long tariffPlanId) {
        this.tariffPlanId = tariffPlanId;
    }

    public Long getTimeBandId() {
        return timeBandId;
    }

    public void setTimeBandId(Long timeBandId) {
        this.timeBandId = timeBandId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public BigDecimal getInputQuantity() {
        return inputQuantity;
    }

    public void setInputQuantity(BigDecimal inputQuantity) {
        this.inputQuantity = inputQuantity;
    }

    public BigDecimal getQuantity() {
        return quantity;
    }

    public void setQuantity(BigDecimal quantity) {
        this.quantity = quantity;
    }

    public Long getCallDuration(Long timePeriodId) {
        if (callDurationPerTimePeriod == null) {
            return null;
        }
        return callDurationPerTimePeriod.get(timePeriodId);
    }

    public Long getRounding() {
        return rounding;
    }

    public void setRounding(Long rounding) {
        this.rounding = rounding;
    }

    public RoundingMode getRoundingMode() {
        return roundingMode;
    }

    public void setRoundingMode(RoundingMode roundingMode) {
        this.roundingMode = roundingMode;
    }

    public BigDecimal getMinCharge() {
        return minCharge;
    }

    public void setMinCharge(BigDecimal minCharge) {
        this.minCharge = minCharge;
    }

    public BigDecimal getConnectionCharge() {
        return connectionCharge;
    }

    public void setConnectionCharge(BigDecimal connectionCharge) {
        this.connectionCharge = connectionCharge;
    }

    public BigDecimal getTotalCharge() {
        return totalCharge;
    }

    public void setTotalCharge(BigDecimal totalCharge) {
        this.totalCharge = totalCharge;
    }

    public Long getMinDuration() {
        return minDuration;
    }

    public void setMinDuration(Long minDuration) {
        this.minDuration = minDuration;
    }

    public boolean isSplit() {
        return split;
    }

    public void setSplit(boolean split) {
        this.split = split;
    }

    public Long getInvoiceSubCategoryId() {
        return invoiceSubCategoryId;
    }

    public void setInvoiceSubCategoryId(Long invoiceSubCategoryId) {
        this.invoiceSubCategoryId = invoiceSubCategoryId;
    }

    public Long getAccountingCodeId() {
        return accountingCodeId;
    }

    public void setAccountingCodeId(Long accountingCodeId) {
        this.accountingCodeId = accountingCodeId;
    }

    public Long getTaxClassId() {
        return taxClassId;
    }

    public void setTaxClassId(Long taxClassId) {
        this.taxClassId = taxClassId;
    }

    public List<TimePeriodDto> getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(List<TimePeriodDto> timePeriods) {
        this.timePeriods = timePeriods;
    }

    public Map<Long, Long> getTimePeriodPerCalendar() {
        return timePeriodPerCalendar;
    }

    public void setTimePeriodPerCalendar(Map<Long, Long> timePeriodPerCalendar) {
        this.timePeriodPerCalendar = timePeriodPerCalendar;
    }

    public Date getInitDate() {
        return initDate;
    }

    public void setInitDate(Date initDate) {
        this.initDate = initDate;
    }

    @Override
    public String toString() {
        StringBuilder value = new StringBuilder();
        value.append(minCharge).append(SEPARATOR);
        value.append(trafficClassDescription).append(SEPARATOR);
        for (TimePeriodDto timePeriod : timePeriods) {
            value.append(timePeriod.getTimePeriodCode()).append(SEPARATOR)
                    .append(formatDate(timePeriod.getTimePeriodStart())).append(SEPARATOR)
                    .append(formatDate(timePeriod.getTimePeriodEnd())).append(SEPARATOR)
                    .append(timePeriod.getTimePeriodAmount()).append(SEPARATOR);
        }
        value.deleteCharAt(value.lastIndexOf(SEPARATOR));
        return value.toString();
    }

    private String formatDate(Date date) {
        if (date == null) {
            return "";
        }
        return DateUtils.formatDateWithPattern(date, Utils.DATE_TIME_PATTERN);
    }
}
