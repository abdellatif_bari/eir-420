package org.meveo.api.rest.eir.tariff;

import org.meveo.api.dto.custom.UnitaryCustomTableDataDto;
import org.meveo.api.dto.flatfile.ExportDto;
import org.meveo.api.rest.IBaseRs;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web service interface for tariff managing.
 *
 * @author Abdellatif BARI
 */
@Path("/tariff")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public interface TariffRs extends IBaseRs {

    /**
     * Create or update tariff plan.
     *
     * @param unitaryCustomTableDataDto Unitary Custom Table Data Dto
     * @return Tariff plan response
     */
    @POST
    @Path("/tariffPlan/createOrUpdate")
    Response createOrUpdateTariffPlan(UnitaryCustomTableDataDto unitaryCustomTableDataDto);

    /**
     * Create or update tariff plan map.
     *
     * @param unitaryCustomTableDataDto Unitary Custom Table Data Dto
     * @return Tariff plan map response
     */
    @POST
    @Path("/tariffPlanMap/createOrUpdate")
    Response createOrUpdateTariffPlanMap(UnitaryCustomTableDataDto unitaryCustomTableDataDto);

    /**
     * Validate tariffs in a staging table
     *
     * @param offerType The offer type
     * @param fileId    The id of a tariff import file
     * @return The validate response
     */
    @POST
    @Path("/validate/{offerType}/{fileId}")
    Response validateTariff(@PathParam("offerType") String offerType, @PathParam("fileId") Long fileId);

    /**
     * Activate tariffs in a staging table
     *
     * @param offerType The offer type
     * @param fileId    The id of a tariff import file
     * @return The activate response
     */
    @POST
    @Path("/activate/{offerType}/{fileId}")
    Response activateTariff(@PathParam("offerType") String offerType, @PathParam("fileId") Long fileId);

    /**
     * Cancel tariffs in a staging table
     *
     * @param fileId The id of a tariff import file
     * @return Tha cancel response
     */
    @POST
    @Path("/cancel/{fileId}")
    Response cancelTariff(@PathParam("fileId") Long fileId);

    /**
     * Export tariff
     *
     * @param exportDto export Dto
     * @return The export response
     */
    @POST
    @Path("/export")
    Response exportTariff(ExportDto exportDto);

}