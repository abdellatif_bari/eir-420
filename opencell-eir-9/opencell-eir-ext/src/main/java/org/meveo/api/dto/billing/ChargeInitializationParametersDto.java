package org.meveo.api.dto.billing;

/**
 * Charge initialization parameters Dto from a tariff
 */
public class ChargeInitializationParametersDto {

    /**
     * Charge/service description
     */
    String description;

    /**
     * Recurring calendar identifier
     */
    Long recurringCalendarId;

    /**
     * Charge initialization parameters from a tariff
     *
     * @param description         Charge/service description
     * @param recurringCalendarId Recurring calendar identifier
     */
    public ChargeInitializationParametersDto(String description, Long recurringCalendarId) {
        super();
        this.description = description;
        this.recurringCalendarId = recurringCalendarId;
    }

    /**
     * @return Charge/service description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @return Recurring calendar identifier
     */
    public Long getRecurringCalendarId() {
        return recurringCalendarId;
    }
}