package org.meveo.api.dto.billing;

import java.io.Serializable;
import java.util.Date;

/**
 * Access point Dto
 *
 * @author Abdellatif BARI
 */
public class AccessDto  implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -8757244515155177974L;

    /**
     * Access user id
     */
    private String accessUserId;

    /**
     * End Date
     */
    private Date endDate;

    /**
     * Gets the accessUserId
     *
     * @return the accessUserId
     */
    public String getAccessUserId() {
        return accessUserId;
    }

    /**
     * Sets the accessUserId.
     *
     * @param accessUserId the new accessUserId
     */
    public void setAccessUserId(String accessUserId) {
        this.accessUserId = accessUserId;
    }

    /**
     * Gets the endDate
     *
     * @return the endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * Sets the endDate.
     *
     * @param endDate the new endDate
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }
}
