package org.meveo.api.rest.eir.billing.impl;

import com.eir.api.billing.BillingAccountApi;
import org.meveo.api.dto.account.BillingAccountDto;
import org.meveo.api.dto.billing.BillingAccountDebtorStatementDto;
import org.meveo.api.dto.billing.BillingAccountDetailsResponseDto;
import org.meveo.api.dto.billing.SequenceResponseDto;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.eir.EirRs;
import org.meveo.api.rest.eir.billing.BillingAccountRs;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.core.Response;

/**
 * REST Web service implementation for managing {@link org.meveo.model.order.Order} ressource.
 *
 * @author Abdellatif BARI
 * @author H. ZNIBAR
 */
@RequestScoped
@Interceptors({WsRestApiInterceptor.class})
public class BillingAccountRsImpl extends EirRs implements BillingAccountRs {

    @Inject
    private BillingAccountApi billingAccountApi;

    /**
     * Create a new billing account.
     *
     * @param billingAccountDto Billing account Dto
     * @return Billing account response
     */
    @Override
    public Response create(BillingAccountDto billingAccountDto) {
        Response response = null;
        try {
            billingAccountApi.create(billingAccountDto);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    /**
     * Get a billing account sequence number.
     *
     * @return Billing account sequence number response.
     */
    @Override
    public Response getSequenceNumber() {
        Response response = null;
        try {
            SequenceResponseDto result = billingAccountApi.getSequenceNumber();
            response = buildSuccessResponse(result);
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    /**
     * Get billing accounts list by customer account code
     *
     * @param customerAccountCode the customer account code
     * @return Order response
     */
    @Override
    public Response listByCustomerAccount(String customerAccountCode) {
        Response response = null;
        try {
            BillingAccountDetailsResponseDto result = billingAccountApi.listByCustomerAccount(customerAccountCode);
            response = buildDtoResponse(result);
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    @Override
    public Response exportDebtorStatement(BillingAccountDebtorStatementDto billingAccountDebtorStatementDto) {
        Response response = null;
        try {
            BillingAccountDebtorStatementDto result = billingAccountApi.export(billingAccountDebtorStatementDto);
            response = buildSuccessResponse(result);
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

}
