package org.meveo.api.dto.customer;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import org.meveo.api.dto.BaseDto;

/**
 * Export Customer balances Dto
 *
 * @author Houssine ZNIBAR
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class ExportCustomerBalancesDto extends BaseDto {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 540150922389950763L;
    
    /** The customer code. */
    private String customerCode;
    
    /**
     * The file path
     */
    private String filePath;
    
    /**
     * Gets the filePath
     *
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Sets the filePath.
     *
     * @param filePath the filePath
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    /**
     * Gets the customer code.
     *
     * @return the customer code
     */
    public String getCustomerCode() {
        return customerCode;
    }

    /**
     * Sets the customer code.
     *
     * @param customerCode the new customer code
     */
    public void setCustomerCode(String customerCode) {
        this.customerCode = customerCode;
    }
    
    
}