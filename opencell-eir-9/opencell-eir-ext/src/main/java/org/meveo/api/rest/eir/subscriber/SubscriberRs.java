package org.meveo.api.rest.eir.subscriber;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.meveo.api.dto.subscriber.QuotesRequestDto;
import org.meveo.api.dto.subscriber.ServiceChargesRequestDto;
import org.meveo.api.rest.IBaseRs;

import io.swagger.v3.oas.annotations.parameters.RequestBody;

/**
 * REST Web service interface for managing {@link org.meveo.model.order.Order} resource.
 *
 * @author Mohammed Amine TAZI
 * @author Hatim OUDAD
 */
@Path("/subscriber")
@Consumes(MediaType.APPLICATION_XML)
@Produces(MediaType.APPLICATION_XML)
public interface SubscriberRs extends IBaseRs {

    /**
     * List services charges for a subscriber
     * @param servicesChargesDto body request
     * @return service charges
     */
    @POST
    @Path("/charges/list")
    Response listServiceCharges(@RequestBody ServiceChargesRequestDto serviceChargesRequestDto);
    
    /**
     * List quotes
     * @param quotesRequestDto
     * @return quotes
     */
    @POST
    @Path("/quotes/list")
	Response listQuotes(@RequestBody QuotesRequestDto quotesRequestDto);
}