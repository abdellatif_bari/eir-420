package org.meveo.api.dto.order;

import org.meveo.api.commons.Utils;
import org.meveo.model.shared.DateUtils;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import java.util.Date;

/**
 * This is Adaptor class which has main responsibility
 * to convert from java.util.Date to format string of date.
 *
 * @author Abdellatif BARI
 */
class DateTimeAdapter extends XmlAdapter<String, Date> {

    @Override
    public Date unmarshal(String xml) {
        return DateUtils.parseDateWithPattern(xml, Utils.DATE_TIME_PATTERN);
    }

    @Override
    public String marshal(Date object) {
        return DateUtils.formatDateWithPattern(object, Utils.DATE_TIME_PATTERN);
    }
}
