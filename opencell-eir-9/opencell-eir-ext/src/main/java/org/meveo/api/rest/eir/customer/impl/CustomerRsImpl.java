package org.meveo.api.rest.eir.customer.impl;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.core.Response;

import org.meveo.api.dto.customer.ExportCustomerBalancesDto;
import org.meveo.api.dto.order.ExportOrderDto;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.eir.EirRs;
import org.meveo.api.rest.eir.customer.CustomerRs;

import com.eir.api.customer.CustomerApi;

/**
 * REST Web service implementation for managing customer resource.
 *
 * @author Houssine ZNIBAR
 */
@RequestScoped
@Interceptors({WsRestApiInterceptor.class})
public class CustomerRsImpl extends EirRs implements CustomerRs {

    @Inject
    CustomerApi customerApi;

    @Override
    public Response exportBalances(String customerCode) {
        Response response = null;
        try {
            ExportCustomerBalancesDto result = customerApi.export(customerCode);
            response = buildSuccessResponse(result);
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    

}
