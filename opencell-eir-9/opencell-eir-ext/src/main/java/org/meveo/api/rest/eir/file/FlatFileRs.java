package org.meveo.api.rest.eir.file;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.meveo.api.dto.flatfile.FlatFilesDto;
import org.meveo.api.dto.response.PagingAndFiltering;
import org.meveo.api.rest.IBaseRs;

/**
 * REST Web service interface for managing {@link org.meveo.model.bi.FlatFile} resource.
 *
 * @author Abdellatif BARI
 */
@Path("/flatFile")
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML })
public interface FlatFileRs extends IBaseRs {

    /**
     * Reprocess a flat file orders
     *
     * @param flatFilesDto the flat file Dto.
     * @return Reprocessing response
     */
    @POST
    @Path("/reprocess")
    Response reprocess(FlatFilesDto flatFilesDto);

    /**
     * Get a unique list of filenames that are in staging table.
     * 
     * @param pagingAndFiltering Search parameters. Expected to contain these parameters: tableName and fileFormat
     * @return A list of file names with id
     */
    @POST
    @Path("/filesInStagingTable")
    Response getFilesInStagingTable(PagingAndFiltering pagingAndFiltering);

}