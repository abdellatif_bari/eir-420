package org.meveo.api.rest.eir.file;

import org.meveo.api.commons.FileRecordsTypeEnum;
import org.meveo.api.dto.flatfile.ExportDto;
import org.meveo.api.rest.IBaseRs;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web service interface for managing file.
 *
 * @author Abdellatif BARI
 */
@Path("/file")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public interface FileRs extends IBaseRs {

    /**
     * Download file with a given file name.
     *
     * @param file file name
     * @return Request processing status
     */
    @GET
    @Path("/downloadFile")
    Response downloadFile(@QueryParam("file") String file);

    /**
     * Download file with a given file id and or without file records type.
     *
     * @param fileId          file id
     * @param fileRecordsType file records type
     * @return Request processing status
     */
    @GET
    @Path("/downloadFile/{fileId}")
    Response downloadFile(@PathParam("fileId") Long fileId, @QueryParam("fileRecordsType") FileRecordsTypeEnum fileRecordsType);

    /**
     * Export the filtered data from custom table as file
     *
     * @param customTableCode Custom table code - can be either db table's name or a custom entity template code
     * @param exportDto       Export Dto
     * @return Request processing status
     */
    @POST
    @Path("/exportFile/{customTableCode}")
    Response exportFile(@PathParam("customTableCode") String customTableCode, ExportDto exportDto);

}