package org.meveo.api.rest.eir.order.impl;

import com.eir.api.order.ExportOrderApi;
import com.eir.api.order.OrderApi;
import com.eir.commons.enums.OrderProcessingTypeEnum;
import org.meveo.api.dto.order.ExportOrderDto;
import org.meveo.api.dto.order.OrderDto;
import org.meveo.api.dto.order.OrdersResponseDto;
import org.meveo.api.dto.order.ReleaseOrderDto;
import org.meveo.api.dto.order.ReprocessOrderDto;
import org.meveo.api.dto.response.PagingAndFiltering;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.eir.EirRs;
import org.meveo.api.rest.eir.order.OrderRs;

import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.core.Response;
import java.util.List;


/**
 * REST Web service implementation for managing {@link org.meveo.model.order.Order} ressource.
 *
 * @author Abdellatif BARI
 */
@RequestScoped
@Interceptors({WsRestApiInterceptor.class})
public class OrderRsImpl extends EirRs implements OrderRs {

    @Inject
    private OrderApi orderApi;
    @Inject
    private ExportOrderApi exportOrderApi;

    /**
     * Place a new order
     *
     * @param orderDto order Dto
     * @return Order response
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public Response create(OrderDto orderDto) {
        Response response = null;
        try {
            OrderDto result = orderApi.createAsync(orderDto);
            response = buildResponse(result);
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    /**
     * Reprocess the orders
     *
     * @param reprocessOrderDtoList reprocess order Dto list.
     * @return Reprocess response
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public Response reprocess(List<ReprocessOrderDto> reprocessOrderDtoList) {

        Response response = null;
        try {
            orderApi.reprocess(reprocessOrderDtoList);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    /**
     * Release the orders
     *
     * @param releaseOrderDtoList release order Dto list.
     * @return Order response
     */
    @Override
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public Response release(List<ReleaseOrderDto> releaseOrderDtoList) {
        Response response = null;
        try {
            orderApi.release(releaseOrderDtoList);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    /**
     * Export the orders
     *
     * @param exportOrderDto the export order Dto.
     * @return File orders response
     */
    @Override
    public Response export(ExportOrderDto exportOrderDto) {
        Response response = null;
        try {
            ExportOrderDto result = exportOrderApi.export(exportOrderDto);
            response = buildSuccessResponse(result);
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    /**
     * List orders matching a given criteria
     *
     * @param pagingAndFiltering the paging and filtering
     * @return orders list response
     */
    @Override
    public Response list(PagingAndFiltering pagingAndFiltering) {
        Response response = null;
        try {
            OrdersResponseDto result = orderApi.list(pagingAndFiltering);
            response = buildSuccessResponse(result);
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }
}
