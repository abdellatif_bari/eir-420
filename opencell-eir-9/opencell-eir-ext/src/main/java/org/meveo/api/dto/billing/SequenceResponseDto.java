package org.meveo.api.dto.billing;

import org.meveo.api.dto.BaseDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Sequence response Dto
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class SequenceResponseDto extends BaseDto {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -6079688246185038427L;

    public SequenceResponseDto(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    /**
     * The sequence number.
     */
    @XmlElement(name = "sequenceNumber")
    String sequenceNumber;

    /**
     * Gets the sequenceNumber
     *
     * @return the sequenceNumber
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the sequenceNumber.
     *
     * @param sequenceNumber the sequenceNumber
     */
    public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }
}