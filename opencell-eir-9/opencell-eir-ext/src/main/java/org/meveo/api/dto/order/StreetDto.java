package org.meveo.api.dto.order;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Street Dto
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class StreetDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -3384661112714749034L;

    /**
     * The street id.
     */
    @XmlElement(name = "Street_Id")
    private String streetId;

    /**
     * The street name.
     */
    @XmlElement(name = "Street_Name")
    private String streetName;

    /**
     * The street suffix.
     */
    @XmlElement(name = "Street_Suffix")
    private String streetSuffix;

    /**
     * The directional qualifier name.
     */
    @XmlElement(name = "Directional_Qualifier_Name")
    private String directionalQualifierName;

    /**
     * The geographical qualifier name.
     */
    @XmlElement(name = "Geographical_Qualifier_Name")
    private String geographicalQualifierName;

    /**
     * The townland name.
     */
    @XmlElement(name = "Townland_Name")
    private String townlandName;

    /**
     * Gets the streetId
     *
     * @return the streetId
     */
    public String getStreetId() {
        return streetId;
    }

    /**
     * Sets the streetId.
     *
     * @param streetId the new streetId
     */
    public void setStreetId(String streetId) {
        this.streetId = streetId;
    }

    /**
     * Gets the streetName
     *
     * @return the streetName
     */
    public String getStreetName() {
        return streetName;
    }

    /**
     * Sets the streetName.
     *
     * @param streetName the new streetName
     */
    public void setStreetName(String streetName) {
        this.streetName = streetName;
    }

    /**
     * Gets the streetSuffix
     *
     * @return the streetSuffix
     */
    public String getStreetSuffix() {
        return streetSuffix;
    }

    /**
     * Sets the streetSuffix.
     *
     * @param streetSuffix the new streetSuffix
     */
    public void setStreetSuffix(String streetSuffix) {
        this.streetSuffix = streetSuffix;
    }

    /**
     * Gets the directionalQualifierName
     *
     * @return the directionalQualifierName
     */
    public String getDirectionalQualifierName() {
        return directionalQualifierName;
    }

    /**
     * Sets the directionalQualifierName.
     *
     * @param directionalQualifierName the new directionalQualifierName
     */
    public void setDirectionalQualifierName(String directionalQualifierName) {
        this.directionalQualifierName = directionalQualifierName;
    }

    /**
     * Gets the geographicalQualifierName
     *
     * @return the geographicalQualifierName
     */
    public String getGeographicalQualifierName() {
        return geographicalQualifierName;
    }

    /**
     * Sets the geographicalQualifierName.
     *
     * @param geographicalQualifierName the new geographicalQualifierName
     */
    public void setGeographicalQualifierName(String geographicalQualifierName) {
        this.geographicalQualifierName = geographicalQualifierName;
    }

    /**
     * Gets the townlandName
     *
     * @return the townlandName
     */
    public String getTownlandName() {
        return townlandName;
    }

    /**
     * Sets the townlandName.
     *
     * @param townlandName the new townlandName
     */
    public void setTownlandName(String townlandName) {
        this.townlandName = townlandName;
    }
}
