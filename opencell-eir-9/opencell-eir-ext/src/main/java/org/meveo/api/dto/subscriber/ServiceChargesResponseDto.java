package org.meveo.api.dto.subscriber;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.meveo.api.dto.BaseDto;

/**
 * Service charges Response DTO
 *
 * @author Mohammed Amine TAZI
 */
@XmlRootElement(name = "OC_Charge_Query_Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class ServiceChargesResponseDto extends BaseDto {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1359700363762222207L;

    @XmlElement(name = "Error_Message")
    private String errorMessage;
    
    /**
     * Unique for charge request order in the custom table  
     */
    @XmlElement(name = "Transaction_Id", required = true)
    private String transactionId;
    
    /**
     * charge Order identifier
     */
    @XmlElement(name = "Charge_Query_Id", required = true)
    private String chargeQueryId;
    
    /**
     * Required Financial Query Action
     */
    @XmlElement(name = "Action", required = true)
    private String action;
    
    /**
     * Value of connection amount
     */
    @XmlElement(name = "Connection_Amount", required = true)
    private String connectionAmount;
    
    /**
     * Value of Annual Rental   
     */
    @XmlElement(name = "Annual_Rental", required = true)
    private String annualRental;
    
    /**
     * Value of any Excess Cost incurred
     */
    @XmlElement(name = "Excess_Cost", required = true)
    private String excessCost;

    /**
     * Indication of Non-Standard Calculation of Charges
     */
    @XmlElement(name = "Specially_Assessed", required = true)
    private String speciallyAssessed;

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getChargeQueryId() {
        return chargeQueryId;
    }

    public void setChargeQueryId(String chargeQueryId) {
        this.chargeQueryId = chargeQueryId;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getConnectionAmount() {
        return connectionAmount;
    }

    public void setConnectionAmount(String connectionAmout) {
        this.connectionAmount = connectionAmout;
    }

    public String getAnnualRental() {
        return annualRental;
    }

    public void setAnnualRental(String annualRental) {
        this.annualRental = annualRental;
    }

    public String getExcessCost() {
        return excessCost;
    }

    public void setExcessCost(String excessCost) {
        this.excessCost = excessCost;
    }

    public String getSpeciallyAssessed() {
        return speciallyAssessed;
    }

    public void setSpeciallyAssessed(String speciallyAssessed) {
        this.speciallyAssessed = speciallyAssessed;
    }
    
    @Override
    public String getErrorMessage() {
        return errorMessage;
    }
    
    @Override
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
