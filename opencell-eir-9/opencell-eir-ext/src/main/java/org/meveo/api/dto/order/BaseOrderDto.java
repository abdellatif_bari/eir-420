package org.meveo.api.dto.order;

import com.eir.commons.enums.OrderProcessingTypeEnum;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.meveo.api.dto.BaseDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Base order Dto (Element tag names use InitCaps with underscores)
 *
 * @author Abdellatif BARI
 */
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BaseOrderDto extends BaseDto {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7096564202088841697L;

    /**
     * The ordering system.
     */
    @XmlElement(name = "Ordering_System", required = true)
    @JsonProperty("orderEmitter")
    private String orderingSystem;

    /**
     * The order id.
     */
    @XmlElement(name = "Order_Id", required = true)
    @JsonProperty("orderCode")
    private String orderCode;


    @XmlTransient
    private OrderProcessingTypeEnum processingType;

    @XmlTransient
    private Set<String> subscriptionCodes = new HashSet<>();

    @XmlTransient
    private String providerCode;

    @XmlTransient
    private String userName;

    /**
     * Gets the orderingSystem
     *
     * @return the orderingSystem
     */
    public String getOrderingSystem() {
        return orderingSystem;
    }

    /**
     * Sets the orderingSystem.
     *
     * @param orderingSystem the orderingSystem
     */
    public void setOrderingSystem(String orderingSystem) {
        this.orderingSystem = orderingSystem;
    }

    /**
     * Gets the orderCode
     *
     * @return the orderCode
     */
    public String getOrderCode() {
        return orderCode;
    }

    /**
     * Sets the orderCode.
     *
     * @param orderCode the orderCode
     */
    public void setOrderCode(String orderCode) {
        this.orderCode = orderCode;
    }

    /**
     * Gets the processingType
     *
     * @return the processingType
     */
    public OrderProcessingTypeEnum getProcessingType() {
        return processingType;
    }

    /**
     * Sets the processingType.
     *
     * @param processingType the processingType
     */
    public void setProcessingType(OrderProcessingTypeEnum processingType) {
        this.processingType = processingType;
    }

    /**
     * Gets the subscriptionCodes
     *
     * @return the subscriptionCodes
     */
    public Set<String> getSubscriptionCodes() {
        return subscriptionCodes;
    }

    /**
     * Sets the subscriptionCodes.
     *
     * @param subscriptionCodes the subscriptionCodes
     */
    public void setSubscriptionCodes(Set<String> subscriptionCodes) {
        this.subscriptionCodes = subscriptionCodes;
    }

    public void addSubscriptionCodes(List<OrderLineDto> orderLines) {
        if (orderLines != null && !orderLines.isEmpty() && getSubscriptionCodes().isEmpty()) {
            for (OrderLineDto orderLineDto : orderLines) {
                // Set subscription codes
                getSubscriptionCodes().add(orderLineDto.getSubscriptionCode());
            }
        }
    }

    /**
     * Gets the providerCode
     *
     * @return the providerCode
     */
    public String getProviderCode() {
        return providerCode;
    }

    /**
     * Sets the providerCode.
     *
     * @param providerCode the providerCode
     */
    public void setProviderCode(String providerCode) {
        this.providerCode = providerCode;
    }

    /**
     * Gets the userName
     *
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * Sets the userName.
     *
     * @param userName the userName
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }
}
