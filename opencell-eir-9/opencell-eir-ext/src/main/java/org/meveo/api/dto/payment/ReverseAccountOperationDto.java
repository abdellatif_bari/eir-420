package org.meveo.api.dto.payment;

/**
 * Reverse account operation DTO
 *
 * @author Abdellatif BARI
 */
public class ReverseAccountOperationDto extends BaseAccountOperationDto {


    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -8634081420622072340L;
}
