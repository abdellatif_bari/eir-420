package org.meveo.api.rest.eir;

import org.meveo.admin.exception.BusinessException;
import org.meveo.api.commons.ErrorEnum;
import org.meveo.api.commons.ResponseBuilderEntity;
import org.meveo.api.commons.ResponseStatusEnum;
import org.meveo.api.dto.BaseDto;
import org.meveo.api.dto.response.SearchResponse;
import org.meveo.api.rest.impl.BaseRs;
import org.meveo.commons.utils.StringUtils;

import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.File;

/**
 * The REST Web service base class.
 *
 * @author Abdellatif BARI
 */
public class EirRs extends BaseRs {

    /**
     * Build success response
     *
     * @return the success response
     */
    protected Response buildSuccessResponse() {
        ResponseBuilderEntity responseBuilderEntity = new ResponseBuilderEntity();
        Response.ResponseBuilder responseBuilder = null;

        responseBuilderEntity.setStatus(ResponseStatusEnum.SUCCESS);
        responseBuilderEntity.setMessage("");
        responseBuilder = Response.status(Response.Status.OK).entity(responseBuilderEntity);
        return responseBuilder.build();
    }

    /**
     * Build success response
     *
     * @param baseDto the base Dto
     * @return the success response
     */
    protected Response buildSuccessResponse(BaseDto baseDto) {
        Response.ResponseBuilder responseBuilder;
        responseBuilder = Response.status(Response.Status.OK).entity(baseDto);
        return responseBuilder.build();
    }

    /**
     * Build success response
     *
     * @param searchResponse the search response
     * @return the success response
     */
    protected Response buildSuccessResponse(SearchResponse searchResponse) {
        Response.ResponseBuilder responseBuilder;
        responseBuilder = Response.status(Response.Status.OK).entity(searchResponse);
        return responseBuilder.build();
    }

    /**
     * Build success response
     *
     * @param successMessage the success message
     * @return the success response
     */
    protected Response buildSuccessResponse(String successMessage) {
        ResponseBuilderEntity responseBuilderEntity = new ResponseBuilderEntity();
        responseBuilderEntity.setStatus(ResponseStatusEnum.SUCCESS);
        responseBuilderEntity.setMessage(successMessage);
        Response.ResponseBuilder responseBuilder = Response.status(Response.Status.OK).entity(responseBuilderEntity);
        return responseBuilder.build();
    }

    /**
     * Build success response
     *
     * @param file the file
     * @return the success response
     */
    protected Response buildSuccessResponse(File file) {
        Response.ResponseBuilder responseBuilder;
        responseBuilder = Response.status(Response.Status.OK).entity(file);
        responseBuilder.header("Content-Disposition", "attachment; filename=" + file.getName());
        return responseBuilder.build();
    }

    /**
     * Build success response
     *
     * @param streamingOutput the streaming output
     * @param fileName        the file name
     * @return the success response
     */
    protected Response buildSuccessResponse(StreamingOutput streamingOutput, String fileName) {
        Response.ResponseBuilder responseBuilder;
        responseBuilder = Response.status(Response.Status.OK).entity(streamingOutput).type(MediaType.APPLICATION_OCTET_STREAM);
        responseBuilder.header("Content-Disposition", "attachment; filename=" + fileName);
        return responseBuilder.build();
    }

    /**
     * Build failure response
     *
     * @param errorCode    the error code
     * @param errorMessage the error message
     * @return the failure response
     */
    protected Response buildFailureResponse(ErrorEnum errorCode, String errorMessage) {
        ResponseBuilderEntity responseBuilderEntity = new ResponseBuilderEntity();
        responseBuilderEntity.setErrorCode(errorCode);
        responseBuilderEntity.setStatus(ResponseStatusEnum.FAILURE);
        responseBuilderEntity.setMessage(errorMessage);
        Response.ResponseBuilder responseBuilder = Response.status(errorCode.getStatus()).entity(responseBuilderEntity);
        return responseBuilder.build();

    }

    /**
     * Build failure response from the exception
     *
     * @param e the exception
     * @return the failure response
     */
    protected Response buildFailureResponse(Exception e) {

        ResponseBuilderEntity responseBuilderEntity = new ResponseBuilderEntity();
        Response.ResponseBuilder responseBuilder = null;
        String errorMessage = e.getMessage();
        ErrorEnum errorCode = ErrorEnum.GENERIC_API_EXCEPTION;

        if (!(e instanceof BusinessException)) {
            Throwable cause = e.getCause();
            if (cause != null) {
                errorMessage = cause.getMessage();
            }
        }

        errorMessage = StringUtils.isBlank(errorMessage) ? "Exception occurred at the API level" : errorMessage;
        responseBuilderEntity.setErrorCode(errorCode);
        responseBuilderEntity.setStatus(ResponseStatusEnum.FAILURE);
        responseBuilderEntity.setMessage(errorMessage);
        responseBuilder = Response.status(errorCode.getStatus()).entity(responseBuilderEntity);
        return responseBuilder.build();
    }

    /**
     * Build response
     *
     * @param baseDto the base response Dto
     * @return the REST response
     */
    protected Response buildResponse(BaseDto baseDto) {
        Response response = null;
        if (baseDto == null) {
            return null;
        }
        if (baseDto.hasBlockedError()) {
            response = buildFailureResponse(baseDto.getErrorCode(), baseDto.getErrorMessage());
        } else {
            response = buildSuccessResponse(baseDto.getSuccessMessage());
        }
        return response;
    }

    /**
     * Build response
     *
     * @param baseDto the base response Dto
     * @return the REST response
     */
    protected Response buildDtoResponse(BaseDto baseDto) {
        Response response = null;
        if (baseDto == null) {
            return null;
        }
        if (baseDto.getErrorCode() != null) {
            response = buildFailureResponse(baseDto.getErrorCode(), baseDto.getErrorMessage());
        } else {
            response = buildSuccessResponse(baseDto);
        }
        return response;
    }
}