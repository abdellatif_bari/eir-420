package org.meveo.api.rest.eir.subscriber.impl;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.core.Response;

import org.meveo.api.dto.subscriber.QuotesRequestDto;
import org.meveo.api.dto.subscriber.QuotesResponseDto;
import org.meveo.api.dto.subscriber.ServiceChargesRequestDto;
import org.meveo.api.dto.subscriber.ServiceChargesResponseDto;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.eir.EirRs;
import org.meveo.api.rest.eir.subscriber.SubscriberRs;

import com.eir.api.subscriber.SubscriberApi;

/**
 * REST Web service implementation for managing Subscriber resources.
 *
 * @author Mohammed Amine TAZI
 * @author Hatim OUDAD
 */
@RequestScoped
@Interceptors({WsRestApiInterceptor.class})
public class SubscriberRsImpl extends EirRs implements SubscriberRs {

    @Inject
    private SubscriberApi subscriberApi;
    
    /**
     * List service charges for a subscriber
     * @param serviceChargesDto body request
     * @return service charges
     */
    @Override
    public Response listServiceCharges(ServiceChargesRequestDto serviceChargesRequestDto) {
        Response response = null;
        try {
            ServiceChargesResponseDto serviceChargesResponseDto = subscriberApi.listServiceCharges(serviceChargesRequestDto);
            response = buildSuccessResponse(serviceChargesResponseDto); 
        } catch (Exception e) {
            response = buildFailureResponse(serviceChargesRequestDto.getErrorCode(), serviceChargesRequestDto.getErrorMessage());
        }
        return response;
    }
    
    /**
     * List service and product quotes 
     * @param quotesDto body request
     * @return quotes
     */
    @Override
    public Response listQuotes(QuotesRequestDto quotesRequestDto) {
        Response response = null;
        try {
        	QuotesResponseDto quotesResponseDto = subscriberApi.listQuotes(quotesRequestDto);
            response = buildSuccessResponse(quotesResponseDto); 
        } catch (Exception e) {
            response = buildFailureResponse(quotesRequestDto.getErrorCode(), quotesRequestDto.getErrorMessage());
        }
        return response;
    }
}
