package org.meveo.api.dto.order;

import org.meveo.model.crm.custom.CustomFieldValue;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Attribute Dto
 *
 * @author abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class AttributeDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1735352520076029445L;

    /**
     * The code.
     */
    @XmlElement(name = "Code", required = true)
    private String code;

    /**
     * The value.
     */
    @XmlElement(name = "Value", required = true)
    private String value;

    /**
     * The unit.
     */
    @XmlElement(name = "Unit")
    private String unit;

    /**
     * The component id.
     */
    @XmlElement(name = "Component_Id")
    private String componentId;

    /**
     * The attribute type.
     */
    @XmlElement(name = "Attribute_Type")
    private String attributeType;

    public String getDetails() {
        StringBuilder sb = new StringBuilder();
        sb.append(value).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(unit).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(componentId)
                .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(attributeType).append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        return sb.toString();
    }

    /**
     * Gets the code
     *
     * @return the code
     */
    public String getCode() {
        return code;
    }

    /**
     * Sets the code.
     *
     * @param code the new code
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * Gets the value
     *
     * @return the value
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value.
     *
     * @param value the new value
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Gets the unit
     *
     * @return the unit
     */
    public String getUnit() {
        return unit;
    }

    /**
     * Sets the unit.
     *
     * @param unit the new unit
     */
    public void setUnit(String unit) {
        this.unit = unit;
    }

    /**
     * Gets the componentId
     *
     * @return the componentId
     */
    public String getComponentId() {
        return componentId;
    }

    /**
     * Sets the componentId.
     *
     * @param componentId the new componentId
     */
    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    /**
     * Gets the attributeType
     *
     * @return the attributeType
     */
    public String getAttributeType() {
        return attributeType;
    }

    /**
     * Sets the attributeType.
     *
     * @param attributeType the new attributeType
     */
    public void setAttributeType(String attributeType) {
        this.attributeType = attributeType;
    }
}
