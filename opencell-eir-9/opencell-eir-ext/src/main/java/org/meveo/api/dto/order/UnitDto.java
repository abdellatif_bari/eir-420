package org.meveo.api.dto.order;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * Unit Dto
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class UnitDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 6401698713477030092L;

    /**
     * The address unit id.
     */
    @XmlElement(name = "Address_Unit_Id")
    private String addressUnitId;

    /**
     * The unit number.
     */
    @XmlElement(name = "Unit_Number")
    private String unitNumber;

    /**
     * The unit name.
     */
    @XmlElement(name = "Unit_Name")
    private String unitName;

    /**
     * The unit type.
     */
    @XmlElement(name = "Unit_Type")
    private String unitType;

    /**
     * The floor number.
     */
    @XmlElement(name = "Floor_Number")
    private String floorNumber;

    /**
     * The eircode.
     */
    @XmlElement(name = "Eircode")
    private String eircode;

    /**
     * Gets the addressUnitId
     *
     * @return the addressUnitId
     */
    public String getAddressUnitId() {
        return addressUnitId;
    }

    /**
     * Sets the addressUnitId.
     *
     * @param addressUnitId the new addressUnitId
     */
    public void setAddressUnitId(String addressUnitId) {
        this.addressUnitId = addressUnitId;
    }

    /**
     * Gets the unitNumber
     *
     * @return the unitNumber
     */
    public String getUnitNumber() {
        return unitNumber;
    }

    /**
     * Sets the unitNumber.
     *
     * @param unitNumber the new unitNumber
     */
    public void setUnitNumber(String unitNumber) {
        this.unitNumber = unitNumber;
    }

    /**
     * Gets the unitName
     *
     * @return the unitName
     */
    public String getUnitName() {
        return unitName;
    }

    /**
     * Sets the unitName.
     *
     * @param unitName the new unitName
     */
    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    /**
     * Gets the unitType
     *
     * @return the unitType
     */
    public String getUnitType() {
        return unitType;
    }

    /**
     * Sets the unitType.
     *
     * @param unitType the new unitType
     */
    public void setUnitType(String unitType) {
        this.unitType = unitType;
    }

    /**
     * Gets the floorNumber
     *
     * @return the floorNumber
     */
    public String getFloorNumber() {
        return floorNumber;
    }

    /**
     * Sets the floorNumber.
     *
     * @param floorNumber the new floorNumber
     */
    public void setFloorNumber(String floorNumber) {
        this.floorNumber = floorNumber;
    }

    /**
     * Gets the eircode
     *
     * @return the eircode
     */
    public String getEircode() {
        return eircode;
    }

    /**
     * Sets the eircode.
     *
     * @param eircode the new eircode
     */
    public void setEircode(String eircode) {
        this.eircode = eircode;
    }
}
