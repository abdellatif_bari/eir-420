package org.meveo.api.dto.payment;

import java.io.Serializable;

/**
 * Base account operation DTO
 *
 * @author Abdellatif BARI
 */
public class BaseAccountOperationDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -2696205013634488895L;

    /**
     * The account operation Id.
     */
    private Long accountOperationId;

    /**
     * The operation reason.
     */
    private String operationReason;

    /**
     * Gets the accountOperationId
     *
     * @return the accountOperationId
     */
    public Long getAccountOperationId() {
        return accountOperationId;
    }

    /**
     * Sets the accountOperationId.
     *
     * @param accountOperationId the accountOperationId
     */
    public void setAccountOperationId(Long accountOperationId) {
        this.accountOperationId = accountOperationId;
    }

    /**
     * Gets the operationReason
     *
     * @return the operationReason
     */
    public String getOperationReason() {
        return operationReason;
    }

    /**
     * Sets the operationReason.
     *
     * @param operationReason the operationReason
     */
    public void setOperationReason(String operationReason) {
        this.operationReason = operationReason;
    }
}
