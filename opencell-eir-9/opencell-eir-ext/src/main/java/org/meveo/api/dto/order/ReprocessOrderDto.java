package org.meveo.api.dto.order;

import com.fasterxml.jackson.annotation.JsonInclude;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Reprocess order Dto
 *
 * @author abdellatif BARI
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class ReprocessOrderDto extends BaseOrderDto {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 8603872953189519507L;

}