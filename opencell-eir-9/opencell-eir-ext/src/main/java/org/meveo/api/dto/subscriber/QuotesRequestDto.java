package org.meveo.api.dto.subscriber;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.meveo.api.dto.BaseDto;
import org.meveo.api.dto.order.OrderLineDto;

/**
 * Quotes Request DTO
 *
 * @author Hatim OUDAD
 */
@XmlRootElement(name = "OC_Quotation_Query_Request")
@XmlAccessorType(XmlAccessType.FIELD)
public class QuotesRequestDto extends BaseDto {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -8202679774333065304L;

    /**
     * Unique for quote request order in the custom table  
     */
    @XmlElement(name = "Transaction_Id", required = true)
    private String transactionId;
    
    /**
     * Identifier for upstream ordering system
     */
    @XmlElement(name = "Ordering_System", required = true)
    private String orderingSystem;

    /**
     * quote Order identifier
     */
    @XmlElement(name = "Quote_Query_Id", required = true)
    private String quoteQueryId;
    
    /**
     * Order number supplied by Operator
     */
    @XmlElement(name = "Order_Number")
    private String orderNumber;

    /**
     * Ordering System Identifier for a related service Order
     */
    @XmlElement(name = "Order_Id")
    private String orderId;
    
    /**
     * The quote details.
     */
    @XmlElement(name = "Quote_Details")
    private List<QuoteDetailDto> quoteDetails = new ArrayList<>();
    
    

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getOrderingSystem() {
        return orderingSystem;
    }

    public void setOrderingSystem(String orderingSystem) {
        this.orderingSystem = orderingSystem;
    }

    public String getQuoteQueryId() {
        return quoteQueryId;
    }

    public void setQuoteQueryId(String quoteQueryId) {
        this.quoteQueryId = quoteQueryId;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public List<QuoteDetailDto> getQuoteDetails() {
		return quoteDetails;
	}

	public void setQuoteDetails(List<QuoteDetailDto> quoteDetails) {
		this.quoteDetails = quoteDetails;
	}
    

}
