package org.meveo.api.rest.eir.file.impl;

import com.eir.api.file.FileApi;
import com.eir.api.tariff.TariffApi;
import org.meveo.api.commons.FileRecordsTypeEnum;
import org.meveo.api.dto.flatfile.ExportDto;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.eir.file.FileRs;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.core.Response;

/**
 * REST Web service implementation for managing file.
 *
 * @author Abdellatif BARI
 */
@RequestScoped
@Interceptors({WsRestApiInterceptor.class})
public class FileRsImpl extends org.meveo.api.rest.eir.FileRs implements FileRs {

    @Inject
    private FileApi fileApi;
    @Inject
    private TariffApi tariffApi;

    /**
     * Download file
     *
     * @param filePath file path
     * @return the file response
     */
    @Override
    public Response downloadFile(String filePath) {
        Response response = null;
        try {
            response = getFile(fileApi.getFile(filePath));
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    /**
     * Download file
     *
     * @param fileId          file id
     * @param fileRecordsType file records type
     * @return the file response
     */
    @Override
    public Response downloadFile(Long fileId, FileRecordsTypeEnum fileRecordsType) {
        Response response = null;
        try {
            response = getFile(fileApi.getFile(fileId, fileRecordsType));
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    /**
     * Export as file the filtered data from custom table
     *
     * @param customTableCode Custom table code - can be either db table's name or a custom entity template code
     * @param exportDto       Export Dto
     * @return Request processing status
     */
    @Override
    public Response exportFile(String customTableCode, ExportDto exportDto) {
        Response response = null;
        try {
            response = getFile(fileApi.getFile(customTableCode, exportDto));
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }
}
