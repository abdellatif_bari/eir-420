package org.meveo.api.dto.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Billing account profiles Dto
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class BillingAccountProfilesDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -5525105611511649636L;

    /**
     * The billing account profiles.
     */
    @XmlElement(name = "Billing_Account_Profile")
    List<String> billingAccountProfiles = new ArrayList<>();

    /**
     * Gets the billingAccountProfiles
     *
     * @return the billingAccountProfiles
     */
    public List<String> getBillingAccountProfiles() {
        return billingAccountProfiles;
    }

    /**
     * Sets the billingAccountProfiles.
     *
     * @param billingAccountProfiles the new billingAccountProfiles
     */
    public void setBillingAccountProfiles(List<String> billingAccountProfiles) {
        this.billingAccountProfiles = billingAccountProfiles;
    }
}