package org.meveo.api.rest.eir.note.impl;

import com.eir.api.note.NoteApi;
import org.meveo.api.dto.note.NoteDto;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.eir.EirRs;
import org.meveo.api.rest.eir.note.NoteRs;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.core.Response;

/**
 * REST Web service implementation for managing note ressource.
 *
 * @author Mohammed Amine TAZI
 */
@RequestScoped
@Interceptors({WsRestApiInterceptor.class})
public class NoteRsImpl extends EirRs implements NoteRs {

    @Inject
    private NoteApi noteApi;

    /**
     * Create a new note
     *
     * @param noteDto note Dto
     * @return Note response
     */
    @Override
    public Response add(NoteDto noteDto) {
        Response response = null;
        try {
            NoteDto result = noteApi.add(noteDto);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    /**
     * Create a new note
     *
     * @param noteDto note Dto
     * @return Note response
     */
    @Override
    public Response delete(NoteDto noteDto) {
        Response response = null;
        try {
            NoteDto result = noteApi.delete(noteDto);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }


}
