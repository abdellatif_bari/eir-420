package org.meveo.api.rest.eir.rating.impl;

import com.eir.api.rating.CdrApi;
import org.meveo.api.dto.response.PagingAndFiltering;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.eir.EirRs;
import org.meveo.api.rest.eir.rating.CdrRs;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.core.Response;

/**
 * REST Web service implementation for CDR managing.
 *
 * @author Abdellatif BARI
 */
@RequestScoped
@Interceptors({WsRestApiInterceptor.class})
public class CdrRsImpl extends EirRs implements CdrRs {

    @Inject
    private CdrApi cdrApi;


    @Override
    public Response validateCdr(Long fileId) {
        Response response = null;
        try {
            cdrApi.validateCdr(fileId);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    @Override
    public Response activateCdr(Long fileId) {
        Response response = null;
        try {
            cdrApi.activateCdr(fileId);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    @Override
    public Response cancelCdr(Long fileId) {
        Response response = null;
        try {
            cdrApi.cancelCdr(fileId);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    @Override
    public Response exportCdr(PagingAndFiltering pagingAndFiltering) {
        Response response = null;
        try {
            cdrApi.exportCdr(pagingAndFiltering);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }
}