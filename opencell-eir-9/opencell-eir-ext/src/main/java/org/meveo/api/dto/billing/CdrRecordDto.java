package org.meveo.api.dto.billing;

import java.io.Serializable;
import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * CDR record Dto
 *
 * @author Abdellatif BARI
 */
@XmlRootElement(name="Record", namespace="http://www.tmforum.org")
@XmlType(name = "Record", namespace = "http://www.tmforum.org")
@JsonInclude(value = Include.NON_NULL)
public class CdrRecordDto implements Serializable {
    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 6024683496759559999L;

    private String processIdentifier = null;
    // Telno / Service ID/Access Number
    private String accessNumber = null;
    private String callDate = null;
    private String callTime = null;
    private String sequenceNumber = null;
    private String terminationNumber = null;
    private String callDuration = null;
    private String pulses = null;
    private String callType = null;
    private String serviceClass = null;
    private String feeCode = null;
    private String networkPlanningCode = null;
    private String originatingNumber = null;
    private String serviceCategory = null;
    private String clipClirIndicator = null;
    private String countryFeeCode = null;
    private String countryDescription = null;
    private StringBuffer errorMessages = null;
    private Date eventDate = null;

    private Long subscriptionId;
    private StringBuffer errorMessage;

    /**
     * Gets the processIdentifier
     *
     * @return the processIdentifier
     */
    public String getProcessIdentifier() {
        return processIdentifier;
    }

    /**
     * Sets the processIdentifier.
     *
     * @param processIdentifier the processIdentifier
     */
    public void setProcessIdentifier(String processIdentifier) {
        this.processIdentifier = processIdentifier;
    }

    /**
     * Gets the accessNumber
     *
     * @return the accessNumber
     */
    public String getAccessNumber() {
        return accessNumber;
    }

    /**
     * Sets the accessNumber.
     *
     * @param accessNumber the accessNumber
     */
    public void setAccessNumber(String accessNumber) {
        this.accessNumber = accessNumber;
    }

    /**
     * Gets the callDate
     *
     * @return the callDate
     */
    public String getCallDate() {
        return callDate;
    }

    /**
     * Sets the callDate.
     *
     * @param callDate the callDate
     */
    public void setCallDate(String callDate) {
        this.callDate = callDate;
    }

    /**
     * Gets the callTime
     *
     * @return the callTime
     */
    public String getCallTime() {
        return callTime;
    }

    /**
     * Sets the callTime.
     *
     * @param callTime the callTime
     */
    public void setCallTime(String callTime) {
        this.callTime = callTime;
    }

    /**
     * Gets the sequenceNumber
     *
     * @return the sequenceNumber
     */
    public String getSequenceNumber() {
        return sequenceNumber;
    }

    /**
     * Sets the sequenceNumber.
     *
     * @param sequenceNumber the sequenceNumber
     */
    public void setSequenceNumber(String sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    /**
     * Gets the terminationNumber
     *
     * @return the terminationNumber
     */
    public String getTerminationNumber() {
        return terminationNumber;
    }

    /**
     * Sets the terminationNumber.
     *
     * @param terminationNumber the terminationNumber
     */
    public void setTerminationNumber(String terminationNumber) {
        this.terminationNumber = terminationNumber;
    }

    /**
     * Gets the callDuration
     *
     * @return the callDuration
     */
    public String getCallDuration() {
        return callDuration;
    }

    /**
     * Sets the callDuration.
     *
     * @param callDuration the callDuration
     */
    public void setCallDuration(String callDuration) {
        this.callDuration = callDuration;
    }

    /**
     * Gets the pulses
     *
     * @return the pulses
     */
    public String getPulses() {
        return pulses;
    }

    /**
     * Sets the pulses.
     *
     * @param pulses the pulses
     */
    public void setPulses(String pulses) {
        this.pulses = pulses;
    }

    /**
     * Gets the callType
     *
     * @return the callType
     */
    public String getCallType() {
        return callType;
    }

    /**
     * Sets the callType.
     *
     * @param callType the callType
     */
    public void setCallType(String callType) {
        this.callType = callType;
    }

    /**
     * Gets the serviceClass
     *
     * @return the serviceClass
     */
    public String getServiceClass() {
        return serviceClass;
    }

    /**
     * Sets the serviceClass.
     *
     * @param serviceClass the serviceClass
     */
    public void setServiceClass(String serviceClass) {
        this.serviceClass = serviceClass;
    }

    /**
     * Gets the feeCode
     *
     * @return the feeCode
     */
    public String getFeeCode() {
        return feeCode;
    }

    /**
     * Sets the feeCode.
     *
     * @param feeCode the feeCode
     */
    public void setFeeCode(String feeCode) {
        this.feeCode = feeCode;
    }

    /**
     * Gets the networkPlanningCode
     *
     * @return the networkPlanningCode
     */
    public String getNetworkPlanningCode() {
        return networkPlanningCode;
    }

    /**
     * Sets the networkPlanningCode.
     *
     * @param networkPlanningCode the networkPlanningCode
     */
    public void setNetworkPlanningCode(String networkPlanningCode) {
        this.networkPlanningCode = networkPlanningCode;
    }

    /**
     * Gets the originatingNumber
     *
     * @return the originatingNumber
     */
    public String getOriginatingNumber() {
        return originatingNumber;
    }

    /**
     * Sets the originatingNumber.
     *
     * @param originatingNumber the originatingNumber
     */
    public void setOriginatingNumber(String originatingNumber) {
        this.originatingNumber = originatingNumber;
    }

    /**
     * Gets the serviceCategory
     *
     * @return the serviceCategory
     */
    public String getServiceCategory() {
        return serviceCategory;
    }

    /**
     * Sets the serviceCategory.
     *
     * @param serviceCategory the serviceCategory
     */
    public void setServiceCategory(String serviceCategory) {
        this.serviceCategory = serviceCategory;
    }

    /**
     * Gets the clipClirIndicator
     *
     * @return the clipClirIndicator
     */
    public String getClipClirIndicator() {
        return clipClirIndicator;
    }

    /**
     * Sets the clipClirIndicator.
     *
     * @param clipClirIndicator the clipClirIndicator
     */
    public void setClipClirIndicator(String clipClirIndicator) {
        this.clipClirIndicator = clipClirIndicator;
    }

    /**
     * Gets the countryFeeCode
     *
     * @return the countryFeeCode
     */
    public String getCountryFeeCode() {
        return countryFeeCode;
    }

    /**
     * Sets the countryFeeCode.
     *
     * @param countryFeeCode the countryFeeCode
     */
    public void setCountryFeeCode(String countryFeeCode) {
        this.countryFeeCode = countryFeeCode;
    }

    /**
     * Gets the countryDescription
     *
     * @return the countryDescription
     */
    public String getCountryDescription() {
        return countryDescription;
    }

    /**
     * Sets the countryDescription.
     *
     * @param countryDescription the countryDescription
     */
    public void setCountryDescription(String countryDescription) {
        this.countryDescription = countryDescription;
    }

    /**
     * Gets the errorMessages
     *
     * @return the errorMessages
     */
    public StringBuffer getErrorMessages() {
        return errorMessages;
    }

    /**
     * Sets the errorMessages.
     *
     * @param errorMessages the errorMessages
     */
    public void setErrorMessages(StringBuffer errorMessages) {
        this.errorMessages = errorMessages;
    }

    /**
     * Gets the eventDate
     *
     * @return the eventDate
     */
    public Date getEventDate() {
        return eventDate;
    }

    /**
     * Sets the eventDate.
     *
     * @param eventDate the eventDate
     */
    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    
    
    public Long getSubscriptionId() {
        return subscriptionId;
    }

    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    /**
     * Gets the errorMessage
     *
     * @return the errorMessage
     */
    public StringBuffer getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the errorMessage.
     *
     * @param errorMessage the errorMessage
     */
    public void setErrorMessage(StringBuffer errorMessage) {
        this.errorMessage = errorMessage;
    }      
}
