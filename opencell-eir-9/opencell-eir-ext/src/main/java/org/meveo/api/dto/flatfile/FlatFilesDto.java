package org.meveo.api.dto.flatfile;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Flat files Dto
 *
 * @author abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class FlatFilesDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -4402641066207919865L;

    /**
     * The file format code.
     */
    @XmlElement(required = true)
    String fileFormatCode;

    /**
     * The flat file ids.
     */
    @XmlElement(required = true)
    List<Long> flatFileIds = new ArrayList<>();

    /**
     * Gets the fileFormatCode
     *
     * @return the fileFormatCode
     */
    public String getFileFormatCode() {
        return fileFormatCode;
    }

    /**
     * Sets the fileFormatCode.
     *
     * @param fileFormatCode the new fileFormatCode
     */
    public void setFileFormatCode(String fileFormatCode) {
        this.fileFormatCode = fileFormatCode;
    }

    /**
     * Gets the flatFileIds
     *
     * @return the flatFileIds
     */
    public List<Long> getFlatFileIds() {
        return flatFileIds;
    }

    /**
     * Sets the flatFileIds.
     *
     * @param flatFileIds the new flatFileIds
     */
    public void setFlatFileIds(List<Long> flatFileIds) {
        this.flatFileIds = flatFileIds;
    }
}