package org.meveo.api.rest.eir.note;

import org.meveo.api.dto.note.NoteDto;
import org.meveo.api.rest.IBaseRs;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web service interface for managing note ressources.
 *
 * @author Mohammed Amine TAZI
 */
@Path("/note")
@Consumes({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
@Produces({ MediaType.APPLICATION_JSON, MediaType.APPLICATION_JSON })
public interface NoteRs extends IBaseRs {

    /**
     * Create a new note
     *
     * @param noteDto note Dto
     * @return Note response
     */
    @POST
    @Path("/add")
    Response add(NoteDto noteDto);
    
    @POST
    @Path("/delete")
    Response delete(NoteDto noteDto);

}