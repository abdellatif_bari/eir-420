package org.meveo.api.rest.eir.order;

import org.meveo.api.dto.order.ExportOrderDto;
import org.meveo.api.dto.order.OrderDto;
import org.meveo.api.dto.order.ReleaseOrderDto;
import org.meveo.api.dto.order.ReprocessOrderDto;
import org.meveo.api.dto.response.PagingAndFiltering;
import org.meveo.api.rest.IBaseRs;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

/**
 * REST Web service interface for managing {@link org.meveo.model.order.Order} ressource.
 *
 * @author Abdellatif BARI
 */
@Path("/order")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public interface OrderRs extends IBaseRs {

    /**
     * Place a new order
     *
     * @param orderDto order Dto
     * @return Order response
     */
    @POST
    @Path("/")
    Response create(OrderDto orderDto);

    /**
     * Reprocess the orders
     *
     * @param reprocessOrderDtoList reprocess order Dto list.
     * @return Reprocess response
     */
    @PUT
    @Path("/reprocess")
    Response reprocess(List<ReprocessOrderDto> reprocessOrderDtoList);

    /**
     * Release the orders
     *
     * @param releaseOrderDtoList release order Dto list.
     * @return Release response
     */
    @PUT
    @Path("/release")
    Response release(List<ReleaseOrderDto> releaseOrderDtoList);

    /**
     * Export the orders
     *
     * @param exportOrderDto the export order Dto.
     * @return File orders response
     */
    @POST
    @Path("/export")
    Response export(ExportOrderDto exportOrderDto);

    /**
     * List orders matching a given criteria
     *
     * @param pagingAndFiltering Pagination and filtering criteria
     * @return Orders list response
     */
    @POST
    @Path("/list")
    Response list(PagingAndFiltering pagingAndFiltering);
}