package org.meveo.api.dto.payment;

import java.math.BigDecimal;
import java.util.List;

/**
 * Transfer billing account DTO
 *
 * @author Abdellatif BARI
 */
public class TransferBillingAccountsDto extends BaseAccountOperationDto {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 6313806439797438746L;

    /**
     * The source billing account.
     */
    private String fromBillingAccountNumber;

    /**
     * The total amount to transfer.
     */
    private BigDecimal totalAmount;

    /**
     * The recipient billing accounts.
     */
    private List<TransferBillingAccountDto> toBillingAccounts;

    /**
     * Gets the fromBillingAccountNumber
     *
     * @return the fromBillingAccountNumber
     */
    public String getFromBillingAccountNumber() {
        return fromBillingAccountNumber;
    }

    /**
     * Sets the fromBillingAccountNumber.
     *
     * @param fromBillingAccountNumber the fromBillingAccountNumber
     */
    public void setFromBillingAccountNumber(String fromBillingAccountNumber) {
        this.fromBillingAccountNumber = fromBillingAccountNumber;
    }

    /**
     * Gets the totalAmount
     *
     * @return the totalAmount
     */
    public BigDecimal getTotalAmount() {
        return totalAmount;
    }

    /**
     * Sets the totalAmount.
     *
     * @param totalAmount the totalAmount
     */
    public void setTotalAmount(BigDecimal totalAmount) {
        this.totalAmount = totalAmount;
    }

    /**
     * Gets the toBillingAccounts
     *
     * @return the toBillingAccounts
     */
    public List<TransferBillingAccountDto> getToBillingAccounts() {
        return toBillingAccounts;
    }

    /**
     * Sets the toBillingAccounts.
     *
     * @param toBillingAccounts the new toBillingAccounts
     */
    public void setToBillingAccounts(List<TransferBillingAccountDto> toBillingAccounts) {
        this.toBillingAccounts = toBillingAccounts;
    }
}
