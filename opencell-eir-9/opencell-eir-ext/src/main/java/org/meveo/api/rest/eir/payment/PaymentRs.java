package org.meveo.api.rest.eir.payment;

import org.meveo.api.dto.payment.AccountOperationDto;
import org.meveo.api.dto.payment.ReverseAccountOperationDto;
import org.meveo.api.dto.payment.TransferBillingAccountDto;
import org.meveo.api.dto.payment.TransferBillingAccountsDto;
import org.meveo.api.rest.IBaseRs;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web service interface for managing payment ressources.
 *
 * @author Abdellatif BARI
 */
@Path("/payments")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public interface PaymentRs extends IBaseRs {

    /**
     * Transfer account operation.
     *
     * @param transferBillingAccountsDto the transfer billing accounts Dto
     * @return the transfer account response
     */
    @POST
    @Path("/transferAccountOperation")
    Response transferAccountOperation(TransferBillingAccountsDto transferBillingAccountsDto);

    /**
     * Transfer amount.
     *
     * @param transferBillingAccountDto the transfer billing account Dto
     * @return the transfer account response
     */
    @POST
    @Path("/transferAmount")
    Response transferAmount(TransferBillingAccountDto transferBillingAccountDto);

    /**
     * Reverse payment.
     *
     * @param reverseAccountOperationDto the reverse account operation Dto
     * @return the reverse payment response
     */
    @POST
    @Path("/reversePayment")
    Response reversePayment(ReverseAccountOperationDto reverseAccountOperationDto);
}