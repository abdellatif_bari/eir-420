package org.meveo.api.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import org.meveo.api.commons.ErrorEnum;

import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import java.io.Serializable;

/**
 * Define the base Dto.
 *
 * @author Abdellatif BARI
 **/
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(Include.NON_NULL)
public abstract class BaseDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -4167352793744070564L;

    @XmlTransient
    private ErrorEnum errorCode;
    @XmlTransient
    private String errorMessage;
    @XmlTransient
    private String successMessage;

    /**
     * Check if order has an blocked error
     *
     * @return true if the order has en error
     */
    public boolean hasBlockedError() {
        return (errorCode != null && errorCode.getStatus() != Response.Status.OK);
    }

    /**
     * Gets the errorCode
     *
     * @return the errorCode
     */
    public ErrorEnum getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the errorCode.
     *
     * @param errorCode the new errorCode
     */
    public void setErrorCode(ErrorEnum errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Gets the errorMessage
     *
     * @return the errorMessage
     */
    public String getErrorMessage() {
        return errorMessage;
    }

    /**
     * Sets the errorMessage.
     *
     * @param errorMessage the new errorMessage
     */
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    /**
     * Gets the successMessage
     *
     * @return the successMessage
     */
    public String getSuccessMessage() {
        return successMessage;
    }

    /**
     * Sets the successMessage.
     *
     * @param successMessage the successMessage
     */
    public void setSuccessMessage(String successMessage) {
        this.successMessage = successMessage;
    }
}