package org.meveo.api.dto.order;

import com.eir.commons.enums.ServiceTypeEnum;
import com.eir.commons.enums.customfields.ServiceInstanceIdentifierAttributesCFsEnum;
import org.meveo.api.commons.Utils;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.crm.custom.CustomFieldValue;
import org.meveo.model.shared.DateUtils;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Order Line Dto
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderLineDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 5057667346724418958L;

    /**
     * The item id.
     */
    @XmlElement(name = "Item_Id", required = true)
    private String itemId;

    /**
     * The operator id.
     */
    @XmlElement(name = "Operator_Id", required = true)
    private String operatorId;

    /**
     * The operator id.
     */
    @XmlElement(name = "Item_Component_Id", required = true)
    private String itemComponentId;

    /**
     * The item version.
     */
    @XmlElement(name = "Item_Version")
    private String itemVersion;

    /**
     * The item type.
     */
    @XmlElement(name = "Item_Type")
    private String itemType;

    /**
     * The agent code.
     */
    @XmlElement(name = "Agent_Code")
    private String agentCode;

    /**
     * The uan.
     */
    @XmlElement(name = "UAN")
    private String uan;

    /**
     * The master account number.
     */
    @XmlElement(name = "Master_Account_Number")
    private String masterAccountNumber;

    /**
     * The subscription code.
     */
    @XmlElement(name = "Service_Id")
    private String subscriptionCode;

    /**
     * The ref subscription code.
     */
    @XmlElement(name = "Ref_Service_Id")
    private String refSubscriptionCode;

    /**
     * The xref subscription code.
     */
    @XmlElement(name = "Xref_Service_Id")
    private String xRefSubscriptionCode;

    /**
     * The bill code.
     */
    @XmlElement(name = "Bill_Code", required = true)
    private String billCode;

    /**
     * The action.
     */
    @XmlElement(name = "Action", required = true)
    private String billAction;

    /**
     * The action qualifier.
     */
    @XmlElement(name = "Action_Qualifier")
    private String billActionQualifier;

    /**
     * The quantity.
     */
    @XmlElement(name = "Quantity", required = true)
    private String billQuantity;

    /**
     * The effect date.
     */
    @XmlElement(name = "Effect_Date", required = true)
    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    Date billEffectDate;

    /**
     * The special connect.
     */
    @XmlElement(name = "Special_Connect")
    private String specialConnect;

    /**
     * The special rental.
     */
    @XmlElement(name = "Special_Rental")
    private String specialRental;

    /**
     * The operator order number.
     */
    @XmlElement(name = "Operator_Order_Number")
    private String operatorOrderNumber;

    /**
     * The main service bill code.
     */
    @XmlElement(name = "Main_Service_Bill_Code")
    private String mainServiceBillCode;

    /**
     * The attributes list.
     */
    @XmlElement(name = "Attribute")
    private List<AttributeDto> attributes = new ArrayList<>();

    @XmlTransient
    private String offerTemplateCode;
    @XmlTransient
    private String billingAccountCode;
    @XmlTransient
    private String serviceType;
    @XmlTransient
    private String migrationCode;
    @XmlTransient
    private Date ceaseEffectDate;
    @XmlTransient
    private Date rentalLiabilityDate;
    @XmlTransient
    private Date slaPremiumLiabilityDate;
    @XmlTransient
    private String originalSubscriptionCode;
    @XmlTransient
    private String originalBillCode;
    @XmlTransient
    String chargeType;
    @XmlTransient
    BigDecimal deltaQuantity;
    @XmlTransient
    OrderLineDto refOrderLine;
    @XmlTransient
    private Map<String, AttributeDto> attributesMap;
    @XmlTransient
    ServiceInstanceIdentifierAttributesCFsEnum serviceInstanceIdentifierAttributes;
    @XmlTransient
    private Long serviceInstanceId;

    /**
     * added for a technical need on the portal side
     **/
    @XmlTransient
    private Long offerTemplateId;
    /**
     * added for a technical need on the portal side
     **/
    @XmlTransient
    private Long subscriptionId;


    public String getDetails() {
        StringBuilder sb = new StringBuilder();
        String action = getOriginalOrderAction();
        String orderAction = getOrderAction();
        sb.append(operatorId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(itemComponentId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(itemVersion)
                .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(itemType).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(agentCode)
                .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(uan).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(masterAccountNumber)
                .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(subscriptionCode).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(refSubscriptionCode)
                .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(xRefSubscriptionCode).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billCode)
                .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(action).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billActionQualifier)
                .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billQuantity).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(DateUtils.formatDateWithPattern(billEffectDate, Utils.DATE_TIME_PATTERN))
                .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(serviceType).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(offerTemplateCode)
                .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(operatorOrderNumber).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(offerTemplateId)
                .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(subscriptionId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billingAccountCode)
                .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(orderAction);
        return sb.toString();
    }

    /**
     * Gets the original order action.
     *
     * @return the original order action
     */
    public String getOriginalOrderAction() {
        String action = billAction;
        if ("COS".equalsIgnoreCase(action) || "TOS".equalsIgnoreCase(action)) {
            action = "C";
        }
        if ("CNS".equalsIgnoreCase(action) || "TNS".equalsIgnoreCase(action)) {
            action = "P";
        }
        return action;
    }

    public String getOrderAction() {

        switch (billAction) {
            case "P":
                return "PROVIDE";
            case "C":
                return "CEASE";
            case "CN":
                return "CHANGE NUMBER";
            case "CU":
                return "CHANGE UAN";
            case "COS":// Change
            case "CNS":
                return "CHANGE";
            case "TOS":// Transfer
            case "TNS":
                return "TRANSFER";
            case "RB":
                return "REBATE";
            case "M":
                return "MISC";
            case "SN": // Swap
                return "SWAP";
            case "CH": // Change class of service
                return "CHANGE";
            default:
                return "";
        }
    }

    /**
     * Get the order line attribute by code
     *
     * @param code the attribute code
     * @return the order line attribute by code
     */
    private AttributeDto getAttributeDto(String code) {
        if (attributesMap == null) {
            attributesMap = new HashMap<>();
            for (AttributeDto attributeDto : attributes) {
                if (attributeDto != null && !StringUtils.isBlank(attributeDto.getCode())) {
                    attributesMap.put(attributeDto.getCode(), attributeDto);
                }
            }
        }
        return attributesMap.get(code);
    }

    /**
     * Get the order line attribute by code
     *
     * @param code the attribute code
     * @return the order line attribute by code
     */
    public String getAttribute(String code) {
        AttributeDto attributeDto = getAttributeDto(code);
        if (attributeDto != null) {
            return attributeDto.getValue();
        }
        return null;
    }

    /**
     * Get the order line unit attribute by code
     *
     * @param code the attribute code
     * @return the order line unit attribute by code
     */
    public String getUnitAttribute(String code) {
        AttributeDto attributeDto = getAttributeDto(code);
        if (attributeDto != null) {
            return attributeDto.getUnit();
        }
        return null;
    }

    /**
     * Set the order line attribute by code
     *
     * @param code  the attribute code
     * @param value the attribute value
     */
    public void setAttribute(String code, String value) {
        if (attributesMap == null) {
            attributesMap = new HashMap<>();
        }
        AttributeDto attributeDto = attributesMap.get(code);
        if (attributeDto == null) {
            attributeDto = new AttributeDto();
        }
        attributeDto.setValue(value);
        attributesMap.put(code, attributeDto);
    }

    /**
     * Check if the main service
     *
     * @return true if is the main service
     */
    public boolean isMain() {
        if (ServiceTypeEnum.MAIN.getLabel().equalsIgnoreCase(serviceType)) {
            return true;
        }
        return false;
    }

    /**
     * Check if the ancillary service
     *
     * @return true if is the ancillary service
     */
    public boolean isAncillary() {
        if (ServiceTypeEnum.ANCILLARY.getLabel().equalsIgnoreCase(serviceType)) {
            return true;
        }
        return false;
    }

    /**
     * Gets the itemId
     *
     * @return the itemId
     */
    public String getItemId() {
        return itemId;
    }

    /**
     * Sets the itemId.
     *
     * @param itemId the itemId
     */
    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    /**
     * Gets the operatorId
     *
     * @return the operatorId
     */
    public String getOperatorId() {
        return operatorId;
    }

    /**
     * Sets the operatorId.
     *
     * @param operatorId the operatorId
     */
    public void setOperatorId(String operatorId) {
        this.operatorId = operatorId;
    }

    /**
     * Gets the itemComponentId
     *
     * @return the itemComponentId
     */
    public String getItemComponentId() {
        return itemComponentId;
    }

    /**
     * Sets the itemComponentId.
     *
     * @param itemComponentId the itemComponentId
     */
    public void setItemComponentId(String itemComponentId) {
        this.itemComponentId = itemComponentId;
    }

    /**
     * Gets the itemVersion
     *
     * @return the itemVersion
     */
    public String getItemVersion() {
        return itemVersion;
    }

    /**
     * Sets the itemVersion.
     *
     * @param itemVersion the itemVersion
     */
    public void setItemVersion(String itemVersion) {
        this.itemVersion = itemVersion;
    }

    /**
     * Gets the itemType
     *
     * @return the itemType
     */
    public String getItemType() {
        return itemType;
    }

    /**
     * Sets the itemType.
     *
     * @param itemType the itemType
     */
    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    /**
     * Gets the agentCode
     *
     * @return the agentCode
     */
    public String getAgentCode() {
        return agentCode;
    }

    /**
     * Sets the agentCode.
     *
     * @param agentCode the agentCode
     */
    public void setAgentCode(String agentCode) {
        this.agentCode = agentCode;
    }

    /**
     * Gets the uan
     *
     * @return the uan
     */
    public String getUan() {
        return uan;
    }

    /**
     * Sets the uan.
     *
     * @param uan the uan
     */
    public void setUan(String uan) {
        this.uan = uan;
    }

    /**
     * Gets the masterAccountNumber
     *
     * @return the masterAccountNumber
     */
    public String getMasterAccountNumber() {
        return masterAccountNumber;
    }

    /**
     * Sets the masterAccountNumber.
     *
     * @param masterAccountNumber the masterAccountNumber
     */
    public void setMasterAccountNumber(String masterAccountNumber) {
        this.masterAccountNumber = masterAccountNumber;
    }

    /**
     * Gets the subscriptionCode
     *
     * @return the subscriptionCode
     */
    public String getSubscriptionCode() {
        return subscriptionCode;
    }

    /**
     * Sets the subscriptionCode.
     *
     * @param subscriptionCode the subscriptionCode
     */
    public void setSubscriptionCode(String subscriptionCode) {
        this.subscriptionCode = subscriptionCode;
    }

    /**
     * Gets the refSubscriptionCode
     *
     * @return the refSubscriptionCode
     */
    public String getRefSubscriptionCode() {
        return refSubscriptionCode;
    }

    /**
     * Sets the refSubscriptionCode.
     *
     * @param refSubscriptionCode the refSubscriptionCode
     */
    public void setRefSubscriptionCode(String refSubscriptionCode) {
        this.refSubscriptionCode = refSubscriptionCode;
    }

    /**
     * Gets the xRefSubscriptionCode
     *
     * @return the xRefSubscriptionCode
     */
    public String getXRefSubscriptionCode() {
        return xRefSubscriptionCode;
    }

    /**
     * Sets the xRefSubscriptionCode.
     *
     * @param xRefSubscriptionCode the xRefSubscriptionCode
     */
    public void setXRefSubscriptionCode(String xRefSubscriptionCode) {
        this.xRefSubscriptionCode = xRefSubscriptionCode;
    }

    /**
     * Gets the billCode
     *
     * @return the billCode
     */
    public String getBillCode() {
        return billCode;
    }

    /**
     * Sets the billCode.
     *
     * @param billCode the billCode
     */
    public void setBillCode(String billCode) {
        this.billCode = billCode;
    }

    /**
     * Gets the billAction
     *
     * @return the billAction
     */
    public String getBillAction() {
        return billAction;
    }

    /**
     * Sets the billAction.
     *
     * @param billAction the billAction
     */
    public void setBillAction(String billAction) {
        this.billAction = billAction;
    }

    /**
     * Gets the billActionQualifier
     *
     * @return the billActionQualifier
     */
    public String getBillActionQualifier() {
        return billActionQualifier;
    }

    /**
     * Sets the billActionQualifier.
     *
     * @param billActionQualifier the billActionQualifier
     */
    public void setBillActionQualifier(String billActionQualifier) {
        this.billActionQualifier = billActionQualifier;
    }

    /**
     * Gets the billQuantity
     *
     * @return the billQuantity
     */
    public String getBillQuantity() {
        return billQuantity;
    }

    /**
     * Sets the billQuantity.
     *
     * @param billQuantity the billQuantity
     */
    public void setBillQuantity(String billQuantity) {
        this.billQuantity = billQuantity;
    }

    /**
     * Gets the billEffectDate
     *
     * @return the billEffectDate
     */
    public Date getBillEffectDate() {
        return billEffectDate;
    }

    /**
     * Sets the billEffectDate.
     *
     * @param billEffectDate the billEffectDate
     */
    public void setBillEffectDate(Date billEffectDate) {
        this.billEffectDate = billEffectDate;
    }

    /**
     * Gets the specialConnect
     *
     * @return the specialConnect
     */
    public String getSpecialConnect() {
        return specialConnect;
    }

    /**
     * Sets the specialConnect.
     *
     * @param specialConnect the specialConnect
     */
    public void setSpecialConnect(String specialConnect) {
        this.specialConnect = specialConnect;
    }

    /**
     * Gets the specialRental
     *
     * @return the specialRental
     */
    public String getSpecialRental() {
        return specialRental;
    }

    /**
     * Sets the specialRental.
     *
     * @param specialRental the specialRental
     */
    public void setSpecialRental(String specialRental) {
        this.specialRental = specialRental;
    }

    /**
     * Gets the operatorOrderNumber
     *
     * @return the operatorOrderNumber
     */
    public String getOperatorOrderNumber() {
        return operatorOrderNumber;
    }

    /**
     * Sets the operatorOrderNumber.
     *
     * @param operatorOrderNumber the operatorOrderNumber
     */
    public void setOperatorOrderNumber(String operatorOrderNumber) {
        this.operatorOrderNumber = operatorOrderNumber;
    }

    /**
     * Gets the mainServiceBillCode
     *
     * @return the mainServiceBillCode
     */
    public String getMainServiceBillCode() {
        return mainServiceBillCode;
    }

    /**
     * Sets the mainServiceBillCode.
     *
     * @param mainServiceBillCode the mainServiceBillCode
     */
    public void setMainServiceBillCode(String mainServiceBillCode) {
        this.mainServiceBillCode = mainServiceBillCode;
    }

    /**
     * Gets the attributes
     *
     * @return the attributes
     */
    public List<AttributeDto> getAttributes() {
        return attributes;
    }

    /**
     * Sets the attributes.
     *
     * @param attributes the attributes
     */
    public void setAttributes(List<AttributeDto> attributes) {
        this.attributes = attributes;
    }

    /**
     * Gets the offerTemplateCode
     *
     * @return the offerTemplateCode
     */
    public String getOfferTemplateCode() {
        return offerTemplateCode;
    }

    /**
     * Sets the offerTemplateCode.
     *
     * @param offerTemplateCode the offerTemplateCode
     */
    public void setOfferTemplateCode(String offerTemplateCode) {
        this.offerTemplateCode = offerTemplateCode;
    }

    /**
     * Gets the billingAccountCode
     *
     * @return the billingAccountCode
     */
    public String getBillingAccountCode() {
        return billingAccountCode;
    }

    /**
     * Sets the billingAccountCode.
     *
     * @param billingAccountCode the billingAccountCode
     */
    public void setBillingAccountCode(String billingAccountCode) {
        this.billingAccountCode = billingAccountCode;
    }

    /**
     * Gets the serviceType
     *
     * @return the serviceType
     */
    public String getServiceType() {
        return serviceType;
    }

    /**
     * Sets the serviceType.
     *
     * @param serviceType the serviceType
     */
    public void setServiceType(String serviceType) {
        this.serviceType = serviceType;
    }

    /**
     * Gets the migrationCode
     *
     * @return the migrationCode
     */
    public String getMigrationCode() {
        return migrationCode;
    }

    /**
     * Sets the migrationCode.
     *
     * @param migrationCode the migrationCode
     */
    public void setMigrationCode(String migrationCode) {
        this.migrationCode = migrationCode;
    }

    /**
     * Gets the ceaseEffectDate
     *
     * @return the ceaseEffectDate
     */
    public Date getCeaseEffectDate() {
        return ceaseEffectDate;
    }

    /**
     * Sets the ceaseEffectDate.
     *
     * @param ceaseEffectDate the ceaseEffectDate
     */
    public void setCeaseEffectDate(Date ceaseEffectDate) {
        this.ceaseEffectDate = ceaseEffectDate;
    }

    /**
     * Gets the rentalLiabilityDate
     *
     * @return the rentalLiabilityDate
     */
    public Date getRentalLiabilityDate() {
        return rentalLiabilityDate;
    }

    /**
     * Sets the rentalLiabilityDate.
     *
     * @param rentalLiabilityDate the rentalLiabilityDate
     */
    public void setRentalLiabilityDate(Date rentalLiabilityDate) {
        this.rentalLiabilityDate = rentalLiabilityDate;
    }

    /**
     * Gets the slaPremiumLiabilityDate
     *
     * @return the slaPremiumLiabilityDate
     */
    public Date getSlaPremiumLiabilityDate() {
        return slaPremiumLiabilityDate;
    }

    /**
     * Sets the slaPremiumLiabilityDate.
     *
     * @param slaPremiumLiabilityDate the slaPremiumLiabilityDate
     */
    public void setSlaPremiumLiabilityDate(Date slaPremiumLiabilityDate) {
        this.slaPremiumLiabilityDate = slaPremiumLiabilityDate;
    }

    /**
     * Gets the originalSubscriptionCode
     *
     * @return the originalSubscriptionCode
     */
    public String getOriginalSubscriptionCode() {
        return originalSubscriptionCode;
    }

    /**
     * Sets the originalSubscriptionCode.
     *
     * @param originalSubscriptionCode the originalSubscriptionCode
     */
    public void setOriginalSubscriptionCode(String originalSubscriptionCode) {
        this.originalSubscriptionCode = originalSubscriptionCode;
    }

    /**
     * Gets the originalBillCode
     *
     * @return the originalBillCode
     */
    public String getOriginalBillCode() {
        return originalBillCode;
    }

    /**
     * Sets the originalBillCode.
     *
     * @param originalBillCode the originalBillCode
     */
    public void setOriginalBillCode(String originalBillCode) {
        this.originalBillCode = originalBillCode;
    }

    /**
     * Gets the chargeType
     *
     * @return the chargeType
     */
    public String getChargeType() {
        return chargeType;
    }

    /**
     * Sets the chargeType.
     *
     * @param chargeType the chargeType
     */
    public void setChargeType(String chargeType) {
        this.chargeType = chargeType;
    }

    /**
     * Gets the deltaQuantity
     *
     * @return the deltaQuantity
     */
    public BigDecimal getDeltaQuantity() {
        return deltaQuantity;
    }

    /**
     * Sets the deltaQuantity.
     *
     * @param deltaQuantity the deltaQuantity
     */
    public void setDeltaQuantity(BigDecimal deltaQuantity) {
        this.deltaQuantity = deltaQuantity;
    }

    /**
     * Gets the refOrderLine
     *
     * @return the refOrderLine
     */
    public OrderLineDto getRefOrderLine() {
        return refOrderLine;
    }

    /**
     * Sets the refOrderLine.
     *
     * @param refOrderLine the refOrderLine
     */
    public void setRefOrderLine(OrderLineDto refOrderLine) {
        this.refOrderLine = refOrderLine;
    }

    /**
     * Gets the serviceInstanceId
     *
     * @return the serviceInstanceId
     */
    public Long getServiceInstanceId() {
        return serviceInstanceId;
    }

    /**
     * Sets the serviceInstanceId.
     *
     * @param serviceInstanceId the serviceInstanceId
     */
    public void setServiceInstanceId(Long serviceInstanceId) {
        this.serviceInstanceId = serviceInstanceId;
    }

    /**
     * Gets the offerTemplateId
     *
     * @return the offerTemplateId
     */
    public Long getOfferTemplateId() {
        return offerTemplateId;
    }

    /**
     * Sets the offerTemplateId.
     *
     * @param offerTemplateId the offerTemplateId
     */
    public void setOfferTemplateId(Long offerTemplateId) {
        this.offerTemplateId = offerTemplateId;
    }

    /**
     * Gets the subscriptionId
     *
     * @return the subscriptionId
     */
    public Long getSubscriptionId() {
        return subscriptionId;
    }

    /**
     * Sets the subscriptionId.
     *
     * @param subscriptionId the subscriptionId
     */
    public void setSubscriptionId(Long subscriptionId) {
        this.subscriptionId = subscriptionId;
    }

    /**
     * Gets the serviceInstanceIdentifierAttributes
     *
     * @return the serviceInstanceIdentifierAttributes
     */
    public ServiceInstanceIdentifierAttributesCFsEnum getServiceInstanceIdentifierAttributes() {
        return serviceInstanceIdentifierAttributes;
    }

    /**
     * Sets the serviceInstanceIdentifierAttributes.
     *
     * @param serviceInstanceIdentifierAttributes the serviceInstanceIdentifierAttributes
     */
    public void setServiceInstanceIdentifierAttributes(ServiceInstanceIdentifierAttributesCFsEnum serviceInstanceIdentifierAttributes) {
        this.serviceInstanceIdentifierAttributes = serviceInstanceIdentifierAttributes;
    }
}
