package org.meveo.api.rest.eir;

import com.eir.commons.enums.ApplicationPropertiesEnum;
import org.meveo.api.commons.Utils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.meveo.admin.exception.BusinessException;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.zip.GZIPOutputStream;

/**
 * The REST Web service file class.
 *
 * @author Abdellatif BARI
 */
public class FileRs extends EirRs {

    /**
     * Get File response.
     *
     * @param file the file.
     * @return File response
     */
    public Response getFile(File file) {
        String downloadMaxFileSize = ApplicationPropertiesEnum.DOWNLOAD_MAX_FILE_SIZE_MEGA_BYTE.getProperty();
        if (Utils.isDouble(downloadMaxFileSize)) {
            double fileSizeMB = (double) file.length() / (1024 * 1024);
            if (fileSizeMB > Double.parseDouble(downloadMaxFileSize)) {
                String zipFileName = FilenameUtils.removeExtension(file.getName()) + ".zip";
                //return buildSuccessResponse(fileApi.zipFile(file, zipFileName), zipFileName);
                return buildSuccessResponse(zipFile(file), zipFileName);
            }
        }
        return buildSuccessResponse(file);
    }

    /**
     * Zip file in memory
     *
     * @param file file.
     * @return the zip of input file.
     * @throws BusinessException business exception
     */
    public StreamingOutput zipFile(File file) throws BusinessException {
        return output -> {
            try {
                byte[] bytes = IOUtils.toByteArray(new FileInputStream(file.getParent() + File.separator + file.getName()));
                try (ByteArrayOutputStream bos = new ByteArrayOutputStream();
                     GZIPOutputStream out = new GZIPOutputStream(bos);) {
                    out.write(bytes);
                    out.finish();
                    byte[] compressed = bos.toByteArray();
                    output.write(compressed);
                }
            } catch (IOException ex) {
                throw new BusinessException("Error during zip file " + file.getName() + " : " + ex.getMessage());
            }
        };
    }

}