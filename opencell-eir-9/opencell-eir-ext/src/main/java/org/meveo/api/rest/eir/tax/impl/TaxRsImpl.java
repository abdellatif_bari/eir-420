package org.meveo.api.rest.eir.tax.impl;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.core.Response;

import org.meveo.api.dto.response.PagingAndFiltering;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.eir.EirRs;
import org.meveo.api.rest.eir.tax.TaxRs;

import com.eir.api.taxMapping.TaxMapApi;

/**
 * REST Web service implementation for tax mapping.
 *
 * @author Hatim OUDAD
 */
@RequestScoped
@Interceptors({ WsRestApiInterceptor.class })
public class TaxRsImpl extends EirRs implements TaxRs {
	
	@Inject
    private TaxMapApi taxMapApi;

    @Override
    public Response activateTaxMapping(Long fileId) {
        Response response = null;
        try {
            taxMapApi.activateTaxMapping(fileId);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    @Override
    public Response cancelTaxMapping(Long fileId) {
        Response response = null;
        try {
        	taxMapApi.cancelTaxMapping(fileId);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    @Override
    public Response exportTaxMapping(PagingAndFiltering pagingAndFiltering) {
        Response response = null;
        try {
            taxMapApi.exportTaxMapping(pagingAndFiltering);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }
    
    @Override
    public Response validateTaxMapping(Long fileId) {
        Response response = null;
        try {
            taxMapApi.validateTaxMapping(fileId);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }
}
