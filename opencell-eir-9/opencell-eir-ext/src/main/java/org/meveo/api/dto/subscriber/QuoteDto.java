package org.meveo.api.dto.subscriber;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class QuoteDto implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -5857852211374966036L;

	/**
     * Value of connection amount
     */
    @XmlElement(name = "Connection_Amount", required = true)
    private String connectionAmount;
    
    /**
     * Value of Annual Rental   
     */
    @XmlElement(name = "Annual_Rental", required = true)
    private String annualRental;

	public String getConnectionAmount() {
		return connectionAmount;
	}

	public void setConnectionAmount(String connectionAmount) {
		this.connectionAmount = connectionAmount;
	}

	public String getAnnualRental() {
		return annualRental;
	}

	public void setAnnualRental(String annualRental) {
		this.annualRental = annualRental;
	}

}
