package org.meveo.api.commons;

import com.eir.commons.utils.HashGenerationException;
import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import java.io.StringReader;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

/**
 * EIR class utils
 *
 * @author Abdellatif BARI
 */
public class Utils {

    private static final Logger log = LoggerFactory.getLogger(Utils.class);

    /**
     * Date time pattern
     */
    public static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

    /**
     * Date default pattern
     */
    public static final String DATE_DEFAULT_PATTERN = "dd/MM/yyyy";

    /**
     * Postgres Max number of parameters
     */
    public static final int MAX_NUMBER_JDBC_PSG_PARAMETERS = 32767;

    /**
     * Generate MD5
     *
     * @param message the message to be hashed
     * @return the hashed message
     * @throws HashGenerationException the Hash generation exception
     */
    public static String generateMD5(String message) throws HashGenerationException {
        return hashString(message, "MD5");
    }

    /**
     * Hash the string with the provided algorithm
     *
     * @param message   the message to be hashed
     * @param algorithm the name of the algorithm requested
     * @return the hash of provided string
     * @throws HashGenerationException the hash generation exception
     */
    private static String hashString(String message, String algorithm) throws HashGenerationException {

        try {
            MessageDigest digest = MessageDigest.getInstance(algorithm);
            byte[] hashedBytes = digest.digest(message.getBytes(StandardCharsets.UTF_8));

            return convertArrayBytesToHexString(hashedBytes);
        } catch (NoSuchAlgorithmException ex) {
            throw new HashGenerationException("Could not generate hash from String", ex);
        }
    }

    /**
     * Convert array bytes to hex string
     *
     * @param arrayBytes the array bytes
     * @return the hex string
     */
    private static String convertArrayBytesToHexString(byte[] arrayBytes) {
        StringBuffer stringBuffer = new StringBuffer();
        for (int i = 0; i < arrayBytes.length; i++) {
            stringBuffer.append(Integer.toString((arrayBytes[i] & 0xff) + 0x100, 16).substring(1));
        }
        return stringBuffer.toString();
    }

    /**
     * Get XML from Dto.
     *
     * @param object the object to be converted to xml
     * @param <T>    the generic type of object to be converted to xml
     * @return the xml
     * @throws BusinessException the business exception
     */
    @SuppressWarnings("unchecked")
    public static <T> String getXmlFromDto(T object) throws BusinessException {
        String xmlContent = null;
        try {
            if (object != null) {
                // Create JAXB Context
                JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());

                // Create Marshaller
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

                // Required formatting??
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

                // Print XML String to Console
                StringWriter stringWriter = new StringWriter();

                // Write XML to StringWriter
                jaxbMarshaller.marshal(object, stringWriter);

                // get XML Content
                xmlContent = stringWriter.toString();
            }
        } catch (JAXBException e) {
            log.error("Convert Dto to xml fail", e);
            throw new BusinessException(e.getMessage(), e);
        }
        return xmlContent;
    }

    /**
     * Get Dto from xml
     *
     * @param xml       the input xml
     * @param jaxbClass the Dto class to which the XML will be converted
     * @param xmlSchema the xml schema
     * @param <T>       the class type
     * @return the Dto object
     * @throws BusinessException the business exception
     */
    public static <T> T getDtoFromXml(String xml, Class<T> jaxbClass, String xmlSchema) throws BusinessException {
        T dto = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(jaxbClass);
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Unmarshaller jaxbUnmarshaller = null;
            jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            if (!StringUtils.isBlank(xmlSchema)) {
                Schema Schema = schemaFactory.newSchema(new SAXSource(new InputSource(new StringReader(xmlSchema))));
                // jaxbUnmarshaller.setSchema(Schema);
            }
            dto = (T) jaxbUnmarshaller.unmarshal(new StringReader(xml));
        } catch (JAXBException | SAXException e) {
            log.error("Convert xml to Dto fail", e);
            throw new BusinessException(e.getMessage(), e);
        }
        return dto;
    }

    /**
     * Add the provided minutes to date
     *
     * @param date    the date
     * @param minutes the minutes to add to date
     * @return the result date
     */
    public static Date addMinutesToDate(Date date, Integer minutes) {
        Date result = null;

        if (date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MINUTE, minutes);
            result = calendar.getTime();
        }

        return result;
    }

    /**
     * Add the provided seconds to date
     *
     * @param date    the date
     * @param seconds the seconds to add to date
     * @return the result date
     */
    public static Date addSecondsToDate(Date date, Integer seconds) {
        Date result = null;

        if (date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.SECOND, seconds);
            result = calendar.getTime();
        }

        return result;
    }

    /**
     * Check number is double
     *
     * @param strNum the string number
     * @return true is the string is double.
     */
    public static boolean isDouble(String strNum) {
        if (StringUtils.isBlank(strNum)) {
            return false;
        }
        try {
            Double.parseDouble(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * Check number is integer
     *
     * @param strNum the string number
     * @return true is the string is integer.
     */
    public static boolean isInteger(String strNum) {
        if (StringUtils.isBlank(strNum)) {
            return false;
        }
        try {
            Integer.parseInt(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * Check string is a Long
     *
     * @param strNum the string number
     * @return true is the string is a Long.
     */
    public static boolean isLong(String strNum) {
        if (StringUtils.isBlank(strNum)) {
            return false;
        }
        try {
            Long.parseLong(strNum);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     * Convert String to long value (possibility to have null as default value)
     *
     * @param str string to be converted
     * @return the long value
     */
    public static Long toLong(String str) {
        return toLong(str, null);
    }

    /**
     * Convert String to long value (possibility to have null as default value)
     *
     * @param str          string to be converted
     * @param defaultValue the default value
     * @return the long value
     */
    public static Long toLong(String str, Long defaultValue) {
        if (StringUtils.isBlank(str)) {
            return defaultValue;
        } else {
            try {
                return Long.parseLong(str.trim());
            } catch (NumberFormatException ne) {
                return defaultValue;
            }
        }
    }

    /**
     * Convert String to integer value (possibility to have null as default value)
     *
     * @param str string to be converted
     * @return the integer value
     */
    public static Integer toInteger(String str) {
        return toInteger(str, null);
    }

    /**
     * Convert String to integer value (possibility to have null as default value)
     *
     * @param str          string to be converted
     * @param defaultValue the default value
     * @return the integer value
     */
    public static Integer toInteger(String str, Integer defaultValue) {
        if (StringUtils.isBlank(str)) {
            return defaultValue;
        } else {
            try {
                return Integer.parseInt(str.trim());
            } catch (NumberFormatException ne) {
                return defaultValue;
            }
        }
    }

    /**
     * Replaces pipe character substring with the backslash
     *
     * @param value this string value whose pipe character will be replaced
     * @returnthe result string
     */
    public static String escapePipeCharacter(String value) {
        if (!StringUtils.isBlank(value)) {
            value = value.replaceAll(Pattern.quote("|"), "&#124;");
        }
        return value;
    }

    /**
     * Replaces backslash character substring with the pipe
     *
     * @param value this string value whose pipe character will be replaced
     * @returnthe result string
     */
    public static String retrievePipeCharacter(String value) {
        if (!StringUtils.isBlank(value)) {
            value = value.replaceAll(Pattern.quote("&#124;"), "|");
        }
        return value;
    }

}
