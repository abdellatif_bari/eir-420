package org.meveo.api.commons;

import javax.ws.rs.core.Response;

/**
 * Define the error codes for response of the EIR API web service.
 *
 * @author Abdellatif BARI
 **/

public enum ErrorEnum {

    /**
     * Bad Request
     */
    BAD_REQUEST("BAD_REQUEST", Response.Status.BAD_REQUEST),

    /**
     * Bad Request : Missing a required parameter or field value
     */
    BAD_REQUEST_MISSING_PARAMETER("MISSING_PARAMETER", Response.Status.BAD_REQUEST),

    /**
     * Bad Request : invalid value
     */
    BAD_REQUEST_INVALID_VALUE("INVALID_VALUE", Response.Status.BAD_REQUEST),

    /**
     * Bad Request : invalid size
     */
    BAD_REQUEST_INVALID_SIZE("INVALID_SIZE", Response.Status.BAD_REQUEST),

    /**
     * Conflict : Indicates that the request could not be processed because of conflict in the current state of the resource, such as an edit conflict between multiple simultaneous
     * updates.
     */
    ORDER_ALREADY_EXISTS("ORDER_ALREADY_EXISTS", Response.Status.CONFLICT),

    /**
     * This error condition may occur if an XML request body contains well-formed (i.e., syntactically correct), but semantically erroneous, XML instructions. the code of this
     * error is 422 but in the case of eir we still create the order successfully with 200 status but in error.
     */
    INVALID_DATA("INVALID_DATA", Response.Status.OK),

    /**
     * Request Entity Too Large : order content too large
     */
    ORDER_TOO_LARGE("PAYLOAD_TOO_LARGE", Response.Status.REQUEST_ENTITY_TOO_LARGE),

    /**
     * A general exception encountered
     */
    GENERIC_API_EXCEPTION("GENERIC_API_EXCEPTION", Response.Status.INTERNAL_SERVER_ERROR),

    /**
     * Not Found
     */
    NOT_FOUND("NOT_FOUND", Response.Status.NOT_FOUND);

    private String code;
    private Response.Status status;

    ErrorEnum(String code, Response.Status status) {
        this.code = code;
        this.status = status;
    }

    public String getCode() {
        return this.code;
    }

    public Response.Status getStatus() {
        return this.status;
    }
}
