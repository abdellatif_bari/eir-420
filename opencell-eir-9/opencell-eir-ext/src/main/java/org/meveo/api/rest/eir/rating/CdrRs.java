package org.meveo.api.rest.eir.rating;

import org.meveo.api.dto.response.PagingAndFiltering;
import org.meveo.api.rest.IBaseRs;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * REST Web service interface for CDR managing.
 *
 * @author Abdellatif BARI
 */
@Path("/cdr")
@Consumes({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
@Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
public interface CdrRs extends IBaseRs {


    /**
     * Validate CDRs in a staging table
     *
     * @param fileId The id of a CDR import file
     * @return The validate response
     */
    @POST
    @Path("/validate/{fileId}")
    Response validateCdr(@PathParam("fileId") Long fileId);

    /**
     * Activate CDRs in a staging table
     *
     * @param fileId The id of a CDR import file
     * @return The activate response
     */
    @POST
    @Path("/activate/{fileId}")
    Response activateCdr(@PathParam("fileId") Long fileId);

    /**
     * Cancel CDRs in a staging table
     *
     * @param fileId The id of a CDR import file
     * @return Tha cancel response
     */
    @POST
    @Path("/cancel/{fileId}")
    Response cancelCdr(@PathParam("fileId") Long fileId);

    /**
     * Export CDR
     *
     * @param pagingAndFiltering Search and paging configuration
     * @return The export response
     */
    @POST
    @Path("/export")
    Response exportCdr(PagingAndFiltering pagingAndFiltering);

}