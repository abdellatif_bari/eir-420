package org.meveo.api.dto.billing;

import org.meveo.api.dto.BaseDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Billing account details response Dto
 *
 * @author Abdellatif BARI
 */
@XmlRootElement(name = "Retrieve_Billing_Account_Details_Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class BillingAccountDetailsResponseDto extends BaseDto {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -8817004375961651528L;

    /**
     * The customer details.
     */
    @XmlElement(name = "Customer_Details", required = true)
    CustomerDetailsDto customerDetails;

    /**
     * Gets the customerDetails
     *
     * @return the customerDetails
     */
    public CustomerDetailsDto getCustomerDetails() {
        return customerDetails;
    }

    /**
     * Sets the customerDetails.
     *
     * @param customerDetails the new customerDetails
     */
    public void setCustomerDetails(CustomerDetailsDto customerDetails) {
        this.customerDetails = customerDetails;
    }
}
