package org.meveo.api.dto.order;

import org.meveo.commons.utils.StringUtils;

import java.io.Serializable;
import java.util.Map;

/**
 * Complimentary order Dto
 *
 * @author Abdellatif BARI
 */
public class ComplimentaryOrderDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 3527149084229998498L;

    /**
     * The ref order.
     */
    String refOrderCode;

    /**
     * Subscription main details item.
     */
    Map<String, String> mainItem;

    /**
     * Get subscription main details attribute ref order
     *
     * @return subscription main details attribute ref order
     */
    public String getMainItemElement(String elementName) {
        if (!StringUtils.isBlank(refOrderCode) && mainItem != null && !mainItem.isEmpty()) {
            return mainItem.get(elementName);
        }
        return null;
    }

    /**
     * Gets the refOrderCode
     *
     * @return the refOrderCode
     */
    public String getRefOrderCode() {
        return refOrderCode;
    }

    /**
     * Sets the refOrderCode.
     *
     * @param refOrderCode the refOrderCode
     */
    public void setRefOrderCode(String refOrderCode) {
        this.refOrderCode = refOrderCode;
    }

    /**
     * Gets the mainItem
     *
     * @return the mainItem
     */
    public Map<String, String> getMainItem() {
        return mainItem;
    }

    /**
     * Sets the mainItem.
     *
     * @param mainItem the new mainItem
     */
    public void setMainItem(Map<String, String> mainItem) {
        this.mainItem = mainItem;
    }
}
