package org.meveo.api.dto.order;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;

/**
 * County Dto
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class CountyDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -3323043462971078120L;

    /**
     * The county id.
     */
    @XmlElement(name = "County_Id")
    private String countyId;

    /**
     * The county name.
     */
    @XmlElement(name = "County_Name")
    private String countyName;

    /**
     * The town name.
     */
    @XmlElement(name = "Town_Name")
    private String townName;

    /**
     * The postal district.
     */
    @XmlElement(name = "Postal_District")
    private String postalDistrict;

    /**
     * Gets the countyId
     *
     * @return the countyId
     */
    public String getCountyId() {
        return countyId;
    }

    /**
     * Sets the countyId.
     *
     * @param countyId the new countyId
     */
    public void setCountyId(String countyId) {
        this.countyId = countyId;
    }

    /**
     * Gets the countyName
     *
     * @return the countyName
     */
    public String getCountyName() {
        return countyName;
    }

    /**
     * Sets the countyName.
     *
     * @param countyName the new countyName
     */
    public void setCountyName(String countyName) {
        this.countyName = countyName;
    }

    /**
     * Gets the townName
     *
     * @return the townName
     */
    public String getTownName() {
        return townName;
    }

    /**
     * Sets the townName.
     *
     * @param townName the new townName
     */
    public void setTownName(String townName) {
        this.townName = townName;
    }

    /**
     * Gets the postalDistrict
     *
     * @return the postalDistrict
     */
    public String getPostalDistrict() {
        return postalDistrict;
    }

    /**
     * Sets the postalDistrict.
     *
     * @param postalDistrict the new postalDistrict
     */
    public void setPostalDistrict(String postalDistrict) {
        this.postalDistrict = postalDistrict;
    }
}
