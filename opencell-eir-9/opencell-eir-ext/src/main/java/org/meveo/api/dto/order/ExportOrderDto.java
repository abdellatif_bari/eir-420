package org.meveo.api.dto.order;

import org.meveo.api.dto.BaseDto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Export order Dto
 *
 * @author abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class ExportOrderDto extends BaseDto {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 540150922389950763L;

    /**
     * The file type.
     */
    @XmlElement(required = true)
    String fileType;

    /**
     * The orders ids.
     */
    @XmlElement(required = true)
    List<String> orderIds = new ArrayList<>();

    /**
     * The file path
     */
    String filePath;

    /**
     * Gets the fileType
     *
     * @return the fileType
     */
    public String getFileType() {
        return fileType;
    }

    /**
     * Sets the fileType.
     *
     * @param fileType the new fileType
     */
    public void setFileType(String fileType) {
        this.fileType = fileType;
    }

    /**
     * Gets the orderIds
     *
     * @return the orderIds
     */
    public List<String> getOrderIds() {
        return orderIds;
    }

    /**
     * Sets the orderIds.
     *
     * @param orderIds the new orderIds
     */
    public void setOrderIds(List<String> orderIds) {
        this.orderIds = orderIds;
    }

    /**
     * Gets the filePath
     *
     * @return the filePath
     */
    public String getFilePath() {
        return filePath;
    }

    /**
     * Sets the filePath.
     *
     * @param filePath the filePath
     */
    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }
}