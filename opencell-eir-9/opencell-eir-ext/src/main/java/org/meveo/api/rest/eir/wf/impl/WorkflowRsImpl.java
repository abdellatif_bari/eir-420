package org.meveo.api.rest.eir.wf.impl;

import org.meveo.api.dto.wf.WorkflowResponseDto;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.eir.wf.WorkflowRs;
import org.meveo.api.rest.impl.BaseRs;
import org.meveo.api.wf.WorkflowApi;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

/**
 * REST Web service implementation for managing {@link org.meveo.model.wf.Workflow} ressource.
 *
 * @author Abdellatif BARI
 */
@RequestScoped
@Interceptors({WsRestApiInterceptor.class})
public class WorkflowRsImpl extends BaseRs implements WorkflowRs {

    @Inject
    private WorkflowApi workflowApi;

    public WorkflowResponseDto find(String workflowCode) {
        log.info("Call my simple RS ");
        WorkflowResponseDto result = new WorkflowResponseDto();
        try {
            result.setWorkflow(workflowApi.find(workflowCode));
        } catch (Exception e) {
            processException(e, result.getActionStatus());
        }
        return result;
    }
}