/*
 * (C) Copyright 2015-2020 Opencell SAS (https://opencellsoft.com/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
 * OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS
 * IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO
 * THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 *
 * For more information on the GNU Affero General Public License, please consult
 * <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

package org.meveo.api.dto.order;

import org.meveo.api.dto.BusinessEntityDto;
import org.meveo.api.dto.CustomFieldsDto;
import org.meveo.model.BusinessEntity;
import org.meveo.model.order.OrderStatusEnum;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Order response Dto
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class OrderResponseDto extends BusinessEntityDto {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -4817256013309129939L;

    /**
     * The order number.
     */
    private String orderNumber;

    /**
     * The order date.
     */
    private Date orderDate;

    /**
     * Completion priority
     */
    private Integer priority;

    /**
     * The status.
     */
    private OrderStatusEnum status;

    /**
     * The status message.
     */
    private String statusMessage;

    /**
     * The received from app.
     */
    private String receivedFromApp;

    /**
     * The electronic billing.
     */
    private boolean electronicBilling;

    /**
     * The custom fields.
     */
    private CustomFieldsDto cfValues;

    /**
     * Converts BusinessEntity JPA entity to DTO
     *
     * @param e Entity to convert
     */
    public OrderResponseDto(BusinessEntity e) {
        super(e);
    }

    /**
     * Gets the orderNumber
     *
     * @return the orderNumber
     */
    public String getOrderNumber() {
        return orderNumber;
    }

    /**
     * Sets the orderNumber.
     *
     * @param orderNumber the orderNumber
     */
    public void setOrderNumber(String orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * Gets the orderDate
     *
     * @return the orderDate
     */
    public Date getOrderDate() {
        return orderDate;
    }

    /**
     * Sets the orderDate.
     *
     * @param orderDate the orderDate
     */
    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * Gets the priority
     *
     * @return the priority
     */
    public Integer getPriority() {
        return priority;
    }

    /**
     * Sets the priority.
     *
     * @param priority the priority
     */
    public void setPriority(Integer priority) {
        this.priority = priority;
    }

    /**
     * Gets the status
     *
     * @return the status
     */
    public OrderStatusEnum getStatus() {
        return status;
    }

    /**
     * Sets the status.
     *
     * @param status the status
     */
    public void setStatus(OrderStatusEnum status) {
        this.status = status;
    }

    /**
     * Gets the statusMessage
     *
     * @return the statusMessage
     */
    public String getStatusMessage() {
        return statusMessage;
    }

    /**
     * Sets the statusMessage.
     *
     * @param statusMessage the statusMessage
     */
    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    /**
     * Gets the receivedFromApp
     *
     * @return the receivedFromApp
     */
    public String getReceivedFromApp() {
        return receivedFromApp;
    }

    /**
     * Sets the receivedFromApp.
     *
     * @param receivedFromApp the receivedFromApp
     */
    public void setReceivedFromApp(String receivedFromApp) {
        this.receivedFromApp = receivedFromApp;
    }

    /**
     * Gets the electronicBilling
     *
     * @return the electronicBilling
     */
    public boolean isElectronicBilling() {
        return electronicBilling;
    }

    /**
     * Sets the electronicBilling.
     *
     * @param electronicBilling the electronicBilling
     */
    public void setElectronicBilling(boolean electronicBilling) {
        this.electronicBilling = electronicBilling;
    }

    /**
     * Gets the cfValues
     *
     * @return the cfValues
     */
    public CustomFieldsDto getCfValues() {
        return cfValues;
    }

    /**
     * Sets the cfValues.
     *
     * @param cfValues the cfValues
     */
    public void setCfValues(CustomFieldsDto cfValues) {
        this.cfValues = cfValues;
    }
}