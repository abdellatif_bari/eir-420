package org.meveo.api.dto.note;

import javax.enterprise.context.RequestScoped;

import org.meveo.api.dto.BaseDto;

/**
 * Note Dto
 *
 * @author Mohammed Amine TAZI
 */

@RequestScoped
public class NoteDto extends BaseDto {

	private String code;
	private String type;
	private String comment;
	private String[] ids;
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getComment() {
		return comment;
	}
	public void setComment(String comment) {
		this.comment = comment;
	}	
	public String[] getIds() {
		return ids;
	}
	public void setIds(String[] ids) {
		this.ids = ids;
	}
}
