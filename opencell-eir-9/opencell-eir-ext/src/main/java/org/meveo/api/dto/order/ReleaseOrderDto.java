package org.meveo.api.dto.order;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.meveo.model.order.OrderStatusEnum;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Reprocess order Dto
 *
 * @author abdellatif BARI
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class ReleaseOrderDto extends BaseOrderDto {

    private static final long serialVersionUID = 5297742680682894133L;

    /**
     * The cancel reason code.
     */
    private String cancelReasonCode;

    /**
     * The cancel description.
     */
    private String cancelDescription;

    /**
     * The release description.
     */
    private String releaseDescription;

    /**
     * The ordering status.
     */
    private OrderStatusEnum releaseStatus;


    /**
     * Gets the cancelReasonCode
     *
     * @return the cancelReasonCode
     */
    public String getCancelReasonCode() {
        return cancelReasonCode;
    }

    /**
     * Sets the cancelReasonCode.
     *
     * @param cancelReasonCode the cancelReasonCode
     */
    public void setCancelReasonCode(String cancelReasonCode) {
        this.cancelReasonCode = cancelReasonCode;
    }

    /**
     * Gets the cancelDescription
     *
     * @return the cancelDescription
     */
    public String getCancelDescription() {
        return cancelDescription;
    }

    /**
     * Sets the cancelDescription.
     *
     * @param cancelDescription the cancelDescription
     */
    public void setCancelDescription(String cancelDescription) {
        this.cancelDescription = cancelDescription;
    }

    /**
     * Gets the releaseDescription
     *
     * @return the releaseDescription
     */
    public String getReleaseDescription() {
        return releaseDescription;
    }

    /**
     * Sets the releaseDescription.
     *
     * @param releaseDescription the releaseDescription
     */
    public void setReleaseDescription(String releaseDescription) {
        this.releaseDescription = releaseDescription;
    }

    /**
     * Gets the releaseStatus
     *
     * @return the releaseStatus
     */
    public OrderStatusEnum getReleaseStatus() {
        return releaseStatus;
    }

    /**
     * Sets the releaseStatus.
     *
     * @param releaseStatus the releaseStatus
     */
    public void setReleaseStatus(OrderStatusEnum releaseStatus) {
        this.releaseStatus = releaseStatus;
    }
}