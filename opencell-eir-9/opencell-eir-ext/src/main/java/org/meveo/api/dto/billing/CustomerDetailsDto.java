package org.meveo.api.dto.billing;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Customer details Dto
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class CustomerDetailsDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 167627751448068343L;

    /**
     * The customer number.
     */
    @XmlElement(name = "Customer_Number")
    String customerNumber;

    /**
     * The customer name.
     */
    @XmlElement(name = "Customer_Name")
    String customerName;

    /**
     * The customer address line 1.
     */
    @XmlElement(name = "Customer_Address_Line_1")
    String customerAddressLine1;

    /**
     * The customer address line 2.
     */
    @XmlElement(name = "Customer_Address_Line_2")
    String customerAddressLine2;

    /**
     * The customer address line 3.
     */
    @XmlElement(name = "Customer_Address_Line_3")
    String customerAddressLine3;

    /**
     * The customer address line 4.
     */
    @XmlElement(name = "Customer_Address_Line_4")
    String customerAddressLine4;

    /**
     * The customer address line 5.
     */
    @XmlElement(name = "Customer_Address_Line_5")
    String customerAddressLine5;

    /**
     * The billing accounts.
     */
    @XmlElement(name = "Billing_Accounts")
    BillingAccountsDto billingAccounts;

    /**
     * Gets the customerNumber
     *
     * @return the customerNumber
     */
    public String getCustomerNumber() {
        return customerNumber;
    }

    /**
     * Sets the customerNumber.
     *
     * @param customerNumber the new customerNumber
     */
    public void setCustomerNumber(String customerNumber) {
        this.customerNumber = customerNumber;
    }

    /**
     * Gets the customerName
     *
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * Sets the customerName.
     *
     * @param customerName the new customerName
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * Gets the customerAddressLine1
     *
     * @return the customerAddressLine1
     */
    public String getCustomerAddressLine1() {
        return customerAddressLine1;
    }

    /**
     * Sets the customerAddressLine1.
     *
     * @param customerAddressLine1 the new customerAddressLine1
     */
    public void setCustomerAddressLine1(String customerAddressLine1) {
        this.customerAddressLine1 = customerAddressLine1;
    }

    /**
     * Gets the customerAddressLine2
     *
     * @return the customerAddressLine2
     */
    public String getCustomerAddressLine2() {
        return customerAddressLine2;
    }

    /**
     * Sets the customerAddressLine2.
     *
     * @param customerAddressLine2 the new customerAddressLine2
     */
    public void setCustomerAddressLine2(String customerAddressLine2) {
        this.customerAddressLine2 = customerAddressLine2;
    }

    /**
     * Gets the customerAddressLine3
     *
     * @return the customerAddressLine3
     */
    public String getCustomerAddressLine3() {
        return customerAddressLine3;
    }

    /**
     * Sets the customerAddressLine3.
     *
     * @param customerAddressLine3 the new customerAddressLine3
     */
    public void setCustomerAddressLine3(String customerAddressLine3) {
        this.customerAddressLine3 = customerAddressLine3;
    }

    /**
     * Gets the customerAddressLine4
     *
     * @return the customerAddressLine4
     */
    public String getCustomerAddressLine4() {
        return customerAddressLine4;
    }

    /**
     * Sets the customerAddressLine4.
     *
     * @param customerAddressLine4 the new customerAddressLine4
     */
    public void setCustomerAddressLine4(String customerAddressLine4) {
        this.customerAddressLine4 = customerAddressLine4;
    }

    /**
     * Gets the customerAddressLine5
     *
     * @return the customerAddressLine5
     */
    public String getCustomerAddressLine5() {
        return customerAddressLine5;
    }

    /**
     * Sets the customerAddressLine5.
     *
     * @param customerAddressLine5 the new customerAddressLine5
     */
    public void setCustomerAddressLine5(String customerAddressLine5) {
        this.customerAddressLine5 = customerAddressLine5;
    }

    /**
     * Gets the billingAccounts
     *
     * @return the billingAccounts
     */
    public BillingAccountsDto getBillingAccounts() {
        return billingAccounts;
    }

    /**
     * Sets the billingAccounts.
     *
     * @param billingAccounts the new billingAccounts
     */
    public void setBillingAccounts(BillingAccountsDto billingAccounts) {
        this.billingAccounts = billingAccounts;
    }
}