package org.meveo.api.dto.flatfile;

import org.meveo.api.dto.response.PagingAndFiltering;

import java.io.Serializable;

/**
 * Export Dto
 *
 * @author Abdellatif BARI
 */
public class ExportDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 6751594909387921563L;

    /**
     * The file name.
     */
    private String fileName;

    /**
     * The file format code.
     */
    private String fileFormatCode;

    /**
     * True if the table to be exported is valid table and having a staging one.
     */
    private boolean exportValidTable;

    /**
     * Export type should be equal import type to be used to convert valid table to staging one.
     */
    private String exportType;

    /**
     * The paging and search criteria.
     */
    private PagingAndFiltering pagingAndFiltering;

    /**
     * Gets the fileName
     *
     * @return the fileName
     */
    public String getFileName() {
        return fileName;
    }

    /**
     * Sets the fileName.
     *
     * @param fileName the fileName
     */
    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    /**
     * Gets the fileFormatCode
     *
     * @return the fileFormatCode
     */
    public String getFileFormatCode() {
        return fileFormatCode;
    }

    /**
     * Sets the fileFormatCode.
     *
     * @param fileFormatCode the fileFormatCode
     */
    public void setFileFormatCode(String fileFormatCode) {
        this.fileFormatCode = fileFormatCode;
    }

    /**
     * Gets the exportValidTable
     *
     * @return the exportValidTable
     */
    public boolean isExportValidTable() {
        return exportValidTable;
    }

    /**
     * Sets the exportValidTable.
     *
     * @param exportValidTable the exportValidTable
     */
    public void setExportValidTable(boolean exportValidTable) {
        this.exportValidTable = exportValidTable;
    }

    /**
     * Gets the exportType
     *
     * @return the exportType
     */
    public String getExportType() {
        return exportType;
    }

    /**
     * Sets the exportType.
     *
     * @param exportType the exportType
     */
    public void setTransformationRulesType(String exportType) {
        this.exportType = exportType;
    }

    /**
     * Gets the pagingAndFiltering
     *
     * @return the pagingAndFiltering
     */
    public PagingAndFiltering getPagingAndFiltering() {
        return pagingAndFiltering;
    }

    /**
     * Sets the pagingAndFiltering.
     *
     * @param pagingAndFiltering the pagingAndFiltering
     */
    public void setPagingAndFiltering(PagingAndFiltering pagingAndFiltering) {
        this.pagingAndFiltering = pagingAndFiltering;
    }
}
