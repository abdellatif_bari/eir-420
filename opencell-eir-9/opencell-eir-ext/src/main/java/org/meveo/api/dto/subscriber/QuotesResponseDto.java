package org.meveo.api.dto.subscriber;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.meveo.api.dto.BaseDto;

/**
 * Quotes Response DTO
 *
 * @author Hatim OUDAD
 */
@XmlRootElement(name = "OC_Quote_Query_Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class QuotesResponseDto extends BaseDto {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = 1359700363762222207L;

    @XmlElement(name = "Error_Message")
    private String errorMessage;
    
    /**
     * Unique for quote request order in the custom table  
     */
    @XmlElement(name = "Transaction_Id", required = true)
    private String transactionId;
    
    /**
     * Identifier for upstream ordering system
     */
    @XmlElement(name = "Ordering_System", required = true)
    private String orderingSystem;
    
    /**
     * quote Order identifier
     */
    @XmlElement(name = "Quote_Query_Id", required = true)
    private String quoteQueryId;
    
    /**
     * Order number supplied by Operator
     */
    @XmlElement(name = "Order_Number")
    private String orderNumber;
    
    /**
     * Ordering System Identifier for a related service Order
     */
    @XmlElement(name = "Order_Id", required = true)
    private String orderId;
    
    @XmlElement(name= "Quote", required = true)
    private List <QuoteDto> quoteDtoList = new ArrayList<>();
    
        
   

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getQuoteQueryId() {
        return quoteQueryId;
    }

    public void setQuoteQueryId(String quoteQueryId) {
        this.quoteQueryId = quoteQueryId;
    }
    
    @Override
    public String getErrorMessage() {
        return errorMessage;
    }
    
    @Override
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

	public String getOrderingSystem() {
		return orderingSystem;
	}

	public void setOrderingSystem(String orderingSystem) {
		this.orderingSystem = orderingSystem;
	}

	public String getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}

	public String getOrderId() {
		return orderId;
	}

	public void setOrderId(String orderId) {
		this.orderId = orderId;
	}

	public List<QuoteDto> getQuoteDtoList() {
		return quoteDtoList;
	}

	public void setQuoteDtoList(List<QuoteDto> quoteDtoList) {
		this.quoteDtoList = quoteDtoList;
	}
    
    
}
