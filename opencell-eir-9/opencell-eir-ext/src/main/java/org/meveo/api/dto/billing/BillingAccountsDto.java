package org.meveo.api.dto.billing;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Billing accounts Dto
 *
 * @author abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class BillingAccountsDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 215109279715516207L;

    /**
     * The billing account details.
     */
    @XmlElement(name = "Billing_Account_Details")
    List<BillingAccountDetailDto> billingAccountDetails = new ArrayList<>();

    /**
     * Gets the billingAccountDetails
     *
     * @return the billingAccountDetails
     */
    public List<BillingAccountDetailDto> getBillingAccountDetails() {
        return billingAccountDetails;
    }

    /**
     * Sets the billingAccountDetails.
     *
     * @param billingAccountDetails the new billingAccountDetails
     */
    public void setBillingAccountDetails(List<BillingAccountDetailDto> billingAccountDetails) {
        this.billingAccountDetails = billingAccountDetails;
    }
}