package org.meveo.api.dto.billing;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Billing account detail Dto
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class BillingAccountDetailDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 155964440591070604L;

    /**
     * The billing account number.
     */
    @XmlElement(name = "Billing_Account_Number")
    String billingAccountNumber;

    /**
     * The billing account name.
     */
    @XmlElement(name = "Billing_Account_Name")
    String billingAccountName;

    /**
     * The billing account address line 1.
     */
    @XmlElement(name = "Billing_Account_Address_Line_1")
    String billingAccountAddressLine1;

    /**
     * The billing account address line 2.
     */
    @XmlElement(name = "Billing_Account_Address_Line_2")
    String billingAccountAddressLine2;

    /**
     * The billing account address line 3.
     */
    @XmlElement(name = "Billing_Account_Address_Line_3")
    String billingAccountAddressLine3;

    /**
     * The billing account address line 4.
     */
    @XmlElement(name = "Billing_Account_Address_Line_4")
    String billingAccountAddressLine4;

    /**
     * The billing account address line 5.
     */
    @XmlElement(name = "Billing_Account_Address_Line_5")
    String billingAccountAddressLine5;

    /**
     * The billing account service type.
     */
    @XmlElement(name = "Billing_Account_Service_Type")
    String billingAccountServiceType;

    /**
     * The billing account status.
     */
    @XmlElement(name = "Billing_Account_Status")
    String billingAccountStatus;

    /**
     * The billing account creation date.
     */
    @XmlElement(name = "Billing_Account_Creation_Date")
    String billingAccountCreationDate;

    /**
     * The billing account category.
     */
    @XmlElement(name = "Billing_Account_Category")
    String billingAccountCategory;

    /**
     * The billing account profile.
     */
    @XmlElement(name = "Billing_Account_Profile")
    String billingAccountProfile;

    /**
     * Gets the billingAccountNumber
     *
     * @return the billingAccountNumber
     */
    public String getBillingAccountNumber() {
        return billingAccountNumber;
    }

    /**
     * Sets the billingAccountNumber.
     *
     * @param billingAccountNumber the new billingAccountNumber
     */
    public void setBillingAccountNumber(String billingAccountNumber) {
        this.billingAccountNumber = billingAccountNumber;
    }

    /**
     * Gets the billingAccountName
     *
     * @return the billingAccountName
     */
    public String getBillingAccountName() {
        return billingAccountName;
    }

    /**
     * Sets the billingAccountName.
     *
     * @param billingAccountName the new billingAccountName
     */
    public void setBillingAccountName(String billingAccountName) {
        this.billingAccountName = billingAccountName;
    }

    /**
     * Gets the billingAccountAddressLine1
     *
     * @return the billingAccountAddressLine1
     */
    public String getBillingAccountAddressLine1() {
        return billingAccountAddressLine1;
    }

    /**
     * Sets the billingAccountAddressLine1.
     *
     * @param billingAccountAddressLine1 the new billingAccountAddressLine1
     */
    public void setBillingAccountAddressLine1(String billingAccountAddressLine1) {
        this.billingAccountAddressLine1 = billingAccountAddressLine1;
    }

    /**
     * Gets the billingAccountAddressLine2
     *
     * @return the billingAccountAddressLine2
     */
    public String getBillingAccountAddressLine2() {
        return billingAccountAddressLine2;
    }

    /**
     * Sets the billingAccountAddressLine2.
     *
     * @param billingAccountAddressLine2 the new billingAccountAddressLine2
     */
    public void setBillingAccountAddressLine2(String billingAccountAddressLine2) {
        this.billingAccountAddressLine2 = billingAccountAddressLine2;
    }

    /**
     * Gets the billingAccountAddressLine3
     *
     * @return the billingAccountAddressLine3
     */
    public String getBillingAccountAddressLine3() {
        return billingAccountAddressLine3;
    }

    /**
     * Sets the billingAccountAddressLine3.
     *
     * @param billingAccountAddressLine3 the new billingAccountAddressLine3
     */
    public void setBillingAccountAddressLine3(String billingAccountAddressLine3) {
        this.billingAccountAddressLine3 = billingAccountAddressLine3;
    }

    /**
     * Gets the billingAccountAddressLine4
     *
     * @return the billingAccountAddressLine4
     */
    public String getBillingAccountAddressLine4() {
        return billingAccountAddressLine4;
    }

    /**
     * Sets the billingAccountAddressLine4.
     *
     * @param billingAccountAddressLine4 the new billingAccountAddressLine4
     */
    public void setBillingAccountAddressLine4(String billingAccountAddressLine4) {
        this.billingAccountAddressLine4 = billingAccountAddressLine4;
    }

    /**
     * Gets the billingAccountAddressLine5
     *
     * @return the billingAccountAddressLine5
     */
    public String getBillingAccountAddressLine5() {
        return billingAccountAddressLine5;
    }

    /**
     * Sets the billingAccountAddressLine5.
     *
     * @param billingAccountAddressLine5 the new billingAccountAddressLine5
     */
    public void setBillingAccountAddressLine5(String billingAccountAddressLine5) {
        this.billingAccountAddressLine5 = billingAccountAddressLine5;
    }

    /**
     * Gets the billingAccountServiceType
     *
     * @return the billingAccountServiceType
     */
    public String getBillingAccountServiceType() {
        return billingAccountServiceType;
    }

    /**
     * Sets the billingAccountServiceType.
     *
     * @param billingAccountServiceType the new billingAccountServiceType
     */
    public void setBillingAccountServiceType(String billingAccountServiceType) {
        this.billingAccountServiceType = billingAccountServiceType;
    }

    /**
     * Gets the billingAccountStatus
     *
     * @return the billingAccountStatus
     */
    public String getBillingAccountStatus() {
        return billingAccountStatus;
    }

    /**
     * Sets the billingAccountStatus.
     *
     * @param billingAccountStatus the new billingAccountStatus
     */
    public void setBillingAccountStatus(String billingAccountStatus) {
        this.billingAccountStatus = billingAccountStatus;
    }

    /**
     * Gets the billingAccountCreationDate
     *
     * @return the billingAccountCreationDate
     */
    public String getBillingAccountCreationDate() {
        return billingAccountCreationDate;
    }

    /**
     * Sets the billingAccountCreationDate.
     *
     * @param billingAccountCreationDate the new billingAccountCreationDate
     */
    public void setBillingAccountCreationDate(String billingAccountCreationDate) {
        this.billingAccountCreationDate = billingAccountCreationDate;
    }

    /**
     * Gets the billingAccountCategory
     *
     * @return the billingAccountCategory
     */
    public String getBillingAccountCategory() {
        return billingAccountCategory;
    }

    /**
     * Sets the billingAccountCategory.
     *
     * @param billingAccountCategory the new billingAccountCategory
     */
    public void setBillingAccountCategory(String billingAccountCategory) {
        this.billingAccountCategory = billingAccountCategory;
    }

    /**
     * Gets the billingAccountProfile
     *
     * @return the billingAccountProfile
     */
    public String getBillingAccountProfile() {
        return billingAccountProfile;
    }

    /**
     * Sets the billingAccountProfile.
     *
     * @param billingAccountProfile the new billingAccountProfile
     */
    public void setBillingAccountProfile(String billingAccountProfile) {
        this.billingAccountProfile = billingAccountProfile;
    }
}