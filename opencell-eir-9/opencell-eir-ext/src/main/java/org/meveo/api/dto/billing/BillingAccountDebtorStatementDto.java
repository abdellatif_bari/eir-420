/*
 * (C) Copyright 2015-2020 Opencell SAS (https://opencellsoft.com/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
 * OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS
 * IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO
 * THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 *
 * For more information on the GNU Affero General Public License, please consult
 * <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

package org.meveo.api.dto.billing;

import java.util.Date;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.meveo.api.dto.BaseDto;

// TODO: Auto-generated Javadoc
/**
 * The Class BillingAccountDebtorStatementDto.
 * 
 * @author H.ZNIBAR
 **/
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
public class BillingAccountDebtorStatementDto extends BaseDto {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 3071509889798928983L;

    /** The billing account code. */
    @XmlElement(required = true)
    private String billingAccountCode;
    
    /** The debtor statement from date. */
    @XmlElement
    private Date debtorStatementFromDate;
    
    /**
     * The file path
     */
    private String filePath;
        

    public String getFilePath() {
        return filePath;
    }

    public void setFilePath(String filePath) {
        this.filePath = filePath;
    }

    public String getBillingAccountCode() {
        return billingAccountCode;
    }

    public void setBillingAccountCode(String billingAccountCode) {
        this.billingAccountCode = billingAccountCode;
    }

    public Date getDebtorStatementFromDate() {
        return debtorStatementFromDate;
    }

    public void setDebtorStatementFromDate(Date debtorStatementFromDate) {
        this.debtorStatementFromDate = debtorStatementFromDate;
    }

    
    
    

}