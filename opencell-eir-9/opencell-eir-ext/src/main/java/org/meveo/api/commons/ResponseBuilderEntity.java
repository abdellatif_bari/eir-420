package org.meveo.api.commons;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import com.fasterxml.jackson.annotation.JsonInclude;

/**
 * Define the default response of the EIR API web service.
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseBuilderEntity {

    /**
     * Tells whether the instance of this <code>ResponseBuilderEntity</code> object ok or not.
     */
    @XmlElement(required = true)
    private ResponseStatusEnum status;

    /**
     * An error code.
     */
    private ErrorEnum errorCode;

    /**
     * A detailed error message if applicable, can contain the entity id that was created.
     */
    @XmlElement(required = true)
    private String message;

    /**
     * Gets the status
     *
     * @return the status
     */
    public ResponseStatusEnum getStatus() {
        return status;
    }

    /**
     * Sets the status.
     *
     * @param status the new status
     */
    public void setStatus(ResponseStatusEnum status) {
        this.status = status;
    }

    /**
     * Gets the errorCode
     *
     * @return the errorCode
     */
    public ErrorEnum getErrorCode() {
        return errorCode;
    }

    /**
     * Sets the errorCode.
     *
     * @param errorCode the new errorCode
     */
    public void setErrorCode(ErrorEnum errorCode) {
        this.errorCode = errorCode;
    }

    /**
     * Gets the message
     *
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * Sets the message.
     *
     * @param message the new message
     */
    public void setMessage(String message) {
        this.message = message;
    }
}
