package org.meveo.api.dto.order;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Address Info Response Dto
 *
 * @author Abdellatif BARI
 */
@XmlRootElement(name = "Address_Info_Response")
@XmlAccessorType(XmlAccessType.FIELD)
public class AddressInfoResponseDto implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 872775769637725503L;

    /**
     * The address infos.
     */
    @XmlElement(name = "Address_Info")
    private List<AddressInfoDto> addressInfos = new ArrayList<>();

    /**
     * Gets the addressInfos
     *
     * @return the addressInfos
     */
    public List<AddressInfoDto> getAddressInfos() {
        return addressInfos;
    }

    /**
     * Sets the addressInfos.
     *
     * @param addressInfos the new addressInfos
     */
    public void setAddressInfos(List<AddressInfoDto> addressInfos) {
        this.addressInfos = addressInfos;
    }
}
