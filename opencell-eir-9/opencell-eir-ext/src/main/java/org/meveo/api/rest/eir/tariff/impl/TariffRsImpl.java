package org.meveo.api.rest.eir.tariff.impl;

import com.eir.api.tariff.TariffApi;
import com.eir.api.tariff.TariffPlanApi;
import org.meveo.api.dto.custom.UnitaryCustomTableDataDto;
import org.meveo.api.dto.flatfile.ExportDto;
import org.meveo.api.logging.WsRestApiInterceptor;
import org.meveo.api.rest.eir.FileRs;
import org.meveo.api.rest.eir.tariff.TariffRs;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.core.Response;

/**
 * REST Web service implementation for tariff managing.
 *
 * @author Abdellatif BARI
 */
@RequestScoped
@Interceptors({WsRestApiInterceptor.class})
public class TariffRsImpl extends FileRs implements TariffRs {

    @Inject
    private TariffApi tariffApi;

    @Inject
    private TariffPlanApi tariffPlanApi;

    @Override
    public Response createOrUpdateTariffPlan(UnitaryCustomTableDataDto unitaryCustomTableDataDto) {
        Response response = null;
        try {
            tariffPlanApi.createOrUpdateTariffPlan(unitaryCustomTableDataDto);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    @Override
    public Response createOrUpdateTariffPlanMap(UnitaryCustomTableDataDto unitaryCustomTableDataDto) {
        Response response = null;
        try {
            tariffPlanApi.createOrUpdateTariffPlanMap(unitaryCustomTableDataDto);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    @Override
    public Response validateTariff(String offerType, Long fileId) {
        Response response = null;
        try {
            tariffApi.validateTariff(offerType, fileId);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    @Override
    public Response activateTariff(String offerType, Long fileId) {
        Response response = null;
        try {
            tariffApi.activateTariff(offerType, fileId);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    @Override
    public Response cancelTariff(Long fileId) {
        Response response = null;
        try {
            tariffApi.cancelTariff(fileId);
            response = buildSuccessResponse();
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }

    @Override
    public Response exportTariff(ExportDto exportDto) {
        Response response = null;
        try {
            response = getFile(tariffApi.exportTariff(exportDto));
        } catch (Exception e) {
            response = buildFailureResponse(e);
        }
        return response;
    }
}