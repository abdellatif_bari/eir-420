package com.eir.api.tariff;

import com.eir.service.tariff.TariffService;
import org.meveo.admin.exception.BusinessException;
import org.meveo.api.BaseApi;
import org.meveo.api.dto.flatfile.ExportDto;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.commons.utils.StringUtils;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.io.File;

/**
 * The tariff API.
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirTariffApi")
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class TariffApi extends BaseApi {

    @Inject
    private TariffService tariffService;

    /**
     * Activate tariffs in a staging table that were imported from a given file name.
     *
     * @param offerType The offer type
     * @param fileId    The id of a tariff import file
     * @throws BusinessException the business exception
     */
    public void validateTariff(String offerType, Long fileId) throws BusinessException {
        if (StringUtils.isBlank(offerType)) {
            throw new BusinessException("Missing file name.");
        }
        if (fileId == null) {
            throw new BusinessException("Missing file name.");
        }
        tariffService.validateTariffs(offerType, fileId);
    }

    /**
     * Activate tariffs in a staging table that were imported from a given file name.
     *
     * @param offerType The offer type
     * @param fileId    The id of a tariff import file
     * @throws BusinessException the business exception
     */
    public void activateTariff(String offerType, Long fileId) throws BusinessException {
        if (StringUtils.isBlank(offerType)) {
            throw new BusinessException("Missing file name.");
        }
        if (fileId == null) {
            throw new BusinessException("Missing file name.");
        }
        tariffService.activateTariff(offerType, fileId);
    }

    /**
     * Cancel tariffs in a staging table that were imported from a given file.
     *
     * @param fileId The id of a tariff import file
     * @throws BusinessException the business exception
     */
    public void cancelTariff(Long fileId) throws BusinessException {
        if (fileId == null) {
            throw new BusinessException("Missing file name.");
        }
        tariffService.cancelTariff(fileId);
    }

    /**
     * Create tariff file from custom table by the filtered data
     *
     * @param exportDto the export Dto
     * @return the tariff file
     * @throws BusinessException business exception
     */
    public File exportTariff(ExportDto exportDto) throws BusinessException {

        if (StringUtils.isBlank(exportDto.getFileName())) {
            throw new BusinessException("the file name is missing");
        }
        if (StringUtils.isBlank(exportDto.getFileFormatCode())) {
            throw new BusinessException("the file format is missing");
        }

        if (StringUtils.isBlank(exportDto.getExportType())) {
            throw new BusinessException("the export type is missing");
        }
        return tariffService.exportTariff(exportDto);
    }

}
