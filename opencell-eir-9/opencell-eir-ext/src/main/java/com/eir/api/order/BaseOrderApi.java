package com.eir.api.order;

import com.eir.cache.OrderCacheContainerProvider;
import com.eir.commons.enums.ApplicationPropertiesEnum;
import com.eir.commons.enums.ChargeTemplateEnum;
import com.eir.commons.enums.ChargeTypeEnum;
import com.eir.commons.enums.OfferTemplateEnum;
import com.eir.commons.enums.OrderEmitterEnum;
import com.eir.commons.enums.OrderLineAttributeNamesEnum;
import com.eir.commons.enums.OrderProcessingTypeEnum;
import com.eir.commons.enums.ServiceTypeEnum;
import com.eir.commons.enums.TerminationReasonEnum;
import com.eir.commons.enums.customfields.ChargeInstanceCFsEnum;
import com.eir.commons.enums.customfields.OrderCFsEnum;
import com.eir.commons.enums.customfields.ProviderCFsEnum;
import com.eir.commons.enums.customfields.ServiceInstanceCFsEnum;
import com.eir.commons.enums.customfields.ServiceInstanceIdentifierAttributesCFsEnum;
import com.eir.commons.enums.customfields.ServiceParametersCFEnum;
import com.eir.commons.enums.customfields.ServiceTemplateCFsEnum;
import com.eir.commons.enums.customfields.SubscriptionCFsEnum;
import com.eir.commons.enums.customtables.CustomTableEnum;
import com.eir.commons.enums.customtables.EndPointCustomTableEnum;
import com.eir.script.OneShotAndRecurringRatingScript;
import com.eir.service.billing.impl.BillingAccountService;
import com.eir.service.billing.impl.SubscriptionService;
import com.eir.service.catalog.impl.ServiceTemplateService;
import com.eir.service.commons.UtilService;
import com.eir.service.crm.impl.SubscriptionTerminationReasonService;
import com.eir.service.mediation.impl.AccessService;
import com.eir.service.order.impl.OrderService;
import org.apache.commons.lang3.SerializationUtils;
import org.hibernate.proxy.HibernateProxy;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.RatingException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.api.BaseApi;
import org.meveo.api.commons.ErrorEnum;
import org.meveo.api.commons.Utils;
import org.meveo.api.dto.billing.ChargeInitializationParametersDto;
import org.meveo.api.dto.order.AddressInfoDto;
import org.meveo.api.dto.order.AddressInfoResponseDto;
import org.meveo.api.dto.order.AttributeDto;
import org.meveo.api.dto.order.BuildingDto;
import org.meveo.api.dto.order.CountyDto;
import org.meveo.api.dto.order.OrderDto;
import org.meveo.api.dto.order.OrderLineDto;
import org.meveo.api.dto.order.StreetDto;
import org.meveo.api.dto.order.UnitDto;
import org.meveo.commons.utils.StringUtils;
import org.meveo.commons.utils.ThreadUtils;
import org.meveo.jpa.EntityManagerWrapper;
import org.meveo.jpa.JpaAmpNewTx;
import org.meveo.jpa.MeveoJpa;
import org.meveo.model.DatePeriod;
import org.meveo.model.ICustomFieldEntity;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.ChargeApplicationModeEnum;
import org.meveo.model.billing.InstanceStatusEnum;
import org.meveo.model.billing.OneShotChargeInstance;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.SubscriptionChargeInstance;
import org.meveo.model.billing.SubscriptionStatusEnum;
import org.meveo.model.billing.SubscriptionTerminationReason;
import org.meveo.model.billing.TerminationChargeInstance;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.OneShotChargeTemplate;
import org.meveo.model.catalog.ServiceChargeTemplateRecurring;
import org.meveo.model.catalog.ServiceChargeTemplateSubscription;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.crm.EntityReferenceWrapper;
import org.meveo.model.crm.custom.CustomFieldValue;
import org.meveo.model.customEntities.CustomEntityInstance;
import org.meveo.model.mediation.Access;
import org.meveo.model.order.Order;
import org.meveo.model.order.OrderStatusEnum;
import org.meveo.model.shared.DateUtils;
import org.meveo.security.CurrentUser;
import org.meveo.security.MeveoUser;
import org.meveo.security.keycloak.CurrentUserProvider;
import org.meveo.service.admin.impl.SellerService;
import org.meveo.service.billing.impl.OneShotChargeInstanceService;
import org.meveo.service.billing.impl.ReratingService;
import org.meveo.service.billing.impl.ServiceInstanceService;
import org.meveo.service.billing.impl.UserAccountService;
import org.meveo.service.catalog.impl.OfferTemplateService;
import org.meveo.service.catalog.impl.OneShotChargeTemplateService;
import org.meveo.service.crm.impl.CustomFieldInstanceService;
import org.meveo.service.crm.impl.CustomFieldTemplateService;
import org.meveo.service.crm.impl.ProviderService;
import org.meveo.service.custom.CustomEntityInstanceService;
import org.meveo.service.custom.CustomTableService;

import javax.annotation.PostConstruct;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.enterprise.concurrent.ManagedExecutorService;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * The base order API.
 *
 * @author Abdellatif BARI
 */
public abstract class BaseOrderApi extends BaseApi {

    @Inject
    protected ProviderService providerService;
    @Inject
    protected OrderService orderService;
    @Inject
    protected BillingAccountService billingAccountService;
    @Inject
    protected SubscriptionService subscriptionService;
    @Inject
    protected OfferTemplateService offerTemplateService;
    @Inject
    protected UserAccountService userAccountService;
    @Inject
    protected SellerService sellerService;
    @Inject
    protected ServiceTemplateService serviceTemplateService;
    @Inject
    protected ServiceInstanceService serviceInstanceService;
    @Inject
    protected OneShotChargeTemplateService oneShotChargeTemplateService;
    @Inject
    protected OneShotChargeInstanceService oneShotChargeInstanceService;
    @Inject
    protected CustomFieldTemplateService customFieldTemplateService;
    @Inject
    protected CustomFieldInstanceService customFieldInstanceService;
    @Inject
    protected CustomEntityInstanceService customEntityInstanceService;
    @Inject
    protected CustomTableService customTableService;
    @Inject
    protected SubscriptionTerminationReasonService subscriptionTerminationReasonService;
    @Inject
    protected AccessService accessService;
    @Inject
    protected OneShotAndRecurringRatingScript ratingScript;
    @Inject
    protected ReratingService reratingService;
    @Inject
    protected UtilService utilService;
    @Inject
    protected CurrentUserProvider currentUserProvider;
    @Inject
    @CurrentUser
    protected MeveoUser currentUser;
    @Inject
    private OrderCacheContainerProvider orderCacheContainerProvider;

    /**
     * Custom field template "ORDER_ORIGINAL_MESSAGE" definition for order
     */
    private static CustomFieldTemplate orderOriginalMessageCFT;

    /**
     * The Constant CET_CIRCUIT_TYPE_MAPPING.
     */
    protected static final String CET_CIRCUIT_TYPE_MAPPING = "CIRCUIT_TYPE_MAPPING";

    /**
     * The prefix constant for MISC subscriptions.
     */
    protected static final String MISC_PREFIX_SERVICE_ID = "MISC_";

    @Inject
    @MeveoJpa
    private EntityManagerWrapper emWrapper;

    /**
     * Engagement duration attribute names Enum
     */
    public enum EngagementDurationAttributeNamesEnum {
        Customer, Offer, startDate, endDate, minTerm
    }

    /**
     * Item status enum
     */
    protected enum ItemStatusEnum {
        AC("Active"), CE("Ceased");

        private String label;

        ItemStatusEnum(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    /**
     * Bill action enum
     */
    protected enum BillActionEnum {
        P, C, CH, M, CN, CU, SN, RB,
        // Added in UG treatment
        TOS, TNS, COS, CNS;

        private static Set<String> _values = new HashSet<>();

        static {
            for (BillActionEnum choice : BillActionEnum.values()) {
                _values.add(choice.name());
            }
        }

        public static boolean contains(String value) {
            return !StringUtils.isBlank(value) && _values.contains(value.toUpperCase());
        }

        public static BillActionEnum valueOfIgnoreCase(String billAction) {
            if (!StringUtils.isBlank(billAction)) {
                billAction = billAction.toUpperCase();
            }
            return valueOf(billAction);
        }
    }

    /**
     * Action qualifier enum
     */
    protected enum ActionQualifierEnum {
        APR, DPR, FAILED_INSTALL, TOS, CX
    }

    /**
     * Which end of the circuit is the endpoint on
     */
    protected enum EndEnum {
        A, B, C, AS
    }

    /**
     * Event type enum
     */
    protected enum EventTypeEnum {
        CHANGE, TRANSFER, CEASE
    }

    /**
     * Order XML Schema
     */
    private static final String XML_SCHEMA = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" elementFormDefault=\"qualified\" attributeFormDefault=\"unqualified\">\n"
            + "   <xs:element name=\"Wholesale_Billing\">\n" + "      <xs:complexType>\n" + "         <xs:sequence>\n"
            + "            <xs:element name=\"Transaction_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "            <xs:element name=\"Ordering_System\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "            <xs:element name=\"Order_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "            <xs:element name=\"Ref_Order_No\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n" + "            <xs:element name=\"Bill_Item\">\n"
            + "               <xs:complexType>\n" + "                  <xs:sequence>\n"
            + "                     <xs:element name=\"Item_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Operator_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Operator_Order_Number\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Item_Component_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Item_Version\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Item_Type\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Agent_Code\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"UAN\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Master_Account_Number\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Service_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Ref_Service_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Xref_Service_id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Bill_Code\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Action\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Action_Qualifier\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Quantity\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Effect_Date\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Special_Connect\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Special_Rental\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"REMARK_ID\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"ELEMENT_TYPE\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"ELEMENT_ID\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"DESCRIPTION\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Attribute\" maxOccurs=\"unbounded\">\n" + "                        <xs:complexType>\n"
            + "                           <xs:sequence>\n" + "                              <xs:element name=\"Code\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                              <xs:element name=\"Value\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                              <xs:element name=\"Unit\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                              <xs:element name=\"Component_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                              <xs:element name=\"Attribute_Type\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                           </xs:sequence>\n" + "                        </xs:complexType>\n" + "                     </xs:element>\n"
            + "                  </xs:sequence>\n" + "               </xs:complexType>\n" + "            </xs:element>\n" + "         </xs:sequence>\n"
            + "      </xs:complexType>\n" + "   </xs:element>\n" + "</xs:schema>";

    @PostConstruct
    public void init() throws BusinessException {

        // Lookup CFT definitions
        orderOriginalMessageCFT = customFieldTemplateService.findByCodeAndAppliesTo(OrderCFsEnum.ORDER_ORIGINAL_MESSAGE.name(), new Order());
        customFieldTemplateService.detach(orderOriginalMessageCFT);
    }

    /**
     * Validate the order Dto
     *
     * @param orderDto the order Dto
     * @return true is the order Dto is valid
     * @throws BusinessException the business exception
     */
    public boolean validate(OrderDto orderDto) throws BusinessException {
        if (isOrderAlreadyExists(orderDto)) {
            return false;
        }
        // Store original xml order
        orderDto.setOriginalXmlOrder(getXmlOrder(orderDto));
        if (isOrderTooLarge(orderDto)) {
            return false;
        }
        return true;
    }

    /**
     * Create order
     *
     * @param orderDto the order Dto
     * @return the order Dto
     * @throws BusinessException the business exception
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public OrderDto create(OrderDto orderDto) throws BusinessException {
        // Process asynchronously the order.
        processWithErrorHandling(orderDto);
        return orderDto;
    }

    /**
     * Process order with erorr handling
     *
     * @param orderDto the order Dto
     * @return the order
     * @throws BusinessException the business exception
     */
    private Order processWithErrorHandling(OrderDto orderDto) {
        try {
            Order order = getSelfForNewTx().process(orderDto);
            if (order.getStatus() == OrderStatusEnum.COMPLETED) {
                getSelfForNewTx().releaseDeferredOrders(orderDto);
            }
            return order;
        } catch (Exception ex) {
            log.error("Order {} execution process ended with errors ", orderDto.getOrderCode(), ex);

            if (orderDto.getErrorCode() == null) {
                if (ex instanceof ValidationException || ex instanceof RatingException
                        || (ex instanceof BusinessException && ex.getMessage().indexOf("RatingScriptExecutionErrorException") > -1)) {
                    setError(orderDto, ErrorEnum.INVALID_DATA, ex.getMessage());

                } else {
                    setError(orderDto, ErrorEnum.GENERIC_API_EXCEPTION, ex.getMessage() != null ? ex.getMessage() : ex.toString());
                }
            }

            persistOrder(orderDto);
        }
        return null;
    }

    /**
     * Process order
     *
     * @param orderDto the order Dto
     * @return the order
     * @throws BusinessException the business exception
     */
    @JpaAmpNewTx
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public Order process(OrderDto orderDto) throws BusinessException {
        Order order = null;
        // We will wait until the pending order is created
        long start = System.currentTimeMillis();
        while ((order = orderService.findByCode(orderDto.getOrderCode())) == null) {
            ThreadUtils.sleepSafe(TimeUnit.SECONDS, 5);
            if (System.currentTimeMillis() - start > 10000 && orderService.findByCode(orderDto.getOrderCode()) == null) {
                throw new BusinessException("A technical problem occurred, please try again");
            }
        }
        orderDto.setOrderId(order.getId());

        // Initialize the order context
        init(orderDto);

        // validate order (Validation of Control-Flow)
        validateOrder(orderDto);

        log.debug("After validation : orderDto =<" + orderDto.getOrderCode() + ">; errorCode =<" + orderDto.getErrorCode() + ">; " + "errorMessage =<" + orderDto.getErrorMessage()
                + ">; statusMessage =<" + orderDto.getStatusMessage() + ">; " + "status =<" + orderDto.getStatus() + ">");

        // executing order
        executeOrder(orderDto);

        log.debug("After process : orderDto =<" + orderDto.getOrderCode() + ">; errorCode =<" + orderDto.getErrorCode() + ">; " + "errorMessage =<" + orderDto.getErrorMessage()
                + ">; statusMessage =<" + orderDto.getStatusMessage() + ">; " + "status =<" + orderDto.getStatus() + ">");

        // persisting order
        order = persistOrder(orderDto);

        log.debug("After persist : orderDto =<" + orderDto.getOrderCode() + ">; errorCode =<" + orderDto.getErrorCode() + ">; " + "errorMessage =<" + orderDto.getErrorMessage()
                + ">; statusMessage =<" + order.getStatusMessage() + ">; " + "status =<" + order.getStatus() + ">");

        return order;
    }

    /**
     * Reprocess order
     *
     * @param orderDto the order Dto
     * @return the order Dto
     * @throws BusinessException the business exception
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public OrderDto reprocess(OrderDto orderDto) throws BusinessException {
        reprocessOrder(orderDto);
        return orderDto;
    }

    /**
     * Reprocess order
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    public void reprocessOrder(OrderDto orderDto) {
        Order order = processWithErrorHandling(orderDto);
        reprocessHeldOrders(orderDto, order);
    }

    /**
     * Release order
     *
     * @param orderCode the order code
     * @return the order Dto
     * @throws BusinessException the business exception
     */
    public OrderDto release(String orderCode) throws BusinessException {
        OrderDto orderDto = orderService.getOrderDto(orderCode, XML_SCHEMA);
        orderDto.setProcessingType(OrderProcessingTypeEnum.RELEASE);
        reprocessOrder(orderDto);
        return orderDto;
    }

    /**
     * Initialize the order context
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    protected abstract void init(OrderDto orderDto) throws BusinessException;

    /**
     * Validate order
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    protected abstract void validateOrder(OrderDto orderDto) throws BusinessException;

    /**
     * validate order line prerequisites
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the The order line Dto
     * @throws BusinessException the business exception
     */
    protected abstract void validateOrderLinePrerequisites(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException;

    /**
     * Validate order line
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    protected abstract void validateOrderLine(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException;

    /**
     * Execute created order by activating/changing/ceasing services in subscription
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    protected abstract void executeOrder(OrderDto orderDto) throws BusinessException;

    /**
     * Persist order
     *
     * @param orderDto the order Dto
     * @return the order
     * @throws BusinessException the business exception
     */
    protected Order persistOrder(OrderDto orderDto) throws BusinessException {
        // Don't persist order if there is at least one blocked error.
        /*
        if (orderDto.hasBlockedError()) {
            log.debug("the orderDto " + orderDto.getErrorCode() + " has a blocked error : errorCode =<" + orderDto.getErrorCode() + ">; " + "errorMessage =<"
                    + orderDto.getErrorMessage() + ">");
            return null;
        }*/

        Order order = !StringUtils.isBlank(orderDto.getOrderId()) ? orderService.findById(orderDto.getOrderId()) : null;
        if (order == null) {
            log.debug("Create the new order");
            order = createOrder(orderDto);
        }

        // never change the order status if it has already been completed
        if (order.getStatus() == OrderStatusEnum.COMPLETED) {
            return order;
        }

        order.setStatus(orderDto.getStatus());
        order.setStatusMessage(orderDto.getSuccessMessage());
        if (!StringUtils.isBlank(orderDto.getErrorMessage())) {
            order.setStatusMessage(orderDto.getErrorMessage());
        }
        if (orderDto.getStatus() == OrderStatusEnum.COMPLETED) {
            order.setCompletionDate(new Date());
        }

        updateOrderCfs(orderDto, order);
        if (order.getOrderItems() == null) {
            order.setOrderItems(new ArrayList<>());
        }
        if (orderDto.getProcessingType() == OrderProcessingTypeEnum.REPROCESS) {
            order.setOrderDate(new Date());
        }
        if (order.getId() == null) {
            orderService.create(order);
        } else {
            order = orderService.update(order);
        }
        return order;
    }

    /**
     * Update order cfs
     *
     * @param orderDto the order Dto
     * @param order    the order
     */
    protected void updateOrderCfs(OrderDto orderDto, Order order) {
        // Don't update order CFs when the status is IN_PROGRESS
        if (orderDto.getStatus() == OrderStatusEnum.IN_PROGRESS) {
            return;
        }

        // store Order Items and attributes in CF at Order level
        Map<String, String> orderItemsCF = new HashMap<>();
        Map<String, String> orderItemAttributesCF = new HashMap<>();

        // Populate order attributes
        for (OrderLineDto orderLineDto : orderDto.getOrderLines()) {

            /** added for a technical need on the portal side **/
            setIds(orderDto, orderLineDto);

            // store order items and their attributes in adequate CFs
            orderItemsCF.put(orderLineDto.getItemId(), orderLineDto.getDetails());
            for (AttributeDto attributeDto : orderLineDto.getAttributes()) {
                orderItemAttributesCF.put(orderLineDto.getItemId() + CustomFieldValue.MATRIX_KEY_SEPARATOR + attributeDto.getCode(), attributeDto.getDetails());
            }

            // Workaround for the portal filter problem at the order level
            setOfferIdCf(orderLineDto, order);
        }

        order.setCfValue(OrderCFsEnum.ORDER_ERROR_MESSAGE.name(), orderDto.getErrorMessage());
        if (!StringUtils.isBlank(orderDto.getReleaseDescription())) {
            order.setCfValue(OrderCFsEnum.RELEASE_DESCRIPTION.name(), orderDto.getReleaseDescription());
        }
        if (StringUtils.isNotBlank(orderDto.getHeldBy())) {
            order.setCfValue(OrderCFsEnum.HELD_BY.name(), orderDto.getHeldBy());
        }
        order.setCfValue(OrderCFsEnum.ORDER_ORIGINAL_MESSAGE.name(), orderDto.getOriginalXmlOrder());
        order.setCfValue(OrderCFsEnum.ORDER_MESSAGE.name(), getXmlOrder(orderDto));
        order.setCfValue(OrderCFsEnum.ORDER_DETAILS.name(), orderDto.getOrderDetails());
        order.setCfValue(OrderCFsEnum.ORDER_REF_ID.name(), orderDto.getRefOrderCode());
        order.setCfValue(OrderCFsEnum.ORDER_ITEMS.name(), orderItemsCF);
        order.setCfValue(OrderCFsEnum.ORDER_ITEM_ATTRIBUTES.name(), orderItemAttributesCF);
        order.setCfValue(OrderCFsEnum.ORDER_DATA_DISCREPANCIES.name(), orderDto.getDataDiscrepancies());
        Set<String> subscriptionCodes = orderDto.getSubscriptionCodes();
        subscriptionCodes.addAll(orderDto.getRefSubscriptionCodes());
        order.setCfValue(OrderCFsEnum.SERVICE_IDS.name(), subscriptionCodes.stream().collect(Collectors.toList()));
    }

    /**
     * Create pending order and set the acknowledgement message
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    public void setOrderInPendingStatus(OrderDto orderDto) throws BusinessException {
        getSelfForNewTx().setPendingStatus(orderDto);
    }

    /**
     * Set in_progress status and set the acknowledgement message
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    public void setOrderInProgressStatus(OrderDto orderDto) throws BusinessException {
        getSelfForNewTx().setInProgressStatus(orderDto);
    }

    /**
     * Sort orderLines
     *
     * @param orderDto the order Dto
     */
    protected abstract void sortOrderLines(OrderDto orderDto);

    /**
     * Check if the item is a service or another thing like EndPoint, Association, Remark,......
     *
     * @param orderLineDto the The order line Dto
     * @return true if the item is a service
     */
    protected abstract boolean isService(OrderLineDto orderLineDto);

    /**
     * Create service instance
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return service instance
     * @throws BusinessException the business exception
     */
    protected abstract ServiceInstance createServiceInstance(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException;

    /**
     * Get the end point
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return the end point
     * @throws BusinessException the business exception
     */
    protected abstract Map<String, Object> getEndPoint(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException;

    /**
     * Create order
     *
     * @param orderDto the order Dto
     */
    protected Order createOrder(OrderDto orderDto) {
        Order order = new Order();
        order.setCode(orderDto.getOrderCode());
        order.setReceivedFromApp(orderDto.getOrderingSystem());
        return order;
    }

    /**
     * Reset order
     *
     * @param orderDto the order Dto
     */
    protected void resetOrder(OrderDto orderDto) {
        orderDto.setErrorCode(null);
        orderDto.setErrorMessage(null);
        orderDto.setSuccessMessage(null);
    }

    /**
     * Create pending order and set the acknowledgement message
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    @JpaAmpNewTx
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void setPendingStatus(OrderDto orderDto) throws BusinessException {

        // Set the pending status
        orderDto.setStatus(OrderStatusEnum.PENDING);
        // Set acknowledgement message
        //orderDto.setSuccessMessage("The order is well received, and it will be processed");
        persistOrder(orderDto);
    }

    /**
     * Set in_progress status and set the acknowledgement message
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    @JpaAmpNewTx
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void setInProgressStatus(OrderDto orderDto) throws BusinessException {

        // Set the pending status
        orderDto.setStatus(OrderStatusEnum.IN_PROGRESS);
        // Set acknowledgement message
        //orderDto.setSuccessMessage(successMessage);
        persistOrder(orderDto);
    }

    /**
     * Get XML order from order Dto.
     *
     * @param orderDto the order Dto
     * @return the XML order
     * @throws BusinessException the business exception
     */
    protected String getXmlOrder(OrderDto orderDto) throws BusinessException {
        return Utils.getXmlFromDto(orderDto);
    }

    /**
     * Sets the originalXmlOrder.
     *
     * @param orderDto the order Dto
     */
    protected void setOriginalXmlOrder(OrderDto orderDto) {
        if (StringUtils.isBlank(orderDto.getOriginalXmlOrder())) {
            orderDto.setOriginalXmlOrder(getXmlOrder(orderDto));
        }
    }

    /**
     * Set bad request error
     *
     * @param orderDto  The order Dto
     * @param errorEnum The error enum
     * @param fieldName The field name
     */
    protected void setBadRequestError(OrderDto orderDto, ErrorEnum errorEnum, String fieldName) {
        String errorMessage = null;
        if (errorEnum == ErrorEnum.BAD_REQUEST_MISSING_PARAMETER) {
            errorMessage = String.format("The following parameter is required : %s", fieldName);
        } else if (errorEnum == ErrorEnum.BAD_REQUEST_INVALID_VALUE) {
            errorMessage = String.format("The following parameter contains invalid value : %s", fieldName);
        } else if (errorEnum == ErrorEnum.BAD_REQUEST_INVALID_SIZE) {
            errorMessage = String.format("The following parameter has invalid size : %s", fieldName);
        }
        setError(orderDto, errorEnum, errorMessage);
    }

    /**
     * Set order error
     *
     * @param orderDto     The order Dto
     * @param errorEnum    The error enum
     * @param orderStatus  The order status
     * @param errorMessage The error message
     */
    protected void setError(OrderDto orderDto, ErrorEnum errorEnum, OrderStatusEnum orderStatus, String errorMessage) {
        if (orderDto != null) {
            orderDto.setError(errorEnum, orderStatus, errorMessage);
        }
    }

    /**
     * Set order error
     *
     * @param orderDto     The order Dto
     * @param errorEnum    The error enum
     * @param errorMessage The error message
     */
    protected void setError(OrderDto orderDto, ErrorEnum errorEnum, String errorMessage) {
        setError(orderDto, errorEnum, OrderStatusEnum.REJECTED, errorMessage);
    }

    /**
     * Set order error
     *
     * @param orderDto     The order Dto
     * @param orderStatus  The order status
     * @param errorMessage The error message
     */
    protected void setError(OrderDto orderDto, OrderStatusEnum orderStatus, String errorMessage) {
        setError(orderDto, ErrorEnum.INVALID_DATA, orderStatus, errorMessage);
    }

    /**
     * Validate order fields
     *
     * @param orderDto           order Dto
     * @param fieldName          Field name
     * @param fieldValue         Field value
     * @param fieldExpectedValue field expected value
     * @param fieldType          Field type
     * @param fieldSize          Field size
     * @param mandatory          Is field mandatory
     * @return Field Value
     */
    @SuppressWarnings("rawtypes")
    protected boolean validateField(OrderDto orderDto, String fieldName, Object fieldValue, Object fieldExpectedValue, Class fieldType, Integer fieldSize, boolean mandatory) {

        if (!mandatory && StringUtils.isBlank(fieldValue)) {
            return true;
        }

        // Check required parameter
        if (mandatory && StringUtils.isBlank(fieldValue)) {
            setBadRequestError(orderDto, ErrorEnum.BAD_REQUEST_MISSING_PARAMETER, fieldName);
            return false;
        }

        // Check expected value
        if (!StringUtils.isBlank(fieldExpectedValue)
                && (!fieldExpectedValue.equals(fieldValue) || (fieldExpectedValue instanceof String && !((String) fieldExpectedValue).equalsIgnoreCase((String) fieldValue)))) {
            setBadRequestError(orderDto, ErrorEnum.BAD_REQUEST_INVALID_VALUE, fieldName);
            return false;
        }

        if (!StringUtils.isBlank(fieldValue) && fieldValue instanceof String) {

            // Check value is valid depending on type
            if ((fieldType.equals(Integer.class) && !Utils.isInteger((String) fieldValue)) || (fieldType.equals(Double.class) && !Utils.isDouble((String) fieldValue))
                    || (fieldType.equals(BigDecimal.class) && !Utils.isInteger((String) fieldValue))) {
                setBadRequestError(orderDto, ErrorEnum.BAD_REQUEST_INVALID_VALUE, fieldName);
                return false;
            }

            // Check field size
            if (fieldSize != null && ((String) fieldValue).length() > fieldSize) {
                setBadRequestError(orderDto, ErrorEnum.BAD_REQUEST_INVALID_SIZE, fieldName);
                return false;
            }
        }
        return true;
    }

    /**
     * Create access point
     *
     * @param orderLineDto the order line Dto
     * @param subscription the subscription
     */
    protected void createAccessPoint(OrderLineDto orderLineDto, Subscription subscription) {
        Access accessPoint = new Access();
        accessPoint.setAccessUserId(orderLineDto.getSubscriptionCode());
        accessPoint.setStartDate(orderLineDto.getBillEffectDate());
        accessPoint.setSubscription(subscription);
        accessService.create(accessPoint);
    }

    /**
     * Get service parameters
     *
     * @param serviceInstance the service instance
     * @param serviceInstance the service instance
     * @return the service parameters CF
     */
    @SuppressWarnings("unchecked")
    protected Map<String, String> getServiceParameters(ServiceInstance serviceInstance, CustomFieldValue lastCfValue) {
        Map<String, String> serviceParametersCF = null;
        if (serviceInstance != null && lastCfValue != null && lastCfValue.getMapValue() != null && !lastCfValue.getMapValue().isEmpty()) {
            serviceParametersCF = (Map<String, String>) utilService.getCFValue(serviceInstance, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(),
                    (String) lastCfValue.getMapValue().values().iterator().next());
        }
        return serviceParametersCF;
    }

    /**
     * Get the service parameters
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return the service parameters
     */
    protected Map<String, Object> getServiceParameters(OrderDto orderDto, OrderLineDto orderLineDto) {
        return getServiceParameters(orderDto, orderLineDto, null);
    }

    /**
     * Get the service parameters
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @param entity       the entity
     * @return the service parameters
     */
    protected Map<String, Object> getServiceParameters(OrderDto orderDto, OrderLineDto orderLineDto, ICustomFieldEntity entity) {

        Map<String, Object> serviceParameters = new HashMap<>();

        /**
         * Attribute Aggregate (for all Bill Items)
         */
        serviceParameters.put(ServiceParametersCFEnum.XREF.getLabel(), orderLineDto.getXRefSubscriptionCode());
        serviceParameters.put(ServiceParametersCFEnum.QUANTITY.getLabel(), orderLineDto.getBillQuantity());
        serviceParameters.put(ServiceParametersCFEnum.ACTION_QUALIFIER.getLabel(), orderLineDto.getBillActionQualifier());
        serviceParameters.put(ServiceParametersCFEnum.REF_SERVICE_ID.getLabel(), orderLineDto.getRefSubscriptionCode());
        serviceParameters.put(ServiceParametersCFEnum.ORDER_ID.getLabel(), orderDto.getOrderCode());

        /**
         * Attribute Aggregate (UG Orders)
         */
        if (OrderEmitterEnum.UG.name().equalsIgnoreCase(orderDto.getOrderingSystem())) {
            serviceParameters.put(ServiceParametersCFEnum.EXCHANGE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_EXCHANGE.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.REGION.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_REGION.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.DENSITY.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_DENSITY.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.RANGE_START.getLabel(),
                    !StringUtils.isBlank(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_NEW_RANGE_START.getLabel()))
                            ? orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_NEW_RANGE_START.getLabel())
                            : orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_START.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.RANGE_END.getLabel(),
                    !StringUtils.isBlank(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_NEW_RANGE_END.getLabel()))
                            ? orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_NEW_RANGE_END.getLabel())
                            : orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_END.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.CLASS_OF_SERVICE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_CLASS_OF_SERVICE.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.EF.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_EF.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.AF.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_AF.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.SLA.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_SLA.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.BANDWIDTH.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_BANDWIDTH.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.ORDER_TYPE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_ORDER_TYPE.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.EGRESS_GROUP.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_EGRESS_GROUP.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.MAIN_SERVICE_BILL_CODE.getLabel(), orderLineDto.getMainServiceBillCode());
            serviceParameters.put(ServiceParametersCFEnum.OPERATOR_ORD_NUMBER.getLabel(), orderLineDto.getOperatorOrderNumber());
            serviceParameters.put(ServiceParametersCFEnum.AGENT_CODE.getLabel(), orderLineDto.getAgentCode());

            if (entity instanceof ServiceInstance) {
                String quantity = String.valueOf((((ServiceInstance) entity).getQuantity()).doubleValue());
                serviceParameters.put(ServiceParametersCFEnum.QUANTITY.getLabel(), quantity);

                Date endEngagementDate = ((ServiceInstance) entity).getEndAgreementDate();
                if (endEngagementDate != null) {
                    serviceParameters.put(ServiceParametersCFEnum.END_ENGAGEMENT_DATE.getLabel(), DateUtils.formatDateWithPattern(endEngagementDate, Utils.DATE_TIME_PATTERN));
                }
            }
        } else if (OrderEmitterEnum.OMS.name().equalsIgnoreCase(orderDto.getOrderingSystem())) {

            /**
             * Attribute Aggregate (for all OMS Bill Items)
             */
            serviceParameters.put(ServiceParametersCFEnum.STATE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_STATE.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.STATUS.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_STATUS.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.PROCESSING_STATUS.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_PROCESSING_STATUS.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.COMPONENT_ID.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_COMPONENT_ID.getLabel()));

            serviceParameters.put(ServiceParametersCFEnum.COMMITMENT_PERIOD.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_COMMITMENT_PERIOD.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.PRICE_PLAN.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_PRICE_PLAN.getLabel()));
            String amt = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_AMT.getLabel());
            serviceParameters.put(ServiceParametersCFEnum.AMT.getLabel(), amt);
            serviceParameters.put(ServiceParametersCFEnum.AMTOOC.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_AMTOOC.getLabel()));

            /**
             * Attribute Aggregate (for OMS Main Subscription Details Bill Items)
             */
            if (orderLineDto.isMain()) {
                serviceParameters.put(ServiceParametersCFEnum.SLA.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_MAIN_SLA_IND.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.SERVICE_TYPE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_MAIN_SERVICE_TYPE.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.NOT_BEFORE_DATE.getLabel(),
                        orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_MAIN_NOT_BEFORE_DATE.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.SUBORDER.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_MAIN_SUBORDER.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.COMMITMENT_TREATMENT.getLabel(),
                        orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_MAIN_COMMITMENT_TREATMENT.getLabel()));
                String siteGeneralNumber = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_MAIN_SITE_GENERAL_NUMBER.getLabel());
                serviceParameters.put(ServiceParametersCFEnum.SITE_GENERAL_NUMBER.getLabel(), siteGeneralNumber);
                serviceParameters.put(ServiceParametersCFEnum.SGN_STD_CODE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_MAIN_SGN_STD_CODE.getLabel()));
            } else {
                /**
                 * Attribute Aggregate (for OMS Charges)
                 */
                // serviceParameters.put(ServiceParametersCFEnum.CHARGE_TYPE.getLabel(), orderLineDto.getChargeType()); // just for display R, O or O/R
                serviceParameters.put(ServiceParametersCFEnum.CHARGE_TYPE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_TYPE.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.CHARGE_ID.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_ID.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.DISTANCE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_DISTANCE.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.RATING_SCHEME.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_RATING_SCHEME.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.BANDWIDTH.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_BANDWIDTH.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.TRANSMISSION.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_TRANSMISSION.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.ZONE_A.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_ZONE_A.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.ZONE_B.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_ZONE_B.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.REGION.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_REGION.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.SLA.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_SLA.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.SERVICE_CATEGORY.getLabel(),
                        orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_SERVICE_CATEGORY.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.ANALOGUE_QUALITY.getLabel(),
                        orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_ANALOGUE_QUALITY.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.ANALOGUE_RATE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_ANALOGUE_RATE.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.CPE_TYPE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_TYPE.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.CPE_DESCRIPTION.getLabel(),
                        orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_DESCRIPTION.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.ETS_TYPE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_ETS_TYPE.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.IND_01.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_01.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.IND_02.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_02.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.IND_03.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_03.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.IND_04.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_04.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.IND_05.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_05.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.IND_06.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_06.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.IND_07.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_07.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.IND_08.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_08.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.IND_09.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_09.getLabel()));
                serviceParameters.put(ServiceParametersCFEnum.IND_10.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_10.getLabel()));
            }
        }
        return serviceParameters;
    }

    /**
     * Get service parameters CF
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @param entity       the entity
     * @return service parameters CF
     */
    protected Map<String, String> getServiceParametersCF(OrderDto orderDto, OrderLineDto orderLineDto, ICustomFieldEntity entity) {

        Map<String, String> cfValue = new HashMap<>();
        Map<String, Object> serviceInstanceValuesCF = getServiceParameters(orderDto, orderLineDto, entity);

        String key = orderLineDto.getItemId() + CustomFieldValue.MATRIX_KEY_SEPARATOR;
        if (!StringUtils.isBlank(orderLineDto.getItemComponentId())) {
            key = key + orderLineDto.getItemComponentId();
        }
        cfValue.put(key, utilService.getCFValue(entity, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), serviceInstanceValuesCF));
        return cfValue;
    }

    /**
     * Get service parameters CF
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @param entity       the entity
     * @param lastCfValue  the last cf value
     * @return service parameters CF
     */
    protected Map<String, String> getServiceParametersCF(OrderDto orderDto, OrderLineDto orderLineDto, ICustomFieldEntity entity, CustomFieldValue lastCfValue) {

        if (lastCfValue == null) {
            lastCfValue = utilService.getLastCfVersion(entity, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), DateUtils.truncateTime(orderLineDto.getBillEffectDate()), false);
        }
        if (lastCfValue != null && lastCfValue.getMapValue() != null && !lastCfValue.getMapValue().isEmpty()) {
            Map<String, String> serviceParametersValues = (Map<String, String>) utilService.getCFValue(entity, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(),
                    (String) lastCfValue.getMapValue().values().iterator().next());
            if (serviceParametersValues != null) {
                Map<String, Object> serviceParameters = getServiceParameters(orderDto, orderLineDto, entity);
                Map<String, Object> newServiceParametersValues = new HashMap<>(serviceParametersValues);

                serviceParameters.values().removeIf(item -> StringUtils.isBlank(item));
                newServiceParametersValues.putAll(serviceParameters);

                String key = orderLineDto.getItemId() + CustomFieldValue.MATRIX_KEY_SEPARATOR;
                if (!StringUtils.isBlank(orderLineDto.getItemComponentId())) {
                    key = key + orderLineDto.getItemComponentId();
                }
                Map<String, String> newServiceParametersCF = new HashMap<>();
                newServiceParametersCF.put(key, utilService.getCFValue(entity, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), newServiceParametersValues));
                return newServiceParametersCF;
            }
        }
        return null;
    }

    /**
     * Get charge initialization parameters from a tariff line
     *
     * @param orderDto      the order Dto
     * @param orderLineDto  Order line Dto
     * @param totalQuantity Quantity
     * @param chargeType    Charge type
     * @return Charge initialization parameters from the tariff
     * @throws BusinessException the business exception
     */
    private ChargeInitializationParametersDto getChargeInitParametersFromTariff(OrderDto orderDto, OrderLineDto orderLineDto, BigDecimal totalQuantity, ChargeTypeEnum chargeType)
            throws BusinessException {

        OfferTemplate offerTemplate = orderService.findEntityByIdOrCode(offerTemplateService, orderDto, orderLineDto.getOfferTemplateCode());
        ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
        BillingAccount billingAccount = orderService.findEntityByIdOrCode(billingAccountService, orderDto, orderLineDto.getBillingAccountCode());
        Map<String, Object> serviceParameters = getServiceParameters(orderDto, orderLineDto);

        return utilService.getChargeInitParametersFromTariff(offerTemplate, serviceTemplate, billingAccount, serviceParameters, orderLineDto.getBillEffectDate(), totalQuantity,
                chargeType);
    }

    /**
     * Activate service
     *
     * @param orderDto                 the order Dto
     * @param orderLineDto             the order line Dto
     * @param subscription             the subscription
     * @param applySubscriptionCharges Indicates if applying the subscription charges or not.
     * @return the service instance
     * @throws BusinessException the business exception
     */
    protected ServiceInstance activateService(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription, boolean applySubscriptionCharges) throws BusinessException {
        if (!validateNewServiceInstance(orderDto, orderLineDto, subscription)) {
            return null;
        }

        ServiceInstance serviceInstance = createServiceInstance(orderDto, orderLineDto);

        // activate service
        serviceInstanceService.serviceInstanciation(serviceInstance);

        if (serviceInstance.getRecurringChargeInstances() != null && !serviceInstance.getRecurringChargeInstances().isEmpty()) {
            ChargeInitializationParametersDto chargeInitParameters = getChargeInitParametersFromTariff(orderDto, orderLineDto, serviceInstance.getQuantity(), ChargeTypeEnum.R);
            utilService.updateRecurringChargeInstances(serviceInstance, chargeInitParameters);
        }

        if (serviceInstance.getSubscriptionChargeInstances() != null && !serviceInstance.getSubscriptionChargeInstances().isEmpty()) {
            ChargeInitializationParametersDto chargeInitParameters = getChargeInitParametersFromTariff(orderDto, orderLineDto, serviceInstance.getQuantity(), ChargeTypeEnum.O);
            utilService.updateSubscriptionChargeInstances(serviceInstance, chargeInitParameters);
        }
        serviceInstanceService.serviceActivation(serviceInstance, applySubscriptionCharges, false);
        return serviceInstance;
    }

    /**
     * Get service template
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the The order line Dto
     * @return
     */
    protected ServiceTemplate getServiceTemplate(OrderDto orderDto, OrderLineDto orderLineDto) {
        ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
        if (serviceTemplate == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Service is not found : " + orderLineDto.getBillCode());
        }
        return serviceTemplate;
    }

    /**
     * Get cease effect date
     *
     * @param orderDto        the order Dto
     * @param orderLineDto    the order line Dto
     * @param serviceInstance the service instance
     * @return cease effect date
     */
    protected Date getCeaseEffectDate(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance) {
        Date ceaseEffectDate = orderLineDto.getCeaseEffectDate();
        if (ceaseEffectDate == null) {
            ceaseEffectDate = orderLineDto.getBillEffectDate();
        } else if (ceaseEffectDate.before(DateUtils.truncateTime(serviceInstance.getSubscription().getSubscriptionDate()))) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Termination date can not be before the subscription date for the service " + serviceInstance.getCode());
            return null;
        }
        // return ceaseEffectDate;
        return DateUtils.truncateTime(ceaseEffectDate);
    }

    /**
     * Terminate ancillary service
     *
     * @param orderDto         the order Dto
     * @param orderLineDto     the order line Dto
     * @param ancillaryService the ancillary service
     * @throws BusinessException the business exception
     */
    protected void terminateAncillaryService(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance ancillaryService) throws BusinessException {
        if (orderDto.getErrorCode() != null) {
            return;
        }
        try {
            if (orderLineDto.getCeaseEffectDate() != null) {
                SubscriptionTerminationReason terminationReasonAncillary = orderService.findEntityByIdOrCode(subscriptionTerminationReasonService, orderDto,
                        TerminationReasonEnum.TR_NO_OVERRIDE_AGR_RREFUND_CHARGE.name());
                serviceInstanceService.terminateService(ancillaryService, orderLineDto.getCeaseEffectDate(), terminationReasonAncillary,
                        orderDto.getOrderCode());
                orderDto.getServiceInstances().put(ancillaryService.getId(), orderLineDto);
            }
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }

    /**
     * Terminate primary service
     *
     * @param orderDto       the order Dto
     * @param orderLineDto   the order line Dto
     * @param primaryService the primary service
     * @throws BusinessException the business exception
     */
    protected void terminatePrimaryService(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance primaryService) throws BusinessException {
        if (orderDto.getErrorCode() != null) {
            return;
        }
        try {
            if (orderLineDto.getCeaseEffectDate() != null) {
                SubscriptionTerminationReason terminationReasonAncillary = orderService.findEntityByIdOrCode(subscriptionTerminationReasonService, orderDto,
                        TerminationReasonEnum.TR_NO_OVERRIDE_AGR_RREFUND_CHARGE.name());
                serviceInstanceService.terminateService(primaryService, orderLineDto.getCeaseEffectDate(), terminationReasonAncillary, orderDto.getOrderCode());
                orderDto.getServiceInstances().put(primaryService.getId(), orderLineDto);
            }
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }

    /**
     * Get engagement duration for a customer & offer code.
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return Min Term duration
     * @throws BusinessException the business exception
     */
    protected Long getEngagementDuration(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        BillingAccount billingAccount = orderService.findEntityByIdOrCode(billingAccountService, orderDto, orderLineDto.getBillingAccountCode());
        String customerCode = billingAccount != null ? billingAccount.getCustomerAccount().getCustomer().getCode() : null;
        Long minTermDuration = getEngagementDuration(customerCode, orderLineDto.getOfferTemplateCode(), orderLineDto.getBillEffectDate());
        if (minTermDuration != null) {
            return minTermDuration;
        }
        minTermDuration = getEngagementDuration(customerCode, "*", orderLineDto.getBillEffectDate());
        if (minTermDuration != null) {
            return minTermDuration;
        }
        minTermDuration = getEngagementDuration("*", orderLineDto.getOfferTemplateCode(), orderLineDto.getBillEffectDate());
        if (minTermDuration != null) {
            return minTermDuration;
        }
        minTermDuration = getEngagementDuration("*", "*", orderLineDto.getBillEffectDate());
        if (minTermDuration != null) {
            return minTermDuration;
        }

        return minTermDuration;
    }

    /**
     * Get engagement duration for a customer & offer code.
     *
     * @param customerCode the customer code
     * @param offerCode    the offer code
     * @param effectDate   Effect Date
     * @return Min Term duration
     */
    @SuppressWarnings("unchecked")
    private Long getEngagementDuration(String customerCode, String offerCode, Date effectDate) {
        Map<String, String> cf = (Map<String, String>) providerService.getProvider().getCfValue(ProviderCFsEnum.ENGAGEMENT_DURATION.name());
        if (cf == null) {
            return null;
        }
        for (Map.Entry<String, String> entry : cf.entrySet()) {
            String[] keys = entry.getKey().split(Pattern.quote(CustomFieldValue.MATRIX_KEY_SEPARATOR));
            if (keys.length < 2) {
                continue;
            }
            String customer = keys[EngagementDurationAttributeNamesEnum.Customer.ordinal()];
            String offer = keys[EngagementDurationAttributeNamesEnum.Offer.ordinal()];

            Date startDate = null;
            if (keys.length > 2) {
                startDate = !StringUtils.isBlank(keys[EngagementDurationAttributeNamesEnum.startDate.ordinal()])
                        ? DateUtils.parseDateWithPattern(keys[EngagementDurationAttributeNamesEnum.startDate.ordinal()], Utils.DATE_DEFAULT_PATTERN)
                        : null;
            }

            Date endDate = null;
            if (keys.length > 3) {
                endDate = !StringUtils.isBlank(keys[EngagementDurationAttributeNamesEnum.endDate.ordinal()])
                        ? DateUtils.parseDateWithPattern(keys[EngagementDurationAttributeNamesEnum.endDate.ordinal()], Utils.DATE_DEFAULT_PATTERN)
                        : null;
            }

            if (isSameAttributeValue(customerCode, customer) && isSameAttributeValue(offerCode, offer)) {
                if (startDate == null) {
                    continue;
                } else if (endDate == null) {
                    if (startDate.before(effectDate)) {
                        return Utils.toLong(entry.getValue());
                    }
                } else {
                    if (startDate.before(effectDate) && endDate.after(effectDate)) {
                        return Utils.toLong(entry.getValue());
                    }
                }
            }
        }
        return null;
    }

    /**
     * Get offer template
     *
     * @param orderDto        the order Dto
     * @param serviceTemplate the service template
     * @return offer template
     */
    protected OfferTemplate getOfferTemplate(OrderDto orderDto, ServiceTemplate serviceTemplate) {
        OfferTemplate offerTemplate = null;
        if (serviceTemplate != null) {
            String offerCode = orderDto.getServiceOfferTemplates().get(serviceTemplate.getCode());
            if (!StringUtils.isBlank(offerCode)) {
                offerTemplate = orderService.findEntityByIdOrCode(offerTemplateService, orderDto, offerCode);
            }
            if (offerTemplate == null) {
                List<OfferTemplate> offerTemplates = offerTemplateService.findByServiceTemplate(serviceTemplate);
                if (offerTemplates != null && !offerTemplates.isEmpty()) {
                    offerTemplate = offerTemplates.get(0);
                    orderDto.getServiceOfferTemplates().put(serviceTemplate.getCode(), offerTemplate.getCode());
                }
            }
        }
        return offerTemplate;
    }

    /**
     * Get the productSet code
     *
     * @param orderDto        the order Dto
     * @param serviceTemplate the service template
     * @return the productSet code
     */
    protected String getProductSetCode(OrderDto orderDto, ServiceTemplate serviceTemplate) {
        String productSetCode = "";
        if (serviceTemplate != null) {
            EntityReferenceWrapper productSetEntity = (EntityReferenceWrapper) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.PRODUCT_SET.name());
            if (productSetEntity != null && StringUtils.isNotBlank(productSetEntity.getCode())) {
                Long id = Long.valueOf(productSetEntity.getCode());
                productSetCode = (String) orderDto.getEntities().get(CustomTableEnum.PRODUCT_SET.getCode() + "_" + id);
                if (!StringUtils.isBlank(productSetCode)) {
                    return productSetCode;
                }
                Map<String, Object> productSetRecord = customTableService.findById(CustomTableEnum.PRODUCT_SET.getCode(), id);
                if (productSetRecord != null && !productSetRecord.isEmpty()) {
                    productSetCode = ((String) productSetRecord.get("code"));
                    if (!StringUtils.isBlank(productSetCode)) {
                        productSetCode = productSetCode.replace("_", "");
                        orderDto.getEntities().put(CustomTableEnum.PRODUCT_SET.getCode() + "_" + id, productSetCode);
                    }
                }
            }
        }
        return productSetCode;
    }

    /**
     * Get ProductSet with prefix code attached
     *
     * @param orderDto     the order Dto
     * @param orderLineDto Order Line
     * @return ProductSet with prefix code
     */
    protected String getProductSetPrefixCode(OrderDto orderDto, OrderLineDto orderLineDto) {
        String productSetPrefixCode = getProductSetCode(orderDto, orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode()));
        return !StringUtils.isBlank(productSetPrefixCode) ? productSetPrefixCode + "_" + orderLineDto.getOperatorId() : orderLineDto.getOperatorId();
    }

    /**
     * Get subscription code with prefix attached
     *
     * @param orderDto     the order Dto
     * @param orderLineDto Order Line Dto
     * @return Prefixed subscription code
     */
    protected String getSubscriptionCodeWithPrefix(OrderDto orderDto, OrderLineDto orderLineDto) {
        try {
            ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
            if (serviceTemplate != null) {
                // get Prefix from catalog
                EntityReferenceWrapper subscriptionPrefix = (EntityReferenceWrapper) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.SUBSCRIBER_PREFIX.name());

                if (subscriptionPrefix != null && !StringUtils.isBlank(subscriptionPrefix.getCode())) {
                    return subscriptionPrefix.getCode().replace("_", "") + "_" + orderLineDto.getSubscriptionCode();
                }
            }
            return orderLineDto.getSubscriptionCode();
        } catch (Exception ex) {
            log.error("Problem extracting Subscription prefix");
            return orderLineDto.getSubscriptionCode();
        }
    }

    /**
     * Get the main service instance
     *
     * @param subscription the subscription
     * @return the main service instance
     */
    protected ServiceInstance getMainServiceInstance(Subscription subscription) {
        ServiceInstance mainServiceInstance = null;
        if (subscription != null && subscription.getServiceInstances() != null) {
            for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
                String serviceType = (String) serviceInstance.getServiceTemplate().getCfValue(ServiceTemplateCFsEnum.SERVICE_TYPE.name());
                if (!StringUtils.isBlank(serviceType) && ServiceTypeEnum.MAIN.getLabel().equalsIgnoreCase(serviceType.trim()) &&
                        serviceInstance.getStatus() == InstanceStatusEnum.ACTIVE) {
                    if (serviceInstance.getSubscribedTillDate() == null) {
                        return serviceInstance;
                    }
                    if (mainServiceInstance == null || serviceInstance.getSubscribedTillDate().after(mainServiceInstance.getSubscribedTillDate())) {
                        mainServiceInstance = serviceInstance;
                    }
                }
            }
        }
        return mainServiceInstance;
    }

    /**
     * Get the main service instance from bill code
     *
     * @param serviceInstances service instances
     * @param billCode         bill code
     * @return service instance
     */
    private ServiceInstance getMainServiceInstance(List<ServiceInstance> serviceInstances, String billCode) {
        ServiceInstance mainServiceInstance = null;
        if (serviceInstances != null && !serviceInstances.isEmpty()) {
            for (ServiceInstance serviceInstance : serviceInstances) {
                if (serviceInstance.getCode().equalsIgnoreCase(billCode) && serviceInstance.getStatus() == InstanceStatusEnum.ACTIVE) {
                    if (serviceInstance.getSubscribedTillDate() == null) {
                        return serviceInstance;
                    }
                    if (mainServiceInstance == null || serviceInstance.getSubscribedTillDate().after(mainServiceInstance.getSubscribedTillDate())) {
                        mainServiceInstance = serviceInstance;
                    }
                }
            }
        }
        return mainServiceInstance;
    }

    /**
     * Get  service instance
     *
     * @param orderDto     the order dto
     * @param orderLineDto the order line dto
     * @param subscription the subscription
     * @return the ancillary service instance
     */
    protected ServiceInstance getServiceInstance(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription) {
        ServiceInstance serviceInstance = null;
        if (orderLineDto.isMain()) {
            serviceInstance = getMainServiceInstance(subscription.getServiceInstances(), orderLineDto.getBillCode());
        } else {
            serviceInstance = getAncillaryServiceInstance(orderDto, orderLineDto, subscription);
        }
        return serviceInstance;
    }

    /**
     * Check if new ranges are valid against the existing ones
     *
     * @param existingRangeStart the existing range start
     * @param existingRangeEnd   the existing range end
     * @param newRangeStart      the new range start
     * @param newRangeEnd        the new range end
     * @return true if new ranges are valid against the existing ones
     */
    protected boolean isValidRanges(Long existingRangeStart, Long existingRangeEnd, Long newRangeStart, Long newRangeEnd) {
        return !((newRangeStart != null && newRangeStart >= existingRangeStart && newRangeStart <= existingRangeEnd)
                || (newRangeEnd != null && newRangeEnd >= existingRangeStart && newRangeEnd <= existingRangeEnd));
    }

    /**
     * Check if new ranges are valid against the existing ones
     *
     * @param orderDto   the order Dto
     * @param rangeStart the range start
     * @param rangeEnd   the range end
     * @return true if new ranges are valid against the existing ones
     */
    protected boolean isValidRanges(OrderDto orderDto, String rangeStart, String rangeEnd) {
        if (!StringUtils.isBlank(rangeStart) || !StringUtils.isBlank(rangeEnd)) {

            Long newRangeStart = null;
            if (!StringUtils.isBlank(rangeStart)) {
                try {
                    newRangeStart = Long.valueOf(rangeStart.replaceAll("-", ""));
                } catch (NumberFormatException nfe) {
                }
            }

            Long newRangeEnd = null;
            if (!StringUtils.isBlank(rangeEnd)) {
                try {
                    newRangeEnd = Long.valueOf(rangeEnd.replaceAll("-", ""));
                } catch (NumberFormatException nfe) {
                }
            }

            if (!isValidRanges(rangeStart, rangeEnd)) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "NEW_RANGE_START and NEW_RANGE_END already exist or interfers with existing one");
                return false;
            }

            // check if new range not already taken
            List<Subscription> subs = subscriptionService.list();
            for (Subscription s : subs) {

                Long newSubscriptionCode = null;
                try {
                    newSubscriptionCode = Long.valueOf(s.getCode().replaceAll("-", ""));
                } catch (NumberFormatException nfe) {
                }

                return (!(newRangeStart != null && newRangeStart == newSubscriptionCode
                        || (newRangeStart != null && newRangeEnd != null && newRangeEnd >= newSubscriptionCode && newRangeStart <= newSubscriptionCode)));

            }
        }
        return true;
    }

    /**
     * Is same attribute value
     *
     * @param itemOrderAttributeValue the item order attribute Value
     * @param cfAttributeValue
     * @return true is the same attribute value
     */
    protected boolean isSameAttributeValue(String itemOrderAttributeValue, String cfAttributeValue) {
        boolean isSameValue = false;
        if ((StringUtils.isBlank(itemOrderAttributeValue) && (StringUtils.isBlank(cfAttributeValue) || "null".equalsIgnoreCase(cfAttributeValue)))
                || (!StringUtils.isBlank(itemOrderAttributeValue) && !StringUtils.isBlank(cfAttributeValue) && itemOrderAttributeValue.equalsIgnoreCase(cfAttributeValue))
                || ("*".equalsIgnoreCase(itemOrderAttributeValue) && !StringUtils.isBlank(cfAttributeValue)
                && (cfAttributeValue.equalsIgnoreCase("*") || cfAttributeValue.equalsIgnoreCase("***")))) {
            isSameValue = true;
        }
        return isSameValue;
    }

    /**
     * Get key
     *
     * @param matrixKey the matrix key
     * @param searchKey the search key
     * @return the key
     */
    private String getKey(String matrixKey, String searchKey) {
        String key = "";
        if (!StringUtils.isBlank(matrixKey)) {
            key = matrixKey.trim();
            if (key.equals("*") && !StringUtils.isBlank(searchKey)) {
                key = searchKey.charAt(0) + key;
            }
            String[] tabFrom = key.split("\\*");
            if (tabFrom.length > 0) {
                key = tabFrom[0]; // 10_01_
            }
        }
        return key;
    }

    /**
     * Get product migration code
     *
     * @param migrationMatrix Migration matrix
     * @param from            Migration Code Source
     * @param to              Migration Code Target
     * @param eventType       the event type
     * @return Migration Code
     */
    protected String getProductMigrationCode(Map<String, String> migrationMatrix, String from, String to, EventTypeEnum eventType) {
        int score = 0;
        List<String> bestMatches = new ArrayList<>();
        for (Map.Entry<String, String> entry : migrationMatrix.entrySet()) {
            String key = entry.getKey(); // 10_10_10|10_10_10|=0

            String[] tab = key.split(Pattern.quote(CustomFieldValue.MATRIX_KEY_SEPARATOR));
            if (tab.length == 0) {
                continue;
            }

            String fromKey = key.split(Pattern.quote(CustomFieldValue.MATRIX_KEY_SEPARATOR))[0];
            String toKey = key.split(Pattern.quote(CustomFieldValue.MATRIX_KEY_SEPARATOR))[1];

            String eventKey = "";
            if (tab.length > 3) {
                eventKey = key.split(Pattern.quote(CustomFieldValue.MATRIX_KEY_SEPARATOR))[3];
            } else if (eventType != null) {
                continue;
            }

            String fromCompare = getKey(fromKey, from);
            String toCompare = getKey(toKey, to);

            if ((!StringUtils.isBlank(from) && (from.equals(fromCompare) || from.startsWith(fromCompare)))
                    && (StringUtils.isBlank(to) || (to.equals(toCompare) || to.startsWith(toCompare))) && eventType.name().equalsIgnoreCase(eventKey)) {
                if (score == (fromCompare.length() + toCompare.length())) {
                    bestMatches.add(key);
                } else if (score < (fromCompare.length() + toCompare.length())) {
                    score = fromCompare.length() + toCompare.length();
                    bestMatches.clear();
                    bestMatches.add(key);
                }
            }
        }
        if (bestMatches.size() == 1) {
            return bestMatches.get(0);
        } else if (bestMatches.size() > 1) {
            if (StringUtils.isNotBlank(from) && StringUtils.isNotBlank(to)) {
                int fromRR = Integer.parseInt(from.split(Pattern.quote("_"))[2].trim());
                int toRR = Integer.parseInt(to.split(Pattern.quote("_"))[2].trim());
                int condition = fromRR - toRR;
                for (String match : bestMatches) {
                    String conditionRule = match.split(Pattern.quote(CustomFieldValue.MATRIX_KEY_SEPARATOR))[2].trim(); // =0 <1 >1
                    if (condition >= 1
                            && (conditionRule.contains(">1") || conditionRule.contains("RR-RR>1") || conditionRule.contains(">0") || conditionRule.contains("RR-RR>0"))) {
                        return match;
                    } else if (condition == 0 && (conditionRule.contains("=0") || conditionRule.contains("RR-RR=0"))) {
                        return match;
                    } else if (condition < 1
                            && (conditionRule.contains("<1") || conditionRule.contains("RR-RR<1") || conditionRule.contains("<0") || conditionRule.contains("RR-RR<0"))) {
                        return match;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Get product migration record
     *
     * @param migrationMatrix    the migration matrix
     * @param migrationMatrixKey the migration matrix key
     * @return the product migration record
     */
    private Map<String, Object> getProductMigrationRecord(Map<String, String> migrationMatrix, String migrationMatrixKey) {
        String value = migrationMatrix.get(migrationMatrixKey);
        if (!StringUtils.isBlank(value)) {
            return (Map<String, Object>) utilService.getCFValue(providerService.getProvider(), ProviderCFsEnum.MIGRATION_MATRIX.name(), value);
        }
        return null;
    }

    /**
     * Get product migration record
     *
     * @param orderDto  the order Dto
     * @param billCode  the bill code
     * @param from      the migration from
     * @param to        the migration to
     * @param eventType the event type
     * @return the migration record
     */
    @SuppressWarnings("unchecked")
    protected Map<String, Object> getProductMigrationRecord(OrderDto orderDto, String billCode, String from, String to, EventTypeEnum eventType) {
        Map<String, String> cf = (Map<String, String>) providerService.getProvider().getCfValue(ProviderCFsEnum.MIGRATION_MATRIX.name());
        if (cf == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Migration Matrix is not found");
            return null;
        }
        String key = null;
        String value = null;
        if ((!StringUtils.isBlank(from) && !StringUtils.isBlank(to)) || (!StringUtils.isBlank(from) && (eventType == EventTypeEnum.CEASE || eventType == EventTypeEnum.TRANSFER))) {
            key = getProductMigrationCode(cf, from, to, eventType);
        }
        if (StringUtils.isBlank(key)) {
            key = "*|*|=0|" + eventType;
            Map<String, Object> valueMap = getProductMigrationRecord(cf, key);
            if (valueMap != null && !valueMap.isEmpty()) {
                return valueMap;
            }

            key = "*|*|null|" + eventType;
            valueMap = getProductMigrationRecord(cf, key);
            if (valueMap != null && !valueMap.isEmpty()) {
                return valueMap;
            }
        }
        value = cf.get(key);

        if (StringUtils.isBlank(value)) {
            List<String> criteria = new ArrayList<>();
            criteria.add(" From = " + from);
            criteria.add(" To = " + to);
            criteria.add(" Event type = " + (eventType != null ? eventType.name() : null));
            String errorMessage = "The product migration record / the rules to apply are not found in migration matrix for the bill code " + billCode;
            if (!criteria.isEmpty()) {
                errorMessage = errorMessage + " (" + String.join(", ", criteria) + ")";
            }
            setError(orderDto, ErrorEnum.INVALID_DATA, errorMessage);
            return null;
        }
        return (Map<String, Object>) utilService.getCFValue(providerService.getProvider(), ProviderCFsEnum.MIGRATION_MATRIX.name(), value);
    }

    /**
     * Apply service to apply.
     *
     * @param orderDto       the order Dto
     * @param orderLineDto   the order line Dto
     * @param subscription   the subscription
     * @param serviceToApply the service to apply
     * @throws BusinessException the business exception
     */
    protected void applyServiceToApply(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription, String serviceToApply) throws BusinessException {
        if (serviceToApply != null) {
            log.debug("Will apply a cease charge for service {}", serviceToApply);

            OfferTemplate miscOfferTemplate = orderService.findEntityByIdOrCode(offerTemplateService, orderDto, OfferTemplateEnum.MISC_WB.name());
            // present,
            // therefore offer is irrevelant
            if (miscOfferTemplate == null) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "The offer template MISC_WB is not found");
                return;
            }

            OneShotChargeTemplate chargetemplate = orderService.findEntityByIdOrCode(oneShotChargeTemplateService, orderDto, ChargeTemplateEnum.MISC_CHARGE.name());
            if (chargetemplate == null) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "The one-shot charge " + ChargeTemplateEnum.MISC_CHARGE.name() + " is not found");
                return;
            }

            OrderLineDto oneShotOrderLineDTO = SerializationUtils.clone(orderLineDto);
            oneShotOrderLineDTO.setBillCode(serviceToApply);
            // template is considered and in case of OTHER charge, a misc service is hardcoded

            OneShotChargeInstance chargeInstance = new OneShotChargeInstance();
            Map<String, String> chargeCF = getServiceParametersCF(orderDto, orderLineDto, chargeInstance);
            chargeInstance.setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), chargeCF);

            try {
                oneShotChargeInstanceService.oneShotChargeApplication(subscription, null, chargetemplate, null, orderLineDto.getBillEffectDate(), null, null, null, serviceToApply, "0", null, null,
                        orderDto.getOrderCode(), chargeInstance.getCfValues(), true, ChargeApplicationModeEnum.SUBSCRIPTION);

            } catch (RatingException exception) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "No price found for a misc charge");
            }
        }
    }

    /**
     * Update service parameters CF
     *
     * @param orderDto        the order Dto
     * @param orderLineDto    the order line Dto
     * @param serviceInstance the service instance
     * @param startDate       the start date
     * @param endDate         the end date
     * @param lastCfValue     the last CF value
     * @param cfValue         the cf value
     */
    protected void updateServiceParametersCF(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance, Date startDate, Date endDate,
                                             CustomFieldValue lastCfValue, Map<String, String> cfValue) {
        if (cfValue == null) {
            cfValue = getServiceParametersCF(orderDto, orderLineDto, serviceInstance);
        }
        utilService.updateCfValue(serviceInstance, startDate != null ? DateUtils.truncateTime(startDate) : DateUtils.truncateTime(orderLineDto.getBillEffectDate()),
                ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), DateUtils.truncateTime(endDate), cfValue, lastCfValue);
    }

    /**
     * create new version service parameters CF
     *
     * @param orderDto        the order Dto
     * @param orderLineDto    the order line Dto
     * @param serviceInstance the service instance
     * @param cfValue         the cf value
     * @throws BusinessException the business exception
     */
    protected void updateServiceParametersCF(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance, Map<String, String> cfValue) throws BusinessException {
        CustomFieldValue lastCfValue = utilService.getLastCfVersion(serviceInstance, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(),
                DateUtils.truncateTime(orderLineDto.getBillEffectDate()), false);

        /** Store service parameters CF **/
        updateServiceParametersCF(orderDto, orderLineDto, serviceInstance, null, null, lastCfValue, cfValue);

    }

    /**
     * Update service parameters CF
     *
     * @param orderDto        the order Dto
     * @param orderLineDto    the order line Dto
     * @param serviceInstance the service instance
     */
    protected void updateServiceParametersCF(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance) {
        updateServiceParametersCF(orderDto, orderLineDto, serviceInstance, null);
    }

    /**
     * Update service parameters CF
     *
     * @param orderDto              the order Dto
     * @param orderLineDto          the order line Dto
     * @param serviceInstance       the service instance
     * @param updateFromLastVersion update the new service parameters version from the last one
     * @throws BusinessException the business exception
     */
    protected void updateServiceParametersCF(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance, boolean updateFromLastVersion)
            throws BusinessException {
        updateServiceParametersCF(orderDto, orderLineDto, serviceInstance, null, updateFromLastVersion);
    }

    /**
     * Update service parameters CF
     *
     * @param orderDto              the order Dto
     * @param orderLineDto          the order line Dto
     * @param serviceInstance       the service instance
     * @param lastCfValue           the last Cf value
     * @param updateFromLastVersion update the new service parameters version from the last one
     * @throws BusinessException the business exception
     */
    protected void updateServiceParametersCF(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance, CustomFieldValue lastCfValue,
                                             boolean updateFromLastVersion) throws BusinessException {
        if (lastCfValue == null) {
            lastCfValue = utilService.getLastCfVersion(serviceInstance, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), DateUtils.truncateTime(orderLineDto.getBillEffectDate()),
                    true);
        }
        Map<String, String> newServiceParametersCF = null;
        if (updateFromLastVersion) {
            newServiceParametersCF = getServiceParametersCF(orderDto, orderLineDto, serviceInstance, lastCfValue);
        }
        updateServiceParametersCF(orderDto, orderLineDto, serviceInstance, null, orderLineDto.getCeaseEffectDate(), lastCfValue, newServiceParametersCF);
    }

    /**
     * Set service instance Cf
     *
     * @param orderDto        the order Dto
     * @param orderLineDto    the order line Dto
     * @param serviceInstance the service instance
     */
    protected void setServiceInstanceCf(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance) {
        // Set the SERVICE_PARAMETERS cf
        updateServiceParametersCF(orderDto, orderLineDto, serviceInstance, null, null, null, null);

        // Set ITEM_COMPONENT_ID cf
        serviceInstance.setCfValue(ServiceInstanceCFsEnum.ITEM_COMPONENT_ID.name(), orderLineDto.getItemComponentId());

        // Set RANGE_START and RANGE_END cfs
        updateRangesCfs(orderDto, orderLineDto, serviceInstance);
    }

    /**
     * Set RANGE_START and RANGE_END cfs
     *
     * @param orderLineDto
     * @param serviceInstance
     */
    protected void updateRangesCfs(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance) {

        if (OrderEmitterEnum.UG.name().equalsIgnoreCase(orderDto.getOrderingSystem())) {
            String rangeStart = !StringUtils.isBlank(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_NEW_RANGE_START.getLabel()))
                    ? orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_NEW_RANGE_START.getLabel())
                    : orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_START.getLabel());
            String rangeEnd = !StringUtils.isBlank(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_NEW_RANGE_END.getLabel()))
                    ? orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_NEW_RANGE_END.getLabel())
                    : orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_END.getLabel());

            if (rangeStart != null) {
                rangeStart = rangeStart.replaceAll("[^\\d]", "");
            }
            if (rangeEnd != null) {
                rangeEnd = rangeEnd.replaceAll("[^\\d]", "");
            }

            if (!StringUtils.isBlank(rangeStart)) {
                serviceInstance.setCfValue(ServiceInstanceCFsEnum.RANGE_START.name(), Long.valueOf(rangeStart));
            }
            if (!StringUtils.isBlank(rangeEnd)) {
                serviceInstance.setCfValue(ServiceInstanceCFsEnum.RANGE_END.name(), Long.valueOf(rangeEnd));
            }

        }
    }

    /**
     * Get complementary order
     *
     * @param orderDto the order Dto
     * @return the complementary order
     */
    protected Order getComplementaryOrder(OrderDto orderDto) {
        Order order = orderService.findEntityByIdOrCode(orderService, orderDto, orderDto.getRefOrderCode());
        if (order != null && (order.getStatus() == OrderStatusEnum.COMPLETED || order.getStatus() == OrderStatusEnum.DEFERRED)) {
            return order;
        }
        return orderService.findComplementaryOrder(orderDto.getOrderCode());
    }

    /**
     * Checks if is held order.
     *
     * @param orderDto the order Dto
     * @return true, if is held order
     */
    protected boolean isHeldOrder(OrderDto orderDto) {
        if (orderDto.getProcessingType() == OrderProcessingTypeEnum.RELEASE) {
            return false;
        }
        List<String> orders = orderService.findPendingRejectedWaitingOrders(orderDto, true);
        if (orders != null && !orders.isEmpty()) {
            orderDto.setHeldBy(orders.get(0));
            setError(orderDto, OrderStatusEnum.HELD, "Service Id already exists against an order that is in Error Management : " + orders.get(0));
            return true;
        }
        return false;
    }

    /**
     * Checks if is deferred order.
     *
     * @param orderDto the order Dto
     * @return true, if is deferred order
     */
    protected boolean isDeferredOrder(OrderDto orderDto) {
        if (orderDto.getProcessingType() == OrderProcessingTypeEnum.RELEASE || StringUtils.isBlank(orderDto.getRefOrderCode())) {
            return false;
        }
        Order refOrder = orderService.findEntityByIdOrCode(orderService, orderDto, orderDto.getRefOrderCode());
        if (refOrder == null || (!OrderStatusEnum.COMPLETED.equals(refOrder.getStatus()) && !OrderStatusEnum.CANCELLED.equals(refOrder.getStatus()))) {
            for (OrderLineDto orderLineDto : orderDto.getOrderLines()) {
                // if the action is P and refOrderCode is filled and order ( by refIdOreder) not found or has a status other than completed
                if (BillActionEnum.P.name().equals(orderLineDto.getBillAction())) {
                    setError(orderDto, OrderStatusEnum.DEFERRED,
                            "The order is set to the status Deferred until the accompanying order " + orderDto.getRefOrderCode() + " is received and successfully processed");
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Checks if is waiting order.
     *
     * @param orderDto the order Dto
     * @return true, if is waiting order
     */
    protected boolean isWaitingOrder(OrderDto orderDto) {
        if (orderDto.getProcessingType() == OrderProcessingTypeEnum.RELEASE || !StringUtils.isBlank(orderDto.getRefOrderCode())) {
            return false;
        }
        List<CustomEntityInstance> cetList = customEntityInstanceService.listByCet(CET_CIRCUIT_TYPE_MAPPING);
        CustomEntityInstance matchedCrtType;
        for (OrderLineDto orderLineDto : orderDto.getOrderLines()) {
            // if the action is P and refOrderCode is empty and subscriptionCode matches a circuit_type_mapping
            if (orderLineDto.isMain() && BillActionEnum.P.name().equals(orderLineDto.getBillAction())) {
                matchedCrtType = cetList.stream().filter(cet -> orderLineDto.getSubscriptionCode().contains(cet.getCode())).findAny().orElse(null);
                if (matchedCrtType != null) {
                    setError(orderDto, OrderStatusEnum.WAITING,
                            "Circuit id in the Service_Id tag is matched against an entry configured in the Billing Trigger Circuit Type Mapping table : " + matchedCrtType.getCode());
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * Reprocess Held Orders
     *
     * @param orderDto the order dto
     * @return the future of order Dto
     */
    @Asynchronous
    public Future<OrderDto> reprocessHeldOrders(OrderDto orderDto) {
        if (!StringUtils.isBlank(orderDto.getOrderCode())) {
            List<String> heldOrders = orderService.getHeldOrdersToReprocess(orderDto.getOrderCode());
            if (heldOrders != null && heldOrders.size() > 0) {
                // Waiting for the parent order to be achieved
                ThreadUtils.sleepSafe(TimeUnit.SECONDS, 5);
                for (String code : heldOrders) {
                    OrderDto dto = orderService.getOrderDto(code);
                    dto.setProcessingType(OrderProcessingTypeEnum.REPROCESS);
                    utilService.sendToOrderQueue(orderDto);
                }
            }
        }
        return new AsyncResult<>(orderDto);
    }

    /**
     * Release deferred orders
     *
     * @param orderDto the order Dto
     * @return the future of order Dto
     */
    @Asynchronous
    public Future<OrderDto> releaseDeferredOrders(OrderDto orderDto) {
        if (orderDto != null && orderDto.getStatus() == OrderStatusEnum.COMPLETED) {
            if (orderDto.getComplimentaryOrderDto() != null && !StringUtils.isBlank(orderDto.getComplimentaryOrderDto().getRefOrderCode())) {

                /** Step 10 : Release the complementary order for processing **/
                Order refOrder = orderService.findEntityByIdOrCode(orderService, orderDto, orderDto.getComplimentaryOrderDto().getRefOrderCode());
                if (refOrder.getStatus() == OrderStatusEnum.DEFERRED) {
                    // Waiting for the parent order to be achieved
                    ThreadUtils.sleepSafe(TimeUnit.SECONDS, 5);
                    OrderDto dto = orderService.getOrderDto(refOrder.getCode());
                    dto.setProcessingType(OrderProcessingTypeEnum.RELEASE);
                    utilService.sendToOrderQueue(orderDto);
                }
            }
        }
        return new AsyncResult<>(orderDto);
    }

    /**
     * Reprocess held orders
     *
     * @param order the order dto
     * @param order the order
     */
    private void reprocessHeldOrders(OrderDto orderDto, Order order) {
        if (order != null && (order.getStatus() == OrderStatusEnum.COMPLETED || order.getStatus() == OrderStatusEnum.CANCELLED)) {
            getSelfForNewTx().reprocessHeldOrders(orderDto);
        }
    }

    /**
     * Release the order
     *
     * @param orderDto the order Dto.
     * @return the order Dto
     * @throws BusinessException the business exception
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public OrderDto release(OrderDto orderDto) throws BusinessException {
        switch (orderDto.getReleaseStatus()) {
            // PENDING
            case PENDING:
                reprocessOrder(orderDto);
                break;
            // WAITING
            case WAITING:
                Order order = orderService.findById(orderDto.getOrderId());
                order.setStatus(OrderStatusEnum.HELD);
                orderService.update(order);
                break;
            // CANCELLED
            case CANCELLED:
                if (StringUtils.isBlank(orderDto.getCancelReasonCode()) ||
                        orderService.findEntityByIdOrCode(subscriptionTerminationReasonService, orderDto, orderDto.getCancelReasonCode()) == null) {
                    setError(orderDto, ErrorEnum.INVALID_DATA, "The reason code for cancellation is not valid");
                }
                if (StringUtils.isBlank(orderDto.getCancelDescription())) {
                    setError(orderDto, ErrorEnum.INVALID_DATA, "The reason description for cancellation is not valid");
                }
                if (orderDto.getErrorCode() != null) {
                    persistOrder(orderDto);
                    return orderDto;
                }
                String orderCode = orderDto.getOrderCode();
                order = orderService.findById(orderDto.getOrderId());
                String newOrderCode = orderDto.getOrderCode() + ".F_" + DateUtils.formatDateWithPattern(new Date(), "dd_MM_yyyy-HHmmss");
                order.setCode(newOrderCode);
                orderDto.setOrderCode(newOrderCode);
                order.setStatus(OrderStatusEnum.CANCELLED);
                order.setCfValue(OrderCFsEnum.CANCEL_REASON_CODE.name(), orderDto.getCancelReasonCode());
                order.setCfValue(OrderCFsEnum.CANCEL_DESCRIPTION.name(), orderDto.getCancelDescription());
                order.setCfValue(OrderCFsEnum.ORDER_DETAILS.name(), orderDto.getOrderDetails());
                orderService.update(order);
                OrderDto dto = new OrderDto();
                dto.setOrderCode(orderCode);
                getSelfForNewTx().reprocessHeldOrders(dto);
                break;
        }
        return orderDto;
    }

    /**
     * Set offer id CF
     *
     * @param orderLineDto the order line dto
     * @param order        the order
     */
    protected void setOfferIdCf(OrderLineDto orderLineDto, Order order) {
        if (!StringUtils.isBlank(orderLineDto.getOfferTemplateCode())) {
            String offerCode = orderLineDto.getOfferTemplateCode();
            String cfValue = (String) order.getCfValue(OrderCFsEnum.ORDER_OFFER_ID.name());
            if (!StringUtils.isBlank(cfValue)) {
                Set<String> set = Arrays.stream(cfValue.split(";")).collect(Collectors.toSet());
                set.add(offerCode);
                offerCode = set.stream().map(String::valueOf).collect(Collectors.joining(";"));
            }
            order.setCfValue(OrderCFsEnum.ORDER_OFFER_ID.name(), offerCode);
        }
    }

    /**
     * Set ids from nested entities (technical need on the portal side)
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line dto
     */
    protected void setIds(OrderDto orderDto, OrderLineDto orderLineDto) {
        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        if (subscription != null) {
            orderLineDto.setSubscriptionId(subscription.getId());
        }
        OfferTemplate offerTemplate = orderService.findEntityByIdOrCode(offerTemplateService, orderDto, orderLineDto.getOfferTemplateCode());
        if (offerTemplate != null) {
            orderLineDto.setOfferTemplateId(offerTemplate.getId());
        }
    }

    /**
     * Set the charge type.
     *
     * @param orderLineDto    the order line dto
     * @param serviceTemplate the service template
     */
    protected void setChargeType(OrderLineDto orderLineDto, ServiceTemplate serviceTemplate) {
        List<ServiceChargeTemplateRecurring> serviceRecurringCharges = serviceTemplate.getServiceRecurringCharges();
        if (serviceRecurringCharges != null && !serviceRecurringCharges.isEmpty()) {
            orderLineDto.setChargeType(ChargeTypeEnum.R.name());
        }

        List<ServiceChargeTemplateSubscription> serviceSubscriptionCharges = serviceTemplate.getServiceSubscriptionCharges();
        if (serviceSubscriptionCharges != null && !serviceSubscriptionCharges.isEmpty()) {
            String chargeType = ChargeTypeEnum.O.name();
            if (!StringUtils.isBlank(orderLineDto.getChargeType())) {
                chargeType = chargeType + "/" + orderLineDto.getChargeType();
            }
            orderLineDto.setChargeType(chargeType);
        }
    }

    /**
     * Validate SLA surcharge
     *
     * @param orderDto          the order dto
     * @param orderLineDto      the order line dto
     * @param serviceTemplateId the service template Id
     */
    protected boolean validateSlaSurcharge(OrderDto orderDto, OrderLineDto orderLineDto, Long serviceTemplateId) {

        if (orderLineDto.isMain()) {
            String sla = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_MAIN_SLA_IND.getLabel());
            if (!StringUtils.isBlank(sla)) {
                if (!orderService.isSlaConfigurationExist(serviceTemplateId, sla, orderLineDto.getBillEffectDate())) {
                    setError(orderDto, ErrorEnum.INVALID_DATA, "the sla configuration is missing for the service :" + orderLineDto.getBillCode());
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Validate service instance
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @param actions      the action values.
     * @return false if the service instance is not found or  already ceased.
     */
    protected boolean validateServiceInstance(OrderDto orderDto, OrderLineDto orderLineDto, String... actions) {
        if (!isService(orderLineDto)) {
            return false;
        }
        if (Stream.of(actions).anyMatch(orderLineDto.getBillAction()::equalsIgnoreCase)) {
            Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
            if (subscription != null) {
                ServiceInstance serviceInstance = getServiceInstance(orderDto, orderLineDto, subscription);
                if (serviceInstance == null) {
                    String message = "The service " + orderLineDto.getBillCode() + " is not found in subscription or already ceased.";
                    if (orderLineDto.isMain() || BillActionEnum.CU.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
                        setError(orderDto, ErrorEnum.INVALID_DATA, message);
                    } else {
                        orderDto.setDataDiscrepancies(orderDto.getDataDiscrepancies() + message + " \n");
                    }
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Get the number of active service instances that exist against the subscription
     *
     * @param servicesInstanceCode the services instance code
     * @param subscription         the subscription
     * @return the number of active service instances that exist against the subscription
     */
    private int getActiveServicesInstancesNumber(String servicesInstanceCode, Subscription subscription) {
        int activeServicesInstancesNumber = 0;
        if (subscription != null) {
            List<ServiceInstance> serviceInstances = subscription.getServiceInstances();
            if (serviceInstances != null && !serviceInstances.isEmpty()) {
                for (ServiceInstance serviceInstance : serviceInstances) {
                    if (serviceInstance.getCode().equalsIgnoreCase(servicesInstanceCode) && serviceInstance.getStatus() == InstanceStatusEnum.ACTIVE) {
                        activeServicesInstancesNumber = activeServicesInstancesNumber + 1;
                    }
                }
            }
        }
        return activeServicesInstancesNumber;
    }

    /**
     * Get ancillary service instance
     *
     * @param orderLineDto the order line Dto
     * @param subscription the subscription
     * @return the service instance
     */
    private ServiceInstance getAncillaryServiceInstance(OrderLineDto orderLineDto, Subscription subscription) {
        if (subscription == null) {
            return null;
        }
        List<ServiceInstance> serviceInstances = subscription.getServiceInstances();
        if (serviceInstances != null && !serviceInstances.isEmpty()) {
            for (ServiceInstance serviceInstance : serviceInstances) {
                String itemComponentId = (String) serviceInstance.getCfValue(ServiceInstanceCFsEnum.ITEM_COMPONENT_ID.name());
                /** Step 5 [PO2-474] : Using the Item_Component_Id and Bill Code, attempt to locate the respective charge (i.e. ancillary service) against the subscriber **/

                /** Step 4 [PO2-490] : Using the item component id, locate the subscriber service **/
                if (serviceInstance.getCode().equalsIgnoreCase(orderLineDto.getBillCode()) &&
                        !StringUtils.isBlank(itemComponentId) && itemComponentId.equalsIgnoreCase(orderLineDto.getItemComponentId()) &&
                        serviceInstance.getStatus() == InstanceStatusEnum.ACTIVE) {
                    return serviceInstance;
                }
            }
        }
        return null;
    }

    /**
     * Get ancillary service instance
     *
     * @param orderLineDto     the order line Dto
     * @param subscription     the subscription
     * @param uniqueAttributes the service template unique attributes
     * @return the service instance
     */
    private ServiceInstance getAncillaryServiceInstance(OrderLineDto orderLineDto, Subscription subscription, ServiceInstanceIdentifierAttributesCFsEnum uniqueAttributes) {
        if (subscription == null || (uniqueAttributes != ServiceInstanceIdentifierAttributesCFsEnum.RANGE_START
                && uniqueAttributes != ServiceInstanceIdentifierAttributesCFsEnum.RANGE_START_RANGE_END)) {
            return null;
        }
        List<ServiceInstance> serviceInstances = subscription.getServiceInstances();
        if (serviceInstances != null && !serviceInstances.isEmpty()) {
            for (ServiceInstance serviceInstance : serviceInstances) {
                if (serviceInstance.getStatus() == InstanceStatusEnum.ACTIVE && orderLineDto.getBillCode().equalsIgnoreCase(serviceInstance.getCode())) {
                    CustomFieldValue lastCfValue = utilService.getLastCfVersion(serviceInstance, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(),
                            DateUtils.truncateTime(serviceInstance.getSubscriptionDate()), true);
                    Map<String, String> cf = getServiceParameters(serviceInstance, lastCfValue);
                    String rangeStart = cf.get(ServiceParametersCFEnum.RANGE_START.getLabel());

                    if (uniqueAttributes == ServiceInstanceIdentifierAttributesCFsEnum.RANGE_START) {
                        if (orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_START.getLabel()).equalsIgnoreCase(rangeStart)) {
                            return serviceInstance;
                        }
                    } else if (uniqueAttributes == ServiceInstanceIdentifierAttributesCFsEnum.RANGE_START_RANGE_END) {

                        String rangeEnd = cf.get(ServiceParametersCFEnum.RANGE_END.getLabel());

                        if (orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_START.getLabel()).equalsIgnoreCase(rangeStart)
                                && orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_END.getLabel()).equalsIgnoreCase(rangeEnd)) {
                            return serviceInstance;
                        }
                    }

                }
            }
        }
        return null;
    }

    /**
     * Get ancillary service instance
     *
     * @param orderDto     the order dto
     * @param orderLineDto the order line dto
     * @param subscription the subscription
     * @return the ancillary service instance
     */
    protected ServiceInstance getAncillaryServiceInstance(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription) {
        ServiceInstance ancillaryService = null;
        ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
        if (serviceTemplate == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The service " + orderLineDto.getBillCode() + " is not found");
            return null;
        }
        Object multipleInstances = serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MULTIPLE_INSTANCES.name());
        if (multipleInstances != null && (Boolean) multipleInstances == true && serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MAX_INSTANCES.name()) == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The MAX_INSTANCES attribute is missing for the service template : " + orderLineDto.getBillCode());
            return null;
        }

        if (multipleInstances != null && ((Boolean) multipleInstances) == true && ((Long) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MAX_INSTANCES.name())) > 1) {
            String multipleInstanceUniqueAttributes = (String) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MULTIPLE_INSTANCE_UNIQUE_ATTRIBUTES.name());
            if (StringUtils.isBlank(multipleInstanceUniqueAttributes)) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "The MULTIPLE_INSTANCE_UNIQUE_ATTRIBUTES attribute is missing for the service template " + orderLineDto.getBillCode());
                return null;
            }

            if (!ServiceInstanceIdentifierAttributesCFsEnum.contains(multipleInstanceUniqueAttributes)) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "The MULTIPLE_INSTANCE_UNIQUE_ATTRIBUTES attribute is not valid for the service template " + orderLineDto.getBillCode());
                return null;
            }

            /**
             * Verify the mandatory attributes in MULTIPLE_INSTANCE_UNIQUE_ATTRIBUTES field are included in Bill_Item
             */
            switch (ServiceInstanceIdentifierAttributesCFsEnum.valueOfIgnoreCase(multipleInstanceUniqueAttributes)) {
                case COMPONENT_ID:
                    if (StringUtils.isBlank(orderLineDto.getItemComponentId())) {
                        setError(orderDto, ErrorEnum.INVALID_DATA, "The Item_Component_Id attribute is missing for the service " + orderLineDto.getBillCode());
                        return null;
                    }
                    ancillaryService = getAncillaryServiceInstance(orderLineDto, subscription);
                    break;
                case RANGE_START:
                    if (StringUtils.isBlank(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_START.getLabel()))) {
                        setError(orderDto, ErrorEnum.INVALID_DATA, "The RANGE_START attribute is missing for the service " + orderLineDto.getBillCode());
                        return null;
                    }
                    ancillaryService = getAncillaryServiceInstance(orderLineDto, subscription, ServiceInstanceIdentifierAttributesCFsEnum.RANGE_START);
                    break;
                case RANGE_START_RANGE_END:
                    if (StringUtils.isBlank(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_START.getLabel()))) {
                        setError(orderDto, ErrorEnum.INVALID_DATA, "The RANGE_START attribute is missing for the service " + orderLineDto.getBillCode());
                        return null;
                    }
                    if (StringUtils.isBlank(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_END.getLabel()))) {
                        setError(orderDto, ErrorEnum.INVALID_DATA, "The RANGE_END attribute is missing for the service " + orderLineDto.getBillCode());
                        return null;
                    }
                    ancillaryService = getAncillaryServiceInstance(orderLineDto, subscription, ServiceInstanceIdentifierAttributesCFsEnum.RANGE_START_RANGE_END);
                    break;
            }
            orderLineDto.setServiceInstanceIdentifierAttributes(ServiceInstanceIdentifierAttributesCFsEnum.valueOfIgnoreCase(multipleInstanceUniqueAttributes));
            return ancillaryService;
        } else {
            return subscription.getServiceInstances().stream()
                    .filter(serviceInstance -> (orderLineDto.getBillCode().equals(serviceInstance.getCode()) && serviceInstance.getStatus() == InstanceStatusEnum.ACTIVE)).findFirst()
                    .orElse(null);
        }

    }

    /**
     * Validate new service instance
     *
     * @param orderDto     the order dto
     * @param orderLineDto the order line Dto
     * @param subscription the subscription
     * @return false if there is an error.
     */
    protected boolean validateNewServiceInstance(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription) {
        int activeServicesInstancesNumber = getActiveServicesInstancesNumber(orderLineDto.getBillCode(), subscription);
        if (orderLineDto.isMain()) {
            if (activeServicesInstancesNumber > 0) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "There is already the main service " + orderLineDto.getBillCode() + " in this subscription");
                return false;
            }
            return true;
        }

        ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
        Object multipleInstances = serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MULTIPLE_INSTANCES.name());
        if (multipleInstances != null && ((Boolean) multipleInstances) == true) {
            if (serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MAX_INSTANCES.name()) == null) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "The MAX_INSTANCES attribute is missing for the service template : " + orderLineDto.getBillCode());
                return false;
            }
            Long maxInstances = (Long) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MAX_INSTANCES.name());
            /**
             * Control the number of service instances that can exist for the bill code against a subscription
             */
            if (activeServicesInstancesNumber >= maxInstances) {
                setError(orderDto, ErrorEnum.INVALID_DATA,
                        "The maximum is already reached, you can no longer activate a new instance for the service " + orderLineDto.getBillCode());
                return false;
            }
        } else if (activeServicesInstancesNumber > 0) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The multiple service instances are not allowed for the service " + orderLineDto.getBillCode());
            return false;
        }
        return true;
    }

    /**
     * Get retries number
     *
     * @return the retries number
     */
    private int getRetriesNumber() {
        try {
            return Integer.parseInt(ApplicationPropertiesEnum.RETRIES_NUMBER_ADDRESS_API.getProperty());
        } catch (NumberFormatException ex) {
        }
        return 1;
    }

    /**
     * Get retry delay
     *
     * @return the retry delay
     */
    private int getRetryDelay() {
        try {
            return Integer.parseInt(ApplicationPropertiesEnum.RETRY_DELAY_ADDRESS_API.getProperty());
        } catch (NumberFormatException ex) {
        }
        return 2000;
    }

    /**
     * Get address info response
     *
     * @param orderDto the order Dto
     * @param httpConn the http URL connection
     * @return the address info response
     * @throws BusinessException the business exception
     */
    private AddressInfoResponseDto getAddressInfoResponse(OrderDto orderDto, HttpURLConnection httpConn) throws BusinessException {
        AddressInfoResponseDto addressInfoResponseDto = null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
            String line;
            final StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            if (httpConn.getContentType().startsWith("application/xml")) {
                addressInfoResponseDto = Utils.getDtoFromXml(sb.toString(), AddressInfoResponseDto.class, null);
            } else {
                log.error("Fail to get address details from eir API : " + sb.toString());
                setError(orderDto, ErrorEnum.GENERIC_API_EXCEPTION, "Fail to get address details from eir API");
            }
        } catch (Exception e) {
            log.error("Fail to get address details from eir API : " + e.getMessage());
            throw new BusinessException("Fail to get address details from eir API");
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (IOException e) {
                log.error("Fail to get address details from eir API : " + e.getMessage());
                throw new BusinessException("Fail to get address details from eir API");
            }
        }
        return addressInfoResponseDto;
    }

    /**
     * Get address details
     *
     * @param orderDto the order Dto
     * @param apiUrl   the api url
     * @return the address details
     * @throws BusinessException the business exception
     */
    private AddressInfoResponseDto getAddressDetails(OrderDto orderDto, String apiUrl) throws BusinessException {

        AddressInfoResponseDto addressInfoResponseDto = null;

        if (!StringUtils.isBlank(apiUrl)) {
            try {

                int retriesNumber = getRetriesNumber();
                int retryDelay = getRetryDelay();

                int status = 0;
                int retry = 0;
                boolean delay = false;

                do {
                    if (delay) {
                        Thread.sleep(retryDelay);
                    }
                    HttpURLConnection httpConn = null;
                    try {
                        URL url = new URL(apiUrl);
                        httpConn = (HttpURLConnection) url.openConnection();
                        if (httpConn != null)
                            httpConn.disconnect();
                        httpConn.setDoOutput(true);
                        httpConn.setRequestProperty("Content-Type", "application/json");
                        httpConn.setRequestProperty("Accept", "application/json");
                        httpConn.setRequestMethod("GET");
                        httpConn.setUseCaches(false);

                        /* To remove */
                        String userCredentials = ApplicationPropertiesEnum.BROKER_ADDRESS_API_USER.getProperty() + ":"
                                + ApplicationPropertiesEnum.BROKER_ADDRESS_API_PASSWORD.getProperty();
                        String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));
                        httpConn.setRequestProperty("Authorization", basicAuth);
                        /* To remove */

                        status = httpConn.getResponseCode();

                        switch (status) {
                            case HttpURLConnection.HTTP_OK:
                                log.info("OK : HTTP code : " + status);
                                // Parse Response.
                                addressInfoResponseDto = getAddressInfoResponse(orderDto, httpConn);
                                break; // if response is returned exit, fine, go on
                            case HttpURLConnection.HTTP_BAD_REQUEST:
                                log.info("Invalid request details : HTTP error code : " + status);
                                break;// retry
                            case HttpURLConnection.HTTP_NOT_FOUND:
                                log.info("No data found for request : HTTP error code : " + status);
                                break;// retry
                            case HttpURLConnection.HTTP_INTERNAL_ERROR:
                                log.info("General Exception or Timeout on backend. : HTTP error code : " + status);
                                break;// retry
                            case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
                                log.info("Gateway timeout : HTTP error code : " + status);
                                break;// retry
                            case HttpURLConnection.HTTP_UNAVAILABLE:
                                log.info("Unavailable : HTTP error code : " + status);
                                break;// retry, server is unstable
                            default:
                                log.info("Failed : HTTP error code : " + status);
                                break; // retry
                        }
                        // retry
                        retry++;
                        log.info("Failed retry " + retry + "/" + retriesNumber);
                        delay = true;
                    } catch (MalformedURLException e) {
                        log.error("Fail to get address details from eir API : " + e.getMessage());
                        throw new BusinessException("Malformed URL API to get eir address");
                    } catch (IOException e) {
                        log.error("Fail to get address details from eir API : " + e.getMessage());
                        throw new BusinessException("Fail to get address details from eir API");
                    } catch (Exception e) {
                        log.error("Fail to get address details from eir API : " + e.getMessage());
                        throw new BusinessException("Fail to get address details from eir API");
                    } finally {
                        if (httpConn != null) {
                            httpConn.disconnect();
                        }
                    }

                } while (status != HttpURLConnection.HTTP_OK && retry < retriesNumber);

            } catch (InterruptedException e) {
                log.error("Fail to get address details from eir API : " + e.getMessage());
                throw new BusinessException("Fail to get address details from eir API");
            }
        }

        return addressInfoResponseDto;
    }

    /**
     * Get the address API url
     *
     * @param addressApiUrl the address API url
     * @param addressId     the address id
     * @return the API url
     */
    private String getApiUrl(String addressApiUrl, String addressId) {

        if (StringUtils.isBlank(addressApiUrl)) {
            return null;
        }

        if (addressApiUrl.endsWith(File.separator)) {
            addressApiUrl = addressApiUrl.substring(0, addressApiUrl.lastIndexOf(File.separator));
        }
        addressApiUrl = addressApiUrl + File.separator + addressId;
        return addressApiUrl;
    }

    /**
     * Get the Eir address details
     *
     * @param orderDto  the order Dto
     * @param addressId the address id
     * @return the Eir address details
     */
    private AddressInfoResponseDto getEirAddressDetails(OrderDto orderDto, String addressId) {

        // An Eircode is a unique 7-character code consisting of letters and numbers. Each Eircode consists of a 3-character
        // routing key to identify the area and a 4-character unique identifier for each address, for example, A65 F4E2.

        /* /Address_Info/Eircode/{Eircode} */
        String apiUrlProperty = ApplicationPropertiesEnum.EIRCODE_ADDRESS_API_URL.getKey();
        String apiUrlValue = ApplicationPropertiesEnum.EIRCODE_ADDRESS_API_URL.getProperty();
        if (Utils.isInteger(addressId)) {

            // ARD Id is a numeric code (it does not contain letters)

            /* /Address_Info/Ard_Id/{Ard_Id} */
            apiUrlProperty = ApplicationPropertiesEnum.ARD_ADDRESS_API_URL.getKey();
            apiUrlValue = ApplicationPropertiesEnum.ARD_ADDRESS_API_URL.getProperty();
        }

        String addressApiUrl = getApiUrl(apiUrlValue, addressId);
        if (StringUtils.isBlank(addressApiUrl)) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Missing " + apiUrlProperty + "API url");
            return null;
        }

        AddressInfoResponseDto addressInfoResponseDto = null;
        try {
            addressInfoResponseDto = getAddressDetails(orderDto, addressApiUrl);
        } catch (BusinessException ve) {
            setError(orderDto, ErrorEnum.GENERIC_API_EXCEPTION, ve.getMessage());
        }

        if (addressInfoResponseDto == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Address " + EndPointCustomTableEnum.EPADDRESS.name() + " not found on broker");
            return null;
        }

        return addressInfoResponseDto;
    }

    /**
     * Validate address
     *
     * @param orderDto               the order Dto
     * @param addressInfoResponseDto the addressInfoResponse Dto
     */
    private void validateAddress(OrderDto orderDto, AddressInfoResponseDto addressInfoResponseDto) {

        // Validate the response adheres to the data contract and if not raise an error
        if (addressInfoResponseDto == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Missing address Info Response");
            return;
        }

        // Address Info aggregate Occurs 1-n
        if (addressInfoResponseDto.getAddressInfos() == null || addressInfoResponseDto.getAddressInfos().isEmpty()) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Missing address Info");
            return;
        }

        // Address details aggregate Occurs 1-1
        AddressInfoDto addressInfoDto = addressInfoResponseDto.getAddressInfos().get(0);
        if (addressInfoDto.getAddress() == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Missing address");
            return;
        }

        // Unit details aggregate Occurs 1-1
        if (addressInfoDto.getAddress().getUnit() == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Missing unit");
            return;
        }

        if (StringUtils.isBlank(addressInfoDto.getAddress().getUnit().getAddressUnitId())) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Missing Address_Unit_Id attribute");
            return;
        }

        // Information for a particular building as it occurs in the address Occurs 1-1
        if (addressInfoDto.getAddress().getBuilding() == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Missing building");
            return;
        }

        // Information relating to County Occurs 1-1
        if (addressInfoDto.getAddress().getCounty() == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Missing county");
            return;
        }

        if (StringUtils.isBlank(addressInfoDto.getAddress().getCounty().getCountyId())) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Missing County_Id attribute");
            return;
        }

        if (StringUtils.isBlank(addressInfoDto.getAddress().getCounty().getCountyName())) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Missing County_Name attribute");
            return;
        }
    }

    /**
     * Format the address
     *
     * @param addressInfoResponseDto the addressInfoResponse Dto
     * @return the formated address
     */
    private Map<String, Object> formatAddress(AddressInfoResponseDto addressInfoResponseDto) {

        StringBuilder addressLine1 = new StringBuilder();
        StringBuilder addressLine2 = new StringBuilder();
        StringBuilder addressLine3 = new StringBuilder();
        StringBuilder addressLine4 = new StringBuilder();
        String addressLine5 = "";
        String countyAddress = "";
        String postalDistrict = "";
        String eircode = "";
        String country = "";

        AddressInfoDto addressInfoDto = addressInfoResponseDto.getAddressInfos().get(0);
        BuildingDto building = addressInfoDto.getAddress().getBuilding();
        UnitDto unit = addressInfoDto.getAddress().getUnit();
        CountyDto county = addressInfoDto.getAddress().getCounty();

        // addressLine1
        if (!StringUtils.isBlank(building.getBuildingNumber())) {
            addressLine1.append(building.getBuildingNumber()).append(" ");
        }
        if (!StringUtils.isBlank(building.getFromBuildingNumber())) {
            addressLine1.append(building.getFromBuildingNumber()).append(" ");
        }
        if (!StringUtils.isBlank(building.getToBuildingNumber())) {
            addressLine1.append(building.getToBuildingNumber()).append(" ");
        }
        if (!StringUtils.isBlank(unit.getUnitName())) {
            addressLine1.append((unit.getUnitName())).append(" ");
        }
        if (!StringUtils.isBlank(unit.getUnitNumber())) {
            addressLine1.append((unit.getUnitNumber())).append(" ");
        }
        if (!StringUtils.isBlank(unit.getFloorNumber())) {
            addressLine1.append((unit.getFloorNumber())).append(" ");
        }
        if (!StringUtils.isBlank(unit.getEircode())) {
            addressLine1.append((unit.getEircode())).append(" ");
        }

        // addressLine2
        if (!StringUtils.isBlank(building.getBuildingName())) {
            addressLine2.append(building.getBuildingName());
        }

        if (addressInfoDto.getAddress().getStreet() != null) {
            StreetDto street = addressInfoDto.getAddress().getStreet();

            // addressLine3
            if (!StringUtils.isBlank(street.getStreetName())) {
                addressLine3.append(street.getStreetName()).append(" ");
            }
            if (!StringUtils.isBlank(street.getStreetSuffix())) {
                addressLine3.append(street.getStreetSuffix()).append(" ");
            }
            if (!StringUtils.isBlank(street.getDirectionalQualifierName())) {
                addressLine3.append(street.getDirectionalQualifierName()).append(" ");
            }
            if (!StringUtils.isBlank(street.getGeographicalQualifierName())) {
                addressLine3.append(street.getGeographicalQualifierName()).append(" ");
            }

            // addressLine4
            if (!StringUtils.isBlank(street.getTownlandName())) {
                addressLine4.append(street.getTownlandName());
            }
        }

        // addressLine5
        if (!StringUtils.isBlank(county.getTownName())) {
            addressLine5 = county.getTownName();
        }

        // county
        if (!StringUtils.isBlank(county.getTownName())) {
            countyAddress = county.getTownName();
        }

        // postalDistrict
        if (!StringUtils.isBlank(county.getPostalDistrict())) {
            postalDistrict = county.getPostalDistrict();
        }

        // eircode
        if (!StringUtils.isBlank(unit.getEircode())) {
            eircode = unit.getEircode();
        }

        Map<String, Object> address = new HashMap<>();
        address.put(EndPointCustomTableEnum.ADDRESS_LINE_1.getLabel(), addressLine1.toString());
        address.put(EndPointCustomTableEnum.ADDRESS_LINE_2.getLabel(), addressLine2.toString());
        address.put(EndPointCustomTableEnum.ADDRESS_LINE_3.getLabel(), addressLine3.toString());
        address.put(EndPointCustomTableEnum.ADDRESS_LINE_4.getLabel(), addressLine4.toString());
        address.put(EndPointCustomTableEnum.ADDRESS_LINE_5.getLabel(), addressLine5);
        address.put(EndPointCustomTableEnum.COUNTY.getLabel(), countyAddress);
        address.put(EndPointCustomTableEnum.POSTAL_DISTRICT.getLabel(), postalDistrict);
        address.put(EndPointCustomTableEnum.COUNTRY.getLabel(), country);
        address.put(EndPointCustomTableEnum.EIR_CODE.getLabel(), eircode);
        return address;
    }

    /**
     * Validate and format address
     *
     * @param orderDto               the order Dto
     * @param addressInfoResponseDto the addressInfoResponse Dto
     * @return the validated address
     */
    private Map<String, Object> ValidateAndFormatAddress(OrderDto orderDto, AddressInfoResponseDto addressInfoResponseDto) {

        validateAddress(orderDto, addressInfoResponseDto);
        if (orderDto.getErrorCode() != null) {
            return null;
        }
        return formatAddress(addressInfoResponseDto);
    }

    /**
     * Get end point address
     *
     * @param orderDto  the order Dto
     * @param epaddress the end point address code
     * @return the end point address
     */

    protected Map<String, Object> getEndPointAddress(OrderDto orderDto, String epaddress) {
        Map<String, Object> address = null;
        // retrieve address details from eir broker
        AddressInfoResponseDto addressInfoResponseDto = getEirAddressDetails(orderDto, epaddress);
        if (orderDto.getErrorCode() != null) {
            return null;
        }
        address = ValidateAndFormatAddress(orderDto, addressInfoResponseDto);
        if (orderDto.getErrorCode() != null) {
            return null;
        }
        return address;
    }

    /**
     * Get end point
     *
     * @param orderDto     the order dto
     * @param orderLineDto the order line Dto
     * @param address      the address
     * @return the end point
     * @throws BusinessException the business exception
     */
    protected Map<String, Object> getEndPoint(OrderDto orderDto, OrderLineDto orderLineDto, Map<String, Object> address) throws BusinessException {
        Map<String, Object> endPoint = getEndPoint(orderDto, orderLineDto);
        endPoint.putAll(address);
        return endPoint;
    }

    /**
     * Update endpoint
     *
     * @param orderDto     the order dto
     * @param orderLineDto the order line Dto
     * @param subscription the subscription
     * @param endPoint     the endpoint
     * @param endpointEnd  the endpoint end
     * @param address      the address
     * @param endDate      the end date
     * @param startDate    the start date
     * @throws BusinessException the business exception
     */
    protected void updateEndPoint(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription, Map<String, Object> endPoint, String endpointEnd,
                                  Map<String, Object> address, Date startDate, Date endDate) throws BusinessException {

        // Get the last valid version, and set end date (the Effective Date/Time on the bill item minus one second)
        Map<String, Object> lastEndPoint = orderService.findLastVersionEndPoint(subscription.getId(), endpointEnd);
        int epVersion = 0;
        if (lastEndPoint != null && !lastEndPoint.isEmpty()) {
            if (orderService.isOverlapEndPoint(orderLineDto, subscription.getId(), lastEndPoint, startDate)) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "A matching endpoint period with " + DateUtils.formatDateWithPattern(startDate, Utils.DATE_TIME_PATTERN)
                        + " was found. Please change the value of existing period instead");
                return;
            }
            // End Date should be set to the Effective Date/Time on the bill item minus one second
            // Date lastEndPointEndDate = Utils.addSecondsToDate(orderLineDto.getBillEffectDate(), -1);
            lastEndPoint.put(EndPointCustomTableEnum.EP_END_DATE.getLabel(), startDate);
            orderService.updateEndPoint(lastEndPoint);
            epVersion = ((BigInteger) lastEndPoint.get(EndPointCustomTableEnum.EPVERSION.getLabel())).intValue();
        }
        // Add the new version of endpoint
        if (endPoint == null || endPoint.isEmpty()) {
            endPoint = getEndPoint(orderDto, orderLineDto, address);
        }
        if (endDate != null) {
            endPoint.put(EndPointCustomTableEnum.EP_END_DATE.getLabel(), endDate);
        }
        endPoint.put(EndPointCustomTableEnum.EPVERSION.getLabel(), epVersion + 1);
        orderService.saveEndPoint(endPoint);
    }

    /**
     * Update endpoint
     *
     * @param orderDto     the order dto
     * @param orderLineDto the order line Dto
     * @param subscription the subscription
     * @param address      the address
     * @throws BusinessException the business exception
     */
    protected void updateEndPoint(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription, Map<String, Object> address) throws BusinessException {
        Date endDate = null;
        String epstatus = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPSTATUS.getLabel());
        if (ItemStatusEnum.CE.name().equalsIgnoreCase(epstatus)) {
            endDate = orderLineDto.getBillEffectDate();
        }
        updateEndPoint(orderDto, orderLineDto, subscription, null, orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPEND.getLabel()), address,
                DateUtils.parseDateWithPattern(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPSTART.getLabel()), Utils.DATE_TIME_PATTERN), endDate);
    }

    /**
     * Set the delta quantity
     *
     * @param orderLineDto the order line Dto
     * @param oldQuantity  the old quantity
     */
    protected void setDeltaQuantity(OrderLineDto orderLineDto, BigDecimal oldQuantity) {
        BigDecimal deltaQuantity = new BigDecimal(orderLineDto.getBillQuantity()).subtract(oldQuantity);
        orderLineDto.setDeltaQuantity(deltaQuantity);
    }

    /**
     * Apply subscription charge
     *
     * @param orderDto        the order Dto
     * @param orderLineDto    the order line Dto
     * @param serviceInstance the service instance
     * @throws BusinessException the business exception
     */
    protected void applySubscriptionCharge(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance) throws BusinessException {
        if (orderLineDto.getDeltaQuantity() != null && orderLineDto.getDeltaQuantity().compareTo(BigDecimal.ZERO) > 0) {
            List<SubscriptionChargeInstance> subscriptionChargeInstances = serviceInstance.getSubscriptionChargeInstances();
            if (subscriptionChargeInstances != null && !subscriptionChargeInstances.isEmpty()) {
                for (SubscriptionChargeInstance subscriptionChargeInstance : subscriptionChargeInstances) {
                    OneShotChargeTemplate oneShotChargeTemplate = null;
                    if (subscriptionChargeInstance.getChargeTemplate() instanceof HibernateProxy) {
                        oneShotChargeTemplate = (OneShotChargeTemplate) ((HibernateProxy) subscriptionChargeInstance.getChargeTemplate()).getHibernateLazyInitializer()
                                .getImplementation();
                    } else {
                        oneShotChargeTemplate = (OneShotChargeTemplate) subscriptionChargeInstance.getChargeTemplate();
                    }

                    log.debug("Will apply a delta subscription charge {}/{}", oneShotChargeTemplate.getId(), oneShotChargeTemplate.getCode());

                    oneShotChargeInstanceService.oneShotChargeApplication(serviceInstance.getSubscription(), serviceInstance,
                            oneShotChargeTemplate, null, orderLineDto.getBillEffectDate(), subscriptionChargeInstance.getAmountWithoutTax(),
                            subscriptionChargeInstance.getAmountWithTax(), orderLineDto.getDeltaQuantity(), null, null, null, null,
                            orderDto.getOrderCode(), subscriptionChargeInstance.getCfValues(), true, ChargeApplicationModeEnum.SUBSCRIPTION);
                    subscriptionChargeInstance.setQuantity(serviceInstance.getQuantity().subtract(orderLineDto.getDeltaQuantity()));
                }
            }
        }
    }

    /**
     * Apply termination charge
     *
     * @param orderDto        the order Dto
     * @param orderLineDto    the order line Dto
     * @param serviceInstance the service instance
     * @throws BusinessException the business exception
     */
    protected void applyTerminationCharge(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance) throws BusinessException {
        List<TerminationChargeInstance> terminationChargeInstances = serviceInstance.getTerminationChargeInstances();
        if (terminationChargeInstances != null && !terminationChargeInstances.isEmpty()) {
            BigDecimal quantity = new BigDecimal(orderLineDto.getBillQuantity());
            for (TerminationChargeInstance terminationChargeInstance : terminationChargeInstances) {
                OneShotChargeTemplate oneShotChargeTemplate = null;
                if (terminationChargeInstance.getChargeTemplate() instanceof HibernateProxy) {
                    oneShotChargeTemplate = (OneShotChargeTemplate) ((HibernateProxy) terminationChargeInstance.getChargeTemplate()).getHibernateLazyInitializer()
                            .getImplementation();
                } else {
                    oneShotChargeTemplate = (OneShotChargeTemplate) terminationChargeInstance.getChargeTemplate();
                }

                log.debug("Will apply a termination charge {}/{}", oneShotChargeTemplate.getId(), oneShotChargeTemplate.getCode());

                oneShotChargeInstanceService.oneShotChargeApplication(serviceInstance.getSubscription(), serviceInstance,
                        oneShotChargeTemplate, null, orderLineDto.getBillEffectDate(), terminationChargeInstance.getAmountWithoutTax(),
                        terminationChargeInstance.getAmountWithTax(), quantity, null, null, null, null,
                        orderDto.getOrderCode(), terminationChargeInstance.getCfValues(), true, ChargeApplicationModeEnum.SUBSCRIPTION);
                terminationChargeInstance.setQuantity(serviceInstance.getQuantity());
            }
        }
    }

    /**
     * Check if the service template has at least one termination charge
     *
     * @param orderDto            the order Dto
     * @param serviceTemplateCode the service template code
     * @return True if the service template has at least one termination charge
     */
    protected boolean serviceTemplateHasTerminationCharge(OrderDto orderDto, String serviceTemplateCode) {
        ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, serviceTemplateCode);
        if (serviceTemplate != null) {
            return serviceTemplate.getServiceTerminationCharges() != null && !serviceTemplate.getServiceTerminationCharges().isEmpty();
        }
        return false;
    }

    /**
     * Check is the dummy subscription or not
     *
     * @param orderLineDto the order line Dto
     * @return True is the dummy subscription
     */
    protected boolean isDummySubscription(OrderLineDto orderLineDto) {
        if (!StringUtils.isBlank(orderLineDto.getSubscriptionCode()) && orderLineDto.getSubscriptionCode().startsWith("00-")) {
            return true;
        }
        return false;
    }

    /**
     * Get misc subscription code
     *
     * @param masterAccountNumber the master account number
     * @return the misc subscription code
     */
    protected String getMiscSubscriptionCode(String masterAccountNumber) {
        return MISC_PREFIX_SERVICE_ID + masterAccountNumber;
    }

    /**
     * Apply the MISC one shot charge
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    protected void applyMiscOneShotCharge(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        if (subscription == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The subscription " + orderLineDto.getSubscriptionCode() + " is not found");
            return;
        }
        OneShotChargeTemplate chargetemplate = orderService.findEntityByIdOrCode(oneShotChargeTemplateService, orderDto, ChargeTemplateEnum.MISC_CHARGE.name());
        if (chargetemplate == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The one-shot charge " + ChargeTemplateEnum.MISC_CHARGE.name() + " is not found");
            return;
        }

        BigDecimal quantity = new BigDecimal(orderLineDto.getBillQuantity());

        OneShotChargeInstance chargeInstance = new OneShotChargeInstance();
        Map<String, String> chargeCF = getServiceParametersCF(orderDto, orderLineDto, chargeInstance);
        chargeInstance.setCfValue(ChargeInstanceCFsEnum.SERVICE_PARAMETERS.name(), chargeCF);
        chargeInstance.setCfValue(ChargeInstanceCFsEnum.SERVICE_CODE.name(), orderLineDto.getBillCode());

        // Check if price was overridden in the order
        BigDecimal chargePriceToOverride = null;
        if (!StringUtils.isBlank(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.WSLAM_REBATE_AMOUNT.getLabel()))) {
            try {
                chargePriceToOverride = new BigDecimal(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.WSLAM_REBATE_AMOUNT.getLabel()));
                chargePriceToOverride = chargePriceToOverride.negate();
            } catch (NumberFormatException ex) {
            }
        }

        log.debug("Will provide a Misc charge {}/{}", chargetemplate.getId(), chargetemplate.getCode());

        chargeInstance = oneShotChargeInstanceService.oneShotChargeApplication(subscription, null, chargetemplate, null,
                orderLineDto.getBillEffectDate(), chargePriceToOverride, null, quantity, null, null, null,
                null, orderDto.getOrderCode(), chargeInstance.getCfValues(), true, ChargeApplicationModeEnum.SUBSCRIPTION);
        chargeInstance.setCode(orderLineDto.getBillCode() + "_NRC");

        if (subscription.getStatus() == SubscriptionStatusEnum.CREATED) {
            subscription.setStatus(SubscriptionStatusEnum.ACTIVE);
        }
    }

    /**
     * Apply misc one shot charge
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    protected void miscCharge(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        ServiceTemplate serviceTemplate = getServiceTemplate(orderDto, orderLineDto);
        if (serviceTemplate == null) {
            return;
        }
        if (isDummySubscription(orderLineDto)) {
            orderLineDto.setSubscriptionCode(getMiscSubscriptionCode(getProductSetPrefixCode(orderDto, orderLineDto)));
        } else {
            orderLineDto.setSubscriptionCode(getSubscriptionCodeWithPrefix(orderDto, orderLineDto));
        }
        applyMiscOneShotCharge(orderDto, orderLineDto);
    }

    /**
     * Check if the provided order is exists
     *
     * @param orderDto the order Dto
     * @return true if the order is exists
     */
    public boolean isOrderAlreadyExists(OrderDto orderDto) {
        if (orderService.findEntityByIdOrCode(orderService, orderDto, orderDto.getOrderCode()) != null) {
            setError(orderDto, ErrorEnum.ORDER_ALREADY_EXISTS, "The order " + orderDto.getOrderCode() + " already exists");
            return true;
        }
        return false;
    }

    /**
     * Check if the provided order is too large
     *
     * @param orderDto the order Dto
     * @return true if the order is too large
     */
    protected boolean isOrderTooLarge(OrderDto orderDto) {
        if (!StringUtils.isBlank(orderDto.getOriginalXmlOrder())) {
            Long orderMaxSizeCharacters = orderOriginalMessageCFT.getMaxValue();
            if (orderMaxSizeCharacters == null || orderMaxSizeCharacters == 0) {
                setError(orderDto, ErrorEnum.INVALID_DATA,
                        "The max length of " + orderOriginalMessageCFT.getCode() + " custom field is missing");
                return true;
            }
            if (orderDto.getOriginalXmlOrder().length() > orderMaxSizeCharacters) {
                setError(orderDto, ErrorEnum.ORDER_TOO_LARGE, "The order's (" + orderDto.getOrderCode() + ") request is larger than the server " + "is willing or able to process");
                return true;
            }
        }
        return false;
    }

    /**
     * Terminate subscription
     *
     * @param orderDto          the order Dto
     * @param orderLineDto      the order line Dto
     * @param subscription      the subscription
     * @param terminationReason the termination reason
     * @param terminationDate   the termination date
     * @throws BusinessException the business exception
     */
    protected void terminateSubscription(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription, SubscriptionTerminationReason terminationReason, Date terminationDate)
            throws BusinessException {
        Map<String, Date> accessPointsMap = new HashMap<>();
        List<Access> accessPoints = subscription.getAccessPoints();
        if (accessPoints != null && !accessPoints.isEmpty()) {
            for (Access a : accessPoints) {
                accessPointsMap.put(a.getAccessUserId(), a.getEndDate());
            }
        }
        subscriptionService.terminateSubscription(subscription, terminationDate, terminationReason, orderDto.getOrderCode());
        // Add OLD_ prefix to subscription code in order to can create the new one with
        // same code
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
        String oldPrefix = "OLD" + sdf.format(new Date().getTime());
        userAccountService.userAccountTermination(subscription.getUserAccount(), terminationDate, terminationReason);
        orderService.updateEntityCode(userAccountService, subscription.getUserAccount(), orderDto, oldPrefix + "_" + subscription.getUserAccount().getCode());
        log.debug("Subscription terminated : " + subscription.getCode());

        if (!accessPointsMap.isEmpty()) {
            for (Access access : accessPoints) {
                Date endDate = accessPointsMap.get(access.getAccessUserId());
                if (endDate != null) {
                    access.setEndDate(endDate);
                    accessService.update(access);
                }
            }
        }
        orderService.updateEntityCode(subscriptionService, subscription, orderDto, oldPrefix + "_" + subscription.getCode());
    }

    /**
     * Execute task
     *
     * @param executor           the managed executor service
     * @param successfulFunction the successful function to be executed
     * @param rejectionFunction  the rejection function to be executed
     * @param parameters         the function parameters
     */
    private void executeTask(ManagedExecutorService executor, Function<Object[], Object> successfulFunction, Function<Object[], Object> rejectionFunction, Object... parameters) {
        MeveoUser lastCurrentUser = currentUser.unProxy();
        Object[] finalParameters = parameters;
        Runnable task = () -> {

            currentUserProvider.reestablishAuthentication(lastCurrentUser);
            successfulFunction.apply(finalParameters);
        };
        if (task != null) {
            try {
                executor.submit(task);
            } catch (RejectedExecutionException ex) {
                List<Object> parametersList = new ArrayList(Arrays.asList(parameters));
                parametersList.add(ex);
                parameters = parametersList.toArray(parameters);
                rejectionFunction.apply(parameters);
            }
        }
    }

    protected abstract BaseOrderApi getSelfForNewTx();

    /**
     * Update subscription cfs
     *
     * @param orderLineDto the order line Dto
     * @param subscription the subscription
     */
    protected void updateSubscriptionCfs(OrderLineDto orderLineDto, Subscription subscription) {
        subscription.setCfValue(SubscriptionCFsEnum.ORIGINAL_CODE.name(), orderLineDto.getOriginalSubscriptionCode());
        subscription.setCfValue(SubscriptionCFsEnum.UAN.name(), new DatePeriod(orderLineDto.getBillEffectDate(), null), null, orderLineDto.getUan());
        utilService.setServiceIdCF(subscription);
    }

    /**
     * Is valid range for service instance
     *
     * @param rangeStart Start of range
     * @param rangeEnd   End of range
     * @return is valid range
     */
    protected Boolean isValidRanges(String rangeStart, String rangeEnd) {
        if (rangeStart != null) {
            rangeStart = rangeStart.replaceAll("[^\\d]", "");
        }
        if (rangeEnd != null) {
            rangeEnd = rangeEnd.replaceAll("[^\\d]", "");
        }

        if (StringUtils.isBlank(rangeStart)) {
            return true;
        }

        String queryString = "SELECT 1 FROM billing_service_instance si,  billing_subscription sub WHERE si.status = 'ACTIVE'"
                + " AND (( (si.cf_values\\:\\:JSON->'RANGE_START'->0 ->>'long')\\:\\:BIGINT = :rangeStart AND (si.cf_values\\:\\:JSON->'RANGE_END'->0 ->>'long')\\:\\:BIGINT IS null )"
                + " OR ((si.cf_values\\:\\:JSON->'RANGE_START'->0 ->>'long')\\:\\:BIGINT <= :rangeStart AND (si.cf_values\\:\\:JSON->'RANGE_END'->0 ->>'long')\\:\\:BIGINT >= :rangeStart)";
        if (!StringUtils.isBlank(rangeEnd)) {
            queryString += " OR((si.cf_values\\:\\:JSON->'RANGE_START'->0 ->>'long')\\:\\:BIGINT <= :rangeEnd AND (si.cf_values\\:\\:JSON->'RANGE_END'->0 ->>'long')\\:\\:BIGINT >= :rangeEnd ))";
        } else {
            queryString += ")";
        }

        queryString += "  AND si.subscription_id = sub.id AND ( (sub.cf_values\\:\\:JSON->'SERVICE_ID'->0 ->>'long')\\:\\:BIGINT is null or (sub.cf_values\\:\\:JSON->'SERVICE_ID'->0 ->>'long')\\:\\:BIGINT <> :rangeStart )";

        Query query = getEntityManager().createNativeQuery(queryString);

        query.setParameter("rangeStart", Long.valueOf(rangeStart));
        if (!StringUtils.isBlank(rangeEnd)) {
            query.setParameter("rangeEnd", Long.valueOf(rangeEnd));
        }

        query.setMaxResults(1);

        return query.getResultList().isEmpty();
    }

    /**
     * Apply the one shot charge
     *
     * @param orderDto         the order Dto
     * @param orderLineDto     the order line Dto
     * @param subscriptionCode the subscription code
     * @throws BusinessException the business exception
     */
    protected void applyOneShotCharge(OrderDto orderDto, OrderLineDto orderLineDto, String subscriptionCode) throws BusinessException {
        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, subscriptionCode);
        if (subscription == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The subscription " + orderLineDto.getSubscriptionCode() + " is not found");
            return;
        }
        String billCode = orderLineDto.getBillAction() + orderLineDto.getBillCode();
        OneShotChargeTemplate chargetemplate = orderService.findEntityByIdOrCode(oneShotChargeTemplateService, orderDto, billCode);
        if (chargetemplate == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The one-shot charge " + billCode + " is not found");
            return;
        }

        OneShotChargeInstance chargeInstance = new OneShotChargeInstance();
        Map<String, String> chargeCF = getServiceParametersCF(orderDto, orderLineDto, chargeInstance);
        chargeInstance.setCfValue(ChargeInstanceCFsEnum.SERVICE_PARAMETERS.name(), chargeCF);
        chargeInstance.setCfValue(ChargeInstanceCFsEnum.SERVICE_CODE.name(), billCode);
        chargeCF.put(ServiceParametersCFEnum.QUANTITY.getLabel(), "1");

        chargeInstance = oneShotChargeInstanceService.oneShotChargeApplication(subscription, null, chargetemplate, null,
                orderLineDto.getBillEffectDate(), null, null, BigDecimal.ONE, null, null, null,
                null, orderDto.getOrderCode(), chargeInstance.getCfValues(), true, ChargeApplicationModeEnum.SUBSCRIPTION);
        chargeInstance.setCode(billCode);
    }

    /**
     * Process order
     *
     * @param updateOrderStatusFunction the change order status function to be executed
     * @param parameters                the function parameters
     * @param <T>
     * @return
     * @throws BusinessException the business exception
     */
    public <T> void updateOrderStatus(Consumer<T> updateOrderStatusFunction, Object... parameters) throws BusinessException {
        if (parameters == null || parameters.length < 1 || parameters[0] == null) {
            return;
        }
        T dto = (T) parameters[0];
        OrderDto orderDto = (OrderDto) dto;
        if (orderDto != null) {
            boolean isOrderAlreadyCached = orderCacheContainerProvider.addToCache("ORDER_CODE_" + orderDto.getOrderCode());
            if (!isOrderAlreadyCached) {
                try {
                    currentUserProvider.forceAuthentication((orderDto).getUserName(), (orderDto).getProviderCode());
                    updateOrderStatusFunction.accept(dto);
                } finally {
                    orderCacheContainerProvider.removeFomCache("ORDER_CODE_" + orderDto.getOrderCode());
                }
            }
        }
    }

    protected EntityManager getEntityManager() {
        return emWrapper.getEntityManager();
    }
}