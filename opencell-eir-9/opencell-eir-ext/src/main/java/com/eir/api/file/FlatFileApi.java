package com.eir.api.file;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.meveo.admin.exception.BusinessException;
import org.meveo.api.BaseApi;
import org.meveo.api.dto.AuditableDto;
import org.meveo.api.dto.bi.FlatFileDto;
import org.meveo.api.dto.flatfile.FlatFilesDto;
import org.meveo.api.dto.response.PagingAndFiltering;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.cache.JobCacheContainerProvider;
import org.meveo.cache.JobRunningStatusEnum;
import org.meveo.commons.utils.EjbUtils;
import org.meveo.commons.utils.ParamBean;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.admin.FileFormat;
import org.meveo.model.bi.FlatFile;
import org.meveo.model.jobs.JobExecutionResultImpl;
import org.meveo.model.jobs.JobInstance;
import org.meveo.model.jobs.JobLauncherEnum;
import org.meveo.service.admin.impl.FileFormatService;
import org.meveo.service.bi.impl.FlatFileService;
import org.meveo.service.job.JobExecutionService;
import org.meveo.service.job.JobInstanceService;

/**
 * The flat file API.
 *
 * @author Abdellatif BARI
 */
@Stateless
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class FlatFileApi extends BaseApi {

    @Inject
    FileFormatService fileFormatService;

    @Inject
    FlatFileService flatFileService;

    @Inject
    JobInstanceService jobInstanceService;

    @Inject
    JobExecutionService jobExecutionService;

    @Inject
    JobCacheContainerProvider jobCacheContainerProvider;

    /**
     * Reprocess a flat file order
     *
     * @param flatFilesDto flat file dto.
     * @throws BusinessException business exception
     */
    public void reprocess(FlatFilesDto flatFilesDto) throws BusinessException {
        if (StringUtils.isBlank(flatFilesDto.getFileFormatCode())) {
            throw new BusinessException("Missing file format.");
        }
        FileFormat fileFormat = fileFormatService.findByCode(flatFilesDto.getFileFormatCode());
        if (fileFormat == null) {
            throw new BusinessException("invalid file format.");
        }
        prepareFiles(flatFilesDto, fileFormat);
    }

    /**
     * Move rejected files to job input directory
     *
     * @param flatFilesDto flat file dto.
     * @param fileFormat file format.
     * @throws BusinessException the business exception
     */
    private void prepareFiles(FlatFilesDto flatFilesDto, FileFormat fileFormat) throws BusinessException {

        String inputDirectory = getDirectory(fileFormat.getInputDirectory(), fileFormat);
        String rejectDirectory = getRejectDirectory(fileFormat);

        if (isEmptyDirectory(rejectDirectory)) {
            throw new BusinessException("the rejection directory : '" + rejectDirectory + "' is empty.");
        }

        File[] files = new File(rejectDirectory).listFiles();

        // Sort files in descending order base on last modification date
        Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);

        String flatFileJobCode = null;

        Iterator iterator = flatFilesDto.getFlatFileIds().iterator();
        // iterate through json ids array
        while (iterator.hasNext()) {
            Long flatFileId = (Long) iterator.next();
            FlatFile flatFile = flatFileService.findById(flatFileId);
            if (flatFile == null) {
                throw new BusinessException("Flat file with id=" + flatFileId + " does not exists.");
            }

            if (flatFileJobCode == null) {
                flatFileJobCode = flatFile.getFlatFileJobCode();
            }

            File file = null;
            String originalNameFile = null;
            for (File currentFile : files) {
                if (currentFile != null) {
                    if (currentFile.getName().startsWith(flatFile.getCode() + "_" + flatFile.getFileOriginalName())) {
                        originalNameFile = flatFile.getCode() + "_" + flatFile.getFileOriginalName();
                        file = currentFile;
                        break;
                    }
                    // case where the file is directly deposited in job input directory and automatically it didn't passed
                    // by a pre validation which implies that its name is not modified (name prefix by its code).
                    if (currentFile.getName().startsWith(flatFile.getFileOriginalName())) {
                        originalNameFile = flatFile.getCode() + "_" + flatFile.getFileOriginalName();
                        file = currentFile;
                        break;
                    }
                }
            }

            if (file == null) {
                throw new BusinessException("Flat file with id=" + fileFormat.getCode() + "_" + flatFile.getFileOriginalName() + " does not exists in rejection directory.");
            }

            try {
                copyFile(file.getName(), originalNameFile, rejectDirectory, inputDirectory);
            } catch (IOException e) {
                log.error("move flat files into job input directory fail", e);
                throw new BusinessException(e.getMessage(), e);
            }
        }

        reProcessFiles(fileFormat, flatFileJobCode);
    }

    /**
     * Reprocess Files
     *
     * @param fileFormat file format.
     * @param flatFileJobCode flat file job code.
     * @throws BusinessException the business exception
     */
    private void reProcessFiles(FileFormat fileFormat, String flatFileJobCode) throws BusinessException {
        if (StringUtils.isBlank(flatFileJobCode)) {
            throw new BusinessException("the selected files are never processed");
        }
        String inputDirectory = getDirectory(fileFormat.getInputDirectory(), fileFormat);
        if (isEmptyDirectory(inputDirectory)) {
            throw new BusinessException("the selected files are not copied to job input directory");
        }

        JobInstance jobInstance = jobInstanceService.findByCode(flatFileJobCode);
        if (jobInstance == null) {
            throw new BusinessException("Job instance with code=" + flatFileJobCode + " does not exists.");
        }

        if (!isAllowedToExecute(jobInstance)) {
            throw new BusinessException("the " + flatFileJobCode + " job can not be run on a current server or cluster node");
        }

        try {
            jobExecutionService.executeJob(jobInstance, null, JobLauncherEnum.API);
            
        } catch (Exception e) {
            log.error("execute flat file job fail", e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Check if empty directory
     *
     * @param path directory path
     * @return true if the input directory is empty
     */
    private boolean isEmptyDirectory(String path) {
        File file = new File(path);
        if (file.isDirectory() && file.list().length > 0) {
            return false;
        }
        return true;
    }

    /**
     * Get absolute path
     *
     * @param path relative path
     * @param fileFormat file format.
     * @return the absolute path
     * @throws BusinessException the business exception
     */
    private String getDirectory(String path, FileFormat fileFormat) throws BusinessException {
        String directory = null;
        if (fileFormat != null && !StringUtils.isBlank(path)) {
            // StringBuilder reportDir = new StringBuilder(ParamBean.getInstance().getChrootDir(provider.getCode()));
            StringBuilder reportDir = new StringBuilder(ParamBean.getInstance().getChrootDir(""));
            reportDir.append(File.separator).append(path);
            directory = reportDir.toString();
        }
        if (StringUtils.isBlank(directory)) {
            throw new BusinessException("The " + path + "directory is missing");
        }
        return directory;
    }

    /**
     * Get reject directory
     *
     * @param fileFormat file format.
     * @return the reject directory
     */
    private String getRejectDirectory(FileFormat fileFormat) {
        String rejectDirectory = null;
        if (StringUtils.isBlank(fileFormat.getRejectDirectory())) {
            String inputDirectory = getDirectory(fileFormat.getInputDirectory(), fileFormat);
            if (inputDirectory.endsWith(File.separator)) {
                inputDirectory = inputDirectory.substring(0, inputDirectory.lastIndexOf(File.separator));
            }
            rejectDirectory = inputDirectory + File.separator + "reject";
        } else {
            getDirectory(fileFormat.getRejectDirectory(), fileFormat);
        }
        return rejectDirectory;
    }

    /**
     * Copy file
     *
     * @param fileName file name
     * @param originalNameFile original name file
     * @param sourcePath source path
     * @param targetPath target path
     * @throws IOException the IO exception
     */
    private void copyFile(String fileName, String originalNameFile, String sourcePath, String targetPath) throws IOException {
        if (!StringUtils.isBlank(fileName) && !StringUtils.isBlank(sourcePath) && !StringUtils.isBlank(targetPath)) {
            Path sourceDirectory = Paths.get(sourcePath.endsWith(File.separator) ? sourcePath + fileName : sourcePath + File.separator + fileName);
            Path targetDirectory = Paths.get(targetPath.endsWith(File.separator) ? targetPath + originalNameFile : targetPath + File.separator + originalNameFile);

            // copy source to target
            Files.copy(sourceDirectory, targetDirectory, StandardCopyOption.REPLACE_EXISTING);
        }
    }

    /**
     * Check if job can be run on a current server or cluster node if deployed in cluster environment
     *
     * @param jobInstance JobInstance entity
     * @return True if it can be executed locally
     */
    public boolean isAllowedToExecute(JobInstance jobInstance) {
        if (jobInstance == null || jobInstance.getId() == null) {
            return false;
        }

        JobRunningStatusEnum isRunning = jobCacheContainerProvider.isJobRunning(jobInstance.getId());
        if (isRunning == JobRunningStatusEnum.NOT_RUNNING) {
            return true;
        } else if (isRunning == JobRunningStatusEnum.RUNNING_THIS) {
            return false;
        } else {
            String nodeToCheck = EjbUtils.getCurrentClusterNode();
            return jobInstance.isRunnableOnNode(nodeToCheck);
        }
    }

    /**
     * Get a unique list of filenames that are in a given staging table and match a given file format
     * 
     * @param pagingAndFiltering Search parameters. Expected to contain these parameters: tableName and fileFormat
     * @return A list of file names with id
     */
    public List<FlatFileDto> getFilesInStagingTable(PagingAndFiltering pagingAndFiltering) {

        String tableName = (String) pagingAndFiltering.getFilters().get("tableName");

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("fileFormat", (String) pagingAndFiltering.getFilters().get("fileFormat"));
        List<Map<String, Object>> fileInfos = flatFileService.executeNativeSelectQuery(
            "select fff.id, fff.code, fff.file_original_name, fff.created, fff.creator from flat_file fff, adm_file_format ff where fff.file_format_id = ff.id and ff.code=:fileFormat and fff.id in (select distinct(file_id) from "
                    + tableName + ")",
            params);

        List<FlatFileDto> files = new ArrayList<FlatFileDto>();
        for (Map<String, Object> fileInfo : fileInfos) {
            FlatFileDto file = new FlatFileDto();
            file.setId(((BigInteger) fileInfo.get("id")).longValue());
            file.setCode((String) fileInfo.get("code"));
            file.setFileOriginalName((String) fileInfo.get("file_original_name"));
            AuditableDto auditable = new AuditableDto();
            auditable.setCreated((Date) fileInfo.get("created"));
            auditable.setCreator((String) fileInfo.get("creator"));
            file.setAuditable(auditable);

            files.add(file);
        }

        return files;
    }
}
