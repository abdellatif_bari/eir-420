package com.eir.api.order;

import com.eir.commons.enums.ApplicationPropertiesEnum;
import com.eir.commons.enums.EndPointTypeEnum;
import com.eir.commons.enums.OrderEmitterEnum;
import com.eir.commons.enums.OrderLineAttributeNamesEnum;
import com.eir.commons.enums.ServiceTypeEnum;
import com.eir.commons.enums.TerminationReasonEnum;
import com.eir.commons.enums.customfields.OrderCFsEnum;
import com.eir.commons.enums.customfields.ProviderCFsEnum;
import com.eir.commons.enums.customfields.ServiceInstanceCFsEnum;
import com.eir.commons.enums.customfields.ServiceParametersCFEnum;
import com.eir.commons.enums.customfields.ServiceTemplateCFsEnum;
import com.eir.commons.enums.customfields.SubscriptionCFsEnum;
import com.eir.commons.enums.customtables.AssociationCustomTableEnum;
import com.eir.commons.enums.customtables.EndPointCustomTableEnum;
import org.apache.commons.httpclient.util.DateUtil;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.api.commons.ErrorEnum;
import org.meveo.api.commons.Utils;
import org.meveo.api.dto.order.AttributeDto;
import org.meveo.api.dto.order.ComplimentaryOrderDto;
import org.meveo.api.dto.order.OrderDto;
import org.meveo.api.dto.order.OrderLineDto;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.InstanceStatusEnum;
import org.meveo.model.billing.RecurringChargeInstance;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.SubscriptionStatusEnum;
import org.meveo.model.billing.SubscriptionTerminationReason;
import org.meveo.model.billing.UserAccount;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.ServiceChargeTemplateRecurring;
import org.meveo.model.catalog.ServiceChargeTemplateSubscription;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.crm.custom.CustomFieldValue;
import org.meveo.model.order.Order;
import org.meveo.model.order.OrderStatusEnum;
import org.meveo.model.shared.DateUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

/**
 * The OMS order API.
 *
 * @author Abdellatif BARI
 */
@Stateless
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class OMSOrderApi extends BaseOrderApi {

    @EJB
    OMSOrderApi omsOrderApi;

    /**
     * Item type enum
     */
    private enum ItemTypeEnum {
        SUBSCRIPTION_MAIN_DETAILS("Subscription main details"), END_POINT("EndPoint"), ASSOCIATION("Association"),
        CHARGE("Charge"), REMARKS("Remark");

        private String label;

        private static final Map<String, ItemTypeEnum> ENUM_MAP;

        ItemTypeEnum(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }

        static {
            Map<String, ItemTypeEnum> map = new ConcurrentHashMap<>();
            for (ItemTypeEnum instance : ItemTypeEnum.values()) {
                map.put(instance.getLabel().toUpperCase(), instance);
            }
            ENUM_MAP = Collections.unmodifiableMap(map);
        }

        public static ItemTypeEnum get(String label) {
            if (!StringUtils.isBlank(label)) {
                label = label.toUpperCase();
            }
            return ENUM_MAP.get(label);
        }
    }

    /**
     * State enum
     */
    private enum StateEnum {
        OR("Ordered"), AS("Assigned"), CA("Cancelled");

        private String label;

        StateEnum(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    /**
     * Period unit Enum
     */
    public enum PeriodUnitEnum {
        Days, Months, Years
    }

    /**
     * Association types Enum
     */
    private enum AssociationTypesEnum {
        CH("Channeling"), BR("Bearing"), AS("Associated"), DI("Diversity");

        private String label;

        AssociationTypesEnum(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    /**
     * Min Term attribute names Enum
     */
    private enum MinTermAttributeNamesEnum {
        START_DATE, END_DATE, OAO, PRODUCT_CODE, MINIMUM_TERM_PERIOD, NOTICE_CEASE_PERIOD
    }

    /**
     * Service migration attribute names Enum
     */
    private enum ServiceMigrationAttributeNamesEnum {
        EVENT, APPLY_MT_ON_PREVIOUS, CONNECTION_CHARGE_ON, RESET_ENGAGEMENT, BILL_CODE_TO_APPLY
    }

    /**
     * Check if the item is a service or another thing like EndPoint, Association, Remark,......
     *
     * @param orderLineDto the The order line Dto
     * @return true if the item is a service
     */
    protected boolean isService(OrderLineDto orderLineDto) {
        if (ItemTypeEnum.END_POINT.getLabel().equalsIgnoreCase(orderLineDto.getItemType()) || ItemTypeEnum.ASSOCIATION.getLabel().equalsIgnoreCase(orderLineDto.getItemType())
                || ItemTypeEnum.REMARKS.getLabel().equalsIgnoreCase(orderLineDto.getItemType())) {
            return false;
        }
        return true;
    }

    /**
     * Validate service template
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the The order line Dto
     * @return true is service template is valid.
     * @throws BusinessException the business exception
     */
    private boolean validateServiceTemplate(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        if (!isService(orderLineDto)) {
            return true;
        }
        ServiceTemplate serviceTemplate = getServiceTemplate(orderDto, orderLineDto);
        if (serviceTemplate == null) {
            return false;
        }
        String serviceType = (String) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.SERVICE_TYPE.name());
        if (StringUtils.isBlank(serviceType)) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Service Type is not found for service " + orderLineDto.getBillCode());
            return false;
        }
        orderLineDto.setServiceType(serviceType);

        Double maxQuantity = (Double) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MAX_QUANTITY.name());
        if (maxQuantity == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The MAX_QUANTITY attribute is missing for the service template " + orderLineDto.getBillCode());
            return false;
        }
        if (new BigDecimal(maxQuantity).compareTo(new BigDecimal(orderLineDto.getBillQuantity())) < 0) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The quantity provided exceeds the maximum quantity allowed for the service : " + orderLineDto.getBillCode());
            return false;
        }
        BigDecimal billQuantity = new BigDecimal(orderLineDto.getBillQuantity());
        if (billQuantity.compareTo(BigDecimal.ZERO) < 0) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The service quantity must be greater than 0 for the service : " + orderLineDto.getBillCode());
            return false;
        }

        if (!validateSlaSurcharge(orderDto, orderLineDto, serviceTemplate.getId())) {
            return false;
        }

        //setChargeType(orderLineDto, serviceTemplate);

        String amt = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_AMT.getLabel());
        List<ServiceChargeTemplateRecurring> serviceRecurringCharges = serviceTemplate.getServiceRecurringCharges();
        if (!StringUtils.isBlank(amt) && (serviceRecurringCharges == null || serviceRecurringCharges.isEmpty())) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "There is no recurring charge configured for the service : " + orderLineDto.getBillCode());
            return false;
        }
        String amtooc = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_AMTOOC.getLabel());
        List<ServiceChargeTemplateSubscription> serviceSubscriptionCharges = serviceTemplate.getServiceSubscriptionCharges();
        if (!StringUtils.isBlank(amtooc) && (serviceSubscriptionCharges == null || serviceSubscriptionCharges.isEmpty())) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "There is no one shot charge configured for the service : " + orderLineDto.getBillCode());
            return false;
        }
        return true;
    }

    /**
     * Validate offer template
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the The order line Dto
     * @return true is offer template is valid.
     * @throws BusinessException the business exception
     */
    private boolean validateOfferTemplate(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        if (!isService(orderLineDto)) {
            return true;
        }
        ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
        if (serviceTemplate == null) {
            return false;
        }
        OfferTemplate offerTemplate = getOfferTemplate(orderDto, serviceTemplate);
        if (offerTemplate == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The offer is not found for a billCode : " + orderLineDto.getBillCode());
            return false;
        }
        orderLineDto.setOfferTemplateCode(offerTemplate.getCode());
        return true;
    }

    /**
     * Validate billing account
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the The order line Dto
     * @return true is billing account is valid.
     * @throws BusinessException the business exception
     */
    private boolean validateBillingAccount(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        if (!StringUtils.isBlank(orderLineDto.getMasterAccountNumber())) {
            /*
            BillingAccount billingAccount = billingAccountService.findByNumber(orderLineDto.getMasterAccountNumber());
            causes the STATUS_MARKED_ROLLBACK  exception later when executing any hql query
            */

            List<BillingAccount> billingAccounts = billingAccountService.findBillingAccountsByNumber(orderLineDto.getMasterAccountNumber());
            if (billingAccounts == null || billingAccounts.isEmpty()) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "The billing account is not found for a number : " + orderLineDto.getMasterAccountNumber());
                return false;
            } else if (billingAccounts.size() > 1) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "There are multiple billing accounts with the same number : " + orderLineDto.getMasterAccountNumber());
                return false;
            }
            orderLineDto.setBillingAccountCode(billingAccounts.get(0).getCode());
        } else if (!StringUtils.isBlank(orderLineDto.getOfferTemplateCode())) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The billing account number is required.");
            return false;
        }
        return true;
    }

    /**
     * Validate subscription
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the The order line Dto
     * @return true is subscription is valid.
     * @throws BusinessException the business exception
     */
    private boolean validateSubscription(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        if (!StringUtils.isBlank(orderLineDto.getSubscriptionCode())) {
            orderLineDto.setOriginalSubscriptionCode(orderLineDto.getSubscriptionCode());
            orderLineDto.setSubscriptionCode(getSubscriptionCodeWithPrefix(orderDto, orderLineDto));
            Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
            if (subscription != null) {
                if (subscription.getStatus() != SubscriptionStatusEnum.ACTIVE) {
                    setError(orderDto, ErrorEnum.INVALID_DATA, "The subscription " + orderLineDto.getSubscriptionCode() + " is no longer active");
                    return false;
                }
                if (subscription.getUserAccount() != null && subscription.getUserAccount().getBillingAccount() != null) {
                    if (!StringUtils.isBlank(orderLineDto.getMasterAccountNumber())) {
                        if (!orderLineDto.getMasterAccountNumber().equals(subscription.getUserAccount().getBillingAccount().getExternalRef1())) {
                            setError(orderDto, ErrorEnum.INVALID_DATA, "Subscription and billing account mismatch");
                            return false;
                        }
                    } else {
                        orderLineDto.setMasterAccountNumber(subscription.getUserAccount().getBillingAccount().getExternalRef1());
                    }
                }
                if (!StringUtils.isBlank(orderLineDto.getOfferTemplateCode()) && !orderLineDto.getOfferTemplateCode()
                        .equalsIgnoreCase(subscription.getOffer().getCode())) {
                    setError(orderDto, ErrorEnum.INVALID_DATA, "The subscription " + orderLineDto.getSubscriptionCode() +
                            " is not associated with the offer " + orderLineDto.getOfferTemplateCode());
                    return false;
                }
            } else if (ItemTypeEnum.SUBSCRIPTION_MAIN_DETAILS.getLabel().equalsIgnoreCase(orderLineDto.getItemType())
                    && BillActionEnum.P.name().equalsIgnoreCase(orderLineDto.getOriginalOrderAction())) {
                orderDto.getProvideMainSubscriptionCodes().add(orderLineDto.getSubscriptionCode());
            } else if (!orderDto.getProvideMainSubscriptionCodes().contains(orderLineDto.getSubscriptionCode())) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "The subscription " + orderLineDto.getSubscriptionCode() + " is not found");
                return false;
            }
        } else {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The Service_Id parameter is missing");
            return false;
        }
        return true;
    }

    /**
     * Validate order line prerequisites
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the The order line Dto
     * @throws BusinessException the business exception
     */
    @Override
    protected void validateOrderLinePrerequisites(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        if (BillActionEnum.RB.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
            return;
        }

        // Check service template (service template is mandatory)
        if (!validateServiceTemplate(orderDto, orderLineDto)) {
            return;
        }

        // Check Offer (Offer is not mandatory)
        if (!validateOfferTemplate(orderDto, orderLineDto)) {
            return;
        }

        // Check billing account (billing account is mandatory)
        if (!validateBillingAccount(orderDto, orderLineDto)) {
            return;
        }

        // Check the subscription before the billing account
        //  (we can deduce the billing account from subscription if it's not provided)
        if (!validateSubscription(orderDto, orderLineDto)) {
            return;
        }
    }

    /**
     * Validate order line
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    @Override
    protected void validateOrderLine(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        /** Item_Id **/
        if (!validateField(orderDto, "Item_Id", orderLineDto.getItemId(), null, String.class, 50, true)) {
            return;
        }
        /** Operator_Id **/
        if (!validateField(orderDto, "Operator_Id", orderLineDto.getOperatorId(), null, String.class, 10, true)) {
            return;
        }
        boolean isItemComponentIdMandatory = true;
        if (BillActionEnum.RB.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
            isItemComponentIdMandatory = false;
        }
        /** Item_Component_Id **/
        if (!validateField(orderDto, "Item_Component_Id", orderLineDto.getItemComponentId(), null, String.class, 15, isItemComponentIdMandatory)) {
            return;
        }
        /** Item_Version **/
        if (!validateField(orderDto, "Item_Version", orderLineDto.getItemVersion(), null, String.class, 3, false)) {
            return;
        }
        /** Item_Type **/
        if (!validateField(orderDto, "Item_Type", orderLineDto.getItemType(), null, String.class, 50, false)) {
            return;
        }
        /** Agent_Code **/
        if (!validateField(orderDto, "Agent_Code", orderLineDto.getAgentCode(), null, String.class, 8, false)) {
            return;
        }
        /** UAN **/
        if (!validateField(orderDto, "UAN", orderLineDto.getUan(), null, String.class, 20, false)) {
            return;
        }
        /** Master_Account_Number **/
        if (!validateField(orderDto, "Master_Account_Number", orderLineDto.getMasterAccountNumber(), null, String.class, 20, false)) {
            return;
        }
        /** Service_Id **/
        if (!validateField(orderDto, "Service_Id", orderLineDto.getSubscriptionCode(), null, String.class, 30, false)) {
            return;
        }
        /** Ref_Service_Id **/
        if (!validateField(orderDto, "Ref_Service_Id", orderLineDto.getRefSubscriptionCode(), null, String.class, 30, false)) {
            return;
        }
        /** Xref_Service_Id **/
        if (!validateField(orderDto, "Xref_Service_Id", orderLineDto.getXRefSubscriptionCode(), null, String.class, 30, false)) {
            return;
        }
        /** Bill_Code **/
        if (!validateField(orderDto, "Bill_Code", orderLineDto.getBillCode(), null, String.class, 100, true)) {
            return;
        }
        /** Action **/
        if (!validateField(orderDto, "Action", orderLineDto.getBillAction(), null, String.class, 10, true)) {
            return;
        }
        if (!BillActionEnum.contains(orderLineDto.getBillAction())) {
            setBadRequestError(orderDto, ErrorEnum.BAD_REQUEST_INVALID_VALUE, "Action");
            return;
        }
        /** Action_Qualifier **/
        if (!validateField(orderDto, "Action_Qualifier", orderLineDto.getBillActionQualifier(), null, String.class, 50, false)) {
            return;
        }
        /** Quantity **/
        if (!validateField(orderDto, "Quantity", orderLineDto.getBillQuantity(), null, Integer.class, 5, true)) {
            return;
        }
        /** Effect_Date **/
        if (!validateField(orderDto, "Effect_Date", orderLineDto.getBillEffectDate(), null, Date.class, null, true)) {
            return;
        }
        /** Special_Connect **/
        if (!validateField(orderDto, "Special_Connect", orderLineDto.getSpecialConnect(), null, Integer.class, 20, false)) {
            return;
        }
        /** Special_Rental **/
        if (!validateField(orderDto, "Special_Rental", orderLineDto.getSpecialRental(), null, Integer.class, 20, false)) {
            return;
        }
        /** Operator_Order_Number **/
        if (!validateField(orderDto, "Operator_Order_Number", orderLineDto.getOperatorOrderNumber(), null, String.class, 20, false)) {
            return;
        }

        for (AttributeDto attributeDto : orderLineDto.getAttributes()) {

            /** Code **/
            if (!validateField(orderDto, "Code", attributeDto.getCode(), null, String.class, 50, true)) {
                return;
            }
            /** Value **/
            if (!validateField(orderDto, "Value", attributeDto.getValue(), null, String.class, 80, true)) {
                return;
            }
            if (OrderLineAttributeNamesEnum.OMS_AMT.getLabel().equalsIgnoreCase(attributeDto.getCode()) ||
                    OrderLineAttributeNamesEnum.OMS_AMTOOC.getLabel().equalsIgnoreCase(attributeDto.getCode())) {
                if (!validateField(orderDto, "Value", attributeDto.getValue(), null, Double.class, null, false)) {
                    return;
                }
            }
            /** Unit **/
            if (!validateField(orderDto, "Unit", attributeDto.getUnit(), null, String.class, 10, false)) {
                return;
            }
            /** Component_Id **/
            if (!validateField(orderDto, "Component_Id", attributeDto.getComponentId(), null, String.class, 15, false)) {
                return;
            }
            /** Attribute_Type **/
            if (!validateField(orderDto, "Attribute_Type", attributeDto.getAttributeType(), null, String.class, 50, false)) {
                return;
            }
        }
        validateOrderLinePrerequisites(orderDto, orderLineDto);
    }

    /**
     * Validate order
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    @Override
    protected void validateOrder(OrderDto orderDto) throws BusinessException {

        resetOrder(orderDto);

        /** Ordering_System **/
        if (!validateField(orderDto, "Ordering_System", orderDto.getOrderingSystem(), OrderEmitterEnum.OMS.name(), String.class, 5, true)) {
            return;
        }
        /** Order_Id **/
        if (!validateField(orderDto, "Order_Id", orderDto.getOrderCode(), null, String.class, 15, true)) {
            return;
        }
        /** Ref_Order_Id **/
        if (!validateField(orderDto, "Ref_Order_Id", orderDto.getRefOrderCode(), null, String.class, 15, false)) {
            return;
        }
        /** Transaction_Id **/
        if (!validateField(orderDto, "Transaction_Id", orderDto.getTransactionId(), null, String.class, 25, true)) {
            return;
        }

        // Check if the order is too large
        if (isOrderTooLarge(orderDto)) {
            return;
        }

        if (orderDto.getStatus() == OrderStatusEnum.IN_CREATION && isOrderAlreadyExists(orderDto)) {
            return;
        }

        for (OrderLineDto orderLineDto : orderDto.getOrderLines()) {
            if (orderDto.getErrorCode() != null) {
                return;
            }
            validateOrderLine(orderDto, orderLineDto);
        }
    }


    /**
     * To process the order in new Transaction and if an error is found we Rollback all order items.
     * but we can save the order with error in the next service by joining the current transaction.
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    private void processOrderLines(OrderDto orderDto) throws BusinessException {

        int i = 1;
        EntityManager em = getEntityManager();
        for (OrderLineDto orderLineDto : orderDto.getOrderLines()) {

            processOrderLine(orderDto, orderLineDto);

            if (i % 100 == 0) {
                em.flush();
                em.clear();
            }
            i++;
        }

        // Rerate order services
        rerateServices(orderDto);
    }

    /**
     * Process a single order item
     *
     * @param orderDto     Order DTO
     * @param orderLineDto Order line DTO
     */
    private void processOrderLine(OrderDto orderDto, OrderLineDto orderLineDto) {

        //validateServiceInstance(orderDto, orderLineDto, new String[]{BillActionEnum.C.name()});

        if (orderDto.getErrorCode() == null) {
            switch (BillActionEnum.valueOfIgnoreCase(orderLineDto.getBillAction())) {
                // OMS Provide
                case P:
                    provideOrder(orderDto, orderLineDto);
                    break;
                // OMS Change
                case CH:
                    changeOrder(orderDto, orderLineDto);
                    break;
                // OMS Cease
                case C:
                    ceaseOrder(orderDto, orderLineDto);
                    break;
                // OMS Rebate
                case RB:
                    miscCharge(orderDto, orderLineDto);
                    break;
            }
        }

        if (orderDto.getStatus() == OrderStatusEnum.DEFERRED) {
            return;
        }

        if (orderDto.getErrorCode() != null) {
            // Rollback transaction
            throw new ValidationException(orderDto.getErrorMessage());
        }

    }

    /**
     * Set complimentary order Dto
     *
     * @param orderDto the order Dto
     */
    private void setComplimentaryOrderDto(OrderDto orderDto) {
        Order refOrder = getComplementaryOrder(orderDto);
        orderDto.setComplimentaryOrderDto(getComplimentaryOrderDto(refOrder));
    }

    /**
     * Execute created order by activating/changing/ceasing services in subscription
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    @Override
    protected void executeOrder(OrderDto orderDto) throws BusinessException {

        // Don't execute order if there is at least one error or if it is a deferred, held or waiting
        if (isHeldOrder(orderDto) || orderDto.getErrorCode() != null || isDeferredOrder(orderDto) || isWaitingOrder(orderDto)) {
            return;
        }

        // Set the referenced order
        setComplimentaryOrderDto(orderDto);

        processOrderLines(orderDto);

        if (orderDto.getErrorCode() == null) {
            if (orderDto.getStatus() != OrderStatusEnum.DEFERRED) {
                orderDto.setStatus(OrderStatusEnum.COMPLETED);
            }
        }
    }

    /**
     * Initialize the order Dto
     *
     * @param orderDto the order DTO
     */
    private void initOrder(OrderDto orderDto) {
        // Store original xml order
        setOriginalXmlOrder(orderDto);

        for (OrderLineDto orderLineDto : orderDto.getOrderLines()) {
            // Set subscription codes
            orderDto.getSubscriptionCodes().add(orderLineDto.getSubscriptionCode());
            if (!StringUtils.isBlank(orderLineDto.getRefSubscriptionCode())) {
                orderDto.getRefSubscriptionCodes().add(orderLineDto.getRefSubscriptionCode());
            }
        }
    }

    /**
     * Initialize the order context
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    @Override
    protected void init(OrderDto orderDto) {
        // Initialize the order Dto
        initOrder(orderDto);
        // Sort Order lines
        sortOrderLines(orderDto);
    }

    /**
     * Sort orderLines
     *
     * @param orderDto the order Dto
     */
    @Override
    protected void sortOrderLines(OrderDto orderDto) {

        // 1 : Charge Items with a STATUS=AC must be processed first/before any of the other bill items on the order
        Collections.sort(orderDto.getOrderLines(), new Comparator<OrderLineDto>() {
            @Override
            public int compare(OrderLineDto orderLineDto1, OrderLineDto orderLineDto2) {
                return getPosition(orderLineDto1) - getPosition(orderLineDto2);
            }

            private int getPosition(OrderLineDto orderLineDto) {
                if (ItemTypeEnum.SUBSCRIPTION_MAIN_DETAILS.getLabel().equalsIgnoreCase(orderLineDto.getItemType())) {
                    return 1;
                } else if (ItemTypeEnum.END_POINT.getLabel().equalsIgnoreCase(orderLineDto.getItemType())) {
                    return 2;
                } else if (ItemTypeEnum.ASSOCIATION.getLabel().equalsIgnoreCase(orderLineDto.getItemType())) {
                    return 3;
                } else if (ItemTypeEnum.CHARGE.getLabel().equalsIgnoreCase(orderLineDto.getItemType())) {
                    String status = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_STATUS.getLabel());
                    if (ItemStatusEnum.CE.getLabel().equalsIgnoreCase(status)) {
                        return 4;
                    } else if (ItemStatusEnum.AC.getLabel().equalsIgnoreCase(status)) {
                        return 5;
                    }
                    return 6;
                }
                return 7;
            }

            /** Old sort by bill action **/
            /*
            private int getPosition(OrderLineDto orderLineDto) {
                String status = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_STATUS.getLabel());
                if (BillActionEnum.P.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
                    if (ItemTypeEnum.SUBSCRIPTION_MAIN_DETAILS.name().equalsIgnoreCase(orderLineDto.getItemType())) {
                        return 1;
                    }
                    return 2;
                } else if (BillActionEnum.CH.name().equalsIgnoreCase(orderLineDto.getBillAction()) && ItemStatusEnum.AC.name().equalsIgnoreCase(status)) {
                    return 3;
                } else if (BillActionEnum.CH.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
                    return 4;
                } else if (BillActionEnum.C.name().equalsIgnoreCase(orderLineDto.getBillAction()) && ItemStatusEnum.CE.name().equalsIgnoreCase(status)) {
                    return 5;
                } else if (BillActionEnum.C.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
                    return 6;
                }
                return 7;
            }
            */
        });
    }

    /**
     * Provide order
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void provideOrder(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        switch (ItemTypeEnum.get(orderLineDto.getItemType())) {
            // OMS Provide: Subscription Main Details
            case SUBSCRIPTION_MAIN_DETAILS:
                provideSubscriptionMainDetails(orderDto, orderLineDto);
                break;
            // OMS Provide: End Points
            case END_POINT:
                provideEndPoints(orderDto, orderLineDto);
                break;
            // OMS Provide: Associations
            case ASSOCIATION:
                provideAssociations(orderDto, orderLineDto);
                break;
            // OMS Orders: Process Service Charges
            case CHARGE:
                processServiceCharges(orderDto, orderLineDto);
                break;
            // Provide: Remarks
            case REMARKS:
                provideRemarks(orderDto, orderLineDto);
                break;
        }
    }

    /**
     * Change order
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void changeOrder(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        switch (ItemTypeEnum.get(orderLineDto.getItemType())) {
            // OMS Change: Subscription Main Details
            case SUBSCRIPTION_MAIN_DETAILS:
                changeSubscriptionMainDetails(orderDto, orderLineDto);
                break;
            // OMS Change: End Points
            case END_POINT:
                changeEndPoints(orderDto, orderLineDto);
                break;
            // OMS Change: Associations
            case ASSOCIATION:
                changeAssociations(orderDto, orderLineDto);
                break;
            // OMS Orders: Process Service Charges
            case CHARGE:
                processServiceCharges(orderDto, orderLineDto);
                break;
        }
    }

    /**
     * Cease order
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void ceaseOrder(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        switch (ItemTypeEnum.get(orderLineDto.getItemType())) {
            // OMS Cease Circuit
            case SUBSCRIPTION_MAIN_DETAILS:
                ceaseCircuit(orderDto, orderLineDto);
                break;
            case CHARGE:
                ceaseAncillary(orderDto, orderLineDto, orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode()));
                break;
        }
    }

    /**
     * Get the end point
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return the end point
     * @throws BusinessException the business exception
     */
    protected Map<String, Object> getEndPoint(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        Map<String, Object> endPoint = new HashMap<>();
        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        endPoint.put(EndPointCustomTableEnum.ITEM_ID.getLabel(), orderLineDto.getItemId());
        endPoint.put(EndPointCustomTableEnum.ITEM_COMPONENT_ID.getLabel(), orderLineDto.getItemComponentId());
        endPoint.put(EndPointCustomTableEnum.SUBSCRIPTION_ID.getLabel(), subscription.getId());
        endPoint.put(EndPointCustomTableEnum.EPID.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPID.getLabel()));
        String epname = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPNAME.getLabel());
        String eptype = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPTYPE.getLabel());
        if (StringUtils.isBlank(epname) && !EndPointTypeEnum.SE.name().equalsIgnoreCase(eptype) && !EndPointTypeEnum.LE.name().equalsIgnoreCase(eptype)) {
            BillingAccount billingAccount = orderService.findEntityByIdOrCode(billingAccountService, orderDto, orderLineDto.getBillingAccountCode());
            if (billingAccount != null) {
                if (!StringUtils.isBlank(billingAccount.getDescription())) {
                    epname = billingAccount.getDescription();
                } else if (billingAccount.getName() != null) {
                    epname = billingAccount.getName().getFirstName() + " " + billingAccount.getName().getLastName();
                }
            }
        }
        endPoint.put(EndPointCustomTableEnum.EPNAME.getLabel(), epname);
        endPoint.put(EndPointCustomTableEnum.EPTYPE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPTYPE.getLabel()));
        endPoint.put(EndPointCustomTableEnum.EPEND.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPEND.getLabel()));
        endPoint.put(EndPointCustomTableEnum.EPADDRESS.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPADDRESS.getLabel()));
        Long epversion = Utils.toLong(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPVERSION.getLabel()));
        endPoint.put(EndPointCustomTableEnum.EPVERSION.getLabel(), (epversion != null ? epversion : 1));
        endPoint.put(EndPointCustomTableEnum.EPSTATUS.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPSTATUS.getLabel()));
        endPoint.put(EndPointCustomTableEnum.EPSTART.getLabel(), DateUtils.parseDateWithPattern(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPSTART.getLabel()), Utils.DATE_TIME_PATTERN));
        endPoint.put(EndPointCustomTableEnum.EPSTATE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPSTATE.getLabel()));
        endPoint.put(EndPointCustomTableEnum.EPORDER.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPORDER.getLabel()));
        return endPoint;
    }

    /**
     * Get the association
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return the association
     * @throws BusinessException the business exception
     */
    protected Map<String, Object> getAssociation(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        Map<String, Object> association = new HashMap<>();
        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        association.put(AssociationCustomTableEnum.ITEM_ID.getLabel(), orderLineDto.getItemId());
        association.put(AssociationCustomTableEnum.ITEM_COMPONENT_ID.getLabel(), orderLineDto.getItemComponentId());
        association.put(AssociationCustomTableEnum.SUBSCRIPTION_ID.getLabel(), subscription.getId());
        association.put(AssociationCustomTableEnum.ASSOCCIRCID.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCCIRCID.getLabel()));
        association.put(AssociationCustomTableEnum.ASSOCTYPE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCTYPE.getLabel()));
        association.put(AssociationCustomTableEnum.ASSOCEND.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCEND.getLabel()));
        //association.put(AssociationCustomTableEnum.ASSOCTRIGGER.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCTRIGGER.getLabel()));
        Long assocversion = Utils.toLong(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCVERSION.getLabel()));
        association.put(AssociationCustomTableEnum.ASSOCVERSION.getLabel(), (assocversion != null ? assocversion : 1));
        association.put(AssociationCustomTableEnum.ASSOCSTATUS.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCSTATUS.getLabel()));
        association.put(AssociationCustomTableEnum.ASSOCSTART.getLabel(), DateUtils.parseDateWithPattern(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCSTART.getLabel()), Utils.DATE_TIME_PATTERN));
        association.put(AssociationCustomTableEnum.ASSOCSTATE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCSTATE.getLabel()));
        association.put(AssociationCustomTableEnum.ASSOCORDER.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCORDER.getLabel()));
        return association;
    }

    /**
     * Get association
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @param address      the address
     * @return the association
     * @throws BusinessException the business exception
     */
    private Map<String, Object> getAssociation(OrderDto orderDto, OrderLineDto orderLineDto, Map<String, Object> address) throws BusinessException {
        Map<String, Object> association = getAssociation(orderDto, orderLineDto);
        if (address != null && !address.isEmpty()) {
            association.putAll(address);
        }
        return association;
    }

    /**
     * Update association
     *
     * @param orderDto        the order Dto
     * @param orderLineDto    the order line Dto
     * @param subscription    the subscription
     * @param associationName the association name
     * @param address         the address
     * @throws BusinessException the business exception
     */
    private void updateAssociation(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription, String associationName, Map<String, Object> address)
            throws BusinessException {
        updateAssociation(orderDto, orderLineDto, subscription, null, associationName, address, null);
    }

    /**
     * Update association
     *
     * @param orderDto        the order Dto
     * @param orderLineDto    the order line Dto
     * @param subscription    the subscription
     * @param associationName the association name
     * @param endDate         the endDate
     * @throws BusinessException the business exception
     */
    private void updateAssociation(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription, String associationName, Date endDate) throws BusinessException {
        updateAssociation(orderDto, orderLineDto, subscription, null, associationName, null, endDate);
    }

    /**
     * Update association
     *
     * @param orderDto        the order Dto
     * @param orderLineDto    the order line Dto
     * @param subscription    the subscription
     * @param association     the association
     * @param associationName the association name
     * @param address         the address
     * @param endDate         the endDate
     * @throws BusinessException the business exception
     */
    private void updateAssociation(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription, Map<String, Object> association, String associationName,
                                   Map<String, Object> address, Date endDate) throws BusinessException {
        // Get the last valid version, and set end date (the Effective Date/Time on the bill item minus one second)
        Map<String, Object> lastAssociation = orderService.findLastVersionAssociation(subscription.getId(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCEND.getLabel()));
        int assocVersion = 0;
        if (lastAssociation != null && !lastAssociation.isEmpty()) {
            if (orderService.isOverlapAssociation(orderLineDto, subscription.getId(), lastAssociation)) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "A matching association period with " + orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCSTART.getLabel()) +
                        " was found. Please change the value of existing period instead");
                return;
            }
            // End Date should be set to the Effective Date/Time on the bill item minus one second
            //Date lastAssociationEndDate = Utils.addSecondsToDate(orderLineDto.getBillEffectDate(), -1);
            Date lastAssociationEndDate = DateUtils.parseDateWithPattern(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCSTART.getLabel()),
                    Utils.DATE_TIME_PATTERN);
            lastAssociation.put(AssociationCustomTableEnum.ASSOC_END_DATE.getLabel(), lastAssociationEndDate);
            orderService.updateAssociation(lastAssociation);
            assocVersion = ((BigInteger) lastAssociation.get(AssociationCustomTableEnum.ASSOCVERSION.getLabel())).intValue();
        }
        // Add the new version of association
        if (association == null || association.isEmpty()) {
            association = getAssociation(orderDto, orderLineDto, address);
        }
        if (!StringUtils.isBlank(associationName)) {
            association.put(AssociationCustomTableEnum.NAME.getLabel(), associationName);
        }
        if (endDate != null) {
            association.put(AssociationCustomTableEnum.ASSOC_END_DATE.getLabel(), endDate);
        }
        association.put(AssociationCustomTableEnum.ASSOCVERSION.getLabel(), assocVersion + 1);
        orderService.saveAssociation(association);
    }

    /**
     * Update association
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void updateAssociation(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());

        // If ASSOCTYPE = CH or BR, and ASSOCEND is A, B or C, then using the value in the ASSOCCIRCID tag,
        // look up the associated circuit to obtain the address for the 'Association'
        Map<Object, Object> associatedEndPoint = getAssociatedEndPoint(orderDto, orderLineDto);
        if (orderDto.getErrorCode() != null) {
            return;
        }

        Map<String, Object> endPoint = null;
        Map<String, Object> address = null;
        if (associatedEndPoint != null && !associatedEndPoint.isEmpty()) {
            Map.Entry<Object, Object> entry = associatedEndPoint.entrySet().iterator().next();
            endPoint = (Map<String, Object>) entry.getKey();
            address = (Map<String, Object>) entry.getValue();
        }

        String associationName = endPoint != null ? (String) endPoint.get(EndPointCustomTableEnum.EPNAME.getLabel()) : null;

        // Store the association
        updateAssociation(orderDto, orderLineDto, subscription, associationName, address);

        if (endPoint != null && !endPoint.isEmpty()) {
            endPoint.remove(EndPointCustomTableEnum.ID.getLabel());
            endPoint.put(EndPointCustomTableEnum.SUBSCRIPTION_ID.getLabel(), subscription.getId());
            Map<String, Object> lastEndPoint = orderService.findLastVersionEndPoint(subscription.getId(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCEND.getLabel()));
            if (lastEndPoint != null && !lastEndPoint.isEmpty()) {
                // Set the EPTYPE = AS
                endPoint.put(EndPointCustomTableEnum.EPTYPE.getLabel(), EndEnum.AS.name());
                lastEndPoint.putAll(endPoint);
                orderService.updateEndPoint(lastEndPoint);
            } else {
                endPoint.put(EndPointCustomTableEnum.EPVERSION.getLabel(), 1);
                orderService.saveEndPoint(endPoint);
            }
        }
    }

    /**
     * Cease all ancillary services of subscription
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @param subscription the subscription
     */
    private void ceaseAllAncillaryServices(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription) {
        if (orderDto.getErrorCode() != null) {
            return;
        }
        if (subscription != null && subscription.getServiceInstances() != null && !subscription.getServiceInstances().isEmpty()) {
            for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
                ServiceTemplate serviceTemplate = serviceInstance.getServiceTemplate();
                if (serviceInstance.getStatus() == InstanceStatusEnum.ACTIVE && serviceTemplate != null) {
                    String serviceType = (String) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.SERVICE_TYPE.name());
                    //if (!StringUtils.isBlank(serviceType) && ServiceTypeEnum.ANCILLARY.getLabel().equalsIgnoreCase(serviceType.trim())) {
                    if (!StringUtils.isBlank(serviceType) && !ServiceTypeEnum.MAIN.getLabel().equalsIgnoreCase(serviceType.trim())) {
                        ceaseAncillary(orderDto, orderLineDto, subscription, serviceInstance);
                    }
                }
            }
        }
    }

    /**
     * Cease circuit
     *
     * @param orderDto     the order line Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void ceaseCircuit(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        /** execute PO2-413 [OMS Cease Circuit] **/

        String state = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_STATE.getLabel());

        /** Step 6 : check to see if a REF_ORDER exists **/
        if (!StringUtils.isBlank(orderDto.getRefOrderCode())) {
            /**
             * Step 7 : using the value in the tag, search the completed orders in the system to determine if the (referenced) order has been successfully processed
             **/
            /** Step 8 : Was a complimentary order found? **/
            if (orderDto.getComplimentaryOrderDto() != null && !StringUtils.isBlank(orderDto.getComplimentaryOrderDto().getRefOrderCode())) {

                /** Step 10 : Release the complementary order for processing **/
                /*
                Order refOrder = orderService.findEntityByIdOrCode(orderService, orderDto, orderDto.getComplimentaryOrderDto().getRefOrderCode());
                refOrder.setStatus(OrderStatusEnum.PENDING);
                orderService.update(refOrder);
                release(refOrder.getCode());
                 */

                /** Step 10A : If the STATE (of the Complimentary Order) on the Main Subscription Details = CA, go to step 12 (i.e. do not execute step 11) **/
                String stateRefOrder = getMainItemAttribute(orderDto, OrderLineAttributeNamesEnum.OMS_STATE.getLabel());

                if (!StateEnum.CA.name().equalsIgnoreCase(stateRefOrder)) {
                    /** Step 11 : Retrieve Effect Date from REF_ORDER (P) order and set this as the Cease Effect Date **/
                    String effectDate = orderDto.getComplimentaryOrderDto().getMainItemElement(OrderLineAttributeNamesEnum.EFFECT_DATE.getLabel());
                    //orderLineDto.setCeaseEffectDate(DateUtils.truncateTime(DateUtils.parseDateWithPattern(effectDate, Utils.DATE_TIME_PATTERN)));
                    Date billEffectDate = DateUtils.parseDateWithPattern(effectDate, Utils.DATE_TIME_PATTERN);
                    for (OrderLineDto lineDto : orderDto.getOrderLines()) {
                        lineDto.setBillEffectDate(billEffectDate);
                    }
                }
            } else {
                /** Step 9 : set the status of the CE order to DEFERRED **/
                orderDto.setStatus(OrderStatusEnum.DEFERRED);
                return;
            }

        }
        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());

        /** Step 12 : Using the Service Id from the order, locate the subscriber **/
        /** Step 13 : Was the subscriber found? **/
        if (subscription == null) {
            /** Step 14 : If the subscriber was NOT found, send the order to Error Management **/
            setError(orderDto, ErrorEnum.INVALID_DATA, "The subscription " + orderLineDto.getSubscriptionCode() + " is not found");
            return;
        }

        if (subscription.getStatus() == SubscriptionStatusEnum.RESILIATED) {
            return;
        }

        /** Step 15 : If the subscriber was found, check if unprocessed Charge bill items exist **/
        /** Step 16 : If unprocessed Charge bill items exist, check if the STATUS of the Charge bill item = AC **/
        /** Step 17 : If STATUS =AC, execute PO2-474: [OMS Orders: Process Service Charges] **/
        /** Step 18 : If STATUS is NOT =AC, Obtain Item Component Id from the CE bill item **/
        // all the unprocessed Charge bill items are treated first before making the cease circuit

        /** Step 18.5 : Check if the STATE of the incoming Subscription Main Details bill item = CA **/
        /** Step 4 : Check if the STATE of the incoming Subscription Main Details bill item = CA **/
        /** Step 5 : If the STATE = CA, then execute PO2-484: [OMS Cancelled Orders] **/
        /** execute PO2-486 [OMS Cancelled Orders] == don't process the Main Subscription Details bill item **/
        /** Adding the test on Bill_Action == P to skip the cancellation in the provide case to cease the subscription **/
        if (StateEnum.CA.name().equalsIgnoreCase(state) && !BillActionEnum.P.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
            return;
        }

        /** Step 20 : Check if active Ancillary Services exist on the subscriber **/
        /** Step 21 : If an active service exists, obtain Item Component Id from the Ancillary Service **/
        /** Step 19 : Execute PO2-490: [OMS Cease Ancillary] **/
        // itemType = Subscription main details && action = C
        ceaseAllAncillaryServices(orderDto, orderLineDto, subscription);

        // Set here the RentalLiabilityDate to used also for associations and endpoints
        setRentalLiabilityDate(orderDto, orderLineDto, subscription);

        /** Step 22 : Update the Attribute Values of the End Points **/
        ceaseEndPoints(orderDto, orderLineDto, subscription);

        /** Step 23 : Update the Attribute Values of the Associations **/
        ceaseAssociations(orderDto, orderLineDto, subscription);

        /** Step 24 : Update the Main Subscription Details Attributes with the values from the bill item **/
        // subscriptionMainDetails(orderLineDto);

        /** Step 25 : Execute PO2-694: [OMS Cease Primary] **/
        ceasePrimary(orderDto, orderLineDto, subscription);

        /** Step 26 : Cease the Subscriber **/
        ceaseSubscription(orderDto, orderLineDto, subscription);

    }

    /**
     * Change endpoint
     *
     * @param orderLineDto the order line Dto
     * @param endPoint     the endpoint
     * @throws BusinessException the business exception
     */
    private void changeEndPoint(OrderLineDto orderLineDto, Map<String, Object> endPoint) throws BusinessException {
        endPoint.put(EndPointCustomTableEnum.EPSTATUS.getLabel(), ItemStatusEnum.CE.name());
        endPoint.put(EndPointCustomTableEnum.EP_END_DATE.getLabel(), orderLineDto.getRentalLiabilityDate());
        orderService.updateEndPoint(endPoint);
    }

    /**
     * Change association
     *
     * @param orderLineDto the order line Dto
     * @param association  the association
     * @throws BusinessException the business exception
     */
    private void changeAssociation(OrderLineDto orderLineDto, Map<String, Object> association) throws BusinessException {
        association.put(AssociationCustomTableEnum.ASSOCSTATUS.getLabel(), ItemStatusEnum.CE.name());
        association.put(AssociationCustomTableEnum.ASSOC_END_DATE.getLabel(), orderLineDto.getRentalLiabilityDate());
        orderService.updateAssociation(association);
    }


    /**
     * Cease change end points
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @param subscription the subscription
     * @throws BusinessException the business exception
     */
    private void ceaseEndPoints(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription) throws BusinessException {
        if (orderDto.getErrorCode() != null) {
            return;
        }

        Map<String, Object> endPointA = orderService.findLastVersionEndPoint(subscription.getId(), EndEnum.A.name());
        if (endPointA != null && !endPointA.isEmpty()) {
            changeEndPoint(orderLineDto, endPointA);
        }

        Map<String, Object> endPointB = orderService.findLastVersionEndPoint(subscription.getId(), EndEnum.B.name());
        if (endPointB != null && !endPointB.isEmpty()) {
            changeEndPoint(orderLineDto, endPointB);
        }
    }

    /**
     * Cease associations case
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @param subscription the subscription
     * @throws BusinessException the business exception
     */
    private void ceaseAssociations(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription) throws BusinessException {
        if (orderDto.getErrorCode() != null) {
            return;
        }

        Map<String, Object> associationA = orderService.findLastVersionAssociation(subscription.getId(), EndEnum.A.name());
        if (associationA != null && !associationA.isEmpty()) {
            changeAssociation(orderLineDto, associationA);
        }

        Map<String, Object> associationB = orderService.findLastVersionAssociation(subscription.getId(), EndEnum.B.name());
        if (associationB != null && !associationB.isEmpty()) {
            changeAssociation(orderLineDto, associationB);
        }
    }

    /**
     * Calculate cease charges
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @param subscription the subscription
     * @throws BusinessException the business exception
     */
    private void calculateCeaseCharges(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription) throws BusinessException {
        if (orderDto.getErrorCode() != null) {
            return;
        }
        ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
        String migrationCode = (String) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MIGRATION_CODE.name());
        if (StringUtils.isBlank(migrationCode)) {
            migrationCode = "*|*|=0";
        }

        Map<String, String> cf = (Map<String, String>) providerService.getProvider().getCfValue(ProviderCFsEnum.MIGRATION_MATRIX.name());
        if (cf == null) {
            // setError(orderDto, ErrorEnum.INVALID_DATA, "Migration Matrix is not found");
            return;
        }

        String serviceMigrationKey = getProductMigrationCode(cf, migrationCode, migrationCode, EventTypeEnum.CEASE);
        String ceaseData = cf.get(serviceMigrationKey);
        if (ceaseData == null) {
            // setError(orderDto, ErrorEnum.INVALID_DATA, "Migration Matrix lacks cease data");
            return;
        }

        Map<String, String> value = (Map<String, String>) utilService.getCFValue(providerService.getProvider(), ProviderCFsEnum.MIGRATION_MATRIX.name(), ceaseData);
        String serviceToApply = value.get(ServiceMigrationAttributeNamesEnum.BILL_CODE_TO_APPLY);

        applyServiceToApply(orderDto, orderLineDto, subscription, serviceToApply);

    }

    /**
     * Calculate rental charges
     *
     * @param orderDto        the order Dto
     * @param orderLineDto    the order line Dto
     * @param serviceInstance the service instance
     * @throws BusinessException the business exception
     */
    private void calculateRentalCharges(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance) throws BusinessException {
        if (orderDto.getErrorCode() != null) {
            return;
        }
        if (orderLineDto.getRentalLiabilityDate() != null && orderLineDto.getRentalLiabilityDate().after(new Date())) {
            for (RecurringChargeInstance recurringChargeInstance : serviceInstance.getRecurringChargeInstances()) {
                //recurringChargeInstanceService.applyRecurringCharge(recurringChargeInstance, orderLineDto.getRentalLiabilityDate(), true, false);
            }
        }
    }

    /**
     * Update service parameters
     *
     * @param orderDto         the order Dto
     * @param orderLineDto     the order line Dto
     * @param ancillaryService the ancillary service
     * @throws BusinessException the business exception
     */
    private void updateServiceParameters(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance ancillaryService) throws BusinessException {
        if (orderDto.getErrorCode() != null) {
            return;
        }

        CustomFieldValue lastCfValue = utilService.getLastCfVersion(ancillaryService, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(),
                DateUtils.truncateTime(orderLineDto.getBillEffectDate()), false);

        String serviceParametersKey = null;
        Map<String, String> serviceParametersValues = null;
        if (lastCfValue != null && lastCfValue.getMapValue() != null && !lastCfValue.getMapValue().isEmpty()) {
            serviceParametersKey = (String) lastCfValue.getMapValue().keySet().iterator().next();
            serviceParametersValues = (Map<String, String>) utilService.getCFValue(ancillaryService, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), (String) lastCfValue.getMapValue().values().iterator().next());

        }

        /** Step 10 : If a bill item (to cease the ancillary service) does NOT exist **/
        if (serviceParametersValues == null) {
            /**
             * Step 7 : Create a new version of the attributes group and populate the Charge Group Attributes with the values from the bill item. Use the effect date from step 6 as
             * the end date of the group version
             **/
            updateServiceParametersCF(orderDto, orderLineDto, ancillaryService);
            if (orderDto.getErrorCode() != null) {
                return;
            }
        } else {
            /**
             * Step 11 : If a bill item (to cease the ancillary service) does exist, create a new version of the Charge
             * Attribute Group using existing attribute values and set status = CE
             **/
            Map<String, Object> newServiceParametersValues = new HashMap<>(serviceParametersValues);
            newServiceParametersValues.put(ServiceParametersCFEnum.STATUS.getLabel(), ItemStatusEnum.CE.name());
            newServiceParametersValues.put(ServiceParametersCFEnum.RENTAL_LIABILITY_DATE.getLabel(), DateUtils.formatDateWithPattern(orderLineDto.getRentalLiabilityDate(), Utils.DATE_TIME_PATTERN));
            newServiceParametersValues.put(ServiceParametersCFEnum.RENTAL_SLA_PREMIUM_LIABILITY_DATE.getLabel(), DateUtils.formatDateWithPattern(orderLineDto.getSlaPremiumLiabilityDate(), Utils.DATE_TIME_PATTERN));

            Map<String, String> newServiceParametersCF = new HashMap<>();

            String itemId = null;
            String itemComponentId = null;
            if (ItemTypeEnum.SUBSCRIPTION_MAIN_DETAILS.getLabel().equalsIgnoreCase(orderLineDto.getItemType())) {
                // Cease Circuit case
                itemId = serviceParametersKey.split(Pattern.quote(CustomFieldValue.MATRIX_KEY_SEPARATOR))[0];
                itemComponentId = serviceParametersKey.split(Pattern.quote(CustomFieldValue.MATRIX_KEY_SEPARATOR))[1];
            } else {
                itemId = orderLineDto.getItemId();
                itemComponentId = orderLineDto.getItemComponentId();
            }

            newServiceParametersCF.put(itemId + CustomFieldValue.MATRIX_KEY_SEPARATOR + itemComponentId,
                    utilService.getCFValue(ancillaryService, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), newServiceParametersValues));
            updateServiceParametersCF(orderDto, orderLineDto, ancillaryService, newServiceParametersCF);
        }
        // serviceInstance.setEndAgreementDate(rentalLiabilityDate);
        //serviceInstanceService.update(ancillaryService);
    }

    /**
     * Cease Ancillary
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @param subscription the subscription
     * @throws BusinessException the business exception
     */
    private void ceaseAncillary(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription) throws BusinessException {
        if (subscription == null || subscription.getServiceInstances() == null || subscription.getServiceInstances().isEmpty()) {
            return;
        }
        ServiceInstance ancillaryService = getAncillaryServiceInstance(orderDto, orderLineDto, subscription);
        ceaseAncillary(orderDto, orderLineDto, subscription, ancillaryService);
    }

    /**
     * Cease Ancillary
     *
     * @param orderDto         the order Dto
     * @param orderLineDto     the order line Dto
     * @param subscription     the subscription
     * @param ancillaryService the ancillary service
     * @throws BusinessException the business exception
     */
    private void ceaseAncillary(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription, ServiceInstance ancillaryService) throws BusinessException {
        if (ancillaryService == null || ancillaryService.getStatus() == InstanceStatusEnum.TERMINATED) {
            return;
        }

        /** Step 6 : If the Main Subscription Details bill item status is NOT = CE, then use the Effect Date from the bill item as the Rental Liability Date **/
        /**
         * Step 8 : If the Main Subscription Details bill item status = CE, then use the Effect Date from the Main Subscription bill item on the CE order
         **/
        orderLineDto.setRentalLiabilityDate(DateUtils.truncateTime(orderLineDto.getBillEffectDate()));
        String status = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_STATUS.getLabel());
        if (ItemTypeEnum.SUBSCRIPTION_MAIN_DETAILS.getLabel().equalsIgnoreCase(orderLineDto.getItemType()) &&
                ItemStatusEnum.CE.name().equalsIgnoreCase(status)) {
            /** Step 9 : Execute PO2-312: [OMS Determine Rental Liability Date] **/
            setRentalLiabilityDate(orderDto, orderLineDto, subscription);
        }
        //ancillaryService.setEndAgreementDate(orderLineDto.getRentalLiabilityDate());
        ancillaryService.setCfValue(ServiceInstanceCFsEnum.RENTAL_LIABILITY_DATE.name(), orderLineDto.getRentalLiabilityDate());

        orderLineDto.setCeaseEffectDate(getCeaseEffectDate(orderDto, orderLineDto, ancillaryService));

        /** Step 5 : Check if the Main Subscription Details bill item status = CE **/
        /**
         * Step 7 : Create a new version of the attributes group and populate the Charge Group Attributes with the values from the bill item. Use the effect date from step 6 as the
         * end date of the group version
         **/
        /** Step 10 : If a bill item (to cease the ancillary service) does NOT exist **/
        /**
         * Step 11 : If a bill item (to cease the ancillary service) does exist, create a new version of the Charge Attribute Group using existing attribute values and set status =
         * CE
         **/
        updateServiceParameters(orderDto, orderLineDto, ancillaryService);

        /** Step 12 : Cease the service instance and use the effective date (from either step 6 or step 8) as the cease date **/
        terminateAncillaryService(orderDto, orderLineDto, ancillaryService);

        /** Step 13 : Calculate Rental Charges to up until the Rental Liability Date **/
        // calculateRentalCharges(orderLineDto, ancillaryService);

        /** Step 14 : Calculate Cease Charges **/
        // calculateCeaseCharges(orderDto, orderLineDto, subscription);

        /** Step 15 : Rebate any charges that were paid In-Advance. Charges must be off-set against other charges outstanding for the subscriber services **/

    }

    /**
     * Cease Primary
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @param subscription the subscription
     * @throws BusinessException the business exception
     */
    private void ceasePrimary(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription) throws BusinessException {
        if (orderDto.getErrorCode() != null) {
            return;
        }

        /** Step 1 : Obtain the Effect Date from the Main Subscription bill item on the CE order **/
        // orderLineDto.billEffectDate

        /** Step 2 : Execute PO2-312: [OMS Determine Rental Liability Date] **/
        //orderLineDto.setRentalLiabilityDate(getRentalLiabilityDate(orderDto, orderLineDto));

        ServiceInstance mainServiceInstance = getMainServiceInstance(subscription);
        if (mainServiceInstance == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The main service is not found on subscription " + subscription.getCode());
            return;
        }

        //mainServiceInstance.setEndAgreementDate(orderLineDto.getRentalLiabilityDate());
        mainServiceInstance.setCfValue(ServiceInstanceCFsEnum.RENTAL_LIABILITY_DATE.name(), orderLineDto.getRentalLiabilityDate());

        orderLineDto.setCeaseEffectDate(getCeaseEffectDate(orderDto, orderLineDto, mainServiceInstance));

        /** Step 3 : Create a new version Main Subscription Details Group **/
        //createNewVersionServiceParameters(orderDto, orderLineDto, mainServiceInstance);

        /**
         * Step 3bis : If a bill item (to cease the main service) does exist, create a new version of the Charge Attribute Group using existing attribute values and set status =
         * CE
         **/
        updateServiceParameters(orderDto, orderLineDto, mainServiceInstance);

        /** Step 4 : Cease the Primary Service instance **/
        terminatePrimaryService(orderDto, orderLineDto, mainServiceInstance);

        /** Step 5 : Calculate Rental Charges to up until the Rental Liability Date **/
        // calculateRentalCharges(orderLineDto, mainServiceInstance);

        /** Step 6 : Calculate Cease Charges **/
        // calculateCeaseCharges(orderDto, orderLineDto, subscription);

        /** Step 7 : Rebate any charges that were paid In-Advance. Charges must be off-set against other charges outstanding for the subscriber services **/

        /** Step 8 : Calculate SLA Charges to up until the SLA Premium Liability Date **/
    }

    /**
     * Create subscription
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return the subscription
     * @throws BusinessException the business exception
     */
    protected Subscription createSubscription(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        UserAccount userAccount = orderService.findEntityByIdOrCode(userAccountService, orderDto, orderLineDto.getSubscriptionCode());
        // if user account doesn't exist, create new one with code = Service_Id
        if (userAccount == null) {
            userAccount = new UserAccount();
            userAccount.setCode(orderLineDto.getSubscriptionCode());
            userAccountService.createUserAccount(orderService.findEntityByIdOrCode(billingAccountService, orderDto, orderLineDto.getBillingAccountCode()), userAccount);
        }

        Subscription subscription = new Subscription();
        subscription.setCode(orderLineDto.getSubscriptionCode());
        subscription.setSeller(orderService.findEntityByIdOrCode(sellerService, orderDto, ApplicationPropertiesEnum.OPENEIR_SELLER_CODE.getProperty()));
        subscription.setUserAccount(userAccount);
        OfferTemplate offerTemplate = orderService.findEntityByIdOrCode(offerTemplateService, orderDto, orderLineDto.getOfferTemplateCode());
        subscription.setOffer(offerTemplate);
        subscription.setSubscriptionDate(orderLineDto.getBillEffectDate());
        subscription.setStatusDate(orderLineDto.getBillEffectDate());

        /** 2 : execute PO2-491 [OMS Min Term: Determine End of Engagement Date] **/
        Date endEngagementDate = getEndEngagementDate(orderDto, orderLineDto);
        subscription.setEndAgreementDate(endEngagementDate);

        updateSubscriptionCfs(orderLineDto, subscription);
        subscriptionService.create(subscription);
        createAccessPoint(orderLineDto, subscription);
        return subscription;
    }

    /**
     * cease subscription
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line dto
     * @param subscription the subscription
     * @throws BusinessException the business exception
     */
    private void ceaseSubscription(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription) throws BusinessException {
        if (orderDto.getErrorCode() != null) {
            return;
        }
        if (subscription.getStatus() == SubscriptionStatusEnum.RESILIATED) {
            return;
        }
        SubscriptionTerminationReason terminationReason = orderService.findEntityByIdOrCode(subscriptionTerminationReasonService, orderDto,
                TerminationReasonEnum.TR_NO_OVERRIDE_AGR_RREFUND.name());
        terminateSubscription(orderDto, orderLineDto, subscription, terminationReason, orderLineDto.getBillEffectDate());
    }

    /**
     * Subscription main details
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void subscriptionMainDetails(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        if (orderDto.getErrorCode() != null) {
            return;
        }
        if (!orderLineDto.isMain()) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The provided bill code (" + orderLineDto.getBillCode() + ") is not a main service");
            return;
        }

        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());

        // If an existing subscriber is found
        if (subscription != null) {
            /** Validate data for change subscription main details **/
            String state = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_STATE.getLabel());

            // If the bill code on the (Main Subscription Details) bill item is different to the bill code on the subscriber,
            // then send the order to Error Management

            ServiceInstance serviceInstance = getMainServiceInstance(subscription);
            String billCodeServiceMain = serviceInstance != null ? serviceInstance.getCode() : null;

            if (StringUtils.isBlank(billCodeServiceMain)) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "The main service doesn't found on the subscriber");
                return;
            }
            if (!orderLineDto.getBillCode().equals(billCodeServiceMain)) {
                setError(orderDto, ErrorEnum.INVALID_DATA,
                        "The provided bill code (" + orderLineDto.getBillCode() + ") is different to the bill code (" + billCodeServiceMain + ") on the subscriber");
                return;
            }

            // if STATUS is CE, or STATE is CA, End date can be set to the Bill Item Effect_Date
            String status = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_STATUS.getLabel());
            Date endDate = (ItemStatusEnum.CE.name().equalsIgnoreCase(status) || StateEnum.CA.name().equalsIgnoreCase(state)) ? orderLineDto.getBillEffectDate() : null;

            /** if the STATE of the incoming Subscription Main Details = CA **/
            /** execute PO2-486 [OMS Cancelled Orders] == don't process the Main Subscription Details bill item **/
            if (StateEnum.CA.name().equalsIgnoreCase(state)) {
                return;
            }

            /** if the STATE of the incoming Subscription Main Details is not CA **/
            /** execute PO2-412 [OMS Change: Subscription Main Details] **/

            CustomFieldValue lastCfValue = utilService.getLastCfVersion(serviceInstance, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(),
                    DateUtils.truncateTime(orderLineDto.getBillEffectDate()), false);

            /** Store service parameters CF **/
            updateServiceParametersCF(orderDto, orderLineDto, serviceInstance, null, endDate, lastCfValue, null);
            serviceInstance.setQuantity(new BigDecimal(orderLineDto.getBillQuantity()));
            serviceInstance = serviceInstanceService.update(serviceInstance);

            // Calculate a delta quantity
            Map<String, String> serviceParametersCF = getServiceParameters(serviceInstance, lastCfValue);
            if (serviceParametersCF != null) {
                setDeltaQuantity(orderLineDto, new BigDecimal(serviceParametersCF.get(ServiceParametersCFEnum.QUANTITY.getLabel())));
            }

            //reratingService.rerate(serviceInstance, orderLineDto.getBillEffectDate());
            orderDto.getServiceInstances().put(serviceInstance.getId(), orderLineDto);

            // Apply SLA charge to existing ancillary recurring charges
            applySlaSurcharge(orderDto, orderLineDto, serviceInstance, lastCfValue);
        } else {

            /** Validate data for provide subscription main details **/
            // PO2-528 : Validation on Provide order to ensure service_id not in use on DDI/MSN Ancillary Service
            // FIXME : isValidRanges loop all eir subscriptions ==> performance problem
            /*
             * if (!isValidRanges(orderLineDto.getSubscriptionCode(), null)) { setError(orderDto, ErrorEnum.INVALID_DATA, "ServiceId " + orderLineDto.getSubscriptionCode() +
             * " already in use on DDI/MSN Ancillary Service"); return; }
             * if (orderDto.getErrorCode() != null) {
             *   return;
             *   }
             */

            /** 1 : execute PO2-128 [OMS Provide: Subscription Main Details] **/
            subscription = createSubscription(orderDto, orderLineDto);

            ServiceInstance serviceInstance = activateService(orderDto, orderLineDto, subscription, true);
            if (serviceInstance != null) {
                subscriptionService.update(subscription);

                /**
                 * [OMS Cancelled Orders] As openeir when a Subscriber does not exist and one was created for a Cancelled Order,
                 * I want to terminate the Subscription when the order has been successfully processed and the cancellation charge successfully applied
                 */
                String state = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_STATE.getLabel());
                if (StateEnum.CA.name().equalsIgnoreCase(state) && BillActionEnum.P.name().equals(orderLineDto.getBillAction())) {
                    orderDto.getServiceInstances().put(serviceInstance.getId(), orderLineDto);
                }
            }
        }
    }

    /**
     * Get OMS Min Term for a customer & migration code.
     *
     * @param customerCode  the customer code
     * @param migrationCode the migration code
     * @param effectDate    Effect Date
     * @return Min Term configuartion
     */
    private Map<String, Object> getMinTerm(String customerCode, String migrationCode, Date effectDate) {
        Map<String, Object> minTerm = getOMSMinTerm(customerCode, migrationCode, effectDate);
        if (minTerm != null && !minTerm.isEmpty()) {
            return minTerm;
        }
        if (!StringUtils.isBlank(migrationCode)) {
            String[] keys = migrationCode.split(Pattern.quote("_"));
            if (keys.length > 1) {
                minTerm = getOMSMinTerm(customerCode, keys[0] + "_" + keys[1] + "_*", effectDate);
                if (minTerm != null && !minTerm.isEmpty()) {
                    return minTerm;
                }
                minTerm = getOMSMinTerm(customerCode, keys[0] + "_*_" + keys[2], effectDate);
                if (minTerm != null && !minTerm.isEmpty()) {
                    return minTerm;
                }
                minTerm = getOMSMinTerm(customerCode, "*_" + keys[1] + "_" + keys[2], effectDate);
                if (minTerm != null && !minTerm.isEmpty()) {
                    return minTerm;
                }
                minTerm = getOMSMinTerm(customerCode, keys[0] + "_*_*", effectDate);
                if (minTerm != null && !minTerm.isEmpty()) {
                    return minTerm;
                }
                minTerm = getOMSMinTerm(customerCode, "*_" + keys[1] + "_*", effectDate);
                if (minTerm != null && !minTerm.isEmpty()) {
                    return minTerm;
                }
                minTerm = getOMSMinTerm(customerCode, "*_*_" + keys[2], effectDate);
                if (minTerm != null && !minTerm.isEmpty()) {
                    return minTerm;
                }
            }
            if (keys.length > 0) {
                if (keys.length == 1) {
                    minTerm = getOMSMinTerm(customerCode, keys[0] + "_*_*", effectDate);
                    if (minTerm != null && !minTerm.isEmpty()) {
                        return minTerm;
                    }
                }
                minTerm = getOMSMinTerm(customerCode, keys[0] + "_*", effectDate);
                if (minTerm != null && !minTerm.isEmpty()) {
                    return minTerm;
                }
            }

            minTerm = getOMSMinTerm(customerCode, "*_*_*", effectDate);
            if (minTerm != null && !minTerm.isEmpty()) {
                return minTerm;
            }
        }
        minTerm = getOMSMinTerm(customerCode, "*", effectDate);
        if (minTerm != null && !minTerm.isEmpty()) {
            return minTerm;
        }
        minTerm = getOMSMinTerm("*", migrationCode, effectDate);
        if (minTerm != null && !minTerm.isEmpty()) {
            return minTerm;
        }
        return minTerm;
    }

    /**
     * Get OMS Min Term for a customer & migration code.
     *
     * @param customerCode  the customer code
     * @param migrationCode the migration code
     * @param effectDate    Effect Date
     * @return Min Term configuartion
     */
    private Map<String, Object> getOMSMinTerm(String customerCode, String migrationCode, Date effectDate) {
        Map<String, String> cf = (Map<String, String>) providerService.getProvider().getCfValue(ProviderCFsEnum.OMS_MIN_TERM.name());
        if (cf == null) {
            return null;
        }
        for (Map.Entry<String, String> entry : cf.entrySet()) {

            String[] keys = entry.getKey().split(Pattern.quote(CustomFieldValue.MATRIX_KEY_SEPARATOR));
            if (keys.length != 4) {
                continue;
            }
            Date startDate = !StringUtils.isBlank(keys[MinTermAttributeNamesEnum.START_DATE.ordinal()])
                    ? DateUtils.parseDateWithPattern(keys[MinTermAttributeNamesEnum.START_DATE.ordinal()], Utils.DATE_DEFAULT_PATTERN)
                    : null;
            Date endDate = !StringUtils.isBlank(keys[MinTermAttributeNamesEnum.END_DATE.ordinal()])
                    ? DateUtils.parseDateWithPattern(keys[MinTermAttributeNamesEnum.END_DATE.ordinal()], Utils.DATE_DEFAULT_PATTERN)
                    : null;
            String oaoId = keys[MinTermAttributeNamesEnum.OAO.ordinal()];
            String productCode = keys[MinTermAttributeNamesEnum.PRODUCT_CODE.ordinal()];

            if (isSameAttributeValue(customerCode, oaoId) && isSameAttributeValue(migrationCode, productCode)) {
                if (startDate == null) {
                    continue;
                } else if (endDate == null) {
                    if (startDate.before(effectDate)) {
                        return (Map<String, Object>) utilService.getCFValue(providerService.getProvider(), ProviderCFsEnum.OMS_MIN_TERM.name(), entry.getValue());
                    }
                } else {
                    if (startDate.before(effectDate) && endDate.after(effectDate)) {
                        return (Map<String, Object>) utilService.getCFValue(providerService.getProvider(), ProviderCFsEnum.OMS_MIN_TERM.name(), entry.getValue());
                    }
                }

            }

        }
        return null;
    }

    /**
     * Get OMS Min Term for a customer & migration code.
     *
     * @param customerCode  the customer code
     * @param migrationCode the migration code
     * @param effectDate    Effect Date
     * @return Min Term configuartion
     */
    private Map<String, Object> getMinTermVersionable(String customerCode, String migrationCode, Date effectDate) {

        String keyToMatch = customerCode + CustomFieldValue.MATRIX_KEY_SEPARATOR + migrationCode;
        Map<String, Object> cf = (Map<String, Object>) customFieldInstanceService.getCFValueByClosestMatch(providerService.getProvider(), ProviderCFsEnum.OMS_MIN_TERM.name(), effectDate, keyToMatch);
        if (cf == null || cf.isEmpty()) {
            keyToMatch = CustomFieldValue.WILDCARD_MATCH_ALL + CustomFieldValue.MATRIX_KEY_SEPARATOR + migrationCode;
            cf = (Map<String, Object>) customFieldInstanceService.getCFValueByClosestMatch(providerService.getProvider(), ProviderCFsEnum.OMS_MIN_TERM.name(), effectDate, keyToMatch);
        }
        if (cf == null || cf.isEmpty()) {
            keyToMatch = customerCode + CustomFieldValue.MATRIX_KEY_SEPARATOR + CustomFieldValue.WILDCARD_MATCH_ALL;
            cf = (Map<String, Object>) customFieldInstanceService.getCFValueByClosestMatch(providerService.getProvider(), ProviderCFsEnum.OMS_MIN_TERM.name(), effectDate, keyToMatch);
        }

        if (cf == null || cf.isEmpty()) {
            return null;
        }
        return cf;
    }

    /**
     * Get minimum term period
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return minimum term period
     */
    private Integer getMinimumTermPeriod(OrderDto orderDto, OrderLineDto orderLineDto) {
        ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
        String migrationCode = (String) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MIGRATION_CODE.name());
        Integer minimumTermPeriod = null;
        if (!StringUtils.isBlank(migrationCode)) {
            // This Migration Code (along with the OLO Id) is then used to find matching entries in the OMS Min Term configuration
            Map<String, Object> minTerm = getMinTerm(orderLineDto.getOperatorId(), migrationCode, orderLineDto.getBillEffectDate());
            if (minTerm != null) {
                minimumTermPeriod = minTerm.get(MinTermAttributeNamesEnum.MINIMUM_TERM_PERIOD.name()) != null
                        ? ((Long) minTerm.get(MinTermAttributeNamesEnum.MINIMUM_TERM_PERIOD.name())).intValue()
                        : null;
            }
            // If a match is found (in the OMS Min Term configuration), add the 'Min Term Period' (months) from the record found to
            // the Order Effective Date and populate this as the End of Engagement Date against the subscriber
        }
        return minimumTermPeriod;
    }

    /**
     * Get notice cease period
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return notice cease period
     */
    private Integer getNoticeCeasePeriod(OrderDto orderDto, OrderLineDto orderLineDto) {
        ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
        String migrationCode = (String) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MIGRATION_CODE.name());
        Integer noticeCeasePeriod = null;
        if (!StringUtils.isBlank(migrationCode)) {
            // This Migration Code (along with the OLO Id) is then used to find matching entries in the OMS Min Term configuration
            Map<String, Object> minTerm = getMinTerm(orderLineDto.getOperatorId(), migrationCode, orderLineDto.getBillEffectDate());
            if (minTerm != null) {
                noticeCeasePeriod = minTerm.get(MinTermAttributeNamesEnum.NOTICE_CEASE_PERIOD.name()) != null
                        ? ((Long) minTerm.get(MinTermAttributeNamesEnum.NOTICE_CEASE_PERIOD.name())).intValue()
                        : null;
            }
            // If a match is found (in the OMS Min Term configuration), add the 'Min Term Period' (months) from the record found to
            // the Order Effective Date and populate this as the End of Engagement Date against the subscriber
        }
        return noticeCeasePeriod;
    }

    /**
     * Get the end of engagement date
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return the end of engagement date
     */
    private Date getEndEngagementDate(OrderDto orderDto, OrderLineDto orderLineDto) {
        /** execute PO2-491 [OMS Min Term: Determine End of Engagement Date] **/

        /** Step 7 : populate the Order Effect Date as the End of Engagement Date against the subscriber **/
        Date endEngagementDate = orderLineDto.getBillEffectDate();

        String commitmentPeriod = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_COMMITMENT_PERIOD.getLabel());
        String commitmentPeriodUnit = orderLineDto.getUnitAttribute(OrderLineAttributeNamesEnum.OMS_COMMITMENT_PERIOD.getLabel());
        String commitmentTreatment = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_MAIN_COMMITMENT_TREATMENT.getLabel());

        /** Step 2 : Check if a value exists in the COMMITMENT TERM tag in the order submitted from OMS **/
        if (!StringUtils.isBlank(commitmentPeriod)) {

            /**
             * Step 3 : add the COMMITMENT TERM tag value (i.e. number of days) to the Order Effect Date and populate this in the End of Engagement Date against the subscriber
             **/
            if (Utils.isInteger(commitmentPeriod)) {
                if (PeriodUnitEnum.Days.name().equalsIgnoreCase(commitmentPeriodUnit)) {
                    endEngagementDate = DateUtils.addDaysToDate(endEngagementDate, Integer.valueOf(commitmentPeriod));
                } else if (PeriodUnitEnum.Months.name().equalsIgnoreCase(commitmentPeriodUnit)) {
                    endEngagementDate = DateUtils.addMonthsToDate(endEngagementDate, Integer.valueOf(commitmentPeriod));
                } else if (PeriodUnitEnum.Years.name().equalsIgnoreCase(commitmentPeriodUnit)) {
                    endEngagementDate = DateUtils.addYearsToDate(endEngagementDate, Integer.valueOf(commitmentPeriod));
                }
            }
        } else {
            /** Step 4 : check if a complementary order exists (i.e. REF_ORDER_ID) **/
            if (StringUtils.isBlank(orderDto.getRefOrderCode())) {

                /** Step 5 : look up the OMS Min Term table **/
                Integer minTermPeriod = getMinimumTermPeriod(orderDto, orderLineDto);

                if (minTermPeriod != null) {
                    /**
                     * Step 6 : add the 'Min Term Period' (months) from the record found to the Order Effective Date and populate this as the End of Engagement Date against the
                     * subscriber
                     **/
                    endEngagementDate = DateUtils.addMonthsToDate(endEngagementDate, minTermPeriod);
                }
            } else {
                /** Step 8 : If a complementary order exists (i.e. REF_ORDER_ID), check if a value exists in the COMMITMENT TREATMENT tag. **/
                if (StringUtils.isBlank(commitmentTreatment)) {

                    /** Step 5 : look up the OMS Min Term table **/
                    Integer minTermPeriod = getMinimumTermPeriod(orderDto, orderLineDto);

                    if (minTermPeriod != null) {
                        /**
                         * Step 6 : add the 'Min Term Period' (months) from the record found to the Order Effective Date and populate this as the End of Engagement Date against the
                         * subscriber
                         **/
                        endEngagementDate = DateUtils.addMonthsToDate(endEngagementDate, minTermPeriod);
                    }
                } else {
                    /** Step 9 : If the value in COMMITMENT TREATMENT is 'Continue' **/
                    if (!"Continue".equalsIgnoreCase(commitmentTreatment)) {

                        /** Step 5 : look up the OMS Min Term table **/
                        Integer minTermPeriod = getMinimumTermPeriod(orderDto, orderLineDto);

                        if (minTermPeriod != null) {
                            /**
                             * Step 6 : add the 'Min Term Period' (months) from the record found to the Order Effective Date and populate this as the End of Engagement Date against
                             * the subscriber
                             **/
                            endEngagementDate = DateUtils.addMonthsToDate(endEngagementDate, minTermPeriod);
                        }
                    } else {
                        /**
                         * Step 10 : Using the REF_ORDER_ID, locate the complementary order, retrieve the Service Id and use that to retrieve the ‘End of Engagement Date’
                         * associated with the Subscriber (found using that service id)
                         **/
                        if (orderDto.getComplimentaryOrderDto() != null && !StringUtils.isBlank(orderDto.getComplimentaryOrderDto().getRefOrderCode())) {

                            String associatedSubscriptionCode = orderDto.getComplimentaryOrderDto().getMainItemElement(OrderLineAttributeNamesEnum.SERVICE_ID.getLabel());
                            Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, associatedSubscriptionCode);
                            if (subscription != null) {

                                /** Step 11 : Check if the 'End of Engagement Date' retrieved represents a date in the past. **/
                                if (subscription.getEndAgreementDate() != null && subscription.getEndAgreementDate().compareTo(new Date()) > 0) {

                                    /**
                                     * Step 12 : Populate the End of Engagement Date (retrieved from the subscriber circuit / via the REF_ORDER_ID on the complimentary order) as
                                     * the End of Engagement Date against the subscriber
                                     **/
                                    endEngagementDate = subscription.getEndAgreementDate();
                                }
                            }
                        }
                    }
                }
            }
        }
        return endEngagementDate;
    }

    /**
     * Get the notice to cease (NC) date
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return the notice to cease (NC) date
     */
    private Date getNoticeToCeaseDate(OrderDto orderDto, OrderLineDto orderLineDto) {

        /** Step 9 : use the Order Effect Date as the Notice to Cease Date **/
        Date noticeToCeaseDate = orderLineDto.getBillEffectDate();

        /** Step 1 : look up the OMS Min Term table **/
        Integer noticeCeasePeriod = getNoticeCeasePeriod(orderDto, orderLineDto);
        /** Step 2 : If a match is found (in the OMS Min Term configuration), retrieve the 'Notice to Cease Period' **/
        if (noticeCeasePeriod != null) {
            /** Step 3 : Check if a value exists in the NOTBEFOREDATE tag in the order submitted from OMS **/
            String notBeforeDateAttribute = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_MAIN_NOT_BEFORE_DATE.getLabel());
            if (StringUtils.isBlank(notBeforeDateAttribute)) {
                /**
                 * Step 4 : If a value does NOT exist in NOTBEFOREDATE (or if the tag is not present on the order), add the 'Notice to Cease Period' (days) retrieved to the Order
                 * Effect Date and use the date derived as the Notice to Cease Date
                 **/
                noticeToCeaseDate = DateUtils.addDaysToDate(noticeToCeaseDate, noticeCeasePeriod);
            } else {
                /** Step 5 : If a value does exist in the NOTBEFOREDATE tag, subtract Effect Date from NOTBEFOREDATE **/
                Date notBeforeDate = DateUtils.parseDateWithPattern(notBeforeDateAttribute, Utils.DATE_TIME_PATTERN);
                if (notBeforeDate != null) {
                    double days = DateUtils.daysBetween(noticeToCeaseDate, notBeforeDate);
                    /** Step 6 b) : Check if the result (i.e. the difference) is greater or equal to the 'Notice to Cease Period' **/
                    if (days >= noticeCeasePeriod) {
                        noticeToCeaseDate = notBeforeDate;
                    } else {
                        /** Step 6 b) : Check if the result (i.e. the difference) is less to the 'Notice to Cease Period' **/
                        noticeToCeaseDate = DateUtils.addDaysToDate(orderLineDto.getBillEffectDate(), noticeCeasePeriod);
                    }
                }
            }

        } else {
            /**
             * Step 8 : If a match is NOT found (in the OMS Min Term configuration), check if a value exists in the NOTBEFOREDATE tag in the order submitted from OMS
             **/
            String notBeforeDateAttribute = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_MAIN_NOT_BEFORE_DATE.getLabel());
            if (!StringUtils.isBlank(notBeforeDateAttribute)) {
                /** Step 7 : If a value does exist in the NOTBEFOREDATE tag, use this Not Before Date as the Notice to Cease Date **/
                Date notBeforeDate = DateUtils.parseDateWithPattern(notBeforeDateAttribute, Utils.DATE_TIME_PATTERN);
                if (notBeforeDate != null) {
                    noticeToCeaseDate = notBeforeDate;
                }
            }
        }
        return noticeToCeaseDate;
    }

    /**
     * Get the rental liability date
     *
     * @param noticeToCeaseDate the notice to cease (NC) date
     * @param subscription      the subscription
     * @return the rental liability date
     */
    private Date getRentalLiabilityCeaseDate(Date noticeToCeaseDate, Subscription subscription) {
        /** Step 6 : Use the Notice to Cease Date (retrieved in step 3) as the Rental Liability Date **/
        Date rentalLiabilityDate = noticeToCeaseDate;
        if (subscription != null && subscription.getEndAgreementDate() != null) {
            /** Step 7 : Check if the subscribers 'End of Engagement Date' is greater than (i.e. occurs before) the 'Notice to Cease Date' (retrieved in step 3) **/
            if (noticeToCeaseDate == null || subscription.getEndAgreementDate().compareTo(noticeToCeaseDate) > 0) {
                /** Step 8 : Use the End of Engagement Date as the Rental Liability Date **/
                rentalLiabilityDate = subscription.getEndAgreementDate();
            }
        }
        return rentalLiabilityDate;
    }

    /**
     * Get complimentary order Dto
     *
     * @param refOrder the ref order
     * @return the complimentary order Dto
     */
    private ComplimentaryOrderDto getComplimentaryOrderDto(Order refOrder) {
        ComplimentaryOrderDto refOrderDto = null;
        if (refOrder != null) {
            refOrderDto = new ComplimentaryOrderDto();
            refOrderDto.setRefOrderCode(refOrder.getCode());
            refOrderDto.setMainItem(getMainItemRefOrder(refOrder));
        }
        return refOrderDto;
    }

    /**
     * Get subscription main details ref order
     *
     * @return subscription main details ref order
     */
    private Map<String, String> getMainItemRefOrder(Order refOrder) {
        if (refOrder != null) {
            Map<String, String> orderItemsCF = (Map<String, String>) refOrder.getCfValue(OrderCFsEnum.ORDER_ITEMS.name());
            if (orderItemsCF != null && !orderItemsCF.isEmpty()) {
                for (Map.Entry<String, String> entry : orderItemsCF.entrySet()) {
                    Map<String, String> value = (Map<String, String>) utilService.getCFValue(refOrder, OrderCFsEnum.ORDER_ITEMS.name(), entry.getValue());
                    if (value != null && !value.isEmpty()) {
                        String itemType = value.get(OrderLineAttributeNamesEnum.ITEM_TYPE.getLabel());
                        if (!StringUtils.isBlank(itemType) && ItemTypeEnum.SUBSCRIPTION_MAIN_DETAILS.getLabel().equalsIgnoreCase(itemType)) {
                            value.put(OrderLineAttributeNamesEnum.ITEM_ID.getLabel(), entry.getKey());
                            return value;
                        }
                    }
                }
            }
        }
        return null;
    }

    /**
     * Get subscription main details attribute ref order
     *
     * @param orderDto      the order Dto
     * @param attributeName the attribute name
     * @return subscription main details attribute ref order
     */
    private String getMainItemAttribute(OrderDto orderDto, String attributeName) {
        if (orderDto.getComplimentaryOrderDto() != null && !StringUtils.isBlank(orderDto.getComplimentaryOrderDto().getRefOrderCode()) &&
                orderDto.getComplimentaryOrderDto().getMainItem() != null && !orderDto.getComplimentaryOrderDto().getMainItem().isEmpty()) {
            String itemId = orderDto.getComplimentaryOrderDto().getMainItem().get(OrderLineAttributeNamesEnum.ITEM_ID.getLabel());
            if (!StringUtils.isBlank(itemId)) {
                Order refOrder = orderService.findEntityByIdOrCode(orderService, orderDto, orderDto.getComplimentaryOrderDto().getRefOrderCode());
                String keyToMatch = itemId + CustomFieldValue.MATRIX_KEY_SEPARATOR + attributeName;
                Map<String, String> attribute = (Map<String, String>) customFieldInstanceService.getCFValueByClosestMatch(refOrder, "ORDER_ITEM_ATTRIBUTES", keyToMatch);
                if (attribute != null && !attribute.isEmpty()) {
                    return attribute.get("Value");
                }
            }
        }
        return null;
    }

    /**
     * Get the rental liability date
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     */
    private void setRentalLiabilityDate(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription) {

        Date rentalLiabilityDate = orderLineDto.getBillEffectDate();
        Date slaPremiumLiabilityDate = orderLineDto.getBillEffectDate();
        Date noticeToCeaseDate = null;
        String commitmentTreatment = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_MAIN_COMMITMENT_TREATMENT.getLabel());

        /** Step 1 : Check if a complementary order exists (i.e. REF_ORDER_ID) **/
        if (orderDto.getComplimentaryOrderDto() != null && !StringUtils.isBlank(orderDto.getComplimentaryOrderDto().getRefOrderCode())) {

            /** Step 2 : retrieve the Order Effect Date from the Complementary order and use this as the Rental Liability Date **/
            String effectDate = orderDto.getComplimentaryOrderDto().getMainItemElement(OrderLineAttributeNamesEnum.EFFECT_DATE.getLabel());
            rentalLiabilityDate = DateUtils.parseDateWithPattern(effectDate, Utils.DATE_TIME_PATTERN);
            slaPremiumLiabilityDate = rentalLiabilityDate;
        } else {

            /** Step 3 : Determine Notice to Cease (NC) Date **/

            /** execute PO2-670 [OMS Determine Notice to Cease (NC) Date] **/
            noticeToCeaseDate = getNoticeToCeaseDate(orderDto, orderLineDto);
            slaPremiumLiabilityDate = noticeToCeaseDate;

            /** Step 4 : Check if the COMMITMENT TREATMENT tag exists and if it contains a value **/
            if (!StringUtils.isBlank(commitmentTreatment)) {

                /** Step 5 : If the value in the COMMITMENT TREATMENT tag is 'Forgive' **/
                if ("Forgive".equalsIgnoreCase(commitmentTreatment)) {

                    /** Step 6 : Use the Notice to Cease Date (retrieved in step 3) as the Rental Liability Date **/
                    rentalLiabilityDate = noticeToCeaseDate;
                } else {

                    /** Step 7 : Check if the subscribers 'End of Engagement Date' is greater than (i.e. occurs before) the 'Notice to Cease Date' (retrieved in step 3) **/
                    /** Step 6 : Use the Notice to Cease Date (retrieved in step 3) as the Rental Liability Date **/
                    /** Step 8 : Use the End of Engagement Date as the Rental Liability Date **/
                    rentalLiabilityDate = getRentalLiabilityCeaseDate(noticeToCeaseDate, subscription);
                }
            } else {

                /** Step 7 : Check if the subscribers 'End of Engagement Date' is greater than (i.e. occurs before) the 'Notice to Cease Date' (retrieved in step 3) **/
                /** Step 6 : Use the Notice to Cease Date (retrieved in step 3) as the Rental Liability Date **/
                /** Step 8 : Use the End of Engagement Date as the Rental Liability Date **/
                rentalLiabilityDate = getRentalLiabilityCeaseDate(noticeToCeaseDate, subscription);
            }

        }
        orderLineDto.setRentalLiabilityDate(DateUtils.truncateTime(rentalLiabilityDate));
        orderLineDto.setSlaPremiumLiabilityDate(DateUtils.truncateTime(slaPremiumLiabilityDate));
    }

    /**
     * Provide Subscription Main Details
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void provideSubscriptionMainDetails(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        subscriptionMainDetails(orderDto, orderLineDto);
    }

    /**
     * Change Subscription Main Details
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void changeSubscriptionMainDetails(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        subscriptionMainDetails(orderDto, orderLineDto);
    }

    /**
     * Validate end point
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     */
    private void validateEndPoint(OrderDto orderDto, OrderLineDto orderLineDto) {

        // Which end of this circuit is the endpoint on: A = A end, B = B end, C = Circuit as a whole
        String epend = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPEND.getLabel());
        if (StringUtils.isBlank(epend)) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Missing " + EndPointCustomTableEnum.EPEND.name() + " attribute");
            return;
        }

        if (!EndEnum.A.name().equalsIgnoreCase(epend) && !EndEnum.B.name().equalsIgnoreCase(epend) && !EndEnum.C.name().equalsIgnoreCase(epend)) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Unrecognized value " + epend + " for " + EndPointCustomTableEnum.EPEND.name() + " attribute");
            return;
        }

        // CU = Irish Customer Premises, OC = Foreign Customer Premises LE = Local Exchange, SE = Serving Exchange, OC = Intl/Partner point of presence
        String eptype = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPTYPE.getLabel());
        if (StringUtils.isBlank(eptype)) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Missing " + EndPointCustomTableEnum.EPTYPE.name() + " attribute");
            return;
        }

        String epstart = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPSTART.getLabel());
        if (DateUtils.parseDateWithPattern(epstart, Utils.DATE_TIME_PATTERN) == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Invalid " + EndPointCustomTableEnum.EPSTART.name() + " attribute");
            return;
        }
    }

    /**
     * Get end point address
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return the end point address
     */

    private Map<String, Object> getEndPointAddress(OrderDto orderDto, OrderLineDto orderLineDto) {
        Map<String, Object> address = null;
        String errorMessage = null;

        String eptype = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPTYPE.getLabel());
        if (EndPointTypeEnum.CC.name().equalsIgnoreCase(eptype)) {
            // if EPTYPE field is foreign Site Address then the address will be Extracted from bill item
            address = new HashMap<>();
            address.put(EndPointCustomTableEnum.ADDRESS_LINE_1.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_LINE1.getLabel()));
            address.put(EndPointCustomTableEnum.ADDRESS_LINE_2.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_LINE2.getLabel()));
            address.put(EndPointCustomTableEnum.ADDRESS_LINE_3.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_LINE3.getLabel()));
            address.put(EndPointCustomTableEnum.ADDRESS_LINE_4.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_LINE4.getLabel()));
            address.put(EndPointCustomTableEnum.ADDRESS_LINE_5.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_LINE5.getLabel()));
            address.put(EndPointCustomTableEnum.COUNTY.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_COUNTY.getLabel()));
            address.put(EndPointCustomTableEnum.POSTAL_DISTRICT.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_POSTAL_DISTRICT.getLabel()));
            address.put(EndPointCustomTableEnum.COUNTRY.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_COUNTRY_CODE.getLabel()));
            address.put(EndPointCustomTableEnum.EIR_CODE.getLabel(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EIRCODE.getLabel()));
            return address;
        } else {
            // The Address ID of the endpoint. this will contain the ARD Id (e.g. 904694) or EIRCODE (e.g. D12W838)
            String epaddress = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPADDRESS.getLabel());
            if (StringUtils.isBlank(epaddress)) {
                errorMessage = "Missing " + EndPointCustomTableEnum.EPADDRESS.name() + " attribute";
                setError(orderDto, ErrorEnum.INVALID_DATA, errorMessage);
                return null;
            }
            // retrieve address details from eir broker
            address = getEndPointAddress(orderDto, epaddress);
        }
        return address;
    }

    /**
     * Check if is cancelled endpoint or not.
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return true if is cancelled endpoint.
     */
    private boolean isCancelledEndPoint(OrderDto orderDto, OrderLineDto orderLineDto) {
        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        ServiceInstance mainServiceInstance = getMainServiceInstance(subscription);
        CustomFieldValue lastCfValue = utilService.getLastCfVersion(mainServiceInstance, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(),
                DateUtils.truncateTime(orderLineDto.getBillEffectDate()), true);
        Map<String, String> serviceParametersCF = getServiceParameters(mainServiceInstance, lastCfValue);
        if (serviceParametersCF != null && !StateEnum.CA.name().equalsIgnoreCase(serviceParametersCF.get(ServiceParametersCFEnum.STATE.getLabel())) &&
                ActionQualifierEnum.CX.name().equalsIgnoreCase(orderLineDto.getBillActionQualifier())) {
            return true;
        }
        return false;
    }

    /**
     * Provide End Points
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void provideEndPoints(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        /** If the MSD STATE is not CA and EP Action Qualifier = CX **/
        /** execute PO2-486 [OMS Cancelled Orders] == don't process the endpoint bill item **/
        if (isCancelledEndPoint(orderDto, orderLineDto)) {
            return;
        }

        // If EPEND = C the order line do not to be processed by Opencell
        String epend = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPEND.getLabel());
        if (EndEnum.C.name().equalsIgnoreCase(epend)) {
            return;
        }

        validateEndPoint(orderDto, orderLineDto);
        if (orderDto.getErrorCode() != null) {
            return;
        }

        Map<String, Object> address = getEndPointAddress(orderDto, orderLineDto);
        if (orderDto.getErrorCode() != null) {
            return;
        }

        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        updateEndPoint(orderDto, orderLineDto, subscription, address);
        //subscriptionService.update(orderLineDto.getSubscription());
    }

    /**
     * Change End Points
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void changeEndPoints(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        /** If Action Qualifier is CX or EPSTATE is CA **/
        /** execute PO2-486 [OMS Cancelled Orders] == don't process the endpoint bill item **/
        String epstate = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPSTATE.getLabel());
        if (ActionQualifierEnum.CX.name().equalsIgnoreCase(orderLineDto.getBillActionQualifier()) || !StateEnum.CA.name().equalsIgnoreCase(epstate)) {
            return;
        }

        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        Map<String, Object> endPoint = orderService.findLastVersionEndPoint(subscription.getId(), orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPEND.getLabel()));
        if (endPoint == null || endPoint.isEmpty()) {

            // PO2-468
            provideEndPoints(orderDto, orderLineDto);

        } else {

            // If EPEND = C the order line do not to be processed by Opencell
            String epend = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_EP_EPEND.getLabel());
            if (EndEnum.C.name().equalsIgnoreCase(epend)) {
                return;
            }

            validateEndPoint(orderDto, orderLineDto);
            if (orderDto.getErrorCode() != null) {
                return;
            }

            Map<String, Object> address = getEndPointAddress(orderDto, orderLineDto);
            if (orderDto.getErrorCode() != null) {
                return;
            }

            updateEndPoint(orderDto, orderLineDto, subscription, address);
        }
    }

    /**
     * Provide Remarks
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void provideRemarks(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {


        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());

        Map<String, String> remark = (Map<String, String>) subscription.getCfValue(SubscriptionCFsEnum.REMARK.name());
        if (remark == null) {
            remark = new HashMap<>();
        }

        StringBuilder key = new StringBuilder();
        key.append(orderLineDto.getItemId()).append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDto.getItemComponentId())) {
            key.append(orderLineDto.getItemComponentId());
        }

        StringBuilder value = new StringBuilder();
        value.append(orderDto.getOrderCode()).append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        String effectiveDate = DateUtil.formatDate(orderLineDto.getBillEffectDate(), Utils.DATE_TIME_PATTERN);
        if (!StringUtils.isBlank(effectiveDate)) {
            value.append(effectiveDate);
        }
        value.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        String remarkId = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_REMARK_ID.getLabel());
        if (!StringUtils.isBlank(remarkId)) {
            value.append(remarkId);
        }
        value.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        String elementType = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_REMARK_ELEMENT_TYPE.getLabel());
        if (!StringUtils.isBlank(elementType)) {
            value.append(elementType);
        }
        value.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        String elementId = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_REMARK_ELEMENT_ID.getLabel());
        if (!StringUtils.isBlank(elementId)) {
            value.append(elementId);
        }
        value.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        String description = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_REMARK_DESCRIPTION.getLabel());
        if (!StringUtils.isBlank(description)) {
            value.append(description);
        }

        remark.put(key.toString(), value.toString());
        subscription.setCfValue(SubscriptionCFsEnum.REMARK.name(), remark);
        subscriptionService.update(subscription);
    }

    /**
     * Get address
     *
     * @param endPoint the end point
     * @return the address
     */
    private Map<String, Object> getAddress(Map<String, Object> endPoint) {
        Map<String, Object> address = null;
        if (endPoint != null && !endPoint.isEmpty()) {
            address = new HashMap<>();
            address.put(EndPointCustomTableEnum.ADDRESS_LINE_1.getLabel(), endPoint.get(EndPointCustomTableEnum.ADDRESS_LINE_1.getLabel()));
            address.put(EndPointCustomTableEnum.ADDRESS_LINE_2.getLabel(), endPoint.get(EndPointCustomTableEnum.ADDRESS_LINE_2.getLabel()));
            address.put(EndPointCustomTableEnum.ADDRESS_LINE_3.getLabel(), endPoint.get(EndPointCustomTableEnum.ADDRESS_LINE_3.getLabel()));
            address.put(EndPointCustomTableEnum.ADDRESS_LINE_4.getLabel(), endPoint.get(EndPointCustomTableEnum.ADDRESS_LINE_4.getLabel()));
            address.put(EndPointCustomTableEnum.ADDRESS_LINE_5.getLabel(), endPoint.get(EndPointCustomTableEnum.ADDRESS_LINE_5.getLabel()));
            address.put(EndPointCustomTableEnum.COUNTY.getLabel(), endPoint.get(EndPointCustomTableEnum.COUNTY.getLabel()));
            address.put(EndPointCustomTableEnum.POSTAL_DISTRICT.getLabel(), endPoint.get(EndPointCustomTableEnum.POSTAL_DISTRICT.getLabel()));
            address.put(EndPointCustomTableEnum.COUNTRY.getLabel(), endPoint.get(EndPointCustomTableEnum.COUNTRY.getLabel()));
            address.put(EndPointCustomTableEnum.EIR_CODE.getLabel(), endPoint.get(EndPointCustomTableEnum.EIR_CODE.getLabel()));
        }
        return address;
    }

    /**
     * Get associated end point
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return the associated end point
     */
    private Map<Object, Object> getAssociatedEndPoint(OrderDto orderDto, OrderLineDto orderLineDto) {
        String assoctype = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCTYPE.getLabel());
        String assoccircid = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCCIRCID.getLabel());
        String assocend = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCEND.getLabel());
        if (AssociationTypesEnum.CH.name().equalsIgnoreCase(assoctype) || AssociationTypesEnum.BR.name().equalsIgnoreCase(assoctype)) {
            if (StringUtils.isBlank(assoccircid)) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "Missing " + AssociationCustomTableEnum.ASSOCCIRCID.name() + " parameter.");
                return null;
            }

            //Subscription associatedSubscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, assoccircid);
            Subscription associatedSubscription = subscriptionService.findByOriginalCode(assoccircid);
            if (associatedSubscription == null) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "The associated subscription " + assoccircid + " is not found");
                return null;
            }

            // There should be only one CC or CU endpoint found in the associated subscriber, if more than one is found, send the order to Error Management
            List<Map<String, Object>> endpoints = orderService.findEndPoints(associatedSubscription.getId(), new ArrayList<>(Arrays.asList(EndPointTypeEnum.CC.name(),
                    EndPointTypeEnum.CU.name())), DateUtils.parseDateWithPattern(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCSTART.getLabel()),
                    Utils.DATE_TIME_PATTERN));
            if (endpoints != null && !endpoints.isEmpty() && endpoints.size() > 1) {
                setError(orderDto, ErrorEnum.INVALID_DATA,
                        "More than one " + EndPointTypeEnum.CC.name() + " or " + EndPointTypeEnum.CU.name() + " endpoint found in the associated subscriber");
                return null;
            }

            Map<String, Object> endPoint = orderService.findEndPoint(associatedSubscription.getId(), assocend,
                    DateUtils.parseDateWithPattern(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCSTART.getLabel()), Utils.DATE_TIME_PATTERN));
            if (endPoint == null || endPoint.isEmpty()) {
                setError(orderDto, ErrorEnum.INVALID_DATA,
                        "There is no endpoint found in the associated subscriber : " + assoccircid);
                return null;
            }
            Map<String, Object> address = getAddress(endPoint);
            Map<Object, Object> associatedEndPoint = new HashMap<>();
            associatedEndPoint.put(endPoint, address);
            return associatedEndPoint;
        }
        return null;
    }

    /**
     * Validate association
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     */
    private void validateAssociation(OrderDto orderDto, OrderLineDto orderLineDto) {
        String assocend = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCEND.getLabel());

        if (!EndEnum.A.name().equalsIgnoreCase(assocend) && !EndEnum.B.name().equalsIgnoreCase(assocend) && !EndEnum.C.name().equalsIgnoreCase(assocend)) {
            setError(orderDto, ErrorEnum.INVALID_DATA,
                    "The " + AssociationCustomTableEnum.ASSOCEND.name() + " value is not equal to " + EndEnum.A.name() + ", " + EndEnum.B.name() + " nor " + EndEnum.C.name());
            return;
        }

        String assocstart = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCSTART.getLabel());
        if (DateUtils.parseDateWithPattern(assocstart, Utils.DATE_TIME_PATTERN) == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Invalid " + AssociationCustomTableEnum.ASSOCSTART.name() + " attribute");
            return;
        }
    }

    /**
     * Check if is cancelled association or not.
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return true if is cancelled association.
     */
    private boolean isCancelledAssociation(OrderDto orderDto, OrderLineDto orderLineDto) {
        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        ServiceInstance mainServiceInstance = getMainServiceInstance(subscription);
        CustomFieldValue lastCfValue = utilService.getLastCfVersion(mainServiceInstance, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(),
                DateUtils.truncateTime(orderLineDto.getBillEffectDate()), true);
        Map<String, String> serviceParametersCF = getServiceParameters(mainServiceInstance, lastCfValue);
        if (serviceParametersCF != null && !StateEnum.CA.name().equalsIgnoreCase(serviceParametersCF.get(ServiceParametersCFEnum.STATE.getLabel())) &&
                !StateEnum.AS.name().equalsIgnoreCase(serviceParametersCF.get(ServiceParametersCFEnum.STATE.getLabel())) &&
                ActionQualifierEnum.CX.name().equalsIgnoreCase(orderLineDto.getBillActionQualifier())) {
            return true;
        }
        return false;
    }

    /**
     * Provide Associations
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void provideAssociations(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        /** If the MSD STATE is not CA and AS Act ion Qualifier = CX **/
        /** execute PO2-486 [OMS Cancelled Orders] == don't process the association bill item **/
        if (isCancelledAssociation(orderDto, orderLineDto)) {
            return;
        }

        validateAssociation(orderDto, orderLineDto);
        if (orderDto.getErrorCode() != null) {
            return;
        }
        updateAssociation(orderDto, orderLineDto);
    }

    /**
     * Change Associations
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void changeAssociations(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        /** If Action Qualifier is CX or ASSOCSTATE is CA **/
        /** execute PO2-486 [OMS Cancelled Orders] == don't process the association bill item **/
        String assocstate = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCSTATE.getLabel());
        if (ActionQualifierEnum.CX.name().equalsIgnoreCase(orderLineDto.getBillActionQualifier()) || !StateEnum.CA.name().equalsIgnoreCase(assocstate)) {
            return;
        }


        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        Map<String, Object> association = orderService.findLastVersionAssociation(subscription.getId(),
                orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCEND.getLabel()));
        String assocstatus = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_ASS_ASSOCSTATUS.getLabel());
        if (association == null || association.isEmpty() || !ItemStatusEnum.CE.name().equalsIgnoreCase(assocstatus)) {
            // PO2-470
            provideAssociations(orderDto, orderLineDto);
        } else {

            validateAssociation(orderDto, orderLineDto);
            if (orderDto.getErrorCode() != null) {
                return;
            }

            // If the ASSOCSTATUS of the ASSOCIATION on the bill item = CE,
            // then set the end date of the respective Association against the subscriber and do not look up the EP to retrieve the address, etc
            if (ItemStatusEnum.CE.name().equalsIgnoreCase(assocstatus)) {

                Date endDate = null;
                if (ItemStatusEnum.CE.name().equalsIgnoreCase(assocstatus)) {
                    endDate = orderLineDto.getBillEffectDate();
                }

                // Store the association
                updateAssociation(orderDto, orderLineDto, subscription, null, endDate);
            }
        }
    }

    /**
     * create new version service parameters CF
     *
     * @param orderDto        the order Dto
     * @param orderLineDto    the order line Dto
     * @param serviceInstance the service instance
     * @param cfValue         the cf value
     * @throws BusinessException the business exception
     */
    protected void updateServiceParametersCF(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance, Map<String, String> cfValue)
            throws BusinessException {
        Date startDate = orderLineDto.getCeaseEffectDate() != null ? orderLineDto.getCeaseEffectDate() : orderLineDto.getBillEffectDate();
        CustomFieldValue lastCfValue = utilService.getLastCfVersion(serviceInstance, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(),
                DateUtils.truncateTime(startDate), false);

        if (orderDto.getErrorCode() != null) {
            return;
        }

        /** Store service parameters CF **/
        updateServiceParametersCF(orderDto, orderLineDto, serviceInstance, orderLineDto.getCeaseEffectDate(), orderLineDto.getRentalLiabilityDate(), lastCfValue, cfValue);

    }

    /**
     * Process Service Charges
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void processServiceCharges(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        ServiceInstance mainServiceInstance = getMainServiceInstance(subscription);
        if (mainServiceInstance == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The main service is not found on subscription " + subscription.getCode());
            return;
        }
        if (orderLineDto.isMain()) {
            if (BillActionEnum.P.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "There is already a main service with the same bill code (" + orderLineDto.getBillCode() + ") on this subscription");
            } else {
                setError(orderDto, ErrorEnum.INVALID_DATA, "Can't change the charge of the main service (" + orderLineDto.getBillCode() + ")");
            }
            return;
        }

        /*
         * // If the STATE of the Subscription Main Details is = CA, this will be addressed in PO2-486 String state = getValueAttribute(mainServiceInstance,
         * ServiceInstanceOmsCFsEnum.MAIN_SUBSCRIPTION_DETAILS.name(), orderLineDto.getBillEffectDate(), SubMainDetailsAttributeNamesEnum.STATE.name());
         */

        /** If the STATE of the charge is = CA, this will be addressed in PO2-486 **/
        /** execute PO2-486 [OMS Cancelled Orders] == don't process the endpoint bill item **/
        String state = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_STATE.getLabel());
        if (StateEnum.CA.name().equalsIgnoreCase(state)) {
            return;
        }

        /**
         * Using the Item_Component_Id and Bill Code, attempt to locate the respective charge (i.e. ancillary service) against the subscriber
         */

        ServiceInstance serviceInstance = getAncillaryServiceInstance(orderDto, orderLineDto, subscription);
        if (orderDto.getErrorCode() != null) {
            return;
        }
        /*
         * if (serviceInstance == null && BillActionEnum.CH.name().equalsIgnoreCase(orderLineDto.getBillAction())) { setError(orderDto, ErrorEnum.INVALID_DATA, "The service to change " +
         * orderLineDto.getBillCode() + " is not found on subscription" + subscription.getCode()); return; }
         */

        CustomFieldValue lastCfValue = utilService.getLastCfVersion(serviceInstance, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(),
                DateUtils.truncateTime(orderLineDto.getBillEffectDate()), false);

        Map<String, String> serviceParametersCF = getServiceParameters(serviceInstance, lastCfValue);
        if (serviceParametersCF != null) {
            String status = serviceParametersCF.get(ServiceParametersCFEnum.STATUS.getLabel());
            if (ItemStatusEnum.CE.name().equalsIgnoreCase(status)) {
                /** execute PO2-490 [OMS Cease Ancillary] **/
                ceaseAncillary(orderDto, orderLineDto, subscription);
                return;
            }
            if (ItemStatusEnum.AC.name().equalsIgnoreCase(status)) {

                /** Store service parameters CF **/
                updateServiceParametersCF(orderDto, orderLineDto, serviceInstance, null, null, lastCfValue, null);
                serviceInstance.setQuantity(new BigDecimal(orderLineDto.getBillQuantity()));
                serviceInstance = serviceInstanceService.update(serviceInstance);

                // Calculate a delta quantity
                setDeltaQuantity(orderLineDto, new BigDecimal(serviceParametersCF.get(ServiceParametersCFEnum.QUANTITY.getLabel())));

                //reratingService.rerate(serviceInstance, orderLineDto.getBillEffectDate());
                orderDto.getServiceInstances().put(serviceInstance.getId(), orderLineDto);
            }
        } else {
            // If the bill code on the (Charge) bill item is NOT found against the Subscriber Ancillary Service, or
            // If the bill code on the (Charge) bill item was found but the Item_Component_Id's found are different

            // activate the ancillary service (i.e. create it) against the subscriber
            activateService(orderDto, orderLineDto, subscription, true);
        }
    }

    /**
     * Create service instance
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return service instance
     * @throws BusinessException the business exception
     */
    @Override
    protected ServiceInstance createServiceInstance(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        ServiceInstance serviceInstance = new ServiceInstance();
        serviceInstance.setVersion(3);
        serviceInstance.setCode(orderLineDto.getBillCode());
        //serviceInstance.setDescription();
        serviceInstance.setOrderNumber(orderDto.getOrderCode());
        serviceInstance.setQuantity(new BigDecimal(orderLineDto.getBillQuantity()));
        ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
        serviceInstance.setServiceTemplate(serviceTemplate);
        serviceInstance.setSubscription(orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode()));
        Date billEffectDate = DateUtils.truncateTime(orderLineDto.getBillEffectDate());
        serviceInstance.setSubscriptionDate(billEffectDate);

        setServiceInstanceCf(orderDto, orderLineDto, serviceInstance);

        Long agreementDuration = getEngagementDuration(orderDto, orderLineDto);
        if (agreementDuration != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(billEffectDate);
            calendar.add(Calendar.MONTH, agreementDuration.intValue());
            serviceInstance.setEndAgreementDate(calendar.getTime());
        }

        return serviceInstance;
    }

    /**
     * Set service instance Cf
     *
     * @param orderDto        the order Dto
     * @param orderLineDto    the order line Dto
     * @param serviceInstance the service instance
     */
    protected void setServiceInstanceCf(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance) {
        // Set the SERVICE_PARAMETERS cf
        updateServiceParametersCF(orderDto, orderLineDto, serviceInstance, null, null, null, null);

        // Set ITEM_COMPONENT_ID cf
        serviceInstance.setCfValue(ServiceInstanceCFsEnum.ITEM_COMPONENT_ID.name(), orderLineDto.getItemComponentId());
    }

    /**
     * Apply SLA surcharge
     *
     * @param orderDto        the order Dto.
     * @param orderLineDto    the order line Dto.
     * @param lastCfValue     the last service parameters CF value.
     * @param serviceInstance the service instance.
     */
    private void applySlaSurcharge(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance, CustomFieldValue lastCfValue) {

        String sla = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_MAIN_SLA_IND.getLabel());
        if (!StringUtils.isBlank(sla)) {
            String oldSla = null;
            if (lastCfValue != null && lastCfValue.getMapValue() != null && !lastCfValue.getMapValue().isEmpty()) {
                Map<String, Object> serviceParametersValues = (Map<String, Object>) utilService.getCFValue(serviceInstance,
                        ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), (String) lastCfValue.getMapValue().values().iterator().next());
                if (serviceParametersValues != null && !serviceParametersValues.isEmpty()) {
                    oldSla = (String) serviceParametersValues.get(ServiceParametersCFEnum.SLA.getLabel());
                }
            }

            // If SLA change on main service
            if (!sla.equalsIgnoreCase(oldSla)) {
                for (ServiceInstance service : serviceInstance.getSubscription().getServiceInstances()) {
                    if (!serviceInstance.equals(service) && service.getRecurringChargeInstances() != null && !service.getRecurringChargeInstances().isEmpty()) {
                        //reratingService.rerate(service, orderLineDto.getBillEffectDate());
                        orderDto.getServiceInstances().put(service.getId(), orderLineDto);
                    }
                }
            }
        }
    }

    /**
     * Rerate all order services
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    private void rerateServices(OrderDto orderDto) throws BusinessException {
        for (Map.Entry<Long, OrderLineDto> entry : orderDto.getServiceInstances().entrySet()) {
            ServiceInstance serviceInstance = serviceInstanceService.findById(entry.getKey());
            OrderLineDto orderLineDto = entry.getValue();

            String state = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.OMS_STATE.getLabel());
            /**
             * [OMS Cancelled Orders] As openeir when a Subscriber does not exist and one was created for a Cancelled Order,
             * I want to terminate the Subscription when the order has been successfully processed and the cancellation charge successfully applied
             */
            if (orderLineDto.isMain() && StateEnum.CA.name().equalsIgnoreCase(state) && BillActionEnum.P.name().equals(orderLineDto.getBillAction())) {
                orderLineDto.setBillEffectDate(Utils.addSecondsToDate(orderLineDto.getBillEffectDate(), 1));
                ceaseCircuit(orderDto, orderLineDto);
                if (orderDto.getErrorCode() != null) {
                    throw new BusinessException(orderDto.getErrorMessage());
                }
            } else {
                reratingService.rerate(serviceInstance, orderLineDto.getBillEffectDate(), true);
                applySubscriptionCharge(orderDto, orderLineDto, serviceInstance);
            }
        }
    }

    @Override
    protected BaseOrderApi getSelfForNewTx() {
        return omsOrderApi;
    }
}
