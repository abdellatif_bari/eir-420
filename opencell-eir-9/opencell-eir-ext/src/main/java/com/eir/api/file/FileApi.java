package com.eir.api.file;

import com.eir.commons.enums.TransformationRulesEnum;
import com.eir.commons.utils.BusinessRulesValidator;
import com.eir.commons.utils.TransformationRule;
import com.eir.commons.utils.TransformationRulesFactory;
import com.eir.commons.utils.TransformationRulesHandler;
import com.eir.service.commons.UtilService;
import org.apache.commons.io.FilenameUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.api.BaseApi;
import org.meveo.api.commons.FileRecordsTypeEnum;
import org.meveo.api.commons.Utils;
import org.meveo.api.dto.flatfile.ExportDto;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.admin.FileFormat;
import org.meveo.model.bi.FlatFile;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.customEntities.CustomEntityTemplate;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.bi.impl.FlatFileService;
import org.meveo.service.custom.CustomTableService;
import org.primefaces.model.SortOrder;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.ws.rs.core.StreamingOutput;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.meveo.service.base.NativePersistenceService.FIELD_ID;

/**
 * The file API.
 *
 * @author Abdellatif BARI
 */
@Stateless
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class FileApi extends BaseApi {

    @Inject
    private CustomTableService customTableService;
    @Inject
    private TransformationRulesFactory transformationRulesFactory;
    @Inject
    private BusinessRulesValidator businessRulesValidator;
    @Inject
    private UtilService utilService;
    @Inject
    private FlatFileService flatFileService;

    /**
     * Download file
     *
     * @param filePath file path.
     * @return the file to be download.
     * @throws BusinessException business exception
     */
    public File getFile(String filePath) throws BusinessException {
        File file = new File(paramBeanFactory.getChrootDir() + File.separator + filePath);
        if (!file.exists()) {
            throw new BusinessException("File does not exists : " + file.getPath());
        }
        return file;
    }

    /**
     * Download file
     *
     * @param fileId          file id
     * @param fileRecordsType file records type
     * @return the file to be download.
     * @throws BusinessException business exception
     */
    public File getFile(Long fileId, FileRecordsTypeEnum fileRecordsType) throws BusinessException {
        if (fileId == null) {
            throw new BusinessException("The file id is required");
        }
        FlatFile flatFile = flatFileService.findById(fileId);
        if (flatFile == null) {
            throw new BusinessException("The file having the id " + fileId + " does not exists");
        }
        File file = getFile(flatFile, fileRecordsType);
        if (file == null || !file.exists()) {
            throw new BusinessException("File does not exists : " + file.getPath());
        }
        return file;
    }

    /**
     * Zip file on disk
     *
     * @param file        file.
     * @param zipFileName zip name file.
     * @return the zip of input file.
     * @throws BusinessException business exception
     */
    public StreamingOutput zipFile(File file, String zipFileName) throws BusinessException {
        try {
            String zipFilePathName = file.getParent() + File.separator + zipFileName;
            createZipFile(file, zipFilePathName);
            File zipfile = new File(zipFilePathName);

            return output -> {
                Files.copy(zipfile.toPath(), output);
                zipfile.delete();
            };
        } catch (IOException ex) {
            throw new BusinessException("Error during zip file " + file.getName() + " : " + ex.getMessage());
        }
    }

    /**
     * Create zip file
     *
     * @param file            the file
     * @param zipFilePathName the zip file path name
     * @throws IOException the IO exception
     */
    private void createZipFile(File file, String zipFilePathName) throws IOException {
        byte[] buffer = new byte[1024];
        try (FileOutputStream fos = new FileOutputStream(zipFilePathName);
             ZipOutputStream zos = new ZipOutputStream(fos);
             FileInputStream in = new FileInputStream(file)) {
            ZipEntry ze = new ZipEntry(file.getName());
            zos.putNextEntry(ze);
            int len;
            while ((len = in.read(buffer)) > 0) {
                zos.write(buffer, 0, len);
            }
        } catch (IOException ex) {
            throw ex;
        }
    }

    /**
     * Create file from custom table by the filtered data
     *
     * @param customTableCode Custom table code - can be either db table's name or a custom entity template code
     * @param exportDto       export Dto
     * @return the file to be exported.
     * @throws BusinessException business exception
     */
    public File getFile(String customTableCode, ExportDto exportDto) throws BusinessException {
        try {
            if (StringUtils.isBlank(exportDto.getFileName())) {
                throw new BusinessException("the file name is missing");
            }
            if (StringUtils.isBlank(exportDto.getFileFormatCode())) {
                throw new BusinessException("the file format is missing");
            }
            TransformationRulesHandler transformationRulesHandler = null;
            if (exportDto.isExportValidTable()) {
                if (StringUtils.isBlank(exportDto.getExportType())) {
                    throw new BusinessException("the export type is missing");
                }
                transformationRulesHandler = transformationRulesFactory.getInstance(TransformationRulesEnum.valueOfIgnoreCase(exportDto.getExportType()));
                if (transformationRulesHandler == null || transformationRulesHandler.getTransformationRules() == null ||
                        transformationRulesHandler.getTransformationRules().isEmpty()) {
                    throw new BusinessException("the transformation rules between stage and valid table are missing");
                }
            }

            LinkedHashMap<String, String> fieldNamesDescriptions = utilService.getFieldNamesDescriptions(exportDto.getFileFormatCode());
            // Export valid custom table content.
            if (exportDto.isExportValidTable() && (fieldNamesDescriptions == null || fieldNamesDescriptions.isEmpty())) {
                throw new BusinessException("the header record is missing in configuration template in file format definition.");
            }

            List<String> fieldNames = new ArrayList<>();
            Collection<String> fieldDescriptions = new ArrayList<>();
            if (fieldNamesDescriptions != null && !fieldNamesDescriptions.isEmpty()) {
                fieldNames = new ArrayList(fieldNamesDescriptions.keySet());
                fieldDescriptions = fieldNamesDescriptions.values();
            }

            List<String> fields = null;
            Map<String, TransformationRule> referenceData = new HashMap<>();


            // in the case of generating the export of the valid table, it is possible that
            // the csv file contain also other fields of the stage table e.g Action field.
            List<String> stagingFields = null;

            if (exportDto.isExportValidTable()) {
                fields = new ArrayList<>();
                stagingFields = new ArrayList<>();
                for (String fieldName : fieldNames) {
                    boolean isValidFieldExist = false;
                    for (TransformationRule transformationRule : transformationRulesHandler.getTransformationRules()) {
                        if (fieldName.equalsIgnoreCase(transformationRule.getStageFieldName()) && !StringUtils.isBlank(transformationRule.getValidFieldName())) {
                            fields.add(transformationRule.getValidFieldName());
                            stagingFields.add(transformationRule.getValidFieldName());
                            if (!StringUtils.isBlank(transformationRule.getReferenceTable())) {
                                referenceData.put(transformationRule.getValidFieldName(), transformationRule);
                            }
                            isValidFieldExist = true;
                            break;
                        }
                    }
                    if (!isValidFieldExist) {
                        stagingFields.add(fieldName);
                    }
                }
                fieldNames = fields;
            }


            fields = exportDto.getPagingAndFiltering() != null && exportDto.getPagingAndFiltering().getFields() != null ?
                    Arrays.asList(exportDto.getPagingAndFiltering().getFields().split(",")) : fieldNames;


            CustomEntityTemplate cet = customTableService.getCET(customTableCode);
            Map<String, CustomFieldTemplate> cfts = customTableService.retrieveAndValidateCfts(cet, false);
            PaginationConfiguration paginationConfig = toPaginationConfiguration(FIELD_ID, SortOrder.ASCENDING, fields, exportDto.getPagingAndFiltering(), cfts);
            List<Map<String, Object>> list = customTableService.list(cet.getDbTablename(), paginationConfig);

            // Create temp file.
            File file = new File(System.getProperty("java.io.tmpdir"), FilenameUtils.removeExtension(exportDto.getFileName()) + ".csv");

            if (file.exists()) {
                file.delete();
            }

            // Delete temp file when program exits.
            file.deleteOnExit();

            // Write to temp file
            BufferedWriter out = new BufferedWriter(new FileWriter(file));

            if (fieldDescriptions != null) {
                out.write(String.join(",", fieldDescriptions));
                out.write(System.lineSeparator());
            }

            if (stagingFields != null && !stagingFields.isEmpty()) {
                fields = stagingFields;
            }

            if (fields == null || fields.isEmpty()) {
                fields = new ArrayList<>();
                Collection<CustomFieldTemplate> cftFields = cfts.values();
                for (CustomFieldTemplate cft : cftFields) {
                    fields.add(cft.getCode());
                }
            }

            for (Map<String, Object> record : list) {
                List<String> fieldValues = new ArrayList<>();
                for (String field : fields) {
                    Object value = record.get(field) != null ? record.get(field) : "";

                    // Get the code instead the id
                    if (exportDto.isExportValidTable() && referenceData.containsKey(field)) {
                        value = businessRulesValidator.getMatchedValue(value, referenceData.get(field));
                    }
                    if (value instanceof Date) {
                        fieldValues.add(DateUtils.formatDateWithPattern((Date) value, Utils.DATE_DEFAULT_PATTERN));
                    } else {
                        fieldValues.add(String.valueOf(value));
                    }
                }
                if (!fieldValues.isEmpty()) {
                    out.write(String.join(",", fieldValues));
                    out.write(System.lineSeparator());
                }
            }
            out.close();
            return file;
        } catch (IOException ex) {
            throw new BusinessException("Error during the export file  " + exportDto.getFileName() + " : " + ex.getMessage(), ex);
        }
    }

    /**
     * Get file
     *
     * @param flatFile        flat file
     * @param fileRecordsType file records type
     * @return the file to be download.
     * @throws BusinessException business exception
     */
    private File getFile(FlatFile flatFile, FileRecordsTypeEnum fileRecordsType) throws BusinessException {
        FileFormat fileFormat = flatFile.getFileFormat();
        String filePath = null;
        String fileName = flatFile.getFileCurrentName();
        switch (flatFile.getStatus()) {
            case WELL_FORMED:
                if (fileFormat != null) {
                    filePath = paramBeanFactory.getChrootDir() + File.separator + fileFormat.getInputDirectory();
                }
                break;
            case BAD_FORMED:
                if (fileFormat != null) {
                    filePath = paramBeanFactory.getChrootDir() + File.separator + fileFormat.getRejectDirectory();
                }
                break;
            case VALID:
                filePath = flatFile.getCurrentDirectory();
                break;
            case REJECTED:
                if (fileRecordsType == FileRecordsTypeEnum.REJECTED) {
                    filePath = flatFile.getCurrentDirectory();
                } else {
                    if (fileFormat != null) {
                        if (fileRecordsType == FileRecordsTypeEnum.SUCCESSFUL) {
                            fileName = !StringUtils.isBlank(fileName) ? fileName.replace(".rejected", ".processed") : null;
                            filePath = paramBeanFactory.getChrootDir() + File.separator + fileFormat.getOutputDirectory();
                        } else {
                            fileName = !StringUtils.isBlank(fileName) ? fileName.replace(".rejected", "") : null;
                            filePath = paramBeanFactory.getChrootDir() + File.separator + fileFormat.getArchiveDirectory();
                        }
                    } else {
                        filePath = flatFile.getCurrentDirectory();
                        if (!StringUtils.isBlank(filePath)) {
                            if (filePath.endsWith(File.separator)) {
                                filePath = filePath.substring(0, filePath.lastIndexOf(File.separator));
                            }
                            filePath = filePath.substring(0, filePath.lastIndexOf(File.separator)) + File.separator;
                            if (fileRecordsType == FileRecordsTypeEnum.SUCCESSFUL) {
                                fileName = !StringUtils.isBlank(fileName) ? fileName.replace(".rejected", ".processed") : null;
                                filePath = filePath + "output";
                            } else {
                                fileName = !StringUtils.isBlank(fileName) ? fileName.replace(".rejected", "") : null;
                                filePath = filePath + "archive";
                            }
                        }
                    }
                }
                break;
            case ARCHIVED:
                filePath = paramBeanFactory.getChrootDir() + File.separator + fileFormat.getArchiveDirectory() + File.separator + flatFile.getFileCurrentName();
                break;
        }
        File file = null;
        if (!StringUtils.isBlank(filePath)) {
            file = new File(filePath + File.separator + fileName);
        }
        return file;
    }

}
