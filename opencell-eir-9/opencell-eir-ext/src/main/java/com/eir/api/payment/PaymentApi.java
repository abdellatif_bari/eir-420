package com.eir.api.payment;

import com.eir.commons.enums.ApplicationPropertiesEnum;
import com.eir.commons.enums.customfields.AccountOperationCFsEnum;
import com.eir.service.billing.impl.BillingAccountService;
import org.meveo.admin.exception.BusinessException;
import org.meveo.api.BaseApi;
import org.meveo.api.dto.payment.ReverseAccountOperationDto;
import org.meveo.api.dto.payment.TransferBillingAccountDto;
import org.meveo.api.dto.payment.TransferBillingAccountsDto;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.payments.AccountOperation;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.model.payments.CustomerAccountStatusEnum;
import org.meveo.model.payments.MatchingAmount;
import org.meveo.model.payments.MatchingCode;
import org.meveo.model.payments.MatchingStatusEnum;
import org.meveo.model.payments.MatchingTypeEnum;
import org.meveo.model.payments.OCCTemplate;
import org.meveo.model.payments.OtherCreditAndCharge;
import org.meveo.service.payments.impl.AccountOperationService;
import org.meveo.service.payments.impl.MatchingCodeService;
import org.meveo.service.payments.impl.OCCTemplateService;
import org.meveo.service.payments.impl.OtherCreditAndChargeService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * The payment API.
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirPaymentApi")
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class PaymentApi extends BaseApi {

    @Inject
    private BillingAccountService billingAccountService;

    @Inject
    AccountOperationService accountOperationService;

    @Inject
    MatchingCodeService matchingCodeService;

    @Inject
    OtherCreditAndChargeService otherCreditAndChargeService;

    @Inject
    private OCCTemplateService occTemplateService;

    /**
     * Transfer account operation.
     *
     * @param transferBillingAccountsDto the transfer billing account Dto
     * @throws BusinessException the business exception
     */
    public void transferAccountOperation(TransferBillingAccountsDto transferBillingAccountsDto) throws BusinessException {
        validateTransferAccountOperation(transferBillingAccountsDto);
        processTransferAccountOperation(transferBillingAccountsDto);
    }

    /**
     * Transfer amount.
     *
     * @param transferBillingAccountDto the transfer billing account Dto
     * @throws BusinessException the business exception
     */
    public void transferAmount(TransferBillingAccountDto transferBillingAccountDto) throws BusinessException {
        validateTransferAmount(transferBillingAccountDto);
        processTransferAmount(transferBillingAccountDto);
    }

    /**
     * Reverse payment.
     *
     * @param reverseAccountOperationDto the reverse account operation Dto
     * @throws BusinessException the business exception
     */
    public void reversePayment(ReverseAccountOperationDto reverseAccountOperationDto) throws BusinessException {
        validateReversePayment(reverseAccountOperationDto);
        processReversePayment(reverseAccountOperationDto);
    }

    /**
     * Validate transfer account operation.
     *
     * @param transferBillingAccountsDto the transfer billing accounts Dto
     * @throws BusinessException the business exception
     */
    private void validateTransferAccountOperation(TransferBillingAccountsDto transferBillingAccountsDto) throws BusinessException {
        if (StringUtils.isBlank(transferBillingAccountsDto.getFromBillingAccountNumber())) {
            throw new BusinessException("Missing fromBillingAccountNumber parameter.");
        }

        if (transferBillingAccountsDto.getAccountOperationId() == null) {
            throw new BusinessException("Missing account operation id parameter.");
        }

        if (transferBillingAccountsDto.getToBillingAccounts() == null || transferBillingAccountsDto.getToBillingAccounts().isEmpty()) {
            throw new BusinessException("Missing toBillingAccountNumber parameter.");
        }

        transferBillingAccountsDto.setTotalAmount(BigDecimal.ZERO);
        for (int i = 0; i < transferBillingAccountsDto.getToBillingAccounts().size(); i++) {
            TransferBillingAccountDto transferBillingAccountDto = transferBillingAccountsDto.getToBillingAccounts().get(i);

            if (StringUtils.isBlank(transferBillingAccountDto.getToBillingAccountNumber())) {
                throw new BusinessException("Missing billingAccounts[" + i + "].toBillingAccountNumber parameter");
            }

            if (transferBillingAccountDto.getAmount() == null) {
                throw new BusinessException("Missing billingAccounts[" + i + "].amount parameter.");
            }

            if (transferBillingAccountDto.getAmount().compareTo(BigDecimal.ZERO) == 0) {
                throw new BusinessException("The billingAccounts[" + i + "].amount must not be zero");
            }
            transferBillingAccountsDto.setTotalAmount(transferBillingAccountsDto.getTotalAmount().add(transferBillingAccountDto.getAmount()));
        }
    }

    /**
     * Process the transfer account operation from a billing account to an other.
     *
     * @param transferBillingAccountsDto the transfer billing accounts Dto
     * @throws BusinessException business exception
     */
    private void processTransferAccountOperation(TransferBillingAccountsDto transferBillingAccountsDto) throws BusinessException {
        String fromBillingAccountNumber = transferBillingAccountsDto.getFromBillingAccountNumber();
        Long accountOperationId = transferBillingAccountsDto.getAccountOperationId();

        // Get the account operation to transfer
        AccountOperation accountOperation = accountOperationService.findById(accountOperationId);
        if (accountOperation == null) {
            throw new BusinessException("the account operation " + accountOperationId + " not found");
        }
        if (accountOperation.getUnMatchingAmount() == null || accountOperation.getUnMatchingAmount().compareTo(transferBillingAccountsDto.getTotalAmount()) < 0) {
            throw new BusinessException("Insufficient amount to perform transfer operation from the account operation  " + accountOperationId);
        }
        if (accountOperation.getUnMatchingAmount().compareTo(transferBillingAccountsDto.getTotalAmount()) > 0) {
            throw new BusinessException("The partial transfer is not authorized");
        }

        BillingAccount fromBillingAccount = billingAccountService.findByNumber(fromBillingAccountNumber);
        if (fromBillingAccount == null) {
            throw new BusinessException("The source billing account with number : " + fromBillingAccountNumber + " is not found");
        }

        String billingAccountNumber = (String) accountOperation.getCfValue(AccountOperationCFsEnum.BILLING_ACCOUNT_NUMBER.name());
        if (!fromBillingAccountNumber.equals(billingAccountNumber)) {
            throw new BusinessException(
                    "the account operation " + accountOperationId + " to be transferred doesn't belong to the source billing account " + fromBillingAccountNumber);
        }

        if (transferBillingAccountsDto.getToBillingAccounts() != null && !transferBillingAccountsDto.getToBillingAccounts().isEmpty()) {
            // Unmatching the accountOperation
            /*
             * if (accountOperation.getMatchingStatus() == MatchingStatusEnum.L || accountOperation.getMatchingStatus() == MatchingStatusEnum.P) {
             * matchingCodeService.unmatchingOperationAccount(accountOperation); }
             */

            for (TransferBillingAccountDto toBillingAccount : transferBillingAccountsDto.getToBillingAccounts()) {
                transferAccountOperation(accountOperation, fromBillingAccount, toBillingAccount, transferBillingAccountsDto.getOperationReason());
            }

            // Update the old account operation
            accountOperation.setReference(
                    accountOperationService.getRefrence(accountOperation.getId(), accountOperation.getReference(), AccountOperationService.AccountOperationActionEnum.s.name()));
        }

    }

    /**
     * Transfer an account operation from a billing account to an other.
     *
     * @param accountOperation          the account operation
     * @param fromBillingAccount        source billing account
     * @param transferBillingAccountDto destination billing account
     * @param operationReason           operation reason
     * @throws BusinessException business exception
     */
    private void transferAccountOperation(AccountOperation accountOperation, BillingAccount fromBillingAccount, TransferBillingAccountDto transferBillingAccountDto,
                                          String operationReason) throws BusinessException {

        // check if it is not the same account
        if (transferBillingAccountDto.getToBillingAccountNumber().equalsIgnoreCase(fromBillingAccount.getExternalRef1())) {
            throw new BusinessException("the source billing account is the same as the destination billing account");
        }

        BillingAccount toBillingAccount = billingAccountService.findByNumber(transferBillingAccountDto.getToBillingAccountNumber());
        if (toBillingAccount == null) {
            throw new BusinessException("The destination billing account with number : " + transferBillingAccountDto.getToBillingAccountNumber() + " is not found");
        }

        String fileName = (String) accountOperation.getCfValue(AccountOperationCFsEnum.PAYMENT_FILE_NAME.name());

        // Create the new account operation on the fromBillingAccountNumber to settle the old one.
        OtherCreditAndCharge fromAccountOperation = accountOperationService.createFromAccountOperation(accountOperation, transferBillingAccountDto.getAmount());
        fromAccountOperation.setAmountWithoutTax(fromAccountOperation.getAmount());
        fromAccountOperation.setCfValue(AccountOperationCFsEnum.BILLING_ACCOUNT_NUMBER.name(), fromBillingAccount.getExternalRef1());
        fromAccountOperation.setCfValue(AccountOperationCFsEnum.FROM_BILLING_ACCOUNT_NUMBER.name(), fromBillingAccount.getExternalRef1());
        fromAccountOperation.setCfValue(AccountOperationCFsEnum.TO_BILLING_ACCOUNT_NUMBER.name(), toBillingAccount.getExternalRef1());
        fromAccountOperation.setCfValue(AccountOperationCFsEnum.PAYMENT_FILE_NAME.name(), fileName);
        fromAccountOperation.setCfValue(AccountOperationCFsEnum.OPERATION_REASON.name(), operationReason);
        accountOperationService.update(fromAccountOperation);

        // Create the new account operation on the toBillingAccountNumber which is equivalent to the transfer.
        AccountOperation toAccountOperation = accountOperationService.createToAccountOperation(accountOperation, toBillingAccount.getCustomerAccount(),
                transferBillingAccountDto.getAmount());
        toAccountOperation.setAmountWithoutTax(toAccountOperation.getAmount());
        toAccountOperation.setCfValue(AccountOperationCFsEnum.BILLING_ACCOUNT_NUMBER.name(), toBillingAccount.getExternalRef1());
        toAccountOperation.setCfValue(AccountOperationCFsEnum.FROM_BILLING_ACCOUNT_NUMBER.name(), fromBillingAccount.getExternalRef1());
        toAccountOperation.setCfValue(AccountOperationCFsEnum.TO_BILLING_ACCOUNT_NUMBER.name(), toBillingAccount.getExternalRef1());
        toAccountOperation.setCfValue(AccountOperationCFsEnum.PAYMENT_FILE_NAME.name(), fileName);
        fromAccountOperation.setCfValue(AccountOperationCFsEnum.OPERATION_REASON.name(), operationReason);
        accountOperationService.update(toAccountOperation);

    }

    /**
     * Validate the transfer billing account Dto
     *
     * @param transferBillingAccountDto the transfer billing account Dto
     * @throws BusinessException the business exception
     */
    private void validateTransferAmount(TransferBillingAccountDto transferBillingAccountDto) throws BusinessException {
        if (StringUtils.isBlank(transferBillingAccountDto.getFromBillingAccountNumber())) {
            throw new BusinessException("Missing fromBillingAccountNumber parameter.");
        }

        if (StringUtils.isBlank(transferBillingAccountDto.getToBillingAccountNumber())) {
            throw new BusinessException("Missing toBillingAccountNumber parameter.");
        }

        if (transferBillingAccountDto.getAmount() == null) {
            throw new BusinessException("Missing amount parameter.");
        }
        if (transferBillingAccountDto.getAmount().compareTo(BigDecimal.ZERO) == 0) {
            throw new BusinessException("The amount must not be zero");
        }
    }

    /**
     * Process the transfer amount from a billing account to an other.
     *
     * @param transferBillingAccountDto the transfer billing account Dto
     * @throws BusinessException business exception
     */
    private void processTransferAmount(TransferBillingAccountDto transferBillingAccountDto) throws BusinessException {

        BillingAccount fromBillingAccount = billingAccountService.findByNumber(transferBillingAccountDto.getFromBillingAccountNumber());
        if (fromBillingAccount == null) {
            throw new BusinessException("The source billing account with number : " + transferBillingAccountDto.getFromBillingAccountNumber() + " is not found");
        }

        BillingAccount toBillingAccount = billingAccountService.findByNumber(transferBillingAccountDto.getToBillingAccountNumber());
        if (toBillingAccount == null) {
            throw new BusinessException("The recipient billing account with number : " + transferBillingAccountDto.getToBillingAccountNumber() + " is not found");
        }
        transferAmount(transferBillingAccountDto, fromBillingAccount, toBillingAccount);
    }

    /**
     * Transfer amount.
     *
     * @param transferBillingAccountDto the transfer billing account Dto
     * @param fromBillingAccount        the from billing account
     * @param toBillingAccount          the to billing account
     * @throws BusinessException the business exception
     */
    private void transferAmount(TransferBillingAccountDto transferBillingAccountDto, BillingAccount fromBillingAccount, BillingAccount toBillingAccount) throws BusinessException {
        String occTransferAccountCredit = ApplicationPropertiesEnum.TEMPLATE_TRANSFER_ACCOUNT_CREDIT.getProperty();
        String occTransferAccountDebit = ApplicationPropertiesEnum.TEMPLATE_TRANSFER_ACCOUNT_DEBIT.getProperty();
        String descTransferFrom = ApplicationPropertiesEnum.DESCRIPTION_TRANSFER_FROM.getProperty();
        String descTransferTo = ApplicationPropertiesEnum.DESCRIPTION_TRANSFER_TO.getProperty();

        OtherCreditAndCharge otherCreditAndChargeDebit = otherCreditAndChargeService.addOCC(occTransferAccountDebit,
                descTransferFrom + " " + toBillingAccount.getCustomerAccount().getCode(), fromBillingAccount.getCustomerAccount(), transferBillingAccountDto.getAmount(), new Date());
        otherCreditAndChargeDebit.setAmountWithoutTax(otherCreditAndChargeDebit.getAmount());
        otherCreditAndChargeDebit.setCfValue(AccountOperationCFsEnum.BILLING_ACCOUNT_NUMBER.name(), fromBillingAccount.getExternalRef1());
        otherCreditAndChargeDebit.setCfValue(AccountOperationCFsEnum.FROM_BILLING_ACCOUNT_NUMBER.name(), fromBillingAccount.getExternalRef1());
        otherCreditAndChargeDebit.setCfValue(AccountOperationCFsEnum.TO_BILLING_ACCOUNT_NUMBER.name(), toBillingAccount.getExternalRef1());
        otherCreditAndChargeDebit.setCfValue(AccountOperationCFsEnum.OPERATION_REASON.name(), transferBillingAccountDto.getOperationReason());
        accountOperationService.update(otherCreditAndChargeDebit);

        OtherCreditAndCharge otherCreditAndChargeCredit = otherCreditAndChargeService.addOCC(occTransferAccountCredit,
                descTransferTo + " " + fromBillingAccount.getCustomerAccount().getCode(), toBillingAccount.getCustomerAccount(), transferBillingAccountDto.getAmount(), new Date());
        otherCreditAndChargeCredit.setAmountWithoutTax(otherCreditAndChargeCredit.getAmount());
        otherCreditAndChargeCredit.setCfValue(AccountOperationCFsEnum.BILLING_ACCOUNT_NUMBER.name(), toBillingAccount.getExternalRef1());
        otherCreditAndChargeCredit.setCfValue(AccountOperationCFsEnum.FROM_BILLING_ACCOUNT_NUMBER.name(), fromBillingAccount.getExternalRef1());
        otherCreditAndChargeCredit.setCfValue(AccountOperationCFsEnum.TO_BILLING_ACCOUNT_NUMBER.name(), toBillingAccount.getExternalRef1());
        otherCreditAndChargeCredit.setCfValue(AccountOperationCFsEnum.OPERATION_REASON.name(), transferBillingAccountDto.getOperationReason());
        accountOperationService.update(otherCreditAndChargeCredit);
    }

    /**
     * Validate the payment to be reversed
     *
     * @param reverseAccountOperationDto the reverse account operation Dto
     * @throws BusinessException the business exception
     */
    private void validateReversePayment(ReverseAccountOperationDto reverseAccountOperationDto) throws BusinessException {
        if (reverseAccountOperationDto == null || reverseAccountOperationDto.getAccountOperationId() == null) {
            throw new BusinessException("The payment id is required");
        }

        if (StringUtils.isBlank(reverseAccountOperationDto.getOperationReason())) {
            throw new BusinessException("The reverse reason is required");
        }

        AccountOperation accountOperation = accountOperationService.findById(reverseAccountOperationDto.getAccountOperationId());
        if (accountOperation == null) {
            throw new BusinessException("the payment to be reversed " + reverseAccountOperationDto.getAccountOperationId() + " not found");
        }

        if (!"P".equalsIgnoreCase(accountOperation.getType())) {
            throw new BusinessException("the account operation to be reversed is not a payment type");
        }
    }

    /**
     * Process the reverse payment.
     *
     * @param reverseAccountOperationDto the reverse account operation Dto
     * @throws BusinessException the business exception
     */
    private void processReversePayment(ReverseAccountOperationDto reverseAccountOperationDto) throws BusinessException {

        AccountOperation accountOperation = accountOperationService.findById(reverseAccountOperationDto.getAccountOperationId());
        Date dueDate = new Date();
        AccountOperation reversedAccountOperation = createReversedAccountOperation(accountOperation, reverseAccountOperationDto, dueDate);
        //updateReversedPayment(accountOperation, dueDate);

        List<Long> accountOperations = new ArrayList<>();
        accountOperations.add(accountOperation.getId());
        accountOperations.add(reversedAccountOperation.getId());
        try {
            matchingCodeService.matchOperations(accountOperation.getCustomerAccount().getId(), null, accountOperations, null);
        } catch (Exception e) {
            throw new BusinessException(e);
        }
        accountOperation.setMatchingStatus(MatchingStatusEnum.C);
        reversedAccountOperation.setMatchingStatus(MatchingStatusEnum.C);
    }

    /**
     * create reversed account operation
     *
     * @param accountOperation           the account operation
     * @param reverseAccountOperationDto the reverse account operation Dto
     * @param dueDate                    the due date
     * @return the reversed account operation
     * @throws BusinessException the business exception
     */
    private AccountOperation createReversedAccountOperation(AccountOperation accountOperation, ReverseAccountOperationDto reverseAccountOperationDto, Date dueDate) throws BusinessException {
        String occTemplateCode = ApplicationPropertiesEnum.TEMPLATE_REVERSE_PAYMENT_DEBIT.getProperty();
        OCCTemplate occTemplate = occTemplateService.findByCode(occTemplateCode);
        if (occTemplate == null) {
            throw new BusinessException("cannot find OCCTemplate by code : " + occTemplateCode);
        }
        CustomerAccount customerAccount = accountOperation.getCustomerAccount();
        if (customerAccount == null || customerAccount.getStatus() == CustomerAccountStatusEnum.CLOSE) {
            throw new BusinessException("the payment customerAccount is closed");
        }

        OtherCreditAndCharge otherCreditAndCharge = new OtherCreditAndCharge();
        otherCreditAndCharge.setCustomerAccount(customerAccount);
        otherCreditAndCharge.setCode(occTemplate.getCode());
        otherCreditAndCharge.setDescription(occTemplate.getDescription());
        otherCreditAndCharge.setAccountingCode(occTemplate.getAccountingCode());
        otherCreditAndCharge.setAccountCodeClientSide(occTemplate.getAccountCodeClientSide());
        otherCreditAndCharge.setTransactionCategory(occTemplate.getOccCategory());
        otherCreditAndCharge.setDueDate(dueDate);
        otherCreditAndCharge.setTransactionDate(dueDate);
        otherCreditAndCharge.setAmountWithoutTax(accountOperation.getAmount());
        otherCreditAndCharge.setAmount(accountOperation.getAmount());
        otherCreditAndCharge.setUnMatchingAmount(accountOperation.getAmount());
        otherCreditAndCharge.setMatchingStatus(MatchingStatusEnum.O);


        otherCreditAndCharge.setCfValue(AccountOperationCFsEnum.BILLING_ACCOUNT_NUMBER.name(),
                accountOperation.getCfValue(AccountOperationCFsEnum.BILLING_ACCOUNT_NUMBER.name()));
        otherCreditAndCharge.setCfValue(AccountOperationCFsEnum.OPERATION_REASON.name(), reverseAccountOperationDto.getOperationReason());
        customerAccount.getAccountOperations().add(otherCreditAndCharge);
        otherCreditAndChargeService.create(otherCreditAndCharge);
        return otherCreditAndCharge;
    }

    /**
     * Update reversed payment
     *
     * @param accountOperation the account operation
     * @param dueDate          the due date
     * @throws BusinessException the business exception
     */
    private void updateReversedPayment(AccountOperation accountOperation, Date dueDate) throws BusinessException {
        // Update the reversed account operation
        accountOperation.setMatchingAmount(accountOperation.getAmount());
        accountOperation.setUnMatchingAmount(BigDecimal.ZERO);
        accountOperation.setMatchingStatus(MatchingStatusEnum.C);

        MatchingCode matchingCode = new MatchingCode();
        MatchingAmount matchingAmount = new MatchingAmount();
        matchingAmount.setMatchingAmount(accountOperation.getAmount());
        matchingAmount.updateAudit(currentUser);
        matchingAmount.setAccountOperation(accountOperation);
        matchingAmount.setMatchingCode(matchingCode);

        accountOperation.getMatchingAmounts().add(matchingAmount);
        matchingCode.getMatchingAmounts().add(matchingAmount);

        matchingCode.setMatchingAmountDebit(accountOperation.getAmount());
        matchingCode.setMatchingAmountCredit(accountOperation.getAmount());
        matchingCode.setMatchingDate(dueDate);
        matchingCode.setMatchingType(MatchingTypeEnum.A);
        matchingCodeService.create(matchingCode);
    }
}
