package com.eir.api.billing;

import com.eir.commons.enums.customfields.BillingAccountCFsEnum;
import com.eir.service.billing.impl.BillingAccountService;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.api.BaseApi;
import org.meveo.api.commons.ErrorEnum;
import org.meveo.api.commons.Utils;
import org.meveo.api.dto.account.BillingAccountDto;
import org.meveo.api.dto.billing.BillingAccountDebtorStatementDto;
import org.meveo.api.dto.billing.BillingAccountDetailDto;
import org.meveo.api.dto.billing.BillingAccountDetailsResponseDto;
import org.meveo.api.dto.billing.BillingAccountsDto;
import org.meveo.api.dto.billing.CustomerDetailsDto;
import org.meveo.api.dto.billing.SequenceResponseDto;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.commons.utils.NumberUtils;
import org.meveo.commons.utils.ParamBean;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.billing.AccountStatusEnum;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.catalog.RoundingModeEnum;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.model.shared.DateUtils;
import org.meveo.model.shared.Name;
import org.meveo.service.payments.impl.CustomerAccountService;
import org.meveo.service.script.ScriptInstanceService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * The billing account API.
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirBillingAccountApi")
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class BillingAccountApi extends BaseApi {

    @Inject
    private CustomerAccountService customerAccountService;

    @Inject
    private BillingAccountService billingAccountService;

    @Inject
    private org.meveo.api.account.BillingAccountApi BillingAccountApi;

    @Inject
    private ScriptInstanceService scriptInstanceService;

    private static final String UNDERSCORE = "_";

    private static final String PIPE_DELIMITER = "|";

    /**
     * date format.
     */
    public static String DATE_TIME_PATTERN = "yyyyMMdd_HHmm";

    public static String REPORT_DATE_PATTERN = "dd-MMM-yyyy";

    /**
     * The directory of the export.
     */
    private final String EXPORT_DIRECTORY = "reports" + File.separator + "BillingAccount" + File.separator + "DebtorStatement";

    /**
     * File prefix
     */
    private final static String FILE_PREFIX = "Confidential_Reg_CWCI_DEBTOR_TRANS_";


    private static final Object FILE_EXTENSION = ".csv";

    /**
     * Active status
     */
    private static final String ACTIVE_STATUS = "ACTV";

    private static final String EMPTY_STRING = "";

    /**
     * Data Model Enum
     */
    public enum DataModelEnum {
        Balances("Balances",
                "SELECT sum(CASE   WHEN ao4.transaction_category = 'DEBIT' \r\n" +
                        "                THEN ao4.amount \r\n" +
                        "                ELSE (- 1 * ao4.amount) \r\n" +
                        "                END) as \"Balance\"\r\n" +
                        "                FROM ar_account_operation AS ao4 \r\n" +
                        "                where (ao4.cf_values\\:\\:JSON->'BILLING_ACCOUNT_NUMBER'->0 ->>'string') = :billingAccountNumber\r\n" +
                        "                 and ao4.transaction_date <= :fromDate",
                "SELECT ao.transaction_date as \"Transaction Date\",(CASE   \r\n" +
                        "                WHEN ao.code like '%ADJ%' THEN 'Adjustment' \r\n" +
                        "                WHEN ao.code like '%INV%' THEN 'Invoice'\r\n" +
                        "                WHEN ao.code like '%PAY%' THEN 'Payment'\r\n" +
                        "                WHEN ao.code like '%TRS%' THEN 'Transfer'\r\n" +
                        "                ELSE 'Other'\r\n" +
                        "                END) as \"Transaction Type\", (CASE WHEN ao.code like '%INV%' THEN reference ELSE '' END)  as \"Invoice Number\",\r\n" +
                        "            (CASE   WHEN ao.transaction_category = 'DEBIT' \r\n" +
                        "                THEN ao.amount \r\n" +
                        "                ELSE (- 1 * ao.amount) \r\n" +
                        "                END) as \"Amount\", (SELECT sum(CASE   WHEN ao4.transaction_category = 'DEBIT' \r\n" +
                        "                THEN ao4.amount \r\n" +
                        "                ELSE (- 1 * ao4.amount) \r\n" +
                        "                END) as \"Balance\"\r\n" +
                        "                FROM ar_account_operation AS ao4 \r\n" +
                        "                where (ao4.cf_values\\:\\:JSON->'BILLING_ACCOUNT_NUMBER'->0 ->>'string') = :billingAccountNumber\r\n" +
                        "                 and ao4.transaction_date<= ao.transaction_date\r\n" +
                        "                 and ((CASE   WHEN ao4.transaction_date  = ao.transaction_date\r\n" +
                        "                             THEN ( ao4.id <=ao.id) ELSE (1=1)  END )))\r\n" +
                        "                 from ar_account_operation as ao \r\n" +
                        "                where (ao.cf_values\\:\\:JSON->'BILLING_ACCOUNT_NUMBER'->0 ->>'string') = :billingAccountNumber\r\n" +
                        "                 and ao.transaction_date >= :fromDate order by ao.transaction_date,id",
                "SELECT sum(CASE   WHEN ao4.transaction_category = 'DEBIT' \r\n" +
                        "                THEN ao4.amount \r\n" +
                        "                ELSE (- 1 * ao4.amount) \r\n" +
                        "                END) as \"Balance\"\r\n" +
                        "                FROM ar_account_operation AS ao4 \r\n" +
                        "                where (ao4.cf_values\\:\\:JSON->'BILLING_ACCOUNT_NUMBER'->0 ->>'string') = :billingAccountNumber\r\n" +
                        "                 and ao4.transaction_date<= current_date"
        );

        /**
         * The description
         */
        private String description;


        /**
         * The sql header.
         */
        private String sqlHeader;

        /**
         * The sql body.
         */
        private String sqlBody;

        /**
         * The sql trailer.
         */
        private String sqlFooter;


        /**
         * Instantiates a new data model enum.
         *
         * @param description the description
         * @param sqlHeader   the sql header
         * @param sqlBody     the sql body
         * @param sqlFooter   the sql footer
         */
        private DataModelEnum(String description, String sqlHeader, String sqlBody, String sqlFooter) {
            this.description = description;
            this.sqlHeader = sqlHeader;
            this.sqlBody = sqlBody;
            this.sqlFooter = sqlFooter;
        }

        /**
         * Gets the description.
         *
         * @return the description
         */
        public String getDescription() {
            return description;
        }

        public String getSqlHeader() {
            return sqlHeader;
        }

        public String getSqlBody() {
            return sqlBody;
        }

        public String getSqlFooter() {
            return sqlFooter;
        }

    }


    /**
     * Create billing account
     *
     * @param billingAccountDto the billing account Dto
     * @throws BusinessException the business exception
     */
    public void create(BillingAccountDto billingAccountDto) throws BusinessException {
        String sequenceNumber = billingAccountService.getNextSequence();
        billingAccountDto.setExternalRef1(sequenceNumber);
        BillingAccountApi.create(billingAccountDto);
    }

    /**
     * Get a billing account sequence number.
     *
     * @return the billing account sequence number.
     * @throws BusinessException the business exception
     */
    public SequenceResponseDto getSequenceNumber() throws BusinessException {
        String sequenceNumber = billingAccountService.getNextSequence();
        return new SequenceResponseDto(sequenceNumber);
    }

    /**
     * Get billing accounts list by customer account code
     *
     * @param customerAccountCode the customer account code
     * @return the billing accounts list by customer account code
     */

    public BillingAccountDetailsResponseDto listByCustomerAccount(String customerAccountCode) {
        BillingAccountDetailsResponseDto billingAccountDetailsResponse = new BillingAccountDetailsResponseDto();
        // Validates the Request URI received
        if (StringUtils.isBlank(customerAccountCode)) {
            // General Exception or Timeout on backend.
            billingAccountDetailsResponse.setErrorCode(ErrorEnum.GENERIC_API_EXCEPTION);
            return billingAccountDetailsResponse;
        }

        // Find the customer account in OC
        CustomerAccount customerAccount = customerAccountService.findByCode(customerAccountCode);
        if (customerAccount == null) {
            // Invalid request details
            billingAccountDetailsResponse.setErrorCode(ErrorEnum.BAD_REQUEST);
            return billingAccountDetailsResponse;
        }

        // set the customer details found in response.
        billingAccountDetailsResponse.setCustomerDetails(getCustomerDetails(customerAccount));

        // Get only the active Billing Accounts
        List<BillingAccount> listBillingAccounts = findActiveBillingAccounts(customerAccount.getCode());
        if (listBillingAccounts == null || listBillingAccounts.size() == 0) {
            // No data found for request
            billingAccountDetailsResponse.setErrorCode(ErrorEnum.NOT_FOUND);
            return billingAccountDetailsResponse;
        }

        // set the billing account details found in response.
        List<BillingAccountDetailDto> billingAccountDetails = getBillingAccountDetails(listBillingAccounts);
        BillingAccountsDto billingAccounts = new BillingAccountsDto();
        billingAccounts.setBillingAccountDetails(billingAccountDetails);
        billingAccountDetailsResponse.getCustomerDetails().setBillingAccounts(billingAccounts);

        return billingAccountDetailsResponse;
    }

    /**
     * Get customer details.
     *
     * @param customerAccount the customer account
     * @return the customer details
     */
    private CustomerDetailsDto getCustomerDetails(CustomerAccount customerAccount) {
        CustomerDetailsDto customerDetails = new CustomerDetailsDto();
        customerDetails.setCustomerNumber(customerAccount.getCode());
        // customerDetails.customerName = customerAccount.getName() != null ? customerAccount.getName().getFullName() : null;
        customerDetails.setCustomerName(customerAccount.getDescription());
        customerDetails.setCustomerAddressLine1(customerAccount.getAddress() != null ? customerAccount.getAddress().getAddress1() : null);
        customerDetails.setCustomerAddressLine2(customerAccount.getAddress() != null ? customerAccount.getAddress().getAddress2() : null);
        customerDetails.setCustomerAddressLine3(customerAccount.getAddress() != null ? customerAccount.getAddress().getAddress3() : null);
        customerDetails.setCustomerAddressLine4(customerAccount.getAddress() != null ? customerAccount.getAddress().getCity() : null);
        customerDetails.setCustomerAddressLine5(customerAccount.getAddress() != null ? customerAccount.getAddress().getState() : null);
        return customerDetails;
    }

    /**
     * Get active billing accounts
     *
     * @param customerAccountCode the customer account code
     * @return the active billing accounts list
     */
    private List<BillingAccount> findActiveBillingAccounts(String customerAccountCode) {
        Map<String, Object> filters = new HashMap<>();
        filters.put("status", AccountStatusEnum.ACTIVE);
        filters.put("customerAccount.code", customerAccountCode);
        PaginationConfiguration paginationConfiguration = new PaginationConfiguration(filters);
        return billingAccountService.list(paginationConfiguration);
    }

    /**
     * Get billing account details.
     *
     * @param listBillingAccounts the list billing accounts
     * @return the billing account details
     */
    private List<BillingAccountDetailDto> getBillingAccountDetails(List<BillingAccount> listBillingAccounts) {
        List<BillingAccountDetailDto> billingAccountDetails = new ArrayList<>();
        for (BillingAccount billingAccount : listBillingAccounts) {
            if (billingAccount != null) {
                BillingAccountDetailDto billingAccountDetail = new BillingAccountDetailDto();
                billingAccountDetail.setBillingAccountNumber(billingAccount.getExternalRef1());
                // billingAccountDetail.setBillingAccountName(billingAccount.getName() != null ? billingAccount.getName().getFullName() : null);
                billingAccountDetail.setBillingAccountName(billingAccount.getDescription());
                billingAccountDetail.setBillingAccountAddressLine1(billingAccount.getAddress() != null ? billingAccount.getAddress().getAddress1() : null);
                billingAccountDetail.setBillingAccountAddressLine2(billingAccount.getAddress() != null ? billingAccount.getAddress().getAddress2() : null);
                billingAccountDetail.setBillingAccountAddressLine3(billingAccount.getAddress() != null ? billingAccount.getAddress().getAddress3() : null);
                billingAccountDetail.setBillingAccountAddressLine4(billingAccount.getAddress() != null ? billingAccount.getAddress().getCity() : null);
                billingAccountDetail.setBillingAccountAddressLine5(billingAccount.getAddress() != null ? billingAccount.getAddress().getState() : null);
                billingAccountDetail.setBillingAccountServiceType((String) billingAccount.getCfValuesNullSafe().getValue(BillingAccountCFsEnum.SERVICE_TYPE.name()));
                billingAccountDetail.setBillingAccountStatus(ACTIVE_STATUS);
                billingAccountDetail.setBillingAccountCreationDate(DateUtils.formatDateWithPattern(billingAccount.getAuditable().getCreated(), Utils.DATE_DEFAULT_PATTERN));
                billingAccountDetail.setBillingAccountCategory((String) billingAccount.getCfValuesNullSafe().getValue("TAX_CATEGORY"));
                // billingAccountDetail.billingAccountProfile = "Discount Plan. e.g. '001' = no discount";
                billingAccountDetail.setBillingAccountProfile("001");
                billingAccountDetails.add(billingAccountDetail);
            }
        }
        return billingAccountDetails;
    }

    public BillingAccountDebtorStatementDto export(BillingAccountDebtorStatementDto dto) {
        try {
            if (StringUtils.isBlank(dto.getBillingAccountCode()) || StringUtils.isBlank(dto.getDebtorStatementFromDate())) {
                throw new BusinessException("Billing Account Code and Debtor Statement From Date are mandatory !");
            }
            BillingAccount ba = billingAccountService.findByCode(dto.getBillingAccountCode());
            if (ba == null) {
                throw new BusinessException("Billing Account : " + dto.getBillingAccountCode() + " not found !");
            }

            if (StringUtils.isBlank(ba.getExternalRef1())) {
                throw new BusinessException("Billing Account Number (external ref 1) not filled in for the Billing Account : " + dto.getBillingAccountCode() + " !");
            }

            String filePath = getFilename(EXPORT_DIRECTORY, ba.getExternalRef1());
            String reportFileName = ParamBean.getInstance().getChrootDir("").concat(File.separator).concat(filePath);
            exportData(ba, dto.getDebtorStatementFromDate(), DataModelEnum.Balances, reportFileName);
            dto.setFilePath(filePath);
            return dto;
        } catch (Exception e) {
            log.error("Execute export Billing Account Debtor Statement fail", e);
            throw new BusinessException("Execute export Billing Account Debtor Statement fail.");
        }
    }

    private void exportData(BillingAccount ba, Date fromDate, DataModelEnum dataModel, String fileName) throws BusinessException {
        try {
            List<Map<String, Object>> resultList = null;
            String headerBalance = "0.00";
            String footerBalance = "0.00";
            if (!StringUtils.isBlank(ba.getExternalRef1())) {
                try {
                    Object balance = scriptInstanceService.getEntityManager().createNativeQuery(dataModel.getSqlHeader()).setParameter("billingAccountNumber", ba.getExternalRef1())
                            .setParameter("fromDate", fromDate).getSingleResult();
                    if (!StringUtils.isBlank(balance)) {
                        headerBalance = NumberUtils.roundToString(new BigDecimal(String.valueOf(balance)), 2, RoundingModeEnum.NEAREST);
                    }

                    balance = scriptInstanceService.getEntityManager().createNativeQuery(dataModel.getSqlFooter()).setParameter("billingAccountNumber", ba.getExternalRef1()).getSingleResult();
                    if (!StringUtils.isBlank(balance)) {
                        footerBalance = NumberUtils.roundToString(new BigDecimal(String.valueOf(balance)), 2, RoundingModeEnum.NEAREST);
                    }


                    Map<String, Object> parameters = new HashMap<>();
                    parameters.put("billingAccountNumber", ba.getExternalRef1());
                    parameters.put("fromDate", fromDate);
                    resultList = scriptInstanceService.executeNativeSelectQuery(dataModel.getSqlBody(), parameters);
                } catch (Exception e) {
                    log.error("Invalid SQL query : {}", e.getMessage(), e);
                    throw new BusinessException(" Invalid SQL query on export billing account debtor statement : " + e.getMessage(), e);
                }
                writeResult(fileName, ba, headerBalance, footerBalance, resultList, fromDate);
            }

        } catch (BusinessException e) {
            log.error("Export billing account debtor statement fail", ba.getCode(), e);
            throw e;
        }
    }

    /**
     * Write result.
     *
     * @param reportFileName the report file name
     * @param ba             the ba
     * @param headerBalance  the header balance
     * @param footerBalance  the footer balance
     * @param resultList     the result list
     * @param fromDate       the from date
     * @throws BusinessException the business exception
     * @throws IOException
     */
    private void writeResult(String reportFileName, BillingAccount ba, String headerBalance, String footerBalance, List<Map<String, Object>> resultList, Date fromDate) throws BusinessException {
        String reportDir = getReportDirectory();
        File f = new File(reportDir);
        if (!f.exists()) {
            f.mkdirs();
            log.info("Ebill folder created :" + reportDir);
        }
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(reportFileName, false))) {
            writeHeader(bw, ba, headerBalance, fromDate);
            writeBody(bw, resultList);
            writeFooter(bw, footerBalance);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
            throw new BusinessException("Cannot write report to file.");
        }
    }

    /**
     * Write footer.
     *
     * @param bw            the bw
     * @param footerBalance the footer balance
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void writeFooter(BufferedWriter bw, String footerBalance) throws IOException {
        bw.newLine();
        bw.write("Closing Balance as of " + DateUtils.formatDateWithPattern(new Date(), REPORT_DATE_PATTERN) + " : " + footerBalance);

    }

    /**
     * Write body.
     *
     * @param bw         the bw
     * @param resultList the result list
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void writeBody(BufferedWriter bw, List<Map<String, Object>> resultList) throws IOException {
        if (resultList != null && resultList.size() > 0) {
            String line = EMPTY_STRING;
            Iterator ite;
            Map<String, Object> firstRow = resultList.get(0);
            ite = firstRow.keySet().iterator();
            while (ite.hasNext()) {
                line = line.concat(ite.next() + "|");
            }
            line = line.substring(0, line.length() - 1);
            bw.newLine();
            bw.write(line);
            String key;
            String value;
            for (Map<String, Object> row : resultList) {
                bw.newLine();
                line = EMPTY_STRING;
                ite = row.keySet().iterator();
                while (ite.hasNext()) {
                    key = String.valueOf(ite.next());
                    value = String.valueOf(row.get(key));
                    if (key.toUpperCase().contains("DATE")) {
                        value = DateUtils.formatDateWithPattern((Date) row.get(key), REPORT_DATE_PATTERN);
                    } else if ("Balance".equals(key)) { //value.matches("^\\d+\\.\\d+") || value.matches("^-\\d+\\.\\d+")
                        value = NumberUtils.roundToString(new BigDecimal(value), 2, RoundingModeEnum.NEAREST);
                    }
                    line = line.concat(value).concat(PIPE_DELIMITER);
                }
                line = line.substring(0, line.length() - 1);
                bw.write(line);
            }
        }


    }

    /**
     * Write header.
     * Billing Account Name|Billing Account Number|Opening Balance as of "input date"
     *
     * @param bw            the bw
     * @param ba            the ba
     * @param headerBalance the header balance
     * @param fromDate      the from date
     * @throws IOException
     */
    private void writeHeader(BufferedWriter bw, BillingAccount ba, String headerBalance, Date fromDate) throws IOException {
        String contratName = ba.getDescription();
        if (StringUtils.isBlank(contratName)) {
            Name name = ba.getName();
            if (name == null) {
                contratName = EMPTY_STRING;
            } else {
                contratName = (name.getFirstName() != null ? name.getFirstName() + " " : EMPTY_STRING) + (name.getLastName() != null ? name.getLastName() : EMPTY_STRING);
            }
        }
        String baNumber = StringUtils.isNotBlank(ba.getExternalRef1()) ? ba.getExternalRef1() : ba.getCode();
        bw.write(contratName + PIPE_DELIMITER + baNumber + PIPE_DELIMITER + "Opening Balance as of " + DateUtils.formatDateWithPattern(fromDate, REPORT_DATE_PATTERN) + " : " + headerBalance);
    }


    /**
     * Gets the short filename.
     *
     * @param billingAccountNumber the billing account number
     * @return the short filename
     */
    private String getShortFilename(String billingAccountNumber) {
        //Confidential_Reg_CWCI_DEBTOR_TRANS_BillingAccountNumber_YYYYMMDD_HHMM
        StringBuffer shortFilename = new StringBuffer();
        shortFilename.append(FILE_PREFIX).append(billingAccountNumber).append(UNDERSCORE).append(DateUtils.formatDateWithPattern(new Date(), DATE_TIME_PATTERN))
                .append(FILE_EXTENSION);
        return shortFilename.toString();
    }

    private String getReportDirectory() {
        StringBuilder reportDir = new StringBuilder(ParamBean.getInstance().getChrootDir(""));
        reportDir.append(File.separator + EXPORT_DIRECTORY);
        return reportDir.toString();
    }

    /**
     * Gets the filename.
     *
     * @param reportDir            the report dir
     * @param billingAccountNumber the billing account number
     * @return the filename
     */
    private String getFilename(String reportDir, String billingAccountNumber) {
        StringBuffer filename = new StringBuffer();
        filename.append(reportDir).append(File.separator).append(getShortFilename(billingAccountNumber));
        return filename.toString();
    }
}
