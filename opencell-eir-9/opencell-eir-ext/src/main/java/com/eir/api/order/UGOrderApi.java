package com.eir.api.order;

import com.eir.commons.enums.ApplicationPropertiesEnum;
import com.eir.commons.enums.ChargeTemplateEnum;
import com.eir.commons.enums.OrderEmitterEnum;
import com.eir.commons.enums.OrderLineAttributeNamesEnum;
import com.eir.commons.enums.ServiceTypeEnum;
import com.eir.commons.enums.TerminationReasonEnum;
import com.eir.commons.enums.customfields.BillingAccountCFsEnum;
import com.eir.commons.enums.customfields.MigrationCFEnum;
import com.eir.commons.enums.customfields.OfferTemplateCFsEnum;
import com.eir.commons.enums.customfields.ProviderCFsEnum;
import com.eir.commons.enums.customfields.ServiceInstanceCFsEnum;
import com.eir.commons.enums.customfields.ServiceInstanceIdentifierAttributesCFsEnum;
import com.eir.commons.enums.customfields.ServiceParametersCFEnum;
import com.eir.commons.enums.customfields.ServiceTemplateCFsEnum;
import com.eir.commons.enums.customfields.SubscriptionCFsEnum;
import com.eir.commons.enums.customtables.EndPointCustomTableEnum;
import org.apache.commons.lang3.SerializationUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.api.commons.ErrorEnum;
import org.meveo.api.commons.Utils;
import org.meveo.api.dto.account.AccessDto;
import org.meveo.api.dto.order.AttributeDto;
import org.meveo.api.dto.order.OrderDto;
import org.meveo.api.dto.order.OrderLineDto;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.DatePeriod;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.ChargeApplicationModeEnum;
import org.meveo.model.billing.InstanceStatusEnum;
import org.meveo.model.billing.OneShotChargeInstance;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.SubscriptionStatusEnum;
import org.meveo.model.billing.SubscriptionTerminationReason;
import org.meveo.model.billing.UserAccount;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.OneShotChargeTemplate;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.crm.custom.CustomFieldValue;
import org.meveo.model.mediation.Access;
import org.meveo.model.order.OrderStatusEnum;
import org.meveo.model.shared.DateUtils;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;


/**
 * The UG order API.
 *
 * @author Mohammed Amine TAZI
 */
@Stateless
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class UGOrderApi extends BaseOrderApi {

    @EJB
    UGOrderApi ugOrderApi;

    /**
     * Check data discrepancies.
     *
     * @param orderDto the order DTO
     */
    private void checkDataDiscrepancies(OrderDto orderDto) {
        // PO2-459
        for (OrderLineDto orderLineDto : orderDto.getOrderLines()) {

            // 2- The UAN on the bill item should be the same as the UAN against the
            // subscriber, Except CU orders
            if (orderLineDto.getUan() != null && !BillActionEnum.CU.name().equals(orderLineDto.getBillAction())
                    && !BillActionEnum.P.name().equalsIgnoreCase(orderLineDto.getOriginalOrderAction())) {
                Subscription oldSubscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
                if (oldSubscription != null && oldSubscription.getStatus() == SubscriptionStatusEnum.ACTIVE
                        && oldSubscription.getCfValuesNullSafe().getValue(SubscriptionCFsEnum.UAN.name()) != null
                        && !orderLineDto.getUan().equals(oldSubscription.getCfValuesNullSafe().getValue(SubscriptionCFsEnum.UAN.name()))) {
                    log.debug("oldSubscription :" + oldSubscription.getCode() + " UAN: "
                            + oldSubscription.getCfValuesNullSafe().getValue(SubscriptionCFsEnum.UAN.name()) + " New UAN : "
                            + orderLineDto.getUan());
                    log.debug("orderLineDto " + orderLineDto.getDetails());
                    setError(orderDto, ErrorEnum.INVALID_DATA,
                            "The UAN provided in the bill item is not the same as the UAN stored against the subscription "
                                    + oldSubscription.getCode());
                    return;
                }
            }

            // START_RANGE an END_RANGE attributes exist on bill items where the bill code
            // is not for an MSN or DDI service
            if (orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_START.getLabel()) != null) {
                ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
                if (serviceTemplate == null) {
                    setError(orderDto, ErrorEnum.INVALID_DATA, "The service " + orderLineDto.getBillCode() + " is not found");
                    return;
                }
                Object multipleInstanceUniqueAttributes = serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MULTIPLE_INSTANCE_UNIQUE_ATTRIBUTES.name());
                if (multipleInstanceUniqueAttributes == null || !((String) multipleInstanceUniqueAttributes).contains("RANGE_START")) {
                    orderDto.setDataDiscrepancies(orderDto.getDataDiscrepancies() + "RANGE_START or RANGE_END attribute exists " +
                            "on bill items where the bill code is not for a multiple instance with ranges. \n");
                } else if (!BillActionEnum.CN.name().equals(orderLineDto.getBillAction()) && !BillActionEnum.C.name().equals(orderLineDto.getBillAction())
                        && orderLineDto.isMain() && !isValidRanges(orderDto, orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_START.getLabel()),
                        orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_END.getLabel()), orderLineDto.getOriginalSubscriptionCode(), orderLineDto.getBillEffectDate())) {
                    setError(orderDto, ErrorEnum.INVALID_DATA,
                            "RANGE_START or RANGE_END already exist or interfers with existing subscription");
                    return;
                }
            }
        }
    }

    /**
     * Initialize the order Dto
     *
     * @param orderDto the order DTO
     */
    private void initOrder(OrderDto orderDto) {
        // Store original xml order
        setOriginalXmlOrder(orderDto);

        for (OrderLineDto orderLineDto : orderDto.getOrderLines()) {
            // Set subscription codes
            orderDto.getSubscriptionCodes().add(orderLineDto.getSubscriptionCode());
            if (!StringUtils.isBlank(orderLineDto.getRefSubscriptionCode())) {
                orderDto.getRefSubscriptionCodes().add(orderLineDto.getRefSubscriptionCode());
            }

            // Set service type for each provide or cease order line (needed during order lines sort)
            if (Arrays.asList(BillActionEnum.P.name(), BillActionEnum.C.name()).contains(orderLineDto.getBillAction())) {
                ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
                if (serviceTemplate == null && !StringUtils.isBlank(orderLineDto.getMainServiceBillCode())) {
                    ServiceTemplate serviceTemplateMain = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getMainServiceBillCode());
                    if (serviceTemplateMain != null) {
                        String productSetCode = getProductSetCode(orderDto, serviceTemplateMain);
                        if (!StringUtils.isBlank(productSetCode)) {
                            String billCode = productSetCode + "_" + orderLineDto.getBillCode();
                            serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, billCode);
                            if (serviceTemplate != null) {
                                orderLineDto.setOriginalBillCode(orderLineDto.getBillCode());
                                orderLineDto.setBillCode(billCode);
                            }
                        }
                    }
                }
                if (serviceTemplate != null) {
                    orderLineDto.setServiceType((String) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.SERVICE_TYPE.name()));
                }
            }
        }

        // Detect change or transfer
        detectChangeOrTransfer(orderDto);
    }

    /**
     * Get change or transfer order lines
     *
     * @param orderDto the order DTO
     * @return the change or transfer order lines
     */
    private List<OrderLineDto> getChangeOrTransferOrderLines(OrderDto orderDto) {
        List<OrderLineDto> provideOrderLines = new ArrayList();
        List<OrderLineDto> ceaseOrderLines = new ArrayList();
        for (OrderLineDto orderLineDto : orderDto.getOrderLines()) {
            if (!Arrays.asList(BillActionEnum.P.name(), BillActionEnum.C.name()).contains(orderLineDto.getBillAction()) ||
                    (StringUtils.isBlank(orderLineDto.getSubscriptionCode()) && StringUtils.isBlank(orderLineDto.getXRefSubscriptionCode()))) {
                continue;
            }
            if (BillActionEnum.P.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
                provideOrderLines.add(orderLineDto);
            } else if (BillActionEnum.C.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
                ceaseOrderLines.add(orderLineDto);
            }
        }
        List<OrderLineDto> orderLines = new ArrayList();
        if (!provideOrderLines.isEmpty() && !ceaseOrderLines.isEmpty()) {
            (orderLines = new ArrayList<>(provideOrderLines)).addAll(ceaseOrderLines);
            for (OrderLineDto orderLineDto : orderLines) {
                orderLineDto.setOriginalSubscriptionCode(orderLineDto.getSubscriptionCode());
                orderLineDto.setSubscriptionCode(getSubscriptionCodeWithPrefix(orderDto, orderLineDto));
            }
        }
        return orderLines;
    }

    /**
     * Get alpha Ref order line Dto
     *
     * @param orderLines order lines list
     * @return the Ref order line Dto
     */
    private OrderLineDto getAlphaRefOrderLineDto(List<OrderLineDto> orderLines) {
        if (!orderLines.isEmpty()) {
            orderLines.sort(Comparator.comparing(OrderLineDto::getSubscriptionCode));
            return orderLines.get(0);
        }
        return null;
    }

    /**
     * Get numeric Ref order line Dto
     *
     * @param orderLines order lines list
     * @return the Ref order line Dto
     */
    private OrderLineDto getNumericRefOrderLineDto(List<OrderLineDto> orderLines) {
        if (!orderLines.isEmpty()) {
            orderLines.sort(Comparator.comparingInt(e -> Integer.parseInt(e.getSubscriptionCode().replace("-", ""))));
            return orderLines.get(0);
        }
        return null;
    }

    /**
     * Get Ref order line Dto
     *
     * @param orderLineDto the order line Dto
     * @param orderLines   the order lines list
     * @return the Ref order line Dto
     */
    private OrderLineDto getRefOrderLineDto(OrderLineDto orderLineDto, List<OrderLineDto> orderLines) {
        List<OrderLineDto> numericMainOrderLines = new ArrayList();
        List<OrderLineDto> alphaMainOrderLines = new ArrayList();
        List<OrderLineDto> numericAncillaryOrderLines = new ArrayList();
        List<OrderLineDto> alphaAncillaryOrderLines = new ArrayList();
        for (OrderLineDto refOrderLineDto : orderLines) {
            //when the same Service Id exists in the 'Service Id' or 'Xref_Service_Id' tags on Provide and Cease bill items
            if (refOrderLineDto.getOriginalSubscriptionCode() != null &&
                    (refOrderLineDto.getOriginalSubscriptionCode().equalsIgnoreCase(orderLineDto.getOriginalSubscriptionCode()) ||
                            refOrderLineDto.getOriginalSubscriptionCode().equalsIgnoreCase(orderLineDto.getXRefSubscriptionCode())) &&
                    !refOrderLineDto.getBillAction().equalsIgnoreCase(orderLineDto.getBillAction())) {

                boolean isNumericRefOrderLine = Utils.isInteger(refOrderLineDto.getSubscriptionCode().replace("-", ""));

                if (refOrderLineDto.isMain()) {
                    if (isNumericRefOrderLine) {
                        numericMainOrderLines.add(refOrderLineDto);
                    } else {
                        alphaMainOrderLines.add(refOrderLineDto);
                    }
                } else if (!orderLineDto.isMain()) {
                    if (isNumericRefOrderLine) {
                        numericAncillaryOrderLines.add(refOrderLineDto);
                    } else {
                        alphaAncillaryOrderLines.add(refOrderLineDto);
                    }
                }
            }
        }

        if (numericMainOrderLines.isEmpty() && alphaMainOrderLines.isEmpty() && numericAncillaryOrderLines.isEmpty() && alphaAncillaryOrderLines.isEmpty()) {
            return null;
        }

        boolean isNumericOrderLine = Utils.isInteger(orderLineDto.getSubscriptionCode().replace("-", ""));

        OrderLineDto refOrderLineDto = null;
        if (orderLineDto.isMain()) {
            if (isNumericOrderLine) {
                refOrderLineDto = getNumericRefOrderLineDto(numericMainOrderLines);
                if (refOrderLineDto == null) {
                    refOrderLineDto = getAlphaRefOrderLineDto(alphaMainOrderLines);
                }
            } else {
                refOrderLineDto = getAlphaRefOrderLineDto(alphaMainOrderLines);
                if (refOrderLineDto == null) {
                    refOrderLineDto = getNumericRefOrderLineDto(numericMainOrderLines);
                }
            }
        } else {
            if (isNumericOrderLine) {
                refOrderLineDto = getNumericRefOrderLineDto(numericAncillaryOrderLines);
                if (refOrderLineDto == null) {
                    refOrderLineDto = getNumericRefOrderLineDto(numericMainOrderLines);
                }
                if (refOrderLineDto == null) {
                    refOrderLineDto = getAlphaRefOrderLineDto(alphaAncillaryOrderLines);
                }
                if (refOrderLineDto == null) {
                    refOrderLineDto = getAlphaRefOrderLineDto(alphaMainOrderLines);
                }
            } else {
                refOrderLineDto = getAlphaRefOrderLineDto(alphaAncillaryOrderLines);
                if (refOrderLineDto == null) {
                    refOrderLineDto = getAlphaRefOrderLineDto(alphaMainOrderLines);
                }
                if (refOrderLineDto == null) {
                    refOrderLineDto = getNumericRefOrderLineDto(numericAncillaryOrderLines);
                }
                if (refOrderLineDto == null) {
                    refOrderLineDto = getNumericRefOrderLineDto(numericMainOrderLines);
                }
            }
        }
        return refOrderLineDto;
    }

    /**
     * Detect change or transfer
     *
     * @param orderDto the order Dto
     */
    private void detectChangeOrTransfer(OrderDto orderDto) {
        List<OrderLineDto> orderLines = getChangeOrTransferOrderLines(orderDto);

        if (orderLines.isEmpty()) {
            return;
        }

        // Setting the link between order items
        for (OrderLineDto orderLineDto : orderLines) {
            OrderLineDto refOrderLineDto = getRefOrderLineDto(orderLineDto, new ArrayList(orderLines));
            if (refOrderLineDto == null && !StringUtils.isBlank(orderLineDto.getXRefSubscriptionCode())) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "The bill item " + orderLineDto.getBillCode() + " with bill action " + orderLineDto.getBillAction() +
                        " and with service id " + orderLineDto.getOriginalSubscriptionCode() + " does not match any other bill item for transfer or change");
                return;
            }
            orderLineDto.setRefOrderLine(refOrderLineDto);
        }

        // Change the action of change and transfer order items
        for (OrderLineDto orderLineDto : orderLines) {
            if (orderLineDto.getRefOrderLine() == null) {
                continue;
            }
            // Change case
            if (orderLineDto.getOperatorId().equalsIgnoreCase(orderLineDto.getRefOrderLine().getOperatorId())) {
                if (BillActionEnum.C.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
                    orderLineDto.setBillAction(BillActionEnum.COS.name());
                } else if (BillActionEnum.P.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
                    orderLineDto.setBillAction(BillActionEnum.CNS.name());
                }
            } else {
                // Transfer case
                if (BillActionEnum.C.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
                    orderLineDto.setBillAction(BillActionEnum.TOS.name());
                } else if (BillActionEnum.P.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
                    orderLineDto.setBillAction(BillActionEnum.TNS.name());
                }
            }
        }
    }

    /**
     * Change class of service.
     *
     * @param orderDto     the order DTO
     * @param orderLineDto the order line DTO
     * @throws BusinessException the business exception
     */
    @SuppressWarnings("unchecked")
    private void changeClassOfService(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        if (subscription == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA,
                    "The subscription " + orderLineDto.getSubscriptionCode() + " is not found");
            return;
        }

        String classOfService = null;
        String af = null;
        String ef = null;
        final String classOfServiceString = "CLASS_OF_SERVICE";
        final String afString = "AF";
        final String efString = "EF";
        // get class of service attributes for current order line
        for (AttributeDto AttributeDto : orderLineDto.getAttributes()) {
            if (classOfServiceString.equalsIgnoreCase(AttributeDto.getCode()))
                classOfService = AttributeDto.getValue();
            if (afString.equalsIgnoreCase(AttributeDto.getCode()))
                af = AttributeDto.getValue();
            if (efString.equalsIgnoreCase(AttributeDto.getCode()))
                ef = AttributeDto.getValue();
        }

        // updating Class of service, EF and AF
        String oldClassOfService = null, oldAF = null, oldEF = null;
        String oldCharge = null, newCharge = null;
        for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
            if (serviceInstance.getStatus().equals(InstanceStatusEnum.ACTIVE)
                    && orderLineDto.getBillCode().equalsIgnoreCase(serviceInstance.getCode())) {

                CustomFieldValue lastCfValue = utilService.getLastCfVersion(serviceInstance, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(),
                        DateUtils.truncateTime(orderLineDto.getBillEffectDate()), true);
                Map<String, String> serviceParametersCF = getServiceParameters(serviceInstance, lastCfValue);
                oldClassOfService = serviceParametersCF.get(ServiceParametersCFEnum.CLASS_OF_SERVICE.getLabel());
                oldAF = serviceParametersCF.get(ServiceParametersCFEnum.AF.getLabel());
                oldEF = serviceParametersCF.get(ServiceParametersCFEnum.EF.getLabel());

                updateServiceParametersCF(orderDto, orderLineDto, serviceInstance, true);
                serviceInstance = serviceInstanceService.update(serviceInstance);
                reratingService.rerate(serviceInstance, orderLineDto.getBillEffectDate(), true);

                OfferTemplate offerTemplate = orderService.findEntityByIdOrCode(offerTemplateService, orderDto, orderLineDto.getOfferTemplateCode());
                if (offerTemplate != null && offerTemplate.getCfValues() != null) {
                    Map<String, String> classOfServiceTarifTable = (Map<String, String>) offerTemplate
                            .getCfValue(OfferTemplateCFsEnum.CLASS_OF_SERVICE.name(), orderLineDto.getBillEffectDate());
                    if (classOfServiceTarifTable != null) {
                        if (oldClassOfService != null && oldAF != null && oldEF != null) {
                            oldCharge = classOfServiceTarifTable
                                    .get(orderLineDto.getBillCode() + "|" + oldClassOfService + "|" + oldAF + "|" + oldEF);
                        }
                        newCharge = classOfServiceTarifTable
                                .get(orderLineDto.getBillCode() + "|" + classOfService + "|" + af + "|" + ef);
                        log.debug("####### Old charge " + oldCharge);
                        log.debug("####### new charge " + newCharge);
                        if (oldCharge == newCharge) { // charges do not need to be (re)calculated. No action is
                            // necessary
                            return;
                        }
                        OneShotChargeTemplate chargeTemplate = orderService.findEntityByIdOrCode(oneShotChargeTemplateService, orderDto, ChargeTemplateEnum.MISC_CHARGE.name());
                        if (chargeTemplate == null) {
                            setError(orderDto, ErrorEnum.INVALID_DATA,
                                    "The one-shot charge " + orderLineDto.getBillCode() + " is not found");
                            return;
                        }
                        if (oldCharge != null) {
                            applyMiscOneShotCharge(orderLineDto, orderDto, subscription, new BigDecimal(oldCharge));
                            log.debug("Old charge applied!");
                        }
                        if (newCharge != null) {
                            applyMiscOneShotCharge(orderLineDto, orderDto, subscription, new BigDecimal(newCharge));
                            log.debug("New charge applied!");
                        }

                    }
                }
            }
        }

        if (orderDto.getErrorCode() == null) {
            UpdateEndpointAddresses(orderDto, orderLineDto);
        }

    }

    /**
     * Change UAN
     *
     * @param orderDto     Order
     * @param orderLineDto Order Line
     * @throws BusinessException the business exception
     */
    private void changeUAN(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        if (subscription != null) {
            if (!validateServiceInstance(orderDto, orderLineDto, new String[]{BillActionEnum.CU.name()})) {
                return;
            }
            boolean isSubscriptionUpdated = false;
            if (subscription.getCfValuesNullSafe() != null) {
                Map<String, List<CustomFieldValue>> valuesByCode = subscription.getCfValuesNullSafe().getValuesByCode();
                if (valuesByCode != null && valuesByCode.get(SubscriptionCFsEnum.UAN.name()) != null) {
                    List<CustomFieldValue> customFieldValueList = valuesByCode.get(SubscriptionCFsEnum.UAN.name());
                    for (CustomFieldValue customFieldValue : customFieldValueList) {
                        if (customFieldValue != null && customFieldValue.getPeriod() != null
                                && customFieldValue.getPeriod().getTo() == null) {
                            customFieldValue.getPeriod().setTo(Utils.addSecondsToDate(orderLineDto.getBillEffectDate(), -1));
                            isSubscriptionUpdated = true;
                        }
                    }
                }
            }
            if (orderLineDto.getMasterAccountNumber()
                    .equals(subscription.getUserAccount().getBillingAccount().getCode())) {
                DatePeriod datePeriod = new DatePeriod(orderLineDto.getBillEffectDate(), null);
                subscription.getCfValuesNullSafe().setValue(SubscriptionCFsEnum.UAN.name(), datePeriod, null, orderLineDto.getUan());
                isSubscriptionUpdated = true;
            }
            if (isSubscriptionUpdated) {
                subscriptionService.update(subscription);
            }
        }
    }

    /**
     * Change Number
     *
     * @param orderDto     Current Order
     * @param orderLineDto Order line
     * @throws BusinessException the business exception
     */
    @SuppressWarnings("unchecked")
    private void changeNumber(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        if ("ALL".equalsIgnoreCase(orderLineDto.getBillCode())) {
            log.debug("Changing Tel Number ==> ALL ...");

            if (orderLineDto.getRefSubscriptionCode() == null || "".equals(orderLineDto.getRefSubscriptionCode())) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "Missing Ref_Service_Id");
                return;
            }
            List<Subscription> refSubscriptions = subscriptionService.findActiveSubscriptionsByCodeWithoutPrefix(orderLineDto.getRefSubscriptionCode());
            if (refSubscriptions != null && !refSubscriptions.isEmpty()) {
                setError(orderDto, ErrorEnum.INVALID_DATA,
                        "refServiceId is already assigned to the subscription " + refSubscriptions.get(0).getCode());
                return;
            }
            List<Subscription> subscriptionsToModify = subscriptionService.findNonTerminatedSubscriptionsByCodeWithoutPrefix(
                    orderLineDto.getSubscriptionCode());
            if (subscriptionsToModify == null || subscriptionsToModify.size() == 0) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "The subscription " + orderLineDto.getSubscriptionCode() + " is not found");
                return;
            }

            for (Subscription subscriptionToModify : subscriptionsToModify) {
                Map<String, Object> cfMap = new HashMap<>();
                String telNumber = orderLineDto.getRefSubscriptionCode() != null ? orderLineDto.getRefSubscriptionCode() : orderLineDto.getXRefSubscriptionCode();
                telNumber = subscriptionToModify.getCode().substring(0, subscriptionToModify.getCode().indexOf("_") + 1) + telNumber;
                cfMap.put(SubscriptionCFsEnum.CHANGE_TEL.name(),
                        DateUtils.formatDateWithPattern(new Date(), Utils.DATE_TIME_PATTERN) + "|"
                                + orderLineDto.getBillEffectDate() + "|" + subscriptionToModify.getCode() + "|"
                                + telNumber
                                + "|" + "DONE");
                DatePeriod datePeriod = new DatePeriod(orderLineDto.getBillEffectDate(), null);
                subscriptionToModify.getCfValuesNullSafe().setValue(SubscriptionCFsEnum.CHANGE_TEL.name(), datePeriod, 0, cfMap);
                orderService.updateEntityCode(subscriptionService, subscriptionToModify, orderDto,
                        subscriptionToModify.getCode().replace(orderLineDto.getOriginalSubscriptionCode(), orderLineDto.getRefSubscriptionCode()));
                // Update user account
                UserAccount userAccount = subscriptionToModify.getUserAccount();
                orderService.updateEntityCode(userAccountService, userAccount, orderDto,
                        userAccount.getCode().replace(orderLineDto.getOriginalSubscriptionCode(), orderLineDto.getRefSubscriptionCode()));
                for (Access accessPoint : subscriptionToModify.getAccessPoints()) {
                    if (accessPoint.getAccessUserId().endsWith(orderLineDto.getSubscriptionCode()) && accessPoint.getEndDate() == null) {
                        // accessPoint
                        // .setAccessUserId(accessPoint.getAccessUserId().replace(oldTelNumber,
                        // newTelNumber));
                        accessPoint.setEndDate(Utils.addMinutesToDate(orderLineDto.getBillEffectDate(), -1));
                        accessService.update(accessPoint);
                        // add access point
                        Access newAccessPoint = new Access();
                        newAccessPoint.setAccessUserId(accessPoint.getAccessUserId()
                                .replace(orderLineDto.getOriginalSubscriptionCode(), orderLineDto.getRefSubscriptionCode()));
                        newAccessPoint
                                .setStartDate(orderLineDto.getBillEffectDate());
                        newAccessPoint.setSubscription(subscriptionToModify);
                        accessService.update(newAccessPoint);
                    }
                }
                utilService.setServiceIdCF(subscriptionToModify);
                subscriptionService.update(subscriptionToModify);
            }
            applyOneShotCharge(orderDto, orderLineDto, orderLineDto.getRefSubscriptionCode());
        } else {

            if (!isValidRanges(orderDto, orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_NEW_RANGE_START.getLabel()),
                    orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_NEW_RANGE_END.getLabel()),
                    orderLineDto.getOriginalSubscriptionCode(), orderLineDto.getBillEffectDate())) {
                return;
            }

            // updating ranges for adequate Service_Ids
            List<Subscription> subscriptions = subscriptionService.findNonTerminatedSubscriptionsByCodeWithoutPrefix(orderLineDto.getSubscriptionCode());
            for (Subscription subscription : subscriptions) {
                ServiceInstance serviceInstance = getAncillaryServiceInstance(orderDto, orderLineDto, subscription);
                if (orderDto.getErrorCode() != null) {
                    return;
                }
                if (serviceInstance == null) {
                    continue;
                }


                updateServiceParametersCF(orderDto, orderLineDto, serviceInstance, true);
                updateRangesCfs(orderDto, orderLineDto, serviceInstance);
                serviceInstanceService.update(serviceInstance);

                String rangeStart = !StringUtils.isBlank(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_NEW_RANGE_START.getLabel()))
                        ? orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_NEW_RANGE_START.getLabel())
                        : orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_START.getLabel());
                String rangeEnd = !StringUtils.isBlank(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_NEW_RANGE_END.getLabel()))
                        ? orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_NEW_RANGE_END.getLabel())
                        : orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_END.getLabel());

                if (!StringUtils.isBlank(rangeStart) && StringUtils.isBlank(rangeEnd)) {
                    // add access point
                    Access newAccessPoint = new Access();
                    newAccessPoint.setAccessUserId(rangeStart);
                    newAccessPoint.setStartDate(orderLineDto.getBillEffectDate());
                    newAccessPoint.setSubscription(subscription);
                    accessService.update(newAccessPoint);
                }

                return;
            }
            setError(orderDto, ErrorEnum.INVALID_DATA, "Ancillary service with right Range Numbers not found");


        }
    }

    /**
     * Check if new ranges are valid against the existing ones
     *
     * @param rangeStart               the range start
     * @param rangeEnd                 the range end
     * @param originalSubscriptionCode the original subscriptionC cde
     * @param billEffectDate           the bill effect date
     * @return true if new ranges are valid against the existing ones
     */
    private boolean isValidRanges(OrderDto orderDto, String rangeStart, String rangeEnd, String originalSubscriptionCode, Date billEffectDate) {
        if (!StringUtils.isBlank(rangeStart) || !StringUtils.isBlank(rangeEnd)) {

            if (!isValidOnUsedRanges(orderDto, rangeStart, rangeEnd)) {
                return false;
            }
            // check if new range not already taken
            List<Subscription> subs = subscriptionService.findNonTerminatedSubscriptionsByCodeWithoutPrefix(rangeStart);

            if (subs != null && !subs.isEmpty() && !originalSubscriptionCode.equalsIgnoreCase(rangeStart)) {
                log.debug(rangeStart + " already exist or interferes with existing subscription" + subs.get(0));
                setError(orderDto, ErrorEnum.INVALID_DATA, "RANGE_START or RANGE_END already exists or interferes with existing subscription" + subs.get(0));
                return false;

            }

            if (!StringUtils.isBlank(rangeStart) && StringUtils.isBlank(rangeEnd)) {
                // check access point
                List<Access> accessPoints = accessService.getAccess(rangeStart, billEffectDate);

                if (accessPoints != null && !accessPoints.isEmpty()) {
                    setError(orderDto, ErrorEnum.INVALID_DATA, "the access point corresponds to RANGE_START / NEW_RANGE_START value match with another access point in the system");
                    return false;
                }
            }
        }

        return true;

    }

    /**
     * Create new access point
     *
     * @param orderLineDto        the order line Dto
     * @param subscription        the subscription
     * @param subscriptionCode    the subscription code
     * @param refSubscriptionCode the ref subscription code
     * @throws BusinessException the business exception
     */
    private void createNewAccessPoint(OrderLineDto orderLineDto, Subscription subscription, String subscriptionCode, String refSubscriptionCode) throws BusinessException {
        // update the end date of last access point
        List<Access> newAccessPoints = new ArrayList<>();
        Access lastAccessPoint = null;
        for (Access accessPoint : subscription.getAccessPoints()) {
            if (accessPoint.getAccessUserId().endsWith(subscriptionCode) && (lastAccessPoint == null || accessPoint.getId() > lastAccessPoint.getId())) {
                lastAccessPoint = accessPoint;
            }
        }
        if (lastAccessPoint != null) {
            lastAccessPoint.setEndDate(DateUtils.setDateToEndOfDay(DateUtils.addDaysToDate(orderLineDto.getBillEffectDate(), -1)));
            accessService.update(lastAccessPoint);
        }

        // add access point
        Access newAccessPoint = new Access();
        newAccessPoint.setAccessUserId(subscription.getCode().replace(subscriptionCode, refSubscriptionCode));
        newAccessPoint.setStartDate(DateUtils.setDateToStartOfDay(orderLineDto.getBillEffectDate()));
        newAccessPoint.setSubscription(subscription);
        newAccessPoint.updateAudit(currentUser);
        newAccessPoints.add(newAccessPoint);
        accessService.create(newAccessPoint);
        subscription.getAccessPoints().addAll(newAccessPoints);
        subscriptionService.update(subscription);
    }

    /**
     * Swap Number
     *
     * @param orderDto     Current Order
     * @param orderLineDto Order line
     * @throws BusinessException the business exception
     */
    private void swapNumber(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        if ("ALL".equalsIgnoreCase(orderLineDto.getBillCode())) {
            log.debug("Swapping Tel Number ==> ALL ...");
            if (orderLineDto.getRefSubscriptionCode() == null || "".equals(orderLineDto.getRefSubscriptionCode())) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "Missing Ref_Service_Id");
                return;
            }
            List<Subscription> refSubscriptionsToSwap = subscriptionService.findNonTerminatedSubscriptionsByCodeWithoutPrefix(
                    orderLineDto.getRefSubscriptionCode());
            log.debug("####refSubscriptionsToSwap" + refSubscriptionsToSwap);

            if (refSubscriptionsToSwap != null) {
                for (Subscription refSubscriptionToSwap : refSubscriptionsToSwap) {
                    if (refSubscriptionToSwap.getStatus() != SubscriptionStatusEnum.ACTIVE) {
                        setError(orderDto, ErrorEnum.INVALID_DATA,
                                "The subscription " + refSubscriptionToSwap.getCode() + " is not active");
                        return;
                    }

                }
            }

            if (refSubscriptionsToSwap == null || refSubscriptionsToSwap.isEmpty()) {
                setError(orderDto, ErrorEnum.INVALID_DATA,
                        "Any subscription is found for " + orderLineDto.getRefSubscriptionCode());
                return;
            }

            List<Subscription> subscriptionsToSwap = subscriptionService.findNonTerminatedSubscriptionsByCodeWithoutPrefix(
                    orderLineDto.getOriginalSubscriptionCode());
            log.debug("####subscriptionsToSwap" + subscriptionsToSwap);

            if (subscriptionsToSwap != null) {
                for (Subscription subscriptionToSwap : subscriptionsToSwap) {
                    if (subscriptionToSwap.getStatus() != SubscriptionStatusEnum.ACTIVE) {
                        setError(orderDto, ErrorEnum.INVALID_DATA,
                                "The subscription" + subscriptionToSwap.getCode() + "is not active");
                        return;
                    }
                }

            }

            if (refSubscriptionsToSwap != null) {
                for (Subscription refSubscriptionToSwap : refSubscriptionsToSwap) {

                    String refSubscriptionToSwapCode = "OLD" + refSubscriptionToSwap.getCode();
                    orderService.updateEntityCode(subscriptionService, refSubscriptionToSwap, orderDto, refSubscriptionToSwapCode);

                    UserAccount userAccount = refSubscriptionToSwap.getUserAccount();
                    orderService.updateEntityCode(userAccountService, refSubscriptionToSwap.getUserAccount(), orderDto, "OLD" + userAccount.getCode());
                }
            }

            if (subscriptionsToSwap != null) {
                for (Subscription subscriptionToSwap : subscriptionsToSwap) {

                    String subscriptionToSwapCode = subscriptionToSwap.getCode();

                    Map<String, Object> cfMap = new HashMap<>();
                    cfMap.put(SubscriptionCFsEnum.SWAP_TEL.name(), DateUtils.formatDateWithPattern(new Date(), Utils.DATE_TIME_PATTERN) + "|"
                            + orderLineDto.getBillEffectDate() + "|" + subscriptionToSwapCode + "|" + subscriptionToSwap
                            .getCode().replace(orderLineDto.getSubscriptionCode(), orderLineDto.getRefSubscriptionCode())
                            + "|" + "DONE");

                    DatePeriod datePeriod = new DatePeriod(orderLineDto.getBillEffectDate(), null);
                    subscriptionToSwap.getCfValuesNullSafe().setValue(SubscriptionCFsEnum.SWAP_TEL.name(), datePeriod, -1, cfMap);

                    // swap user account
                    UserAccount userAccount = subscriptionToSwap.getUserAccount();
                    String newUACode = subscriptionToSwap.getUserAccount().getCode().replace(orderLineDto.getSubscriptionCode(), orderLineDto.getRefSubscriptionCode());
                    if (orderService.findEntityByIdOrCode(userAccountService, orderDto, newUACode) == null) {
                        orderService.updateEntityCode(userAccountService, userAccount, orderDto, newUACode);

                        subscriptionToSwap.setUserAccount(userAccount);
                        subscriptionService.update(subscriptionToSwap);
                    }
                    utilService.setServiceIdCF(subscriptionToSwap, newUACode);
                    if (orderService.findEntityByIdOrCode(subscriptionService, orderDto, newUACode) == null) {
                        orderService.updateEntityCode(subscriptionService, subscriptionToSwap, orderDto, newUACode);
                    }

                    createNewAccessPoint(orderLineDto, subscriptionToSwap, orderLineDto.getSubscriptionCode(), orderLineDto.getRefSubscriptionCode());
                }

            }

            if (refSubscriptionsToSwap != null) {
                for (Subscription refSubscriptionToSwap : refSubscriptionsToSwap) {

                    UserAccount userAccount = refSubscriptionToSwap.getUserAccount();
                    String code = userAccount.getCode().replace(orderLineDto.getRefSubscriptionCode(), orderLineDto.getSubscriptionCode());
                    code = code.replace("OLD", "");
                    orderService.updateEntityCode(userAccountService, userAccount, orderDto, code);

                    code = refSubscriptionToSwap.getCode().replace(orderLineDto.getRefSubscriptionCode(), orderLineDto.getSubscriptionCode());
                    code = code.replace("OLD", "");
                    utilService.setServiceIdCF(refSubscriptionToSwap, code);
                    orderService.updateEntityCode(subscriptionService, refSubscriptionToSwap, orderDto, code);

                    Map<String, Object> cfMap = new HashMap<>();

                    cfMap.put(SubscriptionCFsEnum.SWAP_TEL.name(),
                            DateUtils.formatDateWithPattern(new Date(), Utils.DATE_TIME_PATTERN) + "|"
                                    + orderLineDto.getBillEffectDate() + "|"
                                    + refSubscriptionToSwap.getCode().replace(orderLineDto.getSubscriptionCode(),
                                    orderLineDto.getRefSubscriptionCode())
                                    + "|" + refSubscriptionToSwap.getCode().replace(orderLineDto.getRefSubscriptionCode(),
                                    orderLineDto.getSubscriptionCode())
                                    + "|" + "DONE");

                    DatePeriod datePeriod = new DatePeriod(orderLineDto.getBillEffectDate(), null);
                    refSubscriptionToSwap.getCfValuesNullSafe().setValue(SubscriptionCFsEnum.SWAP_TEL.name(), datePeriod, -1, cfMap);

                    createNewAccessPoint(orderLineDto, refSubscriptionToSwap, orderLineDto.getRefSubscriptionCode(), orderLineDto.getSubscriptionCode());

                }

            }
            //For SN bill items the numbers can exists on separate Billing Accounts so when applying the 'number change'
            // charges to the two subscribers associated with the SN bill item, (each subscriber may exist on separate billing accounts and so)
            // each charge may need to be posted against two different billing accounts
            applyOneShotCharge(orderDto, orderLineDto, orderLineDto.getSubscriptionCode());
            applyOneShotCharge(orderDto, orderLineDto, orderLineDto.getRefSubscriptionCode());
        }
    }

    /**
     * apply Misc OneShot Charge
     *
     * @param orderLineDto
     * @param orderDto
     * @param subscription
     * @param chargePriceToOverride
     * @throws BusinessException the business exception
     */
    private void applyMiscOneShotCharge(OrderLineDto orderLineDto, OrderDto orderDto, Subscription subscription, BigDecimal chargePriceToOverride) throws BusinessException {
  
        OneShotChargeTemplate chargetemplate = orderService.findEntityByIdOrCode(oneShotChargeTemplateService, orderDto, ChargeTemplateEnum.MISC_CHARGE.name());
        if (chargetemplate == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA,
                    "The one-shot charge " + orderLineDto.getBillCode() + " is not found");
            return;
        }
        
        log.debug("Will apply a Misc charge {}/{}", chargetemplate.getId(), chargetemplate.getCode());

        OneShotChargeInstance chargeInstance = new OneShotChargeInstance();
        Map<String, String> chargeCF = getServiceParametersCF(orderDto, orderLineDto, chargeInstance);
        chargeInstance.setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), chargeCF);
        oneShotChargeInstanceService.oneShotChargeApplication(subscription, null, chargetemplate,
                null, orderLineDto.getBillEffectDate(), chargePriceToOverride, null, null, null, null, null, null,
                orderDto.getOrderCode(), chargeInstance.getCfValues(), true, ChargeApplicationModeEnum.SUBSCRIPTION);

        if (subscription.getStatus() == SubscriptionStatusEnum.CREATED) {
            subscription.setStatus(SubscriptionStatusEnum.ACTIVE);
        }

    }

    /**
     * Get the transfer product migration record
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return the change product migration record
     */
    private Map<String, Object> getTransferProductMigrationRecord(OrderDto orderDto, OrderLineDto orderLineDto) {
        String from = null;
        String to = null;
        if (BillActionEnum.TOS.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
            from = orderLineDto.getMigrationCode();
            to = orderLineDto.getRefOrderLine().getMigrationCode();
        } else if (BillActionEnum.TNS.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
            from = orderLineDto.getRefOrderLine().getMigrationCode();
            to = orderLineDto.getMigrationCode();
        }
        return getProductMigrationRecord(orderDto, orderLineDto.getBillCode(), from, to, EventTypeEnum.TRANSFER);
    }

    /**
     * Transfer service.
     * <p>
     * * @param orderDto     the order Dto
     *
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void transferService(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        if (orderDto.getErrorCode() != null) {
            return;
        }

        Map<String, Object> migration = getTransferProductMigrationRecord(orderDto, orderLineDto);
        if (BillActionEnum.TOS.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
            Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
            if (subscription == null) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "The subscription " + orderLineDto.getSubscriptionCode() + " is not found");
                return;
            }
            if (subscription.getStatus().equals(SubscriptionStatusEnum.RESILIATED) || subscription.getStatus().equals(SubscriptionStatusEnum.CANCELED)) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "The subscription " + subscription.getCode() + " has status "
                        + subscription.getStatus() + ", it can not be transferred");
                return;
            }

            ServiceInstance serviceInstance = getServiceInstance(orderDto, orderLineDto, subscription);
            if (serviceInstance != null && serviceInstance.getStatus() != InstanceStatusEnum.TERMINATED) {
                orderLineDto.setServiceInstanceId(serviceInstance.getId());
                if (orderLineDto.getXRefSubscriptionCode() != null) {
                    serviceInstance.getCfValuesNullSafe().setValue(ServiceInstanceCFsEnum.XREF_SERVICE_ID.name(), orderLineDto.getXRefSubscriptionCode());
                }
                if (!serviceInstance.getStatus().equals(InstanceStatusEnum.TERMINATED)) {
                    SubscriptionTerminationReason terminationReason = getSubscriptionTerminationReason(serviceInstance, orderDto, migration);
                    terminateService(orderDto, orderLineDto, serviceInstance, terminationReason, orderLineDto.getBillEffectDate(), null);
                    if (orderLineDto.isMain()) {
                        // Add OLD_ prefix to subscription code in order to can create the new one with
                        // same code (for transfer)
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
                        String oldPrefix = "OLD" + sdf.format(new Date().getTime());
                        userAccountService.userAccountTermination(subscription.getUserAccount(), orderLineDto.getBillEffectDate(), terminationReason);
                        orderService.updateEntityCode(userAccountService, subscription.getUserAccount(), orderDto, oldPrefix + "_" + subscription.getUserAccount().getCode());
                        log.debug("Transfer - Subscription terminated : " + subscription.getCode());
                        orderService.updateEntityCode(subscriptionService, subscription, orderDto, oldPrefix + "_" + subscription.getCode());
                    }
                }
            }

        } else if (BillActionEnum.TNS.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
            if (orderLineDto.isMain()) {
                Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
                if (subscription != null) {
                    setError(orderDto, ErrorEnum.INVALID_DATA, "The subscription " + orderLineDto.getSubscriptionCode() + " already exists");
                    return;
                }
            }
            provideService(orderDto, orderLineDto, migration);
        }
    }

    /**
     * Get the change product migration record
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return the change product migration record
     */
    private Map<String, Object> getChangeProductMigrationRecord(OrderDto orderDto, OrderLineDto orderLineDto) {
        String from = null;
        String to = null;
        if (BillActionEnum.COS.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
            from = orderLineDto.getMigrationCode();
            //to = isChangeNumber ? from : orderLineDto.getRefOrderLine().getMigrationCode();
            to = orderLineDto.getRefOrderLine().getMigrationCode();
        } else if (BillActionEnum.CNS.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
            from = orderLineDto.getRefOrderLine().getMigrationCode();
            //to = isChangeNumber ? from : orderLineDto.getMigrationCode();
            to = orderLineDto.getMigrationCode();
        }
        return getProductMigrationRecord(orderDto, orderLineDto.getBillCode(), from, to, EventTypeEnum.CHANGE);
    }

    /**
     * Change Service from Order Line
     *
     * @param orderDto     Order
     * @param orderLineDto Order Line
     * @throws BusinessException the business exception
     */
    @SuppressWarnings("unchecked")
    private void changeService(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        boolean isChangeNumber = false;
        if (orderLineDto.getOriginalSubscriptionCode().equalsIgnoreCase(orderLineDto.getRefOrderLine().getXRefSubscriptionCode())
                && orderLineDto.getRefOrderLine().getOriginalSubscriptionCode().equalsIgnoreCase(orderLineDto.getXRefSubscriptionCode())) {
            isChangeNumber = true;
        }

        Map<String, Object> migration = getChangeProductMigrationRecord(orderDto, orderLineDto);
        if (BillActionEnum.COS.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
            Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
            if (subscription == null) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "The subscription " + orderLineDto.getSubscriptionCode() + " is not found");
                return;
            }
            if (subscription.getStatus() == SubscriptionStatusEnum.RESILIATED || subscription.getStatus() == SubscriptionStatusEnum.CANCELED) {
                log.debug("Subscription " + subscription.getCode() + " is already terminated");
                return;
            }

            SubscriptionTerminationReason terminationReason = null;
            ServiceInstance serviceInstance = getServiceInstance(orderDto, orderLineDto, subscription);
            if (serviceInstance != null && serviceInstance.getStatus() != InstanceStatusEnum.TERMINATED) {
                // Saving reference to Service Id in ceased service
                if (orderLineDto.getXRefSubscriptionCode() != null) {
                    serviceInstance.setCfValue(ServiceInstanceCFsEnum.XREF_SERVICE_ID.name(), orderLineDto.getXRefSubscriptionCode());
                }
                if (!InstanceStatusEnum.TERMINATED.equals(serviceInstance.getStatus())) {
                    orderLineDto.setServiceInstanceId(serviceInstance.getId());
                    terminationReason = getSubscriptionTerminationReason(serviceInstance, orderDto, migration);
                    terminateService(orderDto, orderLineDto, serviceInstance, terminationReason, orderLineDto.getBillEffectDate(), null);
                    if (orderLineDto.isMain()) {
                        for (ServiceInstance ancillaryServiceInstance : subscription.getServiceInstances()) {
                            if (!orderLineDto.getBillCode().equals(ancillaryServiceInstance.getCode()) &&
                                    !InstanceStatusEnum.TERMINATED.equals(ancillaryServiceInstance.getStatus())) {
                                terminateService(orderDto, orderLineDto, ancillaryServiceInstance,
                                        orderService.findEntityByIdOrCode(subscriptionTerminationReasonService, orderDto, TerminationReasonEnum.TR_NO_OVERRIDE_RREFUND.name()),
                                        orderLineDto.getBillEffectDate(), null);
                            }
                        }
                        if (isChangeNumber == true && terminationReason != null) {
                            terminateSubscription(orderDto, orderLineDto, subscription, terminationReason, orderLineDto.getBillEffectDate());
                        }
                    }
                }
            }

        } else if (BillActionEnum.CNS.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
            Subscription subscription = null;
            // change UAN
            changeUAN(orderDto, orderLineDto);
            if (orderDto.getErrorCode() != null) {
                return;
            }

            if (orderLineDto.isMain()) {
                // change service with a change number
                if (isChangeNumber) {
                    subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getRefOrderLine().getSubscriptionCode());
                    if (subscription != null && isChangeNumber) {
                        updateChangeNumberHistory(orderLineDto, subscription);
                        for (Access accessPoint : subscription.getAccessPoints()) {
                            if (accessPoint.getAccessUserId().endsWith(orderLineDto.getXRefSubscriptionCode())) {
                                accessPoint.setEndDate(DateUtils
                                        .setDateToEndOfDay(DateUtils.addDaysToDate(orderLineDto.getBillEffectDate(), -1)));
                                accessService.update(accessPoint);
                                // add access point if same master account
                                if (orderLineDto.getMasterAccountNumber()
                                        .equals(orderLineDto.getRefOrderLine().getMasterAccountNumber())) {
                                    Access newAccessPoint = new Access();
                                    newAccessPoint.setAccessUserId(orderLineDto.getSubscriptionCode());
                                    newAccessPoint.setStartDate(DateUtils.setDateToStartOfDay(orderLineDto.getBillEffectDate()));
                                    newAccessPoint.setSubscription(subscription);
                                    accessService.update(newAccessPoint);
                                }
                            }
                        }
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
                        String newCode = "OLD" + sdf.format(new Date().getTime()) + "_" + orderLineDto.getRefOrderLine().getSubscriptionCode();
                        orderService.updateEntityCode(subscriptionService, subscription, orderDto, newCode);
                        orderService.updateEntityCode(userAccountService, subscription.getUserAccount(), orderDto, newCode);
                    }

                } else {
                    subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getRefOrderLine().getSubscriptionCode());
                    // check if we have to transfer subscription if different billing account or different prefix for subscription
                    if (subscription != null && (!orderLineDto.getRefOrderLine().getSubscriptionCode().equals(orderLineDto.getSubscriptionCode())
                            || !orderLineDto.getMasterAccountNumber().equals(subscription.getUserAccount().getBillingAccount().getCode()))) {
                        List<AccessDto> AccessDtoList = new ArrayList<>();
                        for (Access a : subscription.getAccessPoints()) {
                            AccessDto access = new AccessDto();
                            access.setCode(a.getAccessUserId());
                            access.setEndDate(a.getEndDate());
                            AccessDtoList.add(access);
                        }
                        SubscriptionTerminationReason terminationReason = orderService.findEntityByIdOrCode(subscriptionTerminationReasonService, orderDto,
                                TerminationReasonEnum.TR_NO_OVERRIDE_NONE.name());
                        // Add OLD_ prefix to subscription code in order to can create the new one with
                        // same code (for transfer)
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
                        String oldPrefix = "OLD" + sdf.format(new Date().getTime());
                        userAccountService.userAccountTermination(subscription.getUserAccount(), orderLineDto.getRefOrderLine().getBillEffectDate(), terminationReason);
                        orderService.updateEntityCode(userAccountService, subscription.getUserAccount(), orderDto, oldPrefix + "_" + subscription.getUserAccount().getCode());
                        for (AccessDto AccessDto : AccessDtoList) {
                            for (Access access : subscription.getAccessPoints()) {
                                if (access.getAccessUserId().equals(AccessDto.getCode())) {
                                    if (AccessDto.getEndDate() != null) {
                                        access.setEndDate(AccessDto.getEndDate());
                                        accessService.update(access);
                                    }
                                }
                            }
                        }

                        orderService.updateEntityCode(subscriptionService, subscription, orderDto, oldPrefix + "_" + subscription.getCode());
                    }
                }
            }

            provideService(orderDto, orderLineDto, migration);
        }
    }

    /**
     * Provide service
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @param migration    the migration matrix
     */
    private void provideService(OrderDto orderDto, OrderLineDto orderLineDto, Map<String, Object> migration) {
        String serviceToApply = null;
        boolean connectionChargeOn = false;
        boolean resetEngagement = false;
        if (migration != null && !migration.isEmpty()) {
            serviceToApply = (String) migration.get(MigrationCFEnum.BILL_CODE_TO_APPLY.name());
            if ("Y".equalsIgnoreCase((String) migration.get(MigrationCFEnum.CONNECTION_CHARGE_ON.name()))) {
                connectionChargeOn = true;
            }
            if ("Y".equalsIgnoreCase((String) migration.get(MigrationCFEnum.RESET_ENGAGEMENT.name()))) {
                resetEngagement = true;
            }
        }
        provideService(orderLineDto, orderDto, connectionChargeOn, resetEngagement, serviceToApply);
    }

    /**
     * Get termination reason
     *
     * @param serviceInstance        the service instance
     * @param orderDto               the order Dto
     * @param applyMinTermOnPrevious the apply min term on previous
     * @param applyCeaseCharge       the apply cease charge
     * @param applyService           the apply service
     * @return the termination reason code
     */
    private TerminationReasonEnum getTerminationReason(ServiceInstance serviceInstance, OrderDto orderDto, boolean applyMinTermOnPrevious, boolean applyCeaseCharge,
                                                       boolean applyService) {
        TerminationReasonEnum terminationReason = TerminationReasonEnum.TR_NO_OVERRIDE_RREFUND;
        if (applyMinTermOnPrevious && serviceInstance.getEndAgreementDate() != null) {
            if (serviceInstance.getEndAgreementDate().after(new Date())) {
                terminationReason = TerminationReasonEnum.TR_NO_OVERRIDE_AGR_RREFUND;
            }
        } else if (applyCeaseCharge && serviceTemplateHasTerminationCharge(orderDto, serviceInstance.getCode())) {
            terminationReason = TerminationReasonEnum.TR_NO_OVERRIDE_RREFUND_CHARGE;
        } else if (applyService) {
            terminationReason = TerminationReasonEnum.TR_NO_OVERRIDE_RREFUND_SERVICE;
        }
        return terminationReason;
    }

    /**
     * Get subscription termination reason
     *
     * @param serviceInstance the service instance
     * @param orderDto        the order Dto
     * @param migration       the migration matrix
     * @return the subscription termination reason
     */
    private SubscriptionTerminationReason getSubscriptionTerminationReason(ServiceInstance serviceInstance, OrderDto orderDto, Map<String, Object> migration) {
        boolean applyMinTermOnPrevious = false;
        boolean applyCeaseCharge = false;
        if (migration != null && !migration.isEmpty()) {
            if ("Y".equalsIgnoreCase((String) migration.get(MigrationCFEnum.APPLY_MT_ON_PREVIOUS.name()))) {
                applyMinTermOnPrevious = true;
            }
            if ("Y".equalsIgnoreCase((String) migration.get(MigrationCFEnum.APPLY_CEASE_CHARGE.name()))) {
                applyCeaseCharge = true;
            }
        }
        TerminationReasonEnum terminationReason = getTerminationReason(serviceInstance, orderDto, applyMinTermOnPrevious, applyCeaseCharge, false);
        return orderService.findEntityByIdOrCode(subscriptionTerminationReasonService, orderDto, terminationReason.getCode());
    }

    /**
     * Terminate the service from Order Line
     *
     * @param orderLineDto the order line Dto
     * @param orderDto     the order Dto
     * @throws BusinessException the business exception
     */
    private void terminateService(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        boolean applyMinTermOnPrevious = false;
        boolean applyCeaseCharge = false;
        String serviceToApply = null;

        // Check Service Type (Main, Event, Ancillary)
        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        if (subscription == null || subscription.getStatus() == SubscriptionStatusEnum.RESILIATED) {
            if (!orderLineDto.isMain()) {
                return;
            }
            String error = "The subscription " + orderLineDto.getSubscriptionCode() + " is not found or already ceased.";
            orderDto.setDataDiscrepancies(orderDto.getDataDiscrepancies() + error + " \n");
            setError(orderDto, ErrorEnum.INVALID_DATA, error);
            return;
        }

        if (!ActionQualifierEnum.FAILED_INSTALL.name().equalsIgnoreCase(orderLineDto.getBillActionQualifier())) {
            Map<String, Object> migration = getProductMigrationRecord(orderDto, orderLineDto.getBillCode(), orderLineDto.getMigrationCode(), null, EventTypeEnum.CEASE);
            if (migration != null && !migration.isEmpty()) {
                if ("Y".equalsIgnoreCase((String) migration.get(MigrationCFEnum.APPLY_MT_ON_PREVIOUS.name()))) {
                    applyMinTermOnPrevious = true;
                }
                if ("Y".equalsIgnoreCase((String) migration.get(MigrationCFEnum.APPLY_CEASE_CHARGE.name()))) {
                    applyCeaseCharge = true;
                }
                serviceToApply = (String) migration.get(MigrationCFEnum.BILL_CODE_TO_APPLY.name());
            }
        }

        // Set the service quantity
        BigDecimal quantity = BigDecimal.ZERO;
        quantity = updateServiceQuantity(orderLineDto, orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode()), quantity);
        if (quantity == null) {
            return;
        }
        // if main service then terminate all
        if (orderLineDto.isMain()) {
            terminateMainService(orderLineDto, orderDto, subscription, quantity, applyMinTermOnPrevious, applyCeaseCharge, serviceToApply);
        } else {
            terminateAncillaryService(orderLineDto, orderDto, subscription, quantity, applyMinTermOnPrevious, applyCeaseCharge, serviceToApply);
        }
    }

    /**
     * Terminate the main service and all it's ancillary services.
     *
     * @param orderLineDto           the order line DTO
     * @param orderDto               the order DTO
     * @param subscription           the subscription
     * @param quantity               the quantity
     * @param applyMinTermOnPrevious the apply min term on previous
     * @param applyCeaseCharge       the apply cease charge
     * @param serviceToApply         the service to apply
     * @throws BusinessException the business exception
     */
    private void terminateMainService(OrderLineDto orderLineDto, OrderDto orderDto, Subscription subscription, BigDecimal quantity,
                                      boolean applyMinTermOnPrevious, boolean applyCeaseCharge, String serviceToApply) throws BusinessException {
        ServiceInstance mainServiceInstance = getServiceInstance(orderDto, orderLineDto, subscription);
        if (mainServiceInstance == null) {
            return;
        }

        SubscriptionTerminationReason terminationReason = null;
        SubscriptionTerminationReason ancillaryTerminationReason = null;
        Date terminationDate = orderLineDto.getBillEffectDate();
        if (ActionQualifierEnum.FAILED_INSTALL.name().equalsIgnoreCase(orderLineDto.getBillActionQualifier())) {
            // #140 to rebate the fully amount
            terminationDate = subscription.getSubscriptionDate();
            terminationReason = orderService.findEntityByIdOrCode(subscriptionTerminationReasonService, orderDto, TerminationReasonEnum.TR_FAILED_INSTALL.name());
            ancillaryTerminationReason = terminationReason;
        } else {
            TerminationReasonEnum terminationReasonEnum = getTerminationReason(mainServiceInstance, orderDto, applyMinTermOnPrevious, applyCeaseCharge,
                    !StringUtils.isBlank(serviceToApply));
            if (!terminationReasonEnum.isApplyService()) {
                serviceToApply = null;
            }
            terminationReason = orderService.findEntityByIdOrCode(subscriptionTerminationReasonService, orderDto, terminationReasonEnum.getCode());
            ancillaryTerminationReason = orderService.findEntityByIdOrCode(subscriptionTerminationReasonService, orderDto, TerminationReasonEnum.TR_NO_OVERRIDE_RREFUND.name());
            terminationDate = orderLineDto.getBillEffectDate();
        }


        if (mainServiceInstance.getStatus() == InstanceStatusEnum.ACTIVE && mainServiceInstance.getQuantity().compareTo(quantity) >= 0) {
            quantity = updateServiceQuantity(orderLineDto, orderDto, mainServiceInstance, quantity, null);
            if (quantity != null && quantity.compareTo(BigDecimal.ZERO) > 0) {
                mainServiceInstance.setQuantity(quantity);
                mainServiceInstance = serviceInstanceService.update(mainServiceInstance);
                rerate(orderDto, orderLineDto, mainServiceInstance, terminationReason);
                return;
            }
        }
        if (quantity == null) {
            return;
        }

        for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
            if (serviceInstance.getStatus().equals(InstanceStatusEnum.TERMINATED)) {
                continue;
            }
            if (orderLineDto.getXRefSubscriptionCode() != null) {
                serviceInstance.getCfValuesNullSafe().setValue(ServiceInstanceCFsEnum.XREF_SERVICE_ID.name(),
                        orderLineDto.getXRefSubscriptionCode());
            }

            String serviceType = (String) serviceInstance.getServiceTemplate().getCfValue(ServiceTemplateCFsEnum.SERVICE_TYPE.name());
            if (!StringUtils.isBlank(serviceType)) {
                if (ServiceTypeEnum.MAIN.getLabel().equalsIgnoreCase(serviceType)) {
                    terminateService(orderDto, orderLineDto, serviceInstance, terminationReason, terminationDate, serviceToApply);
                } else {
                    // Active ancillary services should not exist against a subscriber when a
                    // request to cease the primary service is being processed
                    if (InstanceStatusEnum.ACTIVE == serviceInstance.getStatus()) {
                        orderDto.setDataDiscrepancies(orderDto.getDataDiscrepancies() + "The Cease request is for a Primary Service and active ancillary " +
                                "services exist against the subscription. \n ");
                    }
                    terminateService(orderDto, orderLineDto, serviceInstance, ancillaryTerminationReason, terminationDate, null);
                }
            }
        }
        terminateSubscription(orderDto, orderLineDto, subscription, terminationReason, terminationDate);
    }


    /**
     * Terminate the ancillary service from Order Line
     *
     * @param orderLineDto           the order line DTO
     * @param orderDto               the order DTO
     * @param subscription           the subscription
     * @param quantity               the quantity
     * @param applyMinTermOnPrevious the apply min term on previous
     * @param applyCeaseCharge       the apply cease charge
     * @param serviceToApply         the service to apply
     * @throws BusinessException the business exception
     */
    private void terminateAncillaryService(OrderLineDto orderLineDto, OrderDto orderDto, Subscription subscription, BigDecimal quantity,
                                           boolean applyMinTermOnPrevious, boolean applyCeaseCharge, String serviceToApply) throws BusinessException {
        if (subscription == null) {
            return;
        }

        // Tha map which allows to store for each each service its new quantity.
        Map<ServiceInstance, BigDecimal> serviceInstanceQuantity = new HashMap<>();
        ServiceInstance serviceInstance = getAncillaryServiceInstance(orderDto, orderLineDto, subscription);
        if (orderDto.getErrorCode() != null) {
            return;
        }

        SubscriptionTerminationReason terminationReason = null;
        Date terminationDate = orderLineDto.getBillEffectDate();
        if (ActionQualifierEnum.FAILED_INSTALL.name().equalsIgnoreCase(orderLineDto.getBillActionQualifier())) {
            // #140 to rebate the fully amount
            terminationDate = subscription.getSubscriptionDate();
            terminationReason = orderService.findEntityByIdOrCode(subscriptionTerminationReasonService, orderDto, TerminationReasonEnum.TR_FAILED_INSTALL.name());
        } else {
            TerminationReasonEnum terminationReasonEnum = getTerminationReason(serviceInstance, orderDto, applyMinTermOnPrevious, applyCeaseCharge,
                    !StringUtils.isBlank(serviceToApply));
            terminationReason = orderService.findEntityByIdOrCode(subscriptionTerminationReasonService, orderDto, terminationReasonEnum.getCode());
            if (!terminationReasonEnum.isApplyService()) {
                serviceToApply = null;
            }
            terminationDate = orderLineDto.getBillEffectDate();
        }


        if (orderLineDto.getServiceInstanceIdentifierAttributes() == ServiceInstanceIdentifierAttributesCFsEnum.RANGE_START ||
                orderLineDto.getServiceInstanceIdentifierAttributes() == ServiceInstanceIdentifierAttributesCFsEnum.RANGE_START_RANGE_END) {

            updateServiceParametersCF(orderDto, orderLineDto, serviceInstance);
            if (orderDto.getErrorCode() != null) {
                return;
            }
//          serviceInstance.setCfValue(ServiceInstanceCFsEnum.RANGE_START.name(), "");
//          serviceInstance.setCfValue(ServiceInstanceCFsEnum.RANGE_END.name(), "");
//          serviceInstance.removeCfValue(ServiceInstanceCFsEnum.RANGE_START.name());
//          serviceInstance.removeCfValue(ServiceInstanceCFsEnum.RANGE_END.name());
        }

        // validate and calculate the quantity to be reduced
        if (!validateAndCalculateQuantity(orderLineDto, orderDto, subscription, serviceInstanceQuantity, quantity)) {
            return;
        }

        // AC4 : For Cease bill items, if the quantity against the service/bill item is
        // reduced to zero, the service is ceased
        if (serviceInstanceQuantity.get(serviceInstance) != null) {
            if (serviceInstance.getQuantity().compareTo(quantity) <= 0) {
                if (serviceInstance.getQuantity().compareTo(quantity) == 0) {
                    serviceInstance.setQuantity(BigDecimal.ZERO);
                    serviceInstanceService.update(serviceInstance);
                }
                terminateService(orderDto, orderLineDto, serviceInstance, terminationReason, terminationDate, serviceToApply);
                String rangeStart = orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_START.getLabel());
                if (!StringUtils.isBlank(rangeStart) && StringUtils.isBlank(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_END.getLabel()))) {
                    Access access = accessService.getAccess(subscription, rangeStart);
                    if (access != null) {
                        access.setEndDate(orderLineDto.getBillEffectDate());
                        accessService.update(access);
                    }
                }
            } else if (serviceInstance.getQuantity().compareTo(quantity) > 0) {
                serviceInstance.setQuantity(serviceInstanceQuantity.get(serviceInstance));
                rerate(orderDto, orderLineDto, serviceInstance, terminationReason);
                return;
            }
        }

    }

    /**
     * Suspend the service from Order Line
     *
     * @param orderDto     Order
     * @param orderLineDto Order Line
     * @throws BusinessException the business exception
     */
    private void suspendService(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        log.debug("Suspending service...");

        // Check Service Type (Main, Event, Ancillary)
        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        if (subscription == null) {
            String error = "The subscription " + orderLineDto.getSubscriptionCode() + " is not found or already ceased.";
            orderDto.setDataDiscrepancies(orderDto.getDataDiscrepancies() + error + " \n");
            setError(orderDto, ErrorEnum.INVALID_DATA, error);
        }

        SubscriptionTerminationReason terminationReason = orderService.findEntityByIdOrCode(subscriptionTerminationReasonService, orderDto, TerminationReasonEnum.TR_TOS.name());

        if (orderLineDto.isMain()) {
            // services
            for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
                if (serviceInstance.getStatus().equals(InstanceStatusEnum.TERMINATED)) {
                    continue;
                }
                terminateService(orderDto, orderLineDto, serviceInstance, terminationReason, orderLineDto.getBillEffectDate(), null);
            }
            terminateSubscription(orderDto, orderLineDto, subscription, terminationReason, orderLineDto.getBillEffectDate());
        } else {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The service " + orderLineDto.getBillCode() + " is not Main to be suspended");
            return;
        }
    }

    /**
     * Provide the service
     *
     * @param orderDto     Order
     * @param orderLineDto Order Line
     * @throws BusinessException the business exception
     */
    private void provideService(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        provideService(orderLineDto, orderDto, true, true, null);
        if (orderDto.getErrorCode() == null) {
            UpdateEndpointAddresses(orderDto, orderLineDto);
        }
    }

    /**
     * Create subscription
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return the subscription
     * @throws BusinessException the business exception
     */
    protected Subscription createSubscription(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        Subscription subscription = new Subscription();
        subscription.setCode(orderLineDto.getSubscriptionCode());
        subscription.setSeller(orderService.findEntityByIdOrCode(sellerService, orderDto, ApplicationPropertiesEnum.OPENEIR_SELLER_CODE.getProperty()));
        UserAccount userAccount = orderService.findEntityByIdOrCode(userAccountService, orderDto, orderLineDto.getSubscriptionCode());
        if (userAccount == null) {
            log.debug("User account does not exist, it will be created");
            userAccount = new UserAccount();
            userAccount.setCode(orderLineDto.getSubscriptionCode());
            BillingAccount billingAccount = orderService.findEntityByIdOrCode(billingAccountService, orderDto, orderLineDto.getBillingAccountCode());
            userAccountService.createUserAccount(billingAccount, userAccount);
        }
        subscription.setUserAccount(userAccount);
        subscription.setSeller(orderService.findEntityByIdOrCode(sellerService, orderDto, ApplicationPropertiesEnum.OPENEIR_SELLER_CODE.getProperty()));
        OfferTemplate offerTemplate = orderService.findEntityByIdOrCode(offerTemplateService, orderDto, orderLineDto.getOfferTemplateCode());
        subscription.setOffer(offerTemplate);
        subscription.setSubscriptionDate(orderLineDto.getBillEffectDate());
        subscription.setStatusDate(orderLineDto.getBillEffectDate());

        updateSubscriptionCfs(orderLineDto, subscription);
        subscriptionService.create(subscription);

        // add access point
        createAccessPoint(orderLineDto, subscription);
        return subscription;
    }

    /**
     * Activate the service from Order Line with CHANGE options
     *
     * @param orderLineDto         the order line Dto
     * @param orderDto             the order Dto
     * @param connectionChargeOn   the connection charge on
     * @param resetEngagementOnNew the reset engagement on new
     * @param serviceToApply       the service to apply
     * @throws BusinessException the business exception
     */
    private void provideService(OrderLineDto orderLineDto, OrderDto orderDto, boolean connectionChargeOn, boolean resetEngagementOnNew, String serviceToApply)
            throws BusinessException {

        // PO2-528 : Validation on Provide order to ensure service_id not in use on
        // DDI/MSN Ancillary Service
        if (!isValidOnUsedRanges(orderDto, orderLineDto.getSubscriptionCode(), null)) {
            String errorMessage = "ServiceId " + orderLineDto.getSubscriptionCode() + " already in use on ranges of Ancillary Service or an active subscription";
            setError(orderDto, ErrorEnum.INVALID_DATA, errorMessage);
            return;
        }

        ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());

        // Set the service quantity
        BigDecimal quantity = updateServiceQuantity(orderLineDto, serviceTemplate, BigDecimal.ZERO);
        if (quantity == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The quantity exceeds the maximum quantity allowed for the service : " +
                    orderLineDto.getBillCode());
            return;
        }

        // Create subscription if doesn't exist
        if (subscription == null) {
            subscription = createSubscription(orderDto, orderLineDto);
        } else {

            if (subscription.getStatus() == SubscriptionStatusEnum.RESILIATED || subscription.getStatus() == SubscriptionStatusEnum.CANCELED) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "Subscription " + subscription.getCode() + " has status " + subscription.getStatus());
                return;
            }

            if (orderLineDto.isMain()) {
                ServiceInstance serviceInstance = getMainServiceInstance(subscription);
                if (serviceInstance != null && (serviceInstance.getSubscribedTillDate() == null || orderLineDto.getBillEffectDate().before(serviceInstance.getSubscribedTillDate()))) {
                    setError(orderDto, ErrorEnum.INVALID_DATA, "You cannot provide the main service " + orderLineDto.getBillCode() + " for the subscription " + subscription.getCode() +
                            " because it already has " + serviceInstance.getCode() + " as main service");
                    return;
                }
            } else if (orderLineDto.isAncillary()) {
                //update master account number to be right in case of transfer
                orderLineDto.setMasterAccountNumber(subscription.getUserAccount().getBillingAccount().getCode());

                resetEngagementOnNew = false;
                // check if there is a main service when activating an ancillary service
                if (!isMainServiceExist(subscription)) {
                    setError(orderDto, ErrorEnum.INVALID_DATA, "Main service is not found");
                    return;
                }
                ServiceInstance serviceInstance = getAncillaryServiceInstance(orderDto, orderLineDto, subscription);
                if (serviceInstance != null) {
                    quantity = updateServiceQuantity(orderLineDto, orderDto, serviceInstance, quantity, null);
                    if (quantity != null) {
                        serviceInstance.setQuantity(quantity);
                        rerate(orderDto, orderLineDto, serviceInstance, null);
                    }
                    return;
                }

                Object multipleInstances = serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MULTIPLE_INSTANCES.name());
                if (multipleInstances != null && ((Boolean) multipleInstances) == true) {
                    if (!isValidRanges(orderDto, orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_START.getLabel()),
                            orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_END.getLabel()), orderLineDto.getOriginalSubscriptionCode(),
                            orderLineDto.getBillEffectDate())) {
                        return;
                    }

                    if (!StringUtils.isBlank(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_START.getLabel())) &&
                            StringUtils.isBlank(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_END.getLabel()))) {
                        // add access point
                        Access newAccessPoint = new Access();
                        newAccessPoint.setAccessUserId(orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_RANGE_START.getLabel()));
                        newAccessPoint.setStartDate(orderLineDto.getBillEffectDate());
                        newAccessPoint.setSubscription(subscription);
                        accessService.update(newAccessPoint);
                    }
                }
            }
        }


        ServiceInstance serviceInstance = activateService(orderDto, orderLineDto, subscription, connectionChargeOn);
        if (serviceInstance != null) {
            if (!resetEngagementOnNew && orderLineDto.getRefOrderLine() != null && orderLineDto.getRefOrderLine().getServiceInstanceId() != null) {
                ServiceInstance refServiceInstance = serviceInstanceService.findById(orderLineDto.getRefOrderLine().getServiceInstanceId());
                serviceInstance.setEndAgreementDate(refServiceInstance.getEndAgreementDate());
            }

            // Saving reference to Service Id in provided service
            if (!StringUtils.isBlank(orderLineDto.getXRefSubscriptionCode())) {
                serviceInstance.setCfValue(ServiceInstanceCFsEnum.XREF_SERVICE_ID.name(), orderLineDto.getXRefSubscriptionCode());
            }
            serviceInstanceService.update(serviceInstance);
            applyService(orderDto, orderLineDto, serviceToApply);
        }
    }

    /**
     * Checks if main service exist.
     *
     * @param subscription the subscription
     * @return true, if main service exist
     */
    private boolean isMainServiceExist(Subscription subscription) {
        for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
            if (serviceInstance.getStatus().equals(InstanceStatusEnum.TERMINATED)) {
                continue;
            }

            if (serviceInstance.getServiceTemplate() != null && ServiceTypeEnum.MAIN.getLabel().equalsIgnoreCase(serviceInstance.getServiceTemplate().getCfValue(ServiceTemplateCFsEnum.SERVICE_TYPE.name()).toString())) {
                return true;
            }
        }
        return false;
    }


    /**
     * Update exchange code to exchange class in CF_BILL_ATTRIBUTE
     *
     * @param orderDto     Order
     * @param orderLineDto Order Line
     * @param attributes   Order Line attributes
     */
    @SuppressWarnings("unchecked")
    private void updateExchangeAttribute(OrderDto orderDto, OrderLineDto orderLineDto, Map<String, String> attributes) {
        String exchangeClass = null;
        String exchangeCodeLine = (String) attributes.get("EXCHANGE");
        if (exchangeCodeLine == null) {
            // no exchange attribute found
            return;
        }
        String exchangeCode = exchangeCodeLine.split(Pattern.quote("|"))[0];
        Map<String, String> exchangeDetails = (Map<String, String>) customFieldInstanceService.getCFValueByKey(
                appProvider, ProviderCFsEnum.EXCHANGE_DETAILS.name(), orderLineDto.getBillEffectDate(), exchangeCode);
        if (exchangeDetails == null) {
            return;
        }

        exchangeClass = exchangeDetails.get("Exchange_Class");
        if (StringUtils.isBlank(exchangeClass)) {
            return;
        }
        exchangeCodeLine = exchangeCodeLine.replaceFirst(exchangeCode, exchangeClass);
        attributes.put("EXCHANGE", exchangeCodeLine);
    }

    private boolean isValidOnUsedRanges(OrderDto orderDto, String rangeStart, String rangeEnd) {

        if (!StringUtils.isBlank(rangeStart) || !StringUtils.isBlank(rangeEnd)) {

            if (!isValidRanges(rangeStart, rangeEnd)) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "NEW_RANGE_START and NEW_RANGE_END already exist or interferes with existing one");
                return false;
            }
        }

        return true;
    }

    /**
     * Set the service quantity
     *
     * @param orderLineDto    The Order Line
     * @param serviceTemplate The service template
     * @param quantity        The quantity
     * @return The service quantity
     */
    private BigDecimal updateServiceQuantity(OrderLineDto orderLineDto, ServiceTemplate serviceTemplate, BigDecimal quantity) {

        Double maxQuantity = (Double) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MAX_QUANTITY.name());
        BigDecimal billQuantity = new BigDecimal(orderLineDto.getBillQuantity());
        if (billQuantity.compareTo(new BigDecimal(maxQuantity)) > 0) {
            return null;
        }
        quantity = quantity.add(billQuantity);
        return quantity;
    }

    /**
     * Set the service quantity
     *
     * @param orderLineDto    The Order Line
     * @param serviceInstance The service instance.
     * @param quantity        The quantity
     * @return The service quantity
     */
    private BigDecimal updateServiceQuantity(OrderLineDto orderLineDto, OrderDto orderDto, ServiceInstance serviceInstance, BigDecimal quantity,
                                             Map<ServiceInstance, BigDecimal> serviceInstanceQuantity) {

        // Get the max quantity for the current service.
        Double maxQuantity = (Double) serviceInstance.getServiceTemplate().getCfValue(ServiceTemplateCFsEnum.MAX_QUANTITY.name());

        if (quantity != null && serviceInstance.getQuantity() != null) {

            switch (BillActionEnum.valueOfIgnoreCase(orderLineDto.getBillAction())) {
                case P:
                case CNS:
                case TNS:
                    quantity = quantity.add(serviceInstance.getQuantity());
                    break;
                case C:
                case COS:
                case TOS:
                    if (serviceInstance.getQuantity().compareTo(quantity) <= 0) {
                        if (serviceInstance.getQuantity().compareTo(quantity) < 0) {
                            orderDto.setDataDiscrepancies(orderDto.getDataDiscrepancies() + "The quantity on the bill code should not be greater than the quantity on the active service. \n");
                            quantity = serviceInstance.getQuantity();
                        } else {
                            quantity = BigDecimal.ZERO;
                        }
                        if (serviceInstanceQuantity != null) {
                            serviceInstanceQuantity.put(serviceInstance, quantity);
                        }
                    } else {
                        if (serviceInstanceQuantity != null) {
                            serviceInstanceQuantity.put(serviceInstance, serviceInstance.getQuantity().subtract(quantity));
                        }
                        quantity = serviceInstance.getQuantity().subtract(quantity);
                    }
                    break;
            }
        }
        if (new BigDecimal(maxQuantity).compareTo(quantity) < 0) {
            setError(orderDto, ErrorEnum.INVALID_DATA,
                    "The quantity supplied plus the existing quantity exceeds the maximum quantity allowed for the service : "
                            + orderLineDto.getBillCode());
            return null;
        }
        return quantity;
    }

    /**
     * Validate bill item quantity and calculate the service quantity.
     *
     * @param orderLineDto            The order line dTO
     * @param subscription            The subscription
     * @param serviceInstanceQuantity The service instance quantity map
     * @param quantity                The bill item quantity
     * @return
     */
    private boolean validateAndCalculateQuantity(OrderLineDto orderLineDto, OrderDto orderDto, Subscription subscription,
                                                 Map<ServiceInstance, BigDecimal> serviceInstanceQuantity, BigDecimal quantity) {

        for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
            if (serviceInstance != null) {
                if (orderLineDto.getBillCode().equals(serviceInstance.getCode())
                        && serviceInstance.getStatus() == InstanceStatusEnum.ACTIVE) {
                    serviceInstanceQuantity.put(serviceInstance, serviceInstance.getQuantity());
                    quantity = updateServiceQuantity(orderLineDto, orderDto, serviceInstance, quantity, serviceInstanceQuantity);
                    if (quantity == null) {
                        return false;
                    }
                    if (serviceInstance.getQuantity().compareTo(quantity) == 0) {
                        break;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Get service template
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the The order line Dto
     * @return
     */
    protected ServiceTemplate getServiceTemplate(OrderDto orderDto, OrderLineDto orderLineDto) {
        ServiceTemplate serviceTemplate = (ServiceTemplate) orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
        if (serviceTemplate != null) {
            return serviceTemplate;
        }
        // If the bill code is already calculated and service template not found so don't recalculated the bill code
        if (serviceTemplate == null && !StringUtils.isBlank(orderLineDto.getOriginalBillCode()) &&
                !orderLineDto.getOriginalBillCode().equalsIgnoreCase(orderLineDto.getBillCode())) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Service is not found : " + orderLineDto.getBillCode());
            return null;
        }
        if (StringUtils.isBlank(orderLineDto.getMainServiceBillCode())) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Service is not found : " + orderLineDto.getBillCode());
            return null;
        }
        serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getMainServiceBillCode());
        if (serviceTemplate == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Main service is not found : " + orderLineDto.getMainServiceBillCode());
            return null;
        }
        String productSetCode = getProductSetCode(orderDto, serviceTemplate);
        if (StringUtils.isBlank(productSetCode)) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "No ProductSet is configured for the main bill code : " + orderLineDto.getMainServiceBillCode());
            return serviceTemplate;
        }
        orderLineDto.setOriginalBillCode(orderLineDto.getBillCode());
        orderLineDto.setBillCode(productSetCode + "_" + orderLineDto.getBillCode());
        serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
        if (serviceTemplate == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Service is not found : " + orderLineDto.getBillCode());
            return null;
        }
        return serviceTemplate;
    }

    /**
     * Validate service template
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the The order line Dto
     * @return true is service template is valid.
     * @throws BusinessException the business exception
     */
    private boolean validateServiceTemplate(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        ServiceTemplate serviceTemplate = getServiceTemplate(orderDto, orderLineDto);
        if (serviceTemplate == null) {
            return false;
        }
        String serviceType = (String) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.SERVICE_TYPE.name());
        if (StringUtils.isBlank(serviceType)) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "Service Type is not found for service " + orderLineDto.getBillCode());
            return false;
        }
        orderLineDto.setServiceType(serviceType);
        orderLineDto.setMigrationCode((String) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MIGRATION_CODE.name()));

        Double maxQuantity = (Double) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MAX_QUANTITY.name());
        if (maxQuantity == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The MAX_QUANTITY attribute is missing for the service template " + orderLineDto.getBillCode());
            return false;
        }

        BigDecimal billQuantity = new BigDecimal(orderLineDto.getBillQuantity());
        if (billQuantity.compareTo(BigDecimal.ZERO) < 0) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The service quantity must be greater than 0 for the service : " + orderLineDto.getBillCode());
            return false;
        }

        //setChargeType(orderLineDto, serviceTemplate);

        return true;
    }

    /**
     * Validate offer template
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the The order line Dto
     * @return true is offer template is valid.
     * @throws BusinessException the business exception
     */
    private boolean validateOfferTemplate(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
        if (serviceTemplate == null) {
            return false;
        }
        OfferTemplate offerTemplate = getOfferTemplate(orderDto, serviceTemplate);
        if (offerTemplate == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The offer is not found for a billCode : " + orderLineDto.getBillCode());
            return false;
        }
        orderLineDto.setOfferTemplateCode(offerTemplate.getCode());
        return true;
    }

    /**
     * Validate billing account
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the The order line Dto
     * @return true is billing account is valid.
     * @throws BusinessException the business exception
     */
    private boolean validateBillingAccount(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        if (StringUtils.isBlank(orderLineDto.getMasterAccountNumber())) {
            // Master Account Guiding is only for provide orders
            if (!Arrays.asList(BillActionEnum.P.name(), BillActionEnum.CNS.name(), BillActionEnum.TNS.name(), BillActionEnum.CU.name()).contains(orderLineDto.getBillAction()) ||
                    (BillActionEnum.P.name().equalsIgnoreCase(orderLineDto.getBillAction()) && !orderLineDto.isMain())) {
                Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
                if (subscription != null) {
                    orderLineDto.setMasterAccountNumber(subscription.getUserAccount().getBillingAccount().getCode());
                }
            }

            if (StringUtils.isBlank(orderLineDto.getMasterAccountNumber())) {
                orderLineDto.setMasterAccountNumber(getProductSetPrefixCode(orderDto, orderLineDto));
            }

            if (orderService.findEntityByIdOrCode(billingAccountService, orderDto, orderLineDto.getMasterAccountNumber()) == null
                    && StringUtils.isNotBlank(orderLineDto.getUan())
                    && Arrays.asList(BillActionEnum.P.name(), BillActionEnum.CNS.name(),
                    BillActionEnum.TNS.name()).contains(orderLineDto.getBillAction())) {
                String prefixCode = getProductSetCode(orderDto, orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode()));
                if ("WLR".equalsIgnoreCase(prefixCode)) {
                    List<Subscription> subscriptions = subscriptionService.findSubscriptionsByUAN(orderLineDto.getUan());
                    Set<String> billingAccounts = new HashSet<>();
                    for (Subscription subscription : subscriptions) {
                        ServiceInstance mainServiceInstance = getMainServiceInstance(subscription);
                        if (mainServiceInstance != null && "WLR".equalsIgnoreCase(getProductSetCode(orderDto, mainServiceInstance.getServiceTemplate()))) {
                            billingAccounts.add(subscription.getUserAccount().getBillingAccount().getCode());
                        }
                    }
                    Boolean billingAccountFound = false;
                    if ((billingAccounts.isEmpty() || billingAccounts.size() != 1) && orderLineDto.getBillAction().equals(BillActionEnum.CNS.name())) {
                        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getOriginalSubscriptionCode());
                        if (subscription != null) {
                            orderLineDto.setMasterAccountNumber(subscription.getUserAccount().getBillingAccount().getCode());
                            billingAccountFound = true;
                        } else {
                            setError(orderDto, ErrorEnum.INVALID_DATA, "Subscriber Billing to Account Guiding - No subscription found :" + orderLineDto.getSubscriptionCode());
                            return false;
                        }
                    }

                    if (!billingAccountFound) {
                        if (billingAccounts.isEmpty()) {
                            setError(orderDto, ErrorEnum.INVALID_DATA, "No subscription found with the UAN : " + orderLineDto.getUan() + ", having a WLR main service");
                            return false;
                        }

                        if (billingAccounts.size() == 1) {
                            orderLineDto.setMasterAccountNumber(billingAccounts.iterator().next());
                        } else {
                            setError(orderDto, ErrorEnum.INVALID_DATA, "More than one billing account found for UAN : " + orderLineDto.getUan());
                            return false;
                        }
                    }
                }
            }
        }

        orderLineDto.setBillingAccountCode(orderLineDto.getMasterAccountNumber());

        // Check Billing Account (Master_Account_Number)
        BillingAccount billingAccount = orderService.findEntityByIdOrCode(billingAccountService, orderDto, orderLineDto.getMasterAccountNumber());
        if (billingAccount == null) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The billing account " + orderLineDto.getMasterAccountNumber() + " is not found");
            return false;
        }

        // Check Customer (Operator_Id)
        String operatorId = (String) billingAccount.getCfValue(BillingAccountCFsEnum.OAO_ID.name());
        if (!orderLineDto.getOperatorId().equalsIgnoreCase(operatorId) && orderLineDto.isMain()) {
            setError(orderDto, ErrorEnum.INVALID_DATA, "OAOID " + orderLineDto.getOperatorId() + " does not match the Billing Account");
            return false;
        }
        return true;
    }

    /**
     * Validate subscription
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the The order line Dto
     * @return true is subscription is valid.
     * @throws BusinessException the business exception
     */
    private boolean validateSubscription(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        if (!StringUtils.isBlank(orderLineDto.getSubscriptionCode())) {
            if (StringUtils.isBlank(orderLineDto.getOriginalSubscriptionCode())) {
                orderLineDto.setOriginalSubscriptionCode(orderLineDto.getSubscriptionCode());
                orderLineDto.setSubscriptionCode(getSubscriptionCodeWithPrefix(orderDto, orderLineDto));
            }
            Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
            if (subscription == null) {
                if (orderLineDto.isMain() && BillActionEnum.P.name().equalsIgnoreCase(orderLineDto.getOriginalOrderAction())) {
                    orderDto.getProvideMainSubscriptionCodes().add(orderLineDto.getSubscriptionCode());
                } else if (!orderDto.getProvideMainSubscriptionCodes().contains(orderLineDto.getSubscriptionCode())) {
                    setError(orderDto, ErrorEnum.INVALID_DATA, "The subscription " + orderLineDto.getSubscriptionCode() + " is not found");
                    return false;
                }
            } else if (subscription.getStatus() != SubscriptionStatusEnum.ACTIVE) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "The subscription " + orderLineDto.getSubscriptionCode() + " is no longer active");
                return false;
            }
        } else {
            setError(orderDto, ErrorEnum.INVALID_DATA, "The Service_Id parameter is missing");
            return false;
        }
        return true;
    }

    @Override
    protected void validateOrderLinePrerequisites(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        if (BillActionEnum.M.name().equalsIgnoreCase(orderLineDto.getBillAction()) || BillActionEnum.RB.name().equalsIgnoreCase(orderLineDto.getBillAction())) {
            return;
        }

        if ("ALL".equalsIgnoreCase(orderLineDto.getBillCode())) {
            orderLineDto.setOriginalSubscriptionCode(orderLineDto.getSubscriptionCode());
            orderLineDto.setSubscriptionCode(getSubscriptionCodeWithPrefix(orderDto, orderLineDto));
            return;
        }

        // Check service template (service template is mandatory)
        if (!validateServiceTemplate(orderDto, orderLineDto)) {
            return;
        }

        // Check Offer (Offer is not mandatory)
        if (!validateOfferTemplate(orderDto, orderLineDto)) {
            return;
        }

        // Check the subscription
        if (!validateSubscription(orderDto, orderLineDto)) {
            return;
        }

        // Check billing account (billing account is mandatory)
        if (!validateBillingAccount(orderDto, orderLineDto)) {
            return;
        }
    }

    @Override
    protected void validateOrderLine(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        /** Item_Id **/
        if (!validateField(orderDto, "Item_Id", orderLineDto.getItemId(), null, String.class, 50, true)) {
            return;
        }
        /** Operator_Id **/
        if (!validateField(orderDto, "Operator_Id", orderLineDto.getOperatorId(), null, String.class, 10, true)) {
            return;
        }
        /** Item_Component_Id **/
        if (!validateField(orderDto, "Item_Component_Id", orderLineDto.getItemComponentId(), null, String.class, 15, false)) {
            return;
        }
        /** Item_Version **/
        if (!validateField(orderDto, "Item_Version", orderLineDto.getItemVersion(), null, String.class, 3, false)) {
            return;
        }
        /** Item_Type **/
        if (!validateField(orderDto, "Item_Type", orderLineDto.getItemType(), null, String.class, 50, false)) {
            return;
        }
        /** Agent_Code **/
        if (!validateField(orderDto, "Agent_Code", orderLineDto.getAgentCode(), null, String.class, 8, false)) {
            return;
        }
        /** UAN **/
        if (!validateField(orderDto, "UAN", orderLineDto.getUan(), null, String.class, 20, false)) {
            return;
        }
        /** Master_Account_Number **/
        if (!validateField(orderDto, "Master_Account_Number", orderLineDto.getMasterAccountNumber(), null, String.class, 20, false)) {
            return;
        }
        /** Service_Id **/
        if (!validateField(orderDto, "Service_Id", orderLineDto.getSubscriptionCode(), null, String.class, 30, false)) {
            return;
        }
        /** Ref_Service_Id **/
        if (!validateField(orderDto, "Ref_Service_Id", orderLineDto.getRefSubscriptionCode(), null, String.class, 30, false)) {
            return;
        }
        /** Xref_Service_Id **/
        if (!validateField(orderDto, "Xref_Service_Id", orderLineDto.getXRefSubscriptionCode(), null, String.class, 30, false)) {
            return;
        }
        /** Bill_Code **/
        if (!validateField(orderDto, "Bill_Code", orderLineDto.getBillCode(), null, String.class, 100, true)) {
            return;
        }
        /** Action **/
        if (!validateField(orderDto, "Action", orderLineDto.getBillAction(), null, String.class, 10, true)) {
            return;
        }
        /*
        if (!BillActionEnum.contains(orderLineDto.getBillAction())) {
            setBadRequestError(orderDto, ErrorEnum.BAD_REQUEST_INVALID_VALUE, "Action");
            return;
        }
        */
        /** Action_Qualifier **/
        if (!validateField(orderDto, "Action_Qualifier", orderLineDto.getBillActionQualifier(), null, String.class, 50, false)) {
            return;
        }
        /** Quantity **/
        if (!validateField(orderDto, "Quantity", orderLineDto.getBillQuantity(), null, Integer.class, 5, true)) {
            return;
        }
        /** Effect_Date **/
        if (!validateField(orderDto, "Effect_Date", orderLineDto.getBillEffectDate(), null, Date.class, null, true)) {
            return;
        }
        /** Special_Connect **/
        if (!validateField(orderDto, "Special_Connect", orderLineDto.getSpecialConnect(), null, Integer.class, 20, false)) {
            return;
        }
        /** Special_Rental **/
        if (!validateField(orderDto, "Special_Rental", orderLineDto.getSpecialRental(), null, Integer.class, 20, false)) {
            return;
        }
        /** Operator_Order_Number **/
        if (!validateField(orderDto, "Operator_Order_Number", orderLineDto.getOperatorOrderNumber(), null, String.class, 20, false)) {
            return;
        }

        for (AttributeDto attributeDto : orderLineDto.getAttributes()) {

            /** Code **/
            if (!validateField(orderDto, "Code", attributeDto.getCode(), null, String.class, 50, true)) {
                return;
            }
            /** Value **/
            if (!validateField(orderDto, "Value", attributeDto.getValue(), null, String.class, 80, true)) {
                return;
            }
            /** Unit **/
            if (!validateField(orderDto, "Unit", attributeDto.getUnit(), null, String.class, 10, false)) {
                return;
            }
            /** Component_Id **/
            if (!validateField(orderDto, "Component_Id", attributeDto.getComponentId(), null, String.class, 15, false)) {
                return;
            }
            /** Attribute_Type **/
            if (!validateField(orderDto, "Attribute_Type", attributeDto.getAttributeType(), null, String.class, 50, false)) {
                return;
            }
        }
        validateOrderLinePrerequisites(orderDto, orderLineDto);
    }

    @Override
    protected void validateOrder(OrderDto orderDto) {

        // Change and transfer Cases
        if (orderDto.getErrorCode() != null) {
            return;
        }

        resetOrder(orderDto);

        /** Ordering_System **/
        if (!validateField(orderDto, "Ordering_System", orderDto.getOrderingSystem(), OrderEmitterEnum.UG.name(), String.class, 5, true)) {
            return;
        }
        /** Order_Id **/
        if (!validateField(orderDto, "Order_Id", orderDto.getOrderCode(), null, String.class, 15, true)) {
            return;
        }
        /** Ref_Order_Id **/
        if (!validateField(orderDto, "Ref_Order_Id", orderDto.getRefOrderCode(), null, String.class, 15, false)) {
            return;
        }
        /** Transaction_Id **/
        if (!validateField(orderDto, "Transaction_Id", orderDto.getTransactionId(), null, String.class, 25, true)) {
            return;
        }

        // Check if the order is too large
        if (isOrderTooLarge(orderDto)) {
            return;
        }

        if (orderDto.getStatus() == OrderStatusEnum.IN_CREATION && isOrderAlreadyExists(orderDto)) {
            return;
        }

        for (OrderLineDto orderLineDto : orderDto.getOrderLines()) {
            if (orderDto.getErrorCode() != null) {
                return;
            }
            validateOrderLine(orderDto, orderLineDto);
        }

        checkDataDiscrepancies(orderDto);
    }

    /**
     * To process the order in new Transaction and if an error is found we Rollback all order items.
     * but we can save the order with error in the next service by joining the current transaction.
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    public void processOrderLines(OrderDto orderDto) throws BusinessException {

        int i = 1;
        EntityManager em = getEntityManager();
        for (OrderLineDto orderLineDto : orderDto.getOrderLines()) {

            processOrderLine(orderDto, orderLineDto);

            if (i % 100 == 0) {
                em.flush();
                em.clear();
            }
            i++;
        }
    }

    /**
     * Process a single order item
     *
     * @param orderDto     Order DTO
     * @param orderLineDto Order line DTO
     */
    private void processOrderLine(OrderDto orderDto, OrderLineDto orderLineDto) {

        /**
         * [Process Cease Request - Ref: step 124, page 4]: An ancillary service should exist against an active subscriber when a Cease request is received.
         * Action: If the ancillary service is already ceased or does not exist, mark the order bill item as complete and create a Data Discrepancy Record
         * and continue processing
         */
        if (!validateServiceInstance(orderDto, orderLineDto, new String[]{BillActionEnum.C.name(), BillActionEnum.COS.name(), BillActionEnum.TOS.name()})) {
            return;
        }


        if (orderDto.getErrorCode() == null) {
            switch (BillActionEnum.valueOfIgnoreCase(orderLineDto.getBillAction())) {
                case CH: // Change class of service
                    changeClassOfService(orderDto, orderLineDto);
                    break;
                case C:
                    if (ActionQualifierEnum.TOS.name().equalsIgnoreCase(orderLineDto.getBillActionQualifier())) {
                        suspendService(orderDto, orderLineDto);
                        break;
                    }
                    terminateService(orderDto, orderLineDto);
                    break;
                case P:
                    provideService(orderDto, orderLineDto);
                    break;
                case TOS:// Transfer
                case TNS:
                    transferService(orderDto, orderLineDto);
                    break;
                case COS:// Change
                case CNS:
                    changeService(orderDto, orderLineDto);
                    break;
                case CU:
                    changeUAN(orderDto, orderLineDto);
                    break;
                case CN:
                    changeNumber(orderDto, orderLineDto);
                    break;
                case SN: // Swap
                    swapNumber(orderDto, orderLineDto);
                    break;
                case RB:
                case M:
                    miscCharge(orderDto, orderLineDto);
                    break;
            }
        }

        if (orderDto.getErrorCode() != null) {
            // Rollback transaction
            throw new ValidationException(orderDto.getErrorMessage());
        }

    }

    @Override
    protected void executeOrder(OrderDto orderDto) throws BusinessException {
        // Don't execute order if there is at least one error or the order is on hold and not released.
        if (isHeldOrder(orderDto) || orderDto.getErrorCode() != null) {
            return;
        }

        processOrderLines(orderDto);

        if (orderDto.getErrorCode() == null) {
            orderDto.setStatus(OrderStatusEnum.COMPLETED);
        }
    }

    /**
     * Initialize the order context
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    @Override
    protected void init(OrderDto orderDto) {
        // Initialize the order Dto
        initOrder(orderDto);
        // Sort Order lines
        sortOrderLines(orderDto);
    }

    @Override
    protected void sortOrderLines(OrderDto orderDto) {
        Collections.sort(orderDto.getOrderLines(), new Comparator<OrderLineDto>() {
            @Override
            public int compare(OrderLineDto l1, OrderLineDto l2) {
                return getPosition(l1) - getPosition(l2);
            }

            private int getPosition(OrderLineDto l1) {
                switch (BillActionEnum.valueOfIgnoreCase(l1.getBillAction())) {
                    /** Change case **/
                    case CH:
                        return 1;

                    /** Cease cases **/
                    case TOS:
                    case COS:
                        if (!l1.isMain()) {
                            return 2; // cease the old main service (transfer (TOS) or change (COS))
                        }
                        return 4; // cease the old ancillary service (transfer (TOS) or change (COS))
                    case C:
                        if (!l1.isMain()) {
                            return 3;
                        }
                        return 5;

                    /** Provide cases **/
                    case TNS:
                    case CNS:
                        if (l1.isMain()) {
                            return 6; // provide the new main service (transfer (TNS) or change (CNS))
                        }
                        return 8; // provide the new ancillary service (transfer (TNS) or change (CNS))
                    case P:
                        if (l1.isMain()) {
                            return 7;
                        }
                        return 9;

                    /** Change UAN case **/
                    case CU:
                        return 10;
                    /** Change Number case **/
                    case CN:
                        return 11;
                    /** Swap Number case **/
                    case SN:
                        return 12;
                    /** Miscellaneous charge case **/
                    case M:
                        return 13;
                }
                /** each other actions will be at the end of list **/
                return 14;
            }
        });
    }

    @Override
    protected boolean isService(OrderLineDto orderLineDto) {
        return true;
    }

    @Override
    protected ServiceInstance createServiceInstance(OrderDto orderDto, OrderLineDto orderLineDto) {
        ServiceInstance serviceInstance = new ServiceInstance();
        serviceInstance.setCode(orderLineDto.getBillCode());
        serviceInstance.setOrderNumber(orderDto.getOrderCode());

        BigDecimal quantity = new BigDecimal(orderLineDto.getBillQuantity());
        serviceInstance.setQuantity(quantity);

        ServiceTemplate serviceTemplate = orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, orderLineDto.getBillCode());
        serviceInstance.setServiceTemplate(serviceTemplate);
        serviceInstance.setSubscription(orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode()));
        Date billEffectDate = DateUtils.truncateTime(orderLineDto.getBillEffectDate());
        serviceInstance.setSubscriptionDate(billEffectDate);

        if (orderLineDto.isMain()) {
            Long agreementDuration = getEngagementDuration(orderDto, orderLineDto);
            if (agreementDuration != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(billEffectDate);
                calendar.add(Calendar.MONTH, agreementDuration.intValue());
                serviceInstance.setEndAgreementDate(calendar.getTime());
            }
        }
        setServiceInstanceCf(orderDto, orderLineDto, serviceInstance);
        return serviceInstance;
    }

    /**
     * update change number history
     *
     * @param orderLineDto
     * @param subscription
     */
    private void updateChangeNumberHistory(OrderLineDto orderLineDto, Subscription subscription) {
        Map<String, Object> cfMap = new HashMap<>();
        String oldTelNumber = orderLineDto.getXRefSubscriptionCode() != null ? orderLineDto.getXRefSubscriptionCode() : orderLineDto.getRefSubscriptionCode();
        oldTelNumber = subscription.getCode().substring(0, subscription.getCode().indexOf("_") + 1) + oldTelNumber;
        cfMap.put(SubscriptionCFsEnum.CHANGE_TEL.name(),
                DateUtils.formatDateWithPattern(new Date(), Utils.DATE_TIME_PATTERN) + "|"
                        + orderLineDto.getBillEffectDate() + "|" + oldTelNumber + "|"
                        + subscription.getCode()
                        + "|" + "DONE");
        DatePeriod datePeriod = new DatePeriod(orderLineDto.getBillEffectDate(), null);
        subscription.getCfValuesNullSafe().setValue(SubscriptionCFsEnum.CHANGE_TEL.name(), datePeriod, 0, cfMap);
        subscriptionService.update(subscription);
    }

    /**
     * Get the end point
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return the end point
     * @throws BusinessException the business exception
     */
    protected Map<String, Object> getEndPoint(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        Map<String, Object> endPoint = new HashMap<>();
        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        endPoint.put(EndPointCustomTableEnum.ITEM_ID.getLabel(), orderLineDto.getItemId());
        endPoint.put(EndPointCustomTableEnum.ITEM_COMPONENT_ID.getLabel(), orderLineDto.getItemComponentId());
        endPoint.put(EndPointCustomTableEnum.SUBSCRIPTION_ID.getLabel(), subscription.getId());
        endPoint.put(EndPointCustomTableEnum.EPSTART.getLabel(), DateUtils.truncateTime(orderLineDto.getBillEffectDate()));
        return endPoint;
    }

    /**
     * Update end point address
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @param subscription the subscription
     * @param epaddress    the endpoint address
     * @param epend        endpoint end
     * @return the end point
     * @throws BusinessException the business exception
     */
    private Map<String, Object> UpdateEndpointAddresse(OrderDto orderDto, OrderLineDto orderLineDto, Subscription subscription, String epaddress, EndEnum epend)
            throws BusinessException {

        // The X_END_ARD_ID (X = A or B) attribute will contain the Address ID of the endpoint X (A or B). this will contain the ARD Id (e.g. 904694).
        Map<String, Object> address = null;
        if (Utils.isInteger(epaddress)) {
            address = getEndPointAddress(orderDto, epaddress);
        }
        if (address == null) {
            return null;
        }
        Map<String, Object> endPoint = getEndPoint(orderDto, orderLineDto, address);
        endPoint.put(EndPointCustomTableEnum.EPEND.getLabel(), epend.name());
        endPoint.put(EndPointCustomTableEnum.EPADDRESS.getLabel(), epaddress);
        updateEndPoint(orderDto, orderLineDto, subscription, endPoint, epend.name(), null, orderLineDto.getBillEffectDate(), null);
        return endPoint;
    }

    /**
     * Update end points address
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    private void UpdateEndpointAddresses(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        Subscription subscription = orderService.findEntityByIdOrCode(subscriptionService, orderDto, orderLineDto.getSubscriptionCode());
        if (subscription == null) {
            return;
        }

        // The A_END_ARD_ID attribute will contain the Address ID of the endpoint A. this will contain the ARD Id (e.g. 904694).
        Map<String, Object> endPointA = UpdateEndpointAddresse(orderDto, orderLineDto, subscription,
                orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_A_END_ARD_ID.getLabel()), EndEnum.A);

        if (orderDto.getErrorCode() != null || endPointA == null) {
            return;
        }

        // The B_END_ARD_ID attribute will contain the Address ID of the endpoint B. this will contain the ARD Id (e.g. 904694).
        UpdateEndpointAddresse(orderDto, orderLineDto, subscription, orderLineDto.getAttribute(OrderLineAttributeNamesEnum.UG_B_END_ARD_ID.getLabel()), EndEnum.B);

    }

    /**
     * Terminate service
     *
     * @param orderDto          the order Dto
     * @param orderLineDto      the order line Dto
     * @param serviceInstance   the service instance
     * @param terminationReason the termination reason
     * @param terminationDate   the termination date
     * @param serviceToApply    the service to apply
     * @throws BusinessException the business exception
     */
    private void terminateService(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance, SubscriptionTerminationReason terminationReason,
                                  Date terminationDate, String serviceToApply) throws BusinessException {
        if (terminationReason == null) {
            return;
        }
        if (orderLineDto.getCeaseEffectDate() == null) {
            orderLineDto.setCeaseEffectDate(orderLineDto.getBillEffectDate());
            if (terminationReason.isApplyAgreement()) {
                orderLineDto.setCeaseEffectDate(serviceInstance.getEndAgreementDate());
            }
        }
        updateServiceParametersCF(orderDto, orderLineDto, serviceInstance, true);
        serviceInstanceService.terminateService(serviceInstance, terminationDate, terminationReason, orderDto.getOrderCode());
        serviceInstance.setQuantity(BigDecimal.ZERO);
        serviceInstanceService.update(serviceInstance);
        if (!terminationReason.isApplyAgreement() && !terminationReason.isApplyTerminationCharges()) {
            applyService(orderDto, orderLineDto, serviceToApply);
        }
    }

    /**
     * Rerate
     *
     * @param serviceInstance   the service instance
     * @param orderDto          the order Dto
     * @param orderLineDto      the order line Dto
     * @param serviceInstance   the service instance
     * @param terminationReason the termination reason
     * @throws BusinessException the business exception
     */
    private void rerate(OrderDto orderDto, OrderLineDto orderLineDto, ServiceInstance serviceInstance, SubscriptionTerminationReason terminationReason)
            throws BusinessException {
        CustomFieldValue lastCfValue = utilService.getLastCfVersion(serviceInstance, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(),
                DateUtils.truncateTime(orderLineDto.getBillEffectDate()), true);
        updateServiceParametersCF(orderDto, orderLineDto, serviceInstance, lastCfValue, true);
        //serviceInstance = serviceInstanceService.update(serviceInstance);
        reratingService.rerate(serviceInstance, orderLineDto.getBillEffectDate(), true);
        if (terminationReason != null) {
            TerminationReasonEnum terminationReasonEnum = TerminationReasonEnum.valueOf(terminationReason.getCode());
            if (terminationReasonEnum.isApplyTerminationCharges()) {
                // Calculate a delta quantity
                Map<String, String> serviceParametersCF = getServiceParameters(serviceInstance, lastCfValue);
                BigDecimal quantity = null;
                if (serviceParametersCF != null) {
                    quantity = new BigDecimal(serviceParametersCF.get(ServiceParametersCFEnum.QUANTITY.getLabel())).subtract(new BigDecimal(orderLineDto.getBillQuantity()));
                }
                if (quantity != null && quantity.compareTo(BigDecimal.ZERO) > 0) {
                    applyTerminationCharge(orderDto, orderLineDto, serviceInstance);
                }
            }
        }
    }

    /**
     * Apply the service
     *
     * @param orderDto       the order Dto
     * @param orderLineDto   the order line Dto
     * @param serviceToApply the service to apply
     * @throws BusinessException the business exception
     */
    private void applyService(OrderDto orderDto, OrderLineDto orderLineDto, String serviceToApply) throws BusinessException {
        if (!StringUtils.isBlank(serviceToApply)) {
            if (orderService.findEntityByIdOrCode(serviceTemplateService, orderDto, serviceToApply) == null) {
                setError(orderDto, ErrorEnum.INVALID_DATA, "Service is not found : " + serviceToApply);
                return;
            }
            OrderLineDto oneShotOrderLineDTO = SerializationUtils.clone(orderLineDto);
            oneShotOrderLineDTO.setBillCode(serviceToApply);
            applyMiscOneShotCharge(orderDto, oneShotOrderLineDTO);
        }
    }


    @Override
    protected BaseOrderApi getSelfForNewTx() {
        return ugOrderApi;
    }

    /**
     * Update subscription cfs
     *
     * @param orderLineDto the order line Dto
     * @param subscription the subscription
     */
    protected void updateSubscriptionCfs(OrderLineDto orderLineDto, Subscription subscription) {
        subscription.setCfValue(SubscriptionCFsEnum.ORIGINAL_CODE.name(), orderLineDto.getOriginalSubscriptionCode());
        subscription.setCfValue(SubscriptionCFsEnum.UAN.name(), new DatePeriod(orderLineDto.getBillEffectDate(), null), null, orderLineDto.getUan());
        subscription.setCfValue(SubscriptionCFsEnum.AGENT_CODE.name(), orderLineDto.getAgentCode());
        subscription.setCfValue(SubscriptionCFsEnum.OAO_ORDERNO.name(), orderLineDto.getOperatorOrderNumber());
        utilService.setServiceIdCF(subscription);
    }
}