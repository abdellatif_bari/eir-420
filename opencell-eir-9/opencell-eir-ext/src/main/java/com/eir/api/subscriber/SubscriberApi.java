package com.eir.api.subscriber;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.meveo.admin.exception.BusinessException;
import org.meveo.api.BaseApi;
import org.meveo.api.dto.subscriber.QuotesRequestDto;
import org.meveo.api.dto.subscriber.QuotesResponseDto;
import org.meveo.api.dto.subscriber.ServiceChargesRequestDto;
import org.meveo.api.dto.subscriber.ServiceChargesResponseDto;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;

import com.eir.service.subscriber.SubscriberApiService;

/**
 * The subscriber API.
 *
 * @author Mohammed Amine TAZI
 * @author Hatim OUDAD
 */
@Stateless
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class SubscriberApi extends BaseApi {

    @Inject
    private SubscriberApiService subscriberApiService;
    
	/**
	 * List service charges by subscriber
	 * @param serviceChargesRequestDto service charges request
	 * @return all services charges belonging to the subscription found
	 * @throws BusinessException Application Exception
	 */
	public ServiceChargesResponseDto listServiceCharges(ServiceChargesRequestDto serviceChargesRequestDto) throws BusinessException {
        ServiceChargesResponseDto serviceChargesResponseDto = null;
	    try {
    	    serviceChargesResponseDto = subscriberApiService.listServiceCharges(serviceChargesRequestDto);
	    } catch(Exception ex) {
	        throw new BusinessException(ex);
	    } finally {
	        subscriberApiService.storeRequest(serviceChargesRequestDto, serviceChargesResponseDto);
	    }
	    return serviceChargesResponseDto;
	}
	
	/**
	 * List quotes
	 * @param serviceChargesRequestDto service charges request
	 * @return all services charges belonging to the subscription found
	 * @throws BusinessException Application Exception
	 */
	public QuotesResponseDto listQuotes(QuotesRequestDto quotesRequestDto) throws BusinessException {
        QuotesResponseDto quotesResponseDto = null;
	    try {
    	    quotesResponseDto = subscriberApiService.listQuotes(quotesRequestDto);
	    } catch(Exception ex) {
	        throw new BusinessException(ex);
	    } finally {
	        subscriberApiService.storeRequest(quotesRequestDto, quotesResponseDto);
	    }
	    return quotesResponseDto;
	}

	
}
