package com.eir.api.order;

import com.eir.commons.enums.OrderEmitterEnum;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.api.commons.ErrorEnum;
import org.meveo.api.dto.order.AttributeDto;
import org.meveo.api.dto.order.OrderDto;
import org.meveo.api.dto.order.OrderLineDto;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.order.OrderStatusEnum;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.interceptor.Interceptors;
import java.util.Date;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * The WSLAM order API.
 *
 * @author Abdellatif BARI
 */
@Stateless
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class WSLAMOrderApi extends BaseOrderApi {

    @EJB
    WSLAMOrderApi wslamOrderApi;

    /**
     * Bill action enum
     */
    protected enum BillActionEnum {
        RB;

        private static Set<String> _values = new HashSet<>();

        static {
            for (BaseOrderApi.BillActionEnum choice : BaseOrderApi.BillActionEnum.values()) {
                _values.add(choice.name());
            }
        }

        public static boolean contains(String value) {
            return !StringUtils.isBlank(value) && _values.contains(value.toUpperCase());
        }

        public static BillActionEnum valueOfIgnoreCase(String billAction) {
            if (!StringUtils.isBlank(billAction)) {
                billAction = billAction.toUpperCase();
            }
            return valueOf(billAction);
        }
    }

    /**
     * Validate order line prerequisites
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the The order line Dto
     * @throws BusinessException the business exception
     */
    @Override
    protected void validateOrderLinePrerequisites(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

    }

    /**
     * Validate order line
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @throws BusinessException the business exception
     */
    @Override
    protected void validateOrderLine(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {

        /** Item_Id **/
        if (!validateField(orderDto, "Item_Id", orderLineDto.getItemId(), null, String.class, 50, true)) {
            return;
        }
        /** Operator_Id **/
        if (!validateField(orderDto, "Operator_Id", orderLineDto.getOperatorId(), null, String.class, 10, true)) {
            return;
        }
        /** Item_Component_Id **/
        if (!validateField(orderDto, "Item_Component_Id", orderLineDto.getItemComponentId(), null, String.class, 15, false)) {
            return;
        }
        /** Item_Version **/
        if (!validateField(orderDto, "Item_Version", orderLineDto.getItemVersion(), null, String.class, 3, false)) {
            return;
        }
        /** Item_Type **/
        if (!validateField(orderDto, "Item_Type", orderLineDto.getItemType(), null, String.class, 50, false)) {
            return;
        }
        /** Agent_Code **/
        if (!validateField(orderDto, "Agent_Code", orderLineDto.getAgentCode(), null, String.class, 8, false)) {
            return;
        }
        /** UAN **/
        if (!validateField(orderDto, "UAN", orderLineDto.getUan(), null, String.class, 20, false)) {
            return;
        }
        /** Master_Account_Number **/
        if (!validateField(orderDto, "Master_Account_Number", orderLineDto.getMasterAccountNumber(), null, String.class, 20, false)) {
            return;
        }
        /** Service_Id **/
        if (!validateField(orderDto, "Service_Id", orderLineDto.getSubscriptionCode(), null, String.class, 30, false)) {
            return;
        }
        /** Ref_Service_Id **/
        if (!validateField(orderDto, "Ref_Service_Id", orderLineDto.getRefSubscriptionCode(), null, String.class, 30, false)) {
            return;
        }
        /** Xref_Service_Id **/
        if (!validateField(orderDto, "Xref_Service_Id", orderLineDto.getXRefSubscriptionCode(), null, String.class, 30, false)) {
            return;
        }
        /** Bill_Code **/
        if (!validateField(orderDto, "Bill_Code", orderLineDto.getBillCode(), null, String.class, 100, true)) {
            return;
        }
        /** Action **/
        if (!validateField(orderDto, "Action", orderLineDto.getBillAction(), null, String.class, 10, true)) {
            return;
        }
        if (!BillActionEnum.contains(orderLineDto.getBillAction())) {
            setBadRequestError(orderDto, ErrorEnum.BAD_REQUEST_INVALID_VALUE, "Action");
            return;
        }
        /** Action_Qualifier **/
        if (!validateField(orderDto, "Action_Qualifier", orderLineDto.getBillActionQualifier(), null, String.class, 50, false)) {
            return;
        }
        /** Quantity **/
        if (!validateField(orderDto, "Quantity", orderLineDto.getBillQuantity(), null, Integer.class, 5, true)) {
            return;
        }
        /** Effect_Date **/
        if (!validateField(orderDto, "Effect_Date", orderLineDto.getBillEffectDate(), null, Date.class, null, true)) {
            return;
        }
        /** Special_Connect **/
        if (!validateField(orderDto, "Special_Connect", orderLineDto.getSpecialConnect(), null, Integer.class, 20, false)) {
            return;
        }
        /** Special_Rental **/
        if (!validateField(orderDto, "Special_Rental", orderLineDto.getSpecialRental(), null, Integer.class, 20, false)) {
            return;
        }
        /** Operator_Order_Number **/
        if (!validateField(orderDto, "Operator_Order_Number", orderLineDto.getOperatorOrderNumber(), null, String.class, 20, false)) {
            return;
        }

        for (AttributeDto attributeDto : orderLineDto.getAttributes()) {

            /** Code **/
            if (!validateField(orderDto, "Code", attributeDto.getCode(), null, String.class, 50, true)) {
                return;
            }
            /** Value **/
            if (!validateField(orderDto, "Value", attributeDto.getValue(), null, String.class, 80, true)) {
                return;
            }

            /** Unit **/
            if (!validateField(orderDto, "Unit", attributeDto.getUnit(), null, String.class, 10, false)) {
                return;
            }
            /** Component_Id **/
            if (!validateField(orderDto, "Component_Id", attributeDto.getComponentId(), null, String.class, 15, false)) {
                return;
            }
            /** Attribute_Type **/
            if (!validateField(orderDto, "Attribute_Type", attributeDto.getAttributeType(), null, String.class, 50, false)) {
                return;
            }
        }
        validateOrderLinePrerequisites(orderDto, orderLineDto);
    }

    /**
     * Validate order
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    @Override
    protected void validateOrder(OrderDto orderDto) throws BusinessException {

        resetOrder(orderDto);

        /** Ordering_System **/
        if (!validateField(orderDto, "Ordering_System", orderDto.getOrderingSystem(), OrderEmitterEnum.WSLAM.name(), String.class, 5, true)) {
            return;
        }
        /** Order_Id **/
        if (!validateField(orderDto, "Order_Id", orderDto.getOrderCode(), null, String.class, 15, true)) {
            return;
        }
        /** Ref_Order_Id **/
        if (!validateField(orderDto, "Ref_Order_Id", orderDto.getRefOrderCode(), null, String.class, 15, false)) {
            return;
        }
        /** Transaction_Id **/
        if (!validateField(orderDto, "Transaction_Id", orderDto.getTransactionId(), null, String.class, 25, true)) {
            return;
        }

        // Check if the order is too large
        if (isOrderTooLarge(orderDto)) {
            return;
        }

        if (orderDto.getStatus() == OrderStatusEnum.IN_CREATION && isOrderAlreadyExists(orderDto)) {
            return;
        }

        for (OrderLineDto orderLineDto : orderDto.getOrderLines()) {
            if (orderDto.getErrorCode() != null) {
                return;
            }
            validateOrderLine(orderDto, orderLineDto);
        }
    }

    /**
     * To process the order in new Transaction and if an error is found we Rollback all order items. but we can save the order with error in the next service by joining the current
     * transaction.
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    private void processOrderLines(OrderDto orderDto) throws BusinessException {
        for (OrderLineDto orderLineDto : orderDto.getOrderLines()) {

            if (orderDto.getErrorCode() == null) {
                switch (BillActionEnum.valueOfIgnoreCase(orderLineDto.getBillAction())) {
                    case RB:
                        miscCharge(orderDto, orderLineDto);
                        break;
                }
            }

            if (orderDto.getErrorCode() != null) {
                // Rollback transaction
                throw new ValidationException(orderDto.getErrorMessage());
            }
        }
    }

    /**
     * Execute created order by activating/changing/ceasing services in subscription
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    @Override
    protected void executeOrder(OrderDto orderDto) throws BusinessException {

        if (orderDto.getErrorCode() != null) {
            return;
        }

        processOrderLines(orderDto);

        if (orderDto.getErrorCode() == null) {
            if (orderDto.getStatus() != OrderStatusEnum.DEFERRED) {
                orderDto.setStatus(OrderStatusEnum.COMPLETED);
            }
        }
    }

    /**
     * Initialize the order context
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    @Override
    protected void init(OrderDto orderDto) {

        // Store original xml order
        setOriginalXmlOrder(orderDto);
        // Sort Order lines
        sortOrderLines(orderDto);
    }

    /**
     * Sort orderLines
     *
     * @param orderDto the order Dto
     */
    @Override
    protected void sortOrderLines(OrderDto orderDto) {
    }

    /**
     * Check if the item is a service or another thing like EndPoint, Association, Remark,......
     *
     * @param orderLineDto the The order line Dto
     * @return true if the item is a service
     */
    @Override
    protected boolean isService(OrderLineDto orderLineDto) {
        return false;
    }

    /**
     * Create service instance
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return service instance
     * @throws BusinessException the business exception
     */
    @Override
    protected ServiceInstance createServiceInstance(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        return null;
    }

    /**
     * Get the end point
     *
     * @param orderDto     the order Dto
     * @param orderLineDto the order line Dto
     * @return the end point
     * @throws BusinessException the business exception
     */
    @Override
    protected Map<String, Object> getEndPoint(OrderDto orderDto, OrderLineDto orderLineDto) throws BusinessException {
        return null;
    }

    @Override
    protected BaseOrderApi getSelfForNewTx() {
        return wslamOrderApi;
    }
}