package com.eir.api.taxMapping;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.math.BigInteger;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.api.BaseApi;
import org.meveo.api.custom.CustomTableApi;
import org.meveo.api.dto.custom.CustomTableRecordDto;
import org.meveo.api.dto.custom.UnitaryCustomTableDataDto;
import org.meveo.api.dto.response.PagingAndFiltering;
import org.meveo.api.dto.response.tax.TaxMappingListResponseDto;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.DatePeriod;
import org.meveo.model.bi.FlatFile;
import org.meveo.model.billing.Tax;
import org.meveo.model.customEntities.CustomEntityTemplate;
import org.meveo.model.shared.DateUtils;
import org.meveo.model.tax.TaxCategory;
import org.meveo.model.tax.TaxClass;
import org.meveo.model.tax.TaxMapping;
import org.meveo.service.bi.impl.FlatFileService;
import org.meveo.service.catalog.impl.TaxService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.tax.TaxCategoryService;
import org.meveo.service.tax.TaxClassService;
import org.meveo.service.tax.TaxMappingService;

import com.eir.service.taxMapping.TaxMapService;
import com.fasterxml.jackson.databind.ObjectMapper;

@Stateless(name = "EirTaxMapApi")
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class TaxMapApi extends BaseApi {

	@Inject
	private CustomTableApi customTableApi;

	@Inject
	private TaxCategoryService taxCategoryService;

	@Inject
	private TaxClassService taxClassService;

	@Inject
	private TaxService taxService;

	@Inject
	private TaxMapService taxMapService;

	@Inject
	private CustomTableService customTableService;

	@Inject
	private TaxMappingService taxMappingService;

	@Inject
	private FlatFileService flatFileService;

	/**
	 * Cancel tax mapping in a staging table that were imported from a given file
	 * name.
	 *
	 * @param filename the file name
	 * @throws BusinessException the business exception
	 */
	public void cancelTaxMapping(Long fileId) throws BusinessException {
		List<Map<String, Object>> taxMappingStages = getTaxMappingStageByFileId(fileId);
		Set<Long> ids = taxMappingStages.stream().map(entry -> {
			return ((BigInteger) entry.get("id")).longValue();
		}).collect(Collectors.toSet());
		customTableService.remove("EIR_TAX_MAPPING_STAGE", ids);

	}

	/**
	 * Get tax mapping in a staging table that were imported from a given file.
	 *
	 * @param fileId the file id
	 * @throws BusinessException the business exception
	 */
	private List<Map<String, Object>> getTaxMappingStageByFileId(Long fileId) {
		if (fileId == null) {
			throw new BusinessException("Missing file name.");
		}
		List<Map<String, Object>> taxMappingStages = taxMapService.findTaxMappingStageByFileId(fileId);

		if (taxMappingStages == null || taxMappingStages.isEmpty()) {
			throw new BusinessException("There is no tax mapping in the staging table for this file");
		}
		return taxMappingStages;
	}

	/**
	 * Activate tax mapping in a staging table that were imported from a given file
	 * name.
	 *
	 * @param filename the file name
	 * @throws BusinessException the business exception
	 * @throws ParseException
	 */
	public void activateTaxMapping(Long fileId) throws BusinessException, ParseException {
		List<Map<String, Object>> taxMappingStages = getTaxMappingStageByFileId(fileId);
		

		for (Map<String, Object> taxMappingStageMap : taxMappingStages) {

			if (!(taxMappingStageMap.get("error_reason") == null
					|| ((String) taxMappingStageMap.get("error_reason")).isBlank())) {
				throw new BusinessException("There is still errors in file" + fileId);
			}

			TaxMapping taxMapping = null;

			// record with add action
			if (taxMappingStageMap.get("record_action").equals("ADD")) {

				taxMapping = new TaxMapping();
				
				validateTaxMapping(taxMappingStageMap, taxMapping);
				taxMappingService.create(taxMapping);

				// Remove tax mapping from staging table
				CustomEntityTemplate cet = customTableService.getCET("EIR_TAX_MAPPING_STAGE");
				customTableService.remove(cet.getDbTablename(), ((BigInteger) taxMappingStageMap.get("id")).longValue());

				// record with MODIFY action
			} else if (taxMappingStageMap.get("record_action").equals("MODIFY")) {

				List<TaxMapping> activeTaxToModify = taxMapService.findActiveTaxMappings(
						(String) taxMappingStageMap.get("tax_class"), (String) taxMappingStageMap.get("tax_category"),
						(String) taxMappingStageMap.get("tax"));
				if (activeTaxToModify != null && !activeTaxToModify.isEmpty() && activeTaxToModify.get(0) != null) {
					taxMapping = activeTaxToModify.get(0);
				}
				if (activeTaxToModify != null && !activeTaxToModify.isEmpty() && taxMapping.getValid().getFrom().after(
						(Date)taxMappingStageMap.get("valid_to"))) {
					throw new BusinessException("We cannot have a stop date earlier than the start date");
				}
				if (taxMapping != null) {
					taxMapping.setValid(
							new DatePeriod(taxMapping.getValid().getFrom(), (Date) taxMappingStageMap.get("valid_to")));
					// Set the file name
					FlatFile flatFile = flatFileService.findById(((BigInteger) taxMappingStageMap.get("file_id")).longValue());
					if (flatFile == null) {
						throw new BusinessException("The file name is missing in staging table");
					}

					taxMapping.setSource(taxMapping.getSource() + "," + flatFile.getFileOriginalName());
					taxMappingService.update(taxMapping);

					// Remove tax mapping from staging table
					CustomEntityTemplate cet = customTableService.getCET("EIR_TAX_MAPPING_STAGE");
					customTableService.remove(cet.getDbTablename(), ((BigInteger) taxMappingStageMap.get("id")).longValue());
				}

			}

		}

	}

	/**
	 * 
	 * @param taxMappingStageDto
	 * @param taxMapping
	 * @throws ParseException
	 */
	private void validateTaxMapping(Map<String, Object> taxMappingStageMap, TaxMapping taxMapping)
			throws ParseException {
		if (taxMapping == null) {
			throw new BusinessException("No Tax mapping found with ID ");
		}

		// Set the file name
		FlatFile flatFile = flatFileService.findById(((BigInteger) taxMappingStageMap.get("file_id")).longValue());
		if (flatFile == null) {
			throw new BusinessException("The file name is missing in staging table");
		}
		taxMapping.setSource(flatFile.getFileOriginalName());

		// Tax category
		TaxCategory taxCategory = taxCategoryService.findByCode((String) taxMappingStageMap.get("tax_category"));
		if (taxCategory == null) {
			throw new BusinessException("Tax category " + taxMappingStageMap.get("tax_category") + "does not exist");
		}
		taxMapping.setAccountTaxCategory(taxCategory);

		// Tax class
		TaxClass taxClass = taxClassService.findByCode((String) taxMappingStageMap.get("tax_class"));
		if (taxClass != null) {
			taxMapping.setChargeTaxClass(taxClass);
		}

		// Tax
		Tax tax = taxService.findByCode((String) taxMappingStageMap.get("tax"));
		if (tax == null) {
			throw new BusinessException("Tax " + taxMappingStageMap.get("tax") + " does not exist");
		}
		taxMapping.setTax(tax);

		// Dates

		if (taxMappingStageMap.get("valid_from") != null && !StringUtils.isBlank(taxMappingStageMap.get("valid_from"))
				&& taxMappingStageMap.get("valid_to") != null
				&& !StringUtils.isBlank(taxMappingStageMap.get("valid_to"))
				&& ((Date)taxMappingStageMap.get("valid_from"))
						.after((Date)taxMappingStageMap.get("valid_to"))) {
			throw new BusinessException("We cannot have a stop date earlier than the start date");
		} else if(taxMappingStageMap.get("valid_from") != null && !StringUtils.isBlank(taxMappingStageMap.get("valid_from"))
				&& taxMappingStageMap.get("valid_to") != null
				&& !StringUtils.isBlank(taxMappingStageMap.get("valid_to"))) {
			taxMapping.setValid(new DatePeriod(
					(Date)taxMappingStageMap.get("valid_from"),
					(Date)taxMappingStageMap.get("valid_to")));
		}else {
					taxMapping.setValid(new DatePeriod(
					(Date)taxMappingStageMap.get("valid_from"),null));
		}
	}

	/**
	 * Export tax mapping records matching a given search criteria
	 *
	 * @param pagingAndFiltering Search and pagination criteria
	 */
	public void exportTaxMapping(PagingAndFiltering pagingAndFiltering) {
		// get Tax mappings to export
		List<TaxMapping> taxMappings = getTaxMappingList(pagingAndFiltering);
		// generate csv file
		generateTaxMappingFile(taxMappings);

	}

	/**
	 * get Tax Mapping List
	 * 
	 * @param pagingAndFiltering
	 * @return
	 */
	private List<TaxMapping> getTaxMappingList(PagingAndFiltering pagingAndFiltering) {
		List<TaxMapping> taxMappings = new ArrayList<TaxMapping>();

		String sortBy = "id";
		if (!StringUtils.isBlank(pagingAndFiltering.getSortBy())) {
			sortBy = pagingAndFiltering.getSortBy();
		}

		PaginationConfiguration paginationConfiguration = toPaginationConfiguration(sortBy,
				org.primefaces.model.SortOrder.ASCENDING, null, pagingAndFiltering, TaxMapping.class);

		Long totalCount = taxMappingService.count(paginationConfiguration);

		TaxMappingListResponseDto result = new TaxMappingListResponseDto();

		result.setPaging(pagingAndFiltering != null ? pagingAndFiltering : new PagingAndFiltering());
		result.getPaging().setTotalNumberOfRecords(totalCount.intValue());

		if (totalCount > 0) {
			taxMappings = taxMappingService.list(paginationConfiguration);
		}
		return taxMappings;
	}

	/**
	 * generate Tax Mapping File
	 * 
	 * @param taxMappings
	 */
	public void generateTaxMappingFile(List<TaxMapping> taxMappings) {

		try {
			String taxMappingFolder = "exports/tax_mapping/output";
			taxMappingFolder = paramBeanFactory.getChrootDir() + File.separator + taxMappingFolder;
			File f = new File(taxMappingFolder);
			if (!f.exists()) {
				f.mkdirs();
				log.info("tax mapping folder created :" + taxMappingFolder);
			}
			String taxMappingFileName = taxMappingFolder + File.separator + "Tax mapping_"
					+ DateUtils.formatDateWithPattern(new Date(), "ddMMyyyy") + ".csv";
			String separator = ",";
			FileWriter fw = null;
			BufferedWriter bw = null;

			fw = new FileWriter(taxMappingFileName, true);
			bw = new BufferedWriter(fw);

			bw.append("Tax Category" + separator + "Tax Class" + separator + "Tax" + separator + "Start Date"
					+ separator + "End Date" + "\r\n");

			for (TaxMapping taxMapping : taxMappings) {
				String taxCategoryCode = "";
				if (taxMapping.getAccountTaxCategory() != null) {
					taxCategoryCode = taxMapping.getAccountTaxCategory().getCode();
				}

				String taxClassCode = "";
				if (taxMapping.getAccountTaxCategory() != null) {
					taxClassCode = taxMapping.getChargeTaxClass().getCode();
				}

				String taxCode = "";
				if (taxMapping.getAccountTaxCategory() != null) {
					taxCode = taxMapping.getTax().getCode();
				}

				String startDate = "";
				if (taxMapping.getValid().getFrom() != null) {
					startDate = new SimpleDateFormat("MM-dd-yyyy").format(taxMapping.getValid().getFrom()).toString();
				}

				String endDate = "";
				if (taxMapping.getValid().getTo() != null) {
					endDate = new SimpleDateFormat("MM-dd-yyyy").format(taxMapping.getValid().getTo()).toString();
				}

				bw.append(taxCategoryCode + separator + taxClassCode + separator + taxCode + separator + startDate
						+ separator + endDate + "\r\n");

			}
			if (bw != null) {
				bw.close();
			}

		} catch (Exception e) {
			log.error(e.getMessage());
			throw new BusinessException(e.getMessage());
		}
	}

	/**
	 * Activate tax mapping in a staging table that were imported from a given file
	 * name.
	 *
	 * @param fileId the file id
	 * @throws BusinessException the business exception
	 * @throws ParseException 
	 */
	public void validateTaxMapping(Long fileId) throws BusinessException, ParseException {
		List<Map<String, Object>> taxMappingStages = getTaxMappingStageByFileId(fileId);
		removeErrorReasons(taxMappingStages);

		for (Map<String, Object> entry : taxMappingStages) {

			if (!StringUtils.isBlank((String) entry.get("error_reason"))) {
				throw new BusinessException("The file cannot be activated as long as it contains invalid tax mappings");
			}
	        
			taxMapService.validateTaxMapping(entry, fileId);
			UnitaryCustomTableDataDto pendingTaxMapping = new UnitaryCustomTableDataDto();
			pendingTaxMapping.setCustomTableCode("EIR_TAX_MAPPING_STAGE");
			CustomTableRecordDto value = new CustomTableRecordDto(entry);
			value.setId(((BigInteger) entry.get("id")).longValue());
			pendingTaxMapping.setValue(value);
			customTableApi.update(pendingTaxMapping);
		}
	}

	/**
	 * The pending tax mapping
	 *
	 * @param taxMappingStages the pending tax mappings
	 */
	private void removeErrorReasons(List<Map<String, Object>> taxMappingStages) {

		for (Map<String, Object> entry : taxMappingStages) {
			entry.put("error_reason", null);
		}
		CustomEntityTemplate cet = customTableService.getCET("EIR_TAX_MAPPING_STAGE");
		customTableService.update(cet.getDbTablename(), taxMappingStages);
	}

}
