package com.eir.api.customer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.meveo.admin.exception.BusinessException;
import org.meveo.api.BaseApi;
import org.meveo.api.dto.customer.ExportCustomerBalancesDto;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.commons.utils.ParamBean;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.crm.impl.CustomerService;
import org.meveo.service.payments.impl.CustomerAccountService;
import org.meveo.service.script.ScriptInstanceService;

/**
 * The customer API.
 *
 * @author Houssine ZNIBAR
 */
@Stateless(name = "EirCustomerApi")
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class CustomerApi extends BaseApi {
    
    private static final String UNDER_SCOR = "_";

    @Inject
    CustomerService customerService;
    
    @Inject
    CustomerAccountService customerAccountService;
    
    @Inject
    private ScriptInstanceService scriptInstanceService;

    /**
     * date format.
     */
    public static String DATE_TIME_PATTERN = "yyyy-MM-dd_HH-mm-ss";

    /**
     * The directory of the export.
     */
    private final String EXPORT_DIRECTORY = "reports"+File.separator+"CustomerBalances";

    /**
     * File prefix
     */
    private final static String FILE_PREFIX = "Balance_";
    
    private static final String SPECIAL_CHARACTER = "@_@";

    

    /**
     * Dot.
     */
    public final static String DOT = ".";

    private static final Object FILE_EXTENSION = ".xls";
    
    
    /**
     * Data Model Enum
     */
    public enum DataModelEnum {
        Balances("Balances", "BALANCES",
                "CUSTOMER,CUST BALANCE €,CUSTOMER ACCOUNT CODE ,CUSTOMER ACCOUNT NAME,CUST ACCT BALANCE €, BILLING ACCOUNT CODE, BILLING ACCOUNT NAME, BILLING ACCOUNT NUMBER, BILLING ACCOUNT BALANCE €",
                "SELECT ac.code AS CUSTOMER\r\n" + 
                "    ,(\r\n" + 
                "        SELECT sum(CASE \r\n" + 
                "                    WHEN ao2.transaction_category = 'DEBIT'\r\n" + 
                "                        THEN ao2.un_matching_amount\r\n" + 
                "                    ELSE (- 1 * ao2.un_matching_amount)\r\n" + 
                "                    END)\r\n" + 
                "        FROM ar_account_operation AS ao2\r\n" + 
                "        JOIN ar_customer_account ca2 ON ca2.id = ao2.customer_account_id\r\n" + 
                "        JOIN crm_customer c ON c.id = ca2.customer_id\r\n" + 
                "        JOIN account_entity ac2 ON ac2.id = c.id\r\n" + 
                "            AND ac2.code = :customerCode\r\n" + 
                "        ) AS \"CUST BALANCE €\"\r\n" + 
                "    ,caa.code AS \"CUSTOMER ACCOUNT CODE\"\r\n" + 
                "    ,caa.description AS \"CUSTOMER ACCOUNT NAME\"\r\n" + 
                "    ,(\r\n" + 
                "        SELECT sum(CASE \r\n" + 
                "                    WHEN ao3.transaction_category = 'DEBIT'\r\n" + 
                "                        THEN ao3.un_matching_amount\r\n" + 
                "                    ELSE (- 1 * ao3.un_matching_amount)\r\n" + 
                "                    END)\r\n" + 
                "        FROM ar_account_operation AS ao3\r\n" + 
                "        JOIN ar_customer_account ca3 ON ca3.id = ao3.customer_account_id\r\n" + 
                "            AND ca3.id = ca.id\r\n" + 
                "        ) AS \"CUST ACCT BALANCE €\"\r\n" + 
                "    ,baa.code AS \"BILLING ACCOUNT CODE\"\r\n" + 
                "    ,baa.description AS \"BILLING ACCOUNT NAME\"\r\n" + 
                "    ,baa.external_ref_1 AS \"BILLING ACCOUNT NUMBER\"\r\n" + 
                "    ,(\r\n" + 
                "        SELECT sum(CASE \r\n" + 
                "                    WHEN ao4.transaction_category = 'DEBIT'\r\n" + 
                "                        THEN ao4.un_matching_amount\r\n" + 
                "                    ELSE (- 1 * ao4.un_matching_amount)\r\n" + 
                "                    END)\r\n" + 
                "        FROM ar_account_operation AS ao4\r\n" + 
                "        JOIN ar_customer_account ca4 ON ca4.id = ao4.customer_account_id\r\n" + 
                "        JOIN billing_billing_account ba4 ON ba4.customer_account_id = ca4.id\r\n" + 
                "            AND ba4.id = ba.id\r\n" + 
                "            AND (ao4.cf_values\\:\\:JSON->'BILLING_ACCOUNT_NUMBER'->0->>'string') = baa.external_ref_1\r\n" + 
                "        ) AS \"BILLING ACCOUNT BALANCE €\"\r\n" + 
                "FROM billing_billing_account ba\r\n" + 
                "JOIN ar_customer_account ca ON ba.customer_account_id = ca.id\r\n" + 
                "JOIN account_entity baa ON baa.id = ba.id\r\n" + 
                "JOIN account_entity caa ON caa.id = ca.id\r\n" + 
                "JOIN crm_customer c ON c.id = ca.customer_id\r\n" + 
                "JOIN account_entity ac ON ac.id = c.id\r\n" + 
                "    AND ac.code = :customerCode",
                false);

        /**
         * The description
         */
        private String description;

        /**
         * The delimiter
         */
        private String delimiter;

        /**
         * The header
         */
        private String header;

        /**
         * The sqlQuery
         */
        private String sqlQuery;

        /**
         * The sqlResultHasCfField
         */
        private boolean sqlResultHasCfField;

        /**
         * Instantiates a new data model enum.
         *
         * @param description the description
         * @param delimiter the delimiter
         * @param header the header
         * @param sqlQuery the sql query
         * @param sqlResultHasCfField indicates if the SQL query has a cf field
         */
        DataModelEnum(String description, String delimiter, String header, String sqlQuery, boolean sqlResultHasCfField) {
            this.description = description;
            this.delimiter = delimiter;
            this.header = header;
            this.sqlQuery = sqlQuery;
            this.sqlResultHasCfField = sqlResultHasCfField;
        }

        /**
         * Gets the description.
         *
         * @return the description
         */
        public String getDescription() {
            return description;
        }

        /**
         * Gets the delimiter.
         *
         * @return the delimiter
         */
        public String getDelimiter() {
            return delimiter;
        }

        /**
         * Gets the header.
         *
         * @return the header
         */
        public String getHeader() {
            return header;
        }

        /**
         * Gets the sqlQuery.
         *
         * @return the sqlQuery
         */
        public String getSqlQuery() {
            return sqlQuery;
        }

        /**
         * Gets the sqlResultHasCfField.
         *
         * @return the sqlResultHasCfField
         */
        public boolean isSqlResultHasCfField() {
            return sqlResultHasCfField;
        }
    }
    
    /**
     * Xsl CF Transformer Enum
     */
    public enum XslCFTransformerEnum {
        ORDER_ORIGINAL_MESSAGE("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
                + "<xsl:stylesheet xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\" xmlns:fo=\"http://www.w3.org/1999/XSL/Format\" version=\"1.0\">\n"
                + "   <xsl:output method=\"text\" omit-xml-declaration=\"yes\" indent=\"no\" />\n" + "   <xsl:template match=\"/\">\n"
                + "         <xsl:for-each select=\"//Bill_Item\">\n"
                + "            <xsl:value-of select=\"concat('Bill Item Record',',',Item_Id,',',Operator_Id,',',Item_Component_Id,',',Item_Version,',',Item_Type,',',Agent_Code,',',UAN,',',Master_Account_Number,',',Service_Id,',',Ref_Service_Id,',',Xref_Service_id,',',Bill_Code,',',Action,',',Action_Qualifier,',',Quantity,',',Effect_Date,',','&#xA;')\" />\n"
                + "         </xsl:for-each>\n" + "   </xsl:template>\n" + "</xsl:stylesheet>");

        /**
         * The xsl
         */
        private String xsl;

        /**
         * Instantiates a new xsl CF transformer enum.
         *
         * @param xsl the xsl
         */
        XslCFTransformerEnum(String xsl) {
            this.xsl = xsl;
        }

        /**
         * Gets the xsl.
         *
         * @return the xsl
         */
        public String getXsl() {
            return xsl;
        }
    }

    public ExportCustomerBalancesDto export(String customerCode) throws BusinessException {

        try {
            ExportCustomerBalancesDto dto = new ExportCustomerBalancesDto();
            dto.setCustomerCode(customerCode);
            StringBuilder fileBody = new StringBuilder();

            exportData(customerCode, DataModelEnum.Balances, fileBody);

            // create output file.
            dto.setFilePath(createFile(fileBody, customerCode));
            return dto;
        } catch (Exception e) {
            log.error("Execute export customer balances fail", e);
            throw new BusinessException("Execute export Customer balancest fail.");
        }
    }


    private void exportData(String customerCode, DataModelEnum dataModel, StringBuilder fileBody) throws BusinessException {
        try {
            List<Map<String, Object>> resultList = null;
            if (!StringUtils.isBlank(customerCode)) {
                Map<String, Object> parameters = new HashMap<>();
                parameters.put("customerCode", customerCode);
                try {
                    resultList = scriptInstanceService.executeNativeSelectQuery(dataModel.getSqlQuery(), parameters);
                } catch (Exception e) {
                    log.error("Invalid SQL query : {}", e.getMessage(), e);
                    throw new BusinessException(" Invalid SQL query on export customer balances : " + e.getMessage(), e);
                }
                writeResult(fileBody, dataModel.getDelimiter(), dataModel.getHeader(), resultList, dataModel.isSqlResultHasCfField());
            }

        } catch (BusinessException e) {
            log.error("write {} data fail", dataModel.getDescription(), e);
            throw e;
        }
    }

    private String getReportDirectory() {
        StringBuilder reportDir = new StringBuilder(ParamBean.getInstance().getChrootDir(""));
        reportDir.append(File.separator + EXPORT_DIRECTORY);
        return reportDir.toString();
    }

    private String getShortFilename(String customerCode) {
        StringBuffer shortFilename = new StringBuffer();
        shortFilename.append(FILE_PREFIX).append(customerCode).append(UNDER_SCOR).append(DateUtils.formatDateWithPattern(new Date(), DATE_TIME_PATTERN))
            .append(FILE_EXTENSION);
        return shortFilename.toString();
    }

    private String getFilename(String reportDir, String customerCode) {
        StringBuffer filename = new StringBuffer();
        filename.append(reportDir).append(File.separator).append(getShortFilename(customerCode));
        return filename.toString();
    }

    private String createFile(StringBuilder fileBody, String customerCode) throws BusinessException {

        if (fileBody.length() == 0) {
            throw new BusinessException("No found data");
        }
       //return writeAsFile(fileBody, customerCode);
        return createExcel(fileBody.toString(), customerCode);
    }

//    private String writeAsFile(StringBuilder fileBody, String customerCode) throws BusinessException {
//        FileWriter fileWriter = null;
//        String filename;
//        try {
//            String reportDir = getReportDirectory();
//            FileUtils.forceMkdir(new File(reportDir));
//            filename = getFilename(reportDir, customerCode);
//            File file = new File(filename);
//            file.createNewFile();            
//            fileWriter = new FileWriter(file);
//            fileWriter.write(fileBody.toString());
//            log.info(" >>> Successfully generated file : {} ", filename);
//
//        } catch (Exception e) {
//            log.error("Cannot write report to file", e);
//            throw new BusinessException("Cannot write report to file.", e);
//        } finally {
//            IOUtils.closeQuietly(fileWriter);
//        }
//        return filename;
//    }

    private void writeLine(StringBuilder fileBody, StringBuilder line) {
        line.deleteCharAt(line.length() - 1);
        fileBody.append(line.toString());
        fileBody.append(System.lineSeparator());
    }   
    
    private void createBodyFromSqlResult(StringBuilder fileBody, List<Map<String, Object>> resultList) {
        StringBuilder line = new StringBuilder();

        // get the header
        Map<String, Object> firstRow = resultList.get(0);
        Iterator ite = firstRow.keySet().iterator();        
        while (ite.hasNext()) {
            line.append(ite.next() + ",");
        }
        writeLine(fileBody, line);

        line = new StringBuilder();
        String value = "";
        for (Map<String, Object> row : resultList) {
            ite = firstRow.keySet().iterator();
            while (ite.hasNext()) {
                value = String.valueOf(row.get(ite.next()));
                if (value.contains(",")) {
                    value = value.replaceAll(",", SPECIAL_CHARACTER);
                }                
                line.append(value + ",");
            }
            writeLine(fileBody, line);
            line = new StringBuilder();
        }
    }

    private void createHeader(StringBuilder fileBody, String header) {
        StringBuilder line = new StringBuilder();
        line.append(header);
        writeLine(fileBody, line);
    }

    private void writeResult(StringBuilder fileBody, String delimiter, String header, List<Map<String, Object>> resultList, boolean isResultHasCfField) throws BusinessException {

        try {
            if (resultList != null && !resultList.isEmpty()) {
                createBodyFromSqlResult(fileBody, resultList);
            } else {
                createHeader(fileBody, header);
            }
        } catch (Exception e) {
            log.error("Cannot write report to file: {}", e);
            throw new BusinessException("Cannot write report to file.");
        }
    }
    
    /**
     * Creates the excel.
     *
     * @param body the body
     * @param customerCode the customer code
     * @return the excel file name
     * @throws BusinessException the business exception
     */
    private String createExcel(String body, String customerCode) throws BusinessException {
        String filename = null;
        FileOutputStream out = null;
        XSSFWorkbook workbook = null;
        String cellValue = null;
        try {
            // Create blank workbook
            workbook = new XSSFWorkbook();
            // Create a blank sheet
            XSSFSheet spreadsheet_1 = workbook.createSheet("Excel Sheet");

            ArrayList<String> arrRows = new ArrayList<String>(Arrays.asList(body.split("\n")));

            for (int i = 0; i < arrRows.size(); i++) {

                XSSFRow row = spreadsheet_1.createRow(i);

                ArrayList<String> arrElement = new ArrayList<String>(Arrays.asList(arrRows.get(i).split(",")));

                for (int j = 0; j < arrElement.size(); j++) {

                    XSSFCell cell = row.createCell(j);
                    cellValue = arrElement.get(j);
                    if("null".equalsIgnoreCase(cellValue)) {
                        cellValue = "";
                    } else {
                        cellValue = cellValue.replaceAll(SPECIAL_CHARACTER, ",");
                    }
                    if(NumberUtils.isCreatable(cellValue)) {
                        cell.setCellValue(Double.valueOf(cellValue));
                    } else {
                        cell.setCellValue(cellValue);
                    }
                }
            }
            // Write the workbook in file system
            String reportDir = getReportDirectory();
            FileUtils.forceMkdir(new File(reportDir));
            filename = getFilename(reportDir, customerCode);

            out = new FileOutputStream(new File(filename));
            workbook.write(out);
            return getFilename(EXPORT_DIRECTORY, customerCode);
        } catch (Exception e) {
            throw new BusinessException(e);
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
                if (workbook != null) {
                    workbook.close();
                }
            } catch (IOException e) {
                throw new BusinessException(e);
            }
        }        
    }        
}


