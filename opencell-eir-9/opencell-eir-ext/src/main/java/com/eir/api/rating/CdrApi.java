package com.eir.api.rating;

import com.eir.service.rating.CdrService;
import org.meveo.admin.exception.BusinessException;
import org.meveo.api.BaseApi;
import org.meveo.api.dto.response.PagingAndFiltering;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

/**
 * The CDR API.
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirCdrApi")
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class CdrApi extends BaseApi {

    @Inject
    private CdrService cdrService;

    /**
     * Activate CDRs in a staging table that were imported from a given file name.
     *
     * @param fileId The id of a CDR import file
     * @throws BusinessException the business exception
     */
    public void validateCdr(Long fileId) throws BusinessException {
        if (fileId == null) {
            throw new BusinessException("Missing file name.");
        }
        cdrService.validateCdrs(fileId);
    }

    /**
     * Activate CDRs in a staging table that were imported from a given file name.
     *
     * @param fileId The id of a CDR import file
     * @throws BusinessException the business exception
     */
    public void activateCdr(Long fileId) throws BusinessException {
        if (fileId == null) {
            throw new BusinessException("Missing file name.");
        }
        cdrService.activateCdr(fileId);
    }

    /**
     * Cancel CDRs in a staging table that were imported from a given file.
     *
     * @param fileId The id of a CDR import file
     * @throws BusinessException the business exception
     */
    public void cancelCdr(Long fileId) throws BusinessException {
        if (fileId == null) {
            throw new BusinessException("Missing file name.");
        }
        cdrService.cancelCdr(fileId);
    }

    /**
     * Export CDR records matching a given search criteria
     *
     * @param pagingAndFiltering Search and pagination criteria
     * @throws BusinessException the business exception
     */
    public void exportCdr(PagingAndFiltering pagingAndFiltering) throws BusinessException {
        // TODO implement
    }
}
