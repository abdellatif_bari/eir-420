package com.eir.api.note;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.meveo.admin.exception.BusinessException;
import org.meveo.api.BaseApi;
import org.meveo.api.dto.note.NoteDto;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.crm.Customer;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.security.keycloak.CurrentUserProvider;
import org.meveo.service.admin.impl.UserService;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.crm.impl.CustomerService;
import org.meveo.service.crm.impl.ProviderService;
import org.meveo.service.payments.impl.CustomerAccountService;

/**
 * The note API.
 *
 * @author Mohammed Amine TAZI
 */
@Stateless
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class NoteApi extends BaseApi {

	@Inject
	private CustomerService customerService;
	@Inject
	private CustomerAccountService customerAccountService;
	@Inject
	private BillingAccountService billingAccountService;
	@Inject
	private CurrentUserProvider currentUserProvider;
	@Inject
	private UserService userService;
	@Inject
	private ProviderService providerService;
	
	private static final String SEPARATOR = "|";
	/**
	 * Create a new note
	 *
	 * @param noteDto note Dto
	 * @return the note Dto
	 * @throws BusinessException the business exception
	 */
	public NoteDto add(NoteDto noteDto) throws BusinessException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
		String userName = currentUserProvider
				.getCurrentUser(providerService.getProvider().getCode(), userService.getEntityManager()).getUserName();
		String date = "" + sdf.format(new Date().getTime());
		int noteId = 1;
		if ("customer".equalsIgnoreCase(noteDto.getType())) {
			addCustomerNote(date, userName, noteDto, noteId);
		} else if("customerAccount".equalsIgnoreCase(noteDto.getType())) {
			addCustomerAccountNote(date, userName, noteDto, noteId);

		} else if("billingAccount".equalsIgnoreCase(noteDto.getType())) {
			addBillingAccountNote(date, userName, noteDto, noteId);
		}
		return noteDto;
	}

	/**
	 * delete note
	 *
	 * @param noteDto note Dto
	 * @throws BusinessException the business exception
	 */
	public NoteDto delete(NoteDto noteDto) throws BusinessException {

		log.debug("#####################Starting of script DeleteNoteScript");

		if ("customer".equalsIgnoreCase(noteDto.getType())) {
			Customer customer = customerService.findByCode(noteDto.getCode());
			if (customer == null) {
				throw new BusinessException("The customer is not found");
			}
			Map<String, String> notes = (Map<String, String>) customer.getCfValue("NOTES");
			if (notes != null) {
				notes.keySet().removeIf(key -> Arrays.asList(noteDto.getIds()).contains(key));
                customerService.update(customer);
			}

		} else if ("customerAccount".equalsIgnoreCase(noteDto.getType())) {
			CustomerAccount customerAccount = customerAccountService.findByCode(noteDto.getCode());
			if (customerAccount == null) {
				throw new BusinessException("The customer account is not found");
			}
			Map<String, String> notes = (Map<String, String>) customerAccount.getCfValue("CA_NOTES");
			if (notes != null) {
				notes.keySet().removeIf(key -> Arrays.asList(noteDto.getIds()).contains(key));
				customerAccountService.update(customerAccount);
			}
		} else if ("billingAccount".equalsIgnoreCase(noteDto.getType())) {
			BillingAccount billingAccount = billingAccountService.findByCode(noteDto.getCode());
			if (billingAccount == null) {
				throw new BusinessException("The billing account is not found");
			}
			Map<String, String> notes = (Map<String, String>) billingAccount.getCfValue("BA_NOTES");
			if (notes != null) {
				notes.keySet().removeIf(key -> Arrays.asList(noteDto.getIds()).contains(key));
				billingAccountService.update(billingAccount);
			}
		}
		
		log.debug("#####################Ending of script DeleteNoteScript");
		return noteDto;
	}
		
	private void addCustomerNote(String date, String userName, NoteDto noteDto, int noteId) {
		Customer cust = customerService.findByCode(noteDto.getCode());

		if (cust == null) {
			throw new BusinessException("The customer is not found");
		}
		Map<String, String> notes = new HashMap<String, String>();
		for (Customer customer : customerService.list()) {
			if (customer.getCfValuesNullSafe().getValue("NOTES") != null
					&& customer.getCfValuesNullSafe().getValue("NOTES") instanceof Map<?, ?>) {

				notes = (Map<String, String>) customer.getCfValue("NOTES");

				for (String value : notes.keySet()) {
					if (Integer.parseInt(value) > noteId) {
						noteId = Integer.parseInt(value);
					}

				}
			}
		}

		// add a note
		Map<String, String> custNotes = (Map<String, String>) cust.getCfValue("NOTES");
		if(custNotes == null) {
			custNotes=new HashMap<String, String>();
		}
		custNotes.put((noteId + 1) + "", date + SEPARATOR + userName + SEPARATOR + noteDto.getComment());
		cust.getCfValuesNullSafe().setValue("NOTES", custNotes);
		customerService.update(cust);
		
	}

	/**
	 * 
	 * @param date
	 * @param userName
	 * @param noteDto.getCode()
	 * @param comment
	 * @param noteDto.getId()
	 */
	private void addCustomerAccountNote(String date, String userName, NoteDto noteDto, int noteId) {
		CustomerAccount customerAccount = customerAccountService.findByCode(noteDto.getCode());

		if (customerAccount == null) {
			throw new BusinessException("The customer account is not found");
		}
		Map<String, String> notes = new HashMap<String, String>();
		for (CustomerAccount ca : customerAccountService.list()) {
			if (ca.getCfValuesNullSafe().getValue("CA_NOTES") != null
					&& ca.getCfValuesNullSafe().getValue("CA_NOTES") instanceof Map<?, ?>) {

				notes = (Map<String, String>) ca.getCfValue("CA_NOTES");

				for (String value : notes.keySet()) {
					if (Integer.parseInt(value) > noteId) {
						noteId = Integer.parseInt(value);
					}

				}
			}			
		}
		// add a note
		Map<String, String> caNotes = (Map<String, String>) customerAccount.getCfValue("CA_NOTES");
		if(caNotes == null) {
			caNotes=  new HashMap<String, String>();
		}
		
		caNotes.put((noteId + 1) + "", date + SEPARATOR + userName + SEPARATOR + noteDto.getComment());
		customerAccount.setCfValue("CA_NOTES", caNotes);
		customerAccountService.update(customerAccount);	
	}
	
	/**
	 * 
	 * @param date
	 * @param userName
	 * @param noteDto.getCode()
	 * @param comment
	 * @param noteDto.getId()
	 */
	private void addBillingAccountNote(String date, String userName, NoteDto noteDto, int noteId) {
		BillingAccount billingAccount = billingAccountService.findByCode(noteDto.getCode());
		if (billingAccount == null) {
			throw new BusinessException("The billing account is not found");
		}
		Map<String, String> notes = new HashMap<String, String>();
		for (BillingAccount ba : billingAccountService.list()) {
			if (ba.getCfValuesNullSafe().getValue("BA_NOTES") != null
					&& ba.getCfValuesNullSafe().getValue("BA_NOTES") instanceof Map<?, ?>) {

				notes = (Map<String, String>) ba.getCfValue("BA_NOTES");

				for (String value : notes.keySet()) {
					if (Integer.parseInt(value) > noteId) {
						noteId = Integer.parseInt(value);
					}

				}
			}
		}
		// add a note
		Map<String, String> baNotes = (Map<String, String>) billingAccount.getCfValuesNullSafe().getValue("BA_NOTES");
		if(baNotes == null) {
			baNotes=new HashMap<String, String>();
		}
		baNotes.put((noteId + 1) + "", date + SEPARATOR + userName + SEPARATOR + noteDto.getComment());
		billingAccount.getCfValuesNullSafe().setValue("BA_NOTES", baNotes);
		billingAccountService.update(billingAccount);		
	}
}
