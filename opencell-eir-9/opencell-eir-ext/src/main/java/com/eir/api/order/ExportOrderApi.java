package com.eir.api.order;

import com.eir.commons.enums.customfields.OrderCFsEnum;
import com.eir.service.order.impl.OrderService;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.api.BaseApi;
import org.meveo.api.dto.order.ExportOrderDto;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.commons.utils.ParamBean;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.order.Order;
import org.meveo.model.order.OrderStatusEnum;
import org.meveo.model.shared.DateUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Stream;

/**
 * The export order API.
 *
 * @author Abdellatif BARI
 */
@Stateless
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class ExportOrderApi extends BaseApi {

    @Inject
    OrderService orderService;

    /**
     * date format.
     */
    public static String DATE_TIME_PATTERN = "_yyyy-MM-dd_HH-mm-ss";

    /**
     * The directory of the export.
     */
    private final String EXPORT_DIRECTORY = "reports/exportOrder";

    /**
     * File prefix
     */
    private final static String FILE_PREFIX = "ORDER";

    /**
     * Output Format Enum
     */
    public enum OutputFormatEnum {
        CSV, XML
    }

    public ExportOrderDto export(ExportOrderDto exportOrderDto) throws BusinessException {
        if (StringUtils.isBlank(exportOrderDto.getFileType())) {
            throw new BusinessException("Missing file type.");
        }
        if (!Arrays.stream(OutputFormatEnum.values()).anyMatch((t) -> t.name().equalsIgnoreCase(exportOrderDto.getFileType()))) {
            throw new BusinessException("invalid file type.");
        }
        List<String> orderIds = exportOrderDto.getOrderIds();
        if (orderIds == null || orderIds.isEmpty()) {
            throw new BusinessException("Missing order ids.");
        }
        StringBuilder fileBody = new StringBuilder();
        Iterator iterator = orderIds.iterator();
        // iterate through json array
        while (iterator.hasNext()) {
            exportOrder((String) iterator.next(), exportOrderDto.getFileType(), fileBody);
        }

        createFile(exportOrderDto.getFileType(), fileBody);
        exportOrderDto.setFilePath(getFilename(EXPORT_DIRECTORY, exportOrderDto.getFileType()));
        return exportOrderDto;
    }

    private void exportOrder(String orderId, String fileType, StringBuilder fileBody) throws BusinessException {

        try {

            Order order = orderService.findByCode(orderId);
            if (order == null) {
                throw new BusinessException("Order with code=" + orderId + " does not exists.");
            }

            String orderMessage = (String) order.getCfValuesNullSafe().getValue(OrderCFsEnum.ORDER_ORIGINAL_MESSAGE.name());
            if (StringUtils.isBlank(orderMessage)) {
                throw new BusinessException("No Custom field matched by name " + OrderCFsEnum.ORDER_ORIGINAL_MESSAGE.name());
            }
            //orderMessage = Utils.retrievePipeCharacter(orderMessage);

            if (fileType.equalsIgnoreCase(OutputFormatEnum.XML.name())) {
                stringToXml(order, orderMessage, fileBody);
            } else if (fileType.equalsIgnoreCase(OutputFormatEnum.CSV.name())) {
                stringToCsv(order, orderMessage, fileBody);
            }

        } catch (Exception e) {
            log.error("execute export order script fail", e);
            throw new BusinessException(e.getMessage(), e);
        }
    }

    private boolean isInvalidOrderStatus(Order order) {
        Boolean isInvalidtatus = Stream.of(OrderStatusEnum.REJECTED, OrderStatusEnum.DEFERRED, OrderStatusEnum.HELD, OrderStatusEnum.WAITING)
                .anyMatch(status -> status == order.getStatus());
        if (isInvalidtatus && !StringUtils.isBlank(order.getStatusMessage())) {
            return true;
        }
        return false;
    }

    private void stringToXml(Order order, String xmlSource, StringBuilder fileBody) throws SAXException, ParserConfigurationException, IOException, TransformerException {

        // Parse the given input
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new StringReader(xmlSource)));

        // Write the parsed document to an xml file
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();

        //if (fileBody.length() != 0) {
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        //}

        // Adding the error message to xml document
        if (isInvalidOrderStatus(order)) {
            Node root = document.getDocumentElement();
            Element errorMessageElement = document.createElement("errorMessage");
            errorMessageElement.setTextContent(order.getStatusMessage());
            root.insertBefore(errorMessageElement, root.getFirstChild());
        }

        // Write the parsed document to a xml file
        write(document, transformer, fileBody);
        fileBody.append(System.lineSeparator());
    }

    private void stringToCsv(Order order, String xmlSource, StringBuilder fileBody) throws SAXException, ParserConfigurationException, IOException, TransformerException {

        // Adding the error message to csv file
        if (isInvalidOrderStatus(order)) {
            StringBuilder invalidOrderXML = new StringBuilder();
            stringToXml(order, xmlSource, invalidOrderXML);
            xmlSource = invalidOrderXML.toString();
        }

        String xsl = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n"
                + "<xsl:stylesheet version=\"1.0\" xmlns:fo=\"http://www.w3.org/1999/XSL/Format\" xmlns:xsl=\"http://www.w3.org/1999/XSL/Transform\">\r\n"
                + "<xsl:param name=\"quote\">\"</xsl:param>"
                + "     <xsl:output indent=\"no\" method=\"text\" omit-xml-declaration=\"yes\"/>\r\n" + "     <xsl:template match=\"/\">\r\n"
                + "         <xsl:for-each select=\"//Wholesale_Billing\">\r\n"
                + "             <xsl:value-of select=\"concat('WB Record',',',$quote,Transaction_Id,$quote,',',$quote,Ordering_System,$quote,',',$quote,Order_Id,$quote,',',$quote,Ref_Order_Id,$quote,',',$quote,errorMessage,$quote,'&#xA;')\" />\r\n"
                + "             <xsl:for-each select=\"./Bill_Item\">\r\n"
                + "                 <xsl:value-of select=\"concat('Bill Item Record',',',$quote,Item_Id,$quote,',',$quote,Operator_Id,$quote,',',$quote,Operator_Order_Number,$quote,',',$quote,Item_Component_Id,$quote,',',$quote,Item_Version,$quote,',',$quote,Item_Type,$quote,',',$quote,Agent_Code,$quote,',',$quote,UAN,$quote,',',$quote,Master_Account_Number,$quote,',',$quote,Service_Id,$quote,',',$quote,Ref_Service_Id,$quote,',',$quote,Xref_Service_Id,$quote,',',$quote,Bill_Code,$quote,',',$quote,Action,$quote,',',$quote,Action_Qualifier,$quote,',',$quote,Main_Service_Bill_Code,$quote,',',$quote,Quantity,$quote,',',$quote,Effect_Date,$quote,'&#xA;')\" />\r\n"
                + "                 <xsl:for-each select=\"./Attribute\">\r\n"
                + "                     <xsl:value-of select=\"concat('Attribute Record',',',$quote,Code,$quote,',',$quote,Value,$quote,',',$quote,Unit,$quote,',',$quote,Component_Id,$quote,',',$quote,Attribute_Type,$quote,'&#xA;')\"/>\r\n"
                + "                 </xsl:for-each>\r\n" + "             </xsl:for-each>\r\n" + "         </xsl:for-each>\r\n" + "     </xsl:template>\r\n" + "</xsl:stylesheet>";

        // Parse the given input
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new StringReader(xmlSource)));

        StreamSource xslSource = new StreamSource(new StringReader(xsl));
        Transformer transformer = TransformerFactory.newInstance().newTransformer(xslSource);

        // Write the parsed document to a csv file
        write(document, transformer, fileBody);
    }

    private void write(Document document, Transformer transformer, StringBuilder fileBody) throws TransformerException {
        Source source = new DOMSource(document);
        StringWriter writer = new StringWriter();
        transformer.transform(source, new StreamResult(writer));
        String output = writer.toString();
        fileBody.append(output);
    }

    private void createFile(String fileType, StringBuilder fileBody) throws BusinessException {

        if (fileBody.length() == 0) {
            throw new BusinessException("No data found for selected orders");
        }
        if (fileType.equalsIgnoreCase(OutputFormatEnum.XML.name())) {
            fileBody.insert(0, "<?xml version=\"1.0\" encoding=\"UTF-8\"?><Orders>").append("</Orders>");
        }
        writeAsFile(fileType, fileBody);
    }

    private void writeAsFile(String fileType, StringBuilder fileBody) throws BusinessException {
        FileWriter fileWriter = null;
        try {
            String reportDir = getReportDirectory();
            FileUtils.forceMkdir(new File(reportDir));
            File file = new File(getFilename(reportDir, fileType));
            file.createNewFile();
            fileWriter = new FileWriter(file);
            fileWriter.write(fileBody.toString());

        } catch (Exception e) {
            log.error("Cannot write report to file: {}", e);
            throw new BusinessException("Cannot write report to file.", e);
        } finally {
            IOUtils.closeQuietly(fileWriter);
        }
    }

    private String getReportDirectory() {
        //StringBuilder reportDir = new StringBuilder(ParamBean.getInstance().getChrootDir(provider.getCode()));
        StringBuilder reportDir = new StringBuilder(ParamBean.getInstance().getChrootDir(""));
        reportDir.append(File.separator).append(EXPORT_DIRECTORY);
        return reportDir.toString();
    }

    private String getShortFilename(String fileType) {
        StringBuffer shortFilename = new StringBuffer();
        shortFilename.append(FILE_PREFIX).append(DateUtils.formatDateWithPattern(new Date(), DATE_TIME_PATTERN));
        shortFilename.append(".").append(fileType.toLowerCase());
        return shortFilename.toString();
    }

    private String getFilename(String reportDir, String fileType) {
        StringBuffer filename = new StringBuffer();
        filename.append(reportDir).append(File.separator).append(getShortFilename(fileType));
        return filename.toString();
    }

}
