package com.eir.api.order;

import com.eir.commons.enums.OrderEmitterEnum;
import com.eir.commons.enums.OrderProcessingTypeEnum;
import com.eir.service.commons.UtilService;
import com.eir.service.crm.impl.SubscriptionTerminationReasonService;
import com.eir.service.order.impl.OrderService;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.api.BaseApi;
import org.meveo.api.commons.ErrorEnum;
import org.meveo.api.dto.order.BaseOrderDto;
import org.meveo.api.dto.order.OrderDto;
import org.meveo.api.dto.order.OrderResponseDto;
import org.meveo.api.dto.order.OrdersResponseDto;
import org.meveo.api.dto.order.ReleaseOrderDto;
import org.meveo.api.dto.order.ReprocessOrderDto;
import org.meveo.api.dto.response.PagingAndFiltering;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.api.security.config.annotation.SecuredBusinessEntityMethod;
import org.meveo.api.security.filter.ListFilter;
import org.meveo.commons.utils.StringUtils;
import org.meveo.jpa.JpaAmpNewTx;
import org.meveo.model.crm.custom.CustomFieldInheritanceEnum;
import org.meveo.model.order.Order;
import org.meveo.model.order.OrderStatusEnum;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;
import java.util.ArrayList;
import java.util.List;

/**
 * The order API.
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirOrderApi")
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class OrderApi extends BaseApi {

    @Inject
    private OrderService orderService;

    @Inject
    private OMSOrderApi omsOrderApi;

    @Inject
    private UGOrderApi ugOrderApi;

    @Inject
    private WSLAMOrderApi wslamOrderApi;

    @Inject
    private UtilService utilService;

    @Inject
    protected SubscriptionTerminationReasonService subscriptionTerminationReasonService;

    /**
     * List orders matching a given criteria
     *
     * @param pagingAndFiltering the paging and filtering
     * @return orders list response
     * @throws BusinessException the business exception
     */
    @SecuredBusinessEntityMethod(resultFilter = ListFilter.class)
    public OrdersResponseDto list(PagingAndFiltering pagingAndFiltering) throws BusinessException {
        OrdersResponseDto result = new OrdersResponseDto();

        String sortBy = "code";
        if (!StringUtils.isBlank(pagingAndFiltering.getSortBy())) {
            sortBy = pagingAndFiltering.getSortBy();
        }

        PaginationConfiguration paginationConfiguration = toPaginationConfiguration(sortBy, org.primefaces.model.SortOrder.ASCENDING, null, pagingAndFiltering, Order.class);

        Long totalCount = orderService.count(paginationConfiguration);
        result.setPaging(pagingAndFiltering != null ? pagingAndFiltering : new PagingAndFiltering());
        result.getPaging().setTotalNumberOfRecords(totalCount.intValue());
        result.setOrders(new ArrayList<>());

        if (totalCount > 0) {
            List<Order> orders = orderService.list(paginationConfiguration);
            if (orders != null) {
                for (Order order : orders) {
                    result.getOrders().add(orderToDto(order, CustomFieldInheritanceEnum.getInheritCF(true, false)));
                }
            }
        }
        return result;
    }

    /**
     * Place a new order in asynchronous mode
     *
     * @param orderDto the order Dto
     * @return the order Dto
     * @throws BusinessException the business exception
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public OrderDto createAsync(OrderDto orderDto) throws BusinessException {
        log.info("Received new async order {}", orderDto.getOrderCode());
        try {
            BaseOrderApi orderAPi = getOrderApi(orderDto);
            if (!orderAPi.validate(orderDto)) {
                return orderDto;
            }
            orderDto.setProcessingType(OrderProcessingTypeEnum.CREATE);
            // Create pending order
            orderAPi.updateOrderStatus(orderAPi::setPendingStatus, orderDto);
            utilService.sendToOrderQueue(orderDto);
            OrderDto orderDtoResp = new OrderDto();
            orderDtoResp.setOrderCode(orderDto.getOrderCode());
            orderDtoResp.setSuccessMessage("The order is scheduled for processing");
            return orderDtoResp;

        } catch (Exception ex) {
            OrderDto orderDtoResp = new OrderDto();
            orderDtoResp.setOrderCode(orderDto.getOrderCode());
            orderDtoResp.setErrorCode(ErrorEnum.GENERIC_API_EXCEPTION);
            //orderDtoResp.setErrorMessage("Not able to accept the order for processing");
            orderDtoResp.setErrorMessage(ex.getMessage() != null ? ex.getMessage() : ex.toString());
            return orderDtoResp;
        }
    }

    /**
     * Place a new provided order from bulk file in asynchronous mode
     *
     * @param orderDto the order Dto
     * @throws BusinessException the business exception
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public void processBulkOrder(OrderDto orderDto) throws BusinessException {
        BaseOrderApi orderAPi = getOrderApi(orderDto);
        if (!orderAPi.validate(orderDto)) {
            throw new BusinessException(orderDto.getErrorMessage());
        }
        log.info("Received new async order {}", orderDto.getOrderCode());
        orderDto.setProcessingType(OrderProcessingTypeEnum.BULK);
        // Create pending order
        orderAPi.updateOrderStatus(orderAPi::setPendingStatus, orderDto);
        utilService.sendToOrderQueue(orderDto);
    }

    /**
     * Place a new order
     *
     * @param orderDto the order Dto
     * @return the order Dto
     * @throws BusinessException the business exception
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public OrderDto createOrder(OrderDto orderDto) throws BusinessException {
        BaseOrderApi orderAPi = getOrderApi(orderDto);
        return orderAPi.create(orderDto);
    }

    /**
     * Reprocess order in asynchronous mode
     *
     * @param reprocessOrderDto the reprocess order Dto.
     * @return the order Dto
     * @throws BusinessException the business exception
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public OrderDto reprocessAsync(ReprocessOrderDto reprocessOrderDto) throws BusinessException {
        log.info("Received to reprocess async order {}", reprocessOrderDto.getOrderCode());
        try {
            OrderDto orderDto = orderService.getOrderDto(reprocessOrderDto.getOrderCode());
            BaseOrderApi orderAPi = getOrderApi(reprocessOrderDto);
            if (orderDto.getStatus() != OrderStatusEnum.COMPLETED) {
                orderDto.setProcessingType(OrderProcessingTypeEnum.REPROCESS);
                orderAPi.updateOrderStatus(orderAPi::setOrderInProgressStatus, orderDto);
                utilService.sendToOrderQueue(orderDto);
            }
            OrderDto orderDtoResp = new OrderDto();
            orderDtoResp.setOrderCode(orderDto.getOrderCode());
            orderDtoResp.setSuccessMessage("The order is scheduled for reprocessing");
            return orderDtoResp;
        } catch (Exception ex) {
            OrderDto orderDtoResp = new OrderDto();
            orderDtoResp.setOrderCode(reprocessOrderDto.getOrderCode());
            orderDtoResp.setErrorCode(ErrorEnum.GENERIC_API_EXCEPTION);
            orderDtoResp.setErrorMessage(ex.getMessage() != null ? ex.getMessage() : ex.toString());
            return orderDtoResp;
        }
    }


    /**
     * Reprocess the orders
     *
     * @param reprocessOrderDtoList reprocess order Dto list.
     * @throws BusinessException the business exception
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public void reprocess(List<ReprocessOrderDto> reprocessOrderDtoList) throws BusinessException {
        for (ReprocessOrderDto reprocessOrderDto : reprocessOrderDtoList) {
            reprocessAsync(reprocessOrderDto);
        }
    }

    /**
     * Reprocess order
     *
     * @param orderDto the order Dto
     * @return the order Dto
     * @throws BusinessException the business exception
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public OrderDto reprocessOrder(OrderDto orderDto) throws BusinessException {
        BaseOrderApi orderAPi = getOrderApi(orderDto);
        return orderAPi.reprocess(orderDto);
    }

    /**
     * Release order in asynchronous mode
     *
     * @param releaseOrderDto the reprocess order Dto.
     * @return the order Dto
     * @throws BusinessException the business exception
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public OrderDto releaseAsync(ReleaseOrderDto releaseOrderDto) throws BusinessException {
        log.info("Received to release async order {}", releaseOrderDto.getOrderCode());
        try {
            BaseOrderApi orderAPi = getOrderApi(releaseOrderDto);
            OrderDto orderDto = orderService.getOrderDto(releaseOrderDto.getOrderCode());
            if (releaseOrderDto.getReleaseStatus() == OrderStatusEnum.PENDING && orderDto.getStatus() != OrderStatusEnum.DEFERRED &&
                    orderDto.getStatus() != OrderStatusEnum.HELD && orderDto.getStatus() != OrderStatusEnum.WAITING) {
                return orderDto;
            }
            orderDto.setProcessingType(OrderProcessingTypeEnum.RELEASE);
            orderAPi.updateOrderStatus(orderAPi::setOrderInProgressStatus, orderDto);
            //BeanUtils.copyProperties(orderDto, releaseOrderDto);
            orderDto.setCancelReasonCode(releaseOrderDto.getCancelReasonCode());
            orderDto.setCancelDescription(releaseOrderDto.getCancelDescription());
            orderDto.setReleaseDescription(releaseOrderDto.getReleaseDescription());
            orderDto.setReleaseStatus(releaseOrderDto.getReleaseStatus());
            utilService.sendToOrderQueue(orderDto);

            OrderDto orderDtoResp = new OrderDto();
            orderDtoResp.setOrderCode(orderDto.getOrderCode());
            orderDtoResp.setSuccessMessage("The order is scheduled for release");
            return orderDtoResp;
        } catch (Exception ex) {
            OrderDto orderDtoResp = new OrderDto();
            orderDtoResp.setOrderCode(releaseOrderDto.getOrderCode());
            orderDtoResp.setErrorCode(ErrorEnum.GENERIC_API_EXCEPTION);
            orderDtoResp.setErrorMessage(ex.getMessage() != null ? ex.getMessage() : ex.toString());
            return orderDtoResp;
        }
    }

    /**
     * Release the orders
     *
     * @param releaseOrderDtoList release order Dto list.
     * @throws BusinessException the business exception
     */
    @TransactionAttribute(TransactionAttributeType.NEVER)
    public void release(List<ReleaseOrderDto> releaseOrderDtoList) throws BusinessException {
        for (ReleaseOrderDto releaseOrderDto : releaseOrderDtoList) {
            releaseAsync(releaseOrderDto);
        }
    }

    /**
     * Release order
     *
     * @param orderDto the order Dto.
     * @return the order Dto
     * @throws BusinessException the business exception
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public OrderDto releaseOrder(OrderDto orderDto) throws BusinessException {
        BaseOrderApi orderAPi = getOrderApi(orderDto);
        return orderAPi.release(orderDto);
    }

    /**
     * Convert order dto to entity
     *
     * @param order     instance of order to be mapped
     * @param inheritCF choose whether CF values are inherited and/or merged
     * @return instance of OrderResponseDto
     */
    private OrderResponseDto orderToDto(Order order, CustomFieldInheritanceEnum inheritCF) {
        OrderResponseDto dto = new OrderResponseDto(order);
        dto.setOrderNumber(order.getOrderNumber());
        dto.setOrderDate(order.getOrderDate());
        dto.setPriority(order.getPriority());
        dto.setStatus(order.getStatus());
        dto.setStatusMessage(order.getStatusMessage());
        dto.setReceivedFromApp(order.getReceivedFromApp());
        dto.setElectronicBilling(order.getElectronicBilling());
        dto.setCfValues(entityToDtoConverter.getCustomFieldsDTO(order, inheritCF));
        setAuditableFieldsDto(order, dto);
        return dto;
    }

    /**
     * Get the order API instance
     *
     * @param orderDto the order Dto
     * @return the order API instance
     * @throws BusinessException the business exception
     */
    public BaseOrderApi getOrderApi(BaseOrderDto orderDto) throws BusinessException {
        if (orderDto == null) {
            throw new BusinessException("The order is invalid");
        }
        BaseOrderApi orderAPi = null;
        if (OrderEmitterEnum.OMS.name().equalsIgnoreCase(orderDto.getOrderingSystem())) {
            orderAPi = omsOrderApi;
        } else if (OrderEmitterEnum.UG.name().equalsIgnoreCase(orderDto.getOrderingSystem())) {
            orderAPi = ugOrderApi;
        } else if (OrderEmitterEnum.WSLAM.name().equalsIgnoreCase(orderDto.getOrderingSystem())) {
            orderAPi = wslamOrderApi;
        } else {
            throw new BusinessException("The following parameter contains invalid value : Ordering_System");
        }
        return orderAPi;
    }

    /**
     * Set in_progress status and set the acknowledgement message
     *
     * @param order the order
     * @throws BusinessException the business exception
     */
    @JpaAmpNewTx
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void setInProgressStatus(Order order) throws BusinessException {

        order.setStatus(OrderStatusEnum.IN_PROGRESS);
        orderService.update(order);
    }

}
