package com.eir.api.tariff;

import java.math.BigInteger;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.api.BaseApi;
import org.meveo.api.custom.CustomTableApi;
import org.meveo.api.dto.custom.UnitaryCustomTableDataDto;
import org.meveo.api.exception.EntityDoesNotExistsException;
import org.meveo.api.exception.MeveoApiException;
import org.meveo.api.security.Interceptor.SecuredBusinessEntityMethodInterceptor;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.custom.CustomTableService;

import com.eir.commons.enums.customtables.TariffPlanCustomTableEnum;
import com.eir.commons.enums.customtables.TariffPlanMapCustomTableEnum;
import com.eir.service.tariff.TariffPlanService;

/**
 * The tariff plan API.
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirTariffPlanApi")
@Interceptors(SecuredBusinessEntityMethodInterceptor.class)
public class TariffPlanApi extends BaseApi {

    @Inject
    private CustomTableApi customTableApi;
    @Inject
    private CustomTableService customTableService;
    @Inject
    private TariffPlanService tariffPlanService;


    /**
     * Create or update a tariff plan.
     *
     * @param tariffPlanDto the tariff plan Dto
     * @throws BusinessException the business exception
     */
    public void createOrUpdateTariffPlan(UnitaryCustomTableDataDto tariffPlanDto) throws BusinessException {
        validateTariffPlan(tariffPlanDto);

        // create or update tariff plan
        Long tariffPlanId = tariffPlanDto.getValue().getId();
        boolean isDefaultTariffPlan = (boolean) tariffPlanDto.getValue().getValues().get(TariffPlanCustomTableEnum.IS_DEFAULT.getLabel());

        tariffPlanDto.getValue().getValues().remove(TariffPlanCustomTableEnum.IS_DEFAULT.getLabel());

        if (tariffPlanId == null) {
            Map<String, Object> tariffPlan = tariffPlanService.findTariffPlanByCode((String) tariffPlanDto.getValue().getValues().get(TariffPlanCustomTableEnum.CODE.getLabel()));
            if (tariffPlan != null && !tariffPlan.isEmpty()) {
                throw new BusinessException("Please choose another code, there is already a tariff with the same code.");
            }
            customTableApi.create(tariffPlanDto);

            // if default is a yes we add the tariff plan map for this tariff
            if (isDefaultTariffPlan) {
                List<Map<String, Object>> tariffPlanMaps = tariffPlanService.findTariffPlanMaps(tariffPlanDto);
                updateDefaultTariffPlan(tariffPlanDto, tariffPlanMaps);
            }
        } else {
            try {
                updateTariffPlan(tariffPlanDto, isDefaultTariffPlan);
            } catch (MeveoApiException e) {
                throw new BusinessException(e);
            }
        }
    }

    /**
     * Create or update a tariff plan map.
     *
     * @param tariffPlanMapDto the tariff plan map Dto
     * @throws BusinessException the business exception
     */
    public void createOrUpdateTariffPlanMap(UnitaryCustomTableDataDto tariffPlanMapDto) throws BusinessException {
        validateTariffPlanMap(tariffPlanMapDto);
        tariffPlanService.createOrUpdateTariffPlanMap(tariffPlanMapDto);
    }

    /**
     * Validate a tariff plan
     *
     * @param tariffPlanDto the tariff plan Dto
     * @throws ValidationException the validation exception
     */
    private void validateTariffPlan(UnitaryCustomTableDataDto tariffPlanDto) throws ValidationException {
        if (tariffPlanDto == null || tariffPlanDto.getValue() == null || tariffPlanDto.getValue().getValues() == null || tariffPlanDto.getValue().getValues().isEmpty()) {
            throw new ValidationException("Missing tariff plan.");
        }

        if (StringUtils.isBlank(tariffPlanDto.getValue().getValues().get(TariffPlanCustomTableEnum.OFFER_ID.getLabel()))) {
            throw new ValidationException("Missing offer for tariff plan.");
        }
    }

    /**
     * Validate a tariff plan map
     *
     * @param tariffPlanMapDto the tariff plan map Dto
     * @throws ValidationException the validation exception
     */
    private void validateTariffPlanMap(UnitaryCustomTableDataDto tariffPlanMapDto) throws ValidationException {
        if (tariffPlanMapDto == null || tariffPlanMapDto.getValue() == null || tariffPlanMapDto.getValue().getValues() == null || tariffPlanMapDto.getValue().getValues().isEmpty()) {
            throw new ValidationException("Missing tariff plan.");
        }

        if (StringUtils.isBlank(tariffPlanMapDto.getValue().getValues().get(TariffPlanMapCustomTableEnum.CUSTOMER_ID.getLabel()))) {
            throw new ValidationException("Missing customer.");
        }
        if (StringUtils.isBlank(tariffPlanMapDto.getValue().getValues().get(TariffPlanMapCustomTableEnum.OFFER_ID.getLabel()))) {
            throw new ValidationException("Missing offer.");
        }
        if (StringUtils.isBlank(tariffPlanMapDto.getValue().getValues().get(TariffPlanMapCustomTableEnum.TARIFF_PLAN_ID.getLabel()))) {
            throw new ValidationException("Missing tariff plan.");
        }
        Date startDate = DateUtils.parseDate(tariffPlanMapDto.getValue().getValues().get(TariffPlanMapCustomTableEnum.VALID_FROM.getLabel()));
        Date endDate = DateUtils.parseDate(tariffPlanMapDto.getValue().getValues().get(TariffPlanMapCustomTableEnum.VALID_TO.getLabel()));
        if (startDate != null && endDate != null && startDate.after(endDate)) {
            throw new ValidationException("The end date can not be earlier than the start date.");
        }
    }

    /**
     * Update default tariff plan
     *
     * @param tariffPlanDto  the tariff plan dto
     * @param tariffPlanMaps the list of default tariff plans
     * @throws BusinessException the business exception
     */
    private void updateDefaultTariffPlan(UnitaryCustomTableDataDto tariffPlanDto, List<Map<String, Object>> tariffPlanMaps) throws BusinessException {
        Date now = new Date();
        // Remove the default tariff plan map if exists
        if (tariffPlanMaps != null && !tariffPlanMaps.isEmpty()) {
            removeTariffPlanMaps(tariffPlanMaps);
        }
        // Insert the new default tariff plan map
        tariffPlanService.insertTariffPlanMap(tariffPlanDto, now);
    }

    /**
     * Rmove tariff plan maps
     *
     * @param tariffPlanMaps the list of tariff plan maps
     * @throws BusinessException the business exception
     */
    private void removeTariffPlanMaps(List<Map<String, Object>> tariffPlanMaps) throws BusinessException {
        if (tariffPlanMaps != null && !tariffPlanMaps.isEmpty()) {
            Set<Long> ids = tariffPlanMaps.stream().map(entry -> {
                return ((BigInteger) entry.get(TariffPlanMapCustomTableEnum.ID.getLabel())).longValue();
            }).collect(Collectors.toSet());
            customTableService.remove(TariffPlanMapCustomTableEnum.getCustomTableCode(), ids);
        }
    }

    /**
     * Update tariff plan
     *
     * @param tariffPlanDto       the tariff plan Dto
     * @param isDefaultTariffPlan Indicate is the default tariff plan or not
     * @throws MeveoApiException the meveo api exception
     * @throws BusinessException the business exception
     */
    private void updateTariffPlan(UnitaryCustomTableDataDto tariffPlanDto, boolean isDefaultTariffPlan) throws MeveoApiException, BusinessException {
        Long tariffPlanId = tariffPlanDto.getValue().getId();
        if (customTableService.validateRecordExistanceByTableName(TariffPlanCustomTableEnum.getCustomTableCode(), tariffPlanId)) {

            Map<String, Object> values = customTableService.findById(TariffPlanCustomTableEnum.getCustomTableCode(), tariffPlanId);
            if (((BigInteger) values.get(TariffPlanCustomTableEnum.OFFER_ID.getLabel())).longValue() != Long.valueOf((String) tariffPlanDto.getValue().getValues().get(TariffPlanCustomTableEnum.OFFER_ID.getLabel()))) {
                throw new BusinessException("the provided offer does not match with the tariff plan offer.");
            }

            Map<String, Object> tariffPlan = tariffPlanService.findTariffPlanByCode((String) tariffPlanDto.getValue().getValues().get(TariffPlanCustomTableEnum.CODE.getLabel()));
            if (tariffPlan != null && !tariffPlan.isEmpty() && ((BigInteger) tariffPlan.get(TariffPlanCustomTableEnum.ID.getLabel())).longValue() != tariffPlanId) {
                throw new BusinessException("Please choose another code, there is already a tariff with the same code.");
            }
            customTableApi.update(tariffPlanDto);

            List<Map<String, Object>> tariffPlanMaps = tariffPlanService.findTariffPlanMaps(tariffPlanDto);

            // no to yes
            if (isDefaultTariffPlan) {
                updateDefaultTariffPlan(tariffPlanDto, tariffPlanMaps);
            } else { // yes to no
                // remove default tariff plan map
                removeTariffPlanMaps(tariffPlanMaps);
            }
        } else {
            throw new EntityDoesNotExistsException(TariffPlanCustomTableEnum.getCustomTableCode(), Arrays.asList(tariffPlanId));
        }
    }
}