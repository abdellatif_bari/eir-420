/*
 * (C) Copyright 2015-2020 Opencell SAS (https://opencellsoft.com/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
 * OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS
 * IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO
 * THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 *
 * For more information on the GNU Affero General Public License, please consult
 * <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

package com.eir.cache;

import com.eir.service.order.impl.OrderService;
import org.infinispan.Cache;
import org.infinispan.commons.CacheException;
import org.infinispan.context.Flag;
import org.meveo.api.dto.order.OrderDto;
import org.meveo.commons.utils.StringUtils;
import org.meveo.commons.utils.ThreadUtils;
import org.meveo.security.CurrentUser;
import org.meveo.security.MeveoUser;
import org.slf4j.Logger;

import javax.annotation.Resource;
import javax.ejb.Lock;
import javax.ejb.LockType;
import javax.ejb.Singleton;
import javax.inject.Inject;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * Provides cache related processing orders
 *
 * @author Abdellatif BARI
 */
@Singleton
@Lock(LockType.WRITE)
public class OrderCacheContainerProvider implements Serializable {

    private static final long serialVersionUID = 1695039022084869911L;

    @Inject
    @CurrentUser
    protected MeveoUser currentUser;

    @Inject
    protected Logger log;

    @Resource(lookup = "java:jboss/infinispan/cache/opencell/opencell-orders")
    private Cache<String, Long> ordersCache;

    @Inject
    protected OrderService orderService;

    /**
     * Sets the time to live in the cache for an element before it expires
     */
    private final static long TIME_TO_LIVE_SECONDS = 15 * 60 * 1000;

    /**
     * Add the list order and subscription codes to the cache
     *
     * @param dto the order dto
     * @return True is any order or subscription code already cached.
     */
    public boolean addToCache(OrderDto dto) {
        if (dto != null) {
            if (!StringUtils.isBlank(dto.getOrderCode())) {
                boolean isKeyAlreadyCached = putInCacheWithRetry(dto, "ORDER_CODE_" + dto.getOrderCode(), 100, 3);
                if (isKeyAlreadyCached) {
                    log.info("The order {} is already cached through it's code", dto.getOrderCode());
                    // Check if in the meantime the order has been deleted before it is finished
                    if (orderService.findByCode(dto.getOrderCode()) == null) {
                        log.info("Removing the order {} from the cache because it was deleted from the database", dto.getOrderCode());
                        removeFomCache(dto);
                    }
                    return true;
                }
            }
            Set<String> subscriptionCodesAdded = new HashSet<>();
            if (dto.getSubscriptionCodes() != null && !dto.getSubscriptionCodes().isEmpty()) {
                for (String subscriptionCode : dto.getSubscriptionCodes()) {
                    boolean isKeyAlreadyCached = putInCacheWithRetry(dto, subscriptionCode, 100, 3);
                    if (isKeyAlreadyCached) {
                        // Remove the current order code and its subscription codes already inserted in the cache
                        removeFromCacheWithRetry("ORDER_CODE_" + dto.getOrderCode(), 100, 3);
                        if (!subscriptionCodesAdded.isEmpty()) {
                            subscriptionCodesAdded.forEach(keyToBeRemoved -> removeFromCacheWithRetry(keyToBeRemoved, 100, 3));
                        }
                        log.info("The order {} is already cached through it's subscription code {} ", dto.getOrderCode(), subscriptionCode);
                        return true;
                    }
                    subscriptionCodesAdded.add(subscriptionCode);
                }
            }
        }
        return false;
    }

    /**
     * @param dto the order dto
     */
    private void removeFomCache(OrderDto dto) {
        removeFomCache(dto, dto.getOrderCode());
    }

    /**
     * @param dto the order dto
     * @param dto the order code ( the order dto code can be changed (i.e the order code can be renamed in cancel case) so the order code parameter can be different to order dto code)
     */
    public void removeFomCache(OrderDto dto, String orderCode) {
        if (dto != null) {
            if (!StringUtils.isBlank(orderCode)) {
                removeFromCacheWithRetry("ORDER_CODE_" + orderCode, 100, 3);
            }
            Set<String> subscriptionCodes = dto.getSubscriptionCodes();
            if (subscriptionCodes != null && !subscriptionCodes.isEmpty()) {
                subscriptionCodes.forEach(keyToBeRemoved -> removeFromCacheWithRetry(keyToBeRemoved, 100, 3));
            }
        }
    }

    /**
     * Remove item from the cache, and in case of CacheException, retry based on times and delay params.
     *
     * @param cacheKey the order code or the subscription code
     * @param delay    Delay between tries in ms
     * @param times    Number of tries to update the cache
     */
    private void removeFromCacheWithRetry(String cacheKey, long delay, final long times) {

        try {
            // adding Flag.IGNORE_RETURN_VALUES to enhance the update performances since we dont need a return value from
            // a store or from a remote node is not necessary
            Long value = ordersCache.getAdvancedCache().withFlags(Flag.IGNORE_RETURN_VALUES).remove(cacheKey);
            if (value != null) {
                log.info("The key {} is removed successfully from the cache", cacheKey);
            }
        } catch (CacheException e) {
            log.error("removeFromCacheWithRetry -> CacheException for [cacheKey = {}]", cacheKey, e);

            if (times > 0) {
                // waiting for the delay :
                ThreadUtils.sleepSafe(TimeUnit.SECONDS, delay);
                // then retry :
                this.removeFromCacheWithRetry(cacheKey, delay, times - 1);
            } else {
                throw e; // If all retries are failing, then throw the CacheException
            }
        }
    }

    /**
     * Put item in the cache , and in case of CacheException, retry based on times and delay params
     *
     * @param dto      the order dto
     * @param cacheKey the order code or the subscription code
     * @param delay    Delay between tries in ms
     * @param times    Number of tries to update the cache
     * @return true if there was no mapping for the key
     */
    private boolean putInCacheWithRetry(OrderDto dto, String cacheKey, long delay, final long times) {

        Long value = null;
        try {
            value = ordersCache.getAdvancedCache().putIfAbsent(cacheKey, 1000L);

        } catch (CacheException e) {
            log.error(" putInCacheWithRetry -> CacheException for [cacheKey = {}]", cacheKey, e);

            if (times > 0) {
                log.info(" putInCacheWithRetry : Retry with delay = {} and times = {} ", delay, times);
                // waiting for the delay :
                ThreadUtils.sleepSafe(TimeUnit.SECONDS, delay);
                // then retry :
                this.putInCacheWithRetry(dto, cacheKey, delay, times - 1);
            } else {
                throw e; // If all reties are failing, then throw the CacheException
            }
        }

        if (value != null) {
            if (value + 1000 < TIME_TO_LIVE_SECONDS) {
                log.info("The key {} take the {} seconds in the cache", cacheKey, value + 1000);
                ordersCache.getAdvancedCache().put(cacheKey, value + 1000);
                return true;
            }
            removeFomCache(dto);
        } else {
            log.info("The key {} is added successfully into the cache", cacheKey);
        }
        return false;
    }

    /**
     * Put item in the cache , and in case of CacheException, retry based on times and delay params
     *
     * @param cacheKey the order code or the subscription code
     * @param delay    Delay between tries in ms
     * @param times    Number of tries to update the cache
     * @return true if there was no mapping for the key
     */
    private boolean putInCacheWithRetry(String cacheKey, long delay, final long times) {

        Long value = null;
        try {
            value = ordersCache.getAdvancedCache().putIfAbsent(cacheKey, 1000L);

        } catch (CacheException e) {
            log.error(" putInCacheWithRetry -> CacheException for [cacheKey = {}]", cacheKey, e);

            if (times > 0) {
                log.info(" putInCacheWithRetry : Retry with delay = {} and times = {} ", delay, times);
                // waiting for the delay :
                ThreadUtils.sleepSafe(TimeUnit.SECONDS, delay);
                // then retry :
                this.putInCacheWithRetry(cacheKey, delay, times - 1);
            } else {
                throw e; // If all reties are failing, then throw the CacheException
            }
        }

        if (value != null) {
            return true;
        }
        log.info("The key {} is added successfully into the cache", cacheKey);
        return false;
    }

    /**
     * Add the key to the cache.
     *
     * @param cacheKey the key to be add into the cache
     * @return True is the key is already cached.
     */
    public boolean addToCache(String cacheKey) {
        boolean isKeyAlreadyCached = false;
        if (!StringUtils.isBlank(cacheKey)) {
            isKeyAlreadyCached = putInCacheWithRetry(cacheKey, 100, 3);
        }
        return isKeyAlreadyCached;
    }

    /**
     * Remove the key from the cache.
     *
     * @param cacheKey the key to be removed from the cache
     */
    public void removeFomCache(String cacheKey) {
        if (!StringUtils.isBlank(cacheKey)) {
            removeFromCacheWithRetry(cacheKey, 100, 3);
        }
    }
}