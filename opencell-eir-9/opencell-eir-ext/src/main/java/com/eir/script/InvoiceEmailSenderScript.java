package com.eir.script;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.ParamBeanFactory;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.Invoice;
import org.meveo.model.billing.InvoiceStatusEnum;
import org.meveo.model.communication.email.EmailTemplate;
import org.meveo.model.crm.EntityReferenceWrapper;
import org.meveo.model.jobs.JobExecutionResultImpl;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.base.ValueExpressionWrapper;
import org.meveo.service.billing.impl.InvoiceService;
import org.meveo.service.communication.impl.EmailSender;
import org.meveo.service.communication.impl.EmailTemplateService;
import org.meveo.service.job.JobExecutionResultService;
import org.meveo.service.job.JobInstanceService;
import org.meveo.service.script.Script;

/**
 * The Class SendEmailScript.
 * 
 * @author hznibar
 */
@Stateless
public class InvoiceEmailSenderScript extends Script {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    @Inject
    InvoiceService invoiceService;

    /** The param bean factory. */
    @Inject
    private ParamBeanFactory paramBeanFactory;

    @Inject
    private EmailSender emailSender;

    @Inject
    private EmailTemplateService emailTemplateService;

    @Inject
    private JobInstanceService jobInstanceService;
    
    @Inject
    private JobExecutionResultService jobExecutionResultService;

    /** The Constant DATETIME_FORMAT. */
    private static final String DATETIME_FORMAT = "ddMMyyyyHHmmss";

    /** The Constant EMAIL_SENDER. */
    private static final String EMAIL_SENDER = "CSBilling@eir.com";

    /** The Constant E_BILL_COPIED_TO_HUB. */
    private static final String E_BILL_COPIED_TO_HUB = "E_BILL_COPIED_TO_HUB";

    /** The Constant OAO_HUB_BILL_DELIVERY_TYPE. */
    private static final String OAO_HUB_BILL_DELIVERY_TYPE = "OAO_HUB";

    /** The Constant CSV_EXTENSION. */
    private static final String CSV_EXTENSION = ".csv";

    /** The Constant REPROT_FILE_NAME_PREFIX. */
    private static final String REPROT_FILE_NAME_PREFIX = "REPROT_FILE_NAME_PREFIX";

    /** The Constant REPORT_FOLDER. */
    private static final String REPORT_FOLDER = "REPORT_FOLDER";

    /** The Constant EMAIL_T_BY_EMAIL. */
    private static final String EMAIL_T_BY_EMAIL = "EMAIL_T_BY_EMAIL";
    
    /** The Constant EMAIL_T_BY_OAO_HUB. */
    private static final String EMAIL_T_BY_OAO_HUB = "EMAIL_T_BY_OAO_HUB";
    
    /** The Constant EMAIL_BILL_DELIVERY_TYPE. */
    private static final String EMAIL_BILL_DELIVERY_TYPE = "EMAIL";

    /** The Constant BILL_DELIVERY_TYPE. */
    private static final String BILL_DELIVERY_TYPE = "BILL_DELIVERY_TYPE";

    
    private static final String CONTEXT_ENTITY = "CONTEXT_ENTITY";

    /**
     * script main execute method.
     *
     * @param methodContext method context params
     * @throws BusinessException business exception
     */
    @Override
    public void execute(Map<String, Object> methodContext) throws BusinessException {

        List<Invoice> invoices = getNotAlreadySentInvoices();
        if (invoices == null || invoices.isEmpty()) {
            log.info("No invoices found to be delivered by email or to HUB");
            methodContext.put(Script.JOB_RESULT_NB_OK, 0);
            methodContext.put(Script.JOB_RESULT_NB_KO, 0);
            return;
        }

        log.info("{} invoices will be delivered by email or to HUB", invoices.size());

        JobExecutionResultImpl jobExecutionResult = (JobExecutionResultImpl) methodContext.get(Script.JOB_EXECUTION_RESULT);        
        jobExecutionResult.addNbItemsToProcess(invoices.size());

        String reportFolder = (String) methodContext.get(REPORT_FOLDER);
        String reportFileNamePrefix = (String) methodContext.get(REPROT_FILE_NAME_PREFIX);
        String reportFileName = reportFileNamePrefix + DateUtils.formatDateWithPattern(new Date(), DATETIME_FORMAT) + CSV_EXTENSION;
        reportFolder = paramBeanFactory.getChrootDir() + File.separator + reportFolder;
        File f = new File(reportFolder);
        if (!f.exists()) {
            f.mkdirs();
            log.info("Report folder created :" + reportFolder);
        }
        reportFileName = reportFolder + File.separator + reportFileName;

        try (BufferedWriter bw = new BufferedWriter(new FileWriter(reportFileName, true))) {

            BillingAccount ba;
            long lastBaId = 0L;
            Boolean sendEbill = false;
            String billDeliveryType;
            EntityReferenceWrapper emailTemplateCode = null;
            EmailTemplate emailTemplate;
            long itemProcessedWithError = 0L;
            long itemCorrectlyProcessed = 0L;
            for (Invoice invoice : invoices) {
                
                ba = invoice.getBillingAccount();
                billDeliveryType = (String) ba.getCfValue(BILL_DELIVERY_TYPE);
                if (EMAIL_BILL_DELIVERY_TYPE.equals(billDeliveryType)) {
                    emailTemplateCode = (EntityReferenceWrapper) ba.getBillingCycle().getCfValue(EMAIL_T_BY_EMAIL);
                    sendEbill = true;
                } else if (OAO_HUB_BILL_DELIVERY_TYPE.equals(billDeliveryType)) {
                    emailTemplateCode = (EntityReferenceWrapper) ba.getBillingCycle().getCfValue(EMAIL_T_BY_OAO_HUB);
                    sendEbill = false;
                }
                
                emailTemplate = emailTemplateCode != null ? emailTemplateService.findByCode(emailTemplateCode.getCode()) : null;
                boolean isSent = sendInvoiceByMail(invoice, emailTemplate, sendEbill);
                if (!isSent) {
                    itemProcessedWithError++;
                    String errorMsg = "could not send the invoice and eBill by Email. billingAccount: "+ba.getCode()+" invoice "+invoice.getCode()+" email template "+(emailTemplateCode != null ? emailTemplateCode.getCode() : null);
                    jobExecutionResult.addReport(errorMsg);
                    log.error(errorMsg);
                    continue;
                }

                itemCorrectlyProcessed++;

                if (lastBaId != ba.getId().longValue()) {
                    bw.write(ba.getCode() + "," + ba.getBillingCycle().getCode() + "," + ba.getContactInformation().getEmail() + "," + DateUtils.formatDateWithPattern(new Date(), DATETIME_FORMAT));
                    bw.newLine();
                }
                lastBaId = ba.getId().longValue();
            }
            methodContext.put(Script.JOB_RESULT_NB_OK, itemCorrectlyProcessed);
            methodContext.put(Script.JOB_RESULT_NB_KO, itemProcessedWithError);
        } catch (BusinessException e) {
            throw e;

        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }

    private boolean sendInvoiceByMail(Invoice invoice, EmailTemplate emailTemplate, Boolean sendEbill) throws BusinessException {

        if (emailTemplate == null) {
            return false;
        }

        try {
            List<String> to = new ArrayList<>();
            List<String> cc = new ArrayList<>();
            List<File> files = new ArrayList<>();

            // Verify that actual file exists
            String fileName = invoiceService.getFullPdfFilePath(invoice, false);
            File attachment = new File(fileName);
            if (!attachment.exists()) {
                log.error("PDF file was not found for invoice {}", invoice.getId());
                return false;
            }
            files.add(attachment);

            if(sendEbill == true) {
                // add ebill
                String ebillFileName = getFullEbillFilePath(invoice);
                File ebill = new File(ebillFileName);
                if (!ebill.exists()) {
                    log.error("eBill file was not found for invoice {}", invoice.getId());
                    //return false;
                } else {
                    files.add(ebill);
                }            
            }
            BillingAccount billingAccount = invoice.getBillingAccount();
            if (billingAccount.getContactInformation() != null) {
                to.addAll(Arrays.asList(billingAccount.getContactInformation().getEmail().split(";")));
            }
            if (billingAccount.getCcedEmails() != null) {
                cc.addAll(Arrays.asList(billingAccount.getCcedEmails().split(";")));
            }

            String sender = ParamBeanFactory.getAppScopeInstance().getProperty("invoice.email.sender", EMAIL_SENDER);
            cc.add(sender);

            if (to.isEmpty()) {
                log.warn("No Email is configured to receive the invoice {}", invoice.getId());
                return false;
            }

            Map<Object, Object> params = new HashMap<>();
            params.put("invoice", invoice);
            String subject = ValueExpressionWrapper.evaluateExpression(emailTemplate.getSubject(), params, String.class);
            String content = ValueExpressionWrapper.evaluateExpression(emailTemplate.getTextContent(), params, String.class);
            String contentHtml = ValueExpressionWrapper.evaluateExpression(emailTemplate.getHtmlContent(), params, String.class);

            emailSender.send(sender, Arrays.asList(sender), to, cc, null, subject, content, contentHtml, files, null, true);
            invoice.setStatus(InvoiceStatusEnum.SENT);
            invoice.setAlreadySent(true);
            invoiceService.update(invoice);
            return true;

        } catch (Exception e) {
            log.error("Failed to send invoice email notification for invoice {}", invoice.getId(), e);
            return false;
        }
    }

    /**
     * Return a list of invoices that not already sent.
     *
     * @return a list of invoices
     * @throws BusinessException the business exception
     */
    public List<Invoice> getNotAlreadySentInvoices() throws BusinessException {
        return invoiceService.getEntityManager().createQuery(
            "select i from Invoice i where i.alreadySent=false and i.pdfFilename is not null and (varcharFromJson(i.billingAccount.cfValues, BILL_DELIVERY_TYPE, string) ='EMAIL' or (varcharFromJson(i.billingAccount.cfValues, BILL_DELIVERY_TYPE, string) ='OAO_HUB' and booleanFromJson(i.cfValues, E_BILL_COPIED_TO_HUB, boolean, boolean)=true)) order by i.billingAccount.id, i.id",
            Invoice.class).getResultList();
    }

    /**
     * Get a full path of e-bill file
     * 
     * @param invoice Invoice
     * @return A full path to a e-bill file
     */
    private String getFullEbillFilePath(Invoice invoice) {
        String meveoDir = paramBeanFactory.getChrootDir() + File.separator;

        String ebillPath = meveoDir + "invoices" + File.separator + "pdf" + File.separator + (String) invoice.getCfValue("E_BILL_FILENAME");
        return ebillPath;
    }
}
