package com.eir.script;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.hibernate.Session;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.model.billing.AccountingCode;
import org.meveo.model.billing.BillingRun;
import org.meveo.model.billing.ChargeInstance;
import org.meveo.model.billing.InvoiceSubCategory;
import org.meveo.model.billing.WalletOperation;
import org.meveo.model.billing.WalletOperationStatusEnum;
import org.meveo.model.catalog.OneShotChargeTemplate;
import org.meveo.service.billing.impl.AccountingCodeService;
import org.meveo.service.billing.impl.BillingRunService;
import org.meveo.service.billing.impl.ChargeInstanceService;
import org.meveo.service.billing.impl.RatingService;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.billing.impl.WalletOperationService;
import org.meveo.service.catalog.impl.InvoiceSubCategoryService;
import org.meveo.service.catalog.impl.OneShotChargeTemplateService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.script.Script;
import org.primefaces.model.SortOrder;

/**
 * The Class VolumeDiscountApplicationScript.
 * 
 * @author hznibar
 */
@Stateless
public class VolumeDiscountApplicationScript extends Script {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    private static final String CUSTOMER_ACCOUNT = "customer_account";

    private static final String BILLING_ACCOUNT = "billing_account";

    private static final String DISCOUNT_TYPE = "discount_type";

//private static final String PROFILE_CODE = "profile_code";

    private static final String DESCRIPTION = "description";

    private static final String EMPTY_STRING = "";

    private static final String ACCOUNTING_CODE = "accounting_code";

    private static final String INVOICE_SUB_CAT = "invoice_sub_cat";

    private static final String MIGRATION_CODE = "migration_code";

    private static final String CHARGE_TYPE = "charge_type";

    private static final String AGGREGATE_SQL = "aggregate_sql";

    private static final String MIGRATION_CODE_PARAM = ":migration_code";

    private static final String CA_ID_PARAM = ":ca_id";

    private static final String BA_ID_PARAM = ":ba_id";

    private static final String EIR_OR_VOL_DISCOUNT_AGGR_TABLE = "EIR_OR_VOL_DISCOUNT_AGGR";

    private static final String AGGREGATE_TYPE = "aggregate_type";

    private static final String EIR_OR_VOL_DISCOUNT_TABLE = "EIR_OR_VOL_DISCOUNT";

    private static final String MINMAX_OPTIONAL_RANGE_VALID_FROM_VALID_TO = "minmaxOptionalRange valid_from valid_to";

    private static final String DISCOUNT_CHARGE_TEMPLATE_CODE = "DISCOUNT_CHARGE";

    private static final String NOT_FOUND_CHARGE_MESSAGE = "The one-shot charge DISCOUNT_CHARGE is not found";

    private static final String NOT_FOUND_BILLING_RUN = "Billing run not found!";

    private static final String NULL_BILLING_RUN_INVOICE_DATE = "The billing Run invoice date is null !";

    /** The custom table service. */
    @Inject
    private CustomTableService customTableService;

    @Inject
    ChargeInstanceService<ChargeInstance> chargeInstanceService;

    @Inject
    private OneShotChargeTemplateService chargeTemplateService;

    @Inject
    InvoiceSubCategoryService invoiceSubCategoryService;

    @Inject
    AccountingCodeService accountingCodeService;

    @Inject
    WalletOperationService walletOperationService;

    @Inject
    SubscriptionService subscriptionService;

    @Inject
    RatingService ratingService;

    @Inject
    BillingRunService billingRunService;

    /**
     * script main execute method.
     *
     * @param methodContext method context params
     * @throws BusinessException business exception
     */
    @Override
    public void execute(Map<String, Object> methodContext) throws BusinessException {
        try {

            BillingRun billingRun = null;
            Object brId = methodContext.get("BR_ID");

            if (brId != null) {
                billingRun = billingRunService.findById(Long.valueOf(brId.toString()));
            }
            if (billingRun == null) {
                throw new BusinessException(NOT_FOUND_BILLING_RUN + " Id = " + brId + ". Please enter the correct billing run id \"BR_ID\" in the job configuration variables!");
            }

            Date invoiceDate = billingRun.getInvoiceDate();
            if (invoiceDate == null) {
                throw new BusinessException(NULL_BILLING_RUN_INVOICE_DATE);
            }

            OneShotChargeTemplate chargetemplate = chargeTemplateService.findByCode(DISCOUNT_CHARGE_TEMPLATE_CODE);
            if (chargetemplate == null) {
                throw new BusinessException(NOT_FOUND_CHARGE_MESSAGE);
            }

            EntityManager em = walletOperationService.getEntityManager();

            // Get all discounts
            List<Map<String, Object>> volumeDiscounts = getActiveVolumeDiscounts(invoiceDate);

            if (!volumeDiscounts.isEmpty()) {
                String aggSqlId;
                Map<String, Object> aggSql;
                String sqlRequest;
                WalletOperation wo;
                WalletOperation oldWO;
                BigDecimal discountAmount;
                BigDecimal percent;
                List<Object[]> walletOperationsForDiscount;
                String volumeFields = EMPTY_STRING;
                String aggregationParameters = EMPTY_STRING;
                String newVolumeFields = EMPTY_STRING;
                String newAggregationParameters = EMPTY_STRING;
                String customerAccount;
                String billingAccount;
                String migrationCode;
                String discountType;
                String chargeType;
                String description;
                InvoiceSubCategory invoiceSubCat;
                AccountingCode accountingCode;
                for (Map<String, Object> volumeDiscount : volumeDiscounts) {
                    customerAccount = String.valueOf(volumeDiscount.get(CUSTOMER_ACCOUNT));
                    billingAccount = String.valueOf(volumeDiscount.get(BILLING_ACCOUNT));
                    migrationCode = String.valueOf(volumeDiscount.get(MIGRATION_CODE));
                    discountType = String.valueOf(volumeDiscount.get(DISCOUNT_TYPE));
                    chargeType = String.valueOf(volumeDiscount.get(CHARGE_TYPE));
                    aggSqlId = String.valueOf(volumeDiscount.get(AGGREGATE_TYPE));
                    newAggregationParameters = customerAccount.concat(billingAccount).concat(migrationCode).concat(aggSqlId);
                    newVolumeFields = newAggregationParameters.concat(discountType).concat(chargeType);
                    if (!newVolumeFields.equals(volumeFields)) {
                        volumeFields = newVolumeFields;
                        aggSql = customTableService.findById(EIR_OR_VOL_DISCOUNT_AGGR_TABLE, Long.valueOf(aggSqlId));
                        if (aggSql != null && aggSql.size() > 0) {
                            // run the aggregation sql only if the SQL query or one of its parameters changes.
                            if (!aggregationParameters.equals(newAggregationParameters)) {
                                aggregationParameters = newAggregationParameters;
                                sqlRequest = (String) aggSql.get(AGGREGATE_SQL);
                                if (sqlRequest.contains(BA_ID_PARAM)) {
                                    sqlRequest = sqlRequest.replace(BA_ID_PARAM, billingAccount);
                                }
                                if (sqlRequest.contains(CA_ID_PARAM)) {
                                    sqlRequest = sqlRequest.replace(CA_ID_PARAM, customerAccount);
                                }
                                if (sqlRequest.contains(MIGRATION_CODE_PARAM)) {
                                    sqlRequest = sqlRequest.replace(MIGRATION_CODE_PARAM, "'%" + migrationCode.replace("*", EMPTY_STRING) + "%'");
                                }
                                executeAggregateSql(sqlRequest);
                            }
                            // profileCode = String.valueOf(volumeDiscount.get(PROFILE_CODE));

                            String percentSql = getDiscountPercentSql(newVolumeFields, invoiceDate);
                            String descriptionSql = getDiscountDescriptionSql(newVolumeFields, invoiceDate);

                            walletOperationsForDiscount = getWalletOperationsForDiscount(chargeType, migrationCode, percentSql, descriptionSql, invoiceDate);                            
                            if (walletOperationsForDiscount != null && walletOperationsForDiscount.size() > 0) {
                                invoiceSubCat = em.getReference(InvoiceSubCategory.class, ((BigInteger) volumeDiscount.get(INVOICE_SUB_CAT)).longValue());
                                accountingCode = em.getReference(AccountingCode.class, ((BigInteger) volumeDiscount.get(ACCOUNTING_CODE)).longValue());
                                for (Object[] row : walletOperationsForDiscount) {
                                    oldWO = walletOperationService.findById(((BigInteger) row[0]).longValue());
                                    // percent = getDiscountPercent(newVolumeFields,discountType,(BigInteger) row[1]);
                                    percent = (BigDecimal) row[2];
                                    description = (String) row[3];
                                    if (percent.compareTo(BigDecimal.ZERO) != 0) { // this check is already done at the sql request
                                        discountAmount = oldWO.getAmountWithoutTax() != null
                                                ? oldWO.getAmountWithoutTax().multiply(percent).divide(BigDecimal.valueOf(100), 12, RoundingMode.HALF_EVEN).negate()
                                                : null;

                                        wo = new WalletOperation(DISCOUNT_CHARGE_TEMPLATE_CODE, description, oldWO.getWallet(),
                                            oldWO.getOperationDate(), oldWO.getInvoicingDate(), oldWO.getType(), oldWO.getCurrency(), oldWO.getTax(), discountAmount, null, null,
                                            BigDecimal.ONE, discountAmount, null, null, null, null, null, null, oldWO.getStartDate(), oldWO.getEndDate(),
                                            oldWO.getSubscriptionDate(), oldWO.getOfferTemplate(), oldWO.getSeller(), null, null, BigDecimal.ONE, oldWO.getOrderNumber(),
                                            invoiceSubCat, accountingCode, WalletOperationStatusEnum.OPEN, oldWO.getUserAccount(), oldWO.getBillingAccount());

//                                  wo = new WalletOperation(DISCOUNT_CHARGE_TEMPLATE_CODE, (String) volumeDiscount.get(DESCRIPTION), oldWO.getWallet(), oldWO.getOperationDate(), oldWO.getInvoicingDate(),
//                                      oldWO.getType(), oldWO.getCurrency(), oldWO.getTax(), discountAmount, null, null, BigDecimal.ONE, discountAmount, null, null, null, null, null, null, oldWO.getStartDate(),
//                                      oldWO.getEndDate(), oldWO.getSubscriptionDate(), oldWO.getOfferTemplate(), oldWO.getSeller(), null, null, BigDecimal.ONE, oldWO.getOrderNumber(), invoiceSubCat,
//                                      WalletOperationStatusEnum.OPEN); // TODO add accountingCode when move to OC v. 10

                                        wo.setTaxClass(oldWO.getTaxClass());
                                        wo.setSubscription(oldWO.getSubscription());
                                        wo.setChargeInstance(oldWO.getChargeInstance());
                                        wo.setServiceInstance(oldWO.getServiceInstance());
                                        oldWO.setParameterExtra(DISCOUNT_CHARGE_TEMPLATE_CODE); // to not apply the discount for several times
                                        ratingService.calculateAmounts(wo, wo.getUnitAmountWithoutTax(), wo.getUnitAmountWithTax());                                        
                                        walletOperationService.create(wo);
                                        walletOperationService.update(oldWO);
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BusinessException(e);
        }

    }

//protected BigDecimal getDiscountPercent(String concatFields, String discountType, BigInteger rowNumber) {
//  String percentSql;
//  concatFields = concatFields.replaceAll("null", "");
//  if (TIER_DISCOUNT.equals(discountType)) {
//      percentSql = "select max (percent) from EIR_OR_VOL_DISCOUNT v where v.band_from <= " + rowNumber;
//  } else {
//      percentSql = "select min (percent) from EIR_OR_VOL_DISCOUNT v where v.band_from >= " + rowNumber;
//  }
//  percentSql += " and concat(customer_account,billing_account,migration_code,aggregate_type,discount_type,charge_type) = '" + concatFields + "' and (v.valid_from <= '" + invoiceDate + "' and (v.valid_to IS NULL or v.valid_to > '" + invoiceDate
//          + "'))";
//  Object percent = customTableService.getEntityManager().createNativeQuery(percentSql).getSingleResult();
//  return percent != null ? new BigDecimal(percent.toString()) : BigDecimal.ZERO;
//}

    /**
     * Gets the discount percent sql.
     *
     * @param concatFields the concat fields
     * @param invoiceDate the invoice date
     * @return the discount percent sql
     */
    protected String getDiscountPercentSql(String concatFields, Date invoiceDate) {
        String percentSql;
        concatFields = concatFields.replaceAll("null", "");
        percentSql = "(select max (percent) from EIR_OR_VOL_DISCOUNT v where v.band_from <= vw.row_number and concat(customer_account,billing_account,migration_code,aggregate_type,discount_type,charge_type) = '" + concatFields + "' and (v.valid_from <= '"
                + invoiceDate + "' and (v.valid_to IS NULL or v.valid_to > '" + invoiceDate + "')))";
        return percentSql;
    }

    /**
     * Gets the description of the discount
     * @param concatFields the concat fields
     * @param invoiceDate the invoice date
     * @return the discount description sql
     */
    protected String getDiscountDescriptionSql(String concatFields, Date invoiceDate) {
        String percentSql;
        concatFields = concatFields.replaceAll("null", "");
        percentSql = "(select description from EIR_OR_VOL_DISCOUNT where percent =( select max (percent) from EIR_OR_VOL_DISCOUNT v where v.band_from <= vw.row_number and concat(customer_account,billing_account,migration_code,aggregate_type,discount_type,charge_type) = '" + concatFields + "' and (v.valid_from <= '"
                + invoiceDate + "' and (v.valid_to IS NULL or v.valid_to > '" + invoiceDate + "'))) limit 1)";
        return percentSql;
    }


    /**
     * Gets the active volume discounts.
     * 
     * @param invoiceDate
     *
     * @return the active volume discounts
     */
    protected List<Map<String, Object>> getActiveVolumeDiscounts(Date invoiceDate) {
        Map<String, Object> filters = new HashMap<>();
        filters.put(MINMAX_OPTIONAL_RANGE_VALID_FROM_VALID_TO, invoiceDate);
        Object[] sortOrdering = new Object[] { CUSTOMER_ACCOUNT, SortOrder.ASCENDING, BILLING_ACCOUNT, SortOrder.ASCENDING, MIGRATION_CODE, SortOrder.ASCENDING, DISCOUNT_TYPE,
                SortOrder.ASCENDING, CHARGE_TYPE, SortOrder.ASCENDING };
        return customTableService.list(EIR_OR_VOL_DISCOUNT_TABLE, new PaginationConfiguration(null, null, filters, null, null, sortOrdering));
    }

    /**
     * Gets the wallet operations for discount.
     *
     * @param chargeType the charge type
     * @param migrationCode the migration code
     * @return the wallet operations for discount
     */
    @SuppressWarnings("unchecked")
    protected List<Object[]> getWalletOperationsForDiscount(String chargeType, String migrationCode, String percentSql, String descriptionSql, Date invoiceDate) {
        String sql = "select wo.id , vw.row_number, " + percentSql + ","+ descriptionSql + " from billing_wallet_operation wo "
                + "join mview_volume_discount_subscription vw on wo.subscription_id = vw.subscription_id " + "join billing_service_instance s on wo.service_instance_id = s.id and "
                + percentSql + " is not null " + "join cat_service_template st on s.service_template_id = st.id " + "and st.cf_values like '%" + migrationCode
                + "%' and wo.code <> '" + DISCOUNT_CHARGE_TEMPLATE_CODE + "' " + "and (wo.invoicing_date IS NULL or wo.invoicing_date <= '"
                + invoiceDate + "') and (parameter_extra is null or parameter_extra <> '" + DISCOUNT_CHARGE_TEMPLATE_CODE + "') "; // to not apply the discount for several times

        if (!"*".equals(chargeType)) {
            String inChargeType = "('R')";
            if(!"R".equals(chargeType)) {
                inChargeType = "('O','S')";
            }
            sql += "join billing_charge_instance ch on wo.charge_instance_id = ch.id " + "and ch.charge_type in " + inChargeType;
        }        
        Query query = walletOperationService.getEntityManager().createNativeQuery(sql);
        return query.getResultList();
    }

    /**
     * Execute aggregate sql.
     *
     * @param sqlRequest the sql request
     */
    protected void executeAggregateSql(final String sqlRequest) {
        Session hibernateSession = subscriptionService.getEntityManager().unwrap(Session.class);

        hibernateSession.doWork(new org.hibernate.jdbc.Work() {

            @Override
            public void execute(Connection connection) throws SQLException {

                try (Statement statement = connection.createStatement()) {
                    statement.execute("drop materialized view if exists mview_volume_discount_subscription");
                    statement.execute("create materialized view mview_volume_discount_subscription (subscription_id, row_number) as " + sqlRequest);
                    statement.execute("create index volume_discount_subscription_pk on mview_volume_discount_subscription (subscription_id)");
                } catch (Exception e) {
                    log.error("Failed to drop/create the materialized view mview_volume_discount_subscription");
                    throw new BusinessException(e);
                }
            }
        });
    }
}
