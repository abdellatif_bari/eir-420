/*
 * (C) Copyright 2015-2020 Opencell SAS (https://opencellsoft.com/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
 * OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS
 * IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO
 * THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 *
 * For more information on the GNU Affero General Public License, please consult
 * <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

package com.eir.script;

import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import org.meveo.admin.exception.BusinessException;
import org.meveo.security.CurrentUser;
import org.meveo.security.MeveoUser;
import org.meveo.service.medina.impl.CDRService;
import org.meveo.service.script.Script;

/**
 * A script for moving written-off CDR from CDR table to Custom table EIR_U_CDR_WRITTEN_OFF
 * 
 * @author Mohammed Amine Tazi
 */
@Stateless
public class CDRWritingOffScript extends Script {

    private static final long serialVersionUID = -9117095335653744694L;
    
    @Inject
    private CDRService cdrService;
    
    @Inject
    @CurrentUser
    protected MeveoUser currentUser;

    
    @Override
    public void execute(Map<String, Object> methodContext) throws BusinessException {
        String query = "insert into eir_u_cdr_written_off (select * from rating_cdr where status = 'DISCARDED')";
        int nbCdrs = cdrService.getEntityManager().createNativeQuery(query).executeUpdate();
        
        query = "update eir_u_cdr_written_off set user_name = :userName where id in (select id from rating_cdr where status = 'DISCARDED')";

        cdrService.getEntityManager().createNativeQuery(query)
                                     .setParameter("userName", currentUser.getUserName())
                                     .executeUpdate();
        
        cdrService.getEntityManager().createQuery("DELETE FROM CDR where status = 'DISCARDED'").executeUpdate();
        
        log.debug("{} written-off CDRs moved to EIR_U_CDR_WRITTEN_OFF table", nbCdrs);
    }
}
