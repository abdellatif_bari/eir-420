package com.eir.script;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.FlushModeType;

import org.hibernate.LockMode;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.NumberUtils;
import org.meveo.commons.utils.ParamBean;
import org.meveo.commons.utils.ParamBeanFactory;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.DatePeriod;
import org.meveo.model.ICustomFieldEntity;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.BillingCycle;
import org.meveo.model.billing.BillingRun;
import org.meveo.model.billing.Invoice;
import org.meveo.model.billing.RatedTransaction;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.WalletOperation;
import org.meveo.model.catalog.RoundingModeEnum;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.crm.EntityReferenceWrapper;
import org.meveo.model.crm.custom.CustomFieldValue;
import org.meveo.model.crm.custom.CustomFieldValues;
import org.meveo.model.customEntities.CustomEntityInstance;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.model.shared.DateUtils;
import org.meveo.model.shared.Name;
import org.meveo.model.tax.TaxClass;
import org.meveo.model.transformer.AliasToEntityOrderedMapResultTransformer;
import org.meveo.service.admin.impl.TradingCurrencyService;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.billing.impl.InvoiceService;
import org.meveo.service.billing.impl.RatedTransactionService;
import org.meveo.service.billing.impl.WalletOperationService;
import org.meveo.service.crm.impl.CustomFieldTemplateService;
import org.meveo.service.custom.CustomEntityInstanceService;
import org.meveo.service.script.Script;
import org.meveo.service.tax.TaxClassService;

import com.eir.commons.enums.customfields.ServiceInstanceCFsEnum;
import com.eir.commons.enums.customfields.ServiceParametersCFEnum;
import com.eir.commons.enums.customfields.SubscriptionCFsEnum;
import com.eir.service.billing.impl.SubscriptionService;
import com.eir.service.catalog.impl.ServiceTemplateService;
import com.eir.service.commons.UtilService;
import com.eir.service.order.impl.OrderService;

/**
 * The Class SendEmailScript.
 *
 * @author hznibar
 */
@Stateless
public class EbillGeneratorScript extends Script {

    private static final String END_BLOPHEAD = "<END BLOPHEAD>";

    private static final String START_BLOPHEAD = "<START BLOPHEAD>\r\n"
            + "CYCLE NO.,MAJOR ACCNT. NO.,SUM SEQ NO.,FLANGIND,CUST NAME,LAST BIL AMT,BAL OS,QAMT TOT,ISSUE DATE,DUE DATE,FDISTCOD,TOTAL DUE,TOT PAID,MESSAGE1,MESSAGE2,PRINT ADDR1,PRINT ADDR2,PRINT ADDR3,PRINT ADDR4,PRINT ADDR5,FSETCNT,FPGCNTTS,FPGCNTPS,FPGCNTCL,FPGCNTFL,FPGCNTCA,FPGCNTFA,FPGCNTOS,FPGCNTVS,FPGCNTVE,FPGCNTCC,AVAT AMT,BVAT AMT,CVAT AMT,DVAT AMT,CUR";

    private static final String BLOPEXP_HEADER = "<START BLOPEXP>\r\n"
            + "CYCLE NO.,MAJOR ACCOUNT NO.,ACCOUNT NO.,DISTRICT CODE,BILL SEQUENCE,AMOUNT EXEMPT,VAT RATE,VAT EXEMPT REF NO.,CUR\r\n" + "<END BLOPEXP>";

    private static final String START_BLOPCALL = "<START BLOPCALL>\r\n"
            + "SORT-NO,MASTER ACCOUNT NUMBER,ACCOUNT NUMBER,BILL SEQ,STD,TELEPHONE,CALL TYPE,CALL TYPE DESCRIPTION,RATE,DATE,TIME,TERMINATING NUMBER,NUMBER OF CALLS,DESTINATION,BAND,DURATION-SEC,CLASS OF SERVICE,C-O-S DESCRIPTION,CALL-TRACKING,"
            + "CALL-TRACK DESCRIPTION,TOTALS DESCRIPTION,TOTAL";

    private static final String END_BLOPCALL = "<END BLOPCALL>";

    private static final String START_BLOPMAJ = "<START BLOPMAJ>\r\n" + "MAJOR ACC NO,MINOR ACC NO,STD CODE,TEL NO,DIST CODE";

    private static final String END_BLOPMAJ = "<END BLOPMAJ>";

    private static final String START_BLOPOCD = "<START BLOPOCD>\r\n" + "FRUNDTE,FMACNO,FACCNO,FDISTCOD,FBILSEQ,FOOCTYP,FOOCDES,FOOCAMT,FSTD,FTELNO,FNORECON,FXFRACCN,FLANGIND,CUR,FROMDATE,TODATE,QUANTITY,REFERENCE";

    private static final String END_BLOPOCD = "<END BLOPOCD>";

    private static final String START_BLOPOCH = "<START BLOPOCH>\r\n" + "FRUNDTE,FMACNO,FACCNO,FDISTCOD,FBILSEQ,FORDNO,FREF,FCHGDES,FCHGAMT,CUR,FORENFRM,FORENTO";

    private static final String END_BLOPOCH = "<END BLOPOCH>";

    private static final String START_BLOPORD = "<START BLOPORD>\r\n"
            + "FRUNDTE,FMACNO,FACCNO,FDISTCOD,FBILSEQ,FORDNO,FCMPLDTE,FRECPTNO,FTOTORD,FRNTDES,FRNTAMT,FORDDES,FORDAMT,FTRNDES,FTRNAMT,FDEPDES,FDEPAMT,FRRCDES,FRRCAMT,FFFRCDES,FFFRCCHG,FBPSDES,FBPSTOT,CUR";

    private static final String END_BLOPORD = "<END BLOPORD>";

    private static final String START_BLOPORI = "<START BLOPORI>\r\n" + "FRUNDTE,FMACNO,FACCNO,FDISTCOD,FBILSEQ,FORDNO,FSTD,FTELNO,FBLITDES,FOSOCQTY,FREF,FACTDES,CUR";

    private static final String END_BLOPORI = "<END BLOPORI>";

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    @Inject
    InvoiceService invoiceService;

    @Inject
    BillingAccountService billingAccountService;

    @Inject
    SubscriptionService subscriptionService;

    @Inject
    OrderService orderService;

    @Inject
    TradingCurrencyService tradingCurrencyService;

    @Inject
    RatedTransactionService ratedTransactionService;

    @Inject
    WalletOperationService walletOperationService;

    @Inject
    ServiceTemplateService serviceTemplateService;

    @Inject
    CustomEntityInstanceService customEntityInstanceService;
    /**
     * The param bean factory.
     */
    @Inject
    private ParamBeanFactory paramBeanFactory;

    @Inject
    private UtilService utilService;

    @Inject
    private TaxClassService taxClassService;

    @Inject
    private CustomFieldTemplateService cftService;

    /**
     * Custom field template "SERVIVE_PARAMETERS" definition
     */
    private static CustomFieldTemplate serviceParamsCFT;

    /**
     * The Constant DATETIME_FORMAT.
     */
    private static final String DATE_FORMAT_DDMMYYYY = "ddMMyyyy";

    private static final String DATE_FORMAT_YYYYMMDD = "yyyyMMdd";

    private static final String INVOICE_DATE_FORMAT = "dd/MM/yyyy";

    /**
     * The Constant CSV_EXTENSION.
     */
    private static final String CSV_EXTENSION = ".csv";

    private static final String CSV_SEPARATOR = ",";

    private static final String EMPTY_STRING = "";

    private static final String NGN_SERVICE_TYPE = "NGN";

    private static final String PPC_ICP_INT_LL_SERVICE_TYPE = "PPC_ICP_INT_LL";

    private static final String AUTHORIZED_SERVICE_TYPE = "DSL_FTT_LLU_VUA_NGN_PPC_ICP_INT_LL_AGC_RIO_DFN_MAST_COLO_RWO_WLR";

    private static final String NON_TRAFFIC_SERVICE_TYPE = "AGC_RIO_DFN_MAST_COLO_RWO";

    private static final String OMS_HEADER = "0,CUSTOMER_NUMBER,CONTRACT_NAME,ACCOUNT,BILL_NUMBER,BILL_DATE,CIRCUIT TYPE,CIRCUIT NUMBER,TRANSDATE,TRANSDATE_TO,QUANTITY,CHARGE TYPE,INVOICE_HEADER,PRODUCT_DESCRIPTION,AMOUNT,INCLUSIVE DISCOUNT,TAX_RATE,CURRENCY,ORDER_ID,OPERATOR_ORDER_NUMBER,CHARGE_REFERENCE,SERVICE INSTANCE ID,A End Name,A End Address,B End Name,B End Address,DISTANCE,A ASSOCIATION";

    private static final String UG_HEADER = "0,CUSTOMER_NUMBER,CONTRACT_NAME,ACCOUNT,BILL_NUMBER,BILL_DATE,AREA,TELEPHONE_NUMBER,TRANSDATE,TRANSDATE_TO,QUANTITY,CHARGE_TYPE,INVOICE_HEADER,PRODUCT_DESCRIPTION,AMOUNT,INCLUSIVE_DISCOUNT,TAX_RATE,CURRENCY,ORDER_ID,OPERATOR_ORDER_NUMBER,CHARGE_REFERENCE";

    private static final String WLR_HEADER = "<START BLOPABP>\r\n"
            + "CYCLE, CYCLE,DISTRICT,MAJOR ACC NO,ACC NO,BILL SEQ,BILL ISSUE DATE,LANG IND,PLAN ID,PLAN DESC,SUBSCRIPTION,SUB LEVEL,DISCOUNT,ACCUM LEVEL,PENALTY,TOTAL,CUR\r\n"
            + "<END BLOPABP>";

    private static final String BLOPPTD_HEADER = "<START BLOPPTD>\r\n"
            + "CYCLE,DIST,MAJOR ACC NO,ACC NO,BILL SEQ,BILL ISSUE DATE,GROUP ACC NO,GROUP BILL SEQ,STD,TELNO,ACCUM LEVEL,LANG IND,PLAN ID,PLAN DESC,PTD DISCNT,PTD USAGE,CONTR USAGE,START DATE,SCH END DATE,CUR\r\n"
            + "<END BLOPPTD>";

    private static final String BLOPSAV_HEADER = "<START BLOPSAV>\r\n"
            + "CYCLE NO.,MAJOR ACCOUNT NO.,ACCOUNT NO.,DISTRICT CODE,BILL SEQUENCE,STD TELNO,BILLING LITERAL1,BILLING LITERAL2,GROSS SAVINGS,NETT SAVINGS,GROSS USAGE,NETT USAGE,PERCENTAGE SAVING,CUR\r\n"
            + "<END BLOPSAV>";

    private static final String START_BLOPRENT = "<START BLOPRENT>\r\n"
            + "SORT-NO,MAJOR ACCOUNT NUMBER,ACCOUNT NUMBER,BILL SEQ,STD,TELEPHONE,TYPE,SOC CODE,SOC CODE DESCRIPTION,FORENFRM,FORENTO,QUANTITY,PRICE,TOTAL";

    private static final String END_BLOPRENT = "<END BLOPRENT>";

    private static final String START_BLOPSUMA = "<START BLOPSUMA>\r\n"
            + "CYCLE NO.,MAJOR A/C ,ACCOUNT NO.,FDISTCOD,FBILSEQ,NO. OF LINES,ISSUE DATE,RENTAL FROM,RENTAL TO,CALLS FROM,CALLS TO,RECURR RENT,RECURR EQUIP,FDIRRENT,PLAN CR/DR,TOTAL CALLS,OTHER CR/DR,ARREARS,TOTAL,CUR,FTRENTCR,FFUNTVAL,ALLOW,TYPE,DATE,REF";

    private static final String END_BLOPSUMA = "<END BLOPSUMA>";

    private static final String START_BLOPVAT = "<START BLOPVAT>\r\n" + "CYCLE NO.,MAJOR ACCNT. NO.,ACCOUNT NO.,FDISTCOD,FBILSEQ,TOTAL,VAT CHARGE,TOTAL VAT,CUR";

    private static final String END_BLOPVAT = "<END BLOPVAT>";

    private static final String SERVICE_TYPE = "SERVICE_TYPE";

    private static final String EBILL_FORMAT = "Ebill_Format";

    private static final String UNDERSCORE = "_";

    private static final String DOUBLE_QUOTES_CHARACTER = "\"";

    private static final String DATA_EXTENSION = ".DATA";

    private static final String DOT = ".";

    private static final String WLR_SERVICE_TYPE = "WLR";

    private static final String AGENT_CODE = "AGENT_CODE";

    Map<String, String> orderActionByService = new HashMap<String, String>();

    static final Map<String, String> STD_MAP = new HashMap<String, String>() {
        {
            put("01", "01");
            put("021", "021");
            put("022", "022");
            put("023", "023");
            put("024", "024");
            put("025", "025");
            put("026", "026");
            put("027", "027");
            put("028", "028");
            put("029", "029");
            put("0402", "0402");
            put("0404", "0404");
            put("041", "041");
            put("042", "042");
            put("043", "043");
            put("044", "044");
            put("045", "045");
            put("046", "046");
            put("047", "047");
            put("049", "049");
            put("0504", "0504");
            put("0505", "0505");
            put("051", "051");
            put("052", "052");
            put("053", "053");
            put("056", "056");
            put("057", "057");
            put("058", "058");
            put("059", "059");
            put("061", "061");
            put("062", "062");
            put("063", "063");
            put("064", "064");
            put("065", "065");
            put("066", "066");
            put("067", "067");
            put("068", "068");
            put("069", "069");
            put("071", "071");
            put("074", "074");
            put("090", "090");
            put("090", "090");
            put("091", "091");
            put("093", "093");
            put("094", "094");
            put("095", "095");
            put("096", "096");
            put("097", "097");
            put("098", "098");
            put("099", "099");
        }
    };

    @Override
    public void init(Map<String, Object> methodContext) throws BusinessException {
        super.init(methodContext);
    }

    /**
     * script main execute method.
     *
     * @param methodContext method context params
     * @throws BusinessException business exception
     */
    @Override
    public void execute(Map<String, Object> methodContext) throws BusinessException {
        Invoice invoice = (Invoice) methodContext.get("record");
        invoice = invoiceService.findById(invoice.getId());
        
        BillingAccount ba = invoice.getBillingAccount();
        if (ba == null) {
            throw new BusinessException("Can not generate eBill for the invoice: " + invoice.getInvoiceNumber() + " , Billing Account: null");
        }
        String serviceType = (String) ba.getCfValue(SERVICE_TYPE);
        
        CustomEntityInstance cei = customEntityInstanceService.findByCodeByCet(SERVICE_TYPE, serviceType);
        if (cei == null || cei.getCfValue(EBILL_FORMAT) == null) {
            throw new BusinessException(
                    "Can not find e-bill format for service type " + serviceType);
        }
        
        log.info("Generating e-bill for invoice {}/{}", invoice.getId(), invoice.getInvoiceNumber());
            
        String ebillFormat = ((EntityReferenceWrapper) cei.getCfValue(EBILL_FORMAT)).getCode();

        String eBillFileName = getEbillFileName(invoice);
        String eBillPath = paramBeanFactory.getChrootDir() + File.separator + "invoices" + File.separator + "pdf" + File.separator;
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(eBillPath + eBillFileName, false))) {
            generateEbill(bw, invoice, ebillFormat);
            invoice.setCfValue("E_BILL_FILENAME", eBillFileName);
            invoiceService.update(invoice);

            log.trace("Generated e-bill for invoice {}/{}", invoice.getId(), invoice.getInvoiceNumber());
            
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }

    /**
     * Generate The ebill.
     *
     * @param bw          the Buffered Writer
     * @param invoice     the invoice
     * @param ebillForamt the e-bill format
     * @throws IOException Signals that an I/O exception has occurred.
     */
    private void generateEbill(BufferedWriter bw, Invoice invoice, String ebillFormat) throws IOException {
        if (WLR_SERVICE_TYPE.equals(ebillFormat)) {
            // The Header contains BLOPABP section
            bw.write(WLR_HEADER);
        } else if (NGN_SERVICE_TYPE.equals(ebillFormat) || PPC_ICP_INT_LL_SERVICE_TYPE.equals(ebillFormat)) {
            bw.write(OMS_HEADER);
            if (NGN_SERVICE_TYPE.equals(ebillFormat)) {
                bw.write(",B ASSOCIATION,C ASSOCIATION");
            }
        } else {
            if (NON_TRAFFIC_SERVICE_TYPE.equals(ebillFormat)) {
                bw.write(UG_HEADER.replace("TELEPHONE_NUMBER", "SUBSCRIPTION"));
                bw.write(",SERVICE INSTANCE ID");
            } else {
                bw.write(UG_HEADER);
                bw.write(",AGENT,SERVICE INSTANCE ID,SUBSCRIBER,UAN");
            }
        }

        writeBody(bw, invoice, ebillFormat);

    }

    /**
     * Write body for the Ebill.
     *
     * @param bw          the Buffered Writer
     * @param invoice     the invoice
     * @param serviceType the service type
     * @throws IOException Signals that an I/O exception has occurred.
     */
    @SuppressWarnings("unchecked")
    private void writeBody(BufferedWriter bw, Invoice invoice, String ebillFormat) throws IOException {
        BillingAccount ba = invoice.getBillingAccount();
        CustomerAccount ca = ba.getCustomerAccount();
        String customerNumber = ca != null ? ca.getCode() : "";

        String contratName = ba.getDescription();
        if (StringUtils.isBlank(contratName)) {
            Name name = ba.getName();
            if (name == null) {
                contratName = EMPTY_STRING;
            } else {
                contratName = (name.getFirstName() != null ? name.getFirstName() + " " : EMPTY_STRING) + (name.getLastName() != null ? name.getLastName() : EMPTY_STRING);
            }

        }
        // add a quote " character if the contratName contains already a csv separator
        if (contratName.contains(CSV_SEPARATOR)) {
            contratName = DOUBLE_QUOTES_CHARACTER + contratName + DOUBLE_QUOTES_CHARACTER;
        }

        Map<Long, String[]> subscriptionMap = new HashMap<Long, String[]>();
        if (NGN_SERVICE_TYPE.equals(ebillFormat) || PPC_ICP_INT_LL_SERVICE_TYPE.equals(ebillFormat)) {
            List<Object[]> associations = getAssociationsAndAddresses(invoice.getId());

            for (Object[] association : associations) {
                // subscription_id a_association b_association c_association a_ep_name a_address b_ep_name b_address
                if(association[0] != null) {
                    subscriptionMap.put(((BigInteger) association[0]).longValue(), new String[]{Objects.toString(association[1],""), Objects.toString(association[2],""),
                            Objects.toString(association[3],""),Objects.toString(association[4],""),Objects.toString(association[5],""),Objects.toString(association[6],"")
                            ,Objects.toString(association[7],"")});
                }
            }

        }
        
        ScrollableResults ratedTransactions = getRatedTransactionsByInvoice(invoice, ebillFormat);

        String account = ba.getExternalRef1() != null ? ba.getExternalRef1() : EMPTY_STRING;
        String billNumber = invoice.getInvoiceNumber() != null ? invoice.getInvoiceNumber() : EMPTY_STRING;
        String billDate = DateUtils.formatDateWithPattern(invoice.getInvoiceDate(), INVOICE_DATE_FORMAT);
        String currency = invoice.getTradingCurrency() != null ? invoice.getTradingCurrency().getCurrencyCode()
                : ((ca != null && ca.getTradingCurrency() != null) ? ca.getTradingCurrency().getCurrencyCode() : EMPTY_STRING);
        Subscription subscription;
        ServiceInstance serviceInstance;
        WalletOperation wo;
        Map<String, String> serviceParametersCF = null;

        if (ratedTransactions != null && ratedTransactions.next()) {
            String transDate;
            String transDateTo;
            String forenfrm;
            String forento;
            Date rentalFrom = null;
            Date rentalTo = null;
            Date callsFrom = null;
            Date callsTo = null;
            String qty;
            String chargeType;
            String invoiceHeader;
            String productDescription;
            String amount;
            String inclusiveDiscount;
            String taxRate;
            String chargeReference = EMPTY_STRING;
            String orderId = null;
            String operatorOrderNumber = EMPTY_STRING;
            int fref = 0;
            String seqno;
            String distance;
            String area = EMPTY_STRING;
            String telNumber = EMPTY_STRING;
            String agent = EMPTY_STRING;
            String subscriber = EMPTY_STRING;
            String uan = EMPTY_STRING;
            // NGN fields
            String circuitType = EMPTY_STRING;
            String circuitNumber = EMPTY_STRING;
            String aEndName = EMPTY_STRING;
            String aEndAddress = EMPTY_STRING;
            String bEndName = EMPTY_STRING;
            String bEndAddress = EMPTY_STRING;
            String aAssociation = EMPTY_STRING;
            String bAssociation = EMPTY_STRING;
            String cAssociation = EMPTY_STRING;
            String subscriptionCode = null;


            StringBuilder blopmajBody = new StringBuilder();
            StringBuilder blopocdBody = new StringBuilder();
            StringBuilder blopochBody = new StringBuilder();
            StringBuilder blopordBody = new StringBuilder();
            StringBuilder bloporiBody = new StringBuilder();
            StringBuilder bloprentBody = new StringBuilder();
            StringBuilder blopsumaBody = new StringBuilder();
            StringBuilder blopvatBody = new StringBuilder();
            StringBuilder blopCallBody = new StringBuilder();

            // WLR fields
            // Ex: 18/05 and the bill cycle name is WLR-71 hence 180571
            String cycleNo = getWlrCycleNo(ba.getBillingCycle());
            String majorAccntNo = ba.getCode() != null ? ba.getCode() : EMPTY_STRING;
            String sumseqno = "077";
            String flangind = "E";
            String custName = (ca != null && ca.getCustomer() != null) ? (ca.getCustomer().getDescription() != null ? ca.getCustomer().getDescription() : EMPTY_STRING)
                    : EMPTY_STRING;
            String lastBilAmt = StringUtils.isBlank(invoice.getCfValue("LAST_INVOICE_AMOUNT")) ? "0.00"
                    : NumberUtils.roundToString(new BigDecimal((Double) invoice.getCfValue("LAST_INVOICE_AMOUNT")), 2, RoundingModeEnum.NEAREST);

            BigDecimal balOs = new BigDecimal(lastBilAmt)
                    .add(invoice.getCfValue("PAYMENTS_ADJUSTMENTS") != null ? new BigDecimal((Double) invoice.getCfValue("PAYMENTS_ADJUSTMENTS")) : BigDecimal.ZERO);
            String sBalos = NumberUtils.roundToString(balOs, 2, RoundingModeEnum.NEAREST);
            String qamtTot = "0.00";
            String issueDate = DateUtils.formatDateWithPattern(invoice.getInvoiceDate(), DATE_FORMAT_YYYYMMDD);
            String dueDate = DateUtils.formatDateWithPattern(invoice.getDueDate(), DATE_FORMAT_YYYYMMDD);
            String fdistcod = "AAA";
            String totaldue = NumberUtils.roundToString(invoice.getNetToPay(), 2, RoundingModeEnum.NEAREST);
            BigDecimal totPaid = invoice.getCfValue("PAYMENTS_ADJUSTMENTS") != null ? new BigDecimal((Double) invoice.getCfValue("PAYMENTS_ADJUSTMENTS")) : BigDecimal.ZERO;
            String sTotPaid = NumberUtils.roundToString(totPaid, 2, RoundingModeEnum.NEAREST);
            String fblitdes = EMPTY_STRING;
            String message1 = EMPTY_STRING;
            String message2 = EMPTY_STRING;
            String addr1 = EMPTY_STRING;
            String addr2 = EMPTY_STRING;
            String addr3 = EMPTY_STRING;
            String addr4 = EMPTY_STRING;
            String addr5 = EMPTY_STRING;
            String socCode = EMPTY_STRING;
            String socDescription = EMPTY_STRING;
            String price = EMPTY_STRING;
            BigDecimal total = BigDecimal.ZERO;
            BigDecimal recurrRent = BigDecimal.ZERO;
            BigDecimal recurrEquip = BigDecimal.ZERO;
            BigDecimal otherCrDr = BigDecimal.ZERO;
            String totalBlopSuma = EMPTY_STRING;
            BigDecimal ftotord = BigDecimal.ZERO;
            BigDecimal frrcamt = BigDecimal.ZERO;
            String fcmpldte = EMPTY_STRING;
            String blopordCur = EMPTY_STRING;
            String blopordUan = EMPTY_STRING;
            String bloprentUan = EMPTY_STRING;
            String serviceAction = EMPTY_STRING;
            Map<String, BigDecimal> totalBlopCallByUan = new HashMap<String, BigDecimal>();


            if (ba != null && ba.getAddress() != null) {
                addr1 = ba.getAddress().getAddress1();
                addr2 = ba.getAddress().getAddress2();
                addr3 = ba.getAddress().getAddress3();
                addr4 = ba.getAddress().getCity();
                addr5 = ba.getAddress().getCountry() != null ? ba.getAddress().getCountry().getDescription() : EMPTY_STRING;
                if (addr5.contains(CSV_SEPARATOR)) {
                    addr5 = DOUBLE_QUOTES_CHARACTER + addr5 + DOUBLE_QUOTES_CHARACTER;
                }
            }

            String wlrLine;
            String wlrLineDuplicate;
            String bloprentLineBody = EMPTY_STRING;
            int bloprentLine = 3001;
            long noOfLines = 0;
            BigDecimal totalPerUan = BigDecimal.ZERO;
            BigDecimal vatCharge = BigDecimal.ZERO;
            String totalVat = EMPTY_STRING;
            String newUan = EMPTY_STRING;
            String newOrder = EMPTY_STRING;
            String newOpOrder = EMPTY_STRING;
            BigDecimal newVatCharge = null;
            //Date previousDate = DateUtils.addMonthsToDate(invoice.getInvoiceDate(), -1);
            RatedTransaction ratedTransaction;
            Map<String, Object> row = null;
            TaxClass taxClassB = null;
            TaxClass taxClassC = null;
            TaxClass taxClassD = null;
            BigDecimal bvatAmt = BigDecimal.ZERO;
            BigDecimal cvatAmt = BigDecimal.ZERO;
            BigDecimal dvatAmt = BigDecimal.ZERO;
            if (WLR_SERVICE_TYPE.equals(ebillFormat)) {
                taxClassB = taxClassService.findByCode("B");
                taxClassC = taxClassService.findByCode("C");
                taxClassD = taxClassService.findByCode("D");
            }

            do {
                row = (Map<String, Object>) ratedTransactions.get()[0];
                ratedTransaction = getRatedTransaction((Long) row.get("R_ID"), (String) row.get("R_CODE"), (String) row.get("R_DESCRIPTION"), (Date) row.get("R_START_DATE"),
                        (Date) row.get("R_END_DATE"), (Date) row.get("R_USAGE_DATE"), (BigDecimal) row.get("R_QUANTITY"), (BigDecimal) row.get("R_AMOUNT_WITH_TAX"), (BigDecimal) row.get("R_AMOUNT_WITHOUT_TAX"), (BigDecimal) row.get("R_RAW_AMOUNT_WITHOUT_TAX"),
                        (BigDecimal) row.get("R_TAX_PERCENT"), (BigDecimal) row.get("R_UNIT_AMOUNT_WITHOUT_TAX"), (String) row.get("R_ORDER_NUMBER"));
                subscription = getSubscription((Long) row.get("SUBSCRIPTION_ID"), (String) row.get("SUBSCRIPTION_CODE"), (CustomFieldValues) row.get("SUBSCRIPTION_CFVALUES"));
                serviceInstance = getServiceInstance((Long) row.get("SERVICE_INSTANCE_ID"),(String) row.get("SERVICE_INSTANCE_CODE"), (CustomFieldValues) row.get("SERVICE_INSTANCE_CFVALUES"));

                if (subscription != null && subscription.getCode() != null) {
                    uan = subscription.getCfValue(SubscriptionCFsEnum.UAN.name()) != null ? (String) subscription.getCfValue(SubscriptionCFsEnum.UAN.name()) : EMPTY_STRING;
                    subscriptionCode = getSubscriptionCode(subscription, invoice.getInvoiceDate(), ebillFormat);
                    if (WLR_SERVICE_TYPE.equals(ebillFormat)) {
                        if (subscription.getCfValue(SubscriptionCFsEnum.UAN.name(), invoice.getInvoiceDate()) != null) {
                            uan = (String) subscription.getCfValue(SubscriptionCFsEnum.UAN.name(), invoice.getInvoiceDate());
                        }
                    }

                    int firstAreaPos = subscriptionCode.contains("-") ? subscriptionCode.indexOf(UNDERSCORE) + 1 : 0;
                    int lastAreaPos = Integer.max(subscriptionCode.lastIndexOf("-"), subscriptionCode.lastIndexOf(UNDERSCORE));
                    area = (lastAreaPos > firstAreaPos) ? subscriptionCode.substring(firstAreaPos, lastAreaPos) : EMPTY_STRING;
                    telNumber = subscriptionCode.substring(lastAreaPos + 1);
                    agent = subscription.getCfValue(AGENT_CODE) != null ? (String) subscription.getCfValue(AGENT_CODE) : EMPTY_STRING;
                    subscriber = subscriptionCode.substring(firstAreaPos);
                    chargeReference = getChargeReference(subscription, invoice.getInvoiceDate());
                }
                transDate = ratedTransaction.getStartDate() != null ? DateUtils.formatDateWithPattern(ratedTransaction.getStartDate(), INVOICE_DATE_FORMAT)
                        : DateUtils.formatDateWithPattern(ratedTransaction.getUsageDate(), INVOICE_DATE_FORMAT);
                transDateTo = ratedTransaction.getEndDate() != null
                        ? DateUtils.formatDateWithPattern(DateUtils.addDaysToDate(ratedTransaction.getEndDate(), -1), INVOICE_DATE_FORMAT)
                        : EMPTY_STRING;
                forenfrm = ratedTransaction.getStartDate() != null ? DateUtils.formatDateWithPattern(ratedTransaction.getStartDate(), DATE_FORMAT_YYYYMMDD)
                        : DateUtils.formatDateWithPattern(ratedTransaction.getUsageDate(), DATE_FORMAT_YYYYMMDD);
                forento = ratedTransaction.getEndDate() != null ? DateUtils.formatDateWithPattern(DateUtils.addDaysToDate(ratedTransaction.getEndDate(), -1), DATE_FORMAT_YYYYMMDD) : EMPTY_STRING;
                qty = ratedTransaction.getQuantity() != null ? String.valueOf(ratedTransaction.getQuantity().abs().intValue()) : "0";
                chargeType = row.get("CHARGE_TYPE") != null ? ((String) row.get("CHARGE_TYPE")).replace("S", "O") : EMPTY_STRING;
                invoiceHeader = row.get("INVOICESUBCATEGORY_DESCRIPTION") != null ? (String) row.get("INVOICESUBCATEGORY_DESCRIPTION") : EMPTY_STRING;
                productDescription = ratedTransaction.getDescription() != null ? ratedTransaction.getDescription() : EMPTY_STRING;
                amount = NumberUtils.roundToString(ratedTransaction.getAmountWithoutTax(), 2, RoundingModeEnum.NEAREST);
                inclusiveDiscount = (ratedTransaction.getRawAmountWithoutTax() != null && ratedTransaction.getAmountWithoutTax() != null)
                        ? NumberUtils.roundToString(ratedTransaction.getRawAmountWithoutTax().add(ratedTransaction.getAmountWithoutTax().negate()), 2, RoundingModeEnum.NEAREST)
                        : "0.00";
                taxRate = NumberUtils.roundToString(ratedTransaction.getTaxPercent(), 2, RoundingModeEnum.NEAREST);
                fref = (StringUtils.isNotBlank(orderId) && orderId.equals(ratedTransaction.getOrderNumber())) ? (fref + 1) : 1;
                orderId = ratedTransaction.getOrderNumber() != null ? ratedTransaction.getOrderNumber() : EMPTY_STRING;
                operatorOrderNumber = getOperatorOrderNumber((String) row.get("O_ORDER_ITEMS"));
                fblitdes = serviceInstance != null ? serviceInstance.getCode() : EMPTY_STRING;
                socCode = fblitdes;// billCode
                seqno = serviceInstance != null && serviceInstance.getId() != null ? String.valueOf(serviceInstance.getId()) : EMPTY_STRING;

                if (invoiceHeader.contains(CSV_SEPARATOR)) {
                    invoiceHeader = DOUBLE_QUOTES_CHARACTER + invoiceHeader + DOUBLE_QUOTES_CHARACTER;
                }
                if (productDescription.contains(CSV_SEPARATOR)) {
                    productDescription = DOUBLE_QUOTES_CHARACTER + productDescription + DOUBLE_QUOTES_CHARACTER;
                }

                socDescription = productDescription;

                if (WLR_SERVICE_TYPE.equals(ebillFormat)) {
                    if (row.get("EDR_EVENT_DATE") != null) {
                        String originatingNumber = (String) row.get("EDR_PARAMETER6");
                        String std = EMPTY_STRING;
                        String telephone = EMPTY_STRING;
                        if (StringUtils.isNotBlank(originatingNumber)) {
                            if (originatingNumber.length() >= 2 && STD_MAP.get(originatingNumber.substring(0, 2)) != null) {
                                std = STD_MAP.get(originatingNumber.substring(0, 2));
                            } else if (originatingNumber.length() >= 3 && STD_MAP.get(originatingNumber.substring(0, 3)) != null) {
                                std = STD_MAP.get(originatingNumber.substring(0, 3));
                            } else if (originatingNumber.length() >= 4 && STD_MAP.get(originatingNumber.substring(0, 4)) != null) {
                                std = STD_MAP.get(originatingNumber.substring(0, 4));
                            }
                            telephone = originatingNumber.substring(std.length());
                        }

                        String callType = row.get("INVOICESUBCATEGORY_CODE") != null ? (String) row.get("INVOICESUBCATEGORY_CODE") : EMPTY_STRING;
                        String callTypeDes = row.get("INVOICESUBCATEGORY_DESCRIPTION") != null ? (String) row.get("INVOICESUBCATEGORY_DESCRIPTION") : EMPTY_STRING;
                        String rate = EMPTY_STRING;
                        String blopCallDate = DateUtils.formatDateWithPattern((Date) row.get("EDR_EVENT_DATE"), "yyyyMMdd");
                        String blopCallTime = DateUtils.formatDateWithPattern((Date) row.get("EDR_EVENT_DATE"), "HHmmss");
                        String terminatingNumber = row.get("EDR_PARAMETER1") != null ? (String) row.get("EDR_PARAMETER1") : EMPTY_STRING;
                        //String destination = ratedTransactiondto.getEdrExtraParameter() != null ? ratedTransactiondto.getEdrExtraParameter() : EMPTY_STRING;
                        String duration = row.get("EDR_QUANTITY") != null ? NumberUtils.roundToString(((BigDecimal) row.get("EDR_QUANTITY")).divide(new BigDecimal(1000)), 2, RoundingModeEnum.NEAREST)
                                : EMPTY_STRING;
                        String blopCallTotal = EMPTY_STRING;
                        String destination = EMPTY_STRING;

                        wo = getWalletOperation((CustomFieldValues) row.get("WO_CFVALUES"));
                        CustomFieldValue lastCfValue = utilService.getLastCfVersion(wo, "CALL_INFORMATION", invoice.getInvoiceDate(), true);
                        if (lastCfValue != null && lastCfValue.getMapValue() != null && !lastCfValue.getMapValue().isEmpty()) {
                            Map<String, String> callInformationCF = (Map<String, String>) utilService.getCFValue(wo, "CALL_INFORMATION",
                                    (String) lastCfValue.getMapValue().values().iterator().next());
                            if (callInformationCF != null && !callInformationCF.isEmpty()) {
                                destination = callInformationCF.get("traffic_class") != null ? callInformationCF.get("traffic_class") : EMPTY_STRING;
                                rate = callInformationCF.get("time_period_A") != null ? callInformationCF.get("time_period_A") : EMPTY_STRING;
                                blopCallTotal = callInformationCF.get("Time period A Amount") != null
                                        ? NumberUtils.roundToString(new BigDecimal(String.valueOf(callInformationCF.get("Time period A Amount"))), 2, RoundingModeEnum.NEAREST)
                                        : EMPTY_STRING;
                                if (totalBlopCallByUan.get(uan) != null) {
                                    totalBlopCallByUan.put(uan, totalBlopCallByUan.get(uan).add(new BigDecimal(String.valueOf(callInformationCF.get("Time period A Amount")))));
                                } else {
                                    totalBlopCallByUan.put(uan, new BigDecimal(String.valueOf(callInformationCF.get("Time period A Amount"))));
                                }
                            }
                        }

                        blopCallBody = blopCallBody.append("999," + ba.getCode() + CSV_SEPARATOR + uan + ",999," + std + CSV_SEPARATOR + telephone + CSV_SEPARATOR + callType + CSV_SEPARATOR
                                + callTypeDes + CSV_SEPARATOR + rate + CSV_SEPARATOR + blopCallDate + CSV_SEPARATOR + blopCallTime + CSV_SEPARATOR + terminatingNumber
                                + CSV_SEPARATOR + EMPTY_STRING + CSV_SEPARATOR + destination + CSV_SEPARATOR + EMPTY_STRING + CSV_SEPARATOR + duration + ",,,,,," + blopCallTotal
                                + "\r\n");
                    }

                    //3- blopHead
                    if (taxClassB != null && taxClassB.getId() == (Long) row.get("TAX_CLASS_ID")) {
                        bvatAmt = ratedTransaction.getAmountWithTax() != null ? bvatAmt.add(ratedTransaction.getAmountWithTax()) : bvatAmt;
                        bvatAmt = ratedTransaction.getAmountWithoutTax() != null ? bvatAmt.subtract(ratedTransaction.getAmountWithoutTax()) : bvatAmt;
                    }
                    if (taxClassC != null && taxClassC.getId() == (Long) row.get("TAX_CLASS_ID")) {
                        cvatAmt = ratedTransaction.getAmountWithTax() != null ? cvatAmt.add(ratedTransaction.getAmountWithTax()) : cvatAmt;
                        cvatAmt = ratedTransaction.getAmountWithoutTax() != null ? cvatAmt.subtract(ratedTransaction.getAmountWithoutTax()) : cvatAmt;
                    }
                    if (taxClassD != null && taxClassD.getId() == (Long) row.get("TAX_CLASS_ID")) {
                        dvatAmt = ratedTransaction.getAmountWithTax() != null ? dvatAmt.add(ratedTransaction.getAmountWithTax()) : dvatAmt;
                        dvatAmt = ratedTransaction.getAmountWithoutTax() != null ? dvatAmt.subtract(ratedTransaction.getAmountWithoutTax()) : dvatAmt;
                    }

                    // 4- blopmajBody
                    wlrLine = ba.getCode() + CSV_SEPARATOR + uan + CSV_SEPARATOR + area + CSV_SEPARATOR + telNumber + ",AAA";
                    if (blopmajBody.indexOf(wlrLine) == -1) {
                        blopmajBody = blopmajBody.append(wlrLine + "\r\n");
                    }

                    // 5- blopocdBody
                    if (isMiscOffer((String) row.get("OFFER_CODE"))) {
                        wlrLine = cycleNo + CSV_SEPARATOR + ba.getCode() + CSV_SEPARATOR + uan + CSV_SEPARATOR + "AAA,999,OC," + productDescription + CSV_SEPARATOR + amount
                                + CSV_SEPARATOR + area + CSV_SEPARATOR + telNumber + ",0,,E," + currency + CSV_SEPARATOR + forenfrm + CSV_SEPARATOR + forento + CSV_SEPARATOR + qty + CSV_SEPARATOR + chargeReference;
                        blopocdBody = blopocdBody.append(wlrLine + "\r\n");
                    }

                    // 6- blopochBody
                    if (isBlopoch(invoice, ba, ratedTransaction)) {
                        wlrLine = cycleNo + CSV_SEPARATOR + ba.getCode() + CSV_SEPARATOR + uan + CSV_SEPARATOR + "AAA,999," + orderId + CSV_SEPARATOR + fref + CSV_SEPARATOR
                                + productDescription + CSV_SEPARATOR + amount + CSV_SEPARATOR + currency + CSV_SEPARATOR + forenfrm + CSV_SEPARATOR + forento;
                        blopochBody = blopochBody.append(wlrLine + "\r\n");
                    }

                    // 7- blopordBody                    
                    if (newOrder != null && newOrder.equals(orderId)) {
                        if (ratedTransaction.getAmountWithoutTax() != null) {
                            ftotord = ftotord.add(ratedTransaction.getAmountWithoutTax());
                            if (ratedTransaction.getCode().endsWith("_RC")) {
                                frrcamt = frrcamt.add(ratedTransaction.getAmountWithoutTax());
                            }
                        }
                    } else {
                        if (StringUtils.isNotBlank(newOrder)) {
                            String sftotord = NumberUtils.roundToString(ftotord, 2, RoundingModeEnum.NEAREST);
                            String sfrrcamt = NumberUtils.roundToString(frrcamt, 2, RoundingModeEnum.NEAREST);
                            String frrcdes = EMPTY_STRING;
                            if (serviceAction.toUpperCase().contains("CHANGE")) {
                                frrcdes = (ftotord != null && ftotord.compareTo(BigDecimal.ZERO) < 0) ? "DECREASED" : "INCREASED";
                            }
                            wlrLine = cycleNo + CSV_SEPARATOR + ba.getCode() + CSV_SEPARATOR + blopordUan + CSV_SEPARATOR + "AAA,999," + newOrder + CSV_SEPARATOR + fcmpldte
                                    + CSV_SEPARATOR + newOpOrder + CSV_SEPARATOR + sftotord + ",,," + serviceAction + ",,,,,," + frrcdes + CSV_SEPARATOR + sfrrcamt + ",,,,,"
                                    + currency;
                            if (blopordBody.indexOf(wlrLine) == -1) {
                                blopordBody = blopordBody.append(wlrLine + "\r\n");
                            }
                        }
                        if (StringUtils.isNotBlank(orderId)) {
                            ftotord = BigDecimal.ZERO;
                            frrcamt = BigDecimal.ZERO;
                            if (ratedTransaction.getAmountWithoutTax() != null) {
                                ftotord = ftotord.add(ratedTransaction.getAmountWithoutTax());
                                if (ratedTransaction.getCode().endsWith("_RC")) {
                                    frrcamt = frrcamt.add(ratedTransaction.getAmountWithoutTax());
                                }
                            }

                            serviceAction = getServiceAction(serviceInstance, "BLOPORD", (String) row.get("O_ORDER_ITEMS"));
                            fcmpldte = forenfrm;
                            blopordUan = uan;
                        }
                        newOrder = orderId;
                        newOpOrder = operatorOrderNumber;

                    }


                    // 8- bloporiBody
                    wlrLine = cycleNo + CSV_SEPARATOR + ba.getCode() + CSV_SEPARATOR + uan + CSV_SEPARATOR + "AAA,999," + orderId + CSV_SEPARATOR + area + CSV_SEPARATOR
                            + telNumber + CSV_SEPARATOR + fblitdes + CSV_SEPARATOR + qty + CSV_SEPARATOR + fref + CSV_SEPARATOR + getServiceAction(serviceInstance, "BLOPORI", (String) row.get("O_ORDER_ITEMS")) + CSV_SEPARATOR
                            + currency + "\r\n";
                    wlrLineDuplicate = cycleNo + CSV_SEPARATOR + ba.getCode() + CSV_SEPARATOR + uan + CSV_SEPARATOR + "AAA,999," + orderId + CSV_SEPARATOR + area + CSV_SEPARATOR
                            + telNumber + CSV_SEPARATOR + fblitdes + CSV_SEPARATOR + qty + CSV_SEPARATOR + getServiceAction(serviceInstance, "BLOPORI", (String) row.get("O_ORDER_ITEMS")) + CSV_SEPARATOR
                            + currency + "\r\n";

                    if (bloporiBody.indexOf(wlrLineDuplicate) == -1) {
                        bloporiBody = bloporiBody.append(wlrLine);
                    }

                    // 10- bloprentBody
                    if (StringUtils.isNotBlank(uan) && ratedTransaction.getCode().endsWith("_RC") && isRtInFullPeriod(ratedTransaction)) {


                        if (uan != null && bloprentLineBody.contains(uan)) {
                            if (ratedTransaction.getAmountWithoutTax() != null) {
                                total = total.add(ratedTransaction.getAmountWithoutTax());
                            }
                        } else {
                            if (StringUtils.isNotBlank(bloprentUan)) {
                                String stotal = NumberUtils.roundToString(total, 2, RoundingModeEnum.NEAREST);
                                wlrLine = bloprentLine + CSV_SEPARATOR + ba.getCode() + CSV_SEPARATOR + bloprentUan + CSV_SEPARATOR + "999,,,RENTALS,,,,,,RENTALS," + stotal + "\r\n";
                                bloprentBody = bloprentBody.append(wlrLine + bloprentLineBody);
                                bloprentLineBody = EMPTY_STRING;
                                bloprentLine = 3001;
                            }
                        }
                        if (StringUtils.isNotBlank(uan)) {
                            if(bloprentLine == 3001) {
                                total = ratedTransaction.getAmountWithoutTax() != null ? ratedTransaction.getAmountWithoutTax() : BigDecimal.ZERO;
                            }
                            bloprentUan = uan;
                            price = NumberUtils.roundToString(ratedTransaction.getUnitAmountWithoutTax(), 2, RoundingModeEnum.NEAREST);
                            bloprentLineBody += bloprentLine + CSV_SEPARATOR + ba.getCode() + CSV_SEPARATOR + uan + CSV_SEPARATOR + "999" + CSV_SEPARATOR + area + CSV_SEPARATOR + telNumber
                                    + ",Rentals," + socCode + CSV_SEPARATOR + socDescription + CSV_SEPARATOR + forenfrm + CSV_SEPARATOR + forento + CSV_SEPARATOR + qty + CSV_SEPARATOR
                                    + price + CSV_SEPARATOR + amount + "\r\n";
                            bloprentLine++;
                        }


                    }

                    // 12- blopsumaBody
                    if (newUan != null && newUan.equals(uan)) {
                        if (ratedTransaction.getCode().endsWith("_RC")) {
                            if (ratedTransaction.getStartDate() != null && (rentalFrom == null || ratedTransaction.getStartDate().before(rentalFrom))) {
                                rentalFrom = ratedTransaction.getStartDate();
                            }
                            if (ratedTransaction.getEndDate() != null && (rentalTo == null || ratedTransaction.getEndDate().after(rentalTo))) {
                                rentalTo = DateUtils.addDaysToDate(ratedTransaction.getEndDate(), -1);
                            }

                            if (ratedTransaction.getAmountWithoutTax() != null) {
                                if ("Main".equals(row.get("SERVICE_TYPE")) && isRtInFullPeriod(ratedTransaction)) {
                                    recurrRent = recurrRent.add(ratedTransaction.getAmountWithoutTax());
                                } else if (! "Main".equals(row.get("SERVICE_TYPE"))) {
                                    recurrEquip = recurrEquip.add(ratedTransaction.getAmountWithoutTax());
                                }
                            }
                        }
                        if (ratedTransaction.getAmountWithoutTax() != null && (isMiscOffer((String) row.get("OFFER_CODE")) || isBlopoch(invoice, ba, ratedTransaction))) {
                            otherCrDr = otherCrDr.add(ratedTransaction.getAmountWithoutTax());
                        }
                        if (ratedTransaction.getCode().contains("USAGE")) {
                            if (ratedTransaction.getUsageDate() != null) {
                                if (callsFrom == null || ratedTransaction.getUsageDate().before(callsFrom)) {
                                    callsFrom = ratedTransaction.getUsageDate();
                                }
                                if (callsTo == null || ratedTransaction.getUsageDate().after(callsTo)) {
                                    callsTo = DateUtils.addDaysToDate(ratedTransaction.getUsageDate(), -1);
                                }
                            }

                        }
                    } else {
                        if (StringUtils.isNotBlank(newUan)) {
                            if (totalBlopCallByUan.get(newUan) != null) {
                                totalBlopSuma = NumberUtils.roundToString(recurrRent.add(recurrEquip).add(otherCrDr).add(totalBlopCallByUan.get(newUan)), 2, RoundingModeEnum.NEAREST);
                            } else {
                                totalBlopSuma = NumberUtils.roundToString(recurrRent.add(recurrEquip).add(otherCrDr), 2, RoundingModeEnum.NEAREST);
                            }

                            if (callsTo != null && callsFrom != null && callsTo.before(callsFrom)) {
                                callsTo = callsFrom;
                            }
                            noOfLines = subscriptionService.countActiveSubscriptionsByUAN(newUan);
                            wlrLine = cycleNo + CSV_SEPARATOR + ba.getCode() + CSV_SEPARATOR + newUan + CSV_SEPARATOR + "AAA,999," + String.format("%05d", noOfLines) + CSV_SEPARATOR + issueDate + CSV_SEPARATOR
                                    + DateUtils.formatDateWithPattern(rentalFrom, DATE_FORMAT_YYYYMMDD) + CSV_SEPARATOR + DateUtils.formatDateWithPattern(rentalTo, DATE_FORMAT_YYYYMMDD) + CSV_SEPARATOR +
                                    DateUtils.formatDateWithPattern(callsFrom, DATE_FORMAT_YYYYMMDD) + CSV_SEPARATOR + DateUtils.formatDateWithPattern(callsTo, DATE_FORMAT_YYYYMMDD) + CSV_SEPARATOR +
                                    NumberUtils.roundToString(recurrRent, 2, RoundingModeEnum.NEAREST) + CSV_SEPARATOR
                                    + NumberUtils.roundToString(recurrEquip, 2, RoundingModeEnum.NEAREST) + ",0.00,0.00," + NumberUtils.roundToString(totalBlopCallByUan.get(uan), 2, RoundingModeEnum.NEAREST) + CSV_SEPARATOR
                                    + NumberUtils.roundToString(otherCrDr, 2, RoundingModeEnum.NEAREST) + ",0.00," + totalBlopSuma + CSV_SEPARATOR + currency + ",0.00,0.00,,,,";

                            if (blopsumaBody.indexOf(wlrLine) == -1) {
                                blopsumaBody = blopsumaBody.append(wlrLine + "\r\n");
                            }
                        }

                        rentalFrom = null;
                        rentalTo = null;
                        callsFrom = null;
                        callsTo = null;
                        recurrRent = BigDecimal.ZERO;
                        recurrEquip = BigDecimal.ZERO;
                        otherCrDr = BigDecimal.ZERO;
                        if (ratedTransaction.getCode().endsWith("_RC")) {
                            rentalFrom = ratedTransaction.getStartDate();
                            rentalTo = DateUtils.addDaysToDate(ratedTransaction.getEndDate(), -1);

                            if ("Main".equals(row.get("SERVICE_TYPE")) && isRtInFullPeriod(ratedTransaction)) {
                                recurrRent = ratedTransaction.getAmountWithoutTax();
                            } else if(!"Main".equals(row.get("SERVICE_TYPE"))) {
                                recurrEquip = ratedTransaction.getAmountWithoutTax();
                            }
                        }
                        if (isMiscOffer((String) row.get("OFFER_CODE"))) {
                            otherCrDr = ratedTransaction.getAmountWithoutTax();
                        }
                        if (ratedTransaction.getCode().contains("USAGE")) {
                            if (ratedTransaction.getUsageDate() != null) {
                                callsFrom = ratedTransaction.getUsageDate();
                                callsTo = callsFrom; // if we have only one transaction
                            }

                        }
                    }

                    // 13- blopvatBody
                    vatCharge = ratedTransaction.getTaxPercent();
                    if (StringUtils.isNotBlank(newUan) && newVatCharge != null && (!newUan.equals(uan) || vatCharge.compareTo(newVatCharge) != 0)) {
                        totalVat = NumberUtils.roundToString((new BigDecimal(0.01)).multiply(totalPerUan).multiply(newVatCharge), 2, RoundingModeEnum.NEAREST);
                        wlrLine = cycleNo + CSV_SEPARATOR + ba.getCode() + CSV_SEPARATOR + newUan + CSV_SEPARATOR + "AAA,999," + NumberUtils.roundToString(totalPerUan, 2, RoundingModeEnum.NEAREST) + CSV_SEPARATOR + NumberUtils.roundToString(newVatCharge, 2, RoundingModeEnum.NEAREST)
                                + CSV_SEPARATOR + totalVat + CSV_SEPARATOR + currency;

                        if (blopvatBody.indexOf(wlrLine) == -1) {
                            blopvatBody = blopvatBody.append(wlrLine + "\r\n");
                        }                        
                        totalPerUan = BigDecimal.ZERO;
                        if (ratedTransaction.getAmountWithoutTax() != null) {
                            totalPerUan = ratedTransaction.getAmountWithoutTax();
                        }

                    } else {
                        if (ratedTransaction.getAmountWithoutTax() != null) {
                            totalPerUan = totalPerUan.add(ratedTransaction.getAmountWithoutTax());
                        }
                    }

                    if (uan != null && !uan.equals(newUan)) {
                        newUan = uan;
                    }
                    if(vatCharge != null && (newVatCharge == null || vatCharge.compareTo(newVatCharge) != 0)) {
                        newVatCharge = vatCharge;
                    }
                } else if (NGN_SERVICE_TYPE.equals(ebillFormat) || PPC_ICP_INT_LL_SERVICE_TYPE.equals(ebillFormat)) {
                    bw.newLine();
                    serviceParametersCF = getServiceParameters(serviceInstance, invoice.getInvoiceDate());
                    distance = (serviceParametersCF != null && !serviceParametersCF.isEmpty() && serviceParametersCF.get(ServiceParametersCFEnum.DISTANCE.getLabel()) != null)
                            ? serviceParametersCF.get(ServiceParametersCFEnum.DISTANCE.getLabel())
                            : EMPTY_STRING;
                    if (subscription != null && subscriptionCode != null) {
                        circuitType = EMPTY_STRING;
                        for (int i = 0; i < subscriptionCode.length(); i++) {
                            if (!Character.isAlphabetic(subscriptionCode.charAt(i))) {
                                break;
                            }
                            circuitType += subscriptionCode.charAt(i);
                        }

                        circuitNumber = subscriptionCode;

                        if (subscription != null && subscriptionMap.containsKey(subscription.getId())) {
                            aEndName = subscriptionMap.get(subscription.getId())[3] != null ? subscriptionMap.get(subscription.getId())[3] : EMPTY_STRING;
                            aEndAddress = subscriptionMap.get(subscription.getId())[4] != null
                                    ? (DOUBLE_QUOTES_CHARACTER + subscriptionMap.get(subscription.getId())[4] + DOUBLE_QUOTES_CHARACTER)
                                    : EMPTY_STRING;
                            bEndName = subscriptionMap.get(subscription.getId())[5] != null ? subscriptionMap.get(subscription.getId())[5] : EMPTY_STRING;
                            bEndAddress = subscriptionMap.get(subscription.getId())[6] != null
                                    ? (DOUBLE_QUOTES_CHARACTER + subscriptionMap.get(subscription.getId())[6] + DOUBLE_QUOTES_CHARACTER)
                                    : EMPTY_STRING;
                            aAssociation = subscriptionMap.get(subscription.getId())[0] != null ? subscriptionMap.get(subscription.getId())[0] : EMPTY_STRING;
                            if (aEndName.contains(CSV_SEPARATOR)) {
                                aEndName = DOUBLE_QUOTES_CHARACTER + aEndName + DOUBLE_QUOTES_CHARACTER;
                            }
                            if (bEndName.contains(CSV_SEPARATOR)) {
                                bEndName = DOUBLE_QUOTES_CHARACTER + bEndName + DOUBLE_QUOTES_CHARACTER;
                            }

                            if (NGN_SERVICE_TYPE.equals(ebillFormat)) {
                                bAssociation = subscriptionMap.get(subscription.getId())[1] != null ? subscriptionMap.get(subscription.getId())[1] : EMPTY_STRING;
                                cAssociation = subscriptionMap.get(subscription.getId())[2] != null ? subscriptionMap.get(subscription.getId())[2] : EMPTY_STRING;
                            }
                        }
                    }
                    bw.write("1," + customerNumber + CSV_SEPARATOR + contratName + CSV_SEPARATOR + account + CSV_SEPARATOR + billNumber + CSV_SEPARATOR + billDate + CSV_SEPARATOR
                            + circuitType + CSV_SEPARATOR + circuitNumber + CSV_SEPARATOR + transDate + CSV_SEPARATOR + transDateTo + CSV_SEPARATOR + qty + CSV_SEPARATOR
                            + chargeType + CSV_SEPARATOR + invoiceHeader + CSV_SEPARATOR + productDescription + CSV_SEPARATOR + amount + CSV_SEPARATOR + inclusiveDiscount
                            + CSV_SEPARATOR + taxRate + CSV_SEPARATOR + currency + CSV_SEPARATOR + orderId + CSV_SEPARATOR + operatorOrderNumber + CSV_SEPARATOR + chargeReference + CSV_SEPARATOR
                            + seqno + CSV_SEPARATOR + aEndName + CSV_SEPARATOR + aEndAddress + CSV_SEPARATOR + bEndName + CSV_SEPARATOR
                            + bEndAddress + CSV_SEPARATOR + distance + CSV_SEPARATOR + aAssociation);
                    if (NGN_SERVICE_TYPE.equals(ebillFormat)) {
                        bw.write(CSV_SEPARATOR + bAssociation + CSV_SEPARATOR + cAssociation);
                    }
                } else {
                    bw.newLine();
                    bw.write("1," + customerNumber + CSV_SEPARATOR + contratName + CSV_SEPARATOR + account + CSV_SEPARATOR + billNumber + CSV_SEPARATOR + billDate + CSV_SEPARATOR
                            + area + CSV_SEPARATOR + telNumber + CSV_SEPARATOR + transDate + CSV_SEPARATOR + transDateTo + CSV_SEPARATOR + qty + CSV_SEPARATOR + chargeType
                            + CSV_SEPARATOR + invoiceHeader + CSV_SEPARATOR + productDescription + CSV_SEPARATOR + amount + CSV_SEPARATOR + inclusiveDiscount + CSV_SEPARATOR
                            + taxRate + CSV_SEPARATOR + currency + CSV_SEPARATOR + orderId + CSV_SEPARATOR + operatorOrderNumber + CSV_SEPARATOR + chargeReference);

                    if (!NON_TRAFFIC_SERVICE_TYPE.equals(ebillFormat)) {
                        bw.write(CSV_SEPARATOR + agent + CSV_SEPARATOR + seqno + CSV_SEPARATOR + subscriber + CSV_SEPARATOR + uan);
                    } else {
                        bw.write(CSV_SEPARATOR + seqno);
                    }

                }
            } while (ratedTransactions.next());

            if (WLR_SERVICE_TYPE.equals(ebillFormat)) {
                bw.newLine();
                // 1-BLOPCALL
                bw.write(START_BLOPCALL);
                bw.newLine();
                bw.write(blopCallBody.toString());
                bw.write(END_BLOPCALL);

                // 2-BLOPEXP
                bw.newLine();
                bw.write(BLOPEXP_HEADER);
                bw.newLine();
                // 3-BLOPHEAD
                bw.write(START_BLOPHEAD);
                bw.newLine();

                bw.write(cycleNo + CSV_SEPARATOR + majorAccntNo + CSV_SEPARATOR + sumseqno + CSV_SEPARATOR + flangind + CSV_SEPARATOR + custName + CSV_SEPARATOR + lastBilAmt
                        + CSV_SEPARATOR + sBalos + CSV_SEPARATOR + qamtTot + CSV_SEPARATOR + issueDate + CSV_SEPARATOR + dueDate + CSV_SEPARATOR + fdistcod + CSV_SEPARATOR
                        + totaldue + CSV_SEPARATOR + sTotPaid + CSV_SEPARATOR + message1 + CSV_SEPARATOR + message2 + CSV_SEPARATOR + addr1 + CSV_SEPARATOR + addr2 + CSV_SEPARATOR
                        + addr3 + CSV_SEPARATOR + addr4 + CSV_SEPARATOR + addr5 + CSV_SEPARATOR + "0,0,0,0,0,0,0,0,0,0,0,0.00,"
                        + NumberUtils.roundToString(bvatAmt, 2, RoundingModeEnum.NEAREST) + CSV_SEPARATOR + NumberUtils.roundToString(cvatAmt, 2, RoundingModeEnum.NEAREST)
                        + CSV_SEPARATOR + NumberUtils.roundToString(dvatAmt, 2, RoundingModeEnum.NEAREST) + CSV_SEPARATOR + currency);
                bw.newLine();
                bw.write(END_BLOPHEAD);
                // **********4-BLOPMAJ********************
                bw.newLine();
                bw.write(START_BLOPMAJ);
                bw.newLine();
                bw.write(blopmajBody.toString());
                bw.write(END_BLOPMAJ);
                // *********************************************
                // **********5-BLOPOCD***************************
                bw.newLine();
                bw.write(START_BLOPOCD);
                bw.newLine();
                bw.write(blopocdBody.toString());
                bw.write(END_BLOPOCD);
                // ********************************************
                // *************6-BLOPOCH**********************
                bw.newLine();
                bw.write(START_BLOPOCH);
                bw.newLine();
                bw.write(blopochBody.toString());
                bw.write(END_BLOPOCH);
                // ******************************************
                // *************7-BLOPORD**********************
                bw.newLine();
                bw.write(START_BLOPORD);
                bw.newLine();
                if (StringUtils.isNotBlank(newOrder)) {
                    String sftotord = NumberUtils.roundToString(ftotord, 2, RoundingModeEnum.NEAREST);
                    String sfrrcamt = NumberUtils.roundToString(frrcamt, 2, RoundingModeEnum.NEAREST);
                    String frrcdes = EMPTY_STRING;
                    if ("CHANGE".equals(serviceAction)) {
                        frrcdes = (ftotord != null && ftotord.compareTo(BigDecimal.ZERO) < 0) ? "DECREASED" : "INCREASED";
                    }
                    wlrLine = cycleNo + CSV_SEPARATOR + ba.getCode() + CSV_SEPARATOR + blopordUan + CSV_SEPARATOR + "AAA,999," + newOrder + CSV_SEPARATOR + fcmpldte
                            + CSV_SEPARATOR + operatorOrderNumber + CSV_SEPARATOR + sftotord + ",,," + serviceAction + ",,,,,," + frrcdes + CSV_SEPARATOR + sfrrcamt + ",,,,,"
                            + blopordCur;
                    if (blopordBody.indexOf(wlrLine) == -1) {
                        blopordBody = blopordBody.append(wlrLine + "\r\n");
                    }
                }
                bw.write(blopordBody.toString());
                bw.write(END_BLOPORD);
                // ******************************************
                // *************8-BLOPORI**********************
                bw.newLine();
                bw.write(START_BLOPORI);
                bw.newLine();
                bw.write(bloporiBody.toString());
                bw.write(END_BLOPORI);
                // ******************************************
                // *************9-BLOPPTD**********************
                bw.newLine();
                bw.write(BLOPPTD_HEADER);
                // ******************************************
                // *************10-BLOPRENT**********************
                bw.newLine();
                bw.write(START_BLOPRENT);
                bw.newLine();
                if (StringUtils.isNotBlank(bloprentLineBody)) {
                    String stotal = NumberUtils.roundToString(total, 2, RoundingModeEnum.NEAREST);
                    wlrLine = bloprentLine + CSV_SEPARATOR + ba.getCode() + CSV_SEPARATOR + bloprentUan + CSV_SEPARATOR + "999,,,RENTALS,,,,,,RENTALS," + stotal + "\r\n";
                    bloprentBody = bloprentBody.append(wlrLine + bloprentLineBody);
                }
                bw.write(bloprentBody.toString());
                bw.write(END_BLOPRENT);
                // ******************************************
                // *************11-BLOPSAV **********************
                bw.newLine();
                bw.write(BLOPSAV_HEADER);
                // ******************************************
                // *************12-BLOPSUMA**********************
                bw.newLine();
                bw.write(START_BLOPSUMA);
                bw.newLine();
                if (StringUtils.isNotBlank(newUan)) {
                    if (totalBlopCallByUan.get(newUan) != null) {
                        totalBlopSuma = NumberUtils.roundToString(recurrRent.add(recurrEquip).add(otherCrDr).add(totalBlopCallByUan.get(newUan)), 2, RoundingModeEnum.NEAREST);
                    } else {
                        totalBlopSuma = NumberUtils.roundToString(recurrRent.add(recurrEquip).add(otherCrDr), 2, RoundingModeEnum.NEAREST);
                    }

                    if (callsTo != null && callsFrom != null && callsTo.before(callsFrom)) {
                        callsTo = callsFrom;
                    }
                    noOfLines = subscriptionService.countActiveSubscriptionsByUAN(newUan);
                    wlrLine = cycleNo + CSV_SEPARATOR + ba.getCode() + CSV_SEPARATOR + newUan + CSV_SEPARATOR + "AAA,999," + String.format("%05d", noOfLines) + CSV_SEPARATOR + issueDate + CSV_SEPARATOR
                            + DateUtils.formatDateWithPattern(rentalFrom, DATE_FORMAT_YYYYMMDD) + CSV_SEPARATOR + DateUtils.formatDateWithPattern(rentalTo, DATE_FORMAT_YYYYMMDD) + CSV_SEPARATOR +
                            DateUtils.formatDateWithPattern(callsFrom, DATE_FORMAT_YYYYMMDD) + CSV_SEPARATOR + DateUtils.formatDateWithPattern(callsTo, DATE_FORMAT_YYYYMMDD) + CSV_SEPARATOR +
                            NumberUtils.roundToString(recurrRent, 2, RoundingModeEnum.NEAREST) + CSV_SEPARATOR
                            + NumberUtils.roundToString(recurrEquip, 2, RoundingModeEnum.NEAREST) + ",0.00,0.00," + NumberUtils.roundToString(totalBlopCallByUan.get(newUan), 2, RoundingModeEnum.NEAREST) + CSV_SEPARATOR
                            + NumberUtils.roundToString(otherCrDr, 2, RoundingModeEnum.NEAREST) + ",0.00," + totalBlopSuma + CSV_SEPARATOR + currency + ",0.00,0.00,,,,";

                    if (blopsumaBody.indexOf(wlrLine) == -1) {
                        blopsumaBody = blopsumaBody.append(wlrLine + "\r\n");
                    }
                }

                bw.write(blopsumaBody.toString());
                bw.write(END_BLOPSUMA);
                // ******************************************
                // *************13-BLOPVAT**********************
                bw.newLine();
                bw.write(START_BLOPVAT);
                bw.newLine();
                if (StringUtils.isNotBlank(newUan) && newVatCharge != null) {
                    totalVat = NumberUtils.roundToString((new BigDecimal(0.01)).multiply(totalPerUan).multiply(newVatCharge), 2, RoundingModeEnum.NEAREST);
                    wlrLine = cycleNo + CSV_SEPARATOR + ba.getCode() + CSV_SEPARATOR + newUan + CSV_SEPARATOR + "AAA,999,"
                            + NumberUtils.roundToString(totalPerUan, 2, RoundingModeEnum.NEAREST) + CSV_SEPARATOR
                            + NumberUtils.roundToString(newVatCharge, 2, RoundingModeEnum.NEAREST) + CSV_SEPARATOR + totalVat + CSV_SEPARATOR + currency;

                    if (blopvatBody.indexOf(wlrLine) == -1) {
                        blopvatBody = blopvatBody.append(wlrLine + "\r\n");
                    }
                }
                bw.write(blopvatBody.toString());
                bw.write(END_BLOPVAT);
                // ******************************************

            }
        }
    }

    private boolean isBlopoch(Invoice invoice, BillingAccount ba, RatedTransaction ratedTransaction) {
        return ratedTransaction.getStartDate() != null && ba.getNextInvoiceDate() != null && ratedTransaction.getStartDate().before(ba.getNextInvoiceDate())
                && !isRtInFullPeriod(ratedTransaction);
    }

    /**
     * Gets the subscription code.
     *
     * @param subscription the subscription
     * @param invoiceDate  the invoice date
     * @param ebillFormat  the ebill format
     * @return the subscription code
     */
    private String getSubscriptionCode(Subscription subscription, Date invoiceDate, String ebillFormat) {
        String subscriptionCode = subscription.getCode().startsWith("OLD") ? subscription.getCode().substring(23) : subscription.getCode();

        if (WLR_SERVICE_TYPE.equals(ebillFormat)) {
            //exclude Swap and Change Number requests made after the bill period but before eBill generation
            String changeTelCf = subscription.getCfValue(SubscriptionCFsEnum.CHANGE_TEL.name()) != null ? subscription.getCfValue(SubscriptionCFsEnum.CHANGE_TEL.name()).toString() : null;
            String changeTelCfEbill = subscription.getCfValue(SubscriptionCFsEnum.CHANGE_TEL.name(), invoiceDate) != null ? subscription.getCfValue(SubscriptionCFsEnum.CHANGE_TEL.name(), invoiceDate).toString() : null;
            String swapTelCf = subscription.getCfValue(SubscriptionCFsEnum.SWAP_TEL.name()) != null ? subscription.getCfValue(SubscriptionCFsEnum.SWAP_TEL.name()).toString() : null;
            String swapTelCfEbill = subscription.getCfValue(SubscriptionCFsEnum.SWAP_TEL.name(), invoiceDate) != null ? subscription.getCfValue(SubscriptionCFsEnum.SWAP_TEL.name(), invoiceDate).toString() : null;
            if (changeTelCf != null && changeTelCf != changeTelCfEbill) {
                subscriptionCode = changeTelCf.split(Pattern.quote("|"))[2];
            }
            if (swapTelCf != null && swapTelCf != swapTelCfEbill) {
                subscriptionCode = swapTelCf.split(Pattern.quote("|"))[2];
            }
        }

        return subscriptionCode;
    }

    /**
     * Gets the charge reference.
     *
     * @param subscription the subscription
     * @param invoiceDate  the invoice date
     * @return the charge reference
     */
    @SuppressWarnings("unchecked")
    private String getChargeReference(Subscription subscription, Date invoiceDate) {
        CustomFieldValue lastCfValue = utilService.getLastCfVersion(subscription, SubscriptionCFsEnum.MANUAL_CHARGE.name(), invoiceDate, true);
        if (subscription != null && lastCfValue != null && lastCfValue.getMapValue() != null && !lastCfValue.getMapValue().isEmpty()) {
            Map<String, String> manualChargeCF = (Map<String, String>) utilService.getCFValue(subscription, SubscriptionCFsEnum.MANUAL_CHARGE.name(),
                    (String) lastCfValue.getMapValue().values().iterator().next());
            if (manualChargeCF != null && !manualChargeCF.isEmpty()) {
                String chargeReference = manualChargeCF.get("CHARGE_REFERENCE");
                return chargeReference != null ? chargeReference : EMPTY_STRING;
            }
        }
        return EMPTY_STRING;
    }

    private String getOperatorOrderNumber(String items) {
        if (StringUtils.isBlank(items)) {
            return EMPTY_STRING;
        }
        String orderNumber;
        for (String item : items.split(",")) {
            orderNumber = item.split(Pattern.quote("|"))[17];
            if (StringUtils.isNotBlank(orderNumber) && !orderNumber.equalsIgnoreCase("null")) {
                return orderNumber;
            }
        }
        return EMPTY_STRING;
    }

    /**
     * Get Service parameters custom field definition
     *
     * @return Custom field template
     */
    protected CustomFieldTemplate getServiceParametersCFT() {

        if (serviceParamsCFT == null) {
            CustomFieldTemplate cft = cftService.findByCodeAndAppliesTo(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), new ServiceInstance());
            cftService.detach(cft);
            serviceParamsCFT = cft;
        }

        return serviceParamsCFT;
    }

    /**
     * Checks if is misc offer.
     *
     * @param ratedTransactiondto the rated transactiondto
     * @return true, if is misc offer
     */
    private boolean isMiscOffer(String offerCode) {
        return offerCode != null && offerCode.contains("MISC");
    }

    public Date getEndOfMonth(Date date) {
        java.util.Calendar c = java.util.Calendar.getInstance();
        c.setTime(date);
        c.set(java.util.Calendar.DAY_OF_MONTH, c.getActualMaximum(java.util.Calendar.DAY_OF_MONTH));

        return c.getTime();
    }

    /**
     * Gets the service action. Cease or Provide
     *
     * @param service the service
     * @return the service action
     */
    private String getServiceAction(ServiceInstance service, String type, String orderItems) {
        if (StringUtils.isNotBlank(orderItems) && service != null && service.getCode() != null) {
            String billCode = service.getCode().replace("_RC", EMPTY_STRING).replace("_NRC", EMPTY_STRING);
            String orderAction = EMPTY_STRING;

            if (orderItems.contains(billCode)) {
                String subCf = orderItems.substring(orderItems.indexOf(billCode));
                int indexOfProvide = subCf.indexOf("PROVIDE");
                int indexOfCease = subCf.indexOf("CEASE");
                int indexOfChange = subCf.indexOf("CHANGE");
                int indexOfChangeNumber = subCf.indexOf("CHANGE NUMBER");
                int indexOfSwapNumber = subCf.indexOf("SWAP");
                int indexOfMisc = subCf.indexOf("MISC");
                int indexOfChangeUan = subCf.indexOf("CHANGE UAN");
                int indexOfTransfer = subCf.indexOf("TRANSFER");
//                int indexOfRebate = subCf.indexOf("REBATE");
                int min = 10000;
                if (indexOfProvide != -1 && indexOfProvide < min) {
                    min = indexOfProvide;
                    orderAction = "PROVIDE";
                }
                if (indexOfCease != -1 && indexOfCease < min) {
                    min = indexOfCease;
                    orderAction = "CEASE";
                }
                if (indexOfChange != -1 && indexOfChange < min) {
                    min = indexOfChange;
                    orderAction = "CHANGE";
                }
                if (indexOfTransfer != -1 && indexOfTransfer < min) {
                    min = indexOfTransfer;
                    orderAction = "CHANGE";
                }
                if (indexOfChangeNumber != -1 && indexOfChangeNumber < min) {
                    min = indexOfChangeNumber;
                    orderAction = "CHANGE NUMBER";
                }
                if (indexOfChangeUan != -1 && indexOfChangeUan < min) {
                    min = indexOfChangeUan;
                    orderAction = "CHANGE UAN";
                }
                if (indexOfSwapNumber != -1 && indexOfSwapNumber < min) {
                    min = indexOfSwapNumber;
                    orderAction = "SWAP NUMBER";
                }
                if (indexOfMisc != -1 && indexOfMisc < min) {
                    min = indexOfMisc;
                    orderAction = "MISC CHARGE";
                }
//                if (indexOfRebate != -1 && indexOfRebate < min) {
//                    min = indexOfRebate;
//                    orderAction = "REBATE";
//                }
            }


            if ("PROVIDE".equals(orderAction)) {
                if ("BLOPORD".equals(type)) {
                    return "Provide new account";
                } else {
                    return "PROVIDE";
                }
            } else if ("CEASE".equals(orderAction)) {
                if ("BLOPORD".equals(type)) {
                    return "Cease existing account";
                } else {
                    return "CEASE";
                }
            } else if ("CHANGE".equals(orderAction) || "SWAP NUMBER".equals(orderAction) || "CHANGE NUMBER".equals(orderAction) || "CHANGE UAN".equals(orderAction)
                    || "MISC CHARGE".equals(orderAction)) {
                if ("BLOPORD".equals(type)) {
                    return "Change on existing account";
                } else {
                    return orderAction;
                }
            }
//          else if ("REBATE".equals(orderAction)) {
//              if ("BLOPORD".equals(type)) {
//                  return "Rebate on existing account";
//              } else {
//                  return "REBATE";
//              }
//          }

            return orderAction;

        }
        return EMPTY_STRING;
    }


    private String getWlrCycleNo(BillingCycle bc) {
        // Ex: 18/05 and the bill cycle name is WLR-71 hence 180571
        if (bc != null) {
            Date date = new Date();
            return String.format("%02d", DateUtils.getDayFromDate(date)) + String.format("%02d", (DateUtils.getMonthFromDate(date) + 1))
                    + bc.getCode().substring(bc.getCode().length() - 2);
        }
        return EMPTY_STRING;
    }


    /**
     * Get service parameters
     *
     * @param serviceInstance the service instance
     * @return the service parameters CF
     */
    @SuppressWarnings("unchecked")
    private Map<String, String> getServiceParameters(ServiceInstance serviceInstance, Date date) {
        CustomFieldValue lastCfValue = utilService.getLastCfVersion(serviceInstance, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), date, true);
        Map<String, String> serviceParametersCF = null;
        if (serviceInstance != null && lastCfValue != null && lastCfValue.getMapValue() != null && !lastCfValue.getMapValue().isEmpty()) {
            serviceParametersCF = (Map<String, String>) utilService.getCFValue(serviceInstance, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(),
                    (String) lastCfValue.getMapValue().values().iterator().next());
        }
        return serviceParametersCF;
    }

    private List<CustomFieldValue> getServiceParameters(ICustomFieldEntity serviceOrChargeInstance, Date startDate, Date endDate, boolean ignoreLastCFPeriod, boolean useLastKnownValue) {

        if (serviceOrChargeInstance == null || serviceOrChargeInstance.getCfValues() == null || serviceOrChargeInstance.getCfValues().getValuesByCode() == null) {
            return null;
        }
        List<CustomFieldValue> cfValues = serviceOrChargeInstance.getCfValues().getValuesByCode().get(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());
        cfValues = cfValues.stream().filter(cfv -> cfv.getPeriod() == null || (cfv.getPeriod().isCorrespondsToPeriod(startDate, endDate, false) && !cfv.getPeriod().hasNoDuration()))
                .sorted(Comparator.comparing(CustomFieldValue::getPeriod)).collect(Collectors.toList());
        if (ignoreLastCFPeriod) {

            if (!cfValues.isEmpty()) {
                int lastIndex = cfValues.size() - 1;
                CustomFieldValue cfValueClone = cfValues.get(lastIndex).clone();
                cfValueClone.setPeriod(new DatePeriod(cfValues.get(lastIndex).getPeriod().getFrom(), null));
                cfValues.set(lastIndex, cfValueClone);

            } else {
                cfValues = serviceOrChargeInstance.getCfValues().getValuesByCode().get(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());

                cfValues = cfValues.stream().filter(cfv -> cfv.getPeriod() == null || !cfv.getPeriod().hasNoDuration()).sorted(Comparator.comparing(CustomFieldValue::getPeriod)).collect(Collectors.toList());
                if (cfValues != null && cfValues.size() > 0) {
                    int lastIndex = cfValues.size() - 1;
                    CustomFieldValue cfValueClone = cfValues.get(lastIndex).clone();
                    cfValueClone.setPeriod(new DatePeriod(cfValues.get(lastIndex).getPeriod().getFrom(), null));
                    cfValues = Arrays.asList(cfValueClone);
                }
            }
        }

        // Special case when provide and cease order is on the same date
        if (cfValues.isEmpty() && useLastKnownValue) {
            cfValues = serviceOrChargeInstance.getCfValues().getValuesByCode().get(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());
            int lastIndex = cfValues.size() - 1;
            CustomFieldValue cfValueClone = cfValues.get(lastIndex).clone();
            cfValueClone.setPeriod(new DatePeriod(cfValues.get(lastIndex).getPeriod().getFrom(), null));
            cfValues = Arrays.asList(cfValueClone);
        }
        return cfValues;
    }


    @SuppressWarnings("unchecked")
    private List<Object[]> getAssociationsAndAddresses(Long invoiceId) {
        return ratedTransactionService.getEntityManager()
                .createNativeQuery("select distinct r.subscription_id,  a.assoc_circ_id as a_Association, b.assoc_circ_id as b_Association," + " c.assoc_circ_id as c_Association,"
                        + " ea.ep_name as a_ep_name, "
                        + " concat(ea.address_line_1,';',ea.address_line_2,';',ea.address_line_3,';',ea.address_line_4,';',ea.address_line_5,';',ea.county,';',ea.postal_district,';',ea.country,';',ea.eir_code) as A_address,"
                        + " eb.ep_name as b_ep_name, "
                        + " concat(eb.address_line_1,';',eb.address_line_2,';',eb.address_line_3,';',eb.address_line_4,';',eb.address_line_5,';',eb.county,';',eb.postal_district,';',eb.country,';',eb.eir_code) as B_address                   "
                        + " from billing_rated_transaction r" + " left join eir_sub_association a on r.subscription_id = a.subscription_id and a.assoc_end='A'"
                        + " left join eir_sub_association b on r.subscription_id = b.subscription_id and b.assoc_end='B'"
                        + " left join eir_sub_association c on r.subscription_id = c.subscription_id and c.assoc_end='C'"
                        + " left join eir_sub_endpoint ea on r.subscription_id = ea.subscription_id and ea.ep_end='A'"
                        + " left join eir_sub_endpoint eb on r.subscription_id = eb.subscription_id and eb.ep_end='B'" + " where invoice_id = :invoice_id order by r.subscription_id")
                .setParameter("invoice_id", invoiceId).setFlushMode(FlushModeType.COMMIT).getResultList();
    }

    private String getEbillFileName(Invoice invoice) {
        String ebillFileName = EMPTY_STRING;
        String pdfFilename = invoice.getPdfFilename();
        if (pdfFilename != null) {
            int pos = Integer.max(pdfFilename.lastIndexOf("/"), pdfFilename.lastIndexOf("\\"));
            if (pos > -1) {
                ebillFileName = pdfFilename.substring(0, pos + 1);
                String ebillFolder = paramBeanFactory.getChrootDir() + File.separator + "invoices" + File.separator + "pdf" + File.separator + ebillFileName;
                File f = new File(ebillFolder);
                if (!f.exists()) {
                    f.mkdirs();
                }
            }
        }
        BillingAccount ba = invoice.getBillingAccount();
        String servCode = null;
        String customer = null;
        String baNumber = null;
        String baCode = null;
        String customerCode = null;
        if (ba != null) {
            servCode = (String) ba.getCfValue(SERVICE_TYPE);
            baCode = ba.getCode();
            // customer = (String) ba.getCfValue(OAO_ID);
            CustomerAccount ca = ba.getCustomerAccount();
            if (ca != null) {
                customer = ca.getDescription();
                customerCode = ca.getCustomer() != null ? ca.getCustomer().getCode() : null;
                if (StringUtils.isBlank(customer)) {
                    Name name = ca.getName();
                    if (name == null) {
                        customer = EMPTY_STRING;
                    } else {
                        customer = (name.getFirstName() != null ? name.getFirstName() + " " : EMPTY_STRING) + (name.getLastName() != null ? name.getLastName() : EMPTY_STRING);
                    }

                }
            }

            baNumber = ba.getExternalRef1();
        }
        if (customer != null && customer.contains(File.separator)) {
            customer = customer.replace(File.separator, "-");
        }
        if (baNumber != null && baNumber.contains(File.separator)) {
            baNumber = baNumber.replace(File.separator, "-");
        }

        if (WLR_SERVICE_TYPE.equals(servCode)) {
            String ebillSeq = getEbillSeq(ba.getBillingCycle(), ba.getBillingRun());
            return ebillFileName + customerCode + DOT + baCode + DOT + ebillSeq + DATA_EXTENSION;
        }

        return ebillFileName + servCode + UNDERSCORE + DateUtils.formatDateWithPattern(invoice.getInvoiceDate(), DATE_FORMAT_DDMMYYYY) + UNDERSCORE
                + invoice.getInvoiceNumber() + UNDERSCORE + baNumber + CSV_EXTENSION;
    }

    private String getEbillSeq(BillingCycle bc, BillingRun br) {
        String brId = EMPTY_STRING;
        String bcCode = "global";
        if (br != null) {
            brId = String.valueOf(br.getId());
        }
        if (bc != null) {
            bcCode = bc.getCode();
        }
        ParamBean paramBean = paramBeanFactory.getInstance();
        String eBillSeq = String.valueOf(paramBean.getProperty("ebill.billingRun.seq." + bcCode, brId + "_216"));
        int seq = Integer.valueOf(eBillSeq.substring(eBillSeq.indexOf('_') + 1));
        if (!eBillSeq.startsWith(brId)) {
            seq++;
        }
        paramBean.getProperties().put("ebill.billingRun.seq." + bcCode, brId + "_" + seq);
        paramBean.saveProperties();
        eBillSeq = String.valueOf(seq);

        while (eBillSeq.length() < 4) {
            eBillSeq = "0" + eBillSeq;
        }
        return "G" + eBillSeq;
    }

    public ScrollableResults getRatedTransactionsByInvoice(Invoice invoice, String ebillFormat) {
        if (invoice == null || invoice.getId() == null) {
            return null;
        }

        String sql = "SELECT r.id as R_ID, r.code as R_CODE, r.description as R_DESCRIPTION, r.startDate as R_START_DATE, r.endDate as R_END_DATE, r.usageDate as R_USAGE_DATE, r.quantity as R_QUANTITY,r.amountWithTax as R_AMOUNT_WITH_TAX, r.amountWithoutTax as R_AMOUNT_WITHOUT_TAX, r.rawAmountWithoutTax as R_RAW_AMOUNT_WITHOUT_TAX, r.taxPercent as R_TAX_PERCENT, r.unitAmountWithoutTax as R_UNIT_AMOUNT_WITHOUT_TAX, r.orderNumber as R_ORDER_NUMBER,"
                + " varcharFromJson(o.cfValues, ORDER_ITEMS, mapString) as O_ORDER_ITEMS, r.taxClass.id as TAX_CLASS_ID, r.subscription.id as SUBSCRIPTION_ID, r.subscription.code as SUBSCRIPTION_CODE,r.subscription.cfValues as SUBSCRIPTION_CFVALUES, r.serviceInstance.id as SERVICE_INSTANCE_ID, r.serviceInstance.code as SERVICE_INSTANCE_CODE , r.serviceInstance.cfValues as SERVICE_INSTANCE_CFVALUES,"
                + " wo.cfValues as WO_CFVALUES, varcharFromJson(r.serviceInstance.serviceTemplate.cfValues,SERVICE_TYPE, string) as SERVICE_TYPE,"
                + " r.edr.parameter1 as EDR_PARAMETER1, r.edr.parameter6 as EDR_PARAMETER6, r.edr.extraParameter as EDR_EXTRAPARAMETER, r.edr.quantity as EDR_QUANTITY, r.edr.eventDate as EDR_EVENT_DATE, r.invoiceSubCategory.code as INVOICESUBCATEGORY_CODE, r.invoiceSubCategory.description as INVOICESUBCATEGORY_DESCRIPTION, r.chargeInstance.chargeType as CHARGE_TYPE, r.offerTemplate.code as OFFER_CODE"
                + " FROM RatedTransaction r"
                + " left join Order o ON r.orderNumber=o.code "
                + " left join r.edr "
                + " left join r.taxClass "
                + " left join r.subscription "
                + " left join r.serviceInstance "
                + " left join r.serviceInstance.serviceTemplate "
                + " left join r.invoiceSubCategory "
                + " left join r.chargeInstance "
                + " left join r.offerTemplate "
                + " left join WalletOperation wo on wo.status='TREATED' and wo.ratedTransaction.id=r.id "
                + " where r.invoice.id=:invoice_id and r.status='BILLED' ";
        if (WLR_SERVICE_TYPE.equals(ebillFormat)) {
            sql += "order by varcharFromJson(r.subscription.cfValues, UAN, string) , lower(r.subscription.code), lower(r.orderNumber)";
        } else {
            sql += "order by r.subscription.id";
        }

        ScrollableResults results = null;
        Map<String, Object> params = new HashMap<>();
        params.put("invoice_id", invoice.getId());
        try {
            results = getScrollableResultNativeQuery(sql, params);
        } catch (Exception e) {
            throw new BusinessException(e);
        }

        return results;
    }

    private WalletOperation getWalletOperation(CustomFieldValues customFieldValues) {
        WalletOperation wo = new WalletOperation();
        wo.setCfValues(customFieldValues);
        return wo;
    }

    private ServiceInstance getServiceInstance(Long id, String code, CustomFieldValues cfValues) {
        ServiceInstance service = new ServiceInstance();
        service.setId(id);
        service.setCode(code);
        service.setCfValues(cfValues);
        return service;
    }

    private Subscription getSubscription(Long id, String code, CustomFieldValues cfValues) {
        Subscription sub = new Subscription();
        sub.setId(id);
        sub.setCode(code);
        sub.setCfValues(cfValues);
        return sub;
    }

    private RatedTransaction getRatedTransaction(Long id, String code, String description, Date startDate, Date endDate, Date usageDate, BigDecimal quantity,  BigDecimal amountWithTax, BigDecimal amountWithoutTax,
            BigDecimal rawAmountWithoutTax, BigDecimal taxPercent, BigDecimal unitAmountWithoutTax, String orderNumber) {
        RatedTransaction rt = new RatedTransaction();
        rt.setId(id);
        rt.setCode(code);
        rt.setDescription(description);
        rt.setStartDate(startDate);
        rt.setEndDate(endDate);
        rt.setUsageDate(usageDate);
        rt.setQuantity(quantity);
        rt.setAmountWithTax(amountWithTax);
        rt.setAmountWithoutTax(amountWithoutTax);
        rt.setRawAmountWithoutTax(rawAmountWithoutTax);
        rt.setTaxPercent(taxPercent);
        rt.setUnitAmountWithoutTax(unitAmountWithoutTax);
        rt.setOrderNumber(orderNumber);
        return rt;
    }

    public ScrollableResults getScrollableResultNativeQuery(String query, Map<String, Object> params) {
        Session session = ratedTransactionService.getEntityManager().unwrap(Session.class);
        Query q = session.createQuery(query);

        q.setResultTransformer(AliasToEntityOrderedMapResultTransformer.INSTANCE);
        q.setFetchSize(10000);
        q.setReadOnly(true);
        q.setLockMode("a", LockMode.NONE);
        if (params != null) {
            for (Map.Entry<String, Object> entry : params.entrySet()) {
                q.setParameter(entry.getKey(), entry.getValue());
            }
        }
        return q.scroll(ScrollMode.FORWARD_ONLY);
    }
    
    private boolean isRtInFullPeriod(RatedTransaction rt) {
        java.util.Calendar rtStartDate = java.util.Calendar.getInstance();
        rtStartDate.setTime(rt.getStartDate());
        java.util.Calendar rtEndDate = java.util.Calendar.getInstance();
        rtEndDate.setTime(rt.getEndDate());
        return rtStartDate.get(java.util.Calendar.DAY_OF_MONTH) == rtEndDate.get(java.util.Calendar.DAY_OF_MONTH) ;
    }

}
