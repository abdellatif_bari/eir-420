/*
 * (C) Copyright 2015-2020 Opencell SAS (https://opencellsoft.com/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
 * OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS
 * IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO
 * THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 *
 * For more information on the GNU Affero General Public License, please consult
 * <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

package com.eir.script;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.ParamBean;
import org.meveo.commons.utils.ParamBeanFactory;
import org.meveo.commons.utils.QueryBuilder;
import org.meveo.model.billing.Invoice;
import org.meveo.service.billing.impl.InvoiceService;
import org.meveo.service.script.Script;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.SftpException;

/**
 * A script for sending invoices and e-bills to OAO Hub
 * 
 * @author Mohammed Amine Tazi
 */
@Stateless
public class InvoiceFileSender extends Script {

    private static final long serialVersionUID = -9117095335653744694L;
    
    @Inject
    private InvoiceService invoiceService;
    
    @Inject
    protected ParamBeanFactory paramBeanFactory;

    
    @Override
    public void execute(Map<String, Object> methodContext) throws BusinessException {
        List<Invoice> invoicesToSend = findInvoicesToSend();
        
        ChannelSftp channelSftp = null;
        try {
            channelSftp = setupJsch();
            channelSftp.connect();
            int nbInvoicesSent = 0;
            int nbInvoicesNotSent = 0;
            StringBuilder report = new StringBuilder();
            for(Invoice invoice : invoicesToSend) {                
                try {
                    sendInvoice(invoice, channelSftp);       
                } catch(Exception ex) {
                    log.error("error sending invoice id {} with code {}", invoice.getId(), invoice.getCode(), ex);
                    nbInvoicesNotSent++;
                    report.append(invoice.getCode()).append(" : ").append(ex.getMessage()).append("\n");
                    continue;
                }
                nbInvoicesSent++;                
            }
            methodContext.put(Script.JOB_RESULT_NB_OK, nbInvoicesSent);
            methodContext.put(Script.JOB_RESULT_NB_KO, nbInvoicesNotSent);
            methodContext.put(Script.JOB_RESULT_REPORT, report);
            log.debug("{} invoices sent to hub directory", nbInvoicesSent);
        } catch(Exception ex) {
            log.debug("unable to send invoices to hub directory : {}", ex.getMessage());
            throw new BusinessException(ex);
        } finally {
            if(channelSftp != null) {
                channelSftp.exit();
            }
        }
        
    }
    
    @SuppressWarnings("unchecked")
    private List<Invoice> findInvoicesToSend() {
        QueryBuilder queryBuilder = new QueryBuilder(Invoice.class, "i", null);
        queryBuilder.addSqlCriterion("booleanFromJson(cfValues, E_BILL_COPIED_TO_HUB, boolean) = 'false' or booleanFromJson(cfValues, E_BILL_COPIED_TO_HUB, boolean) is null", null, null);
        queryBuilder.addSqlCriterion("varcharFromJson(billingAccount.cfValues, BILL_DELIVERY_TYPE, string) = 'OAO_HUB'", null, null);

        List<Invoice> invoices = queryBuilder.getQuery(invoiceService.getEntityManager()).getResultList();
        return invoices;       
    }
    
    private ChannelSftp setupJsch() throws JSchException {
        String userName = paramBeanFactory.getInstance().getProperty("eir.sftp.invoice.username", null);
        String password = paramBeanFactory.getInstance().getProperty("eir.sftp.invoice.password", null);
        String remoteHost = paramBeanFactory.getInstance().getProperty("eir.sftp.invoice.remotehost", null);
        String port = paramBeanFactory.getInstance().getProperty("eir.sftp.invoice.port", null);        
        JSch jsch = new JSch();
        Session jschSession = jsch.getSession(userName, remoteHost);
        jschSession.setPassword(password);
        jschSession.setPort(Integer.parseInt(port));
        jschSession.setTimeout(20000);
        jschSession.setConfig("StrictHostKeyChecking", "no");
        jschSession.connect();
        return (ChannelSftp) jschSession.openChannel("sftp");
    }

    public void sendInvoice(Invoice invoice, ChannelSftp channelSftp) throws JSchException, SftpException {
        String hubDirectory = (String) invoice.getBillingAccount().getCfValue("HUB_DIRECTORY");        
        String invoiceFileName = invoiceService.getFullPdfFilePath(invoice, false);
        if(invoice.getCfValue("E_BILL_FILENAME") != null && !"".equals(invoice.getCfValue("E_BILL_FILENAME").toString().trim())) {
            String ebillFileName = invoice.getCfValue("E_BILL_FILENAME").toString(); 
            String rootDir = "opencelldata/" + ParamBean.getInstance().getProperty("provider.rootDir", "default");
            channelSftp.put(rootDir + "/invoices/pdf/"+ebillFileName, hubDirectory + "/" + getFileNameOnly(ebillFileName));        } 
        channelSftp.put(invoiceFileName, hubDirectory + "/" + getFileNameOnly(invoiceFileName));
        invoice.setCfValue("E_BILL_COPIED_TO_HUB", true);
        invoiceService.update(invoice);    
    }
    
    public String getFileNameOnly(String fileName) {
        if (fileName != null) {
            int pos = Integer.max(fileName.lastIndexOf("/"), fileName.lastIndexOf("\\"));
            if (pos > -1) {
                return fileName.substring(pos + 1);
            }
        }
        return fileName;
    }

    
}
