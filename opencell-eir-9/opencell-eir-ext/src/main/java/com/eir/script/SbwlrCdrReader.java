package com.eir.script;

import org.meveo.api.commons.Utils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.parse.csv.MEVEOCdrFlatFileReader;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.bi.FlatFile;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.bi.impl.FlatFileService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.regex.Pattern;

/**
 * @author Houssine Znibar
 */
@Stateless
public class SbwlrCdrReader extends MEVEOCdrFlatFileReader {

    @Inject
    private FlatFileService flatFileService;


    @Override
    public void init(File cdrFile) throws FileNotFoundException {
        if (cdrFile == null) {
            throw new BusinessException("Invalid file");
        }
//        String origineFileName = cdrFile.getName();
//        origineFileName = origineFileName.substring(0, origineFileName.lastIndexOf(".processing_"));
//        if (!validateFilename(origineFileName)) {
//            throw new BusinessException("Invalid file name");
//        }
        super.init(cdrFile);
    }

    /**
     * Validate file name Naming convention similar to: WLR_cvc.ime.xxx.calls.YYYYMMDDhhmmss_nnnn.csv xxx = identifying exchange type in IME. nnnn = sequence number 0001-9999 plus
     * the date & time.
     *
     * @param filename the file name
     * @return true is the file name is valid.
     */
    private boolean validateFilename(String filename) {
        String[] partsOfTheName = filename.split(Pattern.quote("_"));

        if (partsOfTheName.length < 3 || partsOfTheName.length > 4) {
            return false;
        }

        /**
         * Case in which the file has already been validated with the file format and that its name has been prefixed by the flatFile code.
         */
        if (partsOfTheName.length == 4) {
            boolean isValidFileFormat = false;
            String code = partsOfTheName[0];
            if (code != null) {
                FlatFile flatFile = flatFileService.findByCode(code);
                if (flatFile != null) {
                    isValidFileFormat = true;
                    // Remove the code prefix from name.
                    filename = String.join("_", Arrays.copyOfRange(partsOfTheName, 1, partsOfTheName.length));
                }
            }
            if (!isValidFileFormat) {
                return false;
            }
        }

        partsOfTheName = filename.split(Pattern.quote("."));
        if (partsOfTheName.length != 6) {
            return false;
        }

        if (!filename.startsWith("WLR_cvc.ime.")) {
            return false;
        }

        if (StringUtils.isBlank(partsOfTheName[2]) || partsOfTheName[2].length() != 3) {
            return false;
        }

        if (StringUtils.isBlank(partsOfTheName[3]) || !partsOfTheName[3].equals("calls")) {
            return false;
        }

        if (StringUtils.isBlank(partsOfTheName[4])) {
            return false;
        }

        String[] dateAndSequenceNumber = partsOfTheName[4].split(Pattern.quote("_"));
        if (dateAndSequenceNumber.length != 2) {
            return false;
        }

        if (StringUtils.isBlank(dateAndSequenceNumber[0]) || DateUtils.parseDateWithPattern(dateAndSequenceNumber[0], "yyyyMMddHHmmss") == null) {
            return false;
        }

        if (StringUtils.isBlank(dateAndSequenceNumber[1]) || !Utils.isInteger(dateAndSequenceNumber[1]) || dateAndSequenceNumber[1].length() != 4) {
            return false;
        }
        return true;
    }

}