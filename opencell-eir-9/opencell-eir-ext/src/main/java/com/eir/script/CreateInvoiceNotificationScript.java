package com.eir.script;

import java.math.BigDecimal;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.TypedQuery;

import org.meveo.admin.exception.BusinessException;
import org.meveo.jpa.EntityManagerWrapper;
import org.meveo.jpa.MeveoJpa;
import org.meveo.model.billing.Invoice;
import org.meveo.model.payments.RecordedInvoice;
import org.meveo.model.payments.RecordedInvoiceCatAgregate;
import org.meveo.service.script.Script;

/**
 * Triggered upon invoice creation and populates last invoice amount, payments and adjustments, due balance and net to pay fields
 * 
 * @author Andrius Karpavicius
 */
@Stateless
public class CreateInvoiceNotificationScript extends Script {

    private static final long serialVersionUID = 3655065152212276700L;

    @Inject
    @MeveoJpa
    private EntityManagerWrapper emWrapper;

    @Override
    public void execute(Map<String, Object> context) throws BusinessException {

        Invoice invoice = (Invoice) context.get(Script.CONTEXT_ENTITY_OR_EVENT);

        EntityManager em = getEntityManager();

        // Retrieve the last invoice
        String sql = "select ri.id from RecordedInvoice ri where ri.invoice.billingAccount=:ba order by ri.id desc";

        Long lastRecordedInvoiceId = null;
        BigDecimal lastInvoiceAmount = BigDecimal.ZERO;
        try {
            lastRecordedInvoiceId = em.createQuery(sql, Long.class).setParameter("ba", invoice.getBillingAccount()).setMaxResults(1).getSingleResult();
        } catch (NoResultException e) {
        }

        try {
            sql = "select sum(case when ao.transactionCategory = 'DEBIT' then ao.amount else (-1 * ao.amount) end) from AccountOperation ao where ao.matchingStatus not in ('C','R') and varcharFromJson(ao.cfValues,BILLING_ACCOUNT_NUMBER)=:baNumber";
            if (lastRecordedInvoiceId != null) {
                sql = sql + " and ao.id<=:lastId";
            }

            TypedQuery<BigDecimal> query = em.createQuery(sql, BigDecimal.class).setParameter("baNumber", invoice.getBillingAccount().getExternalRef1());
            if (lastRecordedInvoiceId != null) {
                query.setParameter("lastId", lastRecordedInvoiceId);
            }

            lastInvoiceAmount = query.getSingleResult();
            if (lastInvoiceAmount == null) {
                lastInvoiceAmount = BigDecimal.ZERO;
            }
            // Record last invoice amount
            invoice.setCfValue("LAST_INVOICE_AMOUNT", lastInvoiceAmount);

        } catch (NoResultException e) {
        }

        // Calculate a total of payments/adjustments per billing account since the last invoice

        BigDecimal paymentsAndAdjustments = null;
        if (lastRecordedInvoiceId != null) {
            sql = "select sum(case when ao.transactionCategory = 'DEBIT' then ao.amount else (-1 * ao.amount) end) from AccountOperation ao where type(ao) !=:invoiceAOType and type(ao) !=:invoiceCatAgrAOType and ao.matchingStatus not in ('C','R') and varcharFromJson(ao.cfValues,BILLING_ACCOUNT_NUMBER)=:baNumber and ao.id>:lastRecordedInvoiceId";
            TypedQuery<BigDecimal> query = em.createQuery(sql, BigDecimal.class).setParameter("baNumber", invoice.getBillingAccount().getExternalRef1()).setParameter("invoiceAOType", RecordedInvoice.class)
                .setParameter("invoiceCatAgrAOType", RecordedInvoiceCatAgregate.class).setParameter("lastRecordedInvoiceId", lastRecordedInvoiceId);

            paymentsAndAdjustments = query.getSingleResult();
        }
        if (paymentsAndAdjustments == null) {
            paymentsAndAdjustments = BigDecimal.ZERO;
        }
        invoice.setCfValue("PAYMENTS_ADJUSTMENTS", paymentsAndAdjustments);

        BigDecimal dueBalance = lastInvoiceAmount.add(paymentsAndAdjustments);
        invoice.setDueBalance(dueBalance);
        invoice.setNetToPay(invoice.getAmountWithTax().add(dueBalance));
    }

    public EntityManager getEntityManager() {
        return emWrapper.getEntityManager();
    }
}
