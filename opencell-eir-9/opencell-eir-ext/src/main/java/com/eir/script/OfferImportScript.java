package com.eir.script;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.model.catalog.LifeCycleStatusEnum;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.OfferTemplateCategory;
import org.meveo.model.crm.Provider;
import org.meveo.service.catalog.impl.OfferTemplateCategoryService;
import org.meveo.service.catalog.impl.OfferTemplateService;
import org.meveo.service.catalog.impl.OneShotChargeTemplateService;
import org.meveo.service.catalog.impl.RecurringChargeTemplateService;
import org.meveo.service.catalog.impl.ServiceChargeTemplateRecurringService;
import org.meveo.service.catalog.impl.ServiceChargeTemplateSubscriptionService;
import org.meveo.service.catalog.impl.ServiceTemplateService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.script.Script;
import org.meveo.util.ApplicationProvider;

/**
 * Script to import offer template definition
 * 
 * @author Andrius Karpavicius
 *
 */
@Stateless
public class OfferImportScript extends Script {

    private static final long serialVersionUID = -3467788957890975696L;

    @Inject
    private CustomTableService customTableService;

    @Inject
    private OfferTemplateService offerTemplateService;

    @Inject
    private ServiceTemplateService serviceTemplateService;

    @Inject
    private OneShotChargeTemplateService oneshotChargeTemplateService;

    @Inject
    private RecurringChargeTemplateService recurringChargeTemplateService;

    @Inject
    private ServiceChargeTemplateRecurringService serviceChargeTemplateRecurringService;

    @Inject
    private ServiceChargeTemplateSubscriptionService serviceChargeTemplateSubscriptionService;
//
//    @Inject
//    private ServiceChargeTemplateTerminationService serviceChargeTemplateTerminationService;

    @Inject
    private OfferTemplateCategoryService offerTemplateCategoryService;

    @Inject
    @ApplicationProvider
    private Provider appProvider;

    private static Map<String, OfferTemplateCategory> offerCategoryMap = new HashMap<>();

    @Override
    public void init(Map<String, Object> methodContext) throws BusinessException {

        // Initialize with a map of offer type to an offer template category entity
        List<OfferTemplateCategory> offerCategories = offerTemplateCategoryService.list(true);
        for (OfferTemplateCategory offerTemplateCategory : offerCategories) {

            String offerType = (String) offerTemplateCategory.getCfValue("offerType");
            if (offerType != null) {
                offerCategoryMap.put(offerType, offerTemplateCategory);
            }
        }
    }

    @Override
    public void execute(Map<String, Object> initContext) throws BusinessException {

        @SuppressWarnings("unchecked")
        Map<String, Object> offerLine = (Map<String, Object>) initContext.get(Script.CONTEXT_RECORD);

        OfferTemplate offer = offerTemplateService.findByCode((String) offerLine.get("offer_code"));
        boolean offerCreate = offer == null;

        if (offer == null) {
            offer = new OfferTemplate();
            offer.setCode((String) offerLine.get("offer_code"));
            offer.setName(offer.getCode());
            offer.setLifeCycleStatus(LifeCycleStatusEnum.ACTIVE);
        }

        OfferTemplateCategory matchedOfferCategory = offerCategoryMap.get((String) offerLine.get("offer_type"));
        if (matchedOfferCategory != null) {
            offer.getOfferTemplateCategories().clear();
            offer.getOfferTemplateCategories().add(matchedOfferCategory);

        } else if (!StringUtils.isBlank((String) offerLine.get("offer_type"))) {
            throw new BusinessException("Offer category '" + (String) offerLine.get("offer_type") + "' not found");
        }
        
        offer.setDescription((String) offerLine.get("description"));

        if (offerCreate) {
            offerTemplateService.create(offer);

        } else {
            offer = offerTemplateService.update(offer);
        }
    }
}