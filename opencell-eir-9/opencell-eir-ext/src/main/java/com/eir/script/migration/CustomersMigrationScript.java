package com.eir.script.migration;

import java.util.Map;
import javax.ejb.Stateless;
import javax.inject.Inject;
import org.apache.commons.collections.MapUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ElementNotFoundException;
import org.meveo.model.admin.Seller;
import org.meveo.model.crm.Customer;
import org.meveo.model.crm.CustomerCategory;
import org.meveo.service.admin.impl.SellerService;
import org.meveo.service.crm.impl.CustomerCategoryService;
import org.meveo.service.crm.impl.CustomerService;
import org.meveo.service.script.Script;

/**
 * Script for migrating EIR customers to OC
 * @author Mohammed Amine TAZI
 */
@Stateless
public class CustomersMigrationScript extends Script {

    private static final long serialVersionUID = -6288568811084129324L;

    @Inject
    private CustomerCategoryService customerCategoryService;
    
    @Inject
    private CustomerService customerService;

    @Inject 
    private SellerService sellerService;
    
    @Override
    public void execute(Map<String, Object> initContext) {
        try {
            @SuppressWarnings("unchecked")
            Map<String, Object> record = (Map<String, Object>) initContext.get("record");
           
            if(MapUtils.isEmpty(record)) {
                throw new BusinessException("Empty customer record!");
            }
            
            String customerCode = (String) record.get("CUSTOMER_CODE");
            String description = (String) record.get("DESCRIPTION");
            String customerCategory = (String) record.get("CUSTOMER_CATEGORY");
            String parentSeller = (String) record.get("PARENT_SELLER");
            String rapCustomer = (String) record.get("RAP_CUSTOMER");
            
            if(customerService.findByCode(customerCode) != null) {
            	throw new BusinessException("Customer code already exists : " + customerCode);
            }
            
            if(!"YES".equalsIgnoreCase(rapCustomer) && !"NO".equalsIgnoreCase(rapCustomer)) {
            	throw new ElementNotFoundException(rapCustomer, "RAP Customer ");
            }
                    
            CustomerCategory customerCategoryEntity = customerCategoryService.findByCode(customerCategory);
            if(customerCategoryEntity == null) {
                throw new ElementNotFoundException(customerCategory, "customerCategory");
            }
            
            Seller sellerEntity = sellerService.findByCode(parentSeller);
            if(sellerEntity == null) {
                throw new ElementNotFoundException(parentSeller, "Seller");
            }
            Customer customer = new Customer();
            customer.setCode(customerCode);
            customer.setDescription(description);
            customer.setCustomerCategory(customerCategoryEntity);
            customer.setSeller(sellerEntity);
            customer.setCfValue("STANDARD_CUSTOMER", "YES".equalsIgnoreCase(rapCustomer) ? true : false);
            customerService.create(customer);
        } catch (Exception e) {
            log.error("error adding customer {} ", e.getMessage(), e);
            if (e instanceof BusinessException) {
                throw e;
            } else {
                // wrap the exception in a business exception and throwing it
                throw new BusinessException(e.getMessage());
                
            }
        }

    }
}