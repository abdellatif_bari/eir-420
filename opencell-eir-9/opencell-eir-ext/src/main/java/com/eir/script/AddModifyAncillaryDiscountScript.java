package com.eir.script;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.joda.time.DateTimeComparator;
import org.meveo.admin.exception.ValidationException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.CustomTableEvent;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.script.Script;

/**
 * The Class AddModifyAncillaryDiscountScript.
 *
 * @author hznibar
 */
@Stateless
public class AddModifyAncillaryDiscountScript extends Script {

    private static final long serialVersionUID = 1L;

    @Inject
    private CustomTableService customTableService;

    @Override
    public void execute(Map<String, Object> initContext) throws ValidationException {
        log.debug("#####################Starting of script AddModifyAncillaryDiscountScript");
        try {
            CustomTableEvent ce = (CustomTableEvent) initContext.get("event");
            Map<String, Object> values = ce.getValues();
            // Customer Account
//            CustomerAccount ca = customerAccountService.findById((Long) values.get("customer_account"));
//            if (ca == null) {
//                throw new ValidationException("No CustomerAccount found with ID = " + values.get("customer_account"));
//            }            

            Date startDate = DateUtils.setDateToStartOfDay((Date) values.get("valid_from"));
            values.put("valid_from", startDate);
            // Discount’s start date cannot be earlier than system date.
            if (values.get("id") == null && (startDate == null || DateTimeComparator.getDateOnlyInstance().compare(startDate, new Date()) < 0)) {
                throw new ValidationException("Discount’s start date cannot be null or earlier than system date");
            }

            // End date cannot be earlier than system date or discount’s start date.
            Date endDate = null;
            if (values.get("valid_to") != null && !StringUtils.isBlank(values.get("valid_to"))) {
                endDate = DateUtils.setDateToStartOfDay((Date) values.get("valid_to"));
                values.put("valid_to", endDate);

                if (DateTimeComparator.getDateOnlyInstance().compare(endDate, startDate) < 0) {
                    throw new ValidationException("end date should be null or in the future");
                }
            }

            // Charge Type
            if (!"R".equals(values.get("charge_type")) && !"O".equals(values.get("charge_type")) && !"*".equals(values.get("charge_type"))) {
                throw new ValidationException("Charge Type should be 'O', 'R' or '*'");
            }
            
            // check if an existing line with same profile_code and endDate null
            if (isDiscountExistWithoutEndDate((String) values.get("profile_code"), endDate)) {
                throw new ValidationException("The end Date of an exiting discount with same profile_code is not set!");
            }

            if (isDiscountExistWithFutureEndDate((String) values.get("profile_code"), startDate, endDate)) {
                throw new ValidationException("The start date must be greater than the end date of an existing line with the same profile code.");
            }

        } catch (Exception e) {
            throw new ValidationException(e.getMessage());

        }
    }

    /**
     * Checks if is discount exist without end date.
     *
     * @param profileCode the profile code
     * @param endDate the end date
     * @return true, if is discount exist without end date
     */
    private boolean isDiscountExistWithoutEndDate(String profileCode, Date endDate) {
        Map<String, Object> searchCriteria = new HashMap<>();
        searchCriteria.put("profile_code", profileCode);
        searchCriteria.put("valid_to", "IS_NULL");
        long discounts = customTableService.count("EIR_OR_ANCILLARY_DISCOUNT", new PaginationConfiguration(searchCriteria));
        return (endDate == null && discounts > 1) || (endDate != null && discounts > 0);
    }

    /**
     * Checks if is discount exist with future end date.
     *
     * @param profileCode the profile code
     * @param startDate the start date
     * @param endDate the end date
     * @return true, if is discount exist with future end date
     */
    private boolean isDiscountExistWithFutureEndDate(String profileCode, Date startDate, Date endDate) {
        String query = "select 1 from EIR_OR_ANCILLARY_DISCOUNT where lower(profile_code) = :profileCode and valid_to >= :startDate";
        List discounts = customTableService.getEntityManager().createNativeQuery(query).setParameter("profileCode", profileCode.toLowerCase()).setParameter("startDate", startDate).setMaxResults(2).getResultList();
        return (endDate != null && discounts != null && discounts.size() > 1) || (endDate == null && discounts != null && discounts.size() > 0);
    }
}