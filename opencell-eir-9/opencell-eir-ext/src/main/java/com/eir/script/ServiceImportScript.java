package com.eir.script;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.model.catalog.OfferServiceTemplate;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.OneShotChargeTemplate;
import org.meveo.model.catalog.RecurringChargeTemplate;
import org.meveo.model.catalog.ServiceChargeTemplateRecurring;
import org.meveo.model.catalog.ServiceChargeTemplateSubscription;
import org.meveo.model.catalog.ServiceChargeTemplateTermination;
import org.meveo.model.catalog.ServiceChargeTemplateUsage;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.catalog.UsageChargeTemplate;
import org.meveo.model.crm.EntityReferenceWrapper;
import org.meveo.model.crm.Provider;
import org.meveo.model.customEntities.CustomEntityInstance;
import org.meveo.service.catalog.impl.OfferTemplateService;
import org.meveo.service.catalog.impl.OneShotChargeTemplateService;
import org.meveo.service.catalog.impl.RecurringChargeTemplateService;
import org.meveo.service.catalog.impl.ServiceChargeTemplateRecurringService;
import org.meveo.service.catalog.impl.ServiceChargeTemplateSubscriptionService;
import org.meveo.service.catalog.impl.ServiceChargeTemplateTerminationService;
import org.meveo.service.catalog.impl.ServiceChargeTemplateUsageService;
import org.meveo.service.catalog.impl.UsageChargeTemplateService;
import org.meveo.service.custom.CustomEntityInstanceService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.script.Script;
import org.meveo.util.ApplicationProvider;

import com.eir.commons.enums.ChargeTemplateEnum;
import com.eir.commons.enums.ChargeTypeEnum;
import com.eir.commons.enums.ServiceTypeEnum;
import com.eir.commons.enums.customfields.ServiceInstanceIdentifierAttributesCFsEnum;
import com.eir.commons.enums.customfields.ServiceTemplateCFsEnum;
import com.eir.commons.enums.customtables.CustomTableEnum;
import com.eir.service.catalog.impl.ServiceTemplateService;

/**
 * Script to import service definitions
 */
@Stateless
public class ServiceImportScript extends Script {

    private static final long serialVersionUID = -3467788957890975696L;

    @Inject
    private CustomTableService customTableService;

    @Inject
    private OfferTemplateService offerTemplateService;

    @Inject
    private ServiceTemplateService serviceTemplateService;

    @Inject
    private OneShotChargeTemplateService oneshotChargeTemplateService;

    @Inject
    private RecurringChargeTemplateService recurringChargeTemplateService;

    @Inject
    private UsageChargeTemplateService usageChargeTemplateService;

    @Inject
    private ServiceChargeTemplateRecurringService serviceChargeTemplateRecurringService;

    @Inject
    private ServiceChargeTemplateSubscriptionService serviceChargeTemplateSubscriptionService;

    @Inject
    private ServiceChargeTemplateTerminationService serviceChargeTemplateTerminationService;

    @Inject
    private ServiceChargeTemplateUsageService serviceChargeTemplateUsageService;

    @Inject
    private CustomEntityInstanceService customEntityInstanceService;

    @Inject
    @ApplicationProvider
    private Provider appProvider;

    private static OneShotChargeTemplate subscriptionCharge;

    private static OneShotChargeTemplate terminationCharge;

    private static RecurringChargeTemplate recurringCharge;

    private static UsageChargeTemplate usageCharge;

    @Override
    public void init(Map<String, Object> methodContext) throws BusinessException {

        subscriptionCharge = oneshotChargeTemplateService.findByCode(ChargeTemplateEnum.CH_ONESHOT_SUBSCRIPTION.name());
        terminationCharge = oneshotChargeTemplateService.findByCode(ChargeTemplateEnum.CH_ONESHOT_TERMINATION.name());
        recurringCharge = recurringChargeTemplateService.findByCode(ChargeTemplateEnum.CH_RECURRING.name());
        usageCharge = usageChargeTemplateService.findByCode(ChargeTemplateEnum.USAGE_CHARGE.name());
    }

    @SuppressWarnings("unchecked")
    @Override
    public void execute(Map<String, Object> initContext) throws BusinessException {

        Map<String, Object> serviceLine = (Map<String, Object>) initContext.get(Script.CONTEXT_RECORD);

        OfferTemplate offer = offerTemplateService.findByCode((String) serviceLine.get("offer_code"));
        if (offer == null) {
            throw new BusinessException("Offer '" + serviceLine.get("offer_code") + "' not found");
        }

        ServiceTemplate serviceTemplate = serviceTemplateService.findByCode((String) serviceLine.get("service_code"));
        boolean serviceCreate = serviceTemplate == null;

        if (serviceCreate) {
            serviceTemplate = new ServiceTemplate();
            serviceTemplate.setCode((String) serviceLine.get("service_code"));
        }

        serviceTemplate.setDescription((String) serviceLine.get("service_description"));

        String chargeTypes = (String) serviceLine.get("charge_types");

        if (!StringUtils.isBlank((String) serviceLine.get("service_type"))) {

            if (ServiceTypeEnum.MAIN.getLabel().equalsIgnoreCase((String) serviceLine.get("service_type"))) {
                serviceTemplate.setCfValue(ServiceTemplateCFsEnum.SERVICE_TYPE.name(), ServiceTypeEnum.MAIN.getLabel());
                chargeTypes = chargeTypes + "U";
            } else if (ServiceTypeEnum.ANCILLARY.getLabel().equalsIgnoreCase((String) serviceLine.get("service_type"))) {
                serviceTemplate.setCfValue(ServiceTemplateCFsEnum.SERVICE_TYPE.name(), ServiceTypeEnum.ANCILLARY.getLabel());
            } else if (ServiceTypeEnum.EVENT.getLabel().equalsIgnoreCase((String) serviceLine.get("service_type"))) {
                serviceTemplate.setCfValue(ServiceTemplateCFsEnum.SERVICE_TYPE.name(), ServiceTypeEnum.EVENT.getLabel());
            } else {
                throw new BusinessException("Service type '" + serviceLine.get("service_type") + "' not found. Valid values are Main, Ancillary and Event");
            }
        }

        if (serviceLine.get("max_quantity") != null) {
            serviceTemplate.setCfValue(ServiceTemplateCFsEnum.MAX_QUANTITY.name(), serviceLine.get("max_quantity"));
        }

        if (!StringUtils.isBlank((String) serviceLine.get("migration_code"))) {
            serviceTemplate.setCfValue(ServiceTemplateCFsEnum.MIGRATION_CODE.name(), (String) serviceLine.get("migration_code"));
        }

        if (!StringUtils.isBlank((String) serviceLine.get("product_set"))) {
            Integer id = (Integer) customTableService.getValue(CustomTableEnum.PRODUCT_SET.getCode(), "id", MapUtils.putAll(new HashMap(), new Object[] { "code", serviceLine.get("product_set") }));
            if (id != null) {
                serviceTemplate.setCfValue(ServiceTemplateCFsEnum.PRODUCT_SET.name(), new EntityReferenceWrapper(CustomEntityInstance.class.getName(), CustomTableEnum.PRODUCT_SET.getCode(), id.toString()));
            } else {
                throw new BusinessException("Product set '" + serviceLine.get("product_set") + "' not found");
            }
        } else {
            // throw new BusinessException("Product set not provided for '" + serviceLine.get("service_code") + "' service");
        }

        if (!StringUtils.isBlank((String) serviceLine.get("subscriber_prefix"))) {

            CustomEntityInstance subPrefix = customEntityInstanceService.findByCodeByCet(CustomTableEnum.SUBSCRIBER_PREFIX.getCode(), (String) serviceLine.get("subscriber_prefix"));
            if (subPrefix != null) {
                serviceTemplate.setCfValue(ServiceTemplateCFsEnum.SUBSCRIBER_PREFIX.name(), new EntityReferenceWrapper(subPrefix));
                // serviceTemplate.setCfValue(ServiceTemplateCFsEnum.SUBSCRIBER_PREFIX.name(), new EntityReferenceWrapper(CustomEntityInstance.class.getName(),
                // CustomTableEnum.SUBSCRIBER_PREFIX.getCode(), id.toString()));
            } else {
                throw new BusinessException("Subscriber prefix '" + serviceLine.get("subscriber_prefix") + "' not found");
            }
            serviceTemplate.setCfValue(ServiceTemplateCFsEnum.SUBSCRIBER_PREFIX.name(),
                new EntityReferenceWrapper(CustomEntityInstance.class.getName(), CustomTableEnum.SUBSCRIBER_PREFIX.getCode(), (String) serviceLine.get("subscriber_prefix")));
        } else {
//            throw new BusinessException("Subscriber prefix not provided for '" + serviceLine.get("service_code") + "' service");
        }

        if (!StringUtils.isBlank((String) serviceLine.get("apply_in_advance"))) {
            serviceTemplate.setCfValue(ServiceTemplateCFsEnum.APPLY_IN_ADVANCE.name(), "yes".equalsIgnoreCase((String) serviceLine.get("apply_in_advance")) ? "Yes" : "No");

        } else {
            serviceTemplate.setCfValue(ServiceTemplateCFsEnum.APPLY_IN_ADVANCE.name(), "No");
        }

        if (serviceLine.get("max_instances") != null) {
            serviceTemplate.setCfValue(ServiceTemplateCFsEnum.MAX_INSTANCES.name(), serviceLine.get("max_instances"));
        }

        if (serviceLine.get("multiple_instances") != null) {
            serviceTemplate.setCfValue(ServiceTemplateCFsEnum.MULTIPLE_INSTANCES.name(), Boolean.valueOf((String) serviceLine.get("multiple_instances")));
        }

        if (!StringUtils.isBlank((String) serviceLine.get("multiple_instance_unique_attributes"))) {

            ServiceInstanceIdentifierAttributesCFsEnum serviceUniqueAttribute = ServiceInstanceIdentifierAttributesCFsEnum.valueOfIgnoreCase((String) serviceLine.get("multiple_instance_unique_attributes"));

            if (serviceUniqueAttribute != ServiceInstanceIdentifierAttributesCFsEnum.COMPONENT_ID && serviceUniqueAttribute != ServiceInstanceIdentifierAttributesCFsEnum.RANGE_START
                    && serviceUniqueAttribute != ServiceInstanceIdentifierAttributesCFsEnum.RANGE_START_RANGE_END) {
                throw new BusinessException(
                    "Service unique Identifiers '" + serviceLine.get("multiple_instance_unique_attributes") + "' not found. Valid values are <COMPONENT_ID>, <RANGE_START> and <RANGE_START>;<RANGE_END>");
            }

            serviceTemplate.setCfValue(ServiceTemplateCFsEnum.MULTIPLE_INSTANCE_UNIQUE_ATTRIBUTES.name(), serviceLine.get("multiple_instance_unique_attributes"));
        }

        if (serviceCreate) {
            serviceTemplateService.create(serviceTemplate);
        } else {
            serviceTemplate = serviceTemplateService.update(serviceTemplate);
        }

        List<OfferServiceTemplate> offerServiceTemplates = serviceTemplateService.getOfferServiceTemplates(serviceTemplate);
        boolean offerServiceTemplateExist = false;
        if (offerServiceTemplates != null && !offerServiceTemplates.isEmpty()) {
            for (OfferServiceTemplate offerServiceTemplate : offerServiceTemplates) {
                if (offer.equals(offerServiceTemplate.getOfferTemplate())) {
                    offerServiceTemplateExist = true;
                    break;
                }
            }
        }

        if (!offerServiceTemplateExist) {
            OfferServiceTemplate ost = new OfferServiceTemplate();
            ost.setOfferTemplate(offer);
            ost.setServiceTemplate(serviceTemplate);
            serviceTemplateService.getEntityManager().persist(ost);
        }

        if (chargeTypes.contains(ChargeTypeEnum.O.name())) {
            ServiceChargeTemplateSubscription serviceChargeTemplate = null;
            if (!serviceCreate) {
                serviceChargeTemplate = serviceTemplate.getServiceChargeTemplateSubscriptionByChargeCode(ChargeTemplateEnum.CH_ONESHOT_SUBSCRIPTION.name());
            }

            if (serviceChargeTemplate == null) {
                serviceChargeTemplate = new ServiceChargeTemplateSubscription();
                serviceChargeTemplate.setChargeTemplate(subscriptionCharge);
                serviceChargeTemplate.setServiceTemplate(serviceTemplate);
                serviceChargeTemplateSubscriptionService.create(serviceChargeTemplate);
            }
        }

        if (chargeTypes.contains(ChargeTypeEnum.C.name())) {
            ServiceChargeTemplateTermination serviceChargeTemplate = null;
            if (!serviceCreate) {
                serviceChargeTemplate = serviceTemplate.getServiceChargeTemplateTerminationByChargeCode(ChargeTemplateEnum.CH_ONESHOT_TERMINATION.name());
            }

            if (serviceChargeTemplate == null) {
                serviceChargeTemplate = new ServiceChargeTemplateTermination();
                serviceChargeTemplate.setChargeTemplate(terminationCharge);
                serviceChargeTemplate.setServiceTemplate(serviceTemplate);
                serviceChargeTemplateTerminationService.create(serviceChargeTemplate);
            }
        }

        if (chargeTypes.contains(ChargeTypeEnum.R.name())) {
            ServiceChargeTemplateRecurring serviceChargeTemplate = null;
            if (!serviceCreate) {
                serviceChargeTemplate = serviceTemplate.getServiceRecurringChargeByChargeCode(ChargeTemplateEnum.CH_RECURRING.name());
            }

            if (serviceChargeTemplate == null) {
                serviceChargeTemplate = new ServiceChargeTemplateRecurring();
                serviceChargeTemplate.setChargeTemplate(recurringCharge);
                serviceChargeTemplate.setServiceTemplate(serviceTemplate);
                serviceChargeTemplateRecurringService.create(serviceChargeTemplate);
            }
        }

        if (chargeTypes.contains(ChargeTypeEnum.U.name())) {
            ServiceChargeTemplateUsage serviceChargeTemplate = null;
            if (!serviceCreate) {
                serviceChargeTemplate = serviceTemplate.getServiceChargeTemplateUsageByChargeCode(ChargeTemplateEnum.USAGE_CHARGE.name());
            }

            if (serviceChargeTemplate == null) {
                serviceChargeTemplate = new ServiceChargeTemplateUsage();
                serviceChargeTemplate.setChargeTemplate(usageCharge);
                serviceChargeTemplate.setServiceTemplate(serviceTemplate);
                serviceChargeTemplateUsageService.create(serviceChargeTemplate);
            }
        }
    }
}