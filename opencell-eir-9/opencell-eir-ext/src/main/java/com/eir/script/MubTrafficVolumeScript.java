package com.eir.script;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.ParamBeanFactory;
import org.meveo.commons.utils.QueryBuilder;
import org.meveo.model.IEnable;
import org.meveo.model.billing.InstanceStatusEnum;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.crm.custom.CustomFieldValue;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.script.Script;

import com.eir.commons.enums.customfields.ServiceInstanceCFsEnum;
import com.eir.commons.enums.customfields.ServiceParametersCFEnum;
import com.eir.service.commons.UtilService;

/**
 * The Class MubTrafficVolumeScript. PO2-288
 */
@Stateless
public class MubTrafficVolumeScript extends Script {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;

    /**
     * The param bean factory.
     */
    @Inject
    private ParamBeanFactory paramBeanFactory;

    /**
     * The subscription service.
     */
    @Inject
    private SubscriptionService subscriptionService;
    
    /**
     * util service
     */
    @Inject
    protected UtilService utilService;

    /**
     * The Constant DATETIME_FORMAT.
     */
    private static final String DATETIME_FORMAT = "ddMMyyyyHHmmss";

    /**
     * The NumberFormat.
     */
    private NumberFormat fmt = NumberFormat.getInstance(Locale.ENGLISH);

    /**
     * script main execute method.
     *
     * @param methodContext method context params
     * @throws BusinessException business exception
     */
    @Override
    public void execute(Map<String, Object> methodContext) throws BusinessException {
        log.debug("########## Starting Mub traffic volume script ... ");
        fmt.setGroupingUsed(false);
        fmt.setMaximumIntegerDigits(999);
        fmt.setMaximumFractionDigits(999);
        try {
            List<Subscription> subscriptions = findMubCircuitIds();
            if (subscriptions != null) {
                long startEpocTime = getMubStartEpocTime();
                long endEpocTime = getMubEndEpocTime();
                String mubMounth = getMubMounth();
                // Date endEpocDate = new Date(endEpocTime);
                Double trafficVolume;
                String trafficFolder = (String) methodContext.get("TRAFFIC_VOLUME_FOLDER");
                if (trafficFolder == null) {
                    trafficFolder = "imports/MUB/traffic/input";
                }
                trafficFolder = paramBeanFactory.getChrootDir() + File.separator + trafficFolder;
                File f = new File(trafficFolder);
                if (!f.exists()) {
                    f.mkdirs();
                    log.debug("Traffic folder created :" + trafficFolder);
                }
                String trafficFileName = trafficFolder + File.separator + "Traffic_Volume_" + mubMounth + "_" + DateUtils.formatDateWithPattern(new Date(), DATETIME_FORMAT)
                        + ".csv";
                String separator = ",";
                FileWriter fw = null;
                BufferedWriter bw = null;
                // Subscription subscription;
                for (Subscription subscription : subscriptions) {
                    Map<String, String> cfMubFinalUsage = (Map<String, String>) subscription.getCfValuesNullSafe().getValue("MUB_FINAL_USAGE");
                    trafficVolume = getTrafficVolume(cfMubFinalUsage, startEpocTime, endEpocTime);
                    if (trafficVolume != null) {
                        if (!(new File(trafficFileName)).exists()) {
                            fw = new FileWriter(trafficFileName, true);
                            bw = new BufferedWriter(fw);
                        }
                        bw.append(subscription.getUserAccount().getBillingAccount().getCode() + separator + subscription.getCode() + separator + fmt.format(trafficVolume)
                                + separator + getBillCode(subscription) + separator + getRegion(subscription) + separator + mubMounth + "\r\n");
                    }
                }
                if (bw != null) {
                    bw.close();
                }
            }

        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BusinessException(e.getMessage());
        }

    }

    /**
     * Gets the region.
     *
     * @param subscription the subscription
     * @return the region
     */
    private String getRegion(Subscription subscription) {
        
        for (ServiceInstance service : subscription.getServiceInstances()) {
        	CustomFieldValue lastCfValue = utilService.getLastCfVersion(service, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(),
        			subscription.getSubscriptionDate(), true);
        	Map<String, String> serviceParametersCF = getServiceParameters(service, lastCfValue);
            if (serviceParametersCF != null && serviceParametersCF.get(ServiceParametersCFEnum.REGION.getLabel()) != null) {
                return serviceParametersCF.get(ServiceParametersCFEnum.REGION.getLabel());
            }
        }
        return null;
    }

    /**
     * Gets the bill code.
     *
     * @param subscription the subscription
     * @return the bill code
     */
    private String getBillCode(Subscription subscription) {
        for (ServiceInstance service : subscription.getServiceInstances()) {
            if (isMain(service) && InstanceStatusEnum.ACTIVE.equals(service.getStatus())) {
                return service.getCode();
            }
        }
        return null;
    }
    
    private boolean isMain(ServiceInstance service) {
        if (service.getServiceTemplate() != null
                && "Main".equals(service.getServiceTemplate().getCfValue("SERVICE_TYPE"))) {
            return true;
        }
        return false;
    }

    /**
     * Gets the traffic volume.
     *
     * @param cfMubFinaldUsage the cf mub finald usage
     * @param startEpocTime    the start epoc time
     * @param endEpocTime      the end epoc time
     * @return the traffic volume
     */
    private Double getTrafficVolume(Map<String, String> cfMubFinaldUsage, long startEpocTime, long endEpocTime) {
        if (cfMubFinaldUsage == null || cfMubFinaldUsage.size() == 0) {
            return null;
        }
        
        // filter the map to have only values between start and end epoc time
        Map<String, Double> collect = cfMubFinaldUsage.entrySet().stream().filter(map -> (Long.valueOf(map.getKey()) >= startEpocTime && Long.valueOf(map.getKey()) <= endEpocTime))
                .collect(Collectors.toMap(p -> p.getKey(), (p -> Double.valueOf((p.getValue().split(Pattern.quote("|"))[6]).replace(",", ".")))));

        if (collect != null && collect.size() > 0) {
            return collect.values().stream().mapToDouble(Double::valueOf).sum();
        }
        return null;
    }

    /**
     * Find mub circuit ids.
     *
     * @return the list of subscriptions that contain MUB_FINAL_USAGE as CF
     */
    @SuppressWarnings("unchecked")
    private List<Subscription> findMubCircuitIds() {
        QueryBuilder queryBuilder = new QueryBuilder(Subscription.class, "a", null);
        if (IEnable.class.isAssignableFrom(Subscription.class)) {
            queryBuilder.addBooleanCriterion("disabled", false);
        }
        queryBuilder.addCriterion("cf_values", "like", "%MUB_FINAL_USAGE%", true);
        return queryBuilder.getQuery(subscriptionService.getEntityManager()).getResultList();
    }

    /**
     * Gets the mub start date.
     *
     * @return the mub start date
     */
    private long getMubStartEpocTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
        calendar.set(Calendar.DATE, 1);
        return DateUtils.setDateToStartOfDay(calendar.getTime()).getTime() / 1000;
    }

    /**
     * Gets the mub end date.
     *
     * @return the mub end date
     */
    private long getMubEndEpocTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return DateUtils.setDateToEndOfDay(calendar.getTime()).getTime() / 1000;
    }

    private static String getMubMounth() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        int mounth = calendar.get(Calendar.MONTH);
        int year = calendar.get(Calendar.YEAR);
        if(mounth == 0) {
            mounth = 12;
            year = year - 1;
        }
        String sMounth = mounth < 10 ? "0" + mounth : "" + mounth;
        return String.valueOf(year) + sMounth;
    }
    
    /**
     * Get service parameters
     *
     * @param serviceInstance the service instance
     * @param serviceInstance the service instance
     * @return the service parameters CF
     */
    protected Map<String, String> getServiceParameters(ServiceInstance serviceInstance, CustomFieldValue lastCfValue) {
        Map<String, String> serviceParametersCF = null;
        if (serviceInstance != null && lastCfValue != null && lastCfValue.getMapValue() != null && !lastCfValue.getMapValue().isEmpty()) {
            serviceParametersCF = (Map<String, String>) utilService.getCFValue(serviceInstance, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), (String) lastCfValue.getMapValue().values().iterator().next());
        }
        return serviceParametersCF;
    }

}
