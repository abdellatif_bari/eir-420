package com.eir.script;

import com.eir.service.commons.UtilService;
import org.meveo.admin.exception.BusinessException;
import org.meveo.model.admin.Seller;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.UserAccount;
import org.meveo.model.billing.WalletInstance;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.WalletTemplate;
import org.meveo.security.CurrentUser;
import org.meveo.security.MeveoUser;
import org.meveo.service.admin.impl.SellerService;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.billing.impl.UserAccountService;
import org.meveo.service.catalog.impl.OfferTemplateService;
import org.meveo.service.script.Script;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Map;

/**
 * EIR Create Misc Account Script
 *
 * @author Mohammed Amine TAZI
 */
@Stateless
public class CreateMiscAccountScript extends Script {

    private static final long serialVersionUID = -1482912298340196191L;

    @Inject
    private SubscriptionService subscriptionService;

    @Inject
    private SellerService sellerService;

    @Inject
    private UserAccountService userAccountService;

    @Inject
    private OfferTemplateService offerTemplateService;

    @Inject
    private UtilService utilService;

    @Inject
    @CurrentUser
    protected MeveoUser currentUser;

    private static final String PREFIX = "MISC_";

    // Global variables

    @Override
    public void execute(Map<String, Object> initContext) throws BusinessException {

        BillingAccount ba = (BillingAccount) initContext.get(Script.CONTEXT_ENTITY);
        if (ba == null) {
            throw new BusinessException("Billing Account not found");
        }

        String offerCode = (String) initContext.get("offerCode");
        String sellerCode = (String) initContext.get("sellerCode");

        UserAccount ua = createUserAccount(PREFIX + ba.getCode(), ba);

        createSubscription(PREFIX + ba.getCode(), ua, offerCode, sellerCode);

    }

    private UserAccount createUserAccount(String code, BillingAccount ba) throws BusinessException {
        UserAccount userAccount = userAccountService.findByCode(code);

        if (userAccount != null) {
            return userAccount;
        }

        userAccount = new UserAccount();
        userAccount.setCode(code);
        userAccount.setBillingAccount(ba);

        WalletInstance wallet = new WalletInstance();
        wallet.setCode(WalletTemplate.PRINCIPAL);
        wallet.setUserAccount(userAccount);
        wallet.updateAudit(currentUser);

        userAccount.setWallet(wallet);

        userAccountService.create(userAccount);

        log.debug("New user account created : " + code);

        return userAccount;
    }

    private void createSubscription(String code, UserAccount ua, String offerCode, String sellerCode) throws BusinessException {
        if (subscriptionService.findByCode(code) != null) {
            return;
        }

        OfferTemplate offer = offerTemplateService.findByCode(offerCode);
        if (offer == null) {
            throw new BusinessException("Offer template '" + offerCode + "' not found");
        }

        Seller seller = sellerService.findByCode(sellerCode);
        if (seller == null) {
            throw new BusinessException("Seller '" + sellerCode + "' not found");
        }

        Subscription sub = new Subscription();
        sub.setCode(code);
        sub.setUserAccount(ua);
        sub.setOffer(offer);
        sub.setSeller(seller);
        utilService.setServiceIdCF(sub);
        subscriptionService.create(sub);
        log.debug("New Subscription created : " + code);
    }
}