package com.eir.script;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.Calendar;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.ParamBeanFactory;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.script.Script;

/**
 * The Class Mub95thPercentileCalulationScript.
 */
@Stateless
public class Mub95thPercentileCalculationScript extends Script {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    /** The param bean factory. */
    @Inject
    private ParamBeanFactory paramBeanFactory;
    /** The billing account service. */
    @Inject
    private BillingAccountService billingAccountService;

    /** The Constant DATETIME_FORMAT. */
    private static final String DATETIME_FORMAT = "ddMMyyyyHHmmss";

    /**
     * script main execute method.
     *
     * @param methodContext method context params
     * @throws BusinessException business exception
     */
    @Override
    public void execute(Map<String, Object> methodContext) throws BusinessException {
        log.debug("########## Starting Calculating 95th percentile ... ");
        int jobResultNbOK = 0;
        try {
            List<BillingAccount> billingAccounts = findMubBillingAccounts();
            if (billingAccounts != null) {
                long startEpocTime = getMubStartEpocTime();
                long endEpocTime = getMubEndEpocTime();
                // Date startEpocDate = new Date(startEpocTime);
                // Date endEpocDate = new Date(endEpocTime);
                String keyPercentile;
                String valuePercentile;
                String[] valuePercentileTab;
                String mubDateTime;
                String mubDate;
                String mubTime;
                String percentileFolder = (String) methodContext.get("PERCENTILE_FOLDER");
                if (percentileFolder == null) {
                    percentileFolder = "imports/MUB/percentile/input";
                }
                percentileFolder = paramBeanFactory.getChrootDir() + File.separator + percentileFolder;
                File f = new File(percentileFolder);
                if (!f.exists()) {
                    f.mkdirs();
                }
                String percentileFileName = percentileFolder + File.separator + "Billing Account_" + DateUtils.formatDateWithPattern(new Date(), DATETIME_FORMAT) + ".csv";
                String separator = ",";
                FileWriter fw = null;
                BufferedWriter bw = null;
                for (BillingAccount ba : billingAccounts) {
                    Map<String, String> cfMubAggregatedUsage = (Map<String, String>) ba.getCfValuesNullSafe().getValue("MUB_AGGREGATED_FINAL_USAGE");
                    keyPercentile = getKeyPercentile(cfMubAggregatedUsage, startEpocTime, endEpocTime);
                    if (keyPercentile != null) {
                        valuePercentile = cfMubAggregatedUsage.get(keyPercentile);
                        valuePercentileTab = valuePercentile.split(Pattern.quote("|"));
                        mubDateTime = DateUtils.formatDateWithPattern(new Date(Long.valueOf(keyPercentile) * 1000), "dd-MM-yyyy HH:mm");
                        mubDate = mubDateTime.split(" ")[0];
                        mubTime = mubDateTime.split(" ")[1];
                        if (!(new File(percentileFileName)).exists()) {
                            fw = new FileWriter(percentileFileName, true);
                            bw = new BufferedWriter(fw);
                        }
                        bw.append(ba.getCode() + separator + mubDate + separator + mubTime + separator + valuePercentileTab[0] + separator + valuePercentileTab[2] + separator
                                + valuePercentileTab[1] + "\r\n");
                    }
                }
                if (bw != null) {
                    bw.close();
                    jobResultNbOK = 1;
                }
            }
            methodContext.put(Script.JOB_RESULT_NB_OK,jobResultNbOK);

        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BusinessException(e.getMessage());
        }

    }

    /**
     * Gets the percentile.
     *
     * @param cfMubAggregatedUsage the cf mub aggregated usage
     * @param startEpocTime the start epoc time
     * @param endEpocTime the end epoc time
     * @return the percentile
     */
    private String getKeyPercentile(Map<String, String> cfMubAggregatedUsage, long startEpocTime, long endEpocTime) {
        if (cfMubAggregatedUsage == null || cfMubAggregatedUsage.size() == 0) {
            return null;
        }
        // filter the map to have only values between start and end epoc time
        Map<String, Double> collect = cfMubAggregatedUsage.entrySet().stream()
            .filter(map -> (Long.valueOf(map.getKey()) >= startEpocTime && Long.valueOf(map.getKey()) <= endEpocTime))
            .collect(Collectors.toMap(p -> p.getKey(), (p -> Double.valueOf((p.getValue().split(Pattern.quote("|"))[0])))));
        // sort the result to facilitate the calculation of 95th percentile
        Map<String, Double> result = collect.entrySet().stream().sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
            .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (oldValue, newValue) -> oldValue, LinkedHashMap::new));
        if (result != null && result.size() > 0) {
            int percentilePosition = 0; // if we have only one record, we take this record as 95th percentile.
            if (result.size() > 1) {
                percentilePosition = (int) (result.size() * 0.05) + 1;
            }
            return (String) result.keySet().toArray()[percentilePosition];
        }
        return null;
    }

    
    private List<BillingAccount> findMubBillingAccounts() {        
        String sql = "SELECT b FROM BillingAccount b where varcharFromJson(b.cfValues, MUB_AGGREGATED_FINAL_USAGE, mapString) is not null";        
        //String query = "select id from account_entity where account_type = 'ACCT_BA' and cf_values like '%MUB_AGGREGATED_FINAL_USAGE%'";        
        return billingAccountService.getEntityManager().createQuery(sql, BillingAccount.class).getResultList();
    }

    /**
     * Gets the mub start date.
     *
     * @return the mub start date
     */
    private static long getMubStartEpocTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
        calendar.set(Calendar.DATE, 1);
        return DateUtils.setDateToStartOfDay(calendar.getTime()).getTime() / 1000;
    }

    /**
     * Gets the mub end date.
     *
     * @return the mub end date
     */
    private static long getMubEndEpocTime() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
        calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
        return DateUtils.setDateToEndOfDay(calendar.getTime()).getTime() / 1000;
    }

}
