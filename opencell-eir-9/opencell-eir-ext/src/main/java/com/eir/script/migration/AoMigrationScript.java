package com.eir.script.migration;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ElementNotFoundException;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.payments.AccountOperation;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.model.payments.MatchingStatusEnum;
import org.meveo.model.payments.OCCTemplate;
import org.meveo.model.payments.OperationCategoryEnum;
import org.meveo.model.payments.OtherCreditAndCharge;
import org.meveo.model.payments.Payment;
import org.meveo.model.payments.RecordedInvoice;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.payments.impl.AccountOperationService;
import org.meveo.service.payments.impl.CustomerAccountService;
import org.meveo.service.payments.impl.OCCTemplateService;
import org.meveo.service.script.Script;

import com.eir.commons.enums.ApplicationPropertiesEnum;

/**
 * Script for migrating EIR open account operations to OC
 * 
 * @author Andrius Karpavicius
 */
@Stateless
public class AoMigrationScript extends Script {

    private static final long serialVersionUID = -6288568811084129325L;

    @Inject
    private CustomerAccountService customerAccountService;

    @Inject
    private BillingAccountService billingAccountService;

    @Inject
    private AccountOperationService accountOperationService;

    @Inject
    private OCCTemplateService occTemplateService;

    @Override
    public void execute(Map<String, Object> initContext) {
        try {
            @SuppressWarnings("unchecked")
            Map<String, Object> record = (Map<String, Object>) initContext.get("record");

            if (MapUtils.isEmpty(record)) {
                throw new BusinessException("Empty AO record!");
            }

            String caCode = (String) record.get("CA_CODE");
            String baCode = (String) record.get("BA_CODE");
            String baNumber = (String) record.get("BA_NUMBER");
            String type = (String) record.get("TYPE");
            String transDate = (String) record.get("TRANS_DATE");
            String ref = (String) record.get("REF");
            String dueDate = (String) record.get("DUE_DATE");
            String debit = (String) record.get("DEBIT");
            String credit = (String) record.get("CREDIT");
            String status = ((String) record.get("STATUS")).trim();

            // Customer
            CustomerAccount ca = customerAccountService.findByCode(caCode);
            if (ca == null) {
                throw new ElementNotFoundException(caCode, "Customer account");
            }
            BillingAccount ba = billingAccountService.findByCode(baCode);
            if (ba == null) {
                throw new ElementNotFoundException(baCode, "Billing account");
            }

            AccountOperation ao = null;

            String occCode = null;

            if ("Invoice".equalsIgnoreCase(type)) {
                ao = new RecordedInvoice();
                occCode = !StringUtils.isBlank(debit) ? "INV_STD" : "INV_CRN";

            } else if ("Adjustment".equalsIgnoreCase(type)) {
                ao = new RecordedInvoice();
                occCode = !StringUtils.isBlank(debit) ? "ADJ_001" : "ADJ_002";

            } else if ("Payment".equalsIgnoreCase(type)) {
                ao = new Payment();
                occCode = !StringUtils.isBlank(debit) ? ApplicationPropertiesEnum.TEMPLATE_REVERSE_PAYMENT_DEBIT.getProperty() : ApplicationPropertiesEnum.TEMPLATE_BATCH_PAYMENT_CREDIT.getProperty();

            } else if ("Transfer".equalsIgnoreCase(type)) {
                ao = new OtherCreditAndCharge();
                occCode = !StringUtils.isBlank(debit) ? ApplicationPropertiesEnum.TEMPLATE_TRANSFER_ACCOUNT_DEBIT.getProperty() : ApplicationPropertiesEnum.TEMPLATE_TRANSFER_ACCOUNT_CREDIT.getProperty();

            } else {
                throw new ElementNotFoundException(type, "Account operation type");
            }

            OCCTemplate occTemplate = occTemplateService.findByCode(occCode);
            if (occTemplate == null) {
                throw new ElementNotFoundException(occCode, "occCode template");
            }

            ao.setCustomerAccount(ca);
            ao.setCode(occCode);
            ao.setDescription(occTemplate.getDescription());

            if (!StringUtils.isBlank(dueDate)) {
                ao.setDueDate(DateUtils.parseDateWithPattern(dueDate, "yyyyMMdd"));
            } else {
                ao.setDueDate(new Date());
            }
            if (!StringUtils.isBlank(transDate)) {
                ao.setTransactionDate(DateUtils.parseDateWithPattern(transDate, "yyyyMMdd"));
            }
            if (!StringUtils.isBlank(baNumber)) {
                ao.setCfValue("BILLING_ACCOUNT_NUMBER", baNumber);
            }
            if (!StringUtils.isBlank(ref)) {
                ao.setReference(ref);
            }

            if (!StringUtils.isBlank(debit)) {
                ao.setTransactionCategory(OperationCategoryEnum.DEBIT);
                ao.setAmount(new BigDecimal(debit.trim()).abs());
                ao.setUnMatchingAmount(ao.getAmount());
            } else if (!StringUtils.isBlank(credit)) {
                ao.setTransactionCategory(OperationCategoryEnum.CREDIT);
                ao.setAmount(new BigDecimal(credit.trim()).abs());
                ao.setUnMatchingAmount(ao.getAmount());
            } else {
                throw new ElementNotFoundException((String) null, "Unrecognized if debit or credit");
            }
            if ("Open".equalsIgnoreCase(status)) {
                ao.setMatchingStatus(MatchingStatusEnum.O);
            } else if ("Partial".equalsIgnoreCase(status)) {
                ao.setMatchingStatus(MatchingStatusEnum.P);
            } else {
                throw new ElementNotFoundException(status, "Account operation status");
            }
            accountOperationService.create(ao);

        } catch (Exception e) {
            log.error("Error adding account operation", e);
            if (e instanceof BusinessException) {
                throw e;
            } else {
                // wrap the exception in a business exception and throwing it
                throw new BusinessException(e);
            }
        }
    }
}