package com.eir.script.migration;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.collections.MapUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ElementNotFoundException;
import org.meveo.model.billing.Country;
import org.meveo.model.billing.TradingCurrency;
import org.meveo.model.billing.TradingLanguage;
import org.meveo.model.crm.Customer;
import org.meveo.model.payments.CheckPaymentMethod;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.model.payments.PaymentMethod;
import org.meveo.model.payments.PaymentMethodEnum;
import org.meveo.model.shared.Address;
import org.meveo.service.admin.impl.CountryService;
import org.meveo.service.admin.impl.TradingCurrencyService;
import org.meveo.service.billing.impl.TradingLanguageService;
import org.meveo.service.crm.impl.CustomerService;
import org.meveo.service.payments.impl.CustomerAccountService;
import org.meveo.service.script.Script;

/**
 * Script for migrating EIR customer accounts to OC
 * @author Hatim OUDAD
 */
@Stateless
public class CustomerAccountsMigrationScript extends Script {

    private static final long serialVersionUID = -6288568811084129325L;

    @Inject
    private CustomerAccountService customerAccountService;
    
    @Inject
    private CustomerService customerService;
    
    @Inject
    private CountryService countryService;
    
    @Inject
    private TradingCurrencyService tradingCurrencyService;
    
    @Inject
    private TradingLanguageService tradingLanguageService;

    
    @Override
    public void execute(Map<String, Object> initContext) {
        try {
            @SuppressWarnings("unchecked")
            Map<String, Object> record = (Map<String, Object>) initContext.get("record");
           
            if(MapUtils.isEmpty(record)) {
                throw new BusinessException("Empty customer record!");
            }
            
            String customerCode = (String) record.get("CUSTOMER_CODE");
            String customerAccountCode = (String) record.get("CUSTOMER_ACCOUNT_CODE");
            String customerAccountName = (String) record.get("CUSTOMER_ACCOUNT_NAME");
            String address1 = (String) record.get("ADDRESS_1");
            String address2 = (String) record.get("ADDRESS_2");
            String address3 = (String) record.get("ADDRESS_3");
            String city = (String) record.get("ADDRESS_CITY");
            String zipCode = (String) record.get("ADDRESS_ZIPCODE");
            String countryName = (String) record.get("ADDRESS_COUNTRY");
            
            if(customerAccountService.findByCode(customerAccountCode) != null) {
            	throw new BusinessException("Customer Account code already exists : " + customerAccountCode);
            }
            
           
            //Customer
            Customer customer = customerService.findByCode(customerCode);
            if(customer == null) {
                throw new ElementNotFoundException(customerCode, "customer");
            }
            CustomerAccount customerAccount = new CustomerAccount();
            customerAccount.setCode(customerAccountCode);
            customerAccount.setCustomer(customer);
            customerAccount.setDescription(customerAccountName);
            
            //Address
            Address address= new Address();
            address.setAddress1(address1);
            address.setAddress2(address2);
            address.setAddress3(address3);
            address.setCity(city);
            address.setZipCode(zipCode);
            Country country= countryService.findByName(countryName);
            if(country == null) {
                throw new ElementNotFoundException(countryName, "country");
            }
            address.setCountry(country);
            customerAccount.setAddress(address);
            
            // Payment Method 
            List<PaymentMethod> paymentMethods = new ArrayList<PaymentMethod>();
            customerAccount.setPaymentMethods(paymentMethods);
            CheckPaymentMethod checkPaymentMethod = new CheckPaymentMethod();
            checkPaymentMethod.setPaymentType(PaymentMethodEnum.CHECK);
            checkPaymentMethod.setCustomerAccount(customerAccount);
            checkPaymentMethod.setPreferred(true);
            paymentMethods.add(checkPaymentMethod);
            customerAccount.setPaymentMethods(paymentMethods);
            
            //Currency
            TradingCurrency tradingCurrency= tradingCurrencyService.findByTradingCurrencyCode("EUR");
            if(tradingCurrency == null) {
            	throw new ElementNotFoundException("EUR", "trading currency");
            }
            customerAccount.setTradingCurrency(tradingCurrency);
            
            //Language
            TradingLanguage tradingLanguage= tradingLanguageService.findByTradingLanguageCode("ENG");
            if(tradingLanguage == null) {
            	throw new ElementNotFoundException("ENG", "trading language");
            }
			customerAccount.setTradingLanguage(tradingLanguage);
            
            
            customerAccountService.create(customerAccount);
        } catch (Exception e) {
            log.error("error adding customer account {} ", e.getMessage(), e);
            if (e instanceof BusinessException) {
                throw e;
            } else {
                // wrap the exception in a business exception and throwing it
                throw new BusinessException(e.getMessage());
            }
        }

    }
}