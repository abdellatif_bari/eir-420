package com.eir.script;

import com.eir.service.mediation.impl.AccessService;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.parse.csv.MEVEOCdrParser;
import org.meveo.api.commons.Utils;
import org.meveo.api.dto.billing.CdrRecordDto;
import org.meveo.commons.parsers.RecordContext;
import org.meveo.commons.utils.StringUtils;
import org.meveo.event.qualifier.RejectedCDR;
import org.meveo.model.mediation.Access;
import org.meveo.model.rating.CDR;
import org.meveo.model.rating.CDRStatusEnum;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.medina.impl.ICdrParser;
import org.meveo.service.medina.impl.InvalidAccessException;
import org.meveo.service.medina.impl.InvalidFormatException;

import javax.ejb.Stateless;
import javax.enterprise.event.Event;
import javax.inject.Inject;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author Houssine Znibar
 */
@Stateless
public class SbwlrCdrParser extends MEVEOCdrParser implements ICdrParser {

    @Inject
    private AccessService accessService;

    @Inject
    @RejectedCDR
    private Event<Serializable> rejectededCdrEventProducer;

    @Inject
    private SubscriptionService subscriptionService;

    /**
     * default date time format.
     */
    private static final String DATE_FORMAT = "yyyyMMdd";

    /**
     * default date time format.
     */
    private static final String TIME_FORMAT = "HHmmssSSS";

    @Override
    public CDR parse(Object record) {
        if (record == null) {
            return null;
        }
        CdrRecordDto cdrRecordDto = null;
        CDR cdr = new CDR();

        try {
            if (record instanceof CdrRecordDto) {
                cdrRecordDto = (CdrRecordDto) record;
            } else { // this case should never happen, but it's added to manage the old data
                String line = (String) record;
                try {
                    cdrRecordDto = getCdrRecordDtoFromLine(line);
                } catch (InvalidFormatException e) {
                    throw new BusinessException(e);
                }
            }

            if (cdrRecordDto == null) {
                throw new BusinessException("Parameter record is missing");
            }
            cdr.setSource(RecordContext.serializeRecord(cdrRecordDto));
            cdr.setType(cdrRecordDto.getClass().getName());
            cdrRecordDto.setErrorMessage(new StringBuffer());
            validateItem(cdrRecordDto);
            cdr.setAccessCode(cdrRecordDto.getAccessNumber());
            cdr.setEventDate(cdrRecordDto.getEventDate());
            if (Utils.isInteger(cdrRecordDto.getSequenceNumber())) {
                cdr.setDecimalParam1(new BigDecimal(cdrRecordDto.getSequenceNumber()));
            }
            if (Utils.isInteger(cdrRecordDto.getCallDuration())) {
                cdr.setQuantity(new BigDecimal(cdrRecordDto.getCallDuration()));
            } else if (StringUtils.isNotBlank(cdrRecordDto.getTerminationNumber()) &&
                    (cdrRecordDto.getTerminationNumber().startsWith("*")
                            || cdrRecordDto.getTerminationNumber().startsWith("#")
                            || cdrRecordDto.getTerminationNumber().startsWith("1471"))) {
                cdr.setQuantity(new BigDecimal(0));
            } else {
                cdrRecordDto.getErrorMessage().append("The call duration is empty and the B Number doesn't start with one of these characters [* # 1471] ");
            }
            if (Utils.isInteger(cdrRecordDto.getPulses())) {
                cdr.setDecimalParam2(new BigDecimal(cdrRecordDto.getPulses()));
            }

            cdr.setParameter1(cdrRecordDto.getTerminationNumber());
            cdr.setParameter2(cdrRecordDto.getCallType());
            cdr.setParameter3(cdrRecordDto.getFeeCode());
            cdr.setParameter4(cdrRecordDto.getServiceClass());
            cdr.setParameter5(cdrRecordDto.getNetworkPlanningCode());
            cdr.setParameter6(cdrRecordDto.getOriginatingNumber());
            cdr.setParameter7(cdrRecordDto.getServiceCategory());
            cdr.setParameter8(cdrRecordDto.getClipClirIndicator());
            cdr.setParameter9(cdrRecordDto.getCountryFeeCode());
            cdr.setExtraParameter(cdrRecordDto.getCountryDescription());
            cdr.setStatus(CDRStatusEnum.OPEN);
            if (StringUtils.isNotBlank(cdrRecordDto.getErrorMessage().toString())) {
                cdr.setRejectReason(cdrRecordDto.getErrorMessage().toString());
                cdr.setStatus(CDRStatusEnum.ERROR);
            }
        } catch (BusinessException e) {
            if (record instanceof String) {
                cdr.setRejectReasonException(new InvalidFormatException((String) record, e));
            } else {
                cdr.setRejectReasonException(new InvalidFormatException(((RecordContext) record).getLineContent(), e));
            }

            if (cdrRecordDto != null && StringUtils.isNotBlank(cdrRecordDto.getAccessNumber())) {
                cdr.setAccessCode(cdrRecordDto.getAccessNumber().replaceAll("^0*", ""));
            }

            cdr.setRejectReason(e.getMessage());
            cdr.setStatus(CDRStatusEnum.ERROR);
        }
        return cdr;
    }

    private CdrRecordDto getCdrRecordDtoFromLine(String line) throws InvalidFormatException {
        String[] fields = line.split(",");
        int fieldsLength = fields.length;
        if (fieldsLength == 0) {
            throw new InvalidFormatException(line, "record empty");

        } else if (fieldsLength <= 4) {
            throw new InvalidFormatException(line, "only " + fields.length + " in the record");

        } else {
            CdrRecordDto cdrRecordDto = new CdrRecordDto();
            cdrRecordDto.setProcessIdentifier(fields[0]);
            cdrRecordDto.setAccessNumber(fields[1]);
            cdrRecordDto.setCallDate(fieldsLength <= 2 ? null : fields[2]);
            cdrRecordDto.setCallTime(fieldsLength <= 3 ? null : fields[3]);
            cdrRecordDto.setSequenceNumber(fieldsLength <= 4 ? null : fields[4]);
            cdrRecordDto.setTerminationNumber(fieldsLength <= 5 ? null : fields[5]);
            cdrRecordDto.setCallDuration(fieldsLength <= 6 ? null : fields[6]);
            cdrRecordDto.setPulses(fieldsLength <= 7 ? null : fields[7]);
            cdrRecordDto.setCallType(fieldsLength <= 8 ? null : fields[8]);
            cdrRecordDto.setServiceClass(fieldsLength <= 9 ? null : fields[9]);
            cdrRecordDto.setFeeCode(fieldsLength <= 10 ? null : fields[10]);
            cdrRecordDto.setNetworkPlanningCode(fieldsLength <= 11 ? null : fields[11]);
            cdrRecordDto.setOriginatingNumber(fieldsLength <= 12 ? null : fields[12]);
            cdrRecordDto.setServiceCategory(fieldsLength <= 13 ? null : fields[13]);
            cdrRecordDto.setClipClirIndicator(fieldsLength <= 14 ? null : fields[14]);
            cdrRecordDto.setCountryFeeCode(fieldsLength <= 15 ? null : fields[15]);
            cdrRecordDto.setCountryDescription(fieldsLength <= 16 ? null : fields[16]);


            if (StringUtils.isBlank(cdrRecordDto.getAccessNumber())) {
                throw new BusinessException("The access number is required.");
            }
            setEventDate(cdrRecordDto);
            if (cdrRecordDto.getEventDate() == null) {
                throw new BusinessException("The event date is missing.");
            }
            cdrRecordDto.setAccessNumber(cdrRecordDto.getAccessNumber().replaceAll("^0*", ""));
            /*
             * if (!isNumeric(accessNumber)) { addErrorMessage("The access number is not valid."); return; } // Remove leading zeros from a a_number accessNumber =
             * String.valueOf(Integer.parseInt(accessNumber));
             */
            Access accessPoint = getAccessPoint(cdrRecordDto.getAccessNumber(), cdrRecordDto.getEventDate()).get(0);
            if (accessPoint == null) {
                throw new BusinessException("The access number is not valid.");
            }
            cdrRecordDto.setSubscriptionId(accessPoint.getSubscription().getId());

            return cdrRecordDto;
        }
    }

    /**
     * Get access point
     *
     * @param accessUserId the access user id
     * @param eventDate    the event date
     * @return the access point list
     */
    private List<Access> getAccessPoint(String accessUserId, Date eventDate) {
        List<Access> accessPoints = accessService.getAccess(accessUserId, eventDate);

        if (accessPoints == null || accessPoints.size() == 0) {
            throw new BusinessException("No matching access point " + accessUserId + " was found for the specific period!");
        }

        if (accessPoints.size() > 1) {
            throw new BusinessException("Several access points with user id " + accessUserId + " are active for the specific period!");
        }
        return accessPoints;
    }

    /**
     * Set event date.
     *
     * @param cdrRecordDto the bill account Dto
     */
    private void setEventDate(CdrRecordDto cdrRecordDto) {
        try {
            Date date = DateUtils.parseDateWithPattern(cdrRecordDto.getCallDate(), DATE_FORMAT);
            Date time = DateUtils.parseDateWithPattern(cdrRecordDto.getCallTime(), TIME_FORMAT);

            cdrRecordDto.setEventDate(date);
            if (date != null && time != null) {
                Calendar calendarTime = Calendar.getInstance();
                calendarTime.setTime(time);

                Calendar calendarDate = Calendar.getInstance();
                calendarDate.setTime(date);
                calendarDate.set(Calendar.HOUR_OF_DAY, calendarTime.get(Calendar.HOUR_OF_DAY));
                calendarDate.set(Calendar.MINUTE, calendarTime.get(Calendar.MINUTE));
                calendarDate.set(Calendar.SECOND, calendarTime.get(Calendar.SECOND));
                calendarDate.set(Calendar.MILLISECOND, calendarTime.get(Calendar.MILLISECOND));
                cdrRecordDto.setEventDate(calendarDate.getTime());
            }
        } catch (Exception e) {
            cdrRecordDto.getErrorMessage().append("The event date is not in good format.");
        }
    }

    public List<Access> accessPointLookup(CDR cdr) throws InvalidAccessException {
        if (cdr.getRejectReason() != null) {
            rejectededCdrEventProducer.fire(cdr);
            throw new InvalidAccessException(cdr, cdr.getRejectReason());
        }
        List<Access> accesses = null;
        try {
            accesses = getAccessPoint(cdr.getAccessCode(), cdr.getEventDate());
        } catch (BusinessException e) {
            cdr.setRejectReason(e.getMessage());
            cdr.setStatus(CDRStatusEnum.ERROR);
        }

        if (accesses == null || accesses.size() != 1) {
            rejectededCdrEventProducer.fire(cdr);
            throw new InvalidAccessException(cdr);
        }
        return accesses;
    }

    /**
     * Validate item
     *
     * @param cdrRecordDto the bill account Dto
     * @throws BusinessException the business exception
     */
    private void validateItem(CdrRecordDto cdrRecordDto) throws BusinessException {
        if (StringUtils.isBlank(cdrRecordDto.getFeeCode()) && StringUtils.isBlank(cdrRecordDto.getCallType())
                && StringUtils.isBlank(cdrRecordDto.getServiceCategory()) && StringUtils.isBlank(cdrRecordDto.getTerminationNumber())) {
            cdrRecordDto.getErrorMessage().append("A value must exist in at least one of the (FEE Code, Call Type, Category of Service and b_number prefix) fields configured for a Traffic Class");
        }

        try {
            if (StringUtils.isBlank(cdrRecordDto.getAccessNumber())) {
                cdrRecordDto.getErrorMessage().append("The access number is required.");
            }

            setEventDate(cdrRecordDto);
            if (cdrRecordDto.getEventDate() == null) {
                cdrRecordDto.getErrorMessage().append("The event date is missing.");
                return;
            }

            if (StringUtils.isNotBlank(cdrRecordDto.getAccessNumber())) {
                cdrRecordDto.setAccessNumber(cdrRecordDto.getAccessNumber().replaceAll("^0*", ""));
                Access accessPoint = getAccessPoint(cdrRecordDto.getAccessNumber(), cdrRecordDto.getEventDate()).get(0);
                if (accessPoint == null) {
                    cdrRecordDto.getErrorMessage().append("The access number is not valid.");
                }
                cdrRecordDto.setSubscriptionId(accessPoint.getSubscription().getId());
            }
        } catch (Exception ex) {
            cdrRecordDto.getErrorMessage().append("The access point is not found");
        }
    }
}