package com.eir.script;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.FlushModeType;
import javax.persistence.Query;

import org.hibernate.Session;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.model.billing.AccountingCode;
import org.meveo.model.billing.BillingRun;
import org.meveo.model.billing.ChargeInstance;
import org.meveo.model.billing.InvoiceSubCategory;
import org.meveo.model.billing.WalletOperation;
import org.meveo.model.billing.WalletOperationStatusEnum;
import org.meveo.model.catalog.OneShotChargeTemplate;
import org.meveo.service.billing.impl.AccountingCodeService;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.billing.impl.BillingRunService;
import org.meveo.service.billing.impl.ChargeInstanceService;
import org.meveo.service.billing.impl.RatingService;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.billing.impl.WalletOperationService;
import org.meveo.service.catalog.impl.InvoiceSubCategoryService;
import org.meveo.service.catalog.impl.OneShotChargeTemplateService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.script.Script;
import org.primefaces.model.SortOrder;

/**
 * The Class AncillaryDiscountApplicationScript.
 *
 * @author hznibar
 */
@Stateless
public class AncillaryDiscountApplicationScript extends Script {


    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;

    private static final String CUSTOMER_ACCOUNT = "customer_account";

    private static final String MAIN_SERVICE = "main_service";

    private static final String ANCILLARY_SERVICE = "ancillary_service";

    private static final String DISCOUNT_GROUP = "discount_group";

    private static final String DESCRIPTION = "description";

    private static final String EMPTY_STRING = "";

    private static final String GL_CODE = "gl_code";

    private static final String INVOICE_SUB_CAT = "invoice_sub_cat";

    private static final String CHARGE_TYPE = "charge_type";

    private static final String AGGREGATE_SQL = "aggregate_sql";

    private String queryCondition = " where st.cf_values::json->'SERVICE_TYPE'->0->>'string'  ='Ancillary' and st.cf_values::json->'MIGRATION_CODE'->0->>'string' = :ancillary_service and (si.status = 'ACTIVE' "
            + " or (si.status = 'TERMINATED' and si.status_date >= (select date_trunc('month', current_date)))) "
            + " AND st2.code LIKE ':main_service' AND st2.cf_values:: JSON->'SERVICE_TYPE'->0->>'string' ='Main' AND "
            + " st.code = (SELECT code FROM EIR_OR_DISCOUNT_GROUPS_SERVICES WHERE discount_group = :discount_group AND priority ="
            + " (SELECT MIN(priority) FROM EIR_OR_DISCOUNT_GROUPS_SERVICES WHERE discount_group = :discount_group))";
    private static final String BA_QUERY = " JOIN billing_service_instance si2 ON si.subscription_id = si2.subscription_id "
            + " join cat_service_template st2 on si2.service_template_id = st2.id "
            + " join billing_subscription sub on si.subscription_id = sub.id "
            + " join billing_user_account on sub.user_account_id  =billing_user_account.id  "
            + " join billing_billing_account on billing_user_account.billing_account_id = billing_billing_account.id and billing_billing_account.id in :baIds ";

    private static final String CA_QUERY = " and customer_account_id = :ca_id ";

    private static final String CA_ID_PARAM = ":ca_id";

    private static final String BA_ID_PARAM = ":baIds";

    private static final String MAIN_SERVICE_PARAM = ":main_service";

    private static final String ANCILLARY_SERVICE_PARAM = ":ancillary_service";

    private static final String DISCOUNT_GROUP_PARAM = ":discount_group";

    private static final String EIR_OR_ANCILLARY_DISCOUNT_AGGR_TABLE = "EIR_OR_ANCILLARY_DISCOUNT_AGGR";

    private static final String AGGREGATE_TYPE = "aggregate_type";

    private static final String EIR_OR_ANCILLARY_DISCOUNT_TABLE = "EIR_OR_ANCILLARY_DISCOUNT";

    private static final String MINMAX_OPTIONAL_RANGE_VALID_FROM_VALID_TO = "minmaxOptionalRange valid_from valid_to";

    private static final String ANCILLARY_DISCOUNT_CHARGE_TEMPLATE_CODE = "ANCILLARY_DISCOUNT_CHARGE";

    private static final String NOT_FOUND_CHARGE_MESSAGE = "The one-shot charge DISCOUNT_CHARGE is not found";

    private static final String NOT_FOUND_BILLING_RUN = "Billing run not found!";

    private static final String NULL_BILLING_RUN_INVOICE_DATE = "The billing Run invoice date is null !";

    /**
     * The custom table service.
     */
    @Inject
    private CustomTableService customTableService;

    @Inject
    ChargeInstanceService<ChargeInstance> chargeInstanceService;

    @Inject
    private OneShotChargeTemplateService chargeTemplateService;

    @Inject
    InvoiceSubCategoryService invoiceSubCategoryService;

    @Inject
    AccountingCodeService accountingCodeService;

    @Inject
    WalletOperationService walletOperationService;

    @Inject
    SubscriptionService subscriptionService;

    @Inject
    RatingService ratingService;

    @Inject
    BillingRunService billingRunService;

    @Inject
    BillingAccountService billingAccountService;

    /**
     * script main execute method.
     *
     * @param methodContext method context params
     * @throws BusinessException business exception
     */
    @Override
    public void execute(Map<String, Object> methodContext) throws BusinessException {
        try {
            log.info("Starting AncillaryDiscountApplicationScript ... ");
            BillingRun billingRun = null;
            Object brId = methodContext.get("BR_ID");

            if (brId != null) {
                billingRun = billingRunService.findById(Long.valueOf(brId.toString()));
            }
            if (billingRun == null) {
                throw new BusinessException(NOT_FOUND_BILLING_RUN + " Id = " + brId + ". Please enter the correct billing run id \"BR_ID\" in the job configuration variables!");
            }

            Date invoiceDate = billingRun.getInvoiceDate();
            if (invoiceDate == null) {
                throw new BusinessException(NULL_BILLING_RUN_INVOICE_DATE);
            }

            OneShotChargeTemplate chargetemplate = chargeTemplateService.findByCode(ANCILLARY_DISCOUNT_CHARGE_TEMPLATE_CODE);
            if (chargetemplate == null) {
                throw new BusinessException(NOT_FOUND_CHARGE_MESSAGE);
            }

            EntityManager em = walletOperationService.getEntityManager();

            // Get all discounts
            List<Map<String, Object>> ancillaryDiscounts = getActiveAncillaryDiscounts(invoiceDate);
            int jobResultNbOK = 0;
            if (!ancillaryDiscounts.isEmpty()) {
                String aggSqlId;
                Map<String, Object> aggSql;
                String sqlRequest;
                WalletOperation wo;
                WalletOperation oldWO;
                BigDecimal discountAmount;
                BigDecimal percent;
                List<Object[]> walletOperationsForDiscount;
                String discountFields = EMPTY_STRING;
                String aggregationParameters = EMPTY_STRING;
                String newDiscountFields = EMPTY_STRING;
                String newAggregationParameters = EMPTY_STRING;
                String customerAccount;
                String discountGroup;
                String ancillaryService;
                String mainService;
                String chargeType;
                InvoiceSubCategory invoiceSubCat;
                AccountingCode accountingCode;
                String baList = (billingRun.getBillingCycle() != null && billingRun.getBillingCycle().getBillingAccounts() != null) ? "(" + billingRun.getBillingCycle().getBillingAccounts().stream()
                        .map(a -> String.valueOf(a.getId())).collect(Collectors.joining(",")) + ")" : "(null)";//getBillingAccountsListbyBR(billingRun.getBillingCycle().getBillingAccounts());
                for (Map<String, Object> ancillaryDiscount : ancillaryDiscounts) {
                    customerAccount = String.valueOf(ancillaryDiscount.get(CUSTOMER_ACCOUNT));
                    discountGroup = String.valueOf(ancillaryDiscount.get(DISCOUNT_GROUP));
                    ancillaryService = String.valueOf(ancillaryDiscount.get(ANCILLARY_SERVICE));
                    mainService = String.valueOf(ancillaryDiscount.get(MAIN_SERVICE));
                    chargeType = String.valueOf(ancillaryDiscount.get(CHARGE_TYPE));
                    aggSqlId = String.valueOf(ancillaryDiscount.get(AGGREGATE_TYPE));
                    newAggregationParameters = customerAccount.concat(mainService).concat(discountGroup).concat(ancillaryService).concat(aggSqlId);
                    newDiscountFields = newAggregationParameters.concat(chargeType);

                    if (!newDiscountFields.equals(discountFields)) {
                        discountFields = newDiscountFields;
                        aggSql = customTableService.findById(EIR_OR_ANCILLARY_DISCOUNT_AGGR_TABLE, Long.valueOf(aggSqlId));
                        if (aggSql != null && aggSql.size() > 0) {
                            // run the aggregation sql only if the SQL query or one of its parameters changes.
                            if (!aggregationParameters.equals(newAggregationParameters)) {
                                aggregationParameters = newAggregationParameters;
                                sqlRequest = (String) aggSql.get(AGGREGATE_SQL);
                                //EIROPENE2E-454: discount will be applied only for BA of the billing Run                                
                                sqlRequest = sqlRequest + BA_QUERY.replace(BA_ID_PARAM, baList);

                                if (!"null".equals(customerAccount)) {
                                    sqlRequest = sqlRequest + CA_QUERY.replace(CA_ID_PARAM, customerAccount);
                                }

                                sqlRequest = sqlRequest + queryCondition
                                        .replace(MAIN_SERVICE_PARAM, mainService.replace("*", "%"))
                                        .replace(ANCILLARY_SERVICE_PARAM, "'" + ancillaryService + "'")
                                        .replace(DISCOUNT_GROUP_PARAM, discountGroup);
                               
                                executeAggregateSql(sqlRequest);
                            }

                            String percentSql = getDiscountPercentSql(newDiscountFields, invoiceDate);
                            walletOperationsForDiscount = getWalletOperationsForDiscount(chargeType, ancillaryService, percentSql, invoiceDate);                              
                            if (walletOperationsForDiscount != null && walletOperationsForDiscount.size() > 0) {

                                invoiceSubCat = em.getReference(InvoiceSubCategory.class, ((BigInteger) ancillaryDiscount.get(INVOICE_SUB_CAT)).longValue());
                                accountingCode = em.getReference(AccountingCode.class, ((BigInteger) ancillaryDiscount.get(GL_CODE)).longValue());
                                for (Object[] row : walletOperationsForDiscount) {
                                    oldWO = walletOperationService.findById(((BigInteger) row[0]).longValue());
                                    // percent = getDiscountPercent(newAncillaryFields,discountType,(BigInteger) row[1]);
                                    percent = (BigDecimal) row[2];
                                    discountAmount = oldWO.getAmountWithoutTax() != null
                                            ? oldWO.getAmountWithoutTax().multiply(percent).divide(BigDecimal.valueOf(100), 12, RoundingMode.HALF_EVEN).negate()
                                            : null;
                                    if (discountAmount != null && discountAmount.compareTo(BigDecimal.ZERO) < 0) { // this check is already done at the sql request
                                        discountAmount = oldWO.getAmountWithoutTax() != null
                                                ? oldWO.getAmountWithoutTax().multiply(percent).divide(BigDecimal.valueOf(100), 12, RoundingMode.HALF_EVEN).negate()
                                                : null;

                                        wo = new WalletOperation(ANCILLARY_DISCOUNT_CHARGE_TEMPLATE_CODE, (String) ancillaryDiscount.get(DESCRIPTION), oldWO.getWallet(),
                                                oldWO.getOperationDate(), oldWO.getInvoicingDate(), oldWO.getType(), oldWO.getCurrency(), oldWO.getTax(), discountAmount, null, null,
                                                BigDecimal.ONE, discountAmount, null, null, null, null, null, null, oldWO.getStartDate(), oldWO.getEndDate(),
                                                oldWO.getSubscriptionDate(), oldWO.getOfferTemplate(), oldWO.getSeller(), null, null, BigDecimal.ONE, oldWO.getOrderNumber(),
                                                invoiceSubCat, accountingCode, WalletOperationStatusEnum.OPEN, oldWO.getUserAccount(), oldWO.getBillingAccount());

//                                  wo = new WalletOperation(DISCOUNT_CHARGE_TEMPLATE_CODE, (String) ancillaryDiscount.get(DESCRIPTION), oldWO.getWallet(), oldWO.getOperationDate(), oldWO.getInvoicingDate(),
//                                      oldWO.getType(), oldWO.getCurrency(), oldWO.getTax(), discountAmount, null, null, BigDecimal.ONE, discountAmount, null, null, null, null, null, null, oldWO.getStartDate(),
//                                      oldWO.getEndDate(), oldWO.getSubscriptionDate(), oldWO.getOfferTemplate(), oldWO.getSeller(), null, null, BigDecimal.ONE, oldWO.getOrderNumber(), invoiceSubCat,
//                                      WalletOperationStatusEnum.OPEN); // TODO add accountingCode when move to OC v. 10

                                        wo.setTaxClass(oldWO.getTaxClass());
                                        wo.setSubscription(oldWO.getSubscription());
                                        wo.setChargeInstance(oldWO.getChargeInstance());
                                        wo.setServiceInstance(oldWO.getServiceInstance());
                                        oldWO.setParameterExtra(ANCILLARY_DISCOUNT_CHARGE_TEMPLATE_CODE); // to not apply the discount for several times
                                        ratingService.calculateAmounts(wo, wo.getUnitAmountWithoutTax(), wo.getUnitAmountWithTax());    
                                        walletOperationService.create(wo);
                                        walletOperationService.update(oldWO);
                                        jobResultNbOK++;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            methodContext.put(Script.JOB_RESULT_NB_OK, jobResultNbOK);
            log.info("AncillaryDiscountApplicationScript is finished");
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BusinessException(e);
        }

    }

//protected BigDecimal getDiscountPercent(String concatFields, String discountType, BigInteger rowNumber) {
//  String percentSql;
//  concatFields = concatFields.replaceAll("null", "");
//  if (TIER_DISCOUNT.equals(discountType)) {
//      percentSql = "select max (percent) from EIR_OR_ANCILLARY_DISCOUNT v where v.band_from <= " + rowNumber;
//  } else {
//      percentSql = "select min (percent) from EIR_OR_ANCILLARY_DISCOUNT v where v.band_from >= " + rowNumber;
//  }
//  percentSql += " and concat(customer_account,billing_account,main_service,aggregate_type,discount_type,charge_type) = '" + concatFields + "' and (v.valid_from <= '" + invoiceDate + "' and (v.valid_to IS NULL or v.valid_to > '" + invoiceDate
//          + "'))";
//  Object percent = customTableService.getEntityManager().createNativeQuery(percentSql).getSingleResult();
//  return percent != null ? new BigDecimal(percent.toString()) : BigDecimal.ZERO;
//}

    private String getBillingAccountsListbyBR(Long brId) {
        List<Long> listBas = billingAccountService.findBillingAccountIdsByBillingRun(brId);
        if (listBas != null && !listBas.isEmpty()) {
            listBas.toString().replace("[", "(").replace("]", ")");
        }
        return null;
    }

    /**
     * Gets the discount percent sql.
     *
     * @param concatFields the concat fields
     * @param discountType the discount type
     * @param invoiceDate  the invoice date
     * @return the discount percent sql
     */
    protected String getDiscountPercentSql(String concatFields, Date invoiceDate) {
        String percentSql;
        concatFields = concatFields.replaceAll("null", "");
        percentSql = "(select percent from EIR_OR_ANCILLARY_DISCOUNT v where v.band_from <= vw.row_number and concat(customer_account,main_service,discount_group,ancillary_service,aggregate_type,charge_type) = '" + concatFields + "' and (v.valid_from <= '"
                + invoiceDate + "' and (v.valid_to IS NULL or v.valid_to > '" + invoiceDate + "')) order by v.band_from desc limit 1)";
        return percentSql;
    }


    /**
     * Gets the active ancillary discounts.
     *
     * @param invoiceDate
     * @return the active ancillary discounts
     */
    protected List<Map<String, Object>> getActiveAncillaryDiscounts(Date invoiceDate) {
        Map<String, Object> filters = new HashMap<>();
        filters.put(MINMAX_OPTIONAL_RANGE_VALID_FROM_VALID_TO, invoiceDate);
        Object[] sortOrdering = new Object[]{CUSTOMER_ACCOUNT, SortOrder.ASCENDING, MAIN_SERVICE, SortOrder.ASCENDING, DISCOUNT_GROUP, SortOrder.ASCENDING, ANCILLARY_SERVICE, SortOrder.ASCENDING, CHARGE_TYPE, SortOrder.ASCENDING};
        return customTableService.list(EIR_OR_ANCILLARY_DISCOUNT_TABLE, new PaginationConfiguration(null, null, filters, null, null, sortOrdering));
    }

    /**
     * Gets the wallet operations for discount.
     *
     * @param chargeType    the charge type
     * @param migrationCode the migration code
     * @return the wallet operations for discount
     */
    @SuppressWarnings("unchecked")
    protected List<Object[]> getWalletOperationsForDiscount(String chargeType, String ancillaryService, String percentSql, Date invoiceDate) {
        String sql = "select wo.id , vw.row_number, " + percentSql + " from billing_wallet_operation wo "
                + "join mview_ancillary_discount_subscription vw on wo.service_instance_id = vw.service_instance_id "
                + "left join billing_rated_transaction rt on (rt.id = wo.rated_transaction_id and rt.status = 'OPEN') ";

        if (!"*".equals(chargeType)) {
            String inChargeType = "('R')";
            if (!"R".equals(chargeType)) {
                inChargeType = "('O','S')";
            }
            sql += "join billing_charge_instance ch on wo.charge_instance_id = ch.id " + "and ch.charge_type in " + inChargeType;
        }

        sql += " where " + percentSql + " is not null "
                //+ " and vw.row_number <= ("+percentSql.replace("percent", "band_from")+") "
                + "and wo.code not like '%DISCOUNT_CHARGE' and (wo.invoicing_date IS NULL or wo.invoicing_date <= '"
                + invoiceDate + "') and (wo.parameter_extra is null or wo.parameter_extra <> '" + ANCILLARY_DISCOUNT_CHARGE_TEMPLATE_CODE + "') "; // to not apply the discount for several times

        log.info("getting wallet operations for discount with query : " + sql);
        Query query = walletOperationService.getEntityManager().createNativeQuery(sql).setFlushMode(FlushModeType.COMMIT);
        return query.getResultList();
    }

    /**
     * Execute aggregate sql.
     *
     * @param sqlRequest the sql request
     */
    protected void executeAggregateSql(final String sqlRequest) {
        Session hibernateSession = subscriptionService.getEntityManager().unwrap(Session.class);

        hibernateSession.doWork(new org.hibernate.jdbc.Work() {

            @Override
            public void execute(Connection connection) throws SQLException {

                try (Statement statement = connection.createStatement()) {
                    log.info("dropping materialized view..");
                    statement.execute("drop materialized view if exists mview_ancillary_discount_subscription");
                    log.info("creating materialized view with request : " + sqlRequest);
                    statement.execute("create materialized view mview_ancillary_discount_subscription (service_instance_id, service_code, row_number) as " + sqlRequest);
                    log.info("creating index ancillary_discount_subscription_pk on materialized view");
                    statement.execute("create index ancillary_discount_subscription_pk on mview_ancillary_discount_subscription (service_instance_id)");
                } catch (Exception e) {
                    log.error("Failed to drop/create the materialized view mview_ancillary_discount_subscription", e.getMessage());
                    throw new BusinessException(e);
                }
            }
        });
    }
}
