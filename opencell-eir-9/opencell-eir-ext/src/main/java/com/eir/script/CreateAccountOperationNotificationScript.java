package com.eir.script;

import com.eir.commons.enums.customfields.AccountOperationCFsEnum;
import org.meveo.admin.exception.BusinessException;
import org.meveo.model.payments.AccountOperation;
import org.meveo.model.payments.RecordedInvoice;
import org.meveo.service.script.Script;

import javax.ejb.Stateless;
import java.util.Map;

/**
 * Update each account operation before its creation in database
 *
 * @author Abdellatif BARI
 */
@Stateless
public class CreateAccountOperationNotificationScript extends Script {

    @Override
    public void execute(Map<String, Object> parameters) throws BusinessException {

        try {

            AccountOperation accountOperation = (AccountOperation) parameters.get(Script.CONTEXT_ENTITY);
            if (accountOperation != null && accountOperation instanceof RecordedInvoice) {
                RecordedInvoice recordedInvoice = (RecordedInvoice) accountOperation;
                if (recordedInvoice.getInvoice() != null && recordedInvoice.getInvoice().getBillingAccount() != null) {
                    accountOperation.setCfValue(AccountOperationCFsEnum.BILLING_ACCOUNT_NUMBER.name(), recordedInvoice.getInvoice().getBillingAccount().getExternalRef1());
                }
            }

        } catch (Exception e) {
            log.error("error on process account operation notification script {} ", e.getMessage(), e);
            if (e instanceof BusinessException) {
                throw e;
            } else {
                // wrap the exception in a business exception and throwing it
                throw new BusinessException(e);
            }
        }
    }
}