package com.eir.script;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.NoChargeException;
import org.meveo.admin.exception.NoPricePlanException;
import org.meveo.admin.exception.RatingException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.api.commons.Utils;
import org.meveo.commons.utils.NumberUtils;
import org.meveo.model.DatePeriod;
import org.meveo.model.ICustomFieldEntity;
import org.meveo.model.admin.Seller;
import org.meveo.model.billing.AccountingCode;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.ChargeApplicationModeEnum;
import org.meveo.model.billing.ChargeInstance;
import org.meveo.model.billing.InstanceStatusEnum;
import org.meveo.model.billing.InvoiceSubCategory;
import org.meveo.model.billing.OneShotChargeInstance;
import org.meveo.model.billing.RecurringChargeInstance;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.billing.TerminationChargeInstance;
import org.meveo.model.billing.UserAccount;
import org.meveo.model.billing.WalletInstance;
import org.meveo.model.billing.WalletOperation;
import org.meveo.model.billing.WalletOperationStatusEnum;
import org.meveo.model.catalog.Calendar;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.OfferTemplateCategory;
import org.meveo.model.catalog.OneShotChargeTemplate;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.crm.Customer;
import org.meveo.model.crm.EntityReferenceWrapper;
import org.meveo.model.crm.Provider;
import org.meveo.model.crm.custom.CustomFieldValue;
import org.meveo.model.shared.DateUtils;
import org.meveo.model.shared.DateUtils.DatePeriodSplit;
import org.meveo.model.tax.TaxClass;
import org.meveo.service.base.PersistenceService;
import org.meveo.service.billing.impl.RatingService;
import org.meveo.service.billing.impl.WalletOperationService;
import org.meveo.service.catalog.impl.CalendarService;
import org.meveo.service.catalog.impl.OfferTemplateService;
import org.meveo.service.catalog.impl.ServiceTemplateService;
import org.meveo.service.crm.impl.CustomFieldInstanceService;
import org.meveo.service.crm.impl.CustomFieldTemplateService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.script.Script;
import org.meveo.service.tax.TaxClassService;
import org.meveo.service.tax.TaxMappingService;
import org.meveo.service.tax.TaxMappingService.TaxInfo;
import org.meveo.util.ApplicationProvider;
import org.primefaces.model.SortOrder;

import com.eir.commons.enums.ChargeTypeEnum;
import com.eir.commons.enums.ServiceTypeEnum;
import com.eir.commons.enums.TransformationRulesEnum;
import com.eir.commons.enums.customfields.ChargeInstanceCFsEnum;
import com.eir.commons.enums.customfields.ServiceInstanceCFsEnum;
import com.eir.commons.enums.customfields.ServiceParametersCFEnum;
import com.eir.commons.enums.customfields.ServiceTemplateCFsEnum;
import com.eir.commons.enums.customtables.CustomTableEnum;
import com.eir.commons.enums.customtables.ValidTariffCustomTableEnum;

@Stateless
public class OneShotAndRecurringRatingScript extends Script {

    private static final long serialVersionUID = -3467788957890975696L;

    public static final String CFT_TARIFF_PLAN_ID = "TARIFF_PLAN_ID";
    public static final String CFT_TARIFF_ID = "TARIFF_ID";

    protected static final String SLA_VALID_FROM = "valid_from";
    protected static final String SLA_VALID_TO = "valid_to";
    protected static final String SLA_PERCENT = "percent";
    protected static final String SLA_SERVICE_ID = "service_id";

    protected static final String DISCOUNT_VALID_FROM = "valid_from";
    protected static final String DISCOUNT_VALID_TO = "valid_to";
    protected static final String DISCOUNT_PERCENT = "percent";

    @Inject
    private TaxMappingService taxMappingService;

    @Inject
    private RatingService ratingService;

    @Inject
    private CustomTableService customTableService;

    @Inject
    private TaxClassService taxClassService;

    @Inject
    private CustomFieldTemplateService cftService;

    @Inject
    private ServiceTemplateService serviceTemplateService;

    @Inject
    private OfferTemplateService offerTemplateService;

    @Inject
    @ApplicationProvider
    private Provider appProvider;

//    @Inject
//    private CustomEntityInstanceService customEntityInstanceService;

    @Inject
    private CustomFieldInstanceService customFieldInstanceService;

    @Inject
    private WalletOperationService walletOperationService;

    @Inject
    private CalendarService calendarService;

//    /**
//     * Tariff search criteria fields mapped by a service template identifier
//     */
//    private static Map<Long, String[]> tariffFields;

    /**
     * Custom field template "SERVIVE_PARAMETERS" definition
     */
    private static CustomFieldTemplate serviceParamsCFT;

    /**
     * ID of a GNP product set
     */
    private static String gnpProductSetId;

    private static BigDecimal ONE_HUNDRED = new BigDecimal(100);

    @Override
    public void init(Map<String, Object> methodContext) throws BusinessException {

//        // Load the fields for searching
//        List<Map<String, Object>> fieldInfos = customTableService.list("EIR_OR_TARIFF_CRITERIA_MAP");
//        for (Map<String, Object> fieldInfo : fieldInfos) {
//            tariffFields.put((Long) fieldInfo.get("service_id"), ((String) fieldInfo.get("fields")).split(","));
//        }

        // Lookup CFT and product set definitions
        getServiceParametersCFT();
        getGNPProductSet();

    }

    @Override
    public void execute(Map<String, Object> initContext) throws BusinessException {

        // Init reference values if needed
        getServiceParametersCFT();
        getGNPProductSet();

        final WalletOperation wo = (WalletOperation) initContext.get(Script.CONTEXT_ENTITY);

        WalletOperation originalWoCopy = wo.getUnratedClone();

        log.trace("Rating wallet operation {} of chargeInstance {} and service {} with Service parameters values {}", wo, wo.getChargeInstance(), wo.getServiceInstance(),
            wo.getServiceInstance() != null ? wo.getServiceInstance().getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name()) : wo.getChargeInstance().getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name()));

//        // Remember start and end dates as they might change (applicable to recurring charges only, when calendar change)
//        Date startDate = wo.getStartDate();
//        Date endDate = wo.getEndDate();

        ChargeTypeEnum chargeType = ChargeTypeEnum.getChargeType(wo.getChargeInstance());
        boolean isOneShot = chargeType.isOneShot();
        boolean isQuantityBased = false; // A GNP product set
        boolean isApplyInAdvance = isOneShot ? true : originalWoCopy.isApplyInAdvance();

        // Only for OTHER type charge of MISC type service serviceInstance is null
        if (gnpProductSetId != null) {
            ServiceTemplate serviceTemplate = null;
            if (wo.getServiceInstance() != null) {
                serviceTemplate = wo.getServiceInstance().getServiceTemplate();
            } else {
                serviceTemplate = getServiceTemplateFromChargeInstanceCF(wo.getChargeInstance());
            }

            EntityReferenceWrapper productSet = (EntityReferenceWrapper) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.PRODUCT_SET.name());
            if (productSet != null) {
                isQuantityBased = gnpProductSetId.equals(productSet.getCode());
            }
        }

        // A list inside a list grouped by rating calendar changes.
        List<List<WalletOperation>> ratedWos = null;

        boolean calendarChange = false;
        int nbRatingTried = 1;

        List<WOToRate> wosToRate = new ArrayList<>();
        wosToRate.add(new WOToRate(wo, isOneShot ? null : ((RecurringChargeInstance) wo.getChargeInstance()).getCalendar()));

        do {
            ratedWos = new ArrayList<>();
            calendarChange = false;
            try {
                for (WOToRate woToRateInfo : wosToRate) {
                    if (!isOneShot && !((RecurringChargeInstance) wo.getChargeInstance()).getCalendar().equals(woToRateInfo.getCalendar())) {
                        RecurringChargeInstance chargeInstance = ((RecurringChargeInstance) wo.getChargeInstance());
                        chargeInstance.setCalendar(woToRateInfo.getCalendar());

                        // Update charge, nextCharge and chargedToDates
                        WalletOperation woToRate = woToRateInfo.getWalletOperation();
                        Date chargeDate = woToRate.getOperationDate();
                        Date toDate = woToRate.getFullRatingPeriod().getTo();

                        // Handles a case when reimbusing and calendar change would result in a further end date that should be reimbursed
                        if (originalWoCopy.getChargeMode().isReimbursement() && originalWoCopy.getEndDate().before(toDate)) {
                            toDate = originalWoCopy.getEndDate();
                            wo.setEndDate(toDate);
                        }

                        Date nextDate = toDate;
                        if (!isApplyInAdvance) {
                            chargeDate = toDate;
                            nextDate = walletOperationService.getRecurringPeriodEndDate(chargeInstance, toDate);
                        }
                        chargeInstance.advanceChargeDates(chargeDate, nextDate, toDate);

                        log.debug("Recurring charge {} calendar was be changed to {} charge dates were advanced to charge/next/chargedTo: {}/{}/{}", chargeInstance, woToRateInfo.getCalendar(),
                            DateUtils.formatAsDate(chargeInstance.getChargeDate()), DateUtils.formatAsDate(chargeInstance.getNextChargeDate()), DateUtils.formatAsDate(chargeInstance.getChargedToDate()));

                    }
                    List<WalletOperation> ratingResults = rateWalletOperation(woToRateInfo.getWalletOperation(), chargeType, isQuantityBased);
                    ratedWos.add(ratingResults);
                }

            } catch (RecurringCalendarChangeException e) {

                calendarChange = true;
                nbRatingTried++;

                WalletOperation lastWo = wosToRate.get(wosToRate.size() - 1).getWalletOperation();

                log.debug("Recurring calendar change detected from {} to {} starting {}", ((RecurringChargeInstance) lastWo.getChargeInstance()).getCalendar(), e.recurringCalendar, e.effectiveDate);

                Calendar newCalendar = CalendarService.initializeCalendar(e.getRecurringCalendar(), ((RecurringChargeInstance) lastWo.getChargeInstance()).getSubscriptionDate(), lastWo.getChargeInstance());
                Date newPeriodStartDate = newCalendar.previousCalendarDate(e.getEffectiveDate());
                Date newPeriodEndDate = newCalendar.nextCalendarDate(e.getEffectiveDate());

                // Handles a case when recurring calendar change match the wallet operation start date. In this case the WO.endDate should be disregarded completely.
                if (e.getEffectiveDate().compareTo(originalWoCopy.getStartDate()) == 0) {

                    wosToRate.get(wosToRate.size() - 1).setCalendar(e.getRecurringCalendar());
                    if (!isApplyInAdvance) {
                        lastWo.setOperationDate(newPeriodEndDate);
                    }
                    lastWo.setEndDate(newPeriodEndDate);
                    lastWo.setFullRatingPeriod(new DatePeriod(newPeriodStartDate, newPeriodEndDate));

                    // Handles cases when recurring calendar change somewhere in between the wallet operation start and end dates
                } else {

                    for (int i = 0; i < wosToRate.size(); i++) {

                        lastWo = wosToRate.get(i).getWalletOperation();
                        if (!DateUtils.isDateWithinPeriod(e.effectiveDate, lastWo.getStartDate(), lastWo.getEndDate())) {
                            continue;
                        }

                        // Add a new wallet operation for a period matching the next calendar
                        WalletOperation newWo = originalWoCopy.getUnratedClone();
                        newWo.setStartDate(e.getEffectiveDate());
                        newWo.setEndDate(newPeriodEndDate);
                        newWo.setFullRatingPeriod(new DatePeriod(newPeriodStartDate, newPeriodEndDate));
                        if (!isApplyInAdvance) {
                            newWo.setOperationDate(newWo.getEndDate());
                        }
                        wosToRate.add(i + 1, new WOToRate(newWo, newCalendar));

                        int k = 2;
                        while (newPeriodEndDate.before(lastWo.getEndDate())) {

                            newPeriodStartDate = newCalendar.previousCalendarDate(newPeriodEndDate);
                            newPeriodEndDate = newCalendar.nextCalendarDate(newPeriodEndDate);

                            newWo = originalWoCopy.getUnratedClone();
                            newWo.setStartDate(newPeriodStartDate);
                            newWo.setEndDate(newPeriodEndDate);
                            newWo.setFullRatingPeriod(new DatePeriod(newPeriodStartDate, newPeriodEndDate));
                            newWo.setOperationDate(isApplyInAdvance ? newWo.getStartDate() : newWo.getEndDate());
                            wosToRate.add(k, new WOToRate(newWo, newCalendar));
                            k++;
                        }

                        // Set full rating period if was not set before with wo.startDate and wo.endDate
                        // Set end date to the date that new calendar kicks in
                        DatePeriod fullRatingPeriod = lastWo.getFullRatingPeriod();
                        lastWo
                            .setFullRatingPeriod(new DatePeriod(fullRatingPeriod != null ? fullRatingPeriod.getFrom() : lastWo.getStartDate(), fullRatingPeriod != null ? fullRatingPeriod.getTo() : lastWo.getEndDate()));
                        lastWo.setEndDate(e.getEffectiveDate());
                        if (!isApplyInAdvance) {
                            lastWo.setOperationDate(lastWo.getEndDate());
                        }

                        break;
                    }
                }
                Collections.sort(wosToRate, Comparator.comparing(WOToRate::getStartDate));
            }

        } while (calendarChange && nbRatingTried < 5);

        for (int i = 0; i < ratedWos.size(); i++) {

            List<WalletOperation> ratedWosSingleCalendar = ratedWos.get(i);

            for (int k = 0; k < ratedWosSingleCalendar.size(); k++) {

                WalletOperation ratedWo = ratedWosSingleCalendar.get(k);

                // ----- Apply flat discounts
                applyFlatDiscounts(ratedWo);

                if (!(i == 0 && k == 0)) {
                    walletOperationService.create(ratedWo);
                }
            }

            if (!isOneShot) {
                applySLASurcharge(ratedWosSingleCalendar, isApplyInAdvance);
            }
        }
    }

    /**
     * Rate a single wallet operation. Can result in multiple wallet operations if quantity has changed in between the wallet operation rating period
     *
     * @param wo Wallet operation
     * @param chargeType Charge type
     * @param isQuantityBased Is this a quantity based service
     * @return Wallet operations that result of rating a given wallet operation. Will return the same passed wallet operation in case of one shot charge or recurring charges with no quantity change.
     * @throws RecurringCalendarChangeException Indicates that recurring calendar was changed in the tariff record
     */
    private List<WalletOperation> rateWalletOperation(WalletOperation wo, ChargeTypeEnum chargeType, boolean isQuantityBased) throws RecurringCalendarChangeException {

        boolean isOneShot = chargeType.isOneShot();

        // Split service configuration into separate periods by change in quantity and subsequently split into sub periods when service parameters change
        List<ServiceRatingParameterGroup> ratingGroups = determineRatingGroups(wo, isOneShot);

        List<WalletOperation> wos = new ArrayList<WalletOperation>();
        Date overallStartDate = wo.getFullRatingPeriod() != null ? wo.getFullRatingPeriod().getFrom() : wo.getStartDate();
        Date overallEndDate = wo.getFullRatingPeriod() != null ? wo.getFullRatingPeriod().getTo() : wo.getEndDate();

        // Process each service configuration group separately. The first configuration will update current WO. Subsequent will result in new WOs.
        int i = 0;
        BigDecimal originalUnitAmountWithoutTax = wo.getUnitAmountWithoutTax();
        for (ServiceRatingParameterGroup ratingGroup : ratingGroups) {

            WalletOperation woToUpdate = wo;
            if (i > 0) {
                woToUpdate = woToUpdate.getUnratedClone();
                woToUpdate.setUnitAmountWithoutTax(originalUnitAmountWithoutTax);
                woToUpdate.setStartDate(ratingGroup.getEarliestParameterFromDate());
            }

            // Rate wallet operation
            List<WalletOperation> wosRated = rateRatingGroup(ratingGroup, woToUpdate, chargeType, isQuantityBased, overallStartDate, overallEndDate);
            wos.addAll(wosRated);
            i++;
        }
        return wos;
    }

    /**
     * Apply SLA surcharges to recurring charges if applicable. Will produce a single Wallet Operation if calculated surcharge amount is >0;
     *
     * @param wos Wallet operations to calculate surcharge for
     * @param applyInAdvance Is operation applied in advance (operation date = start date)
     */
    protected List<WalletOperation> applySLASurcharge(List<WalletOperation> wos, boolean applyInAdvance) {

        // Determine the rating period date range
        Date startDate = null;
        Date endDate = null;

        for (WalletOperation wo : wos) {
            if (startDate == null || startDate.after(wo.getStartDate())) {
                startDate = wo.getStartDate();
            }
            if (endDate == null || endDate.before(wo.getEndDate())) {
                endDate = wo.getEndDate();
            }
        }

        WalletOperation firstWo = wos.get(0);

        // Take SLA premium liability date as a termination date if available
        Date maxDateToApplySLATo = firstWo.getChargeInstance().getTerminationDate();
        List<CustomFieldValue> paramValues = firstWo.getServiceInstance().getCfValues().getValuesByCode().get(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());
        if (paramValues != null) {
            Map<String, Object> serviceParams = serviceParamsCFT.deserializeMultiValue((String) paramValues.get(paramValues.size() - 1).getMapValue().values().iterator().next(), null);
            String slaLiabilityDate = serviceParams != null ? (String) serviceParams.get(ServiceParametersCFEnum.RENTAL_SLA_PREMIUM_LIABILITY_DATE.getLabel()) : null;
            if (slaLiabilityDate != null) {
                maxDateToApplySLATo = DateUtils.parseDate(slaLiabilityDate, new String[] { Utils.DATE_TIME_PATTERN });
            }
        }

        // Check if charge is not terminated for non-reimburse. If so - either skip or use an earlier date for calculation
        if (!firstWo.getChargeMode().isReimbursement() && firstWo.getChargeInstance().getStatus() == InstanceStatusEnum.TERMINATED) {
            if (maxDateToApplySLATo.compareTo(startDate) <= 0) {
                return null;
            } else if (maxDateToApplySLATo.before(endDate)) {
                endDate = firstWo.getChargeInstance().getTerminationDate();
            }
        }

        // Split service parameters to SLA parameter change periods

        // For ancillary services use main service for SLA surcharge configuration lookup
        ServiceInstance serviceInstanceForSLALookup = firstWo.getServiceInstance();
        if (ServiceTypeEnum.ANCILLARY.name().equalsIgnoreCase((String) serviceInstanceForSLALookup.getServiceTemplate().getCfValue(ServiceTemplateCFsEnum.SERVICE_TYPE.name()))) {
            for (ServiceInstance si : firstWo.getSubscription().getServiceInstances()) {
                if (ServiceTypeEnum.MAIN.name().equalsIgnoreCase((String) si.getServiceTemplate().getCfValue(ServiceTemplateCFsEnum.SERVICE_TYPE.name()))) {
                    serviceInstanceForSLALookup = si;
                    break;
                }
            }
        }

        // For terminated services, the last CF period in SERVICE_PARAMETERS marks termination date and has no duration.
        // But in case of reimbursement the charge might still have to be calculated past the termination date until the last charge date if charge was applied in advance.
        // Therefore the last CF period should be ignored and the second to last CF value should have its end date removed
        boolean ignoreLastCFPeriod = firstWo.getChargeMode() != null && firstWo.getChargeMode().isReimbursement();

        List<CustomFieldValue> cfValues = getServiceParameters(serviceInstanceForSLALookup, startDate, endDate, ignoreLastCFPeriod, false);

        List<SLAParameters> slaParameters = new ArrayList<>();

        String currentSlaInd = null;
        Date currentPeriodStart = null;
        Date currentPeriodEnd = null;

        int i = 0;
        int totalItems = cfValues.size();
        for (CustomFieldValue cfv : cfValues) {

            Map<String, Object> serviceParams = serviceParamsCFT.deserializeMultiValue((String) cfv.getMapValue().values().iterator().next(), null);

            String slaInd = StringUtils.trimToNull((String) serviceParams.get(ServiceParametersCFEnum.SLA.getLabel()));

            if (StringUtils.compare(slaInd, currentSlaInd) != 0) {

                if (currentSlaInd == null) {
                    currentSlaInd = slaInd;
                    currentPeriodStart = cfv.getPeriod().getFrom().compareTo(startDate) <= 0 ? startDate : cfv.getPeriod().getFrom();
                    currentPeriodEnd = cfv.getPeriod().getTo() != null && cfv.getPeriod().getTo().compareTo(endDate) < 0 ? cfv.getPeriod().getTo() : endDate;

                } else if (slaInd == null) {
                    slaParameters.add(new SLAParameters(new DatePeriod(currentPeriodStart, currentPeriodEnd), currentSlaInd));
                    currentSlaInd = null;
                    currentPeriodStart = null;
                    currentPeriodEnd = null;

                } else {
                    slaParameters.add(new SLAParameters(new DatePeriod(currentPeriodStart, cfv.getPeriod().getFrom()), currentSlaInd));
                    currentSlaInd = slaInd;
                    currentPeriodStart = cfv.getPeriod().getFrom();
                    currentPeriodEnd = cfv.getPeriod().getTo() != null && cfv.getPeriod().getTo().compareTo(endDate) < 0 ? cfv.getPeriod().getTo() : endDate;
                }
            } else if (slaInd != null) {
                currentPeriodEnd = cfv.getPeriod().getTo() != null && cfv.getPeriod().getTo().compareTo(endDate) < 0 ? cfv.getPeriod().getTo() : endDate;
            }

            if (i == totalItems - 1 && currentSlaInd != null) {
                slaParameters.add(new SLAParameters(new DatePeriod(currentPeriodStart, currentPeriodEnd), currentSlaInd));
            }
            i++;
        }

        // Apply SLA surcharge if any SLA_IND was set on service instance and SLA configuration exists for a given date range

        if (slaParameters.isEmpty()) {
            return null;
        }

        Map<String, Object> lastSlaConfig = null;

        EntityManager em = ratingService.getEntityManager();

        List<WalletOperation> slaWos = new ArrayList<>();

        for (SLAParameters slaParameter : slaParameters) {

            // Find SLA configuration for a given SLA parameter date range
            Map<String, Object> slaConfigFilter = new HashMap<String, Object>();

            slaConfigFilter.put("eqOptional service_id", serviceInstanceForSLALookup.getServiceTemplate().getId());
            slaConfigFilter.put("code", slaParameter.slaInd);
            slaConfigFilter.put("overlapOptionalRange valid_from valid_to", new Date[] { slaParameter.period.getFrom(), slaParameter.period.getTo() });

            // Sort by service_id NOT-NULL being first
            List<Map<String, Object>> slaConfigs = customTableService.list("eir_or_sla",
                new PaginationConfiguration(null, null, slaConfigFilter, null, null, "service_id", SortOrder.ASCENDING, SLA_VALID_FROM, SortOrder.ASCENDING));

            // No configuration rule matched
            if (slaConfigs.isEmpty()) {
                continue;
            }

            slaConfigs = normalizeSLAConfig(slaConfigs, new DatePeriod(slaParameter.period.getFrom(), slaParameter.period.getTo()));

            BigDecimal paramAmount = BigDecimal.ZERO;

            // Calculate prorated amount from all WOs that fall within SLA parameter date period
            for (WalletOperation wo : wos) {
                DatePeriod overlap = DateUtils.getPeriodOverlap(wo.getStartDate(), wo.getEndDate(), slaParameter.period.getFrom(), slaParameter.period.getTo());

                // Calculate proration. Assumption is that both wo and overlap period has both start and end date specified
                if (overlap != null) {
                    double woDays = DateUtils.daysBetween(wo.getStartDate(), wo.getEndDate());
                    double overlapDays = DateUtils.daysBetween(overlap.getFrom(), overlap.getTo());
                    if (woDays != overlapDays) {
                        paramAmount = paramAmount.add(wo.getAmountWithoutTax().multiply(new BigDecimal(overlapDays)).divide(new BigDecimal(woDays), 2, RoundingMode.HALF_UP));
                    } else {
                        paramAmount = paramAmount.add(wo.getAmountWithoutTax());
                    }
                }
            }

            // Calculate surcharge amount and prorate Wo amount it if there are more than one SLA configuration found

            int slaConfigSize = slaConfigs.size();
            double remainingProrateRatio = 1d;

            for (int ci = 0; ci < slaConfigSize; ci++) {

                Map<String, Object> slaConfig = slaConfigs.get(ci);

                // Remember first matched SLA configuration to be used for WO creation
                if (ci == slaConfigSize - 1) {
                    lastSlaConfig = slaConfig;
                }

                // Need to prorate the first or the last config when its duration is less than rating
                boolean prorateFirstOrLastAnyway = (ci == 0 && ((Date) slaConfig.get(SLA_VALID_FROM)).after(slaParameter.period.getFrom()))
                        || (ci == slaConfigSize - 1 && slaConfig.get(SLA_VALID_TO) != null && ((Date) slaConfig.get(SLA_VALID_TO)).before(slaParameter.period.getTo()));

                // For one shot there should be only one service parameter
                double parameterProrateRatio = 1d;

                // Last SLA config will take a remaining prorate ratio unless sla config starts later than WO start date from or date to are less
                if (ci == slaConfigSize - 1 && !prorateFirstOrLastAnyway) {
                    parameterProrateRatio = remainingProrateRatio;
                } else {
                    parameterProrateRatio = computeProratingRatio(slaParameter.period.getFrom(), slaParameter.period.getTo(), (Date) slaConfig.get(SLA_VALID_FROM), (Date) slaConfig.get(SLA_VALID_TO));
                    remainingProrateRatio = remainingProrateRatio - parameterProrateRatio;
                }

                slaParameter.addAmount(new BigDecimal(parameterProrateRatio).multiply(paramAmount).multiply((BigDecimal) slaConfig.get(SLA_PERCENT)).divide(ONE_HUNDRED));

            }

            // Create WO for SLA operation
            if (slaParameter.totalAmount.compareTo(BigDecimal.ZERO) != 0) {

                Number invoiceSubCategory = (Number) lastSlaConfig.get("invoice_sub_cat");
                Number accountingCode = (Number) lastSlaConfig.get("accounting_code");
                Date operationDate = applyInAdvance ? slaParameter.period.getFrom() : slaParameter.period.getTo();
                if (firstWo.getChargeInstance().getTerminationDate() != null && operationDate.after(firstWo.getChargeInstance().getTerminationDate())) {
                    operationDate = firstWo.getChargeInstance().getTerminationDate();
                }

                WalletOperation slaWo = new WalletOperation((String) lastSlaConfig.get("code"), (String) lastSlaConfig.get("description"), firstWo.getWallet(), operationDate, firstWo.getInvoicingDate(),
                    firstWo.getType(), firstWo.getCurrency(), firstWo.getTax(), slaParameter.totalAmount, null, null, BigDecimal.ONE, slaParameter.totalAmount, null, null, firstWo.getParameter1(),
                    firstWo.getParameter2(), firstWo.getParameter3(), firstWo.getParameterExtra(), slaParameter.period.getFrom(), slaParameter.period.getTo(), firstWo.getSubscriptionDate(), firstWo.getOfferTemplate(),
                    firstWo.getSeller(), null, null, BigDecimal.ONE, firstWo.getOrderNumber(), em.getReference(InvoiceSubCategory.class, invoiceSubCategory.longValue()),
                    em.getReference(AccountingCode.class, accountingCode.longValue()), WalletOperationStatusEnum.OPEN, firstWo.getUserAccount(), firstWo.getBillingAccount());

                slaWo.setSubscription(firstWo.getSubscription());
                slaWo.setServiceInstance(firstWo.getServiceInstance());
                slaWo.setChargeInstance(firstWo.getChargeInstance());
                slaWo.setTaxClass(firstWo.getTaxClass());

                // Copy tariff plan applied
                Object tariffPlanId = firstWo.getCfValue(CFT_TARIFF_PLAN_ID);
                if (tariffPlanId != null) {
                    slaWo.setCfValue(CFT_TARIFF_PLAN_ID, tariffPlanId);
                }
                ratingService.calculateAmounts(slaWo, slaWo.getUnitAmountWithoutTax(), slaWo.getUnitAmountWithTax());

                walletOperationService.create(slaWo);
                slaWos.add(slaWo);
            }
        }

        if (slaWos.isEmpty()) {
            return null;
        } else {
            return slaWos;
        }
    }

    /**
     * Normalize SLA configuration - when service template specific configuration exists for the same period as a generic configuration, split into multiple smaller unique periods
     *
     * @param slaConfigs SLA configuration lines
     * @param period Rating period
     * @return A list of tariff record groups
     */
    @SuppressWarnings("unchecked")
    protected List<Map<String, Object>> normalizeSLAConfig(List<Map<String, Object>> slaConfigs, DatePeriod period) {

        Map<String, Map<String, Object>> slaRecordGroups = new LinkedHashMap<>();

        // Records are ordered by valid_from date and other fields in lefToRight order based on their importance in matching
        for (Map<String, Object> slaConfig : slaConfigs) {
            Date from = (Date) slaConfig.get(SLA_VALID_FROM);
            Date to = (Date) slaConfig.get(SLA_VALID_TO);

            String key = DateUtils.formatDateWithPattern(from, DateUtils.DATE_PATTERN) + "_" + DateUtils.formatDateWithPattern(to, DateUtils.DATE_PATTERN);

            if (!slaRecordGroups.containsKey(key)) {

                slaRecordGroups.put(key, slaConfig);

                // Take only the top-most matched record as records are ordered by fields based on their importance in matching
            } else {
                continue;
            }

        }

        // To handle a case when generic and specific prices exist and are valid for the same period
        // Split into sub-periods
        DateUtils.DatePeriodSplit[] periods = new DateUtils.DatePeriodSplit[slaRecordGroups.size()];

        int i = 0;
        for (Map<String, Object> slaConfig : slaRecordGroups.values()) {
            periods[i] = new DateUtils.DatePeriodSplit(new DatePeriod((Date) slaConfig.get(SLA_VALID_FROM), (Date) slaConfig.get(SLA_VALID_TO)), slaConfig.get(SLA_SERVICE_ID) != null ? "1" : "2", slaConfig);
            i++;
        }

        List<DatePeriodSplit> normalizedPeriods = DateUtils.normalizeOverlapingDatePeriods(periods);

        // Get rid of values that are outside the current rating period e.g. bigger period 2020-01-01/null and a shorter period 2020-05-01/null with a higher priority will lead to normalized periods 2020-01-01/2020-05-01
        // and 2020-05-01/null. Period 2020-01-01/2020-05-01 is outside the current rating period 2021-07-01/2021-08-01
        List<Map<String, Object>> normalizedSlaConfigs = normalizedPeriods.stream()
            .filter(np -> np.getPeriod().getFrom().before(period.getTo()) && (np.getPeriod().getTo() == null || np.getPeriod().getTo().after(period.getFrom()))).map(np -> {
                Map<String, Object> value = (Map<String, Object>) np.getValue();
                value.put(SLA_VALID_FROM, np.getPeriod().getFrom());
                value.put(SLA_VALID_TO, np.getPeriod().getTo());
                return value;
            }).collect(Collectors.toList());

        return normalizedSlaConfigs;
    }

    /**
     * Rate one shot charge. Does NOT persist any wallet operation.
     *
     * @param userAccount User account
     * @param seller Seller
     * @param offerTemplate Offer template
     * @param serviceTemplateCode Service template code
     * @param chargeTemplate Charge template to rate
     * @param serviceParams Service parameters. A map of parameter key = parameter value.
     * @param quantity Quantity to rate
     * @param operationDate Operation date
     * @return A wallet operation with unit amounts, total amounts, taxes, invoice subcategory and other information populated
     */
    @SuppressWarnings("unchecked")
    public WalletOperation rateOneShotCharge(UserAccount userAccount, Seller seller, OfferTemplate offerTemplate, String serviceTemplateCode, OneShotChargeTemplate chargeTemplate, Map<String, Object> serviceParams,
            BigDecimal quantity, Date operationDate) {

        WalletOperation wo = new WalletOperation();

        WalletInstance wallet = new WalletInstance();
        wallet.setUserAccount(userAccount);
        wo.setWallet(wallet);
        
        wo.setSeller(seller);
        wo.setUserAccount(userAccount);
        wo.setBillingAccount(userAccount.getBillingAccount());
        wo.setOfferTemplate(offerTemplate);
        wo.setQuantity(quantity);
        wo.setOperationDate(operationDate);
        wo.setChargeMode(ChargeApplicationModeEnum.SUBSCRIPTION);

        ChargeInstance chargeInstance = new OneShotChargeInstance();
        chargeInstance.setChargeTemplate(chargeTemplate);
        wo.setChargeInstance(chargeInstance);

        ServiceInstance serviceInstance = new ServiceInstance();
        serviceInstance.setServiceTemplate(serviceTemplateService.findByCode(serviceTemplateCode));
        serviceInstance.setCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), new DatePeriod(), 1,
            MapUtils.putAll(new HashMap<String, Object>(), new Object[] { "0", serviceParamsCFT.serializeMultiValue(serviceParams) }));

        wo.setServiceInstance(serviceInstance);

        execute(MapUtils.putAll(new HashMap<String, Object>(), new Object[] { Script.CONTEXT_ENTITY, wo }));

        return wo;

    }

    /**
     * Estimate a charge amount for a given service
     * 
     * @param customer Customer. Pass new Customer entity object to use default tariff plan.
     * @param offerTemplate Offer template
     * @param serviceTemplateCode Service template code
     * @param chargeType Charge type
     * @param serviceParams Service parameters
     * @param quantity Quantity. Used for one shot charges only.
     * @param operationDate Operation date or period start date for recurring charges
     * @param periodLength Length of period to estimate for. Use java.util.Calendar.MONTH variable for month and java.util.Calendar.YEAR for a year period
     * @return A total amount for a given period length
     */
    public BigDecimal estimateChargeAmount(Customer customer, OfferTemplate offerTemplate, ServiceTemplate serviceTemplate, ChargeTypeEnum chargeType, Map<String, Object> serviceParams, BigDecimal quantity,
            Date operationDate, int periodLength) {

        // Init reference values if needed
        getServiceParametersCFT();
        getGNPProductSet();

        boolean isQuantityBased = false; // A GNP product set

        // Only for OTHER type charge of MISC type service serviceInstance is null
        if (gnpProductSetId != null) {
            EntityReferenceWrapper productSet = (EntityReferenceWrapper) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.PRODUCT_SET.name());
            if (productSet != null) {
                isQuantityBased = gnpProductSetId.equals(productSet.getCode());
            }
        }

        Date from = operationDate;
        Date to = DateUtils.addDaysToDate(operationDate, 1);

        boolean isOneShot = chargeType.isOneShot();

        boolean isDistanceBased = !StringUtils.isBlank((String) serviceParams.get(ServiceParametersCFEnum.DISTANCE.getLabel()));

        // ----- Match tariff records

        // Get matching tariff records, grouped by tariff validity date (only recurring charges could have multiple tariffs groups)
        List<TariffRecordGroup> tariffRecordGroups = getMatchingTariffRecords(customer, offerTemplate, serviceTemplate, chargeType, quantity, from, !isOneShot ? new DatePeriod(from, to) : null, serviceParams,
            isQuantityBased);

        if (tariffRecordGroups.isEmpty()) {
            throw new NoChargeException("Unable to determine a tariff for customer " + customer.getCode() + " offer " + offerTemplate.getCode() + " and date " + from);
        }

        TariffRecordGroup tariffRecordGroup = tariffRecordGroups.get(0);

        BigDecimal totalAmountWithoutTax = BigDecimal.ZERO;

        TariffRecord lastTariffRecord = tariffRecordGroup.getLastTariffRecord();

        // ----- Calculate amounts

        boolean isYearlyTariff = false;

        // Calculate amounts based on a One shot TOTAL amount when total amounts were overridden on service parameter level
        if (isOneShot && !StringUtils.isBlank((String) serviceParams.get(ServiceParametersCFEnum.AMTOOC.getLabel()))) {
            totalAmountWithoutTax = new BigDecimal((String) serviceParams.get(ServiceParametersCFEnum.AMTOOC.getLabel()));

            // Calculate amounts based on a recurring TOTAL amount when total amounts were overridden on service parameter level. Amount is a yearly price.
        } else if (!isOneShot && !StringUtils.isBlank((String) serviceParams.get(ServiceParametersCFEnum.AMT.getLabel()))) {

            totalAmountWithoutTax = new BigDecimal((String) serviceParams.get(ServiceParametersCFEnum.AMT.getLabel()));
            isYearlyTariff = true;
            // For quantity based service, calculate a UNIT amount with a quantity cutoff at max level US 273.
            // Supposedly only one-shot charges, so only one tariff record is expected
        } else if (isQuantityBased) {

            BigDecimal maxQuantity = lastTariffRecord.maxQuantity != null ? new BigDecimal(lastTariffRecord.maxQuantity) : BigDecimal.ZERO;
            if (quantity.compareTo(maxQuantity) > 0) {
                quantity = maxQuantity;
            }

            totalAmountWithoutTax = lastTariffRecord.amountWithoutTax.multiply(quantity);

            // Calculate amounts based on a UNIT amount for non-distance based service
        } else if (!isDistanceBased) {

            totalAmountWithoutTax = lastTariffRecord.amountWithoutTax.multiply(quantity);

            // Calculate amounts based on a UNIT amount for distance based service US 941
        } else {

            int distance = Integer.valueOf((String) serviceParams.get(ServiceParametersCFEnum.DISTANCE.getLabel()));

            boolean isDistanceToPresent = false;

            BigDecimal distanceTotal = BigDecimal.ZERO;

            for (TariffRecord tariffRecord : tariffRecordGroup.tariffRecords) {

                // Non-distance based charges
                if (tariffRecord.distanceTo == null) {
                    distanceTotal = distanceTotal.add(tariffRecord.amountWithoutTax);
                    isDistanceToPresent = true;

                } else {

                    isDistanceToPresent = isDistanceToPresent || distance <= tariffRecord.distanceTo;

                    int diff = (distance <= tariffRecord.distanceTo ? distance : tariffRecord.distanceTo) - tariffRecord.distanceFrom + 1;

                    BigDecimal numberOfUnits = new BigDecimal(diff).divide(new BigDecimal(tariffRecord.distanceUnit), 0, RoundingMode.CEILING);
                    distanceTotal = distanceTotal.add(tariffRecord.amountWithoutTax.multiply(numberOfUnits));
                }

            }

            if (!isDistanceToPresent) {
                throw new RatingException("'Distance' value " + distance + " provided in Order is greater than a value available in tariffs");
            }
            totalAmountWithoutTax = distanceTotal.multiply(quantity);

        }

        // Prorate to a desired length for recurring charges
        if (!isOneShot) {

            Calendar recurringCalendar = calendarService.findById(tariffRecordGroup.recurringCalendarId);
            recurringCalendar.setInitDate(from);
            Date tariffPeriodFrom = recurringCalendar.previousCalendarDate(from);

            // The fixed amount is a yearly amount
            Date tariffPeriodTo = isYearlyTariff ? DateUtils.addYearsToDate(tariffPeriodFrom, 1) : recurringCalendar.nextCalendarDate(from);

            Date overallPeriodTo = to;

            if (periodLength == GregorianCalendar.YEAR) {
                overallPeriodTo = DateUtils.addYearsToDate(tariffPeriodFrom, 1);
            } else if (periodLength == GregorianCalendar.MONTH) {
                overallPeriodTo = DateUtils.addMonthsToDate(tariffPeriodFrom, 1);
            }

            // In cases when tariff period, (e.g. month) is shorter than a period to estimate to, tariff has to be multiplied to match estimation period length
            if (tariffPeriodTo.before(overallPeriodTo)) {
                Double prorate = computeProratingRatioInMonth(tariffPeriodFrom, overallPeriodTo, tariffPeriodFrom, tariffPeriodTo);
                totalAmountWithoutTax = totalAmountWithoutTax.divide(new BigDecimal(prorate), appProvider.getRounding(), appProvider.getRoundingMode().getRoundingMode());
            } else {
                Double prorate = computeProratingRatioInMonth(tariffPeriodFrom, tariffPeriodTo, tariffPeriodFrom, overallPeriodTo);
                totalAmountWithoutTax = totalAmountWithoutTax.multiply(new BigDecimal(prorate));
            }
        }

        totalAmountWithoutTax = totalAmountWithoutTax.setScale(appProvider.getRounding(), appProvider.getRoundingMode().getRoundingMode());

        return totalAmountWithoutTax;
    }

    /**
     * Rate a single quantity-related service parameter group. Each service parameter or tariff change will result in a new Wallet operation.
     *
     * @param ratingGroup Service parameters groped by quantity
     * @param wo Original wallet operation to rate. Note that after rating the first quantity, a unitAmountWithoutTax will be set and it will not be able to distinguish between overridden amount and a calculated amount.
     * @param chargeType Charge type
     * @param isQuantityBased Is this a quantity based service
     * @param overallStartDate Overall recurring rating date period - start date
     * @param overallEndDate Overall recurring rating date period - end date
     * @return A list of rated wallet operations. In case it is the first wallet operation, original wallet operation will be updated. Each service parameter or tariff change will result in a new Wallet operation.
     * @throws RecurringCalendarChangeException Indicates that recurring calendar was changed in the tariff record
     */
    private List<WalletOperation> rateRatingGroup(ServiceRatingParameterGroup ratingGroup, WalletOperation wo, ChargeTypeEnum chargeType, boolean isQuantityBased, Date overallStartDate, Date overallEndDate)
            throws RecurringCalendarChangeException {

        List<WalletOperation> wos = new ArrayList<WalletOperation>();
        BigDecimal originalUnitAmountWithoutTax = wo.getUnitAmountWithoutTax();

        // For one shot charges, quantity specified in WO is the quantity to be used
        // For recurring charges, quantity is versioned.
        // A change in quantity should appear as a separate WO for the recurring period when it was changed. Update WO with a delta quantity

        BigDecimal quantity = chargeType.isOneShot() ? wo.getQuantity() : ratingGroup.quantity;
        boolean isOneShot = chargeType.isOneShot();

        ServiceTemplate serviceTemplate = null;
        if (wo.getServiceInstance() != null) {
            serviceTemplate = wo.getServiceInstance().getServiceTemplate();
        } else {
            serviceTemplate = getServiceTemplateFromChargeInstanceCF(wo.getChargeInstance());
        }

        BillingAccount ba = wo.getBillingAccount();
        Customer customer = ba.getCustomerAccount().getCustomer();

        // -- Process each service parameter period and prorate it accordingly for recurring charges. For one shot there wont be any prorating on rating group and service parameter
        // level - there should be only one of them.
        double totalProrateRatio = 1d;
        Date minGroupDate = null;
        Date maxGroupDate = null;
        int parameterSize = ratingGroup.parameters.size();
        // For one shot there should be only one rating group
        if (!isOneShot) {
            minGroupDate = ratingGroup.parameters.get(0).period.getFrom(); // Periods are sorted in ascending order
            maxGroupDate = ratingGroup.parameters.get(parameterSize - 1).period.getTo();
            totalProrateRatio = computeProratingRatio(overallStartDate, overallEndDate, minGroupDate, maxGroupDate);
        }
        double remainingProrateRatio = totalProrateRatio;

        EntityManager em = ratingService.getEntityManager();

        // For Misc charge, the offer template should be looked up using misc service
        OfferTemplate offerTemplate = wo.getOfferTemplate();
        if (wo.getServiceInstance() == null) {
            List<OfferTemplate> offerTemplates = offerTemplateService.findByServiceTemplate(serviceTemplate);
            if (offerTemplates == null || offerTemplates.size() != 1) {
                throw new BusinessException("Offer template not found for service " + serviceTemplate);
            }
            offerTemplate = offerTemplates.get(0);
            wo.setOfferTemplate(offerTemplate);
            wo.setOfferCode(offerTemplate.getCode());
        }

        int woi = 0;

        String terminationOrderNr = null;
        if (!isOneShot && wo.getChargeMode() != null && wo.getChargeMode().isReimbursement()) {
            terminationOrderNr = (String) getLastServiceParameters(wo.getServiceInstance()).get(ServiceParametersCFEnum.ORDER_ID.getLabel());
        }

        parameterLoop: for (int i = 0; i < parameterSize; i++) {

            ServiceRatingParameters serviceParameter = ratingGroup.parameters.get(i);

            boolean isDistanceBased = !StringUtils.isBlank((String) serviceParameter.parameters.get(ServiceParametersCFEnum.DISTANCE.getLabel()));

            // For one shot there should be only one service parameter
            double parameterProrateRatio = 1d;
            if (!isOneShot) {
                if (i == parameterSize - 1) {
                    parameterProrateRatio = remainingProrateRatio;
                } else {
                    parameterProrateRatio = totalProrateRatio * computeProratingRatio(minGroupDate, maxGroupDate, serviceParameter.period.getFrom(), serviceParameter.period.getTo());
                    remainingProrateRatio = remainingProrateRatio - parameterProrateRatio;
                }
            }

            // ----- Match tariff records

            // Get matching tariff records, grouped by tariff validity date (only recurring charges could have multiple tariffs groups)
            List<TariffRecordGroup> tariffRecordGroups = getMatchingTariffRecords(customer, offerTemplate, serviceTemplate, chargeType, ratingGroup.accumulatedQuantity, wo.getOperationDate(),
                !isOneShot ? serviceParameter.period : null, serviceParameter.parameters, isQuantityBased);

            if (tariffRecordGroups.isEmpty()) {
                throw new NoChargeException("Unable to determine a tariff for customer " + customer.getCode() + " offer " + offerTemplate.getCode() + " and date " + wo.getOperationDate());
            }

            // Check if there is any calendar change in
            if (!isOneShot) {
                for (TariffRecordGroup tariffRecordGroup : tariffRecordGroups) {
                    if (!tariffRecordGroup.recurringCalendarId.equals(((RecurringChargeInstance) wo.getChargeInstance()).getCalendar().getId())) {
                        Date effectiveDate = serviceParameter.period.getFrom().after(tariffRecordGroup.getValidity().getFrom()) ? serviceParameter.period.getFrom() : tariffRecordGroup.getValidity().getFrom();
                        throw new RecurringCalendarChangeException(effectiveDate, calendarService.findById(tariffRecordGroup.getRecurringCalendarId()));
                    }
                }
            }

            int groupCount = tariffRecordGroups.size();

            double totalTariffProrateRatio = parameterProrateRatio;
            double remainingTariffProrateRatio = totalTariffProrateRatio;

            for (int groupIndex = 0; groupIndex < groupCount; groupIndex++) {
                TariffRecordGroup tariffRecordGroup = tariffRecordGroups.get(groupIndex);

                BigDecimal totalUnitAmountWithoutTax = BigDecimal.ZERO;
                BigDecimal totalAmountWithoutTax = BigDecimal.ZERO;

                boolean exitTariff = false;

                if (woi > 0) {
                    wo = wo.getUnratedClone();
                    wo.setUnitAmountWithoutTax(originalUnitAmountWithoutTax);
                }
                wo.setQuantity(quantity);

                if (!isOneShot) {
                    wo.setStartDate(tariffRecordGroup.validity.getFrom() == null || tariffRecordGroup.validity.getFrom().before(serviceParameter.period.getFrom()) ? serviceParameter.period.getFrom()
                            : tariffRecordGroup.validity.getFrom());
                    wo.setEndDate(
                        tariffRecordGroup.validity.getTo() == null || tariffRecordGroup.validity.getTo().after(serviceParameter.period.getTo()) ? serviceParameter.period.getTo() : tariffRecordGroup.validity.getTo());
                }

                // ----- Set tax and other main WO parameters
                TariffRecord lastTariffRecord = tariffRecordGroup.getLastTariffRecord();

                wo.setCode(serviceTemplate.getCode() + (isOneShot ? "_NRC" : "_RC"));
                wo.setDescription(lastTariffRecord.description);
                TaxClass taxClass = taxClassService.findById(lastTariffRecord.taxClassId);
                if(wo.getChargeInstance() != null && StringUtils.isNotBlank(wo.getChargeInstance().getCriteria2())) {
                    TaxClass manualChargeTaxClass = taxClassService.findByCode(wo.getChargeInstance().getCriteria2());
                    if(manualChargeTaxClass != null) {
                        taxClass = manualChargeTaxClass;
                    }
                }
                wo.setTaxClass(taxClass);
                wo.setInvoiceSubCategory(em.getReference(InvoiceSubCategory.class, lastTariffRecord.invoiceSubCategoryId));
                wo.setAccountingCode(em.getReference(AccountingCode.class, lastTariffRecord.accountingCodeId));
                wo.setCfValue(CFT_TARIFF_PLAN_ID, lastTariffRecord.getTariffPlanId());
                wo.setCfValue(CFT_TARIFF_ID, lastTariffRecord.getId());

                // Determine tax
                TaxInfo taxInfo = taxMappingService.determineTax(wo.getTaxClass(), wo.getSeller(), ba, wo.getUserAccount(), wo.getOperationDate(), true, false);
                wo.setTax(taxInfo.tax);
                wo.setTaxPercent(taxInfo.tax.getPercent());

                // In case of one shot charges, order number is correct. It comes from application - either from service.orderNumber for subscription charge, or a provided order number in applyOneShotCharge or
                // termination cases
                // For recurring charges:
                // In case of termination when recurring charges are applied to the end agreement, a subscription order number will be applied.
                // A correct approach is to apply termination order number to charges applied passed the termination date
                // When reimbursing, a termination order number should be applied always
                if (!isOneShot) {
                    if (wo.getChargeMode() != null && wo.getChargeMode().isReimbursement()) {
                        wo.setOrderNumber(terminationOrderNr);
                    } else {
                        wo.setOrderNumber((String) serviceParameter.parameters.get(ServiceParametersCFEnum.ORDER_ID.getLabel()));
                    }
                }

                BigDecimal tariffToApply = null;

                // ----- Calculate amounts

                // Calculate amounts based on a UNIT amount when unit amounts were overridden on charge level
                if (wo.getUnitAmountWithoutTax() != null) {
                    totalUnitAmountWithoutTax = new BigDecimal(parameterProrateRatio).multiply(wo.getUnitAmountWithoutTax());
                    tariffToApply = wo.getUnitAmountWithoutTax();

                    exitTariff = true;

                    // Calculate amounts based on a One shot TOTAL amount when total amounts were overridden on service parameter level
                } else if (isOneShot && !StringUtils.isBlank((String) serviceParameter.parameters.get(ServiceParametersCFEnum.AMTOOC.getLabel()))) {
                    totalAmountWithoutTax = new BigDecimal(parameterProrateRatio).multiply(new BigDecimal((String) serviceParameter.parameters.get(ServiceParametersCFEnum.AMTOOC.getLabel())));
                    tariffToApply = new BigDecimal((String) serviceParameter.parameters.get(ServiceParametersCFEnum.AMTOOC.getLabel()));

                    exitTariff = true;

                    // Calculate amounts based on a recurring TOTAL amount when total amounts were overridden on service parameter level
                } else if (!isOneShot && !StringUtils.isBlank((String) serviceParameter.parameters.get(ServiceParametersCFEnum.AMT.getLabel()))) {

                    // When quantity changes during the rating period, two rating groups will be created: the original quantity and a delta quantity.
                    // When rating with unit amounts at least two WOs will be created: unitAmount x original quantity and unitAmount x delta quantity
                    // When rating with a fixed total amount only one WO should be created: totalAmount with current period quantity (original + delta)
                    // Therefore the rating group with an original quantity will be skipped and only a rating group containing delta quantity will be rated, but with accumulated
                    // quantity shown on WO (accumulated quantity equals to = original + delta)
                    BigDecimal currentParameterPeriodQuantity = new BigDecimal((String) serviceParameter.parameters.get(ServiceParametersCFEnum.QUANTITY.getLabel()));
                    if (currentParameterPeriodQuantity.compareTo(ratingGroup.accumulatedQuantity) != 0) {
                        continue parameterLoop;
                    }

                    quantity = currentParameterPeriodQuantity;
                    wo.setQuantity(currentParameterPeriodQuantity);

                    // The fixed amount is a yearly amount, need to prorate it
                    Date aYearFromDate = DateUtils.addYearsToDate(overallStartDate, 1);

                    parameterProrateRatio = totalProrateRatio * computeProratingRatioInMonth(overallStartDate, aYearFromDate, serviceParameter.period.getFrom(), serviceParameter.period.getTo());

                    totalAmountWithoutTax = new BigDecimal(parameterProrateRatio).multiply(new BigDecimal((String) serviceParameter.parameters.get(ServiceParametersCFEnum.AMT.getLabel())));

                    tariffToApply = new BigDecimal((String) serviceParameter.parameters.get(ServiceParametersCFEnum.AMT.getLabel()));
                    double prorateAMTTariff = computeProratingRatioInMonth(overallStartDate, aYearFromDate, overallStartDate, overallEndDate);
                    tariffToApply = new BigDecimal(prorateAMTTariff).multiply(tariffToApply);

                    exitTariff = true;

                    // For quantity based service, calculate a UNIT amount with a quantity cutoff at max level US 273.
                    // Supposedly only one-shot charges, so only one tariff record is expected
                } else if (isQuantityBased) {

                    BigDecimal maxQuantity = lastTariffRecord.maxQuantity != null ? new BigDecimal(lastTariffRecord.maxQuantity) : BigDecimal.ZERO;
                    if (ratingGroup.accumulatedQuantity.compareTo(maxQuantity) > 0) {
                        wo.setInputQuantity(wo.getQuantity());
                        wo.setQuantity(maxQuantity);
                    }

                    totalUnitAmountWithoutTax = new BigDecimal(parameterProrateRatio).multiply(lastTariffRecord.amountWithoutTax);
                    tariffToApply = lastTariffRecord.amountWithoutTax;

                    exitTariff = true;

                    // Calculate amounts based on a UNIT amount for non-distance based service
                } else if (!isDistanceBased) {

                    // For one shot there should be only one tariff record matched. It should not even come to here
                    double tariffRecProrateRatio = 1d;
                    if (!isOneShot) {
                        if (groupIndex == groupCount - 1) {
                            tariffRecProrateRatio = remainingTariffProrateRatio;
                        } else {
                            tariffRecProrateRatio = totalTariffProrateRatio
                                    * computeProratingRatio(serviceParameter.period.getFrom(), serviceParameter.period.getTo(), tariffRecordGroup.validity.getFrom(), tariffRecordGroup.validity.getTo());
                            remainingTariffProrateRatio = remainingTariffProrateRatio - tariffRecProrateRatio;
                        }
                    }

                    totalUnitAmountWithoutTax = new BigDecimal(tariffRecProrateRatio).multiply(tariffRecordGroup.tariffRecords.get(0).amountWithoutTax);

                    tariffToApply = tariffRecordGroup.tariffRecords.get(0).amountWithoutTax;

                    // Calculate amounts based on a UNIT amount for distance based service US 941
                } else {

                    int distance = Integer.valueOf((String) serviceParameter.parameters.get(ServiceParametersCFEnum.DISTANCE.getLabel()));

                    boolean isDistanceToPresent = false;

                    // For one shot there should be only one tariff record matched. It should not even come to here
                    double tariffRecProrateRatio = 1d;
                    if (!isOneShot) {
                        if (groupIndex == groupCount - 1) {
                            tariffRecProrateRatio = remainingTariffProrateRatio;
                        } else {
                            tariffRecProrateRatio = totalTariffProrateRatio
                                    * computeProratingRatio(serviceParameter.period.getFrom(), serviceParameter.period.getTo(), tariffRecordGroup.validity.getFrom(), tariffRecordGroup.validity.getTo());
                            remainingTariffProrateRatio = remainingTariffProrateRatio - tariffRecProrateRatio;
                        }
                    }

                    BigDecimal distanceTotal = BigDecimal.ZERO;

                    for (TariffRecord tariffRecord : tariffRecordGroup.tariffRecords) {

                        // Non-distance based charges
                        if (tariffRecord.distanceTo == null) {
                            distanceTotal = distanceTotal.add(tariffRecord.amountWithoutTax);
                            isDistanceToPresent = true;

                        } else {

                            isDistanceToPresent = isDistanceToPresent || distance <= tariffRecord.distanceTo;

                            int diff = (distance <= tariffRecord.distanceTo ? distance : tariffRecord.distanceTo) - tariffRecord.distanceFrom + 1;

                            BigDecimal numberOfUnits = new BigDecimal(diff).divide(new BigDecimal(tariffRecord.distanceUnit), 0, RoundingMode.CEILING);
                            distanceTotal = distanceTotal.add(tariffRecord.amountWithoutTax.multiply(numberOfUnits));
                        }

                    }

                    if (!isDistanceToPresent) {
                        throw new RatingException("'Distance' value " + distance + " provided in Order is greater than a value available in tariffs");
                    }
                    totalUnitAmountWithoutTax = new BigDecimal(tariffRecProrateRatio).multiply(distanceTotal);

                    tariffToApply = distanceTotal;

                }

                // For recurring charges, the quantity is taken from SERVICE_PARAMETERS Custom field. Need to negate the value in case of reimbursement
                if (!isOneShot && wo.getChargeMode() != null && wo.getChargeMode().isReimbursement()) {
                    wo.setQuantity(wo.getQuantity().negate());
                    totalAmountWithoutTax = totalAmountWithoutTax.negate();
                }
                wo.setUnitAmountWithoutTax(totalUnitAmountWithoutTax);
                ratingService.calculateAmounts(wo, wo.getUnitAmountWithoutTax(), wo.getUnitAmountWithTax());

                // If AMT and AMTOC value was set, add the value specified to the total amount and set unit amounts to NULL
                if (totalAmountWithoutTax.compareTo(BigDecimal.ZERO) != 0) {
                    wo.setAmountWithoutTax(wo.getAmountWithoutTax().add(totalAmountWithoutTax));
                    BigDecimal[] amounts = NumberUtils.computeDerivedAmounts(wo.getAmountWithoutTax(), wo.getAmountWithTax(), wo.getTaxPercent(), appProvider.isEntreprise(), appProvider.getRounding(),
                        appProvider.getRoundingMode().getRoundingMode());

                    wo.setAmountWithoutTax(amounts[0]);
                    wo.setAmountWithTax(amounts[1]);
                    wo.setAmountTax(amounts[2]);

                    wo.setUnitAmountWithoutTax(null);
                    wo.setUnitAmountWithTax(null);
                    wo.setUnitAmountTax(null);
                }

                woi++;
                wos.add(wo);

                // Record in charge instance the last tariff applied
                if (wo.getChargeInstance() != null) {
                    Double lastTariff = (Double) wo.getChargeInstance().getCfValue(ChargeInstanceCFsEnum.TARIFF.name());

                    if (lastTariff == null || lastTariff.compareTo(tariffToApply.doubleValue()) != 0) {
                        BigDecimal[] amounts = NumberUtils.computeDerivedAmounts(tariffToApply, tariffToApply, wo.getTaxPercent(), appProvider.isEntreprise(), appProvider.getRounding(),
                            appProvider.getRoundingMode().getRoundingMode());
                        wo.getChargeInstance().setCfValue(ChargeInstanceCFsEnum.TARIFF.name(), tariffToApply);
                        wo.getChargeInstance().setCfValue(ChargeInstanceCFsEnum.TARIFF_WITH_TAX.name(), amounts[1]);

                    }
                }

                if (exitTariff) {
                    break;
                }
            }
        }
        return wos;
    }

    /**
     * Split service configuration into separate periods by change in quantity and subsequently split into sub periods when service parameters change
     *
     * @param wo Wallet operation
     * @return A list of service parameter groups
     */
    @SuppressWarnings("unchecked")
    protected List<ServiceRatingParameterGroup> determineRatingGroups(WalletOperation wo, boolean isOneShot) {

        List<ServiceRatingParameterGroup> ratingGroups = new LinkedList<>();

        boolean ignoreLastCFPeriod = false;

        // One shot rating - only one service configuration will apply.
        // Quantity is taken from a charge definition (wo.quantity).
        // Service activation will result in a charge instance with a quantity to activate.
        // Service quantity update will result in separate charge instance with a delta quantity.
        if (isOneShot) {

            // For terminated services, the last CF period in SERVICE_PARAMETERS marks termination date and has no duration.
            // But in case of termination charge charge application and termination date might match.
            // Therefore the last CF period should be ignored and the second to last CF value should have its end date removed
            ignoreLastCFPeriod = wo.getChargeInstance() instanceof TerminationChargeInstance || wo.getChargeMode().isReimbursement();

            // Special case when provide and cease order is on the same date
            boolean useLastKnownCFPeriod = wo.getChargeMode() == ChargeApplicationModeEnum.RERATING || wo.getChargeMode() == ChargeApplicationModeEnum.RERATING_REIMBURSEMENT;

            // Get custom field values omitting periods that have no duration and handling terminated services
            // Only for OTHER type charge of MISC type service chargeInstance is null
            // No date is used for CF value on a charge
            List<CustomFieldValue> cfValues = getServiceParameters(wo.getServiceInstance() != null ? wo.getServiceInstance() : wo.getChargeInstance(), wo.getOperationDate(), null, ignoreLastCFPeriod,
                useLastKnownCFPeriod);

            if (cfValues != null && cfValues.size() > 0) {

                Map<String, Object> serviceParams = serviceParamsCFT.deserializeMultiValue(((Map<String, String>) cfValues.get(0).getValue()).values().iterator().next(), null);
                if (serviceParams != null) {
                    ratingGroups.add(new ServiceRatingParameterGroup(wo.getQuantity(), wo.getQuantity(), Arrays.asList(new ServiceRatingParameters(wo.getOperationDate(), null, serviceParams))));
                }
            }

            // For recurring - multiple service configuration might apply
            // Quantity is stored as part of a versioned SERVICE_PARAMETERS custom field
            // Need to split rating period into sub periods everytime quantity or other service parameters change
        } else {

            BigDecimal lastQuantity = null;// BigDecimal.ZERO;

            // For terminated services, the last CF period in SERVICE_PARAMETERS marks termination date and has no duration.
            // But in case of reimbursement the charge might still have to be calculated past the termination date until the last charge date if charge was applied in advance.
            // Therefore the last CF period should be ignored and the second to last CF value should have its end date removed
            ignoreLastCFPeriod = wo.getChargeMode() != null && wo.getChargeMode().isReimbursement();

            if (wo.getServiceInstance().getCfValues() != null && wo.getServiceInstance().getCfValues().getValuesByCode() != null) {

                // Get custom field values omitting periods that have no duration and handling terminated services
                List<CustomFieldValue> cfValues = getServiceParameters(wo.getServiceInstance(), wo.getStartDate(), wo.getEndDate(), ignoreLastCFPeriod, true);

                int i = 0;
                int totalItems = cfValues.size();
                for (CustomFieldValue cfv : cfValues) {

                    Map<String, Object> serviceParams = serviceParamsCFT.deserializeMultiValue((String) cfv.getMapValue().values().iterator().next(), null);

                    BigDecimal quantity = null;
                    try {
                        quantity = new BigDecimal((String) serviceParams.get(ServiceParametersCFEnum.QUANTITY.getLabel()));

                    } catch (NumberFormatException e) {
                        throw new NoChargeException(
                            "Unable to determine quantity for " + (wo.getServiceInstance() != null ? "service " + wo.getServiceInstance().getCode() : "charge " + wo.getChargeInstance().getCode()) + " and date "
                                    + (isOneShot ? wo.getOperationDate() : wo.getStartDate() + "-" + wo.getEndDate() + ". Quantity is set to " + serviceParams.get(ServiceParametersCFEnum.QUANTITY.getLabel())));
                    }

                    if (lastQuantity == null || quantity.compareTo(lastQuantity) != 0) {

                        Date startDate = cfv.getPeriod().getFrom().compareTo(wo.getStartDate()) <= 0 ? wo.getStartDate() : cfv.getPeriod().getFrom();
                        Date endDate = i == totalItems - 1 && cfv.getPeriod().getTo() != null && cfv.getPeriod().getTo().compareTo(wo.getEndDate()) < 0 ? cfv.getPeriod().getTo() : wo.getEndDate();
                        ServiceRatingParameterGroup paramGroup = new ServiceRatingParameterGroup(quantity.subtract(lastQuantity == null ? BigDecimal.ZERO : lastQuantity), quantity,
                            splitByParameterChange(startDate, endDate, cfValues.subList(i, totalItems)));
                        if (!paramGroup.parameters.isEmpty()) {
                            ratingGroups.add(paramGroup);
                        }
                        lastQuantity = quantity;
                    }
                    i++;
                }
            }
        }

        if (ratingGroups.isEmpty()) {

            log.error("Unable to determine service attributes for {} {} {} and date {}. Current service CF values are: {}", ignoreLastCFPeriod ? isOneShot ? "termination" : "reimbursement" : "regular",
                isOneShot ? "one shot" : "recurring", (wo.getServiceInstance() != null ? "service " + wo.getServiceInstance().getCode() : "charge " + wo.getChargeInstance().getCode()),
                (isOneShot ? DateUtils.formatDateWithPattern(wo.getOperationDate(), DateUtils.DATE_PATTERN)
                        : DateUtils.formatDateWithPattern(wo.getStartDate(), DateUtils.DATE_PATTERN) + " - " + DateUtils.formatDateWithPattern(wo.getEndDate(), DateUtils.DATE_PATTERN)),
                wo.getServiceInstance() != null ? wo.getServiceInstance().getCfValues().getValuesByCode() : wo.getChargeInstance().getCfValues().getValuesByCode());

            throw new NoChargeException("Unable to determine service attributes for " + (wo.getServiceInstance() != null ? "service " + wo.getServiceInstance().getCode() : "charge " + wo.getChargeInstance().getCode())
                    + " and date " + (isOneShot ? DateUtils.formatDateWithPattern(wo.getOperationDate(), DateUtils.DATE_PATTERN)
                            : DateUtils.formatDateWithPattern(wo.getStartDate(), DateUtils.DATE_PATTERN) + " - " + DateUtils.formatDateWithPattern(wo.getEndDate(), DateUtils.DATE_PATTERN)));
        }

        return ratingGroups;
    }

    /**
     * Split further into multiple sub-periods whenever service parameters change
     *
     * @param minDateFrom Date period to split - start date
     * @param maxDateTo Date period to split - end date
     * @param cfValueList Custom field periods
     * @return A list of split date periods in ascending order of date.
     */
    private List<ServiceRatingParameters> splitByParameterChange(Date minDateFrom, Date maxDateTo, List<CustomFieldValue> cfValueList) {

        if (cfValueList.isEmpty()) {
            return null;
        }

        int size = cfValueList.size();
        List<ServiceRatingParameters> ratingParameters = new LinkedList<>();

        CustomFieldValue lastCfv = cfValueList.get(size - 1);
        Date endDate = lastCfv.getPeriod().getTo() != null && maxDateTo.compareTo(lastCfv.getPeriod().getTo()) > 0 ? lastCfv.getPeriod().getTo() : maxDateTo;
        Date startDate = lastCfv.getPeriod().getFrom();
        if (lastCfv.getMapValue() == null || lastCfv.getMapValue().isEmpty()) {
            throw new NoChargeException(
                "Unable to determine service attributes for dates " + DateUtils.formatDateWithPattern(minDateFrom, DateUtils.DATE_PATTERN) + "-" + DateUtils.formatDateWithPattern(maxDateTo, DateUtils.DATE_PATTERN));
        }

        Map<String, Object> lastParameters = serviceParamsCFT.deserializeMultiValue((String) lastCfv.getMapValue().values().iterator().next(), null);

        for (int i = size - 2; i >= 0; i--) {

            CustomFieldValue cfv = cfValueList.get(i);
            Map<String, Object> serviceParams = serviceParamsCFT.deserializeMultiValue((String) cfv.getMapValue().values().iterator().next(), null);

            if (!isServiceParametersEquals(lastParameters, serviceParams)) {
                if (startDate.compareTo(endDate) < 0) {
                    ratingParameters.add(new ServiceRatingParameters(startDate, endDate, lastParameters));
                }
                endDate = startDate;
            }
            lastParameters = serviceParams;
            startDate = cfv.getPeriod().getFrom();
        }

        // Skip same day configuration e.g. service termination
        if (minDateFrom.compareTo(endDate) < 0) {
            ratingParameters.add(new ServiceRatingParameters(minDateFrom, endDate, lastParameters));
        }

        ratingParameters.sort(Comparator.comparing(ServiceRatingParameters::getPeriod));

        return ratingParameters;
    }

    /**
     * Get service parameters from service instance for a given Wallet operation duration and handling service termination case when last CF period marks termination data, yet charges have to be reimbursed after the
     * termination date<br/>
     * <br/>
     * 
     * 
     * For terminated services, the last CF period in SERVICE_PARAMETERS marks termination date and has no duration. <br/>
     * But in case of reimbursement the charge might still have to be calculated past the termination date until the last charge date if charge was applied in advance. <br/>
     * Therefore the last CF period should be ignored and the second to last CF value should have its end date removed
     * 
     *
     * @param serviceOrChargeInstance Service or charge instance containing the parameters
     * @param startDate Date range to match - start date
     * @param endDate Date range to match - end date
     * @param ignoreLastCFPeriod If true, the last CF period should be ignored and the second to last CF value should have its end date removed
     * @param useLastKnownValue User last known service parameter value if no period was matched.
     * @return A list of custom field values
     */
    private List<CustomFieldValue> getServiceParameters(ICustomFieldEntity serviceOrChargeInstance, Date startDate, Date endDate, boolean ignoreLastCFPeriod, boolean useLastKnownValue) {

        List<CustomFieldValue> cfValues = serviceOrChargeInstance.getCfValues().getValuesByCode().get(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());

//        // TODO IF timestamp is added to service parameters, cfValues should be stripped of time before doing further processing
//        List<CustomFieldValue> cfValuesClone = new ArrayList<CustomFieldValue>();
//        for (CustomFieldValue cfValue : cfValues) {
//            CustomFieldValue cfValueClone = cfValue.clone();
//            cfValueClone.setPeriod(new DatePeriod(cfValue.getPeriod().getFrom() == null ? null : DateUtils.truncateTime(cfValue.getPeriod().getFrom()),
//                cfValue.getPeriod().getTo() == null ? null : DateUtils.truncateTime(cfValue.getPeriod().getTo())));
//            cfValuesClone.add(cfValueClone);
//        }
//        cfValues = cfValuesClone;

        // Omit any periods that have no duration.
        // One case that it will happen is for storing termination parameters that are stored as CF SERVICE_PARAMETERS value with start and end date being the same
        cfValues = cfValues.stream().filter(cfv -> cfv.getPeriod() == null || (cfv.getPeriod().isCorrespondsToPeriod(startDate, endDate, false) && !cfv.getPeriod().hasNoDuration()))
            .sorted(Comparator.comparing(CustomFieldValue::getPeriod)).collect(Collectors.toList());

        // For terminated services, the last CF period in SERVICE_PARAMETERS marks termination date and has no duration.
        // But in case of reimbursement the charge might still have to be calculated past the termination date until the last charge date if charge was applied in advance.
        // Therefore the last CF period should be ignored and the second to last CF value should have its end date removed
        if (ignoreLastCFPeriod) {

            if (!cfValues.isEmpty()) {
                int lastIndex = cfValues.size() - 1;
                CustomFieldValue cfValueClone = cfValues.get(lastIndex).clone();
                cfValueClone.setPeriod(new DatePeriod(cfValues.get(lastIndex).getPeriod().getFrom(), null));
                cfValues.set(lastIndex, cfValueClone);

            } else {
                cfValues = serviceOrChargeInstance.getCfValues().getValuesByCode().get(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());

//              // TODO IF timestamp is added to service parameters, cfValues should be stripped of time before doing further processing
//              List<CustomFieldValue> cfValuesClone = new ArrayList<CustomFieldValue>();
//              for (CustomFieldValue cfValue : cfValues) {
//                  CustomFieldValue cfValueClone = cfValue.clone();
//                  cfValueClone.setPeriod(new DatePeriod(cfValue.getPeriod().getFrom() == null ? null : DateUtils.truncateTime(cfValue.getPeriod().getFrom()),
//                      cfValue.getPeriod().getTo() == null ? null : DateUtils.truncateTime(cfValue.getPeriod().getTo())));
//                  cfValuesClone.add(cfValueClone);
//              }
//              cfValues = cfValuesClone;

                cfValues = cfValues.stream().filter(cfv -> cfv.getPeriod() == null || !cfv.getPeriod().hasNoDuration()).sorted(Comparator.comparing(CustomFieldValue::getPeriod)).collect(Collectors.toList());
                if (cfValues != null && cfValues.size() > 0) {
                    int lastIndex = cfValues.size() - 1;
                    CustomFieldValue cfValueClone = cfValues.get(lastIndex).clone();
                    cfValueClone.setPeriod(new DatePeriod(cfValues.get(lastIndex).getPeriod().getFrom(), null));
                    cfValues = Arrays.asList(cfValueClone);
                }
            }
        }

        // Special case when provide and cease order is on the same date
        if (cfValues.isEmpty() && useLastKnownValue) {
            cfValues = serviceOrChargeInstance.getCfValues().getValuesByCode().get(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());
            int lastIndex = cfValues.size() - 1;
            CustomFieldValue cfValueClone = cfValues.get(lastIndex).clone();
            cfValueClone.setPeriod(new DatePeriod(cfValues.get(lastIndex).getPeriod().getFrom(), null));
            cfValues = Arrays.asList(cfValueClone);
        }
        return cfValues;
    }

    /**
     * Get the latest Service parameters custom field value
     * 
     * @param serviceOrChargeInstance Service or charge instance containing the parameters
     * @return The latest Service parameters custom field value parsed as Map
     */
    @SuppressWarnings("unchecked")
    private Map<String, Object> getLastServiceParameters(ICustomFieldEntity serviceOrChargeInstance) {

        List<CustomFieldValue> cfValues = serviceOrChargeInstance.getCfValues().getValuesByCode().get(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());
        CustomFieldValue cfv = cfValues.stream().sorted(Comparator.comparing(CustomFieldValue::getPeriod).reversed()).findFirst().get();

        Map<String, Object> serviceParams = serviceParamsCFT.deserializeMultiValue(((Map<String, String>) cfv.getValue()).values().iterator().next(), null);

        return serviceParams;
    }

    /**
     * Compare two service parameters. Ignores quantity field
     *
     * @param one Service parameters are a map
     * @param two parameters are a map
     * @return True if two service parameters contains same values ignoring a quantity field
     */
    private boolean isServiceParametersEquals(Map<String, Object> one, Map<String, Object> two) {
        if (one == two) {
            return true;
        }

        if (one.size() != two.size())
            return false;

        try {
            Iterator<Map.Entry<String, Object>> i = two.entrySet().iterator();
            while (i.hasNext()) {
                Map.Entry<String, Object> e = i.next();
                String key = e.getKey();
                // Ignore quantity field
                if (key.equals(ServiceParametersCFEnum.QUANTITY.getLabel())) {
                    continue;
                }
                Object value = e.getValue();
                if (value == null) {
                    if (!(one.get(key) == null && one.containsKey(key)))
                        return false;
                } else {
                    if (!value.equals(one.get(key)))
                        return false;
                }
            }
        } catch (ClassCastException unused) {
            return false;
        } catch (NullPointerException unused) {
            return false;
        }

        return true;
    }

    /**
     * Apply flat discounts to a wallet operation
     *
     * @param wo Wallet operation
     */
    protected void applyFlatDiscounts(WalletOperation wo) {

        // Get all matching flat discounts
        List<Map<String, Object>> discountsToApply = getMatchingFlatDiscounts(wo);

        // Apply flat discounts
        if (discountsToApply != null && !discountsToApply.isEmpty()) {
            applyMatchingDiscounts(wo, discountsToApply);
        }
    }

    /**
     * apply matching discounts
     *
     * @param wo
     * @param flatDiscounts
     */
    protected void applyMatchingDiscounts(WalletOperation wo, List<Map<String, Object>> flatDiscounts) {

        BigDecimal unitAmount = wo.getUnitAmountWithoutTax();
        BigDecimal totalAmount = wo.getAmountWithoutTax();

        boolean isTotalAmountOverdriven = unitAmount == null;

        List<BigDecimal> discounts = flatDiscounts.stream().map(d -> (BigDecimal) d.get(DISCOUNT_PERCENT)).filter(percent -> percent != null && percent.compareTo(BigDecimal.ZERO) > 0)
            .map(percent -> percent.divide(BigDecimal.valueOf(100))).collect(Collectors.toList());

        for (BigDecimal discount : discounts) {
            if (isTotalAmountOverdriven) {
                totalAmount = totalAmount.multiply(BigDecimal.ONE.subtract(discount));
            } else {
                unitAmount = unitAmount.multiply(BigDecimal.ONE.subtract(discount));
            }
        }

        wo.setRawAmountWithoutTax(wo.getAmountWithoutTax());
        wo.setRawAmountWithTax(wo.getAmountWithTax());

        if (isTotalAmountOverdriven) {
            wo.setAmountWithoutTax(totalAmount);
            wo.computeDerivedAmounts(appProvider.isEntreprise(), appProvider.getRounding(), appProvider.getRoundingMode());

        } else {
            ratingService.calculateAmounts(wo, unitAmount, unitAmount);
        }

    }

    /**
     * Get the matching flat discounts to apply to a wallet operation
     *
     * @param wo Wallet operation
     * @return A list of discount percents to apply
     */
    private List<Map<String, Object>> getMatchingFlatDiscounts(WalletOperation wo) {

        Map<String, Object> searchCriteria = new HashMap<>();

        boolean isOneShot = wo.getChargeInstance() instanceof OneShotChargeInstance;

        ServiceTemplate serviceTemplate = null;
        if (wo.getServiceInstance() != null) {
            serviceTemplate = wo.getServiceInstance().getServiceTemplate();
        } else {
            serviceTemplate = getServiceTemplateFromChargeInstanceCF(wo.getChargeInstance());
        }

        // Construct flat discount record general search criteria
        searchCriteria.put("customer_account", wo.getBillingAccount().getCustomerAccount().getId());
        searchCriteria.put("eqOptional billing_account", wo.getBillingAccount().getId());
        searchCriteria.put("eqOptional service_id", serviceTemplate.getId());
        searchCriteria.put("charge_type", isOneShot ? ChargeTypeEnum.O.name() : ChargeTypeEnum.R.name());
        EntityReferenceWrapper productSet = (EntityReferenceWrapper) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.PRODUCT_SET.name());
        if (productSet != null) {
            searchCriteria.put("eqOptional product_set", Long.parseLong(productSet.getCode()));
        }

        // For one shot look up by a single date
        if (isOneShot) {
            searchCriteria.put("minmaxOptionalRange valid_from valid_to", wo.getOperationDate());
        } else {
            searchCriteria.put("minmaxOptionalRange valid_from valid_to", wo.getStartDate());
        }

        // Get all discounts
        List<Map<String, Object>> flatDiscounts = customTableService.list("EIR_OR_FLAT_DISCOUNT", new PaginationConfiguration(searchCriteria));

        // check migration code partial match

        Iterator<Map<String, Object>> iterator = flatDiscounts.iterator();
        while (iterator.hasNext()) {
            Map<String, Object> flatDiscount = iterator.next();
            String partialMigrationCode = ((String) flatDiscount.get("migration_code")).replace("*", "");
            String migrationCode = (String) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.MIGRATION_CODE.name());
            if (migrationCode != null && !migrationCode.startsWith(partialMigrationCode)) {
                iterator.remove();
            }
        }

        // sort by start dates
        Comparator<Map<String, Object>> comparator = Comparator.comparing(discount -> (Date) discount.get(DISCOUNT_VALID_FROM));
        flatDiscounts.sort(comparator.reversed());

        return flatDiscounts;
    }

    /**
     * Get a service template from a charge instance custom field
     *
     * @param chargeInstance Charge instance
     * @return Service template
     */
    protected ServiceTemplate getServiceTemplateFromChargeInstanceCF(ChargeInstance chargeInstance) {

        String serviceCode = (String) chargeInstance.getCfValue(ChargeInstanceCFsEnum.SERVICE_CODE.name());
        if (StringUtils.isBlank(serviceCode)) {
            throw new BusinessException("Service code was not specified for a Miscelaneous type charge");
        }

        ServiceTemplate serviceTemplate = serviceTemplateService.findByCode(serviceCode);
        if (serviceTemplate == null) {
            throw new BusinessException("Service " + serviceCode + " was not found");
        }

        return serviceTemplate;
    }

    /**
     * Compute prorating ratio by comparing two sets of dates
     *
     * @param from Date range to prorate period against (bigger period) - from
     * @param to Date range to prorate period against (bigger period) - to
     * @param periodFrom Period to calculate proration % for - from
     * @param periodTo Period to calculate proration % for - to
     * @return A prorating ratio
     */
    private double computeProratingRatio(Date from, Date to, Date periodFrom, Date periodTo) {

        from = DateUtils.truncateTime(from);
        periodFrom = DateUtils.truncateTime(periodFrom);

        boolean periodStartEarlier = periodFrom == null || periodFrom.compareTo(from) <= 0;
        boolean periodEndLater = periodTo == null || periodTo.compareTo(to) >= 0;

        if (periodStartEarlier && periodEndLater) {
            return 1;

        } else {
            double lengthTotal = DateUtils.daysBetween(from, to);
            double periodLength = 0;

            if (periodStartEarlier && !periodEndLater) {
                periodLength = DateUtils.daysBetween(from, periodTo);
            } else if (!periodStartEarlier && periodEndLater) {
                periodLength = DateUtils.daysBetween(periodFrom, to);
            } else {
                periodLength = DateUtils.daysBetween(periodFrom, periodTo);
            }

            if (lengthTotal == 0d) {
                return 0d;
            }
            return periodLength / lengthTotal;
        }
    }

    /**
     * Compute prorating ratio by comparing two sets of dates
     *
     * @param from Date range to prorate period against (bigger period) - from
     * @param to Date range to prorate period against (bigger period) - to
     * @param periodFrom Period to calculate proration % for - from
     * @param periodTo Period to calculate proration % for - to
     * @return A prorating ratio in month
     */
    private double computeProratingRatioInMonth(Date from, Date to, Date periodFrom, Date periodTo) {

        from = DateUtils.truncateTime(from);
        periodFrom = DateUtils.truncateTime(periodFrom);

        boolean periodStartEarlier = periodFrom == null || periodFrom.compareTo(from) <= 0;
        boolean periodEndLater = periodTo == null || periodTo.compareTo(to) >= 0;

        if (periodStartEarlier && periodEndLater) {
            return 1;

        } else {
            double lengthTotal = DateUtils.monthsBetween(from, to);
            double periodLength = 0;

            if (periodStartEarlier && !periodEndLater) {
                periodLength = DateUtils.monthsBetween(from, periodTo);
            } else if (!periodStartEarlier && periodEndLater) {
                periodLength = DateUtils.monthsBetween(periodFrom, to);
            } else {
                periodLength = DateUtils.monthsBetween(periodFrom, periodTo);
            }

            if (periodLength == 0d && periodTo.after(periodFrom)) {
                periodLength = 1;
            }

            if (lengthTotal == 0d && to.after(from)) {
                periodLength = 1;
            } else if (lengthTotal == 0d) {
                return 0d;
            }
            return periodLength / lengthTotal;
        }
    }

    /**
     * Determine applicable tariff records grouped by validity date
     *
     * @param customer Customer
     * @param offerTemplate Offer template
     * @param serviceTemplate Service template
     * @param chargeType Charge type
     * @param totalQuantity Total quantity being rated. Note: this might differ when quantity to rate as set on wo.quantity is a delta quantity. E.g. quantity change from 5 to 7 will result in two WOs to rate with
     *        quantity of 5 and 2. Yet for quantity based services, tariff must be looked up by a total quantity amount.
     * @param operationDate Effective date to look up tariff plan for. WO.operationDate
     * @param period Period to date that WO corresponds to. Recurring charges only.
     * @param serviceParams Service configuration parameters
     * @param isQuantityBased Is this a quantity (GNP) based service
     * @return A list of matching tariff records, grouped by validity dates
     * @throws NoPricePlanException No tariff records were found or distance rating tariffs found for a non-distance service
     * @throws NoChargeException Unable to determine tariff criteria fields or a tariff plan
     */
    @SuppressWarnings({ "unchecked" })
    public List<TariffRecordGroup> getMatchingTariffRecords(Customer customer, OfferTemplate offerTemplate, ServiceTemplate serviceTemplate, ChargeTypeEnum chargeType, BigDecimal totalQuantity, Date operationDate,
            DatePeriod period, Map<String, Object> serviceParams, boolean isQuantityBased) throws NoPricePlanException, NoChargeException {

        Map<String, Object> searchCriteria = new HashMap<>();

        // -- Determine price plan applicable - use main WO operation date as reference date
        Object rapCustomer = customer.getCfValue("STANDARD_CUSTOMER");
        if (rapCustomer == null || (boolean) rapCustomer) {
            searchCriteria.put("customer_id", PersistenceService.SEARCH_IS_NULL);
        } else {
            searchCriteria.put("eqOptional customer_id", customer.getId());
        }
        searchCriteria.put("offer_id", offerTemplate.getId());
        searchCriteria.put("minmaxOptionalRange valid_from valid_to", operationDate);

        List<Map<String, Object>> tariffPlans = customTableService.list("EIR_OR_TARIFF_PLAN_MAP", new PaginationConfiguration(searchCriteria, "customer_id", SortOrder.ASCENDING));
        if (tariffPlans.isEmpty()) {
            throw new NoChargeException("Unable to determine a tariff plan for customer " + customer.getCode() + " offer " + offerTemplate.getCode() + " and date " + DateUtils.formatAsDate(operationDate));
        }

        Number tariffPlanId = (Number) tariffPlans.get(0).get("tariff_plan_id");

        // -- Look up applicable tariffs
        searchCriteria = new HashMap<>();
        boolean isOneShot = chargeType.isOneShot();

        // Construct tariff record general search criteria
        Long serviceTemplateId = serviceTemplate.getId();
        List<TariffSearchCriteria> tariffSearchCriteria = new ArrayList<>();
        tariffSearchCriteria.add(new TariffSearchCriteria("Tariff plan", tariffPlanId, ValidTariffCustomTableEnum.TARIFF_PLAN_ID.getLabel(), tariffPlanId));
        tariffSearchCriteria.add(new TariffSearchCriteria("Offer", offerTemplate.getCode() + "/" + offerTemplate.getId(), ValidTariffCustomTableEnum.OFFER_ID.getLabel(), offerTemplate.getId()));
        tariffSearchCriteria.add(new TariffSearchCriteria("Service", serviceTemplate.getCode() + "/" + serviceTemplateId, ValidTariffCustomTableEnum.SERVICE_ID.getLabel(), serviceTemplateId));
        tariffSearchCriteria.add(new TariffSearchCriteria("Charge type", chargeType.name(), ValidTariffCustomTableEnum.CHARGE_TYPE.getLabel(), chargeType.name()));

        searchCriteria.put(ValidTariffCustomTableEnum.TARIFF_PLAN_ID.getLabel(), tariffPlanId);
        searchCriteria.put(ValidTariffCustomTableEnum.OFFER_ID.getLabel(), offerTemplate.getId());
        searchCriteria.put(ValidTariffCustomTableEnum.SERVICE_ID.getLabel(), serviceTemplateId);
        searchCriteria.put(ValidTariffCustomTableEnum.CHARGE_TYPE.getLabel(), chargeType.name());

        // For one shot look up by a single date
        if (isOneShot) {
            tariffSearchCriteria.add(new TariffSearchCriteria("Validity period", operationDate, "minmaxOptionalRange valid_from valid_to", operationDate));
            searchCriteria.put("minmaxOptionalRange valid_from valid_to", operationDate);
            // For recurring lookup by a date range
        } else {
            tariffSearchCriteria
                .add(new TariffSearchCriteria("Validity period", new DatePeriod(period.getFrom(), period.getTo()), "overlapOptionalRange valid_from valid_to", new Date[] { period.getFrom(), period.getTo() }));
            searchCriteria.put("overlapOptionalRange valid_from valid_to", new Date[] { period.getFrom(), period.getTo() });
        }

        OfferTemplateCategory offerCategory = offerTemplate.getOfferTemplateCategories().get(0);

        String offerType = (String) offerCategory.getCfValue("offerType");

        // Construct tariff record service type specific search criteria

        Object[] sortOrdering = new Object[] { ValidTariffCustomTableEnum.PRIORITY.getLabel(), SortOrder.ASCENDING, ValidTariffCustomTableEnum.VALID_FROM.getLabel(), SortOrder.DESCENDING };

        boolean isDistanceBased = !StringUtils.isBlank((String) serviceParams.get(ServiceParametersCFEnum.DISTANCE.getLabel()));

        // Quantity based MISC service US 273. Filter by a total quantity and not what WO corresponds to
        if (isQuantityBased) {
            tariffSearchCriteria.add(new TariffSearchCriteria("Quantity from/to", totalQuantity, "minmaxOptionalRangeInclusive quantity_from quantity_to", totalQuantity));
            searchCriteria.put("minmaxOptionalRangeInclusive quantity_from quantity_to", totalQuantity);
        }

        // Determine the fields for searching
        String[] fields = getTariffSearchFields(offerType, serviceTemplate);
        if (fields == null) {
            throw new NoChargeException("Can not find tariff criteria mapping for service template " + serviceTemplate.getCode());
        }

        // For distance based services
        if (isDistanceBased) {
            tariffSearchCriteria.add(new TariffSearchCriteria("Distance to", serviceParams.get(ServiceParametersCFEnum.DISTANCE.getLabel()), "SQL_distance_to",
                "(distance_to IS NULL or distance_to <= " + serviceParams.get(ServiceParametersCFEnum.DISTANCE.getLabel()) + " or (distance_from <= " + serviceParams.get(ServiceParametersCFEnum.DISTANCE.getLabel())
                        + " and " + serviceParams.get(ServiceParametersCFEnum.DISTANCE.getLabel()) + "<=distance_to))"));
            searchCriteria.put("SQL_distance_to", "(distance_to IS NULL or distance_to <= " + serviceParams.get(ServiceParametersCFEnum.DISTANCE.getLabel()) + " or (distance_from <= "
                    + serviceParams.get(ServiceParametersCFEnum.DISTANCE.getLabel()) + " and " + serviceParams.get(ServiceParametersCFEnum.DISTANCE.getLabel()) + "<=distance_to))");
        } else {
            tariffSearchCriteria.add(new TariffSearchCriteria("Distance from", PersistenceService.SEARCH_IS_NULL, "distance_from", PersistenceService.SEARCH_IS_NULL));
            searchCriteria.put("distance_from", PersistenceService.SEARCH_IS_NULL);
        }

        StringBuffer criteriaSql = null;
        int i = 0;
        for (String field : fields) {

            Object fieldValue = null;

            // Convert exchange code stored as service parameter to an Exchange class
            if (field.equals("exchange_class")) {

                String exchangeCode = (String) serviceParams.get(ServiceParametersCFEnum.EXCHANGE.getLabel());
                if (exchangeCode != null) {
                    Map<String, String> exchangeDetails = (Map<String, String>) customFieldInstanceService.getCFValueByKey(appProvider, "EXCHANGE_DETAILS", operationDate, exchangeCode);
                    if (exchangeDetails != null) {
                        fieldValue = exchangeDetails.get("Exchange_Class");
                    } else {
                        throw new RatingException("Exchange class not found matching exchange code " + serviceParams.get(ServiceParametersCFEnum.EXCHANGE.getLabel()) + " and date "
                                + DateUtils.formatDateWithPattern(operationDate, DateUtils.DATE_PATTERN));
                    }
                }

            } else {
                fieldValue = serviceParams.get(field);
            }

            if (criteriaSql != null) {
                criteriaSql.append(" and ");
            } else {
                criteriaSql = new StringBuffer();
            }

            if (fieldValue == null) {
                tariffSearchCriteria.add(new TariffSearchCriteria(field, "%/<null>", PersistenceService.SEARCH_SQL, "(" + field + "='%' or " + field + "='<null>')"));
                criteriaSql.append("(").append(field).append("='%' or ").append(field).append("='<null>')");
            } else {
                tariffSearchCriteria.add(new TariffSearchCriteria(field, "*/%/" + fieldValue, PersistenceService.SEARCH_SQL, "(" + field + "='*' or " + field + "='%' or " + field + "='" + fieldValue + "')"));
                criteriaSql.append("(").append(field).append("='*' or ").append(field).append("='%' or ").append(field).append("='").append(fieldValue).append("')");
            }
            i = i + 2;
        }

        if (criteriaSql != null) {
            searchCriteria.put(PersistenceService.SEARCH_SQL, criteriaSql.toString());
        }

        List<Map<String, Object>> tariffRecords = customTableService.list(CustomTableEnum.EIR_OR_TARIFF.getCode(), new PaginationConfiguration(null, null, searchCriteria, null, null, sortOrdering));
        if (tariffRecords.isEmpty()) {
            throwNoTariffFoundException(tariffSearchCriteria);

        } else if (log.isDebugEnabled()) {
            log.debug("Searched for tariffs using criteria: {}", constructNoTariffFoundMessage(tariffSearchCriteria));
        }

        return convertToTariffRecordGroups(tariffRecords, isOneShot, isDistanceBased, serviceTemplate.getCode(), operationDate, period);
    }

    /**
     * Construct and throw an exception indicating that no tariff was found
     *
     * @param tariffSearchCriteria Tariff search criteria
     * @throws NoPricePlanException An constructed exception with a message containing the minimum criteria applied that resulted in no tariffs matched
     */
    private void throwNoTariffFoundException(List<TariffSearchCriteria> tariffSearchCriteria) throws NoPricePlanException {

        // Lookup tariff plan code
        for (TariffSearchCriteria criteria : tariffSearchCriteria) {
            if (criteria.criteriaField.equals("tariff_plan_id")) {
                Map<String, Object> tariffPlanRecord = customTableService.findById("eir_or_tariff_plan", ((Number) criteria.criteriaValue).longValue());
                criteria.values = tariffPlanRecord.get("code") + "/" + tariffPlanRecord.get("id");
                break;
            }
        }

        Map<String, Object> searchCriteria = new HashMap<>();

        for (int i = 0; i < tariffSearchCriteria.size(); i++) {
            TariffSearchCriteria tariffCriteria = tariffSearchCriteria.get(i);
            if (PersistenceService.SEARCH_SQL.equals(tariffCriteria.criteriaField) && searchCriteria.containsKey(PersistenceService.SEARCH_SQL)) {
                searchCriteria.put(PersistenceService.SEARCH_SQL, searchCriteria.get(PersistenceService.SEARCH_SQL) + " and " + tariffCriteria.criteriaValue);
            } else {
                searchCriteria.put(tariffCriteria.criteriaField, tariffCriteria.criteriaValue);
            }
            if (i > 4) {
                long itemsFound = customTableService.count(CustomTableEnum.EIR_OR_TARIFF.getCode(), new PaginationConfiguration(searchCriteria));
                if (itemsFound == 0) {
                    throw new NoPricePlanException("No tariff matched: " + constructNoTariffFoundMessage(tariffSearchCriteria.subList(0, i + 1)) + ". Remaining parameters to consider: "
                            + constructNoTariffFoundMessage(tariffSearchCriteria.subList(i + 1, tariffSearchCriteria.size())));
                }
            }
        }
    }

    /**
     * Construct a message for no tariff found given the search criteria
     *
     * @param tariffSearchCriteria Tariff search criteria
     * @return A message containing concatenated field=value pairs
     */
    private String constructNoTariffFoundMessage(List<TariffSearchCriteria> tariffSearchCriteria) {

        StringBuffer message = new StringBuffer();

        for (TariffSearchCriteria criteria : tariffSearchCriteria) {
            if (message.length() > 0) {
                message.append(", ");
            }

            Object value = criteria.values;
            if (value instanceof Date) {
                value = DateUtils.formatDateWithPattern((Date) value, DateUtils.DATE_PATTERN);
            } else if (value instanceof DatePeriod) {
                value = ((DatePeriod) value).toString(DateUtils.DATE_PATTERN);
            }

            message.append(criteria.fieldname).append('=').append(value);
        }

        return message.toString();
    }

    /**
     * Convert tariff record lines to tariff record groups
     *
     * @param tariffRecords Tariff record lines. Expected to be already sorted by the importance of matching. Newer tariffs are given a priority over older records for the same priority
     * @param isOneShot Is this one shot operation
     * @param isDistanceBased Is this distance based service
     * @param serviceCode Service (code) being rated
     * @param operationDate Rating date
     * @param period Rating period
     * @return A list of tariff record groups
     */
    protected List<TariffRecordGroup> convertToTariffRecordGroups(List<Map<String, Object>> tariffRecords, boolean isOneShot, boolean isDistanceBased, String serviceCode, Date operationDate, DatePeriod period) {

        Map<String, TariffRecordGroup> tariffRecordGroups = new LinkedHashMap<>();

        boolean isMultiplePerPeriod = isDistanceBased;

        // Records are ordered by valid_from date and other fields in lefToRight order based on their importance in matching
        for (Map<String, Object> tariffRecord : tariffRecords) {
            Date from = (Date) tariffRecord.get(ValidTariffCustomTableEnum.VALID_FROM.getLabel());
            Date to = (Date) tariffRecord.get(ValidTariffCustomTableEnum.VALID_TO.getLabel());

            String key = DateUtils.formatDateWithPattern(from, DateUtils.DATE_PATTERN) + "_" + DateUtils.formatDateWithPattern(to, DateUtils.DATE_PATTERN);

            if (!tariffRecordGroups.containsKey(key)) {

                // For one shot there should be only a single validity date period matched. Disregard the other records found but not matching same valid_from and valid_to value.
                if (!tariffRecordGroups.isEmpty() && isOneShot) {
                    continue;
                }

                TariffRecordGroup tariffRecordGroup = new TariffRecordGroup(from, to,
                    ChargeTypeEnum.R.name().equals(tariffRecord.get(ValidTariffCustomTableEnum.CHARGE_TYPE.getLabel())) ? ((BigInteger) tariffRecord.get(ValidTariffCustomTableEnum.REC_CALENDAR.getLabel())).longValue()
                            : null);
                tariffRecordGroups.put(key, tariffRecordGroup);

                // For not distance based, take only top-most matched record as records are ordered by fields based on their importance in matching
            } else if (!isMultiplePerPeriod) {
                continue;
            }

            TariffRecord tariffRecordObj = new TariffRecord(tariffRecord);

            if (tariffRecordObj.distanceFrom != null && !isDistanceBased) {
                throw new NoPricePlanException("Distance based tariff " + tariffRecordObj.id + " was matched for a non-distance service " + serviceCode + " and date " + operationDate);
            }

            tariffRecordGroups.get(key).addTariffRecord(tariffRecordObj);

            // Consider only the first tariff record for one shot non-distance charge
            if (isOneShot && !isMultiplePerPeriod) {
                break;
            }
        }

        // To handle a case when generic and specific prices exist and are valid for the same period
        // Split into sub-periods
        DateUtils.DatePeriodSplit[] periods = new DateUtils.DatePeriodSplit[tariffRecordGroups.size()];

        int i = 0;
        for (TariffRecordGroup tariffRecordGroup : tariffRecordGroups.values()) {
            periods[i] = new DateUtils.DatePeriodSplit(tariffRecordGroup.getValidity(), i, tariffRecordGroup);
            i++;
        }

        List<DatePeriodSplit> normalizedPeriods = DateUtils.normalizeOverlapingDatePeriods(periods);

        List<TariffRecordGroup> tariffGroups = null;
        if (isOneShot) {
            tariffGroups = new ArrayList<OneShotAndRecurringRatingScript.TariffRecordGroup>();
            tariffGroups.add(new TariffRecordGroup(normalizedPeriods.get(0)));

        } else {

            Date from = period == null ? operationDate : period.getFrom();
            Date to = period == null ? operationDate : period.getTo();
            tariffGroups = normalizedPeriods.stream().filter(np -> np.getPeriod().getFrom().before(period.getTo()) && (np.getPeriod().getTo() == null || np.getPeriod().getTo().after(period.getFrom())))
                .map(np -> new TariffRecordGroup(np)).collect(Collectors.toList());
        }

        return tariffGroups;
    }

    /**
     * Get a list of tariff search criteria fieldnames
     *
     * @param offerType Offer type
     * @return
     */
    public static final String[] getTariffSearchFields(String offerType, ServiceTemplate serviceTemplate) {

        if (TransformationRulesEnum.MISC.name().equals(offerType)) {
            return new String[] { ValidTariffCustomTableEnum.ACTION_QUALIFIER.getLabel() };
        } else if (offerType.equals(TransformationRulesEnum.ACCESS.name())) {
            return new String[] { ValidTariffCustomTableEnum.ACTION_QUALIFIER.getLabel(), ValidTariffCustomTableEnum.DENSITY.getLabel(), ValidTariffCustomTableEnum.REGION.getLabel(),
                    ValidTariffCustomTableEnum.SLA.getLabel(), ValidTariffCustomTableEnum.CLASS_OF_SERVICE.getLabel(), ValidTariffCustomTableEnum.EF.getLabel(), ValidTariffCustomTableEnum.AF.getLabel(),
                    ValidTariffCustomTableEnum.EXCHANGE_CLASS.getLabel(), ValidTariffCustomTableEnum.BANDWIDTH.getLabel(), ValidTariffCustomTableEnum.ORDER_TYPE.getLabel(),
                    ValidTariffCustomTableEnum.EGRESS_GROUP.getLabel() };

        } else if (offerType.equals(TransformationRulesEnum.DATA.name())) {

            if (ServiceTypeEnum.ANCILLARY.name().equalsIgnoreCase((String) serviceTemplate.getCfValue(ServiceTemplateCFsEnum.SERVICE_TYPE.name()))) {

                return new String[] { ValidTariffCustomTableEnum.ACTION_QUALIFIER.getLabel(), ValidTariffCustomTableEnum.PRICE_PLAN.getLabel(), ValidTariffCustomTableEnum.RATING_SCHEME.getLabel(),
                        ValidTariffCustomTableEnum.BANDWIDTH.getLabel(), ValidTariffCustomTableEnum.TRANSMISSION.getLabel(), ValidTariffCustomTableEnum.COMMITMENT_PERIOD.getLabel(),
                        ValidTariffCustomTableEnum.ZONE_A.getLabel(), ValidTariffCustomTableEnum.ZONE_B.getLabel(), ValidTariffCustomTableEnum.REGION.getLabel(), ValidTariffCustomTableEnum.SLA.getLabel(),
                        ValidTariffCustomTableEnum.SERVICE_CATEGORY.getLabel(), ValidTariffCustomTableEnum.ANALOGUE_QUALITY.getLabel(), ValidTariffCustomTableEnum.ANALOGUE_RATE.getLabel(),
                        ValidTariffCustomTableEnum.CPE_TYPE.getLabel(), ValidTariffCustomTableEnum.CPE_DESCRIPTION.getLabel(), ValidTariffCustomTableEnum.ETS_TYPE.getLabel(), ValidTariffCustomTableEnum.IND_01.getLabel(),
                        ValidTariffCustomTableEnum.IND_02.getLabel(), ValidTariffCustomTableEnum.IND_03.getLabel(), ValidTariffCustomTableEnum.IND_04.getLabel(), ValidTariffCustomTableEnum.IND_05.getLabel(),
                        ValidTariffCustomTableEnum.IND_06.getLabel(), ValidTariffCustomTableEnum.IND_07.getLabel(), ValidTariffCustomTableEnum.IND_08.getLabel(), ValidTariffCustomTableEnum.IND_09.getLabel(),
                        ValidTariffCustomTableEnum.IND_10.getLabel() };

            } else {
                return new String[] { ValidTariffCustomTableEnum.ACTION_QUALIFIER.getLabel(), ValidTariffCustomTableEnum.PRICE_PLAN.getLabel(), ValidTariffCustomTableEnum.COMMITMENT_PERIOD.getLabel() };

            }
        }
        return new String[] {};
    }

    /**
     * Get Service parameters custom field definition
     *
     * @return Custom field template
     */
    protected CustomFieldTemplate getServiceParametersCFT() {

        if (serviceParamsCFT == null) {
            CustomFieldTemplate cft = cftService.findByCodeAndAppliesTo(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), new ServiceInstance());
            cftService.detach(cft);
            serviceParamsCFT = cft;
        }

        return serviceParamsCFT;
    }

    /**
     * Get GNP product set record identifier
     *
     * @return GNP Product set record identifier
     */
    @SuppressWarnings("unchecked")
    protected String getGNPProductSet() {

        if (gnpProductSetId == null) {

            Map<String, Object> filters = new HashMap<String, Object>();
            filters.put("inList code", Arrays.asList("gnp", "gnp_"));
            PaginationConfiguration paginationConfig = new PaginationConfiguration(0, 1, filters, null, Arrays.asList("id"));
            List<BigInteger> ids = customTableService.listAsObjects("product_set", paginationConfig);
            if (!ids.isEmpty()) {
                gnpProductSetId = ids.get(0).toString();
            }
        }
        return gnpProductSetId;
    }

    /**
     * Groups several tariff lines for the same validity period
     */
    public class TariffRecordGroup {

        /**
         * Tariff lines
         */
        List<TariffRecord> tariffRecords;

        /**
         * Validity period
         */
        DatePeriod validity;

        /**
         * Recurring calendar identifier
         */
        Long recurringCalendarId;

        /**
         * Priority. The lower the number, the higher is the priority.
         */
        String priority;

        /**
         * Constructor
         *
         * @param from Validity period - date from
         * @param to Validity period - date to
         */
        TariffRecordGroup(Date from, Date to, Long recurringCalendarId) {
            this.validity = new DatePeriod(from, to);
            this.recurringCalendarId = recurringCalendarId;
            this.tariffRecords = new ArrayList<OneShotAndRecurringRatingScript.TariffRecord>();
        }

        /**
         * Constructor
         *
         * @param normalizedPeriod Normalized Tariff record groups based on priority and date period
         */
        TariffRecordGroup(DateUtils.DatePeriodSplit normalizedPeriod) {
            this.validity = normalizedPeriod.getPeriod();
            this.priority = normalizedPeriod.getPriority();
            this.recurringCalendarId = ((TariffRecordGroup) normalizedPeriod.getValue()).getRecurringCalendarId();
            this.tariffRecords = ((TariffRecordGroup) normalizedPeriod.getValue()).getTariffRecords();
        }

        /**
         * Append a tariff record. Appends only if priority is the same (records are ordered by priority already. Lower the number, higher the priority)
         *
         * @param tariffRecord Tariff record
         */
        public void addTariffRecord(TariffRecord tariffRecord) {
            if (priority == null || priority.equals(tariffRecord.priority)) {
                tariffRecords.add(tariffRecord);
                priority = tariffRecord.priority;
            }
        }

        /**
         * @return Tariff lines
         */
        public List<TariffRecord> getTariffRecords() {
            return tariffRecords;
        }

        /**
         * @return Validity period
         */
        public DatePeriod getValidity() {
            return validity;
        }

        /**
         * @return Recurring calendar identifier
         */
        public Long getRecurringCalendarId() {
            return recurringCalendarId;
        }

        /**
         * @return Priority. The lower the number, the higher is the priority.
         */
        public String getPriority() {
            return priority;
        }

        /**
         * Get the latest tariff record
         * 
         * @return The last tariff record
         */
        public TariffRecord getLastTariffRecord() {
            return tariffRecords.get(tariffRecords.size() - 1);
        }

        @Override
        public String toString() {
            return validity.toString(DateUtils.DATE_PATTERN);
        }
    }

    /**
     * Tariff line
     */
    public class TariffRecord {

        /**
         * Record identifier
         */
        Long id;

        /**
         * Tariff plan identifier
         */
        Long tariffPlanId;

        /**
         * Quantity for GNP quantity rating - from
         */
        Integer quantityFrom;

        /**
         * Quantity for GNP quantity rating - to
         */
        Integer quantityTo;

        /**
         * Quantity for GNP quantity rating - maximum rate quantity
         */
        Integer maxQuantity;

        /**
         * Distance for distance based rating - from
         */
        Integer distanceFrom;

        /**
         * Distance for distance based rating - to
         */
        Integer distanceTo;

        /**
         * Distance for distance based rating - rating unit
         */
        Integer distanceUnit;

        /**
         * Price
         */
        BigDecimal amountWithoutTax;

        /**
         * Description
         */
        String description;

        /**
         * Tax class identifier
         */
        Long taxClassId;

        /**
         * Invoice sub category identifier
         */
        Long invoiceSubCategoryId;

        /**
         * Accounting code identifier
         */
        Long accountingCodeId;

        /**
         * Record priority. The lower the number, the higher is the priority.<br/>
         * Each field receives a rating based on its value - a real value gets 1, <null> - 2, * - 3, % - 4. <br/>
         * The rating from each field is concatenated to form a single string with as many characters as there are fields.
         */
        String priority;

        /**
         * Constructor
         *
         * @param tariffRecord Tariff line from DB
         */
        public TariffRecord(Map<String, Object> tariffRecord) {

            id = ((BigInteger) tariffRecord.get(ValidTariffCustomTableEnum.ID.getLabel())).longValue();
            tariffPlanId = ((BigInteger) tariffRecord.get(ValidTariffCustomTableEnum.TARIFF_PLAN_ID.getLabel())).longValue();
            amountWithoutTax = (BigDecimal) tariffRecord.get(ValidTariffCustomTableEnum.PRICE.getLabel());
            description = (String) tariffRecord.get(ValidTariffCustomTableEnum.DESCRIPTION.getLabel());
            taxClassId = ((BigInteger) tariffRecord.get(ValidTariffCustomTableEnum.TAX_CLASS.getLabel())).longValue();
            invoiceSubCategoryId = ((BigInteger) tariffRecord.get(ValidTariffCustomTableEnum.INVOICE_SUBCAT_ID.getLabel())).longValue();
            accountingCodeId = ((BigInteger) tariffRecord.get(ValidTariffCustomTableEnum.ACCOUNTING_CODE_ID.getLabel())).longValue();
            priority = (String) tariffRecord.get(ValidTariffCustomTableEnum.PRIORITY.getLabel());

            if (tariffRecord.get(ValidTariffCustomTableEnum.DISTANCE_FROM.getLabel()) != null) {
                distanceFrom = ((BigInteger) tariffRecord.get(ValidTariffCustomTableEnum.DISTANCE_FROM.getLabel())).intValue();
                if (tariffRecord.get(ValidTariffCustomTableEnum.DISTANCE_TO.getLabel()) != null) {
                    distanceTo = ((BigInteger) tariffRecord.get(ValidTariffCustomTableEnum.DISTANCE_TO.getLabel())).intValue();
                }
                if (tariffRecord.get(ValidTariffCustomTableEnum.DISTANCE_UNIT.getLabel()) != null) {
                    distanceUnit = ((BigInteger) tariffRecord.get(ValidTariffCustomTableEnum.DISTANCE_UNIT.getLabel())).intValue();
                }

            } else if (tariffRecord.get(ValidTariffCustomTableEnum.QUANTITY_FROM.getLabel()) != null) {
                quantityFrom = ((BigInteger) tariffRecord.get(ValidTariffCustomTableEnum.QUANTITY_FROM.getLabel())).intValue();
                if (tariffRecord.get(ValidTariffCustomTableEnum.QUANTITY_TO.getLabel()) != null) {
                    quantityTo = ((BigInteger) tariffRecord.get(ValidTariffCustomTableEnum.QUANTITY_TO.getLabel())).intValue();
                }
                if (tariffRecord.get(ValidTariffCustomTableEnum.MAX_RATED_QUANTITY.getLabel()) != null) {
                    maxQuantity = ((BigInteger) tariffRecord.get(ValidTariffCustomTableEnum.MAX_RATED_QUANTITY.getLabel())).intValue();
                }
            }
        }

        /**
         * @return Record identifier
         */
        public Long getId() {
            return id;
        }

        /**
         * @return Tariff plan identifier
         */
        public Long getTariffPlanId() {
            return tariffPlanId;
        }

        /**
         * @return Quantity for GNP quantity rating - from
         */
        public Integer getQuantityFrom() {
            return quantityFrom;
        }

        /**
         * @return Quantity for GNP quantity rating - to
         */
        public Integer getQuantityTo() {
            return quantityTo;
        }

        /**
         * @return Quantity for GNP quantity rating - maximum rate quantity
         */
        public Integer getMaxQuantity() {
            return maxQuantity;
        }

        /**
         * @return Distance for distance based rating - from
         */
        public Integer getDistanceFrom() {
            return distanceFrom;
        }

        /**
         * @return Distance for distance based rating - to
         */
        public Integer getDistanceTo() {
            return distanceTo;
        }

        /**
         * @return Distance for distance based rating - rating unit
         */
        public Integer getDistanceUnit() {
            return distanceUnit;
        }

        /**
         * @return Price
         */
        public BigDecimal getAmountWithoutTax() {
            return amountWithoutTax;
        }

        /**
         * @return Description
         */
        public String getDescription() {
            return description;
        }

        /**
         * @return Tax class identifier
         */
        public Long getTaxClassId() {
            return taxClassId;
        }

        /**
         * @return Invoice sub category identifier
         */
        public Long getInvoiceSubCategoryId() {
            return invoiceSubCategoryId;
        }

        /**
         * @return Accounting code identifier
         */
        public Long getAccountingCodeId() {
            return accountingCodeId;
        }
    }

    /**
     * A group of service parameters that are applicable for a given quantity
     */
    protected class ServiceRatingParameterGroup {

        /**
         * Quantity
         */
        BigDecimal quantity;

        /**
         * Accumulated quantity up to this point
         */
        BigDecimal accumulatedQuantity;

        /**
         * Service parameters split into date periods
         */
        List<ServiceRatingParameters> parameters;

        public ServiceRatingParameterGroup(BigDecimal quantity, BigDecimal accumulatedQuantity, List<ServiceRatingParameters> parameters) {
            this.quantity = quantity;
            this.accumulatedQuantity = accumulatedQuantity;
            this.parameters = parameters;
        }

        @Override
        public String toString() {
            return "ServiceRatingParameterGroup [quantity=" + quantity + ", parameters=" + parameters + "]";
        }

        /**
         * Get the earliest date that service parameters start to be valid
         *
         * @return Date
         */
        public Date getEarliestParameterFromDate() {

            Date date = null;

            for (ServiceRatingParameters serviceRatingParameters : parameters) {

                if (date == null || date.compareTo(serviceRatingParameters.period.getFrom()) > 0) {
                    date = serviceRatingParameters.period.getFrom();
                }
            }
            return date;
        }
    }

    /**
     * Service parameter covering a certain date period
     */
    protected class ServiceRatingParameters {

        /**
         * Date period for which parameters are valid
         */
        DatePeriod period;

        /**
         * Service parameters
         */
        Map<String, Object> parameters;

        public ServiceRatingParameters(Date dateFrom, Date dateTo, Map<String, Object> parameters) {
            this.period = new DatePeriod(dateFrom, dateTo);

            this.parameters = parameters;
        }

        /**
         * @return Date period for which parameters are valid
         */
        public DatePeriod getPeriod() {
            return period;
        }

        /**
         * @return Service parameters
         */
        public Map<String, Object> getParameters() {
            return parameters;
        }

        @Override
        public String toString() {
            return period.toString("yyyy-MM-dd");
        }
    }

    /**
     * SLA surcharge configuration covering a certain date period
     */
    protected class SLAParameters {

        /**
         * Date period for which parameters are valid
         */
        DatePeriod period;

        /**
         * SLA code
         */
        String slaInd;

        BigDecimal totalAmount = BigDecimal.ZERO;

        /**
         * Constructor for SLA surcharge configuration
         *
         * @param period Date period for which parameters are valid
         * @param slaInd SLA code
         */
        public SLAParameters(DatePeriod period, String slaInd) {
            super();
            this.period = period;
            this.slaInd = slaInd;
        }

        /**
         * Add amount to the calculated total amount value
         *
         * @param amountToAdd Amount to add
         */
        public void addAmount(BigDecimal amountToAdd) {
            totalAmount = totalAmount.add(amountToAdd);
        }

        @Override
        public String toString() {
            return period.toString("yyyy-MM-dd");
        }
    }

    /**
     * Tariff search criteria used to generate error message
     */
    private class TariffSearchCriteria {

        /**
         * Field name to display in an error message (humanized name)
         */
        String fieldname;

        /**
         * Search value to display in an error message
         */
        Object values;

        /**
         * Field name used to perform the actual search
         */
        String criteriaField;

        /**
         * Field value used to perform the actual search
         */
        Object criteriaValue;

        /**
         * Constructor for Tariff search criteria used to generate error message
         *
         * @param fieldname Field name to display in an error message (humanized name)
         * @param values Search value to display in an error message
         * @param criteriaField Field name used to perform the actual search
         * @param criteriaValue Field value used to perform the actual search
         */
        public TariffSearchCriteria(String fieldname, Object values, String criteriaField, Object criteriaValue) {
            super();
            this.fieldname = fieldname;
            this.values = values;
            this.criteriaField = criteriaField;
            this.criteriaValue = criteriaValue;
        }

        @Override
        public String toString() {
            return fieldname + "=" + values;
        }
    }

    /**
     * Indicates a recurring calendar change
     */
    protected class RecurringCalendarChangeException extends Exception {

        private static final long serialVersionUID = 4983757609506356589L;

        /**
         * Date from which a new calendar is effective
         */
        private Date effectiveDate;

        /**
         * New recurring calendar
         */
        private Calendar recurringCalendar;

        /**
         * Indicates a recurring calendar change
         *
         * @param effectiveDate Date from which a new calendar is effective
         * @param recurringCalendar New recurring calendar
         */
        public RecurringCalendarChangeException(Date effectiveDate, Calendar recurringCalendar) {
            this.effectiveDate = effectiveDate;
            this.recurringCalendar = recurringCalendar;
        }

        /**
         * @return Date from which a new recurring calendar is effective
         */
        public Date getEffectiveDate() {
            return effectiveDate;
        }

        /**
         * @return New recurring calendar
         */
        public Calendar getRecurringCalendar() {
            return recurringCalendar;
        }
    }

    /**
     * Wallet operation to rate. Used to handle recurring calendar change
     */
    private class WOToRate {

        /**
         * Wallet operation to rate
         */
        WalletOperation walletOperation;

        /**
         * Recurring calendar to apply
         */
        Calendar calendar;

        /**
         * Wallet operation to rate. Used to handle recurring calendar change
         *
         * @param walletOperation Wallet operation to rate
         * @param calendar Recurring calendar to apply
         */
        public WOToRate(WalletOperation walletOperation, Calendar calendar) {
            super();
            this.walletOperation = walletOperation;
            this.calendar = calendar;
        }

        /**
         * @return Wallet operation to rate
         */
        public WalletOperation getWalletOperation() {
            return walletOperation;
        }

        /**
         * @return Recurring calendar to apply
         */
        public Calendar getCalendar() {
            return calendar;
        }

        /**
         * @param calendar Recurring calendar to apply
         */
        public void setCalendar(Calendar calendar) {
            this.calendar = calendar;
        }

        /**
         * Get a start date of wallet operation
         */
        public Date getStartDate() {
            return walletOperation.getStartDate();
        }

        @Override
        public String toString() {
            return DateUtils.formatAsDate(walletOperation.getStartDate()) + " - " + DateUtils.formatAsDate(walletOperation.getEndDate()) + " " + calendar;
        }
    }
}