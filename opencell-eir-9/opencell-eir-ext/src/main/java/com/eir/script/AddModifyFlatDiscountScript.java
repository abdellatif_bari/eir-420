package com.eir.script;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.joda.time.DateTimeComparator;
import org.meveo.admin.exception.ValidationException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.CustomTableEvent;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.payments.impl.CustomerAccountService;
import org.meveo.service.script.Script;

/**
 * The Class AddModifyFlatDiscountScript.
 *
 *
 */
@Stateless
public class AddModifyFlatDiscountScript extends Script {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @Inject
    private CustomerAccountService customerAccountService;
    @Inject
    private BillingAccountService billingAccountService;
    @Inject
    private CustomTableService customTableService;

    @Override
    public void execute(Map<String, Object> initContext) throws ValidationException {
        log.debug("#####################Starting of script AddModifyFlatDiscountScript");
        try {
            Map<String, Object> searchCriteria = new HashMap<>();
            CustomTableEvent ce = (CustomTableEvent) initContext.get("event");
            Map<String, Object> values = ce.getValues();
            // Customer Account
            CustomerAccount ca = null;
            if (!StringUtils.isBlank(values.get("customer_account"))) {
                ca = customerAccountService.findById((Long) values.get("customer_account"));
                if (ca == null) {
                    throw new ValidationException("No CustomerAccount found with ID = " + values.get("customer_account"));
                }
                searchCriteria.put("customer_account", ca.getId());
            } else {
                searchCriteria.put("customer_account ", "IS_NULL");
            }

            // Billing Account
            if (!StringUtils.isBlank(values.get("billing_account"))) {
                BillingAccount ba = billingAccountService.findById((Long) values.get("billing_account"));
                if (ca == null || ba == null || !ba.getCustomerAccount().getCode().equals(ca.getCode())) {
                    throw new ValidationException("Incompatible billing account! ID = " + values.get("billing_account"));
                }
                searchCriteria.put("billing_account", ba.getId());
            } else {
                searchCriteria.put("billing_account", "IS_NULL");
            }

            values.put("valid_from", DateUtils.setDateToStartOfDay((Date) values.get("valid_from")));
            Date startDate = (Date) values.get("valid_from");
            // Discount’s start date cannot be earlier than system date.
            if (values.get("id") == null && (startDate == null || DateTimeComparator.getDateOnlyInstance().compare(startDate, new Date()) < 0)) {
                throw new ValidationException("Discount’s start date cannot be null or earlier than system date");
            }
            searchCriteria.put("valid_from", startDate);

            Date endDate = null;
            if (values.get("valid_to") != null && !StringUtils.isBlank(values.get("valid_to"))) {
                endDate = DateUtils.setDateToStartOfDay((Date) values.get("valid_to"));
                values.put("valid_to", endDate);

                if (DateTimeComparator.getDateOnlyInstance().compare(endDate, startDate) < 0) {
                    throw new ValidationException("end date should be null or in the future");
                }
                searchCriteria.put("valid_to", endDate);
            } else {
                searchCriteria.put("valid_to", "IS_NULL");
            }

            // Charge Type :
            if (!StringUtils.isBlank(values.get("charge_type"))) {
                if (!"R".equals(values.get("charge_type")) && !"O".equals(values.get("charge_type")) && !"*".equals(values.get("charge_type"))) {
                    throw new ValidationException("Charge Type should be 'O', 'R' or '*'");
                }
                searchCriteria.put("charge_type", values.get("charge_type"));
            } else {
                searchCriteria.put("charge_type", "IS_NULL");
            }

            // Service id:
            if (!StringUtils.isBlank(values.get("service_id"))) {
                searchCriteria.put("service_id", values.get("service_id"));
            } else {
                searchCriteria.put("service_id", "IS_NULL");
            }

            // migration code:
            if (!StringUtils.isBlank(values.get("migration_code"))) {
                searchCriteria.put("migration_code", values.get("migration_code"));
            } else {
                searchCriteria.put("migration_code", "IS_NULL");
            }

            // product_set:
            if (!StringUtils.isBlank(values.get("product_set"))) {
                searchCriteria.put("product_set", values.get("product_set"));
            } else {
                searchCriteria.put("product_set", "IS_NULL");
            }

            // percent:
            if (!StringUtils.isBlank(values.get("percent"))) {
                searchCriteria.put("percent", values.get("percent"));
            } else {
                searchCriteria.put("percent", "IS_NULL");
            }

            // description:
            if (!StringUtils.isBlank(values.get("description"))) {
                searchCriteria.put("description", values.get("description"));
            } else {
                searchCriteria.put("description", "IS_NULL");
            }

            // uniqueness
            Long flatDiscountsNbr = customTableService.count("EIR_OR_FLAT_DISCOUNT", new PaginationConfiguration(searchCriteria));

            if (flatDiscountsNbr > 1) {
                throw new ValidationException("Flat discount with the same fields already exists");
            }

            // TODO HZN : try to use only one query with all checks

            // check if an existing line with same profile_code and endDate null
            if (isDiscountExistWithoutEndDate((String) values.get("code"), endDate)) {
                throw new ValidationException("The end Date of an exiting discount with same profile_code is not set!");
            }

            if (isDiscountExistWithFutureEndDate((String) values.get("code"), startDate, endDate)) {
                throw new ValidationException("The start date must be greater than the end date of an existing line with the same profile code.");
            }

        } catch (Exception e) {
            throw new ValidationException(e.getMessage());

        }
    }

    /**
     * Checks if is discount exist with future end date.
     *
     * @param profileCode the profile code
     * @param startDate the start date
     * @param endDate the end date
     * @return true, if is discount exist with future end date
     */
    private boolean isDiscountExistWithFutureEndDate(String profileCode, Date startDate, Date endDate) {        
        log.debug("isDiscountExistWithFutureEndDate profileCode " + profileCode + " startDate " + startDate + " endDate " + endDate);
        String query = "select 1 from EIR_OR_FLAT_DISCOUNT where lower(code) = :profileCode and valid_to >= :startDate"; // profileCode is mandatory
        List discounts = customTableService.getEntityManager().createNativeQuery(query).setParameter("profileCode", profileCode.toLowerCase()).setParameter("startDate", startDate).setMaxResults(2).getResultList();
        return (endDate != null && discounts != null && discounts.size() > 1) || (endDate == null && discounts != null && discounts.size() > 0);
    }

    /**
     * Checks if is discount exist without end date.
     *
     * @param profileCode the profile code
     * @param endDate the end date
     * @return true, if is discount exist without end date
     */
    private boolean isDiscountExistWithoutEndDate(String profileCode, Date endDate) {
        log.debug("isDiscountExistWithoutEndDate profileCode " + profileCode + " endDate " + endDate);
        Map<String, Object> searchCriteria = new HashMap<>();
        searchCriteria.put("code", profileCode);
        searchCriteria.put("valid_to", "IS_NULL");
        long discounts = customTableService.count("EIR_OR_FLAT_DISCOUNT", new PaginationConfiguration(searchCriteria));
        return (endDate == null && discounts > 1) || (endDate != null && discounts > 0);
    }
}