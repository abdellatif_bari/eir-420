package com.eir.script;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Base64;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.bi.FileStatusEnum;
import org.meveo.model.bi.FlatFile;
import org.meveo.service.bi.impl.FlatFileService;
import org.meveo.service.script.Script;
import org.meveo.service.script.ScriptInstanceService;
import org.meveo.service.script.ScriptInterface;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Loading order files for provisioning
 * 
 * @author Hatim OUDAD
 *
 */
@Stateless
public class ProvisioningFileLoaderScript extends Script {

    @Inject
    private ScriptInstanceService scriptInstanceService;
    @Inject
    private FlatFileService flatFileService;

    // Variables
    private static final Logger LOGGER = LoggerFactory.getLogger(ProvisioningFileLoaderScript.class);
    static final String API_RESPONSE_URL = "http://localhost:8080/opencell/inbound/order/place/";
    private static final String CSV_SEPARATOR = ",";

    /**
     * Entry Point for script
     */
    @Override
    public void execute(Map<String, Object> initContext) throws BusinessException {

        LOGGER.info("******Starting of script provisioningFileLoader******");
        StringBuilder inputPath = new StringBuilder("./opencelldata/DEMO/imports/order/input");
        File dir = new File(inputPath.toString());
        File[] directoryListing = dir.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                String name = pathname.getName().toLowerCase();
                return name.endsWith(".csv") && pathname.isFile();
            }
        });
        for (File child : directoryListing) {
            LOGGER.info("***file name***" + child.getName());
            // Folder paths for provisioning operation
            StringBuilder inpPath = new StringBuilder("./opencelldata/DEMO/imports/order/input");

            StringBuilder outputPath = new StringBuilder("./opencelldata/DEMO/imports/order/output");

            StringBuilder rejectPath = new StringBuilder("./opencelldata/DEMO/imports/order/reject");

            Path inPath = Paths.get(inpPath.append(File.separator + child.getName()).toString());

            Path outPath = Paths.get(outputPath.append(File.separator + child.getName()).append(".output").toString());

            Path rejPath = Paths.get(rejectPath.append(File.separator + child.getName()).append(".rejected").toString());

            // reading CSV content
            try {
                LOGGER.info("Reading file..");
                String csvContent = readFile(child, StandardCharsets.UTF_8);
                LOGGER.info("Converting to XML..");

                String xmlContent = convertContentToXML(csvContent);

                LOGGER.info("Splitting orders..");

                String[] orders = xmlContent.split("</Wholesale_Billing>");
                boolean response = true;
                boolean rejected = false;

                for (String order : orders) {
                    order = order + "</Wholesale_Billing>";
                    LOGGER.info("***order***" + order);
                    // executeScript("com.eir.script.PlaceOrderScript", order);*
                    response = getApiResponse(order);
                    LOGGER.info("Response of order execution : " + response);
                    if (response == false) {
                        rejected = true;
                    }
                }

                if (!rejected) {
                    // move file to output folder
                    LOGGER.info("move file to output folder");
                    Files.move(inPath, outPath, StandardCopyOption.ATOMIC_MOVE);
                    updateFlatFile(child.getName(), FileStatusEnum.VALID);
                } else {
                    // move file to reject folder
                    LOGGER.info("move file to reject folder");
                    Files.move(inPath, rejPath, StandardCopyOption.ATOMIC_MOVE);
                    updateFlatFile(child.getName(), FileStatusEnum.REJECTED);
                }

            } catch (Exception e) {
                LOGGER.error(e.getMessage());
            }

        }

        if (LOGGER.isInfoEnabled()) {
            LOGGER.info("*****Ending of script provisioningFileLoader******");
        }
    }

    /**
     * Convert Content from CSV to XML
     * 
     * @param csvContent CSV Content
     * @return XML content
     */
    private String convertContentToXML(String csvContent) {
        try {
            StringBuilder xmlContent = new StringBuilder();
            String[] csvLines = csvContent.split("\n");
            String csvLine;

            for (int i = 0; i < csvLines.length; i++) {

                csvLine = csvLines[i];
                String[] csvItems = csvLine.split(CSV_SEPARATOR);

                if ("WB Record".equals(csvItems[0])) {
                    xmlContent.append("<Wholesale_Billing>");
                    xmlContent.append(fillTag("Transaction_Id", csvItems[1]));
                    xmlContent.append(fillTag("Ordering_System", csvItems[2]));
                    xmlContent.append(fillTag("Order_Id", csvItems[3]));
                    if (csvItems.length > 4) {
                        xmlContent.append(fillTag("Ref_Order_Id", csvItems[4]));
                    }

                }

                if ("Bill Item Record".equals(csvItems[0])) {
                    byte col = 1;
                    xmlContent.append("<Bill_Item>");
                    xmlContent.append(fillTag("Item_Id", csvItems[col++]));
                    xmlContent.append(fillTag("Operator_Id", csvItems[col++]));
                    xmlContent.append(fillTag("Operator_Order_Number", csvItems[col++]));
                    xmlContent.append(fillTag("Item_Component_Id", csvItems[col++]));
                    xmlContent.append(fillTag("Item_Version", csvItems[col++]));
                    xmlContent.append(fillTag("Item_Type", csvItems[col++]));
                    xmlContent.append(fillTag("Agent_Code", csvItems[col++]));
                    xmlContent.append(fillTag("UAN", csvItems[col++]));
                    xmlContent.append(fillTag("Master_Account_Number", csvItems[col++]));
                    xmlContent.append(fillTag("Service_Id", csvItems[col++]));
                    xmlContent.append(fillTag("Ref_Service_Id", csvItems[col++]));
                    xmlContent.append(fillTag("Xref_Service_Id", csvItems[col++]));
                    xmlContent.append(fillTag("Bill_Code", csvItems[col++]));
                    xmlContent.append(fillTag("Action", csvItems[col++]));
                    xmlContent.append(fillTag("Action_Qualifier", csvItems[col++]));
                    xmlContent.append(fillTag("Quantity", csvItems[col++]));
                    xmlContent.append(fillTag("Effect_Date", csvItems[col++].trim()));

                    if (csvLines.length > i + 1) {
                        String csvNextLine = csvLines[i + 1];
                        String[] csvNextItems = csvNextLine.split(CSV_SEPARATOR);
                        if ("Bill Item Record".equals(csvNextItems[0])) {
                            xmlContent.append("</Bill_Item>");
                        }
                        if ("WB Record".equals(csvNextItems[0])) {
                            xmlContent.append("</Bill_Item>");
                            xmlContent.append("</Wholesale_Billing>");

                        }
                    } else {

                        xmlContent.append("</Bill_Item>");
                        xmlContent.append("</Wholesale_Billing>");

                    }

                }

                if ("Attribute Record".equals(csvItems[0]) || (csvItems[0].isEmpty() && !(csvItems[1].isEmpty()))) {

                    xmlContent.append("<Attribute>");

                    xmlContent.append(fillTag("Code", csvItems[1]));
                    xmlContent.append(fillTag("Value", csvItems[2]));
                    xmlContent.append(fillTag("Unit", csvItems[3]));
                    xmlContent.append(fillTag("Component_Id", csvItems[4]));
                    xmlContent.append(fillTag("Attribute_Type", csvItems[5]));

                    xmlContent.append("</Attribute>");
                    if (csvLines.length > i + 1) {
                        String csvNextLine = csvLines[i + 1];
                        String[] csvNextItems = csvNextLine.split(CSV_SEPARATOR);
                        if ("Bill Item Record".equals(csvNextItems[0])) {
                            xmlContent.append("</Bill_Item>");

                        }
                        if ("WB Record".equals(csvNextItems[0])) {
                            xmlContent.append("</Bill_Item>");
                            xmlContent.append("</Wholesale_Billing>");

                        }

                    } else {
                        xmlContent.append("</Bill_Item>");
                        xmlContent.append("</Wholesale_Billing>");
                        break;

                    }

                }

            }
            LOGGER.info("***xml content***" + xmlContent.toString());
            return xmlContent.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            LOGGER.error(ex.getMessage());
        }
        return null;
    }

    private String fillTag(String tagName, String tagValue) {
        if (StringUtils.isBlank(tagValue)) {
            return "";
        }
        return "<" + tagName + ">" + tagValue + "</" + tagName + ">";
    }

    /**
     * GET API Response after calling
     * 
     * @param fileContent File Content
     * @return boolean of the success or not of the order validation
     * @throws IOException Input/Output Exception
     */
    private boolean getApiResponse(String fileContent) {

        URL url;
        String response = "";
        try {
            url = new URL(API_RESPONSE_URL);

            Map<String, Object> params = new LinkedHashMap<>();
            params.put("content", fileContent);

            StringBuilder postData = new StringBuilder();
            for (Map.Entry<String, Object> param : params.entrySet()) {
                if (postData.length() != 0)
                    postData.append('&');

                postData.append(String.valueOf(param.getValue()));
            }
            byte[] postDataBytes = postData.toString().getBytes("UTF-8");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            String userCredentials = "opencell.admin:opencell.admin";
            String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));
            conn.setRequestProperty("Authorization", basicAuth);
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Content-Type", "application/json");
            conn.setRequestProperty("charset", "utf-8");
            conn.setUseCaches(false);

            conn.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
            conn.setDoOutput(true);
            conn.getOutputStream().write(postDataBytes);

            Reader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));

            // Construct api response
            StringBuilder sb = new StringBuilder();
            for (int c; (c = in.read()) >= 0;)
                sb.append((char) c);
            response = sb.toString();
        } catch (Exception e) {
            LOGGER.error(e.getMessage());
        }
        if (response.contains("SUCCESS")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Read file from system
     * 
     * @param file file path
     * @param encoding type of encoding
     * @return content of the file as String
     * @throws IOException Input/Output Exception
     */
    static String readFile(File file, Charset encoding) throws IOException {
        byte[] encoded = Files.readAllBytes(file.toPath());
        return new String(encoded, encoding);
    }

    public void executeScript(String scriptCode, String content) {
        try {
            ScriptInterface script = scriptInstanceService.getScriptInstance(scriptCode);
            HashMap<String, Object> context = new HashMap<>();
            context.put("body", content);
            script.execute(context);
        } catch (Exception ex) {
            log.error("Failed to execute script", ex);
        }

    }

    public void updateFlatFile(String fileOriginalName, FileStatusEnum status) {
        try {
            String[] param = fileOriginalName.split("_");
            String code = param.length > 0 ? param[0] : null;
            String fileCurrentName = null;
            String currentDirectory = null;

            if (status == FileStatusEnum.VALID) {
                fileCurrentName = fileOriginalName + ".output";
                currentDirectory = "./opencelldata/DEMO/imports/order/output";
            } else if (status == FileStatusEnum.REJECTED) {
                fileCurrentName = fileOriginalName + ".rejected";
                currentDirectory = "./opencelldata/DEMO/imports/order/reject";
            }

            String errorMessage = null;

            FlatFile flatFile = flatFileService.findByCode(code);
            if (flatFile != null) {

                flatFile.setFileCurrentName(fileCurrentName);
                flatFile.setCurrentDirectory(currentDirectory);
                flatFile.setStatus(status);
                flatFile.setErrorMessage(errorMessage);
                flatFileService.update(flatFile);
            }
        } catch (BusinessException e) {
            log.error("Failed to update flat file {}", fileOriginalName, e);
        }
    }
}