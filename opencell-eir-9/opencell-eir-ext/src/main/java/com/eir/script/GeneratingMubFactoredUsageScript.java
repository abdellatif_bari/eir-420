package com.eir.script;

import java.text.NumberFormat;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.Subscription;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.script.Script;

/**
 * The Class GeneratingMubFactoredUsageScript.
 */
@Stateless
public class GeneratingMubFactoredUsageScript extends Script {

    private static final long serialVersionUID = 1L;

    /** The Subscription service. */
    @Inject
    private SubscriptionService subscriptionService;
    @Inject
    private BillingAccountService billingAccountService;

    private static final String EPOCH_TIME = "EPOCH_TIME";
    private static final String ROUND_EPOCH_TIME = "ROUND_EPOCH_TIME";
    private static final String RETRIEVAL_DATE = "DATE";
    private static final String RETRIEVAL_TIME = "TIME";
    private static final String ROUND_TIME = "ROUND_TIME";
    private static final String DURATION = "DURATION";
    private static final String TRAFFIC_IN_EF_INPROFILE = "TRAFFIC_IN_EF_INPROFILE";
    private static final String TRAFFIC_IN_EF_OUTPROFILE = "TRAFFIC_IN_EF_OUTPROFILE";
    private static final String TRAFFIC_EG_EF_INPROFILE = "TRAFFIC_EG_EF_INPROFILE";
    private static final String TRAFFIC_EG_EF_OUTPROFILE = "TRAFFIC_EG_EF_OUTPROFILE";
    private static final String TRAFFIC_IN_AF_INPROFILE = "TRAFFIC_IN_AF_INPROFILE";
    private static final String TRAFFIC_IN_AF_OUTPROFILE = "TRAFFIC_IN_AF_OUTPROFILE";
    private static final String TRAFFIC_EG_AF_INPROFILE = "TRAFFIC_EG_AF_INPROFILE";
    private static final String TRAFFIC_EG_AF_OUTPROFILE = "TRAFFIC_EG_AF_OUTPROFILE";
    private static final String TRAFFIC_IN_STD_INPROF = "TRAFFIC_IN_STD_INPROF";
    private static final String TRAFFIC_IN_STD_OUTPROF = "TRAFFIC_IN_STD_OUTPROF";
    private static final String TRAFFIC_EG_STD_INPROFILE = "TRAFFIC_EG_STD_INPROFILE";
    private static final String TRAFFIC_EG_STD_OUTPROFILE = "TRAFFIC_EG_STD_OUTPROFILE";
    // private static final String CUSTOMER_NAME = "CUSTOMER_NAME";
    private static final String CIRCUIT_ID = "CIRCUIT_ID";
    // private static final String PORT_ID = "PORT_ID";
    private static final String ORIGINAL_FILE_NAME = "ORIGINAL_FILE_NAME";

    /** The Constant MUB_RECORD. */
    private static final String MUB_RECORD = "record";

    /**
     * script main execute method.
     *
     * @param methodContext method context params
     * @throws BusinessException business exception
     */
    @SuppressWarnings("unchecked")
    @Override
    public void execute(Map<String, Object> methodContext) throws BusinessException {
        /** The NumberFormat. */
        NumberFormat fmt = NumberFormat.getInstance(Locale.ENGLISH);
        fmt.setGroupingUsed(false);
        fmt.setMaximumIntegerDigits(999);
        fmt.setMaximumFractionDigits(999);

        Map<String, String> item = (Map<String, String>) methodContext.get(MUB_RECORD);
        String epochTime = item.get(EPOCH_TIME);
        String roundEpochTime = item.get(ROUND_EPOCH_TIME);
        String retrievalDate = item.get(RETRIEVAL_DATE);
        String retrievalTime = item.get(RETRIEVAL_TIME);
        String roundTime = item.get(ROUND_TIME);
        String duration = item.get(DURATION);
        String trafficInEfInprofile = item.get(TRAFFIC_IN_EF_INPROFILE);
        String trafficInEfOutprofile = item.get(TRAFFIC_IN_EF_OUTPROFILE);
        String trafficEgEfInprofile = item.get(TRAFFIC_EG_EF_INPROFILE);
        String trafficEgEfOutprofile = item.get(TRAFFIC_EG_EF_OUTPROFILE);
        String trafficInAfInprofile = item.get(TRAFFIC_IN_AF_INPROFILE);
        String trafficInAfOutprofile = item.get(TRAFFIC_IN_AF_OUTPROFILE);
        String trafficEgAfInprofile = item.get(TRAFFIC_EG_AF_INPROFILE);
        String trafficEgAfOutprofile = item.get(TRAFFIC_EG_AF_OUTPROFILE);
        String trafficInStdInprof = item.get(TRAFFIC_IN_STD_INPROF);
        String trafficInStdOutprof = item.get(TRAFFIC_IN_STD_OUTPROF);
        String trafficEgStdInprofile = item.get(TRAFFIC_EG_STD_INPROFILE);
        String trafficEgStdOutprofile = item.get(TRAFFIC_EG_STD_OUTPROFILE);
        // String customerName = item.get(CUSTOMER_NAME);
        String circuitId = item.get(CIRCUIT_ID);
        // String portId = item.get(PORT_ID);
        String originalFileName = item.get(ORIGINAL_FILE_NAME);
        if (StringUtils.isBlank(circuitId)) {
            throw new BusinessException("circuitId is empty!");
        }
        if (circuitId.startsWith("NBB")) {
            throw new BusinessException("Record skipped! circuitId :" + circuitId + "starts with NBB");
        }
        Subscription subscription = subscriptionService.findByCode(circuitId);
        log.debug("###########subscription " + subscription);
        if (subscription == null) {
            throw new BusinessException("Subscription " + circuitId + " not found!");
        }

        Double stEgress = Double.valueOf(trafficEgStdInprofile) + Double.valueOf(trafficEgStdOutprofile);
        Double stIngress = Double.valueOf(trafficInStdInprof) + Double.valueOf(trafficInStdOutprof);
        Double st = Math.max(stEgress, stIngress);
        log.debug("##########st " + st);

        Double afEgress = Double.valueOf(trafficEgAfInprofile) + Double.valueOf(trafficEgAfOutprofile);
        Double afIngress = Double.valueOf(trafficInAfInprofile) + Double.valueOf(trafficInAfOutprofile);
        Double af = Math.max(afEgress, afIngress);
        log.debug("##########af " + af);

        Double efEgress = Double.valueOf(trafficEgEfInprofile) + Double.valueOf(trafficEgEfOutprofile);
        Double efIngress = Double.valueOf(trafficInEfInprofile) + Double.valueOf(trafficInEfOutprofile);
        Double ef = Math.max(efEgress, efIngress);
        log.debug("##########ef " + ef);

        // Covert remaining usage readings in bytes to MegaBits/Second
        double dDuration = Double.valueOf(duration);
        dDuration = dDuration != 0 ? dDuration : 1;
        st = (st * 8) / (1000000 * dDuration);
        af = (af * 8) / (1000000 * dDuration);
        ef = (ef * 8) / (1000000 * dDuration);

        // Create a Factored Usage record for each of the 3 remaining usage reading results.
        st = st * 1;
        af = af * 1.25;
        ef = ef * 1.5;

        // Sum the Factored Usage values in a final Factored Usage record to be stored.
        Double factoredUsage = st + af + ef;

        if (factoredUsage == 0) {
            log.warn("The total calculated Factored Usage value is zero for circuitId " + subscription.getCode() + ", record is skipped");
            throw new BusinessException("Record skipped! The total calculated Factored Usage value is zero for circuitId " + subscription.getCode());
        }

        // TODO AKK - How many records are expected per subscription and BA?
        Map<String, String> cfMubCounters = (Map<String, String>) subscription.getCfValuesNullSafe().getValue("MUB_FINAL_USAGE");
        log.debug("##########cfMubCounters " + cfMubCounters);
        if (cfMubCounters == null) {
            cfMubCounters = new HashMap<>();
        }

        BillingAccount ba = subscription.getUserAccount().getBillingAccount();
        String billingAccountCode = ba.getCode(); // it should never be null

        cfMubCounters.put(epochTime, billingAccountCode + "|" + roundEpochTime + "|" + retrievalDate + "|" + retrievalTime + "|" + roundTime + "|" + duration + "|" + fmt.format(factoredUsage) + "|" + originalFileName
                + "|" + fmt.format(st) + "|" + fmt.format(af) + "|" + fmt.format(ef));
        subscription.getCfValuesNullSafe().setValue("MUB_FINAL_USAGE", cfMubCounters);
        subscriptionService.update(subscription);
        log.debug("Mub Final Usage generated!");

        // PO2-328 : generate aggregates for Factored Usage CDRs per OAO and store these against the respective billing accounts in the system
        Map<String, String> cfMubAggregatedUsage = (Map<String, String>) ba.getCfValuesNullSafe().getValue("MUB_AGGREGATED_FINAL_USAGE");
        log.debug("##########cfMubAggregatedUsage " + cfMubAggregatedUsage);
        if (cfMubAggregatedUsage == null) {
            cfMubAggregatedUsage = new HashMap<>();
        }
        String aggregatedUsage = cfMubAggregatedUsage.get(roundEpochTime);
        log.debug("########## aggregatedUsage " + aggregatedUsage);
        double totalFinalUsage = 0;
        double totalDuration = 0;
        int totalCircuitId = 0;
        if (aggregatedUsage != null && !aggregatedUsage.isEmpty()) {
            totalFinalUsage = Double.valueOf(aggregatedUsage.split(Pattern.quote("|"))[0].replace(',', '.'));
            log.debug("########## old totalFinalUsage " + totalFinalUsage);
            totalDuration = Double.valueOf(aggregatedUsage.split(Pattern.quote("|"))[1].replace(',', '.'));
            log.debug("########## old totalDuration " + totalDuration);
            totalCircuitId = Integer.valueOf(aggregatedUsage.split(Pattern.quote("|"))[2]);
            log.debug("########## old totalCircuitId " + totalCircuitId);
        }

        totalFinalUsage += factoredUsage;
        totalDuration += Long.valueOf(duration);
        totalCircuitId += 1;

        cfMubAggregatedUsage.put(roundEpochTime, fmt.format(totalFinalUsage) + "|" + fmt.format(totalDuration) + "|" + totalCircuitId);
        ba.getCfValuesNullSafe().setValue("MUB_AGGREGATED_FINAL_USAGE", cfMubAggregatedUsage);
        billingAccountService.update(ba);
        log.debug("Mub Final Usage aggregated!");

    }
}
