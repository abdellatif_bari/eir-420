package com.eir.script;

import com.eir.commons.enums.ApplicationPropertiesEnum;
import com.eir.commons.enums.ChargeTypeEnum;
import com.eir.commons.enums.customfields.BillingAccountCFsEnum;
import com.eir.commons.enums.customfields.ServiceInstanceCFsEnum;
import com.eir.commons.enums.customfields.ServiceParametersCFEnum;
import com.eir.commons.enums.customtables.CustomTableEnum;
import com.eir.service.commons.UtilService;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.api.commons.Utils;
import org.meveo.api.dto.billing.ChargeInitializationParametersDto;
import org.meveo.api.dto.flatfile.BillingAccountRecordDto;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.admin.Seller;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.TradingCountry;
import org.meveo.model.billing.TradingLanguage;
import org.meveo.model.billing.UserAccount;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.crm.Customer;
import org.meveo.model.crm.custom.CustomFieldValue;
import org.meveo.model.customEntities.CustomEntityInstance;
import org.meveo.model.mediation.Access;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.model.shared.Address;
import org.meveo.model.shared.DateUtils;
import org.meveo.model.shared.Name;
import org.meveo.model.tax.TaxCategory;
import org.meveo.service.admin.impl.SellerService;
import org.meveo.service.billing.impl.BillingCycleService;
import org.meveo.service.billing.impl.ServiceInstanceService;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.billing.impl.TradingCountryService;
import org.meveo.service.billing.impl.TradingLanguageService;
import org.meveo.service.billing.impl.UserAccountService;
import org.meveo.service.catalog.impl.OfferTemplateService;
import org.meveo.service.catalog.impl.ServiceTemplateService;
import org.meveo.service.custom.CustomEntityInstanceService;
import org.meveo.service.medina.impl.AccessService;
import org.meveo.service.payments.impl.CustomerAccountService;
import org.meveo.service.script.Script;
import org.meveo.service.tax.TaxCategoryService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Create new billing account and/or subscription and activate its oneshot services
 * for each record in the file
 *
 * @author Abdellatif BARI
 */
@Stateless
public class CreateAccountsScript extends Script {

    @Inject
    private com.eir.service.billing.impl.BillingAccountService billingAccountService;
    @Inject
    private UserAccountService userAccountService;
    @Inject
    private SubscriptionService subscriptionService;
    @Inject
    private CustomerAccountService customerAccountService;
    @Inject
    private BillingCycleService billingCycleService;
    @Inject
    private TradingCountryService tradingCountryService;
    @Inject
    private TradingLanguageService tradingLanguageService;
    @Inject
    private OfferTemplateService offerTemplateService;
    @Inject
    private SellerService sellerService;
    @Inject
    private AccessService accessService;
    @Inject
    private ServiceTemplateService serviceTemplateService;
    @Inject
    private ServiceInstanceService serviceInstanceService;
    @Inject
    private CustomEntityInstanceService customEntityInstanceService;
    @Inject
    private TaxCategoryService taxCategoryService;
    @Inject
    protected OneShotAndRecurringRatingScript ratingScript;
    @Inject
    protected UtilService utilService;


    @Override
    public void execute(Map<String, Object> parameters) throws BusinessException {

        try {
            BillingAccountRecordDto billingAccountRecordDto = initContext(parameters);
            processItem(billingAccountRecordDto);
        } catch (Exception e) {
            log.error("error on process create accounts file {} ", e.getMessage(), e);
            if (e instanceof BusinessException) {
                throw e;
            } else {
                // wrap the exception in a business exception and throwing it
                throw new BusinessException(e);
            }
        }
    }

    /**
     * Init context
     *
     * @param parameters parameters
     * @return the bill account Dto
     * @throws BusinessException the business exception
     */
    private BillingAccountRecordDto initContext(Map<String, Object> parameters) throws BusinessException {
        BillingAccountRecordDto billingAccountRecordDto = (BillingAccountRecordDto) parameters.get("record");
        if (billingAccountRecordDto == null) {
            throw new BusinessException("Parameter record is missing");
        }
        billingAccountRecordDto.setErrorMessage(new StringBuffer());
        return billingAccountRecordDto;
    }

    /**
     * Process item
     *
     * @param billingAccountRecordDto the bill account Dto
     * @throws BusinessException the business exception
     */
    private void processItem(BillingAccountRecordDto billingAccountRecordDto) throws BusinessException {
        validateItem(billingAccountRecordDto);
        populateItem(billingAccountRecordDto);
        if (billingAccountRecordDto.getErrorMessage().length() > 0) {
            throw new ValidationException(billingAccountRecordDto.getErrorMessage().toString());
        }
    }

    /**
     * Validate item
     *
     * @param billingAccountRecordDto the bill account Dto
     * @throws BusinessException the business exception
     */
    private void validateItem(BillingAccountRecordDto billingAccountRecordDto) throws BusinessException {

        if (!validateOfferTemplate(billingAccountRecordDto)) {
            return;
        }
        if (!validateAccounts(billingAccountRecordDto)) {
            return;
        }
        if (!validateBillingCycle(billingAccountRecordDto)) {
            return;
        }
        if (!validateServiceType(billingAccountRecordDto)) {
            return;
        }
        if (!validateSubscription(billingAccountRecordDto)) {
            return;
        }
        validateCharge(billingAccountRecordDto);
    }

    /**
     * Populate tariff plan item
     *
     * @param billingAccountRecordDto the bill account Dto
     * @throws BusinessException the business exception
     */
    private void populateItem(BillingAccountRecordDto billingAccountRecordDto) throws BusinessException {
        // if everything is OK and no errors
        if (billingAccountRecordDto.getErrorMessage().length() == 0) {
            // If a BillingAccount is provided then only the Subscription is required to be created
            if (billingAccountRecordDto.getBillingAccount() != null) {
                createUserAccountHierarchy(billingAccountRecordDto);

            } else {// If the Customer_Account is provided in the input file,
                // then a BillingAccount and Subscription (if populated) is required to be created.
                createBillingAccountHierarchy(billingAccountRecordDto);
            }
        }
    }

    /**
     * Get offer template
     *
     * @param billingAccountRecordDto the bill account Dto
     * @return offer template
     */
    private OfferTemplate getOfferTemplate(BillingAccountRecordDto billingAccountRecordDto) {
        List<OfferTemplate> offerTemplates = offerTemplateService.findByServiceTemplate(billingAccountRecordDto.getServiceTemplate());
        if (offerTemplates != null && !offerTemplates.isEmpty()) {
            return offerTemplates.get(0);
        }
        return null;
    }


    /**
     * Validate offer template
     *
     * @param billingAccountRecordDto billingAccountRecordDto the bill account Dto
     * @return true if everything is well, false if else
     * @throws BusinessException the business exception
     */
    private boolean validateOfferTemplate(BillingAccountRecordDto billingAccountRecordDto) throws BusinessException {
        if (StringUtils.isBlank(billingAccountRecordDto.getBillCode())) {
            billingAccountRecordDto.getErrorMessage().append("BillCode does not exist.");
            return false;
        }

        if (billingAccountRecordDto.getStartDate() == null) {
            billingAccountRecordDto.getErrorMessage().append("No Subscription start_date provided.");
            return false;
        }

        ServiceTemplate serviceTemplate = serviceTemplateService.findByCode(billingAccountRecordDto.getBillCode());
        if (serviceTemplate == null) {
            billingAccountRecordDto.getErrorMessage().append("The service template is not found");
            return false;
        }
        billingAccountRecordDto.setServiceTemplate(serviceTemplate);

        OfferTemplate offerTemplate = getOfferTemplate(billingAccountRecordDto);
        if (offerTemplate == null) {
            billingAccountRecordDto.getErrorMessage().append("The offer does not exist.");
            return false;
        }
        billingAccountRecordDto.setOfferTemplate(offerTemplate);
        return true;
    }

    /**
     * Validate accounts
     *
     * @param billingAccountRecordDto the bill account Dto
     * @return true if everything is well, false if else
     * @throws BusinessException the business exception
     */
    private boolean validateAccounts(BillingAccountRecordDto billingAccountRecordDto) throws BusinessException {

        Seller seller = sellerService.findByCode(ApplicationPropertiesEnum.OPENEIR_SELLER_CODE.getProperty());
        if (seller == null) {
            billingAccountRecordDto.getErrorMessage().append("The OPENEIR seller not found");
            return false;
        }
        billingAccountRecordDto.setSeller(seller);

        Customer customer = null;
        BillingAccount billingAccount = null;
        if (StringUtils.isBlank(billingAccountRecordDto.getCustomerAccountCode()) && StringUtils.isBlank(billingAccountRecordDto.getBillingAccountCode())) {
            billingAccountRecordDto.getErrorMessage().append("No customer provided for new Billing Account");
            return false;
        }

        // If the Customer_Account is provided in the input file.
        if (!StringUtils.isBlank(billingAccountRecordDto.getCustomerAccountCode())) {
            // Set the customer account
            CustomerAccount customerAccount = customerAccountService.findByCode(billingAccountRecordDto.getCustomerAccountCode());
            if (customerAccount == null) {
                billingAccountRecordDto.getErrorMessage().append("The customer account : " + billingAccountRecordDto.getCustomerAccountCode() + " does not exist");
                return false;
            }
            billingAccountRecordDto.setCustomerAccount(customerAccount);

            // Set the customer
            customer = customerAccount.getCustomer();
            if (customer == null) {
                billingAccountRecordDto.getErrorMessage().append("The customer : " + billingAccountRecordDto.getCustomerAccountCode() + " does not exist");
                return false;
            }
            billingAccountRecordDto.setCustomer(customer);
        }

        // If a BillingAccount is provided.
        if (!StringUtils.isBlank(billingAccountRecordDto.getBillingAccountCode())) {

            // Set the billing account
            billingAccount = billingAccountService.findByCode(billingAccountRecordDto.getBillingAccountCode());
            if (billingAccount == null) {
                billingAccountRecordDto.getErrorMessage().append("The billing account : " + billingAccountRecordDto.getBillingAccountCode() + " does not exist");
                return false;
            }
            billingAccountRecordDto.setBillingAccount(billingAccount);

            // Set the customer account
            if (billingAccount.getCustomerAccount() == null) {
                billingAccountRecordDto.getErrorMessage().append("The customer account does not exist for the billing acount : " + billingAccountRecordDto.getBillingAccountCode());
                return false;
            }

            if (billingAccountRecordDto.getCustomerAccount() != null && !billingAccountRecordDto.getCustomerAccount().equals(billingAccount.getCustomerAccount())) {
                billingAccountRecordDto.getErrorMessage().append("Customer Account and billing account mismatch");
                return false;
            }
            billingAccountRecordDto.setCustomerAccount(billingAccount.getCustomerAccount());

            // Set the customer
            customer = billingAccount.getCustomerAccount().getCustomer();
            if (customer == null) {
                billingAccountRecordDto.getErrorMessage().append("The customer : " + billingAccountRecordDto.getCustomerAccountCode() + " does not exist");
                return false;
            }
            if (billingAccountRecordDto.getCustomer() != null && !billingAccountRecordDto.getCustomer().equals(customer)) {
                billingAccountRecordDto.getErrorMessage().append("Customer Account and billing account mismatch");
                return false;
            }
            billingAccountRecordDto.setCustomer(customer);
        }
        return true;
    }

    /**
     * Validate billing cycle
     *
     * @param billingAccountRecordDto the bill account Dto
     * @return true if everything is well, false if else
     * @throws BusinessException the business exception
     */
    private boolean validateBillingCycle(BillingAccountRecordDto billingAccountRecordDto) throws BusinessException {
        if (billingAccountRecordDto.getBillingAccount() == null) {
            if (StringUtils.isBlank(billingAccountRecordDto.getBillingCycleCode())) {
                billingAccountRecordDto.getErrorMessage().append("Bill Cycle missing or does not exist for the service " + billingAccountRecordDto.getBillCode());
                return false;
            }
            billingAccountRecordDto.setBillingCycle(billingCycleService.findByCode(billingAccountRecordDto.getBillingCycleCode()));
            if (billingAccountRecordDto.getBillingCycle() == null) {
                billingAccountRecordDto.getErrorMessage().append("Bill Cycle missing or does not exist for the service " + billingAccountRecordDto.getBillCode());
                return false;
            }
        }
        return true;
    }

    /**
     * Validate service type
     *
     * @param billingAccountRecordDto the bill account Dto
     * @return true if everything is well, false if else
     * @throws BusinessException the business exception
     */
    private boolean validateServiceType(BillingAccountRecordDto billingAccountRecordDto) {
        if (billingAccountRecordDto.getBillingAccount() == null) {
            if (StringUtils.isBlank(billingAccountRecordDto.getServiceType())) {
                billingAccountRecordDto.getErrorMessage().append("Service_type missing or does not exist for " + billingAccountRecordDto.getBillCode() + " service");
                return false;
            }
            CustomEntityInstance serviceTypeCET = customEntityInstanceService.findByCodeByCet(CustomTableEnum.SERVICE_TYPE.getCode(), billingAccountRecordDto.getServiceType());
            if (serviceTypeCET == null) {
                billingAccountRecordDto.getErrorMessage().append("The service type " + billingAccountRecordDto.getServiceType() + " is not found");
                return false;
            }
        }
        return true;
    }

    /**
     * Validate subscription
     *
     * @param billingAccountRecordDto the bill account Dto
     * @return true if everything is well, false if else
     * @throws BusinessException the business exception
     */
    private boolean validateSubscription(BillingAccountRecordDto billingAccountRecordDto) {
        if (billingAccountRecordDto.getBillingAccount() != null) {
            if (StringUtils.isBlank(billingAccountRecordDto.getSubscriptionCode())) {
                billingAccountRecordDto.getErrorMessage().append("The subscription code is required.");
                return false;
            }
        }
        if (!StringUtils.isBlank(billingAccountRecordDto.getSubscriptionCode())) {
            Subscription subscription = subscriptionService.findByCode(billingAccountRecordDto.getSubscriptionCode());
            if (subscription != null) {
                billingAccountRecordDto.getErrorMessage().append("The subscription with code : " + billingAccountRecordDto.getSubscriptionCode() + " already exists");
                return false;
            }
        }
        return true;
    }

    /**
     * Determine tax category from a billing account
     *
     * @param billingAccount Billing account
     * @return Tax category
     */
    private TaxCategory getTaxCategory(BillingAccount billingAccount) {
        TaxCategory taxCategory = billingAccount.getTaxCategoryResolved();
        if (taxCategory == null) {
            taxCategory = billingAccount.getTaxCategory();
            if (taxCategory == null) {
                taxCategory = billingAccount.getCustomerAccount().getCustomer().getCustomerCategory().getTaxCategory();
            }
        }
        return taxCategory;
    }

    /**
     * Validate charge
     *
     * @param billingAccountRecordDto the bill account Dto
     * @return true if everything is well, false if else
     * @throws BusinessException the business exception
     */
    private boolean validateCharge(BillingAccountRecordDto billingAccountRecordDto) {
        if (!StringUtils.isBlank(billingAccountRecordDto.getAmount()) && !Utils.isDouble(billingAccountRecordDto.getAmount())) {
            billingAccountRecordDto.getErrorMessage().append("The amount is invalid for the service " + billingAccountRecordDto.getBillCode());
            return false;
        }

        TaxCategory taxCategory = null;
        if (billingAccountRecordDto.getBillingAccount() == null) {
            if (StringUtils.isBlank(billingAccountRecordDto.getBillingTaxCategory())) {
                billingAccountRecordDto.getErrorMessage().append("The tax category is required for the service " + billingAccountRecordDto.getBillCode());
                return false;
            }
            taxCategory = taxCategoryService.findByCode(billingAccountRecordDto.getBillingTaxCategory());
            if (taxCategory == null) {
                billingAccountRecordDto.getErrorMessage().append("The tax category " + billingAccountRecordDto.getBillingTaxCategory() + " is not found");
                return false;
            }
        } else {
            taxCategory = getTaxCategory(billingAccountRecordDto.getBillingAccount());
            if (taxCategory == null) {
                billingAccountRecordDto.getErrorMessage().append("Category missing or does not exist for the billing account " + billingAccountRecordDto.getBillingAccount().getCode());
                return false;
            }
            if (!StringUtils.isBlank(billingAccountRecordDto.getBillingTaxCategory()) && !billingAccountRecordDto.getBillingTaxCategory().equalsIgnoreCase(taxCategory.getCode())) {
                billingAccountRecordDto.getErrorMessage().append("The provided tax category is not the same with that of billing account");
                return false;
            }
        }
        Boolean vatAuthentication = (Boolean) taxCategory.getCfValue("vatAuthNrRequired");
        if (vatAuthentication != null && vatAuthentication == true) {
            billingAccountRecordDto.getErrorMessage().append("The tax category " + billingAccountRecordDto.getBillingTaxCategory() + " requires VAT_NO value so account must be set up manually.’");
            return false;
        }
        return true;
    }

    /**
     * Create billing account hierarchy
     *
     * @param billingAccountRecordDto the bill account Dto
     * @throws BusinessException the business exception
     */
    private void createBillingAccountHierarchy(BillingAccountRecordDto billingAccountRecordDto) throws BusinessException {
        billingAccountRecordDto.setBillingAccount(createBillingAccount(billingAccountRecordDto));
        if (!StringUtils.isBlank(billingAccountRecordDto.getSubscriptionCode())) {
            createUserAccountHierarchy(billingAccountRecordDto);
        }
    }

    /**
     * Create billing account
     *
     * @param billingAccountRecordDto the bill account Dto
     * @return the billing account
     * @throws BusinessException the business exception
     */
    private BillingAccount createBillingAccount(BillingAccountRecordDto billingAccountRecordDto) throws BusinessException {
        String sequenceNumber = billingAccountService.getNextSequence();
        billingAccountRecordDto.setBillingAccountCode(billingAccountRecordDto.getServiceType() + "_" + sequenceNumber);
        BillingAccount billingAccount = new BillingAccount();
        billingAccount.setCode(billingAccountRecordDto.getBillingAccountCode());
        billingAccount.setDescription(getDescription(billingAccountRecordDto));
        billingAccount.setExternalRef1(sequenceNumber);
        billingAccount.setCustomerAccount(billingAccountRecordDto.getCustomerAccount());
        billingAccount.setBillingCycle(billingAccountRecordDto.getBillingCycle());
        billingAccount.setName(getName(billingAccountRecordDto.getCustomerAccount().getName()));
        billingAccount.setAddress(getAddress(billingAccountRecordDto));
        TradingCountry tradingCountry = tradingCountryService.findByTradingCountryCode("IE");
        billingAccount.setTradingCountry(tradingCountry);
        TradingLanguage tradingLanguage = tradingLanguageService.findByTradingLanguageCode("ENG");
        billingAccount.setTradingLanguage(tradingLanguage);
        billingAccount.setTaxCategory(taxCategoryService.findByCode(billingAccountRecordDto.getBillingTaxCategory()));
        billingAccount.setCfValue(BillingAccountCFsEnum.SERVICE_TYPE.name(), billingAccountRecordDto.getServiceType());
        billingAccountService.createBillingAccount(billingAccount);
        return billingAccount;
    }

    /**
     * Create user account hierarchy
     *
     * @param billingAccountRecordDto the bill account Dto
     * @throws BusinessException the business exception
     */
    private void createUserAccountHierarchy(BillingAccountRecordDto billingAccountRecordDto) throws BusinessException {
        billingAccountRecordDto.setUserAccount(userAccountService.findByCode(billingAccountRecordDto.getSubscriptionCode()));
        // if user account doesn't exist, create new one
        if (billingAccountRecordDto.getUserAccount() == null) {
            billingAccountRecordDto.setUserAccount(new UserAccount());
            billingAccountRecordDto.getUserAccount().setCode(billingAccountRecordDto.getSubscriptionCode());
            billingAccountRecordDto.getUserAccount().setName(billingAccountRecordDto.getBillingAccount().getName());
            userAccountService.createUserAccount(billingAccountRecordDto.getBillingAccount(), billingAccountRecordDto.getUserAccount());
        }
        createSubscription(billingAccountRecordDto);
    }

    /**
     * Set the service instance Cf
     *
     * @param billingAccountRecordDto the bill account Dto
     * @param serviceInstance         the service instance
     */
    private void setServiceInstanceCf(BillingAccountRecordDto billingAccountRecordDto, ServiceInstance serviceInstance) {
        Map<String, String> cfValue = new HashMap<>();
        Map<String, Object> serviceInstanceValuesCF = getServiceParameters(billingAccountRecordDto);

        String key = CustomFieldValue.MATRIX_KEY_SEPARATOR;
        cfValue.put(key, utilService.getCFValue(serviceInstance, ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), serviceInstanceValuesCF));
        utilService.addNewVersionCfValue(serviceInstance, DateUtils.truncateTime(billingAccountRecordDto.getStartDate()),
                ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name(), null, cfValue);
    }

    /**
     * Create service instance
     *
     * @param billingAccountRecordDto the bill account Dto
     * @param subscription            the subscription
     * @return service instance
     */
    private ServiceInstance createServiceInstance(BillingAccountRecordDto billingAccountRecordDto, Subscription subscription) {

        ServiceInstance serviceInstance = new ServiceInstance();
        serviceInstance.setVersion(0);
        serviceInstance.setCode(billingAccountRecordDto.getBillCode());
        //serviceInstance.setDescription(billingAccountRecordDto.getTariff().get(Catalog.DESCRIPTION.name()));
        serviceInstance.setQuantity(BigDecimal.ONE);
        serviceInstance.setServiceTemplate(billingAccountRecordDto.getServiceTemplate());
        serviceInstance.setSubscription(subscription);
        serviceInstance.setSubscriptionDate(billingAccountRecordDto.getStartDate());

        setServiceInstanceCf(billingAccountRecordDto, serviceInstance);

        return serviceInstance;
    }

    private Map<String, Object> getServiceParameters(BillingAccountRecordDto billingAccountRecordDto) {
        Map<String, Object> serviceParameters = new HashMap<>();

        serviceParameters.put(ServiceParametersCFEnum.AMTOOC.getLabel(), billingAccountRecordDto.getAmount());
        serviceParameters.put(ServiceParametersCFEnum.QUANTITY.getLabel(), String.valueOf(BigDecimal.ONE));
        return serviceParameters;
    }

    /**
     * Get charge initialization parameters from a tariff line
     *
     * @param billingAccountRecordDto the billing account record Dto
     * @param chargeType              Charge type
     * @return Charge initialization parameters from the tariff
     * @throws ValidationException the validation exception
     */
    private ChargeInitializationParametersDto getChargeInitParametersFromTariff(BillingAccountRecordDto billingAccountRecordDto, ChargeTypeEnum chargeType) throws ValidationException {

        Map<String, Object> serviceParameters = getServiceParameters(billingAccountRecordDto);

        return utilService.getChargeInitParametersFromTariff(billingAccountRecordDto.getOfferTemplate(), billingAccountRecordDto.getServiceTemplate(),
                billingAccountRecordDto.getBillingAccount(), serviceParameters,
                billingAccountRecordDto.getStartDate(), BigDecimal.ONE, chargeType);
    }


    /**
     * Activate service
     *
     * @param billingAccountRecordDto the bill account Dto
     * @return the service instance
     * @throws BusinessException the business exception
     */
    private ServiceInstance activateService(BillingAccountRecordDto billingAccountRecordDto, Subscription subscription) throws BusinessException {
        ServiceInstance serviceInstance = createServiceInstance(billingAccountRecordDto, subscription);
        serviceInstanceService.create(serviceInstance);

        // activate service
        serviceInstanceService.serviceInstanciation(serviceInstance);

        if (serviceInstance.getRecurringChargeInstances() != null && !serviceInstance.getRecurringChargeInstances().isEmpty()) {
            ChargeInitializationParametersDto chargeInitParameters = getChargeInitParametersFromTariff(billingAccountRecordDto, ChargeTypeEnum.R);
            utilService.updateRecurringChargeInstances(serviceInstance, chargeInitParameters);
        }

        if (serviceInstance.getSubscriptionChargeInstances() != null && !serviceInstance.getSubscriptionChargeInstances().isEmpty()) {
            ChargeInitializationParametersDto chargeInitParameters = getChargeInitParametersFromTariff(billingAccountRecordDto, ChargeTypeEnum.O);
            utilService.updateSubscriptionChargeInstances(serviceInstance, chargeInitParameters);
        }
        serviceInstanceService.serviceActivation(serviceInstance);
        return serviceInstance;
    }

    /**
     * Create new subscription
     *
     * @param billingAccountRecordDto the bill account Dto
     * @throws BusinessException the business exception
     */
    private Subscription createNewSubscription(BillingAccountRecordDto billingAccountRecordDto) throws BusinessException {
        Subscription subscription = new Subscription();
        subscription.setCode(billingAccountRecordDto.getSubscriptionCode());
        subscription.setUserAccount(billingAccountRecordDto.getUserAccount());
        subscription.setOffer(billingAccountRecordDto.getOfferTemplate());
        subscription.setSeller(billingAccountRecordDto.getSeller());
        subscription.setSubscriptionDate(billingAccountRecordDto.getStartDate());
        subscription.setStatusDate(billingAccountRecordDto.getStartDate());
        utilService.setServiceIdCF(subscription);
        subscriptionService.create(subscription);
        return subscription;
    }

    /**
     * Create new access point
     *
     * @param billingAccountRecordDto the bill account Dto
     * @param subscription            the subscription
     * @return the access point
     * @throws BusinessException the business exception
     */
    private Access createAccessPoint(BillingAccountRecordDto billingAccountRecordDto, Subscription subscription) throws BusinessException {
        Access accessPoint = new Access();
        accessPoint.setAccessUserId(billingAccountRecordDto.getSubscriptionCode());
        accessPoint.setStartDate(billingAccountRecordDto.getStartDate());
        accessPoint.setSubscription(subscription);
        accessService.create(accessPoint);
        return accessPoint;
    }

    /**
     * Create subscription
     *
     * @param billingAccountRecordDto the bill account Dto
     * @throws BusinessException the business exception
     */
    private void createSubscription(BillingAccountRecordDto billingAccountRecordDto) throws BusinessException {
        Subscription subscription = createNewSubscription(billingAccountRecordDto);

        // add access point
        Access accessPoint = createAccessPoint(billingAccountRecordDto, subscription);
        subscription.getAccessPoints().add(accessPoint);

        activateService(billingAccountRecordDto, subscription);
        subscriptionService.update(subscription);
    }

    /**
     * Get the copy of description
     *
     * @param billingAccountRecordDto the bill account Dto
     * @return the copy of description
     */
    private String getDescription(BillingAccountRecordDto billingAccountRecordDto) {
        return !StringUtils.isBlank(billingAccountRecordDto.getBillingAccountName()) ? billingAccountRecordDto.getBillingAccountName() : billingAccountRecordDto.getCustomerAccount().getDescription();
    }

    /**
     * Get the copy of name
     *
     * @param name the source name
     * @return the copy of name
     */
    private Name getName(Name name) {
        Name newName = null;
        try {
            if (name != null) {
                newName = (Name) name.clone();
            }
            /*
             * newName = name != null ? (Name) name.clone() : new Name(); if (!StringUtils.isBlank(billingAccountName)) { newName.setLastName(billingAccountName); }
             */

        } catch (CloneNotSupportedException e) {
        }
        return newName;
    }

    /**
     * Get the copy of address
     *
     * @param billingAccountRecordDto the bill account Dto
     * @return the copy of address
     */
    private Address getAddress(BillingAccountRecordDto billingAccountRecordDto) {
        Address address = billingAccountRecordDto.getCustomerAccount().getAddress();
        Address newAddress = null;
        try {
            newAddress = address != null ? (Address) address.clone() : new Address();
            if (!StringUtils.isBlank(billingAccountRecordDto.getBillingAddress1())) {
                newAddress.setAddress1(billingAccountRecordDto.getBillingAddress1());
            }
            if (!StringUtils.isBlank(billingAccountRecordDto.getBillingAddress2())) {
                newAddress.setAddress2(billingAccountRecordDto.getBillingAddress2());
            }
            if (!StringUtils.isBlank(billingAccountRecordDto.getBillingAddress3())) {
                newAddress.setAddress3(billingAccountRecordDto.getBillingAddress3());
            }
            if (!StringUtils.isBlank(billingAccountRecordDto.getBillingCity())) {
                newAddress.setCity(billingAccountRecordDto.getBillingCity());
            }
            if (!StringUtils.isBlank(billingAccountRecordDto.getBillingPostalCode())) {
                newAddress.setZipCode(billingAccountRecordDto.getBillingPostalCode());
            }
        } catch (CloneNotSupportedException e) {
        }
        return newAddress;
    }
}