package com.eir.script;

import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.jobs.JobInstance;
import org.meveo.service.crm.impl.CustomFieldInstanceService;
import org.meveo.service.job.JobInstanceService;
import org.meveo.service.script.Script;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Map;

/**
 * Eir Exclude charges from billing script
 *
 * @author Abdellatif BARI
 */
@Stateless
public class ExcludeChargesFromBillingScript extends Script {

    @Inject
    JobInstanceService jobInstanceService;

    @Inject
    protected CustomFieldInstanceService customFieldInstanceService;

    @Override
    public void execute(Map<String, Object> context) throws BusinessException {
        JobInstance jobInstance = jobInstanceService.findByCode("ExcludeChargesFromBillingJob");
        if (jobInstance == null) {
            throw new BusinessException("the ExcludeChargesFromBillingJob job instance is not found");
        }
        String cfCode = "ScriptingJob_SQLQuery";
        String sqlQuery = (String) jobInstance.getParamValue(cfCode);
        if (StringUtils.isBlank(sqlQuery)) {
            sqlQuery = (String) customFieldInstanceService.getCFValue(jobInstance, cfCode);
        }
        if (StringUtils.isBlank(sqlQuery)) {
            throw new BusinessException("the sql query is missing");
        }

        try {
            String[] queries = sqlQuery.split(";");
            String report = "";
            int total = 0;
            for (int i = 0; i < queries.length; i++) {
                String query = queries[i];

                Integer itemsNumber = jobInstanceService.getEntityManager().createNativeQuery(query).executeUpdate();
                if (i == 0) {
                    context.put(Script.JOB_RESULT_NB_OK, itemsNumber);
                }
                report = report + itemsNumber + " records updated in the query starting with : " + query.split("\\s+")[0] + " " + query.split("\\s+")[1];
                total = total + itemsNumber;
            }
            if (total != 0) {
                context.put(Script.JOB_RESULT_REPORT, report);
            }
        } catch (Exception ex) {
            throw new BusinessException(ex.getMessage(), ex);
        }
    }
}