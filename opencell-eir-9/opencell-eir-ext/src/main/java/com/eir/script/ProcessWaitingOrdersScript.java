package com.eir.script;

import com.eir.api.order.BaseOrderApi;
import com.eir.api.order.OrderApi;
import com.eir.commons.enums.OrderProcessingTypeEnum;
import com.eir.commons.enums.customfields.OrderCFsEnum;
import com.eir.service.commons.UtilService;
import com.eir.service.order.impl.OrderService;
import org.meveo.admin.exception.BusinessException;
import org.meveo.api.commons.Utils;
import org.meveo.api.dto.order.AttributeDto;
import org.meveo.api.dto.order.OrderDto;
import org.meveo.api.dto.order.OrderLineDto;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.SubscriptionStatusEnum;
import org.meveo.model.order.Order;
import org.meveo.model.order.OrderStatusEnum;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.script.Script;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Stateless
public class ProcessWaitingOrdersScript extends Script {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    @Inject
    private OrderService orderService;
    @Inject
    private SubscriptionService subscriptionService;
    @Inject
    private UtilService utilService;
    @Inject
    private OrderApi orderApi;


    /**
     * Entry Point for script
     */
    @Override
    public void execute(Map<String, Object> context) throws BusinessException {

        log.debug("******Starting of script ProcessWaitingOrdersScript******");

        List<Order> waitingOrders = orderService.getWaitingOrders();
        int nbKO = 0;
        String report = "";
        List<String> reprocessedOrders = new ArrayList<String>();
        List<String> warningOrders = new ArrayList<String>();
        if (waitingOrders != null) {
            OrderDto dto;
            List<Long> matchedWaitingOrders;
            List<Subscription> activeSubscriptions;
            boolean areAllSubscriptionsActive = true;
            Date latestSubscriptionDate;
            List<String> assoccircidList = new ArrayList<>();
            for (Order order : waitingOrders) {
                try {
                    dto = orderService.getOrderDto(order, null);
                    assoccircidList = getAssoccircidAttribute(dto);
                    areAllSubscriptionsActive = false;
                    activeSubscriptions = new ArrayList<>();
                    matchedWaitingOrders = new ArrayList<>();
                    latestSubscriptionDate = null;
                    List<Subscription> subscriptions = subscriptionService.findByCodes(assoccircidList);
                    List<Long> waitingOrdersIds = orderService.findWaitingOrders(order.getCode(), assoccircidList);
                    if (waitingOrdersIds != null && !waitingOrdersIds.isEmpty()) {
                        matchedWaitingOrders.addAll(waitingOrdersIds);
                    }
                    if (subscriptions != null && !subscriptions.isEmpty()) {
                        areAllSubscriptionsActive = (subscriptions.size() == assoccircidList.size());//if assoccircidList does not exist all in data base, areAllSubscriptionsActive is false
                        for (Subscription subscription : subscriptions) {
                            if ((subscription == null || subscription.getStatus() != SubscriptionStatusEnum.ACTIVE)) {
                                areAllSubscriptionsActive = false;
                            } else {
                                latestSubscriptionDate = (isNullDate(latestSubscriptionDate)
                                        || (!isNullDate(subscription.getSubscriptionDate()) && subscription.getSubscriptionDate().after(latestSubscriptionDate)))
                                        ? subscription.getSubscriptionDate()
                                        : latestSubscriptionDate;
                                activeSubscriptions.add(subscription);
                            }
                        }
                    }

                    if (!areAllSubscriptionsActive && matchedWaitingOrders.isEmpty()) {
                        // If the three following conditions were not verified, keep the order in waiting
                        // status.
                        warningOrders.add(order.getCode());
                    } else if (areAllSubscriptionsActive) { // extract "Subscription date" latest date from found
                        // subscriptions.
                        Date lastEffectiveDate = getOrderBillEffectDate(order);
                        Date newEffectDate = (!isNullDate(latestSubscriptionDate) && !isNullDate(lastEffectiveDate) && latestSubscriptionDate.after(lastEffectiveDate)) ? latestSubscriptionDate : null;
                        updateEffectiveDateAndReprocess(order, newEffectDate, reprocessedOrders, 9, 0);
                    } else if (activeSubscriptions.isEmpty() && !matchedWaitingOrders.isEmpty()) {
                        List<Order> matchedOrders = matchedWaitingOrders.stream().map(id -> orderService.findById(id)).collect(Collectors.toList());
                        Date lastEffectiveDate = getLastEffectiveDate(order, matchedOrders);
                        updateEffectiveDate(matchedOrders, lastEffectiveDate, reprocessedOrders, 9, 0);
                        updateEffectiveDateAndReprocess(order, lastEffectiveDate, reprocessedOrders, 0, 120000);
                    } else if (!activeSubscriptions.isEmpty() && !matchedWaitingOrders.isEmpty()) {
                        List<Order> matchedOrders = matchedWaitingOrders.stream().map(id -> orderService.findById(id)).collect(Collectors.toList());
                        Date lastEffectiveDate = getLastEffectiveDate(order, matchedOrders);
                        if (isNullDate(lastEffectiveDate) || (!isNullDate(latestSubscriptionDate) && latestSubscriptionDate.after(lastEffectiveDate))) {
                            lastEffectiveDate = latestSubscriptionDate;
                        }
                        updateEffectiveDate(matchedOrders, lastEffectiveDate, reprocessedOrders, 9, 0);
                        updateEffectiveDateAndReprocess(order, lastEffectiveDate, reprocessedOrders, 0, 120000);
                    }
                } catch (Exception e) {
                    log.error(e.getMessage(), e);
                    report.concat("Error : " + order.getCode() + ": " + e.getMessage() + " \n");
                    nbKO++;
                }

            }
        }
        warningOrders.removeAll(reprocessedOrders);
        context.put(JOB_RESULT_NB_WARN, warningOrders.size());
        context.put(JOB_RESULT_NB_OK, reprocessedOrders.size());
        context.put(JOB_RESULT_NB_KO, nbKO);
        context.put(JOB_RESULT_REPORT, report.concat(getReport(warningOrders)));
        context.put(JOB_RESULT_TO_PROCESS, (reprocessedOrders.size() + nbKO + warningOrders.size()));

    }

    private String getReport(List<String> warningOrders) {
        StringBuilder report = new StringBuilder();
        for (String code : warningOrders) {
            report.append("Warning Order : " + code + ": ASSOCCIRCID are not all active and no corresponding waiting order exists! \n");
        }
        return report.toString();
    }

    private boolean isNullDate(Date date) {
        return date == null || "".equals("" + date) || "null".equals("" + date);
    }

    /**
     * Update effective date.
     *
     * @param matchedWaitingOrders the matched waiting orders
     * @param lastEffectiveDate    the last effective date
     */
    private void updateEffectiveDate(List<Order> matchedWaitingOrders, Date lastEffectiveDate, List<String> reprocessedOrders, int priority, int delay) {
        for (Order order : matchedWaitingOrders) {
            updateEffectiveDateAndReprocess(order, lastEffectiveDate, reprocessedOrders, priority, delay);
        }

    }

    /**
     * Gets the last effective date.
     *
     * @param order0
     * @param matchedWaitingOrders the matched waiting orders
     * @return the last effective date
     */
    private Date getLastEffectiveDate(Order order0, List<Order> matchedWaitingOrders) {
        Date lastEffectiveDate = getOrderBillEffectDate(order0);
        Date orderEffectDate = null;
        for (Order matchedOrder : matchedWaitingOrders) {
            orderEffectDate = getOrderBillEffectDate(matchedOrder);
            if (isNullDate(lastEffectiveDate) || (!isNullDate(orderEffectDate) && orderEffectDate.after(lastEffectiveDate))) {
                lastEffectiveDate = orderEffectDate;
            }
        }
        return lastEffectiveDate;
    }

    /**
     * Get order bill effect date
     *
     * @return the order bill effect date
     */
    private Date getOrderBillEffectDate(Order refOrder) {
        Date lastEffectiveDate = null;
        if (refOrder != null) {
            String orderMessage = (String) refOrder.getCfValuesNullSafe().getValue(OrderCFsEnum.ORDER_ORIGINAL_MESSAGE.name());

            Pattern pattern = Pattern.compile("<Effect_Date>(.*?)</Effect_Date>", Pattern.DOTALL);
            Matcher matcher = pattern.matcher(orderMessage);
            Date orderEffectDate;
            while (matcher.find()) {
                orderEffectDate = DateUtils.parseDateWithPattern(matcher.group(1), Utils.DATE_TIME_PATTERN);
                if (isNullDate(lastEffectiveDate) || (!isNullDate(orderEffectDate) && orderEffectDate.after(lastEffectiveDate))) {
                    lastEffectiveDate = orderEffectDate;
                }
            }
        }
        return lastEffectiveDate;
    }

    private void updateEffectiveDateAndReprocess(Order order, Date date, List<String> reprocessedOrders, int priority, int delay) {
        if (!reprocessedOrders.contains(order.getCode()) && order.getStatus() == OrderStatusEnum.WAITING) {
            if (!isNullDate(date)) {
                String xmlOrder = (String) order.getCfValue("ORDER_ORIGINAL_MESSAGE");
                if (StringUtils.isBlank(xmlOrder)) {
                    throw new BusinessException("The ORDER_ORIGINAL_MESSAGE custom field is missing");
                }
                xmlOrder = xmlOrder.replaceAll("<Effect_Date>.*</Effect_Date>",
                        "<Effect_Date>" + DateUtils.formatDateWithPattern(date, Utils.DATE_TIME_PATTERN) + "</Effect_Date>");
                order.getCfValuesNullSafe().setValue(OrderCFsEnum.ORDER_ORIGINAL_MESSAGE.name(), xmlOrder);
                orderService.update(order);
            }
            releaseOrder(order, priority, delay);
            reprocessedOrders.add(order.getCode());
        }
    }

    private void releaseOrder(Order order, int priority, int delay) {
        OrderDto orderDto = orderService.getOrderDto(order.getCode());
        BaseOrderApi orderAPi = orderApi.getOrderApi(orderDto);
        orderDto.setProcessingType(OrderProcessingTypeEnum.RELEASE);
        orderAPi.updateOrderStatus(orderAPi::setOrderInProgressStatus, orderDto);
        orderDto.setReleaseStatus(OrderStatusEnum.PENDING);
        orderDto.setReleaseDescription("Released from Process Waiting orders JOB");
        utilService.sendToOrderQueue(orderDto, priority, delay);
    }

    /**
     * Get the order line attribute by code
     *
     * @return the order line attribute by code
     */
    public List<String> getAssoccircidAttribute(OrderDto dto) {
        List<String> assoccirIdList = new ArrayList<String>();
        for (OrderLineDto lineDto : dto.getOrderLines()) {
            for (AttributeDto attributeDTO : lineDto.getAttributes()) {
                if ("ASSOCCIRCID".equals(attributeDTO.getCode())) {
                    assoccirIdList.add(attributeDTO.getValue());
                }
            }
        }
        return assoccirIdList;
    }

}
