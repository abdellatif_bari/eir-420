package com.eir.script;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ElementNotFoundException;
import org.meveo.admin.exception.NoChargeException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.ChargeApplicationModeEnum;
import org.meveo.model.billing.OneShotChargeInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.UserAccount;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.OneShotChargeTemplate;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.crm.Customer;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.admin.impl.SellerService;
import org.meveo.service.base.PersistenceService;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.billing.impl.OneShotChargeInstanceService;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.billing.impl.UserAccountService;
import org.meveo.service.catalog.impl.OfferTemplateService;
import org.meveo.service.catalog.impl.OneShotChargeTemplateService;
import org.meveo.service.catalog.impl.ServiceTemplateService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.script.Script;
import org.primefaces.model.SortOrder;

import com.eir.commons.enums.ApplicationPropertiesEnum;
import com.eir.commons.enums.customfields.ChargeInstanceCFsEnum;
import com.eir.commons.enums.customfields.SubscriptionCFsEnum;
import com.eir.service.commons.UtilService;

/**
 * The Class GeneratingMubFactoredUsageScript.
 */
@Stateless
public class MubChargeRecordScript extends Script {

    private static final String EIR_OR_TARIFF_PLAN_MAP = "EIR_OR_TARIFF_PLAN_MAP";
    private static final String EIR_OR_TARIFF = "EIR_OR_TARIFF";
    private static final String PRICE = "price";
    /** The billing account service. */
    @Inject
    private BillingAccountService billingAccountService;
    @Inject
    private SubscriptionService subscriptionService;
    @Inject
    private SellerService sellerService;
    @Inject
    private UserAccountService userAccountService;
    @Inject
    private OfferTemplateService offerTemplateService;
    @Inject
    private OneShotChargeTemplateService oneShotChargeTemplateService;
    @Inject
    private OneShotChargeInstanceService oneShotChargeInstanceService;
    @Inject
    private CustomTableService customTableService;
    @Inject
    ServiceTemplateService serviceTemplateService;
    @Inject
    private UtilService utilService;

    private static final long serialVersionUID = 1L;

    private static final String BILLING_ACCOUNT = "BILLING_ACCOUNT";
    private static final String RETRIEVAL_DATE = "RETRIEVAL_DATE";
    private static final String FACTORED_USAGE = "FACTORED_USAGE";
    private static final String NO_OF_PORTS = "NO_OF_PORTS";
    private static final String MUB_USG = "MUB_USG";
    private static final String EIR_MUB_OFFER = "EIR_MUB";

    /** The Constant MUB_RECORD. */
    private static final String MUB_RECORD = "record";

    private static final String MUB_CHARGE_TEMPLATE_CODE = "MUB_CHARGE";

    /**
     * script main execute method.
     *
     * @param methodContext method context params
     * @throws BusinessException business exception
     */
    @Override
    public void execute(Map<String, Object> methodContext) throws BusinessException {

        @SuppressWarnings("unchecked")
        Map<String, String> item = (Map<String, String>) methodContext.get(MUB_RECORD);

        String billingAccount = item.get(BILLING_ACCOUNT);
        String retrievalDate = item.get(RETRIEVAL_DATE);
        Double factoredUsage = Double.valueOf(item.get(FACTORED_USAGE).replace(',', '.'));
        int numberOfPorts = Integer.valueOf(item.get(NO_OF_PORTS));

        BillingAccount ba = billingAccountService.findByCode(billingAccount);
        if (StringUtils.isBlank(ba)) {
            throw new ElementNotFoundException(billingAccount, "Billing Account");
        }
        double avgSpeed = 0;
        if (numberOfPorts != 0) {
            avgSpeed = factoredUsage * 1000 / numberOfPorts; // to get the avg speed per user in Kb/Sec
            double modulo = avgSpeed % 25; // the rest of division by 25
            avgSpeed = avgSpeed - modulo + (modulo > 0 ? 25 : 0); // result = avgSpeed - modulo + 25 (if modulo > 0) or result = avgSpeed - modulo + 0 (if modulo = 0)
        }

        OfferTemplate offerTemplate = offerTemplateService.findByCode(EIR_MUB_OFFER);
        BigDecimal rate = getRateBySpeed((int) avgSpeed, offerTemplate, ba.getCustomerAccount().getCustomer(), getDate(retrievalDate));

        if (rate == null) {
            return;
        }

        OneShotChargeTemplate chargeTemplate = oneShotChargeTemplateService.findByCode(MUB_CHARGE_TEMPLATE_CODE);
        if (chargeTemplate == null) {
            throw new ElementNotFoundException(MUB_CHARGE_TEMPLATE_CODE, "One shot charge");
        }

        BigDecimal chargePrice = numberOfPorts != 0 ? rate.multiply(BigDecimal.valueOf(numberOfPorts)) : rate.multiply(BigDecimal.valueOf(factoredUsage));

        String filename = (String) methodContext.get("origin_filename");
        // find Mub dummy subscription or create it
        Subscription sub = findOrCreateSubscription(ba, offerTemplate, filename);

        OneShotChargeInstance chargeInstance = new OneShotChargeInstance();
        Map<String, String> cfValue = new HashMap<>();
        Map<String, Object> serviceParameters = new HashMap<>();
        serviceParameters.put("quantity", "1");
        cfValue.put("mub_charge", utilService.getCFValue(chargeInstance, "SERVICE_PARAMETERS", serviceParameters));


        chargeInstance.setCfValue(ChargeInstanceCFsEnum.SERVICE_PARAMETERS.name(), cfValue);
        chargeInstance.setCfValue(ChargeInstanceCFsEnum.SERVICE_CODE.name(), MUB_USG);
        chargeInstance.setCode(MUB_USG + "_NRC");
        if (filename != null) {
            chargeInstance.setCfValue(ChargeInstanceCFsEnum.FILENAME.name(), filename);
        }        

        oneShotChargeInstanceService.oneShotChargeApplication(sub, null, chargeTemplate, null, DateUtils.setDayToDate(getDate(retrievalDate), 1), chargePrice, null, null, null, null, null, null, null,
            chargeInstance.getCfValues(), true, ChargeApplicationModeEnum.SUBSCRIPTION);
    }

    private Subscription findOrCreateSubscription(BillingAccount ba, OfferTemplate offer, String filename) throws BusinessException {

        String code = "MUB00_" + ba.getCode();

        Subscription sub = subscriptionService.findByCode(code);

        if (sub == null) {
            UserAccount ua = userAccountService.findByCode(code);
            if (ua == null) {
                ua = new UserAccount();
                ua.setCode(code);
                userAccountService.createUserAccount(ba, ua);
            }

            sub = new Subscription();
            sub.setCode(code);
            sub.setUserAccount(ua);
            sub.setOffer(offer);
            sub.setSeller(sellerService.findByCode(ApplicationPropertiesEnum.OPENEIR_SELLER_CODE.getProperty()));
            if (filename != null) {
                sub.setCfValue(SubscriptionCFsEnum.FILENAME.name(), filename);
            }
            subscriptionService.create(sub);
        }
        return sub;
    }

    private BigDecimal getRateBySpeed(int avgSpeed, OfferTemplate offerTemplate, Customer customer, Date operationDate) {
        Map<String, Object> searchCriteria = new HashMap<>();

        // -- Determine price plan applicable - use main WO operation date as reference date
        Object rapCustomer = customer.getCfValue("STANDARD_CUSTOMER");
        if (rapCustomer == null || (boolean) rapCustomer) {
            searchCriteria.put("customer_id", PersistenceService.SEARCH_IS_NULL);
        } else {
            searchCriteria.put("eqOptional customer_id", customer.getId());
        }

        searchCriteria.put("offer_id", offerTemplate.getId());
        searchCriteria.put("minmaxOptionalRange valid_from valid_to", operationDate);

        List<Map<String, Object>> tariffPlans = customTableService.list(EIR_OR_TARIFF_PLAN_MAP, new PaginationConfiguration(searchCriteria, "customer_id", SortOrder.ASCENDING));
        if (tariffPlans.isEmpty()) {
            throw new NoChargeException("Unable to determine a tariff plan for customer " + customer.getCode() + " offer " + offerTemplate.getCode() + " and date " + operationDate);
        }

        Number tariffPlanId = (Number) tariffPlans.get(0).get("tariff_plan_id");

        searchCriteria = new HashMap<>();

        // service template = MUB_USG
        ServiceTemplate serviceTemplate = serviceTemplateService.findByCode(MUB_USG);
        if (serviceTemplate == null) {
            throw new BusinessException("Service " + MUB_USG + " was not found");
        }

        searchCriteria.put("tariff_plan_id", tariffPlanId);
        searchCriteria.put("offer_id", offerTemplate.getId());
        searchCriteria.put("service_id", serviceTemplate.getId());
        searchCriteria.put("average_speed", String.valueOf(avgSpeed));
        searchCriteria.put("minmaxOptionalRange valid_from valid_to", operationDate);

        List<Map<String, Object>> tariffRecords = customTableService.list(EIR_OR_TARIFF, new PaginationConfiguration(searchCriteria));
        if (tariffRecords.isEmpty()) {
            throw new BusinessException("No tarif Found! " + searchCriteria.toString());
        }

        return (BigDecimal) tariffRecords.get(0).get(PRICE);

    }

    /**
     * Get formatted date from XML Order
     *
     * @param date date attribute
     * @return Formatted date
     * @throws ParseException Parsing date exception
     */
    private Date getDate(String date) throws ValidationException {

        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        try {
            return format.parse(date);
        } catch (ParseException e) {
            throw new ValidationException("Date " + date + " does not match format 'dd.MM.yyyy'");
        }
    }
}
