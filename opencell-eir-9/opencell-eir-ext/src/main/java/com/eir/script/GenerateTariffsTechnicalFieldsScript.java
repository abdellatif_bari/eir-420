package com.eir.script;

import com.eir.commons.enums.TransformationRulesEnum;
import com.eir.commons.enums.customtables.CustomTableEnum;
import com.eir.commons.utils.TransformationRulesHandler;
import com.eir.service.commons.ImportService;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.transformer.AliasToEntityOrderedMapResultTransformer;
import org.meveo.service.script.Script;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Eir Generate the tariffs technical fields script
 *
 * @author Abdellatif BARI
 */
@Stateless
public class GenerateTariffsTechnicalFieldsScript extends Script {

    @Inject
    private ImportService importService;

    @Override
    public void execute(Map<String, Object> parameters) throws BusinessException {

        String tableName = (String) parameters.get("tableName");
        if (StringUtils.isBlank(tableName)) {
            throw new BusinessException("the table name is missing");
        }
        boolean priority = Boolean.valueOf((String) parameters.get("priority"));
        boolean hash_code = Boolean.valueOf((String) parameters.get("hash_code"));
        if (!priority && !hash_code) {
            throw new BusinessException("you should specify at least one field to be generated");
        }

        List<String> updatedFields = new ArrayList<>();
        if (priority) {
            updatedFields.add("priority");
        }
        if (hash_code) {
            updatedFields.add("hash_code");
        }

        String query = null;
        if (tableName.equalsIgnoreCase(CustomTableEnum.EIR_OR_TARIFF_STAGE.getCode())) {
            query = "select tariff.*, (cat.cf_values\\:\\:json->'offerType'->0->>'string') as offer_type " +
                    "from EIR_OR_TARIFF_STAGE tariff " +
                    "inner join cat_offer_template offer_template on offer_template.code = tariff.offer and offer_template.type = 'OFFER' " +
                    "inner join cat_product_offer_tmpl_cat ass on ass.product_id = offer_template.id " +
                    "inner join cat_offer_template_category cat on cat.id = ass.offer_template_cat_id ";
            //"where tariff.hash_code is null or trim(tariff.hash_code) = ''";
        } else if (tableName.equalsIgnoreCase(CustomTableEnum.EIR_OR_TARIFF.getCode())) {
            query = "select tariff.id, tariff.charge_type, tariff.action_qualifier, tariff.region, tariff.sla, tariff.density, tariff.class_of_service, tariff.ef, " +
                    "tariff.af, tariff.exchange_class, tariff.order_type, tariff.egress_group, tariff.price_plan, tariff.rating_scheme, tariff.bandwidth, " +
                    "tariff.transmission, tariff.commitment_period, tariff.zone_a, tariff.zone_b, tariff.service_category, tariff.analogue_quality, " +
                    "tariff.analogue_rate, tariff.cpe_type, tariff.cpe_description, tariff.ets_type, tariff.ind_01, tariff.ind_02, tariff.ind_03, tariff.ind_04, " +
                    "tariff.ind_05, tariff.ind_06, tariff.ind_07, tariff.ind_08, tariff.ind_09, tariff.ind_10, tariff.distance_from, tariff.distance_to, " +
                    "tariff.distance_unit, tariff.average_speed, tariff.quantity_from, tariff.quantity_to, tariff.quantity_unit, tariff.max_rated_quantity, " +
                    "tariff.price, tariff.hash_code, (cat.cf_values\\:\\:json->'offerType'->0->>'string') as offer_type, offer_template.code as offer, " +
                    "tariff_plan.code as tariff_plan, service_template.code as service, calendar.code as rec_calendar, accounting_code.code as accounting_code, " +
                    "sub_cat.code as invoice_subcat, tax_class.code as tax_class " +
                    "from EIR_OR_TARIFF tariff " +
                    "inner join cat_offer_template offer_template on offer_template.id = tariff.offer_id and offer_template.type = 'OFFER' " +
                    "inner join cat_product_offer_tmpl_cat ass on ass.product_id = offer_template.id " +
                    "inner join cat_offer_template_category cat on cat.id = ass.offer_template_cat_id " +
                    "inner join EIR_OR_TARIFF_PLAN tariff_plan on tariff_plan.id = tariff.tariff_plan_id " +
                    "inner join cat_service_template service_template on service_template.id = tariff.service_id " +
                    "inner join cat_calendar calendar on calendar.id = tariff.rec_calendar " +
                    "inner join billing_accounting_code accounting_code on accounting_code.id = tariff.accounting_code_id " +
                    "inner join billing_invoice_sub_cat sub_cat on sub_cat.id = tariff.invoice_subcat_id " +
                    "inner join billing_tax_class tax_class on tax_class.id = tariff.tax_class ";
            //"where tariff.hash_code is null or trim(tariff.hash_code) = ''";
        }

        if (!StringUtils.isBlank(query)) {
            Session session = importService.getEntityManager().unwrap(Session.class);
            SQLQuery q = session.createSQLQuery(query);
            q.setResultTransformer(AliasToEntityOrderedMapResultTransformer.INSTANCE);
            List<Map<String, Object>> tariffs = q.list();

            Map<String, TransformationRulesHandler> transformationRulesHandlers = new HashMap<>();
            for (TransformationRulesEnum offerType : TransformationRulesEnum.values()) {
                transformationRulesHandlers.put(offerType.name().toUpperCase(), importService.getTransformationRules(offerType.name()));
            }

            if (tariffs != null && !tariffs.isEmpty()) {
                TransformationRulesHandler transformationRulesHandler = null;
                for (Map<String, Object> record : tariffs) {
                    String offerType = (String) record.get("offer_type");
                    if (!StringUtils.isBlank(offerType) && transformationRulesHandlers.containsKey(offerType.toUpperCase())) {
                        transformationRulesHandler = transformationRulesHandlers.get(offerType.toUpperCase());
                        if (priority) {
                            importService.setPriority(record, transformationRulesHandler);
                        }
                        if (hash_code) {
                            importService.setHashCode(record, transformationRulesHandler);
                        }
                    }
                }
                if (transformationRulesHandler != null) {
                    Map<String, Object> tableFields = null;
                    if (tableName.equalsIgnoreCase(CustomTableEnum.EIR_OR_TARIFF_STAGE.getCode())) {
                        tableFields = transformationRulesHandler.getStageTableFields();
                    } else if (tableName.equalsIgnoreCase(CustomTableEnum.EIR_OR_TARIFF.getCode())) {
                        tableFields = transformationRulesHandler.getValidTableFields();
                    }
                    if (tableFields != null) {
                        importService.updateRecord(tableName.toUpperCase(), tableFields, updatedFields, tariffs);
                    }
                }
            }
        }
    }
}