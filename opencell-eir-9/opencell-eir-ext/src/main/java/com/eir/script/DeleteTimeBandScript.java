package com.eir.script;

import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import org.meveo.admin.exception.BusinessException;
import org.meveo.model.CustomTableEvent;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.script.Script;

/**
 * Delete TimeBand Script
 * 
 * 
 *
 */
@Stateless
public class DeleteTimeBandScript extends Script {

	@Inject
    private CustomTableService customTableService;

    
    @Override
    public void execute(Map<String, Object> initContext) throws BusinessException {
        log.info("#####################Starting of script DeleteTimeBandScript");
        try {
            CustomTableEvent timeBand = (CustomTableEvent) initContext.get("CONTEXT_ENTITY");
            Long timeBandId = timeBand.getId();
            List<String> timeBands = getTimePeriodByTimeBand(timeBandId);
            if (timeBands != null && timeBands.size() > 0) {
                throw new BusinessException(timeBand + ": This time band may not be deleted as it is assigned to a rate plan");
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage());

        }
    }

    
    /**
     * Gets the time period by time band.
     * @param timeBand
     * @return
     */
    @SuppressWarnings("unchecked")
    private List<String> getTimePeriodByTimeBand(Long timeBand) {
    	List<String> listTimeBands = null;
        String sql = "SELECT code FROM eir_u_tariff_plan where time_band=:id";
        Query query = customTableService.getEntityManager().createNativeQuery(sql);
		query.setParameter("id", timeBand);
		listTimeBands = query.getResultList();
        return listTimeBands;
    }
}