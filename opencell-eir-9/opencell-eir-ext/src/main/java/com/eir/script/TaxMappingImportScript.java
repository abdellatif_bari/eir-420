package com.eir.script;

import java.text.ParseException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.api.custom.CustomTableApi;
import org.meveo.api.dto.custom.CustomTableRecordDto;
import org.meveo.api.dto.custom.UnitaryCustomTableDataDto;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.bi.FlatFile;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.bi.impl.FlatFileService;
import org.meveo.service.script.Script;

import com.eir.service.taxMapping.TaxMapService;

@Stateless
public class TaxMappingImportScript extends Script {

    /**
     *
     */
    private static final long serialVersionUID = -8522810414743072926L;

    @Inject
    private TaxMapService taxMappingService;

    @Inject
    private CustomTableApi customTableApi;

    @Inject
    private FlatFileService flatFileService;

    private static Map<String, Map<String, Object>> scriptInstancesParameters = new HashMap<>();

    @Override
    public void execute(Map<String, Object> parameters) {

        try {
            Map<String, Object> taxMappingStageMap = initContext(parameters);
            processItem(taxMappingStageMap);
        } catch (Exception e) {
            log.error("error on process customer payments file {} ", e.getMessage(), e);
            if (e instanceof BusinessException) {
                throw e;
            } else {
                // wrap the exception in a business exception and throwing it
                throw new BusinessException(e);
            }
        }
    }

    /**
     * Process item
     *
     * @param taxMappingStageMap the tax mapping stage Dto
     * @throws BusinessException the business exception
     * @throws ParseException
     */
    private void processItem(Map<String, Object> taxMappingStageMap) throws BusinessException {
        String validFrom = (String) taxMappingStageMap.get("valid_from");
        String validTo = (String) taxMappingStageMap.get("valid_to");
        Date validFromDate = new Date();
        Date validToDate = new Date();
        validFromDate= DateUtils.parseDateWithPattern(validFrom, "dd/MM/yyyy");
        taxMappingStageMap.put("valid_from", validFromDate);
        if(validTo != null) {
	        validToDate= DateUtils.parseDateWithPattern(validTo, "dd/MM/yyyy");
	        taxMappingStageMap.put("valid_to", validToDate);
        }
        try {

            taxMappingService.validateTaxMapping(taxMappingStageMap, null);
        } catch (ParseException e) {
            throw new BusinessException(e);
        }
        
        saveRecord(taxMappingStageMap);
    }

    /**
     * Init context
     *
     * @param parameters parameters
     * @return the tax mapping stage map
     * @throws BusinessException the business exception
     */
    private Map<String, Object> initContext(Map<String, Object> parameters) throws BusinessException {
        Map<String, Object> taxMappingStageMap = (Map<String, Object>) parameters.get("record");
        if (taxMappingStageMap == null || taxMappingStageMap.isEmpty()) {
            throw new BusinessException("Record is missing or empty");
        }

        String fileName = (String) parameters.get("origin_filename");

        Map<String, Object> scriptInstanceParameters = scriptInstancesParameters.get(fileName);
        if (scriptInstanceParameters == null || scriptInstanceParameters.isEmpty()) {
            throw new BusinessException(
                    "The file " + fileName + " is not found or is not uploaded and validated by file format");
        }

        taxMappingStageMap.put("file_id", scriptInstanceParameters.get("flatFileId"));
        return taxMappingStageMap;
    }

    /**
     * Initialize method
     *
     * @param parameters the context
     * @throws BusinessException the business exception
     */
    @Override
    public void init(Map<String, Object> parameters) throws BusinessException {
        try {
            log.debug(" >>> Start LoadPendingTaxMappingScript -> init {} ", parameters.entrySet());
            String fileName = this.getContextParam(parameters, "origin_filename", true);
            FlatFile flatFile = flatFileService.getFlatFileByFileName(fileName);
            if (flatFile == null) {
                throw new BusinessException(
                        "The file " + fileName + " is not found or is not uploaded and validated by file format");
            }
            parameters.put("flatFileId", flatFile.getId());

            // Check if the file name is unique in the staging table.
            List<String> pendingTaxMappingList = taxMappingService
                    .getPendingTaxMappingsByFileName(flatFile.getFileOriginalName());
            if (pendingTaxMappingList != null && !pendingTaxMappingList.isEmpty()) {
                throw new BusinessException("There is already  pending tax mappings with the same file name");
            }

            scriptInstancesParameters.put(fileName, parameters);
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Populate tax mapping item
     *
     * @throws BusinessException the business exception
     */
    private void saveRecord(Map<String, Object> values) throws BusinessException {
        UnitaryCustomTableDataDto stageTaxMapping = new UnitaryCustomTableDataDto();
        stageTaxMapping.setCustomTableCode("EIR_TAX_MAPPING_STAGE");
        CustomTableRecordDto value = new CustomTableRecordDto(values);
        stageTaxMapping.setValue(value);

        customTableApi.create(stageTaxMapping);
    }

    /**
     * Get context parameters
     *
     * @param methodContext the context
     * @param key           the parameter key
     * @param mandatory     true is mandatory parameter
     * @return true is not blank parameter
     * @throws BusinessException the business exception
     */
    private String getContextParam(Map<String, Object> methodContext, String key, boolean mandatory)
            throws BusinessException {
        String value = (String) methodContext.get(key);
        if (mandatory && StringUtils.isBlank(value)) {
            throw new BusinessException(String.format("Missing context param : %s", key));
        }
        return value;
    }

}