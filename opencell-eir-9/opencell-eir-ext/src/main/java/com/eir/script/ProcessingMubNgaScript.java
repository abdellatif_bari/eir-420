package com.eir.script;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.ParamBean;
import org.meveo.commons.utils.ParamBeanFactory;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.billing.Subscription;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.script.Script;

/**
 * The Class ManualChargesScript.
 */
@Stateless
public class ProcessingMubNgaScript extends Script {

    private static final long serialVersionUID = 1L;
    /** The param bean factory. */
    @Inject
    private ParamBeanFactory paramBeanFactory;
    /** The Subscription service. */
    @Inject
    private SubscriptionService subscriptionService;

    private static final String SAMCIRCUITID = "samcircuitid";

    private static final String SAMCUSTOMERNAME = "samcustomername";

    private static final String SAMTIMEYYYYMMDDHHMISSROUND = "samtimeyyyymmddhhmissround";

    private static final String SAMTIMEEPOCROUND = "samtimeepocround";

    private static final String SAMTIMEYYYYMMDDHHMISS = "samtimeyyyymmddhhmiss";

    private static final String SAMTIMEEPOC = "samtimeepoc";

    // private static final String SAMMONITEREDSITENAME = "sammoniteredsitename";

    // private static final String SAMSAPNAME = "samsapname";

    private static final String SAMPORTID = "samportid";

    // private static final String SAMSERVICEID = "samserviceid";

    // private static final String SAMDESCRIPTION = "samdescription";

    // private static final String SAM1QUEUEID = "sam1queueid";

    private static final String SAM1INGRESSPACKETSIN = "sam1ingresspacketsin";

    private static final String SAM1INGRESSPACKETSOUT = "sam1ingresspacketsout";

    private static final String SAM1EGRESSPACKETSIN = "sam1egresspacketsin";

    private static final String SAM1EGRESSPACKETSOUT = "sam1egresspacketsout";

    // private static final String SAM3QUEUEID = "sam3queueid";

    private static final String SAM3INGRESSPACKETSIN = "sam3ingresspacketsin";

    private static final String SAM3INGRESSPACKETSOUT = "sam3ingresspacketsout";

    private static final String SAM3EGRESSPACKETSIN = "sam3egresspacketsin";

    private static final String SAM3EGRESSPACKETSOUT = "sam3egresspacketsout";

    // private static final String SAM6QUEUEID = "sam6queueid";

    private static final String SAM6INGRESSPACKETSIN = "sam6ingresspacketsin";

    private static final String SAM6INGRESSPACKETSOUT = "sam6ingresspacketsout";

    private static final String SAM6EGRESSPACKETSIN = "sam6egresspacketsin";

    private static final String SAM6EGRESSPACKETSOUT = "sam6egresspacketsout";

    /** The Constant MUB_RECORD. */
    private static final String MUB_RECORD = "record";

    /** The Constant DATETIME_FORMAT. */
    private static final String DATETIME_FORMAT = "ddMMyyyyHHmmss";
    
    private static final String CF_MUB_COUNTERS = "MUB_COUNTERS";
    
    private String cdrFolder;
    
    private String cdrFileNamePrefix;
    
    private String cdrFileName;

    @Override
    public void init(Map<String, Object> scriptContext) {
        cdrFolder = (String) scriptContext.get("INTERNAL_CDR_FOLDER");
        cdrFileNamePrefix = (String) scriptContext.get("INTERNAL_CDR_FILE_NAME_PREFIX");
        
        if(StringUtils.isBlank(cdrFolder)) {
            cdrFolder = "imports/MUB/cdr/input";
        }
        if(StringUtils.isBlank(cdrFileNamePrefix)) {
            cdrFileNamePrefix = "mub_cdr";
        }
        cdrFileName = cdrFileNamePrefix + DateUtils.formatDateWithPattern(new Date(), DATETIME_FORMAT) + ".cdr";
        cdrFolder = paramBeanFactory.getChrootDir() + File.separator + cdrFolder;
        File f = new File(cdrFolder);
        if (!f.exists()) {
            f.mkdirs();
        }
        cdrFileName = cdrFolder + File.separator + cdrFileName;
    }

    /**
     * script main execute method.
     *
     * @param methodContext method context params
     * @throws BusinessException business exception
     */
    @SuppressWarnings("unchecked")
    @Override
    public void execute(Map<String, Object> methodContext) throws BusinessException {

        /** The NumberFormat. */
        NumberFormat fmt = NumberFormat.getInstance(Locale.ENGLISH);
        
        String originalFileName = (String) methodContext.get("origin_filename");

        Map<String, String> item = (Map<String, String>) methodContext.get(MUB_RECORD);
        fmt.setGroupingUsed(false);
        fmt.setMaximumIntegerDigits(999);
        fmt.setMaximumFractionDigits(999);
        try {
//            mapItemFields(item);
            // String samserviceid = item.get(SAMSERVICEID).trim();
            String samportid = item.get(SAMPORTID).trim();
            // String samsapname = item.get(SAMSAPNAME).trim();
            // String sammoniteredsitename = item.get(SAMMONITEREDSITENAME).trim();
            String samtimeepoc = item.get(SAMTIMEEPOC).trim();
            String samtimeyyyymmddhhmiss = item.get(SAMTIMEYYYYMMDDHHMISS).trim();
            String samtimeepocround = item.get(SAMTIMEEPOCROUND).trim();
            String samtimeyyyymmddhhmissround = item.get(SAMTIMEYYYYMMDDHHMISSROUND).trim();
            String samcustomername = item.get(SAMCUSTOMERNAME).trim();
            String samcircuitid = item.get(SAMCIRCUITID).trim();
            // String samdescription = item.get(SAMDESCRIPTION).trim();
            // String sam1queueid = item.get(SAM1QUEUEID).trim();
            String sam1ingresspacketsin = item.get(SAM1INGRESSPACKETSIN).trim();
            String sam1ingresspacketsout = item.get(SAM1INGRESSPACKETSOUT).trim();
            String sam1egresspacketsin = item.get(SAM1EGRESSPACKETSIN).trim();
            String sam1egresspacketsout = item.get(SAM1EGRESSPACKETSOUT).trim();
            // String sam3queueid = item.get(SAM3QUEUEID).trim();
            String sam3ingresspacketsin = item.get(SAM3INGRESSPACKETSIN).trim();
            String sam3ingresspacketsout = item.get(SAM3INGRESSPACKETSOUT).trim();
            String sam3egresspacketsin = item.get(SAM3EGRESSPACKETSIN).trim();
            String sam3egresspacketsout = item.get(SAM3EGRESSPACKETSOUT).trim();
            // String sam6queueid = item.get(SAM6QUEUEID).trim();
            String sam6ingresspacketsin = item.get(SAM6INGRESSPACKETSIN).trim();
            String sam6ingresspacketsout = item.get(SAM6INGRESSPACKETSOUT).trim();
            String sam6egresspacketsin = item.get(SAM6EGRESSPACKETSIN).trim();
            String sam6egresspacketsout = item.get(SAM6EGRESSPACKETSOUT).trim();

            // Usage
            double usageSam1ingresspacketsin = 0;
            double usageSam1ingresspacketsout = 0;
            double usageSam1egresspacketsin = 0;
            double usageSam1egresspacketsout = 0;
            double usageSam3ingresspacketsin = 0;
            double usageSam3ingresspacketsout = 0;
            double usageSam3egresspacketsin = 0;
            double usageSam3egresspacketsout = 0;
            double usageSam6ingresspacketsin = 0;
            double usageSam6ingresspacketsout = 0;
            double usageSam6egresspacketsin = 0;
            double usageSam6egresspacketsout = 0;
            long durationEpocTime = 0;

            Subscription subscription = subscriptionService.findByCode(samcircuitid);
            if (subscription == null) {
                throw new BusinessException("Subscription " + samcircuitid + " not found!");
            }
            Map<String, String> cfMubCounters = new HashMap<>();
            if(subscription.getCfValue(CF_MUB_COUNTERS) != null) {
                cfMubCounters = (Map<String, String>) subscription.getCfValue(CF_MUB_COUNTERS);
            }
            
            String counter1 = cfMubCounters.get(samportid + "|" + SAM1INGRESSPACKETSIN);
            if (counter1 != null && !counter1.isEmpty()) {

                // Configuration max counter
                ParamBean paramBean = paramBeanFactory.getInstance();
                double maxCounterSam1ingresspacketsin = Double.valueOf(paramBean.getProperty("mub.max.Counter.sam1ingresspacketsin", "18000000000000000000"));
                double maxCounterSam1ingresspacketsout = Double.valueOf(paramBean.getProperty("mub.max.Counter.sam1ingresspacketsout", "18000000000000000000"));
                double maxCounterSam1egresspacketsin = Double.valueOf(paramBean.getProperty("mub.max.Counter.sam1egresspacketsin", "18000000000000000000"));
                double maxCounterSam1egresspacketsout = Double.valueOf(paramBean.getProperty("mub.max.Counter.sam1egresspacketsout", "18000000000000000000"));
                double maxCounterSam3ingresspacketsin = Double.valueOf(paramBean.getProperty("mub.max.Counter.sam3ingresspacketsin", "18000000000000000000"));
                double maxCounterSam3ingresspacketsout = Double.valueOf(paramBean.getProperty("mub.max.Counter.sam3ingresspacketsout", "18000000000000000000"));
                double maxCounterSam3egresspacketsin = Double.valueOf(paramBean.getProperty("mub.max.Counter.sam3egresspacketsin", "18000000000000000000"));
                double maxCounterSam3egresspacketsout = Double.valueOf(paramBean.getProperty("mub.max.Counter.sam3egresspacketsout", "18000000000000000000"));
                double maxCounterSam6ingresspacketsin = Double.valueOf(paramBean.getProperty("mub.max.Counter.sam6ingresspacketsin", "18000000000000000000"));
                double maxCounterSam6ingresspacketsout = Double.valueOf(paramBean.getProperty("mub.max.Counter.sam6ingresspacketsout", "18000000000000000000"));
                double maxCounterSam6egresspacketsin = Double.valueOf(paramBean.getProperty("mub.max.Counter.sam6egresspacketsin", "18000000000000000000"));
                double maxCounterSam6egresspacketsout = Double.valueOf(paramBean.getProperty("mub.max.Counter.sam6egresspacketsout", "18000000000000000000"));

                durationEpocTime = Long.valueOf(samtimeepoc) - Long.valueOf(counter1.split(Pattern.quote("|"))[0]);
                if (durationEpocTime <= 0) {
                    throw new BusinessException("Epoch time = " + samtimeepoc + " should be greater than the value stored in the counter!");
                }

                String counter2 = cfMubCounters.get(samportid + "|" + SAM1INGRESSPACKETSOUT);
                String counter3 = cfMubCounters.get(samportid + "|" + SAM1EGRESSPACKETSIN);
                String counter4 = cfMubCounters.get(samportid + "|" + SAM1EGRESSPACKETSOUT);
                String counter5 = cfMubCounters.get(samportid + "|" + SAM3INGRESSPACKETSIN);
                String counter6 = cfMubCounters.get(samportid + "|" + SAM3INGRESSPACKETSOUT);
                String counter7 = cfMubCounters.get(samportid + "|" + SAM3EGRESSPACKETSIN);
                String counter8 = cfMubCounters.get(samportid + "|" + SAM3EGRESSPACKETSOUT);
                String counter9 = cfMubCounters.get(samportid + "|" + SAM6INGRESSPACKETSIN);
                String counter10 = cfMubCounters.get(samportid + "|" + SAM6INGRESSPACKETSOUT);
                String counter11 = cfMubCounters.get(samportid + "|" + SAM6EGRESSPACKETSIN);
                String counter12 = cfMubCounters.get(samportid + "|" + SAM6EGRESSPACKETSOUT);

                // Usage counter 1 : sam1ingresspacketsin
                usageSam1ingresspacketsin = Double.valueOf(sam1ingresspacketsin) - Double.valueOf(counter1.split(Pattern.quote("|"))[1]);
                if (usageSam1ingresspacketsin < 0 || usageSam1ingresspacketsin > maxCounterSam1ingresspacketsin) {
                    usageSam1ingresspacketsin = 0;
                }
                // Usage counter 2 : sam1ingresspacketsout
                usageSam1ingresspacketsout = Double.valueOf(sam1ingresspacketsout) - Double.valueOf(counter2.split(Pattern.quote("|"))[1]);
                if (usageSam1ingresspacketsout < 0 || usageSam1ingresspacketsout > maxCounterSam1ingresspacketsout) {
                    usageSam1ingresspacketsout = 0;
                }
                // Usage counter 3 : sam1egresspacketsin
                usageSam1egresspacketsin = Double.valueOf(sam1egresspacketsin) - Double.valueOf(counter3.split(Pattern.quote("|"))[1]);
                if (usageSam1egresspacketsin < 0 || usageSam1egresspacketsin > maxCounterSam1egresspacketsin) {
                    usageSam1egresspacketsin = 0;
                }
                // Usage counter 4 : sam1egresspacketsout
                usageSam1egresspacketsout = Double.valueOf(sam1egresspacketsout) - Double.valueOf(counter4.split(Pattern.quote("|"))[1]);
                if (usageSam1egresspacketsout < 0 || usageSam1egresspacketsout > maxCounterSam1egresspacketsout) {
                    usageSam1egresspacketsout = 0;
                }
                // Usage counter 5 : sam3ingresspacketsin
                usageSam3ingresspacketsin = Double.valueOf(sam3ingresspacketsin) - Double.valueOf(counter5.split(Pattern.quote("|"))[1]);
                if (usageSam3ingresspacketsin < 0 || usageSam3ingresspacketsin > maxCounterSam3ingresspacketsin) {
                    usageSam3ingresspacketsin = 0;
                }
                // Usage counter 6 : sam3ingresspacketsout
                usageSam3ingresspacketsout = Double.valueOf(sam3ingresspacketsout) - Double.valueOf(counter6.split(Pattern.quote("|"))[1]);
                if (usageSam3ingresspacketsout < 0 || usageSam3ingresspacketsout > maxCounterSam3ingresspacketsout) {
                    usageSam3ingresspacketsout = 0;
                }
                // Usage counter 7 : sam3egresspacketsin
                usageSam3egresspacketsin = Double.valueOf(sam3egresspacketsin) - Double.valueOf(counter7.split(Pattern.quote("|"))[1]);
                if (usageSam3egresspacketsin < 0 || usageSam3egresspacketsin > maxCounterSam3egresspacketsin) {
                    usageSam3egresspacketsin = 0;
                }
                // Usage counter 8 : sam3egresspacketsout
                usageSam3egresspacketsout = Double.valueOf(sam3egresspacketsout) - Double.valueOf(counter8.split(Pattern.quote("|"))[1]);
                if (usageSam3egresspacketsout < 0 || usageSam3egresspacketsout > maxCounterSam3egresspacketsout) {
                    usageSam3egresspacketsout = 0;
                }
                // Usage counter 9 : sam6ingresspacketsin
                usageSam6ingresspacketsin = Double.valueOf(sam6ingresspacketsin) - Double.valueOf(counter9.split(Pattern.quote("|"))[1]);
                if (usageSam6ingresspacketsin < 0 || usageSam6ingresspacketsin > maxCounterSam6ingresspacketsin) {
                    usageSam6ingresspacketsin = 0;
                }
                // Usage counter 10 : sam6ingresspacketsout
                usageSam6ingresspacketsout = Double.valueOf(sam6ingresspacketsout) - Double.valueOf(counter10.split(Pattern.quote("|"))[1]);
                if (usageSam6ingresspacketsout < 0 || usageSam6ingresspacketsout > maxCounterSam6ingresspacketsout) {
                    usageSam6ingresspacketsout = 0;
                }
                // Usage counter 11 : sam6egresspacketsin
                usageSam6egresspacketsin = Double.valueOf(sam6egresspacketsin) - Double.valueOf(counter11.split(Pattern.quote("|"))[1]);
                if (usageSam6egresspacketsin < 0 || usageSam6egresspacketsin > maxCounterSam6egresspacketsin) {
                    usageSam6egresspacketsin = 0;
                }
                // Usage counter 12 : sam6egresspacketsout
                usageSam6egresspacketsout = Double.valueOf(sam6egresspacketsout) - Double.valueOf(counter12.split(Pattern.quote("|"))[1]);
                if (usageSam6egresspacketsout < 0 || usageSam6egresspacketsout > maxCounterSam6egresspacketsout) {
                    usageSam6egresspacketsout = 0;
                }
                generateCDR(samtimeyyyymmddhhmiss, samtimeyyyymmddhhmissround, samtimeepoc, samtimeepocround, usageSam6ingresspacketsin, usageSam6ingresspacketsout,
                    durationEpocTime, usageSam6egresspacketsout, usageSam6egresspacketsin, usageSam3ingresspacketsin, usageSam3ingresspacketsout, usageSam3egresspacketsin,
                    usageSam3egresspacketsout, usageSam1egresspacketsin, usageSam1ingresspacketsout, usageSam1ingresspacketsin, originalFileName, samportid, samcircuitid,
                    usageSam1egresspacketsout, samcustomername, cdrFileName, fmt);
            }

            // Save new mub Counters
            cfMubCounters.put(samportid + "|" + SAM1INGRESSPACKETSIN,
                samtimeepoc + "|" + sam1ingresspacketsin + "|" + fmt.format(usageSam1ingresspacketsin) + "|" + durationEpocTime);
            cfMubCounters.put(samportid + "|" + SAM1INGRESSPACKETSOUT,
                samtimeepoc + "|" + sam1ingresspacketsout + "|" + fmt.format(usageSam1ingresspacketsout) + "|" + durationEpocTime);
            cfMubCounters.put(samportid + "|" + SAM1EGRESSPACKETSIN, samtimeepoc + "|" + sam1egresspacketsin + "|" + fmt.format(usageSam1egresspacketsin) + "|" + durationEpocTime);
            cfMubCounters.put(samportid + "|" + SAM1EGRESSPACKETSOUT,
                samtimeepoc + "|" + sam1egresspacketsout + "|" + fmt.format(usageSam1egresspacketsout) + "|" + durationEpocTime);
            cfMubCounters.put(samportid + "|" + SAM3INGRESSPACKETSIN,
                samtimeepoc + "|" + sam3ingresspacketsin + "|" + fmt.format(usageSam3ingresspacketsin) + "|" + durationEpocTime);
            cfMubCounters.put(samportid + "|" + SAM3INGRESSPACKETSOUT,
                samtimeepoc + "|" + sam3ingresspacketsout + "|" + fmt.format(usageSam3ingresspacketsout) + "|" + durationEpocTime);
            cfMubCounters.put(samportid + "|" + SAM3EGRESSPACKETSIN, samtimeepoc + "|" + sam3egresspacketsin + "|" + fmt.format(usageSam3egresspacketsin) + "|" + durationEpocTime);
            cfMubCounters.put(samportid + "|" + SAM3EGRESSPACKETSOUT,
                samtimeepoc + "|" + sam3egresspacketsout + "|" + fmt.format(usageSam3egresspacketsout) + "|" + durationEpocTime);
            cfMubCounters.put(samportid + "|" + SAM6INGRESSPACKETSIN,
                samtimeepoc + "|" + sam6ingresspacketsin + "|" + fmt.format(usageSam6ingresspacketsin) + "|" + durationEpocTime);
            cfMubCounters.put(samportid + "|" + SAM6INGRESSPACKETSOUT,
                samtimeepoc + "|" + sam6ingresspacketsout + "|" + fmt.format(usageSam6ingresspacketsout) + "|" + durationEpocTime);
            cfMubCounters.put(samportid + "|" + SAM6EGRESSPACKETSIN, samtimeepoc + "|" + sam6egresspacketsin + "|" + fmt.format(usageSam6egresspacketsin) + "|" + durationEpocTime);
            cfMubCounters.put(samportid + "|" + SAM6EGRESSPACKETSOUT,
                samtimeepoc + "|" + sam6egresspacketsout + "|" + fmt.format(usageSam6egresspacketsout) + "|" + durationEpocTime);

            subscription.setCfValue(CF_MUB_COUNTERS, cfMubCounters);
            subscriptionService.update(subscription);
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new BusinessException(e.getMessage());
        }

    }

    private void generateCDR(String samtimeyyyymmddhhmiss, String samtimeyyyymmddhhmissround, String samtimeepoc, String samtimeepocround, Double usageSam6ingresspacketsin,
            Double usageSam6ingresspacketsout, long durationEpocTime, Double usageSam6egresspacketsout, Double usageSam6egresspacketsin, Double usageSam3ingresspacketsin,
            Double usageSam3ingresspacketsout, Double usageSam3egresspacketsin, Double usageSam3egresspacketsout, Double usageSam1egresspacketsin,
            Double usageSam1ingresspacketsout, Double usageSam1ingresspacketsin, String originalFileName, String samportid, String samcircuitid, Double usageSam1egresspacketsout,
            String samcustomername, String cdrFileName, NumberFormat fmt) throws IOException, ParseException {
        String separator = ";";
        String time = samtimeyyyymmddhhmiss.substring(8, 10) + ":" + samtimeyyyymmddhhmiss.substring(10, 12);
        String roundTime = samtimeyyyymmddhhmissround.substring(8, 10) + ":" + samtimeyyyymmddhhmissround.substring(10, 12);
        String contentToAppend = samtimeepoc + separator + samtimeepocround + separator
                + DateUtils.formatDateWithPattern(getDate(samtimeyyyymmddhhmiss, "yyyyMMddHHmmss"), "dd.MM.yyyy") + separator + time + separator + roundTime + separator
                + durationEpocTime + separator + fmt.format(usageSam6ingresspacketsin) + separator + fmt.format(usageSam6ingresspacketsout) + separator
                + fmt.format(usageSam6egresspacketsin) + separator + fmt.format(usageSam6egresspacketsout) + separator + fmt.format(usageSam3ingresspacketsin) + separator
                + fmt.format(usageSam3ingresspacketsout) + separator + fmt.format(usageSam3egresspacketsin) + separator + fmt.format(usageSam3egresspacketsout) + separator
                + fmt.format(usageSam1ingresspacketsin) + separator + fmt.format(usageSam1ingresspacketsout) + separator + fmt.format(usageSam1egresspacketsin) + separator
                + fmt.format(usageSam1egresspacketsout) + separator + samcustomername + separator + samcircuitid + separator + samportid + separator + originalFileName + "\r\n";

        
//        try (BufferedWriter bw = new BufferedWriter(new FileWriter(cdrFileName, true))) {
//            bw.write(contentToAppend);
//        } catch (Exception e) {
//            log.error(e.getMessage(), e);
//            throw new BusinessException(e);
//        }
        
        FileWriter fw = new FileWriter(cdrFileName, true);
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(contentToAppend);
        bw.close();
    }

    /**
     * Get formatted date from XML Order
     *
     * @param date date attribute
     * @return Formatted date
     * @throws ParseException Parsing date exception
     */
    private Date getDate(String date, String pattern) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat(pattern);
        return format.parse(date);
    }
}
