package com.eir.script;

import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ExistsRelatedEntityException;
import org.meveo.admin.util.ResourceBundle;
import org.meveo.jpa.EntityManagerWrapper;
import org.meveo.jpa.MeveoJpa;
import org.meveo.model.CustomTableEvent;
import org.meveo.model.customEntities.CustomEntityInstance;
import org.meveo.service.script.Script;

/**
 * Delete ServiceType Script
 * 
 * @author hznibar
 */
@Stateless
public class DeleteServiceTypeScript extends Script {

    private static final long serialVersionUID = -8399269835842326354L;

    @Inject
    @MeveoJpa
    private EntityManagerWrapper emWrapper;

    @Inject
    private ResourceBundle resourceMessages;

    @Override
    public void execute(Map<String, Object> initContext) throws BusinessException {

        String serviceType = null;
        Object entity = initContext.get(Script.CONTEXT_ENTITY);
        if (entity instanceof CustomEntityInstance) {
            serviceType = ((CustomEntityInstance) entity).getCode();
        } else if (entity instanceof CustomTableEvent) {
            serviceType = (String) ((CustomTableEvent) entity).getValues().get("code");
        }

        String query = "SELECT count(*) FROM account_entity where account_type = 'ACCT_BA' and   (cf_values\\:\\:JSON->'SERVICE_TYPE'->0 ->>'string') = '"+serviceType + "'";
        Number totalRefRecords = (Number) emWrapper.getEntityManager().createNativeQuery(query).getSingleResult();

        if (totalRefRecords.intValue() > 0) {
            throw new ExistsRelatedEntityException(resourceMessages.getString("error.delete.entityUsedWDetails", "Service type", "billing account"));
        }
    }
}