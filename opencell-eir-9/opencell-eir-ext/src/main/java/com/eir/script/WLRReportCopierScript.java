/*
 * (C) Copyright 2015-2020 Opencell SAS (https://opencellsoft.com/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the GNU Affero General
 * Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
 * any later version.
 *
 * THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW. EXCEPT WHEN
 * OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER PARTIES PROVIDE THE PROGRAM "AS
 * IS" WITHOUT WARRANTY OF ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO
 * THE QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE DEFECTIVE,
 * YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
 *
 * For more information on the GNU Affero General Public License, please consult
 * <https://www.gnu.org/licenses/agpl-3.0.en.html>.
 */

package com.eir.script;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.FileUtils;
import org.meveo.commons.utils.ParamBeanFactory;
import org.meveo.service.script.Script;

/**
 * A script for copying WLR Report script to archive folder
 * 
 * @author Mohammed Amine Tazi
 */
@Stateless
public class WLRReportCopierScript extends Script {

    @Inject
    private ParamBeanFactory paramBeanFactory;
    
    /**
     * 
     */
    private static final long serialVersionUID = -4093931145700531123L;

    @Override
    public void execute(Map<String, Object> methodContext) throws BusinessException {
        String prefix = "WLR_EXTRACT_" + new SimpleDateFormat("yyyyMMdd").format(new Date());
        String ext = "csv";
        String inputDir = paramBeanFactory.getChrootDir() + "/reports/wlr_reports";
        
        File dir = new File(inputDir);        
        if (!dir.exists()) {
            throw new BusinessException("Directory not found : " + inputDir);
        }

        File[] files = FileUtils.listFiles(inputDir, Arrays.asList(new String[] {ext}), prefix);
        for (File file : files) {
            try {
                log.debug("Archiving file : " + file.getAbsolutePath());
                FileUtils.copy(file.getAbsolutePath(),file.getAbsolutePath().replace("wlr_reports/","wlr_reports/archive/"));
            } catch(IOException ex) {
                throw new BusinessException(ex);
            }
        }
    }
}
