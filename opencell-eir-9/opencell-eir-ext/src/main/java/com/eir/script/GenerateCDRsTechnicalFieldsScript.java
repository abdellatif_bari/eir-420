package com.eir.script;

import com.eir.commons.enums.TransformationRulesEnum;
import com.eir.commons.enums.customtables.CustomTableEnum;
import com.eir.commons.utils.TransformationRulesHandler;
import com.eir.service.commons.ImportService;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.transformer.AliasToEntityOrderedMapResultTransformer;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.script.Script;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Eir Generate the CDRs technical fields script
 *
 * @author Abdellatif BARI
 */
@Stateless
public class GenerateCDRsTechnicalFieldsScript extends Script {

    @Inject
    private CustomTableService customTableService;

    @Inject
    private ImportService importService;

    @Override
    public void execute(Map<String, Object> parameters) throws BusinessException {

        String tableName = (String) parameters.get("tableName");
        if (StringUtils.isBlank(tableName)) {
            return;
        }

        boolean hash_code = Boolean.valueOf((String) parameters.get("hash_code"));
        if (!hash_code) {
            throw new BusinessException("you should specify at least one field to be generated");
        }

        List<String> updatedFields = new ArrayList<>(Arrays.asList("hash_code"));

        if (tableName.equalsIgnoreCase(CustomTableEnum.EIR_U_TARIFF_STAGE.getCode()) || tableName.equalsIgnoreCase(CustomTableEnum.EIR_U_TARIFF.getCode())) {

            List<Map<String, Object>> cdrs = null;
            if (tableName.equalsIgnoreCase(CustomTableEnum.EIR_U_TARIFF_STAGE.getCode())) {
                Map<String, Object> filters = new HashMap<>();
                //filters.put("SQL", "(hash_code is null or trim(hash_code) = '')");
                cdrs = customTableService.list(tableName.toUpperCase(), new PaginationConfiguration(filters));
            } else if (tableName.equalsIgnoreCase(CustomTableEnum.EIR_U_TARIFF.getCode())) {
                String query = "select tariff.id, tariff.min_charge, tariff.connection_charge, tariff.usage_charge, tariff.unit, tariff.duration_rounding, tariff.min_duration, " +
                        "tariff_plan.code as rate_plan, tariff_class.code as traffic_class, time_period.code as time_period, accounting_code.code as accounting_code, " +
                        "sub_cat.code as invoice_subcat, tax_class.code as tax_class " +
                        "from EIR_U_TARIFF tariff " +
                        "inner join EIR_U_TARIFF_PLAN tariff_plan on tariff_plan.id = tariff.rate_plan_id " +
                        "inner join EIR_U_TRAFFIC_CLASS tariff_class on tariff_class.id = tariff.traffic_class_id " +
                        "inner join EIR_U_TIME_PERIOD time_period on time_period.id = tariff.time_period_id " +
                        "inner join billing_accounting_code accounting_code on accounting_code.id = tariff.accounting_code_id " +
                        "inner join billing_invoice_sub_cat sub_cat on sub_cat.id = tariff.invoice_subcat_id " +
                        "inner join billing_tax_class tax_class on tax_class.id = tariff.tax_class ";
                //"where tariff.hash_code is null or trim(tariff.hash_code) = ''";

                Session session = importService.getEntityManager().unwrap(Session.class);
                SQLQuery q = session.createSQLQuery(query);
                q.setResultTransformer(AliasToEntityOrderedMapResultTransformer.INSTANCE);
                cdrs = q.list();
            }

            if (cdrs != null && !cdrs.isEmpty()) {
                TransformationRulesHandler transformationRulesHandler = importService.getTransformationRules(TransformationRulesEnum.CDR.name());
                if (transformationRulesHandler == null) {
                    return;
                }
                for (Map<String, Object> record : cdrs) {
                    importService.setHashCode(record, transformationRulesHandler);
                }

                Map<String, Object> tableFields = null;
                if (tableName.equalsIgnoreCase(CustomTableEnum.EIR_U_TARIFF_STAGE.getCode())) {
                    tableFields = transformationRulesHandler.getStageTableFields();
                } else if (tableName.equalsIgnoreCase(CustomTableEnum.EIR_U_TARIFF.getCode())) {
                    tableFields = transformationRulesHandler.getValidTableFields();
                }
                if (tableFields != null) {
                    importService.updateRecord(tableName.toUpperCase(), tableFields, updatedFields, cdrs);
                }
            }
        }
    }
}