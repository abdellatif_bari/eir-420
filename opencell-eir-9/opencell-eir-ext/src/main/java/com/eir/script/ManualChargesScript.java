package com.eir.script;

import com.eir.commons.enums.customfields.ChargeInstanceCFsEnum;
import com.eir.service.commons.UtilService;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.RatingException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.api.commons.Utils;
import org.meveo.api.dto.billing.ChargeDto;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.DatePeriod;
import org.meveo.model.billing.ChargeApplicationModeEnum;
import org.meveo.model.billing.OneShotChargeInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.catalog.OneShotChargeTemplate;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.shared.DateUtils;
import org.meveo.model.tax.TaxClass;
import org.meveo.service.billing.impl.OneShotChargeInstanceService;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.catalog.impl.OneShotChargeTemplateService;
import org.meveo.service.catalog.impl.ServiceTemplateService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.script.Script;
import org.meveo.service.tax.TaxClassService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * The Manual charges script.
 */
@Stateless
public class ManualChargesScript extends Script {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1L;
    /**
     * The Subscription service.
     */
    @Inject
    private SubscriptionService subscriptionService;
    /**
     * The one shot charge instance service.
     */
    @Inject
    private OneShotChargeInstanceService oneShotChargeInstanceService;
    /**
     * The charge template service.
     */
    @Inject
    private OneShotChargeTemplateService oneShotChargeTemplateService;

    /**
     * The offer template service.
     */
    @Inject
    private ServiceTemplateService serviceTemplateService;
    @Inject
    private CustomTableService customTableService;
    @Inject
    private UtilService utilService;
    @Inject
    private TaxClassService taxClassService;

    /**
     * The Constant CHARGES_RECORD.
     */
    private static final String CHARGES_RECORD = "record";
    private static final String MISC_CHARGE_TEMPLATE_CODE = "MISC_CHARGE";

    /**
     * script main execute method.
     *
     * @param methodContext method context params
     * @throws BusinessException business exception
     */
    @Override
    public void execute(Map<String, Object> methodContext) throws BusinessException {

        ChargeDto chargeDto = (ChargeDto) methodContext.get(CHARGES_RECORD);
        validateRecord(chargeDto);
        String subscriptionCode = getServiceIdWithPrefix(chargeDto.getSubscriberPrefix(), chargeDto.getServiceId());
        Subscription subscription = subscriptionService.findByCode(subscriptionCode);
        if (subscription == null) {
            throw new BusinessException("Subscription " + subscriptionCode + " not found!");
        }

        OneShotChargeTemplate chargeTemplate = oneShotChargeTemplateService.findByCode(MISC_CHARGE_TEMPLATE_CODE);
        if (chargeTemplate == null) {
            throw new BusinessException("The one-shot charge " + MISC_CHARGE_TEMPLATE_CODE + " is not found");
        }

        OneShotChargeInstance chargeInstance = new OneShotChargeInstance();
        Map<String, String> cfValue = new HashMap<>();
        Map<String, Object> serviceParameters = new HashMap<>();
        serviceParameters.put("quantity", chargeDto.getQuantity());
        cfValue.put("manual_charge", utilService.getCFValue(chargeInstance, "SERVICE_PARAMETERS", serviceParameters));

        String filename = (String) methodContext.get("origin_filename");

        chargeInstance.setCfValue(ChargeInstanceCFsEnum.SERVICE_PARAMETERS.name(), cfValue);
        chargeInstance.setCfValue(ChargeInstanceCFsEnum.SERVICE_CODE.name(), chargeDto.getBillCode());
        chargeInstance.setCode(chargeDto.getBillCode() + "_NRC");
        if (filename != null) {
            chargeInstance.setCfValue(ChargeInstanceCFsEnum.FILENAME.name(), filename);
        }
        if (StringUtils.isNotBlank(chargeDto.getNotes())) {
            chargeInstance.setCfValue(ChargeInstanceCFsEnum.NOTES.name(), chargeDto.getNotes());
        }
        try {
            oneShotChargeInstanceService.oneShotChargeApplication(subscription, null, chargeTemplate, null, chargeDto.getTransDate(), new BigDecimal(chargeDto.getAmount()), null, new BigDecimal(chargeDto.getQuantity()),
                chargeDto.getBillCode(), chargeDto.getTaxClass(), null, chargeDto.getBillText(), null, chargeInstance.getCfValues(), true, ChargeApplicationModeEnum.SUBSCRIPTION);

        } catch (RatingException exception) {
            log.error("No price found for a misc charge");
            throw new BusinessException(exception.getMessage());
        }

        String cfVal = "{\"from\": \"" + DateUtils.formatDateWithPattern(new Date(), DateUtils.DATE_TIME_PATTERN) + "\", \"priority\": 1, \"mapString\":{\"MANUAL_CHARGE\":\""
                + DateUtils.formatDateWithPattern(new Date(), Utils.DATE_TIME_PATTERN) + "|" + chargeDto.getSubscriberPrefix() + "|" + chargeDto.getServiceId() + "|" + chargeDto.getBillCode() + "|"
                + chargeDto.getTransDate() + "|" + chargeDto.getTransDateTo() + "|" + chargeDto.getBillText() + "|" + chargeDto.getTaxClass() + "|" + chargeDto.getAmount() + "|" + chargeDto.getChargeReference() + "|"
                + chargeDto.getQuantity() + "\"}}";

        String sql = "update billing_subscription set cf_values = (case when cf_values\\:\\:jsonb->'MANUAL_CHARGE' is null then cf_values\\:\\:jsonb ||  '{\"MANUAL_CHARGE\":[" + cfVal
                + "]}' else  jsonb_set(cf_values\\:\\:jsonb, '{MANUAL_CHARGE, 999999}', '" + cfVal + "', True) end) where id=:id";
        
        subscriptionService.getEntityManager().createNativeQuery(sql).setParameter("id", subscription.getId()).executeUpdate();

        log.debug("Manual charge applied on subscription {}", subscription.getId());

    }

    private void validateRecord(ChargeDto chargeDto) {
        validateField("SERVICE_ID", chargeDto.getServiceId(), String.class, true);
        validateField("BILL_CODE", chargeDto.getBillCode(), String.class, true);
        validateField("TRANSDATE", chargeDto.getTransDate(), Date.class, true);
        validateField("BILLTEXT", chargeDto.getBillText(), String.class, true);
        validateField("TAX_CLASS", chargeDto.getTaxClass(), String.class, true);
        validateField("AMOUNT", chargeDto.getAmount(), Double.class, true);
        validateField("QUANTITY", chargeDto.getQuantity(), Integer.class, true);
    }

    private void validateField(String fieldName, Object fieldValue, Class fieldType, Boolean mandatory) throws ValidationException {

        if (!mandatory && StringUtils.isBlank(fieldValue)) {
            return;
        }

        // Check required parameter
        if (mandatory && (StringUtils.isBlank(fieldValue) || fieldValue.equals("null"))) {
            throw new ValidationException("The following parameter is required : " + fieldName);
        }

        if (!StringUtils.isBlank(fieldValue) && fieldValue instanceof String) {

            // Check value is valid depending on type
            if ((fieldType.equals(Integer.class) && !Utils.isInteger((String) fieldValue)) || (fieldType.equals(Double.class) && !Utils.isDouble((String) fieldValue))
                    || (fieldType.equals(Date.class) && DateUtils.parseDateWithPattern((String) fieldValue, "yyyyMMdd") == null)) {
                throw new ValidationException("The following parameter contains invalid value : " + fieldName);
            }
        }
    }

    /**
     * Gets the service id with prefix.
     *
     * @param subscriberPrefix the subscription prefix
     * @param serviceId the service id
     * @return the service id with prefix
     */
    private String getServiceIdWithPrefix(String subscriberPrefix, String serviceId) {
        if (subscriberPrefix == null || subscriberPrefix.isEmpty() || subscriberPrefix.toUpperCase().contains("NULL")) {
            return serviceId;
        } else {
            return subscriberPrefix.replace("_", "") + "_" + serviceId;
        }
    }
}
