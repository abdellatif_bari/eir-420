package com.eir.script;

import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.model.crm.Provider;
import org.meveo.service.crm.impl.ProviderService;
import org.meveo.service.script.Script;

@Stateless
public class CheckMigrationEvent extends Script {

    @Inject
    private ProviderService providerService;

    /**
     * Entry Point for script
     */
    @Override
    public void execute(Map<String, Object> initContext) throws BusinessException {

        log.info("******Starting of script CheckMigrationEvent******");

        boolean migrationEventFound = false;

        try {

            String code = (String) initContext.get("CODE");

            Provider provider = providerService.getProvider();

            Map<String, String> map = (Map<String, String>) providerService.getProvider().getCfValuesNullSafe().getValue("MIGRATION_MATRIX");

            for (String value : map.values()) {

                if (value.toLowerCase().contains(code.toLowerCase())) {

                    migrationEventFound = true;
                    break;
                }
            }

            if (migrationEventFound == true) {
                throw new BusinessException(code + ": This migration event already exists on the migration matrix.");
            }

        } catch (Exception e) {
            throw new BusinessException(e.getMessage());

        } finally {
            log.debug("******Ending of script CheckMigrationEvent******");
        }
    }

}
