package com.eir.script;

import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.keycloak.representations.idm.UserRepresentation;
import org.meveo.admin.exception.ValidationException;
import org.meveo.commons.utils.CsvBuilder;
import org.meveo.model.admin.User;
import org.meveo.model.security.Permission;
import org.meveo.model.security.Role;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.admin.impl.UserService;
import org.meveo.service.script.Script;
import org.meveo.service.script.finance.ReportExtractScript;

import com.eir.service.keycloak.KeycloakAdminClientService;

/**
 * Generate CSV listing users information from Keycloak.
 *
 * @author Mohammed Amine TAZI
 */
@Stateless
public class UsersReportScript extends Script {

    private static final long serialVersionUID = 1L;

    public static final String SEPARATOR = "|";
    
    @Inject
    private KeycloakAdminClientService keycloakAdminClientService;

    @Inject
    private UserService userService;

    @Override
    public void execute(Map<String, Object> initContext) throws ValidationException {
        log.debug("#####################Starting of script UsersReportScript");
        try {
            String dirOutput = String.valueOf(initContext.get(ReportExtractScript.DIR));
            String filename = String.valueOf(initContext.get(ReportExtractScript.FILENAME));
            String[] header = {"User Name","E-mail","Full Name","Staff Number","User Group","Client Role","User Role","Permissions","Last Log On date","Status","Number of days"};
            CsvBuilder csvBuilder = new CsvBuilder("|", false);
            csvBuilder.appendValues(header);
            csvBuilder.startNewLine();

            Map<String, User> ocUsersMap = userService.list().stream().collect(Collectors.toMap(User::getUserName, Function.identity()));
            ocUsersMap.entrySet().stream().collect(Collectors.toMap(entry -> entry.getKey().toUpperCase(), entry -> entry.getValue()));

            Map<String,String> groups = keycloakAdminClientService.getGroups();
            Map<String,String> clientRoles = keycloakAdminClientService.getClientRoles();

            List<UserRepresentation> kcUsers = keycloakAdminClientService.listUsers();
            String userName = null;
            User ocUser = null;
            StringBuilder permissionList = new StringBuilder("");
            for (UserRepresentation user : kcUsers) {
                userName = user.getUsername() == null ? "" : user.getUsername().toUpperCase();
                ocUser = ocUsersMap.get(userName);
                if(ocUser!= null && ocUser.getRoles() != null) {
                    for(Role ocRole : ocUser.getRoles()) {
                        for(Permission permission : ocRole.getPermissions()) {
                            permissionList.append(permission.getName()).append(",");
                        }
                    }
                }
                csvBuilder.appendValue(StringUtils.defaultString(user.getUsername()));
                csvBuilder.appendValue(StringUtils.defaultString(user.getEmail()));
                csvBuilder.appendValue(StringUtils.defaultString(user.getFirstName()) + " " + StringUtils.defaultString(user.getLastName()));
                csvBuilder.appendValue(user.getAttributes() != null && user.getAttributes().get("Staff ID") != null ? StringUtils.defaultString(user.getAttributes().get("Staff ID").get(0)) : "");
                csvBuilder.appendValue(groups.get(user.getId()) != null ? StringUtils.defaultString(groups.get(user.getId())) : "");
                csvBuilder.appendValue(clientRoles.get(user.getId()) != null ? StringUtils.defaultString(clientRoles.get(user.getId())) : "");
                csvBuilder.appendValue(ocUser != null ? StringUtils.defaultString(ocUser.getRolesLabel()) : "");
                csvBuilder.appendValue(ocUser != null? StringUtils.chop(permissionList.toString()) : "");
                csvBuilder.appendValue(ocUser != null ? DateUtils.formatAsDate(ocUser.getLastLoginDate()) : "");
                csvBuilder.appendValue(user.isEnabled() == true ? "Yes" : "No");
                csvBuilder.appendValue(ocUser != null && ocUser.getLastLoginDate() != null ? ""+ Double.valueOf(DateUtils.daysBetween(ocUser.getLastLoginDate(), new Date())).intValue() : "");
                csvBuilder.startNewLine();
            }
            csvBuilder.toFile(dirOutput + File.separator + filename);           
        } catch (Exception e) {
            throw new ValidationException(e.getMessage());
        }
    }
       
}