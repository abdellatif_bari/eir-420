package com.eir.script;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.StringUtils;
import org.meveo.jpa.JpaAmpNewTx;
import org.meveo.model.bi.FlatFile;
import org.meveo.service.bi.impl.FlatFileService;
import org.meveo.service.script.Script;

import com.eir.commons.enums.TransformationRulesEnum;
import com.eir.commons.utils.TransformationRulesHandler;
import com.eir.service.commons.CachedData;
import com.eir.service.commons.ImportService;

/**
 * Eir Import staging records script
 *
 * @author Abdellatif BARI
 */
@Stateless
public class ImportStagingRecordsScript extends Script {

    @Inject
    private FlatFileService flatFileService;
    @Inject
    private ImportService importService;
    
    @Inject
    private CachedData cachedData;

    private static Map<String, Map<String, Object>> scriptInstancesParameters = new HashMap<>();


    @Override
    public void execute(Map<String, Object> parameters) throws BusinessException {
        try {
            initContext(parameters);
            importService.validateRecord((String) parameters.get("importType"), (Map<String, Object>) parameters.get("stagingRecord"));
        } catch (Exception e) {
            if (scriptInstancesParameters.get(parameters.get("origin_filename")) != null) {
                scriptInstancesParameters.get(parameters.get("origin_filename")).put("exception", e.getMessage());
            }
            log.error("error on process staging records file {} ", e.getMessage(), e);
            if (e instanceof BusinessException) {
                throw e;
            } else {
                // wrap the exception in a business exception and throwing it
                throw new BusinessException(e);
            }
        }
    }

    /**
     * Init context
     *
     * @param parameters parameters
     * @throws BusinessException the business exception
     */
    private void initContext(Map<String, Object> parameters) throws BusinessException {
        Map<String, Object> stagingRecord = (Map<String, Object>) parameters.get("record");
        if (stagingRecord == null || stagingRecord.isEmpty()) {
            throw new BusinessException("Record is missing or empty");
        }

        Map<String, Object> scriptInstanceParameters = getScriptInstanceParameters(parameters);
        stagingRecord.put("file_id", scriptInstanceParameters.get("flatFileId"));
        parameters.put("stagingRecord", stagingRecord);
        parameters.put("importType", scriptInstanceParameters.get("importType"));
    }

    /**
     * Get script instance parameters
     *
     * @param parameters the parameters
     * @return the script instance parameters
     * @throws BusinessException the business exception
     */
    private Map<String, Object> getScriptInstanceParameters(Map<String, Object> parameters) throws BusinessException {
        String fileName = (String) parameters.get("origin_filename");
        Map<String, Object> scriptInstanceParameters = scriptInstancesParameters.get(fileName);
        if (scriptInstanceParameters == null || scriptInstanceParameters.isEmpty()) {
            throw new BusinessException("The file " + fileName + " is not found or is not uploaded and validated by file format");
        }
        return scriptInstanceParameters;
    }

    /**
     * Initialize method
     *
     * @param parameters the context
     * @throws BusinessException the business exception
     */
    @Override
    public void init(Map<String, Object> parameters) throws BusinessException {
        long startInitTime = System.currentTimeMillis();
        parameters.put("startInitTime", startInitTime);
        log.debug("Starting The insertion part ...........");
        cachedData.clear();
        try {
            log.debug(" >>> Start ImportStagingRecordsScript -> init {} ", parameters.entrySet());
            String fileName = this.getContextParam(parameters, "origin_filename", true);
            FlatFile flatFile = flatFileService.getFlatFileByFileName(fileName);
            if (flatFile == null) {
                throw new BusinessException("The file " + fileName + " is not found or is not uploaded and validated by file format");
            }
            parameters.put("flatFileId", flatFile.getId());

            String importType = (String) parameters.get("importType");
            if (StringUtils.isBlank(importType) || !TransformationRulesEnum.contains(importType)) {
                throw new BusinessException("The import type is not recognized");
            }
            parameters.put("importType", importType);

            // Check if the file name is unique in the staging table.
            TransformationRulesHandler transformationRulesHandler = importService.getTransformationRules(importType);
            List<String> stagingRecordsList = importService.findStagingRecordsByFileName(transformationRulesHandler.getStageTableName(), flatFile.getFileOriginalName());
            if (stagingRecordsList != null && !stagingRecordsList.isEmpty()) {
                throw new BusinessException("There is already staging records with the same file name");
            }
            scriptInstancesParameters.put(fileName, parameters);
        } catch (Exception e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * Get context parameters
     *
     * @param methodContext the context
     * @param key           the parameter key
     * @param mandatory     true is mandatory parameter
     * @return true is not blank parameter
     * @throws BusinessException the business exception
     */
    private String getContextParam(Map<String, Object> methodContext, String key, boolean mandatory) throws BusinessException {
        String value = (String) methodContext.get(key);
        if (mandatory && StringUtils.isBlank(value)) {
            throw new BusinessException(String.format("Missing context param : %s", key));
        }
        return value;
    }

    @Override
    @JpaAmpNewTx
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void terminate(Map<String, Object> parameters) throws BusinessException {
        long startTerminateTime = System.currentTimeMillis();
        long endTerminateTime = startTerminateTime;
        if (scriptInstancesParameters.get(parameters.get("origin_filename")) != null &&
                scriptInstancesParameters.get(parameters.get("origin_filename")).get("startInitTime") != null) {
            startTerminateTime = (Long) scriptInstancesParameters.get(parameters.get("origin_filename")).get("startInitTime");
        }
        log.debug("The insertion part took " + (endTerminateTime - startTerminateTime) + " milliseconds");

        if (scriptInstancesParameters.get(parameters.get("origin_filename")) != null &&
                !scriptInstancesParameters.get(parameters.get("origin_filename")).containsKey("exception")) {
            log.debug("Starting The validation part ...........");
            importService.validateRecords((String) parameters.get("importType"), (Long) parameters.get("flatFileId"));
            endTerminateTime = System.currentTimeMillis();
            log.debug("The validation part took " + (endTerminateTime - startTerminateTime) + " milliseconds");
        }
        
        cachedData.clear();
    }

}