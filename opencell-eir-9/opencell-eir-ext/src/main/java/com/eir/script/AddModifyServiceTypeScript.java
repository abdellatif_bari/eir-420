package com.eir.script;

import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.model.customEntities.CustomEntityInstance;
import org.meveo.service.custom.CustomEntityInstanceService;
import org.meveo.service.script.Script;

/**
 * Delete ServiceType Script
 * 
 * @author hznibar
 *
 */
@Stateless
public class AddModifyServiceTypeScript extends Script {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    @Inject
    private CustomEntityInstanceService customEntityInstanceService;

    // private static final String SERVICE_TYPE = "Service_Type";

    @Override
    public void execute(Map<String, Object> initContext) throws BusinessException {
        log.debug("#####################Starting of script AddModifyServiceTypeScript");
        try {
            String serviceType = (String) initContext.get("CODE");
            CustomEntityInstance ce = (CustomEntityInstance) initContext.get("event");

            if (serviceType == null) {
                throw new BusinessException(serviceType + ": The Service Type Code is mandatory!");
            }

            if (serviceType.length() > 4) {
                throw new BusinessException(serviceType + ": The Service Type Code must be a maximum of 4 characters long!");
            }
            if (ce != null && ce.getId() != null) {
                CustomEntityInstance oldServiceType = customEntityInstanceService.findById(ce.getId());
                if (!serviceType.equals(oldServiceType.getCode())) {
                    throw new BusinessException(serviceType + ": Service Type Code should not be modified!");
                }
            }

        } catch (Exception e) {
            throw new BusinessException(e.getMessage());

        }
    }
}