package com.eir.script;

import com.eir.commons.enums.customfields.BillingAccountCFsEnum;
import org.meveo.admin.exception.BusinessException;
import org.meveo.model.billing.BillingAccount;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.script.Script;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Map;

/**
 * Reject BA modification when service type is changed on BA without providing a comment/note
 *
 * @author hznibar
 */
@Stateless
public class ModifyBaNotificationScript extends Script {

    private static final long serialVersionUID = -791986517726415310L;

    @Inject
    private BillingAccountService billingAccountService;

    @SuppressWarnings("unchecked")
    @Override
    public void execute(Map<String, Object> initContext) throws BusinessException {

        BillingAccount newBa = (BillingAccount) initContext.get(Script.CONTEXT_ENTITY);

        BillingAccount oldBa = billingAccountService.findById(newBa.getId());
        String oldServiceType = (String) oldBa.getCfValue(BillingAccountCFsEnum.SERVICE_TYPE.name());
        String newServiceType = (String) newBa.getCfValue(BillingAccountCFsEnum.SERVICE_TYPE.name());

        if (oldServiceType != null && !oldServiceType.equals(newServiceType)) {
            Map<String, String> oldNotes = (Map<String, String>) oldBa.getCfValue("BA_NOTES");
            Map<String, String> newNotes = (Map<String, String>) newBa.getCfValue("BA_NOTES");

            if (newNotes == null || newNotes.size() == 0 || (oldNotes != null && newNotes.size() <= oldNotes.size())) {
                throw new BusinessException("Please insert a comment in the Billing Account Notes regarding change of Service Type.");
            }
        }
    }
}