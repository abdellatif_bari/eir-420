package com.eir.script;

import com.eir.commons.enums.ApplicationPropertiesEnum;
import com.eir.commons.enums.customfields.BillingAccountCFsEnum;
import com.eir.commons.enums.customtables.CustomTableEnum;
import org.meveo.admin.exception.BusinessException;
import org.meveo.api.commons.Utils;
import org.meveo.commons.utils.InvoiceCategoryComparatorUtils;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.BillingCycle;
import org.meveo.model.billing.CategoryInvoiceAgregate;
import org.meveo.model.billing.Invoice;
import org.meveo.model.billing.InvoiceAgregate;
import org.meveo.model.billing.InvoiceCategory;
import org.meveo.model.billing.InvoiceConfiguration;
import org.meveo.model.billing.InvoiceSubCategory;
import org.meveo.model.billing.RatedTransaction;
import org.meveo.model.billing.SubCategoryInvoiceAgregate;
import org.meveo.model.billing.SubcategoryInvoiceAgregateAmount;
import org.meveo.model.billing.Tax;
import org.meveo.model.billing.UserAccount;
import org.meveo.model.billing.WalletInstance;
import org.meveo.model.billing.XMLInvoiceHeaderCategoryDTO;
import org.meveo.model.catalog.Calendar;
import org.meveo.model.catalog.RoundingModeEnum;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.customEntities.CustomEntityInstance;
import org.meveo.model.shared.DateUtils;
import org.meveo.model.tax.TaxCategory;
import org.meveo.service.billing.impl.XmlInvoiceCreatorScript;
import org.meveo.service.crm.impl.CustomFieldTemplateService;
import org.meveo.service.custom.CustomEntityInstanceService;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collectors;

import static org.meveo.commons.utils.StringUtils.getDefaultIfNull;

/**
 * A EIR implementation of XML invoice generation
 *
 * @author Andrius Karpavicius
 */
@Stateless
public class EirXmlInvoiceCreatorScript extends XmlInvoiceCreatorScript {

    @Inject
    private CustomFieldTemplateService customFieldTemplateService;
    @Inject
    private CustomEntityInstanceService customEntityInstanceService;

    @SuppressWarnings("unchecked")
    @Override
    protected List<RatedTransaction> getRatedTransactions(Invoice invoice, boolean isVirtual) {

        String sql = "select description, (CASE WHEN start_date is not null THEN null ELSE DATE(usage_date) END) as usageDate, DATE(start_date) as startDate, " +
                "DATE(end_date) as endDate, aggregate_id_f, tax_id, tax_percent, sum(abs(quantity)) as quantitySum, sum(amount_without_tax) as amountWithoutTax, " +
                "sum(amount_with_tax) as amountWithTax, sum(amount_tax) as amountTax, COUNT(*) as calls  from billing_rated_transaction rt " +
                "where rt.invoice_id=:invoice and rt.status='BILLED' group by description, usageDate, startDate, endDate, tax_percent, aggregate_id_f, tax_id " +
                "order by description, usageDate, startDate ";

        EntityManager em = invoiceService.getEntityManager();
        List<Object[]> rtData = em.createNativeQuery(sql).setParameter("invoice", invoice.getId()).getResultList();

        List<RatedTransaction> ratedTransactions = rtData.stream().map(data -> {
            RatedTransaction rt = new RatedTransaction();
            rt.setDescription((String) data[0]);
            rt.setUsageDate((Date) data[1]);
            rt.setStartDate((Date) data[2]);
            rt.setEndDate((Date) data[3]);
            rt.setInvoiceAgregateF(em.getReference(SubCategoryInvoiceAgregate.class, ((BigInteger) data[4]).longValue()));
            rt.setTax(em.getReference(Tax.class, ((BigInteger) data[5]).longValue()));
            rt.setTaxPercent((BigDecimal) data[6]);
            rt.setQuantity((BigDecimal) data[7]);
            rt.setAmountWithoutTax((BigDecimal) data[8]);
            rt.setAmountWithTax((BigDecimal) data[9]);
            rt.setAmountTax((BigDecimal) data[10]);
            rt.setParameter1(String.valueOf(data[11]));

            return rt;
        }).collect(Collectors.toList());

        return ratedTransactions;
    }

    /**
     * Create invoice/header/categories DOM element
     *
     * @param doc                  XML invoice DOM
     * @param header               the header node
     * @param invoice              Invoice to convert
     * @param invoiceConfiguration Invoice configuration
     * @return DOM element
     */
    private void createHeaderCategoriesSection(Document doc, Element header, Invoice invoice, InvoiceConfiguration invoiceConfiguration) {

        String billingAccountLanguage = invoice.getBillingAccount().getTradingLanguage().getLanguageCode();

        LinkedHashMap<String, XMLInvoiceCategoryDTO> headerCategories = new LinkedHashMap();
        List<CategoryInvoiceAgregate> categoryInvoiceAgregates = new ArrayList<>();

        for (InvoiceAgregate invoiceAgregate : invoice.getInvoiceAgregates()) {

            if (invoiceAgregate instanceof CategoryInvoiceAgregate) {
                CategoryInvoiceAgregate categoryInvoiceAgregate = (CategoryInvoiceAgregate) invoiceAgregate;
                categoryInvoiceAgregates.add(categoryInvoiceAgregate);
            }
        }

        Collections.sort(categoryInvoiceAgregates, InvoiceCategoryComparatorUtils.getInvoiceCategoryComparator());

        for (CategoryInvoiceAgregate categoryInvoiceAgregate : categoryInvoiceAgregates) {
            InvoiceCategory invoiceCategory = categoryInvoiceAgregate.getInvoiceCategory();
            XMLInvoiceCategoryDTO headerCat = null;
            if (headerCategories.containsKey(invoiceCategory.getCode())) {
                headerCat = headerCategories.get(invoiceCategory.getCode());
                headerCat.addAmountWithoutTax(categoryInvoiceAgregate.getAmountWithoutTax());
                headerCat.addAmountWithTax(categoryInvoiceAgregate.getAmountWithTax());
                headerCat.addAmountTax(categoryInvoiceAgregate.getAmountTax());

            } else {
                headerCat = new XMLInvoiceCategoryDTO();
                if (invoiceCategory.getDescriptionI18n() == null || invoiceCategory.getDescriptionI18n().get(billingAccountLanguage) == null) {
                    headerCat.setDescription(invoiceCategory.getDescription());
                } else {
                    headerCat.setDescription(invoiceCategory.getDescriptionI18n().get(billingAccountLanguage));
                }

                headerCat.setCode(invoiceCategory.getCode());
                headerCat.setAmountWithoutTax(categoryInvoiceAgregate.getAmountWithoutTax());
                headerCat.setAmountWithTax(categoryInvoiceAgregate.getAmountWithTax());
                headerCat.setAmountTax(categoryInvoiceAgregate.getAmountTax());
                headerCat.setSortIndex(categoryInvoiceAgregate.getInvoiceCategory().getSortIndex());
                headerCat.setCategoryInvoiceAggregate((String) invoiceCategory.getCfValue("CATEGORY_INVOICE_AGGREGATE"));
                headerCategories.put(invoiceCategory.getCode(), headerCat);
            }

            List<SubCategoryInvoiceAgregate> subCategoryInvoiceAgregates = new ArrayList<SubCategoryInvoiceAgregate>(categoryInvoiceAgregate.getSubCategoryInvoiceAgregates());
            Collections.sort(subCategoryInvoiceAgregates, InvoiceCategoryComparatorUtils.getInvoiceSubCategoryComparator());
            for (SubCategoryInvoiceAgregate subCatInvoiceAgregate : subCategoryInvoiceAgregates) {
                if (!subCatInvoiceAgregate.isDiscountAggregate()) {
                    headerCat.getSubCategoryInvoiceAgregates().add(subCatInvoiceAgregate);
                }
            }
        }


        Map<String, Map<String, XMLInvoiceCategoryDTO>> categoryInvoiceAggregatesMap = new HashMap<>();


        for (XMLInvoiceCategoryDTO xmlInvoiceCategoryDTO : headerCategories.values()) {

            if (!StringUtils.isBlank(xmlInvoiceCategoryDTO.getCategoryInvoiceAggregate()) &&
                    !categoryInvoiceAggregatesMap.containsKey(xmlInvoiceCategoryDTO.getCategoryInvoiceAggregate())) {
                categoryInvoiceAggregatesMap.put(xmlInvoiceCategoryDTO.getCategoryInvoiceAggregate(), new HashMap());
            }

            if (xmlInvoiceCategoryDTO.getSubCategoryInvoiceAgregates() != null) {
                for (SubCategoryInvoiceAgregate subCatInvoiceAgregate : xmlInvoiceCategoryDTO.getSubCategoryInvoiceAgregates()) {

                    if (subCatInvoiceAgregate.getAmountsByTax() != null && !subCatInvoiceAgregate.getAmountsByTax().isEmpty()) {

                        Map<String, XMLInvoiceCategoryDTO> taxInvoiceAggregatesMap = categoryInvoiceAggregatesMap.get(xmlInvoiceCategoryDTO.getCategoryInvoiceAggregate());
                        for (Map.Entry<Tax, SubcategoryInvoiceAgregateAmount> amountByTax : subCatInvoiceAgregate.getAmountsByTax().entrySet()) {

                            if (taxInvoiceAggregatesMap != null) {
                                if (taxInvoiceAggregatesMap.containsKey(amountByTax.getKey().getCode())) {
                                    XMLInvoiceCategoryDTO xmlInvoiceCategory = taxInvoiceAggregatesMap.get(amountByTax.getKey().getCode());
                                    if (amountByTax.getValue().getAmountWithoutTax() != null) {
                                        xmlInvoiceCategory.setAmountWithoutTax(xmlInvoiceCategory.getAmountWithoutTax().add(amountByTax.getValue().getAmountWithoutTax()));
                                    }
                                    if (amountByTax.getValue().getAmountTax() != null) {
                                        xmlInvoiceCategory.setAmountTax(xmlInvoiceCategory.getAmountTax().add(amountByTax.getValue().getAmountTax()));
                                    }
                                    if (amountByTax.getValue().getAmountWithTax() != null) {
                                        xmlInvoiceCategory.setAmountWithTax(xmlInvoiceCategory.getAmountWithTax().add(amountByTax.getValue().getAmountWithTax()));
                                    }
                                } else {
                                    XMLInvoiceCategoryDTO xmlInvoiceCategory = new XMLInvoiceCategoryDTO();
                                    xmlInvoiceCategory.setTaxPercent(amountByTax.getKey().getPercent());
                                    xmlInvoiceCategory.setAmountWithoutTax(amountByTax.getValue().getAmountWithoutTax());
                                    xmlInvoiceCategory.setAmountTax(amountByTax.getValue().getAmountTax());
                                    xmlInvoiceCategory.setAmountWithTax(amountByTax.getValue().getAmountWithTax());
                                    taxInvoiceAggregatesMap.put(amountByTax.getKey().getCode(), xmlInvoiceCategory);
                                }
                            }
                        }
                    }
                }
            }
        }

        if (!categoryInvoiceAggregatesMap.isEmpty()) {
            LinkedHashMap<String, Map<String, XMLInvoiceCategoryDTO>> sortedMap = new LinkedHashMap<>();

            categoryInvoiceAggregatesMap.entrySet().stream().sorted(Map.Entry.comparingByKey()).forEachOrdered(x -> sortedMap.put(x.getKey(), x.getValue()));

            Element categoryInvoiceAggregatesElement = doc.createElement("categoryInvoiceAggregates");
            for (Map.Entry<String, Map<String, XMLInvoiceCategoryDTO>> entryCategory : sortedMap.entrySet()) {
                String description = getCategoryInvoiceAggregateDescription(entryCategory.getKey());
                for (Map.Entry<String, XMLInvoiceCategoryDTO> entryTax : entryCategory.getValue().entrySet()) {
                    Element categoryInvoiceAggregate = doc.createElement("categoryInvoiceAggregate");
                    categoryInvoiceAggregate.setAttribute("code", entryCategory.getKey());
                    categoryInvoiceAggregate.setAttribute("description", description);
                    categoryInvoiceAggregate.setAttribute("amountWithoutTax", toPlainString(entryTax.getValue().getAmountWithoutTax()));
                    categoryInvoiceAggregate.setAttribute("taxPercent", toPlainString(entryTax.getValue().getTaxPercent()));
                    categoryInvoiceAggregate.setAttribute("amountTax", toPlainString(entryTax.getValue().getAmountTax()));
                    categoryInvoiceAggregate.setAttribute("amountWithTax", toPlainString(entryTax.getValue().getAmountWithTax()));
                    categoryInvoiceAggregatesElement.appendChild(categoryInvoiceAggregate);
                }
            }
            header.appendChild(categoryInvoiceAggregatesElement);
        }
    }

    /**
     * Create invoice/header DOM element
     *
     * @param doc                  XML invoice DOM
     * @param invoice              Invoice to convert
     * @param isInvoiceAdjustment  Is this an adjustment invoice
     * @param invoiceConfiguration Invoice configuration
     * @return DOM element
     */
    protected Element createHeaderSection(Document doc, Invoice invoice, boolean isInvoiceAdjustment, InvoiceConfiguration invoiceConfiguration) {

        Element header = doc.createElement("header");
        if (invoiceConfiguration.isDisplayProvider()) {
            Element providerTag = createProviderSection(doc, appProvider, invoiceConfiguration);
            if (providerTag != null) {
                header.appendChild(providerTag);
            }
        }

        Element customerTag = createCustomerSection(doc, invoice, invoiceConfiguration);
        if (customerTag != null) {
            header.appendChild(customerTag);
        }
        Element sellerTag = createSellerSection(doc, invoice);
        if (sellerTag != null) {
            header.appendChild(sellerTag);
        }

        Invoice linkedInvoice = invoiceService.getLinkedInvoice(invoice);

        Element customerAccountTag = createCustomerAccountSection(doc, invoice, linkedInvoice, isInvoiceAdjustment, invoiceConfiguration);
        if (customerAccountTag != null) {
            header.appendChild(customerAccountTag);
        }

        Element billingAccountTag = createBillingAccountSection(doc, invoice, linkedInvoice, isInvoiceAdjustment, invoiceConfiguration);
        if (billingAccountTag != null) {
            header.appendChild(billingAccountTag);
        }

        String invoiceDateFormat = ApplicationPropertiesEnum.INVOICE_DATE_FORMAT.getProperty();

        Date invoiceDateData = invoice.getInvoiceDate();
        if (invoiceDateData != null) {
            Element invoiceDate = doc.createElement("invoiceDate");
            invoiceDate.appendChild(doc.createTextNode(formatDateWithPattern(invoiceDateData, invoiceDateFormat)));
            header.appendChild(invoiceDate);

            Element invoiceMonth = doc.createElement("invoiceMonth");
            invoiceMonth.appendChild(doc.createTextNode(formatDateWithPattern(invoiceDateData, "MMM/yyyy")));
            header.appendChild(invoiceMonth);
        }

        Date dueDateData = invoice.getDueDate();
        if (dueDateData != null) {
            Element dueDate = doc.createElement("dueDate");
            dueDate.appendChild(doc.createTextNode(formatDateWithPattern(dueDateData, invoiceDateFormat)));
            header.appendChild(dueDate);
        }

        String invoiceExternalRefData = invoice.getExternalRef();
        if (!StringUtils.isBlank(invoiceExternalRefData)) {
            Element invoiceExternalRef = doc.createElement("invoiceExternalRef");
            invoiceExternalRef.appendChild(doc.createTextNode(invoiceExternalRefData));
            header.appendChild(invoiceExternalRef);
        }

        TaxCategory taxCategory = getTaxCategory(invoice.getBillingAccount());
        if (taxCategory != null) {
            String invoiceTaxMessage = (String) taxCategory.getCfValue("invoiceTaxMessage");
            if (!StringUtils.isBlank(invoiceTaxMessage)) {
                Element invoiceTaxMessageElement = doc.createElement("invoiceTaxMessage");
                invoiceTaxMessageElement.appendChild(doc.createTextNode(invoiceTaxMessage));
                header.appendChild(invoiceTaxMessageElement);
            }
        }
        createHeaderCategoriesSection(doc, header, invoice, invoiceConfiguration);
        return header;
    }

    /**
     * Determine if invoice category aggregate if for a given user account.
     *
     * @param userAccount             User account or null to select categories not associated to a user account
     * @param categoryInvoiceAgregate Category invoice aggregate
     * @return True if user account match
     */
    private boolean isValidCategoryInvoiceAgregate(final UserAccount userAccount, final CategoryInvoiceAgregate categoryInvoiceAgregate) {
        if (userAccount == null) {
            return categoryInvoiceAgregate.getUserAccount() == null;

        } else {
            Long uaId = userAccount.getId();
            return categoryInvoiceAgregate != null && categoryInvoiceAgregate.getUserAccount() != null && uaId != null &&
                    uaId.equals(categoryInvoiceAgregate.getUserAccount().getId());
        }
    }

    /**
     * Create invoice/details/userAccounts/userAccount/categories DOM element
     *
     * @param doc                  XML invoice DOM
     * @param invoice              Invoice to convert
     * @param userAccount          User account
     * @param ratedTransactions    Rated transactions
     * @param isVirtual            Is this a virtual invoice. If true, no invoice, invoice aggregate nor RT information is persisted in DB
     * @param invoiceConfiguration Invoice configuration
     * @return DOM element
     */
    protected Element createUAInvoiceCategories(Document doc, Invoice invoice, UserAccount userAccount, List<RatedTransaction> ratedTransactions,
                                                boolean isVirtual, InvoiceConfiguration invoiceConfiguration) {

        String invoiceDateFormat = ApplicationPropertiesEnum.INVOICE_DATE_FORMAT.getProperty();
        String invoiceDateTimeFormat = ApplicationPropertiesEnum.INVOICE_DATE_TIME_FORMAT.getProperty();

        String invoiceLanguageCode = invoice.getBillingAccount().getTradingLanguage().getLanguage().getLanguageCode();

        List<CategoryInvoiceAgregate> categoryInvoiceAgregates = new ArrayList<>();

        for (InvoiceAgregate invoiceAgregate : invoice.getInvoiceAgregates()) {
            if (invoiceAgregate instanceof CategoryInvoiceAgregate && isValidCategoryInvoiceAgregate(userAccount, (CategoryInvoiceAgregate) invoiceAgregate)) {
                CategoryInvoiceAgregate categoryInvoiceAgregate = (CategoryInvoiceAgregate) invoiceAgregate;
                categoryInvoiceAgregates.add(categoryInvoiceAgregate);
            }
        }

        if (categoryInvoiceAgregates.isEmpty()) {
            return null;
        }

        Collections.sort(categoryInvoiceAgregates, InvoiceCategoryComparatorUtils.getInvoiceCategoryComparator());

        Element categoryInvoiceAggregatesTag = doc.createElement("categoryInvoiceAggregates");

        Element categoriesTag = null;
        Map<String, Element> categoryInvoiceAggregatesMap = new HashMap<>();
        Map<String, Element> categoryInvoicesMap = new HashMap<>();
        Map<String, XMLInvoiceCategoryDTO> xmlInvoiceCategoryDTOsMap = new HashMap<>();
        for (CategoryInvoiceAgregate categoryInvoiceAgregate : categoryInvoiceAgregates) {

            InvoiceCategory invoiceCategory = categoryInvoiceAgregate.getInvoiceCategory();
            String categoryInvoiceAggregate = (String) invoiceCategory.getCfValue("CATEGORY_INVOICE_AGGREGATE");
            String description = getCategoryInvoiceAggregateDescription(categoryInvoiceAggregate);


            List<Element> categories = createDetailsUAInvoiceCategoriesSection(doc, invoice, categoryInvoiceAgregate, ratedTransactions, isVirtual, invoiceDateFormat,
                    invoiceDateTimeFormat, invoiceLanguageCode, invoiceConfiguration, xmlInvoiceCategoryDTOsMap);
            if (categories != null && !categories.isEmpty()) {
                for (Element categoryTag : categories) {
                    Element categoryInvoiceAggregateTag = categoryInvoiceAggregatesMap.get(categoryInvoiceAggregate);
                    categoriesTag = categoryInvoicesMap.get(categoryInvoiceAggregate);
                    if (categoryInvoiceAggregateTag == null) {
                        categoryInvoiceAggregateTag = doc.createElement("categoryInvoiceAggregate");
                        categoriesTag = doc.createElement("categories");
                        categoryInvoiceAggregateTag.appendChild(categoriesTag);
                        categoryInvoiceAggregatesTag.appendChild(categoryInvoiceAggregateTag);
                        categoryInvoiceAggregatesMap.put(categoryInvoiceAggregate, categoryInvoiceAggregateTag);
                        categoryInvoicesMap.put(categoryInvoiceAggregate, categoriesTag);
                    }
                    XMLInvoiceCategoryDTO xmlInvoiceCategoryDTO = xmlInvoiceCategoryDTOsMap.get(categoryInvoiceAggregate);

                    categoryInvoiceAggregateTag.setAttribute("code", categoryInvoiceAggregate);
                    categoryInvoiceAggregateTag.setAttribute("description", description);
                    categoryInvoiceAggregateTag.setAttribute("amountWithoutTax", toPlainString(xmlInvoiceCategoryDTO.getAmountWithoutTax()));
                    categoryInvoiceAggregateTag.setAttribute("amountTax", toPlainString(xmlInvoiceCategoryDTO.getAmountTax()));
                    categoryInvoiceAggregateTag.setAttribute("amountWithTax", toPlainString(xmlInvoiceCategoryDTO.getAmountWithTax()));
                    categoriesTag.appendChild(categoryTag);
                }
            }
        }
        return categoryInvoiceAggregatesTag.getChildNodes().getLength() > 0 ? categoryInvoiceAggregatesTag : null;
    }

    /**
     * Create invoice/details/userAccounts/userAccount/categories/category DOM element
     *
     * @param doc                       XML invoice DOM
     * @param invoice                   Invoice to convert
     * @param categoryInvoiceAgregate   Invoice category aggregate
     * @param ratedTransactions         Rated transactions
     * @param isVirtual                 Is this a virtual invoice. If true, no invoice, invoice aggregate nor RT information is persisted in DB
     * @param invoiceDateFormat         Date format
     * @param invoiceDateTimeFormat     Timestamp format
     * @param invoiceLanguageCode       Invoice language - language code
     * @param invoiceConfiguration      Invoice configuration
     * @param xmlInvoiceCategoryDTOsMap Xml invoice category DTO map
     * @return DOM list elements
     */
    protected List<Element> createDetailsUAInvoiceCategoriesSection(Document doc, Invoice invoice, CategoryInvoiceAgregate categoryInvoiceAgregate, List<RatedTransaction> ratedTransactions, boolean isVirtual,
                                                                    String invoiceDateFormat, String invoiceDateTimeFormat, String invoiceLanguageCode, InvoiceConfiguration invoiceConfiguration,
                                                                    Map<String, XMLInvoiceCategoryDTO> xmlInvoiceCategoryDTOsMap) {

        InvoiceCategory invoiceCategory = categoryInvoiceAgregate.getInvoiceCategory();
        String invoiceCategoryLabel = categoryInvoiceAgregate.getDescription();

        if (invoiceCategory.getDescriptionI18n() != null && invoiceCategory.getDescriptionI18n().get(invoiceLanguageCode) != null) {
            invoiceCategoryLabel = invoiceCategory.getDescriptionI18n().get(invoiceLanguageCode);
        }


        List<SubCategoryInvoiceAgregate> subCategoryInvoiceAgregates = new ArrayList<SubCategoryInvoiceAgregate>(categoryInvoiceAgregate.getSubCategoryInvoiceAgregates());
        Collections.sort(subCategoryInvoiceAgregates, InvoiceCategoryComparatorUtils.getInvoiceSubCategoryComparator());


        Map<String, List<Element>> mapSubCategories = new HashMap();
        String categoryInvoiceAggregate = (String) invoiceCategory.getCfValue("CATEGORY_INVOICE_AGGREGATE");
        for (SubCategoryInvoiceAgregate subCatInvoiceAgregate : subCategoryInvoiceAgregates) {
            if (subCatInvoiceAgregate.isDiscountAggregate()) {
                continue;
            }

            Element subCategory = createUAInvoiceSubcategorySection(doc, invoice, subCatInvoiceAgregate, ratedTransactions, isVirtual, invoiceDateFormat, invoiceDateTimeFormat, invoiceLanguageCode, invoiceConfiguration);
            if (subCategory != null) {

                String taxPercent = subCategory.getAttribute("taxPercent");
                if (!mapSubCategories.containsKey(taxPercent)) {
                    List<Element> subCategoriesElements = new ArrayList<>();
                    subCategoriesElements.add(subCategory);
                    mapSubCategories.put(taxPercent, subCategoriesElements);
                } else {
                    mapSubCategories.get(taxPercent).add(subCategory);
                }

                if (!xmlInvoiceCategoryDTOsMap.containsKey(categoryInvoiceAggregate)) {
                    xmlInvoiceCategoryDTOsMap.put(categoryInvoiceAggregate, new XMLInvoiceCategoryDTO());
                }
                XMLInvoiceCategoryDTO xmlInvoiceCategoryDTO = xmlInvoiceCategoryDTOsMap.get(categoryInvoiceAggregate);
                xmlInvoiceCategoryDTO.setAmountWithoutTax(xmlInvoiceCategoryDTO.getAmountWithoutTax().add(subCatInvoiceAgregate.getAmountWithoutTax()));
                xmlInvoiceCategoryDTO.setAmountTax(xmlInvoiceCategoryDTO.getAmountTax().add(subCatInvoiceAgregate.getAmountTax()));
                xmlInvoiceCategoryDTO.setAmountWithTax(xmlInvoiceCategoryDTO.getAmountWithTax().add(subCatInvoiceAgregate.getAmountWithTax()));
            }

        }

        List<Element> categories = new ArrayList<>();

        for (Map.Entry<String, List<Element>> entry : mapSubCategories.entrySet()) {
            Element categoryTag = doc.createElement("category");
            categoryTag.setAttribute("label", getDefaultIfNull(invoiceCategoryLabel, ""));
            categoryTag.setAttribute("code", invoiceCategory.getCode());
            categoryTag.setAttribute("amountWithoutTax", toPlainString(categoryInvoiceAgregate.getAmountWithoutTax()));
            categoryTag.setAttribute("amountWithTax", toPlainString(categoryInvoiceAgregate.getAmountWithTax()));
            categoryTag.setAttribute("amountTax", toPlainString(categoryInvoiceAgregate.getAmountTax()));
            categoryTag.setAttribute("sortIndex", (invoiceCategory.getSortIndex() != null) ? invoiceCategory.getSortIndex() + "" : "");

            addCustomFields(invoiceCategory, doc, categoryTag);
            Element subCategories = doc.createElement("subCategories");

            for (Element subCategory : entry.getValue()) {
                subCategories.appendChild(subCategory);
            }
            categoryTag.appendChild(subCategories);
            categories.add(categoryTag);
        }

        return categories;
    }

    /**
     * Create invoice/details/userAccounts/userAccount/categories/category/subcategories/subcategory/line DOM element
     *
     * @param doc                   XML invoice DOM
     * @param invoice               Invoice to convert
     * @param subCatInvoiceAgregate Invoice subcategory aggregate
     * @param ratedTransactions     Rated transactions
     * @param isVirtual             Is this a virtual invoice. If true, no invoice, invoice aggregate nor RT information is persisted in DB
     * @param invoiceDateFormat     Date format
     * @param invoiceDateTimeFormat Timestamp format
     * @param invoiceLanguageCode   Invoice language - language code
     * @param invoiceConfiguration  Invoice configuration
     * @param subCategory           sub category
     */
    private void createRTSection(Document doc, Invoice invoice, SubCategoryInvoiceAgregate subCatInvoiceAgregate, List<RatedTransaction> ratedTransactions, boolean isVirtual,
                                 String invoiceDateFormat, String invoiceDateTimeFormat, String invoiceLanguageCode, InvoiceConfiguration invoiceConfiguration, Element subCategory) {
        WalletInstance wallet = subCatInvoiceAgregate.getWallet();
        Long walletId = wallet != null ? wallet.getId() : null;
        if (isVirtual) {
            ratedTransactions = subCatInvoiceAgregate.getRatedtransactionsToAssociate();
        }
        Collections.sort(ratedTransactions, InvoiceCategoryComparatorUtils.getRatedTransactionComparator());
        BillingCycle billingCycle = getBillingCycle(invoice);

        BillingAccount billingAccount = invoice.getBillingAccount();


        Date initCalendarDate = billingAccount.getSubscriptionDate() != null ? billingAccount.getSubscriptionDate() : billingAccount.getAuditable().getCreated();
        Calendar bcCalendar = billingCycle.getCalendar();
        bcCalendar.setInitDate(initCalendarDate);

        List<RatedTransaction> billedRatedTransactions = new ArrayList<>();
        LinkedHashMap<String, RatedTransaction> groupedRatedTransactions = new LinkedHashMap<>();
        for (RatedTransaction ratedTransaction : ratedTransactions) {
            if ((ratedTransaction.getInvoiceAgregateF().getId() != null && !ratedTransaction.getInvoiceAgregateF().getId().equals(subCatInvoiceAgregate.getId()))
                    || (ratedTransaction.getInvoiceAgregateF().getId() == null && !ratedTransaction.getInvoiceSubCategory().getId().equals(subCatInvoiceAgregate.getInvoiceSubCategory().getId())
                    && !((ratedTransaction.getWallet() == null && walletId == null) || (walletId != null && walletId.equals(ratedTransaction.getWallet().getId()))))) {
                continue;
            }

            if (billingCycle != null) {
                String key = null;
                Date dateRT = null;
                if (ratedTransaction.getStartDate() != null) {
                    dateRT = ratedTransaction.getStartDate();
                } else if (ratedTransaction.getUsageDate() != null) {
                    dateRT = ratedTransaction.getUsageDate();
                }
                if (dateRT != null) {
                    Date periodStartDateRT = bcCalendar.previousCalendarDate(dateRT);
                    Date periodEndDateRT = bcCalendar.nextCalendarDate(dateRT);
                    ratedTransaction.setStartDate(periodStartDateRT);
                    ratedTransaction.setEndDate(periodEndDateRT);
                    key = ratedTransaction.getDescription() + "|@|" + periodStartDateRT.toString() + "|@|" + periodEndDateRT.toString() + "|@|"
                            + ratedTransaction.getTaxPercent();
                    RatedTransaction rt = groupedRatedTransactions.get(key);
                    if (rt != null) {
                        rt.setQuantity(rt.getQuantity().add(ratedTransaction.getQuantity()));
                        rt.setAmountWithoutTax(rt.getAmountWithoutTax().add(ratedTransaction.getAmountWithoutTax()));
                        rt.setAmountWithTax(rt.getAmountWithTax().add(ratedTransaction.getAmountWithTax()));
                        //rt.setAmountTax(rt.getAmountTax().add(ratedTransaction.getAmountTax()));
                        if (!Utils.isInteger(ratedTransaction.getParameter1())) {
                            ratedTransaction.setParameter1("1");
                        }
                        if (!Utils.isInteger(rt.getParameter1())) {
                            rt.setParameter1("1");
                        }
                        int calls = Utils.toInteger(rt.getParameter1()) + Utils.toInteger(ratedTransaction.getParameter1());
                        rt.setParameter1(String.valueOf(calls));
                    } else {
                        groupedRatedTransactions.put(key, ratedTransaction);
                    }
                }
            }
            billedRatedTransactions.add(ratedTransaction);
        }

        billedRatedTransactions = billingCycle != null ? new ArrayList(groupedRatedTransactions.values()) : billedRatedTransactions;

        for (RatedTransaction ratedTransaction : billedRatedTransactions) {
            Element rtTag = createRTSection(doc, invoice, ratedTransaction, invoiceDateFormat, invoiceDateTimeFormat, invoiceConfiguration, invoiceLanguageCode);
            if (rtTag != null) {
                subCategory.setAttribute("taxPercent", toPlainString(ratedTransaction.getTaxPercent()));
                subCategory.appendChild(rtTag);
            }
        }
    }

    /**
     * Create invoice/details/userAccounts/userAccount/categories/category/subcategories/subcategory DOM element
     *
     * @param doc                   XML invoice DOM
     * @param invoice               Invoice to convert
     * @param subCatInvoiceAgregate Invoice subcategory aggregate
     * @param ratedTransactions     Rated transactions
     * @param isVirtual             Is this a virtual invoice. If true, no invoice, invoice aggregate nor RT information is persisted in DB
     * @param invoiceDateFormat     Date format
     * @param invoiceDateTimeFormat Timestamp format
     * @param invoiceLanguageCode   Invoice language - language code
     * @param invoiceConfiguration  Invoice configuration
     * @return DOM element
     */
    protected Element createUAInvoiceSubcategorySection(Document doc, Invoice invoice, SubCategoryInvoiceAgregate subCatInvoiceAgregate, List<RatedTransaction> ratedTransactions, boolean isVirtual,
                                                        String invoiceDateFormat, String invoiceDateTimeFormat, String invoiceLanguageCode, InvoiceConfiguration invoiceConfiguration) {

        InvoiceSubCategory invoiceSubCat = subCatInvoiceAgregate.getInvoiceSubCategory();
        String invoiceSubCategoryLabel = subCatInvoiceAgregate.getDescription();

        if (invoiceSubCat.getDescriptionI18n() != null && invoiceSubCat.getDescriptionI18n().get(invoiceLanguageCode) != null) {
            // get label description by language code
            invoiceSubCategoryLabel = invoiceSubCat.getDescriptionI18n().get(invoiceLanguageCode);
        }

        Element subCategory = doc.createElement("subCategory");

        subCategory.setAttribute("label", getDefaultIfNull(invoiceSubCategoryLabel, ""));
        subCategory.setAttribute("code", invoiceSubCat.getCode());
        subCategory.setAttribute("amountWithoutTax", toPlainString(subCatInvoiceAgregate.getAmountWithoutTax()));
        subCategory.setAttribute("amountWithTax", toPlainString(subCatInvoiceAgregate.getAmountWithTax()));
        subCategory.setAttribute("amountTax", toPlainString(subCatInvoiceAgregate.getAmountTax()));
        subCategory.setAttribute("sortIndex", (invoiceSubCat.getSortIndex() != null) ? invoiceSubCat.getSortIndex() + "" : "");

        createRTSection(doc, invoice, subCatInvoiceAgregate, ratedTransactions, isVirtual, invoiceDateFormat, invoiceDateTimeFormat, invoiceLanguageCode, invoiceConfiguration, subCategory);
        addCustomFields(invoiceSubCat, doc, subCategory);
        return subCategory;
    }

    /**
     * Validate service type
     *
     * @param billingAccount the bill account
     * @return service type of billing account
     * @throws BusinessException the business exception
     */
    private String getBillingAccountServiceType(BillingAccount billingAccount) {
        String serviceTypeDescription = null;
        String serviceType = (String) billingAccount.getCfValue(BillingAccountCFsEnum.SERVICE_TYPE.name());
        if (!StringUtils.isBlank(serviceType)) {
            CustomEntityInstance serviceTypeCET = customEntityInstanceService.findByCodeByCet(CustomTableEnum.SERVICE_TYPE.getCode(), serviceType);
            if (serviceTypeCET != null) {
                serviceTypeDescription = serviceTypeCET.getDescription();
            }
        }
        return serviceTypeDescription;
    }

    /**
     * Get the billing cycle
     *
     * @param invoice the invoice
     * @return the billing cycle
     */
    private BillingCycle getBillingCycle(Invoice invoice) {
        BillingCycle billingCycle = null;
        Invoice linkedInvoice = invoiceService.getLinkedInvoice(invoice);
        boolean isInvoiceAdjustment = invoice.getInvoiceType().getCode().equals(invoiceTypeService.getAdjustementCode());
        if (isInvoiceAdjustment && linkedInvoice != null && linkedInvoice.getBillingRun() != null) {
            billingCycle = linkedInvoice.getBillingRun().getBillingCycle();
        } else {
            if (invoice.getBillingRun() != null && invoice.getBillingRun().getBillingCycle() != null) {
                billingCycle = invoice.getBillingRun().getBillingCycle();
            }
        }
        if (billingCycle == null) {
            billingCycle = invoice.getBillingAccount().getBillingCycle();
        }
        return billingCycle;
    }

    /**
     * Create invoice/header/billingAccount DOM element
     *
     * @param doc                  XML invoice DOM
     * @param invoice              Invoice to convert
     * @param linkedInvoice        Linked invoice
     * @param isInvoiceAdjustment  Is this adjustment invoice
     * @param invoiceConfiguration Invoice configuration
     * @return DOM element
     */
    protected Element createBillingAccountSection(Document doc, Invoice invoice, Invoice linkedInvoice, boolean isInvoiceAdjustment, InvoiceConfiguration invoiceConfiguration) {

        Element billingAccountTag = doc.createElement("billingAccount");

        BillingAccount billingAccount = invoice.getBillingAccount();
        BillingCycle billingCycle = getBillingCycle(invoice);

        billingAccountTag.setAttribute("billingCycleCode", billingCycle != null ? billingCycle.getCode() : "");
        billingAccountTag.setAttribute("id", billingAccount.getId().toString());
        billingAccountTag.setAttribute("code", billingAccount.getCode());
        billingAccountTag.setAttribute("description", getDefaultIfNull(billingAccount.getDescription(), ""));
        billingAccountTag.setAttribute("externalRef1", getDefaultIfNull(billingAccount.getExternalRef1(), ""));
        billingAccountTag.setAttribute("externalRef2", getDefaultIfNull(billingAccount.getExternalRef2(), ""));
        billingAccountTag.setAttribute("jobTitle", getDefaultIfNull(billingAccount.getJobTitle(), ""));
        billingAccountTag.setAttribute("registrationNo", getDefaultIfNull(billingAccount.getRegistrationNo(), ""));
        billingAccountTag.setAttribute("vatNo", getDefaultIfNull(billingAccount.getVatNo(), ""));
        billingAccountTag.setAttribute("serviceType", getDefaultIfNull(getBillingAccountServiceType(billingAccount), ""));

        if (invoiceConfiguration.isDisplayBillingCycle()) {
            Element bcTag = createBillingCycleSection(doc, invoice, billingCycle);
            if (bcTag != null) {
                billingAccountTag.appendChild(bcTag);
            }
        }

        addCustomFields(billingAccount, doc, billingAccountTag);

        /*
         * if (billingAccount.getName() != null && billingAccount.getName().getTitle() != null) { // Element company = doc.createElement("company"); Text companyTxt =
         * doc.createTextNode (billingAccount.getName().getTitle().getIsCompany() + ""); billingAccountTag.appendChild(companyTxt); }
         */

        Element email = doc.createElement("email");
        if (billingAccount.getContactInformation() != null) {
            String billingEmail = billingAccount.getContactInformation().getEmail();
            Text emailTxt = doc.createTextNode(billingEmail != null ? billingEmail : "");
            email.appendChild(emailTxt);
            billingAccountTag.appendChild(email);
        }

        Element nameTag = createNameSection(doc, billingAccount, invoice.getBillingAccount().getTradingLanguage().getLanguage().getLanguageCode());
        if (nameTag != null) {
            billingAccountTag.appendChild(nameTag);
        }

        Element addressTag = createAddressSection(doc, billingAccount, invoice.getBillingAccount().getTradingLanguage().getLanguage().getLanguageCode());
        if (addressTag != null) {
            billingAccountTag.appendChild(addressTag);
        }

        return billingAccountTag;
    }

    /**
     * Create invoice/details/userAccounts/userAccount DOM element
     *
     * @param doc                  XML invoice DOM
     * @param invoice              Invoice to convert
     * @param userAccount          User account
     * @param ratedTransactions    Rated transactions
     * @param isVirtual            Is this a virtual invoice. If true, no invoice, invoice aggregate nor RT information is persisted in DB * @param ignoreUA Shall UA be ignored and all
     *                             subscriptions be returned. Used as true when aggregation by user account is turned off - system property invoice.aggregateByUA=false
     * @param invoiceLanguageCode
     * @param invoiceConfiguration Invoice configuration
     * @return
     */
    public Element createUserAccountSection(Document doc, Invoice invoice, UserAccount userAccount, List<RatedTransaction> ratedTransactions, boolean isVirtual, boolean ignoreUA, String invoiceLanguageCode,
                                            InvoiceConfiguration invoiceConfiguration) {

        Element categoriesTag = createUAInvoiceCategories(doc, invoice, userAccount, ratedTransactions, isVirtual, invoiceConfiguration);
        if (categoriesTag == null) {
            return null;
        }

        Element userAccountTag = doc.createElement("userAccount");

        if (userAccount == null) {
            userAccountTag.setAttribute("description", "-");
        } else {
            userAccountTag.setAttribute("id", userAccount.getId() + "");
            userAccountTag.setAttribute("code", userAccount.getCode());
            userAccountTag.setAttribute("jobTitle", getDefaultIfNull(userAccount.getJobTitle(), ""));
            userAccountTag.setAttribute("description", getDefaultIfNull(userAccount.getDescription(), ""));
            userAccountTag.setAttribute("registrationNo", getDefaultIfNull(userAccount.getRegistrationNo(), ""));
            userAccountTag.setAttribute("vatNo", getDefaultIfNull(userAccount.getVatNo(), ""));

            addCustomFields(userAccount, doc, userAccountTag);
        }

        if (invoiceConfiguration.isDisplaySubscriptions()) {
            Element subscriptionsTag = createSubscriptionsSection(doc, invoice, userAccount, ratedTransactions, isVirtual, ignoreUA, invoiceConfiguration);
            if (subscriptionsTag != null) {
                userAccountTag.appendChild(subscriptionsTag);
            }
        }
        if (userAccount != null) {
            Element nameTag = createNameSection(doc, userAccount, invoiceLanguageCode);
            if (nameTag != null) {
                userAccountTag.appendChild(nameTag);
            }

            Element addressTag = createAddressSection(doc, userAccount, invoiceLanguageCode);
            if (addressTag != null) {
                userAccountTag.appendChild(addressTag);
            }
        }

        userAccountTag.appendChild(categoriesTag);

        return userAccountTag;
    }

    /**
     * Create invoice/details/userAccounts/userAccount/categories/category/subcategories/subcategory/line DOM element
     *
     * @param doc                   XML invoice DOM
     * @param invoice               Invoice to convert
     * @param ratedTransaction      Rated transaction
     * @param invoiceDateFormat     Date format
     * @param invoiceDateTimeFormat Timestamp format
     * @param invoiceConfiguration  Invoice configuration
     * @param invoiceLanguageCode   Invoice language - language code
     * @return DOM element
     */
    protected Element createRTSection(Document doc, Invoice invoice, RatedTransaction ratedTransaction, String invoiceDateFormat, String invoiceDateTimeFormat, InvoiceConfiguration invoiceConfiguration,
                                      String invoiceLanguageCode) {

        Element line = doc.createElement("line");

        Date periodStartDateRT = ratedTransaction.getStartDate();
        Date periodEndDateRT = ratedTransaction.getEndDate();

        Element lebel = doc.createElement("label");
        lebel.appendChild(doc.createTextNode(getDefaultIfNull(ratedTransaction.getDescription(), "")));
        line.appendChild(lebel);
        line.setAttribute("periodEndDate", formatDateWithPattern(DateUtils.addDaysToDate(periodEndDateRT, -1), invoiceDateFormat));
        line.setAttribute("periodStartDate", formatDateWithPattern(periodStartDateRT, invoiceDateFormat));
        Element usageDate = doc.createElement("usageDate");
        Text usageDateTxt = doc.createTextNode(formatDateWithPattern(ratedTransaction.getUsageDate(), invoiceDateFormat));
        usageDate.appendChild(usageDateTxt);
        line.appendChild(usageDate);

        Element callsElement = doc.createElement("calls");
        String calls = ratedTransaction.getParameter1();
        Text callsTxt = doc.createTextNode(Utils.isInteger(calls) ? calls : "");
        callsElement.appendChild(callsTxt);
        line.appendChild(callsElement);

        Element quantityElement = doc.createElement("quantity");
        BigDecimal quantity = ratedTransaction.getQuantity();
        if (quantity != null) {
            quantity = (quantity.setScale(0, getRoundingMode())).abs();
        }
        Text quantityTxt = doc.createTextNode(quantity != null ? quantity.toPlainString() : "");
        quantityElement.appendChild(quantityTxt);
        line.appendChild(quantityElement);

        line.setAttribute("taxPercent", toPlainString(ratedTransaction.getTaxPercent()));
        BigDecimal amountWithoutTax = ratedTransaction.getAmountWithoutTax();
        if (ratedTransaction.getQuantity() != null && ratedTransaction.getQuantity().compareTo(BigDecimal.ZERO) != 0) {
            amountWithoutTax = amountWithoutTax.divide(ratedTransaction.getQuantity(), getScale(), getRoundingMode());
        }
        Element lineAmountWithoutTax = doc.createElement("amountWithoutTax");
        lineAmountWithoutTax.appendChild(doc.createTextNode(toPlainString(amountWithoutTax)));
        line.appendChild(lineAmountWithoutTax);
        Element lineTotalAmountWithoutTax = doc.createElement("totalAmountWithoutTax");
        lineTotalAmountWithoutTax.appendChild(doc.createTextNode(toPlainString(ratedTransaction.getAmountWithoutTax())));
        line.appendChild(lineTotalAmountWithoutTax);
        addCustomFields(ratedTransaction, doc, line);
        return line;
    }

    /**
     * Create invoice/amount DOM element
     *
     * @param doc                  XML invoice DOM
     * @param invoice              Invoice to convert
     * @param invoiceConfiguration Invoice configuration
     * @return DOM element
     */
    protected Element createAmountSection(Document doc, Invoice invoice, InvoiceConfiguration invoiceConfiguration) {

        Element amount = doc.createElement("amount");
        Element currency = doc.createElement("currency");
        Text currencyTxt = doc.createTextNode(invoice.getBillingAccount().getCustomerAccount().getTradingCurrency().getCurrencyCode());
        currency.appendChild(currencyTxt);
        amount.appendChild(currency);

        Element amountWithoutTax = doc.createElement("amountWithoutTax");

        amountWithoutTax.appendChild(doc.createTextNode(toPlainString(invoice.getAmountWithoutTax())));
        amount.appendChild(amountWithoutTax);

        Element amountWithTax = doc.createElement("amountWithTax");
        Text amountWithTaxTxt = doc.createTextNode(toPlainString(invoice.getAmountWithTax()));
        amountWithTax.appendChild(amountWithTaxTxt);
        amount.appendChild(amountWithTax);

        Element balance = doc.createElement("balance");
        balance.appendChild(doc.createTextNode(toPlainString(invoice.getDueBalance())));
        amount.appendChild(balance);

        Element netToPayElement = doc.createElement("netToPay");
        netToPayElement.appendChild(doc.createTextNode(toPlainString(invoice.getNetToPay())));
        amount.appendChild(netToPayElement);

        Element taxes = createAmountTaxSection(doc, invoice, invoiceConfiguration);
        if (taxes != null) {
            amount.appendChild(taxes);
        }

        return amount;
    }

    /**
     * Get category invoice aggregate description
     *
     * @param code the cf code
     * @return the cf description
     */
    private String getCategoryInvoiceAggregateDescription(String code) {
        String description = null;
        if (!StringUtils.isBlank(code)) {
            description = code;
            CustomFieldTemplate cft = customFieldTemplateService.findByCodeAndAppliesToNoCache("CATEGORY_INVOICE_AGGREGATE", "InvoiceCategory");
            if (cft != null && cft.getListValues() != null && !StringUtils.isBlank(cft.getListValues().get(code))) {
                description = cft.getListValues().get(code);
            }
        }
        return description;
    }

    /**
     * Format date to a given pattern
     *
     * @param value   Date to format
     * @param pattern Pattern to format to
     * @return A date as a string
     */
    public static String formatDateWithPattern(Date value, String pattern) {
        if (value == null) {
            return "";
        }
        String result = null;
        SimpleDateFormat sdf = new SimpleDateFormat(pattern, Locale.ENGLISH);
        try {
            result = sdf.format(value);
        } catch (Exception e) {
            result = "";
        }
        return result;
    }

    /**
     * Get BigDecimal as a string
     *
     * @param bigDecimal
     * @return A null-safe Plain String value of the bigDecimal
     */
    private String toPlainString(BigDecimal bigDecimal) {
        DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
        decimalFormatSymbols.setDecimalSeparator('.');
        decimalFormatSymbols.setGroupingSeparator(',');
        return bigDecimal != null ? new DecimalFormat("#,##0.00", decimalFormatSymbols).format(round(bigDecimal)) : round(BigDecimal.ZERO).toPlainString();
    }

    /**
     * Get a scale
     *
     * @return the scale.
     */
    private int getScale() {
        int scale = 2;
        if (appProvider.getInvoiceRounding() > 0) {
            scale = appProvider.getInvoiceRounding();
        }
        return scale;
    }

    /**
     * Get rounding mode
     *
     * @return the rounding mode
     */
    private RoundingMode getRoundingMode() {
        RoundingMode roundingMode = RoundingModeEnum.NEAREST.getRoundingMode();
        if (appProvider.getInvoiceRoundingMode() != null && appProvider.getInvoiceRoundingMode().getRoundingMode() != null) {
            roundingMode = appProvider.getInvoiceRoundingMode().getRoundingMode();
        }
        return roundingMode;
    }

    /**
     * Round a number
     *
     * @param amount the amount
     * @return the rounded number
     */
    private BigDecimal round(BigDecimal amount) {
        if (amount == null) {
            return null;
        }

        amount = amount.setScale(getScale(), getRoundingMode());
        return amount;
    }

    /**
     * Determine tax category from a billing account
     *
     * @param billingAccount Billing account
     * @return Tax category
     */
    private TaxCategory getTaxCategory(BillingAccount billingAccount) {
        TaxCategory taxCategory = null;
        if (billingAccount != null) {
            taxCategory = billingAccount.getTaxCategoryResolved();
            if (taxCategory == null) {
                taxCategory = billingAccount.getTaxCategory();
                if (taxCategory == null) {
                    taxCategory = billingAccount.getCustomerAccount().getCustomer().getCustomerCategory().getTaxCategory();
                }
            }
        }
        return taxCategory;
    }

    public class XMLInvoiceCategoryDTO extends XMLInvoiceHeaderCategoryDTO {
        String categoryInvoiceAggregate;
        BigDecimal taxPercent;

        public XMLInvoiceCategoryDTO() {
            this.setAmountWithoutTax(BigDecimal.ZERO);
            this.taxPercent = BigDecimal.ZERO;
            this.setAmountTax(BigDecimal.ZERO);
            this.setAmountWithTax(BigDecimal.ZERO);
        }

        /**
         * Gets the categoryInvoiceAggregate
         *
         * @return the categoryInvoiceAggregate
         */
        public String getCategoryInvoiceAggregate() {
            return categoryInvoiceAggregate;
        }

        /**
         * Sets the categoryInvoiceAggregate.
         *
         * @param categoryInvoiceAggregate the categoryInvoiceAggregate
         */
        public void setCategoryInvoiceAggregate(String categoryInvoiceAggregate) {
            this.categoryInvoiceAggregate = categoryInvoiceAggregate;
        }

        /**
         * Gets the taxPercent
         *
         * @return the taxPercent
         */
        public BigDecimal getTaxPercent() {
            return taxPercent;
        }

        /**
         * Sets the taxPercent.
         *
         * @param taxPercent the taxPercent
         */
        public void setTaxPercent(BigDecimal taxPercent) {
            this.taxPercent = taxPercent;
        }
    }
}