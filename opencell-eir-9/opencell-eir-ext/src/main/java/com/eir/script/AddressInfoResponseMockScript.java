package com.eir.script;

import java.util.Map;

import org.meveo.admin.exception.BusinessException;
import org.meveo.model.notification.InboundRequest;
import org.meveo.service.script.Script;

import javax.ejb.Stateless;

/**
 * @author Abdellatif BARI
 */
@Stateless
public class AddressInfoResponseMockScript extends Script {

    @Override
    public void execute(Map<String, Object> context) throws BusinessException {

        InboundRequest inboundRequest = (InboundRequest) context.get("event");
        try {

            if (inboundRequest == null) {
                throw new BusinessException("Missing event.");
            }

            String body = inboundRequest.getBody();

            buildSuccessResponse(inboundRequest);

        } catch (Exception e) {
            log.error("Address info response mock script fail", e);
            buildFailureResponse(inboundRequest, e.getMessage());
        }
    }

    /**
     * Build failure response
     *
     * @param inboundRequest  inbound request
     * @param responseMessage response message
     */
    private void buildFailureResponse(InboundRequest inboundRequest, String responseMessage) {
        inboundRequest.setResponseContentType("application/xml");
        inboundRequest.setResponseBody("<?xml version=\"1.0\" encoding=\"utf-8\"?><error>" + responseMessage + "</error>");
        inboundRequest.setResponseStatus(500);
    }

    /**
     * Build success response
     *
     * @param inboundRequest inbound request
     */
    private void buildSuccessResponse(InboundRequest inboundRequest) {
        inboundRequest.setResponseContentType("application/xml");
        inboundRequest.setResponseBody("<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"
                + "<Address_Info_Response xsi:noNamespaceSchemaLocation=\"schema.xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\">\n" + "   <Address_Info>\n"
                + "      <Address>\n" + "         <Unit>\n" + "                <Address_Unit_Id>201090</Address_Unit_Id>\n" + "        <Unit_Number>2</Unit_Number>\n"
                + "        <Unit_Name>BASEMENT_OFFICE</Unit_Name>\n" + "                <Unit_Type>BUSINESS</Unit_Type>\n" + "                <Floor_Number></Floor_Number>\n"
                + "                <Eircode>A94A2X4</Eircode>\n" + "         </Unit>\n" + "         <Building>\n" + "                <Building_Number>102A</Building_Number>\n"
                + "                <From_Building_Number>102</From_Building_Number>\n" + "        <To_Building_Number>106</To_Building_Number>\n"
                + "        <Building_Name>BANK HOUSE</Building_Name>\n" + "         </Building>\n" + "       <Street>\n" + "                <Street_Id>80156294</Street_Id>\n"
                + "            <Street_Name>CLANBRASSIL ST LR</Street_Name>\n" + "        <Street_Suffix>STREET</Street_Suffix>\n"
                + "        <Directional_Qualifier_Name>LOWER</Directional_Qualifier_Name>\n" + "        <Geographical_Qualifier_Name>WEST</Geographical_Qualifier_Name>\n"
                + "        <Townland_Name></Townland_Name>\n" + "         </Street>\n" + "         <County>\n" + "                <County_Id>26</County_Id>\n"
                + "                <County_Name>DUBLIN</County_Name>\n" + "        <Town_Name></Town_Name>\n" + "        <Postal_District>DUBLIN 8</Postal_District>\n"
                + "         </County>\n" + "      </Address>\n" + "   </Address_Info>\n" + "   <Address_Info>\n" + "      <Address>\n" + "         <Unit>\n"
                + "                <Address_Unit_Id>1903379</Address_Unit_Id>\n" + "        <Unit_Number></Unit_Number>\n" + "        <Unit_Name></Unit_Name>\n"
                + "                <Unit_Type></Unit_Type>\n" + "                <Floor_Number></Floor_Number>\n" + "                <Eircode>A94A2X4</Eircode>\n"
                + "         </Unit>\n" + "         <Building>\n" + "                <Building_Number>180</Building_Number>\n"
                + "                <From_Building_Number>180</From_Building_Number>\n" + "        <To_Building_Number>180</To_Building_Number>\n"
                + "        <Building_Name></Building_Name>\n" + "         </Building>\n" + "      <Street>\n" + "                <Street_Id>5011758</Street_Id>\n"
                + "                <Street_Name>ARD MCCOOL ESTATE</Street_Name>\n" + "        <Street_Suffix>ESTATE</Street_Suffix>\n"
                + "        <Directional_Qualifier_Name></Directional_Qualifier_Name>\n" + "        <Geographical_Qualifier_Name></Geographical_Qualifier_Name>\n"
                + "        <Townland_Name>STRANORLAR</Townland_Name>\n" + "         </Street>\n" + "         <County>\n" + "                <County_Id>9</County_Id>\n"
                + "                <County_Name>DONEGAL</County_Name>\n" + "        <Town_Name></Town_Name>\n" + "        <Postal_District>BALLYBOFEY</Postal_District>\n"
                + "         </County>\n" + "      </Address>\n" + "   </Address_Info>\n" + "</Address_Info_Response>");
        inboundRequest.setResponseStatus(200);
    }

}