package com.eir.script;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.NoChargeException;
import org.meveo.admin.exception.NoPricePlanException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.api.dto.billing.TimePeriodDto;
import org.meveo.api.dto.billing.UsageRatingDto;
import org.meveo.model.DatePeriod;
import org.meveo.model.billing.WalletOperation;
import org.meveo.model.catalog.Calendar;
import org.meveo.model.rating.EDR;
import org.meveo.model.shared.DateUtils;
import org.meveo.model.shared.DateUtils.CalendarSplit;
import org.meveo.model.shared.DateUtils.DatePeriodSplit;
import org.meveo.service.base.PersistenceService;
import org.meveo.service.billing.impl.AccountingCodeService;
import org.meveo.service.catalog.impl.CalendarService;
import org.meveo.service.catalog.impl.InvoiceSubCategoryService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.script.Script;
import org.meveo.service.tax.TaxClassService;
import org.meveo.service.tax.TaxMappingService;
import org.meveo.service.tax.TaxMappingService.TaxInfo;
import org.primefaces.model.SortOrder;

/**
 * Usage Rating Script
 * 
 * @author Mohammed Amine TAZI
 *
 */
@Stateless
public class UsageRatingScript extends Script {

    private static final long serialVersionUID = -4939386032505010517L;

    @Inject
    private CustomTableService customTableService;

    @Inject
    private CalendarService calendarService;

    @Inject
    private InvoiceSubCategoryService invoiceSubCategoryService;

    @Inject
    private AccountingCodeService accountingCodeService;

    @Inject
    private TaxClassService taxClassService;

    @Inject
    private TaxMappingService taxMappingService;

    @Override
    public void execute(Map<String, Object> initContext) throws BusinessException {

        final WalletOperation wo = (WalletOperation) initContext.get(Script.CONTEXT_ENTITY);
        UsageRatingDto usageRatingDto = initUsageRatingDto(wo);

        determineTrafficClass(usageRatingDto, wo.getEdr());
        identifyRatePlan(usageRatingDto);
        identifyTimePeriods(usageRatingDto);
        
        if(wo.getInputQuantity().longValue() < 1000 ) {
            usageRatingDto.setInputQuantity(new BigDecimal(0));
        }

        calculateCallCharge(usageRatingDto);
        applyTax(usageRatingDto, wo);
        storeData(usageRatingDto, wo);
        updateInvoicingDate(wo);
    }

    /**
     * Set invoicing date of Wallet operation to a billing account's billing cycle calendar's period end date
     * @param wo Wallet operation
     */
    private void updateInvoicingDate(WalletOperation wo) {
        Calendar invoicingCalendar = CalendarService.initializeCalendar(wo.getBillingAccount().getBillingCycle().getCalendar(), wo.getServiceInstance().getSubscriptionDate(), wo.getChargeInstance());
        Date invoicingDate = invoicingCalendar.nextCalendarDate(wo.getOperationDate());
        wo.setInvoicingDate(invoicingDate);        
    }

    private UsageRatingDto initUsageRatingDto(WalletOperation wo) {
        UsageRatingDto usageRatingDto = new UsageRatingDto();
        usageRatingDto.setCustomerId(wo.getBillingAccount().getCustomerAccount().getCustomer().getId());
        if(wo.getInputQuantity().longValue() > 1000 ) {
            usageRatingDto.setInputQuantity(wo.getEdr().getQuantity().divide(new BigDecimal(1000)));
        } else {
            usageRatingDto.setInputQuantity(new BigDecimal(1));
        }
        usageRatingDto.setEventDate(wo.getEdr().getEventDate());
        usageRatingDto.setInitDate(wo.getSubscriptionDate());
        return usageRatingDto;
    }

    /**
     * Determine traffic class based on the FEE CODE, CALL TYPE, CATEGORY OF SERVICE and B_NUMBER fields on the CDR Best Match strategy is applied if many traffic classes are found
     * 
     * @param usageRatingDto rating dto
     * @param edr edr
     */
    public void determineTrafficClass(UsageRatingDto usageRatingDto, EDR edr) {
        String feeCode = edr.getParameter3();
        String callType = edr.getParameter2();
        String serviceCategory = edr.getParameter7();
        String b_Number = edr.getParameter1();
                
        String bNumber_bestMatchQuery="fee_code IN ('"+ feeCode + "','%','*') AND call_type IN ('"+callType+"','%','*') AND id IN (SELECT id FROM eir_u_traffic_class WHERE '"+b_Number+"' "
        + "LIKE number_prefix || '%' and number_prefix != '%' and LENGTH(number_prefix)  = (SELECT MAX(LENGTH(number_prefix))FROM eir_u_traffic_class WHERE '"+b_Number+"' "
        + "LIKE number_prefix || '%'))";
        String bNumber_allMatchNonEmptyQuery = "fee_code IN ('"+feeCode + "','%','*') AND call_type IN ('"+callType+"','%','*') AND number_prefix='*'";
        String bNumber_allMatchQuery = "fee_code IN ('"+feeCode + "','%','*') AND call_type IN ('"+callType+"','%','*')  AND number_prefix='%'";
        Map<String, Object> searchCriteria = new HashMap<>();
        searchCriteria.put("minmaxOptionalRange valid_from valid_to", edr.getEventDate());
        searchCriteria.put(PersistenceService.SEARCH_SQL,bNumber_bestMatchQuery);

        //Searching b_Number by best match 
        List<Map<String, Object>> trafficClasses = customTableService.list("EIR_U_TRAFFIC_CLASS", new PaginationConfiguration(searchCriteria, null, null));      
        if (!trafficClasses.isEmpty()) {
            if(!calculateTrafficClassUsingServiceCategory(trafficClasses, serviceCategory, usageRatingDto, edr)) {
                if(!calculateTrafficClassUsingServiceCategory(trafficClasses, "*", usageRatingDto, edr)) {
                    if(!calculateTrafficClassUsingServiceCategory(trafficClasses, "%", usageRatingDto, edr)) {
                        throw new NoChargeException("No traffic class found for fee_code " + feeCode + ", call_type " + callType + ", service_category " + serviceCategory + ", b_Number " + b_Number + " event_date " + edr.getEventDate());
                    }
                }
            }
        } else {
            //Searching b_Number by * 
            searchCriteria.put(PersistenceService.SEARCH_SQL, bNumber_allMatchNonEmptyQuery);
            trafficClasses = customTableService.list("EIR_U_TRAFFIC_CLASS", new PaginationConfiguration(searchCriteria, null, null));      
            if (!trafficClasses.isEmpty()) {
                if(!calculateTrafficClassUsingServiceCategory(trafficClasses, serviceCategory, usageRatingDto, edr)) {
                    if(!calculateTrafficClassUsingServiceCategory(trafficClasses, "*", usageRatingDto, edr)) {
                        if(!calculateTrafficClassUsingServiceCategory(trafficClasses, "%", usageRatingDto, edr)) {
                            throw new NoChargeException("No traffic class found for fee_code " + feeCode + ", call_type " + callType + ", service_category %, b_Number " + b_Number + " event_date " + edr.getEventDate());
                        }
                    }
                }
            } else {
                //Searching b_Number by %
                searchCriteria.put(PersistenceService.SEARCH_SQL, bNumber_allMatchQuery);
                trafficClasses = customTableService.list("EIR_U_TRAFFIC_CLASS", new PaginationConfiguration(searchCriteria, null, null));      
                if (! trafficClasses.isEmpty()) {
                    if(!calculateTrafficClassUsingServiceCategory(trafficClasses, serviceCategory, usageRatingDto, edr)) {
                        if(!calculateTrafficClassUsingServiceCategory(trafficClasses, "*", usageRatingDto, edr)) {
                            if(!calculateTrafficClassUsingServiceCategory(trafficClasses, "%", usageRatingDto, edr)) {
                                throw new NoChargeException("No traffic class found for fee_code " + feeCode + ", call_type " + callType + ", service_category *, b_Number " + b_Number + " event_date " + edr.getEventDate());
                            }
                        }
                    }   
                }
            }
        }  
        
        if(usageRatingDto.getTrafficClassId() == null) {
            throw new NoChargeException("No traffic class found for fee_code " + feeCode + ", call_type " + callType + ", service_category " + serviceCategory + ", b_Number " + b_Number + " event_date " + edr.getEventDate());
        }
            
    }
    
    /**
     * Return adequate traffic class using service category
     * @param trafficClasses Traffic classes
     * @param serviceCategory service category
     * @param usageRatingDto Usage rating
     * @return if traffic class is good to use
     */
    private Boolean calculateTrafficClassUsingServiceCategory(List<Map<String,Object>> trafficClasses, String serviceCategory, UsageRatingDto usageRatingDto, EDR edr) {
        Long count = trafficClasses.stream().filter(trafficClass-> trafficClass.get("service_category") != null && serviceCategory.equalsIgnoreCase((String)trafficClass.get("service_category"))).count();
        if(count == 1) {
            Map<String, Object> trafficClass = trafficClasses.stream()
                    .filter(tc-> tc.get("service_category") != null  && serviceCategory.equalsIgnoreCase((String)tc.get("service_category")))
                    .findAny()
                    .get();
            usageRatingDto.setTrafficClassId(toLong(trafficClass.get("id")));
            usageRatingDto.setTrafficClassCode((String) trafficClass.get("code"));
            usageRatingDto.setTrafficClassDescription((String) trafficClass.get("description"));
            return true;
        } else if (count > 1) {
            throw new NoChargeException("Many traffic classes found for fee_code " + edr.getParameter3()  + " callType " + edr.getParameter2() + " service_category "
                    + serviceCategory + ", b_Number " + edr.getParameter1() + " event_date " + edr.getEventDate());
        } else {
            return false;
        }
        

    }

    /**
     * Identify active rate version based on the CALL DATE and CALL TIME fields on the CDR
     * 
     * @param usageRatingDto rating dto
     */
    @SuppressWarnings("static-access")
    public void identifyRatePlan(UsageRatingDto usageRatingDto) {
        Map<String, Object> searchCriteria = Map.of("customer_id", usageRatingDto.getCustomerId(), "minmaxOptionalRange valid_from valid_to", usageRatingDto.getEventDate());
        List<Map<String, Object>> tariffPlanMaps = customTableService.list("EIR_U_TARIFF_PLAN_MAP", new PaginationConfiguration(searchCriteria, null, null));
        if (tariffPlanMaps.isEmpty()) {
            throw new NoChargeException("No tariff plan Map found for customer id " + usageRatingDto.getCustomerId() + ", eventDate " + usageRatingDto.getEventDate());
        }
        usageRatingDto.setTariffPlanId(toLong(tariffPlanMaps.get(0).get("rate_plan_id")));

        searchCriteria = Map.of("id", usageRatingDto.getTariffPlanId());
        List<Map<String, Object>> tariffPlans = customTableService.list("EIR_U_TARIFF_PLAN", new PaginationConfiguration(searchCriteria, null, null));
        if (tariffPlans.isEmpty()) {
            throw new NoChargeException("No tariff plan found for tariff plan id " + usageRatingDto.getTariffPlanId());
        }
        usageRatingDto.setTimeBandId(toLong(tariffPlans.get(0).get("time_band")));
        usageRatingDto.setRounding(toLong(tariffPlans.get(0).get("rounding")));
        usageRatingDto.setRoundingMode(usageRatingDto.getRoundingMode().valueOf(tariffPlans.get(0).get("rounding_mode").toString()));
        usageRatingDto.setSplit(Boolean.parseBoolean(tariffPlans.get(0).get("split").toString()));
    }

    /**
     * When 'Split Rating' option has been selected, Identify adequate time periods based on the CALL DATE and CALL TIME fields on the CDR
     * 
     * @param usageRatingDto rating dto
     */
    private void identifyTimePeriods(UsageRatingDto usageRatingDto) {
        Long calendarId = null;
        Map<Long, Long> timePeriodPerCalendar = new HashMap<>();
        Map<Long, String> timePeriodPerCode = new HashMap<>();
        List<CalendarSplit> calendarSplit = new ArrayList<>();
        DatePeriod datePeriod = new DatePeriod();
        datePeriod.setFrom(usageRatingDto.getEventDate());
        datePeriod.setTo(Date.from(usageRatingDto.getEventDate().toInstant().plus(Duration.ofSeconds(usageRatingDto.getInputQuantity().longValue()))));

        Map<String, Object> searchCriteria = Map.of("time_band", usageRatingDto.getTimeBandId());
        List<Map<String, Object>> timePeriods = customTableService.list("EIR_U_TIME_PERIOD", new PaginationConfiguration(searchCriteria, "priority", SortOrder.ASCENDING));
        if (timePeriods.isEmpty()) {
            throw new NoChargeException("No time period found for timeband " + usageRatingDto.getTimeBandId());
        }
        timePeriods.stream().forEach(timePeriod -> {
            Calendar calendar = calendarService.findById(toLong(timePeriod.get("calendar_id")));
            Long priority = toLong(timePeriod.get("priority"));
            calendarSplit.add(new CalendarSplit(calendar, priority.intValue(), calendar.getId()));
            timePeriodPerCalendar.put(toLong(timePeriod.get("calendar_id")), toLong(timePeriod.get("id")));
            timePeriodPerCode.put(toLong(timePeriod.get("calendar_id")), timePeriod.get("code").toString());
        });
        usageRatingDto.setTimePeriodPerCalendar(timePeriodPerCalendar);
        List<DatePeriodSplit> datePeriodSplit = DateUtils.splitDatePeriodByCalendars(datePeriod, usageRatingDto.getInitDate(), calendarSplit.toArray(new CalendarSplit[] {}));
        if (datePeriodSplit == null || datePeriodSplit.isEmpty()) {
            throw new NoChargeException("No valid time periods found after splitting period " + datePeriod.toString(DateUtils.DATE_TIME_PATTERN) + " with init date of "
                    + DateUtils.formatAsTime(usageRatingDto.getInitDate()) + " by calendars " + calendarSplit);
        }
        List<TimePeriodDto> timePeriodsDto = new ArrayList<>();
        TimePeriodDto timePeriodDto = null;
        for (DatePeriodSplit splittedDatePeriod : datePeriodSplit) {
            calendarId = (Long) splittedDatePeriod.getValue();
            timePeriodDto = new TimePeriodDto();

            timePeriodDto.setCalendarId(calendarId);
            timePeriodDto.setTimePeriodId(timePeriodPerCalendar.get(calendarId));
            timePeriodDto.setTimePeriodCode(timePeriodPerCode.get(calendarId));
            if (usageRatingDto.isSplit()) {
                timePeriodDto.setTimePeriodStart(splittedDatePeriod.getPeriod().getFrom());
                timePeriodDto.setTimePeriodEnd(splittedDatePeriod.getPeriod().getTo());
                timePeriodDto.calculateCallDuration();
                timePeriodsDto.add(timePeriodDto);

            } else {
                timePeriodDto.setTimePeriodStart(datePeriod.getFrom());
                timePeriodDto.setTimePeriodEnd(datePeriod.getTo());
                timePeriodDto.calculateCallDuration();
                timePeriodsDto.add(timePeriodDto);
                break;
            }
        }
        usageRatingDto.setTimePeriods(timePeriodsDto);
    }

    /**
     * Calculate call charge (Summing the connection charge with total usage charges taking into consideration the configured min charge)
     * 
     * @param usageRatingDto rating dto
     */
    public void calculateCallCharge(UsageRatingDto usageRatingDto) {
        BigDecimal totalCharge = null;
        BigDecimal usageCharge = null;
        
        Map<String, Object> searchCriteria = Map.of(
            "rate_plan_id", usageRatingDto.getTariffPlanId(), 
            "inList time_period_id", usageRatingDto.getTimePeriodPerCalendar().values(), 
            "minmaxOptionalRange valid_from valid_to", usageRatingDto.getEventDate(),
            PersistenceService.SEARCH_SQL, "traffic_class_id in (select id from eir_u_traffic_class where code = '" + usageRatingDto.getTrafficClassCode() + "')");

        List<Map<String, Object>> tariffs = customTableService.list("EIR_U_TARIFF", new PaginationConfiguration(searchCriteria, null, null));
        if (tariffs.isEmpty()) {
            throw new NoPricePlanException("No tariff found for rate plan " + usageRatingDto.getTariffPlanId() + ", traffic class " + usageRatingDto.getTrafficClassId() + ", event date " + usageRatingDto.getEventDate());
        }

        getCallInfo(usageRatingDto, tariffs);
        usageCharge = calculateUsageCharge(usageRatingDto, tariffs);

        totalCharge = usageRatingDto.getConnectionCharge().add(usageCharge).max(usageRatingDto.getMinCharge());
        totalCharge = round(usageRatingDto, totalCharge);
        usageRatingDto.setTotalCharge(totalCharge);
    }

    /**
     * Get needed call information before calculating usage and total charges (min charge to apply, connection charge, min duration)
     * 
     * @param usageRatingDto rating dto
     * @param tariffs tariffs to be applied
     */
    private void getCallInfo(UsageRatingDto usageRatingDto, List<Map<String, Object>> tariffs) {
        BigDecimal minCharge = null;
        BigDecimal connectionCharge = null;
        Long minDuration = null;
        Long invoiceSubCategoryId = null;
        Long accountingCodeId = null;
        Long taxClassId = null;
        for (TimePeriodDto timePeriodDto : usageRatingDto.getTimePeriods()) {
            Map<String, Object> tariff = getTariffByTimePeriod(timePeriodDto.getTimePeriodId(), tariffs);
            if(tariff == null) {
                continue;
            }
            minCharge = (minCharge == null) ? (BigDecimal) tariff.get("min_charge") : minCharge;
            connectionCharge = (connectionCharge == null) ? (BigDecimal) tariff.get("connection_charge") : connectionCharge;
            minDuration = (minDuration == null) ? toLong(tariff.get("min_duration")) : minDuration;
            invoiceSubCategoryId = (invoiceSubCategoryId == null) ? toLong(tariff.get("invoice_subcat_id")) : invoiceSubCategoryId;
            accountingCodeId = (accountingCodeId == null) ? toLong(tariff.get("accounting_code_id")) : accountingCodeId;
            taxClassId = (taxClassId == null) ? toLong(tariff.get("tax_class")) : taxClassId;
        }
        if(connectionCharge == null) {
            throw new NoPricePlanException("No tariff found for rate plan " + usageRatingDto.getTariffPlanId() + ", traffic class " + usageRatingDto.getTrafficClassId()
                + ", event date " + usageRatingDto.getEventDate() + " , time periods : " + usageRatingDto.getTimePeriods().stream().map(tp->tp.getTimePeriodCode()).collect(Collectors.joining()));
        }
        usageRatingDto.setMinCharge(round(usageRatingDto, minCharge));
        usageRatingDto.setConnectionCharge(round(usageRatingDto, connectionCharge));
        usageRatingDto.setMinDuration(minDuration);
        usageRatingDto.setInvoiceSubCategoryId(invoiceSubCategoryId);
        usageRatingDto.setAccountingCodeId(accountingCodeId);
        usageRatingDto.setTaxClassId(taxClassId);

        Long delta = minDuration - usageRatingDto.getInputQuantity().longValue();
        if (delta > 0) {
            Long lastPeriodCall = usageRatingDto.getTimePeriods().get(usageRatingDto.getTimePeriods().size() - 1).getCallDuration() + delta;
            usageRatingDto.getTimePeriods().get(usageRatingDto.getTimePeriods().size() - 1).setCallDuration(lastPeriodCall);
            usageRatingDto.setQuantity(new BigDecimal(minDuration));
        } else {
            usageRatingDto.setQuantity(usageRatingDto.getInputQuantity());
        }
    }

    /**
     * Calculate Call usage charge by calculating and summing the usage charge for each time period
     * 
     * @param usageRatingDto rating dto
     * @param tariffs tariffs to be applied
     * @return Total call usage charge
     */
    private BigDecimal calculateUsageCharge(UsageRatingDto usageRatingDto, List<Map<String, Object>> tariffs) {
        BigDecimal totalCharge = new BigDecimal(0);
        BigDecimal calculatedCharge = new BigDecimal(0);
        BigDecimal usageCharge = new BigDecimal(0);
        Long durationRounding = null;
        Long unit = null;
        Long callDuration = null;
        for (TimePeriodDto timePeriodDto : usageRatingDto.getTimePeriods()) {
            Map<String, Object> tariff = getTariffByTimePeriod(timePeriodDto.getTimePeriodId(), tariffs);
            if(tariff == null) {
                continue;
            }
            usageCharge = (BigDecimal) tariff.get("usage_charge");
            durationRounding = toLong(tariff.get("duration_rounding"));
            unit = toLong(tariff.get("unit"));
            if(unit == 0) {
                throw new NoChargeException("Unit value should not be zero in tariff configuration ! ");
            }
            callDuration = timePeriodDto.getCallDuration();

            callDuration = callDuration % unit == 0 ? callDuration : unit * (callDuration / unit) + durationRounding;
            calculatedCharge = new BigDecimal(callDuration).multiply(usageCharge).divide(new BigDecimal(unit), usageRatingDto.getRounding().intValue(), getRoundingMode(usageRatingDto));
            calculatedCharge = round(usageRatingDto, calculatedCharge);
            totalCharge = totalCharge.add(calculatedCharge);

            timePeriodDto.setTimePeriodAmount(calculatedCharge);
        }
        return totalCharge;
    }

    /**
     * Determine and apply relevant Tax to current amount
     * 
     * @param usageRatingDto rating dto
     * @param wo wallet operation
     */
    public void applyTax(UsageRatingDto usageRatingDto, WalletOperation wo) {
        wo.setInvoiceSubCategory(invoiceSubCategoryService.findById(usageRatingDto.getInvoiceSubCategoryId()));
        wo.setAccountingCode(accountingCodeService.findById(usageRatingDto.getAccountingCodeId()));
        wo.setOperationDate(usageRatingDto.getEventDate());
        wo.setQuantity(usageRatingDto.getQuantity());
        wo.setInputQuantity(usageRatingDto.getQuantity());
        wo.setTaxClass(taxClassService.findById(usageRatingDto.getTaxClassId()));
        wo.setAmountWithoutTax(usageRatingDto.getTotalCharge());
        if(usageRatingDto.getQuantity().longValue() != 0) {
            wo.setUnitAmountWithoutTax(usageRatingDto.getTotalCharge().divide(usageRatingDto.getQuantity(), 4, RoundingMode.HALF_UP));
        } else {
            wo.setUnitAmountWithoutTax(usageRatingDto.getTotalCharge());
        }
        TaxInfo taxInfo = taxMappingService.determineTax(wo.getTaxClass(), wo.getSeller(), wo.getBillingAccount(), wo.getWallet().getUserAccount(), wo.getOperationDate(), true, false);
        wo.setTax(taxInfo.tax);
        wo.setTaxPercent(taxInfo.tax.getPercent());

        wo.setUnitAmountWithTax(wo.getUnitAmountWithoutTax().multiply(wo.getTaxPercent()).divide(new BigDecimal(100)).add(wo.getUnitAmountWithoutTax()));
        wo.setAmountWithTax(wo.getAmountWithoutTax().multiply(wo.getTaxPercent()).divide(new BigDecimal(100)).add(wo.getAmountWithoutTax()));
        wo.setAmountTax(wo.getAmountWithoutTax().multiply(wo.getTaxPercent()).divide(new BigDecimal(100)));
    }

    /**
     * Store calculated and relevant data to Wallet operation
     * 
     * @param usageRatingDto rating dto
     * @param wo wallet operation
     */
    public void storeData(UsageRatingDto usageRatingDto, WalletOperation wo) {
        Map<String, String> callInformation = new HashMap<>();
        callInformation.put(usageRatingDto.getConnectionCharge().toString(), usageRatingDto.toString());
        wo.setCfValue("CALL_INFORMATION", callInformation);
        wo.setCfValue("TARIFF_PLAN_ID", usageRatingDto.getTariffPlanId());
    }

    /**
     * Find right tariff for a time period
     * 
     * @param timePeriodId Time period Id
     * @param tariffs List of tariffs
     * @return tariff concerning the time period
     */
    private Map<String, Object> getTariffByTimePeriod(Long timePeriodId, List<Map<String, Object>> tariffs) {
        return tariffs.stream().filter(tariff -> toLong(tariff.get("time_period_id")).equals(timePeriodId)).findFirst().orElse(null);
    
    }

    /**
     * Number to Long converter (By-pass DB conversion problem of type BIGINT)
     * 
     * @param number Big Integer number
     * @return Number converted to Long
     */
    private Long toLong(Object number) {
        if (number instanceof BigInteger) {
            return ((BigInteger) number).longValue();
        } else if (number instanceof String) {
            return Long.valueOf(number.toString());
        }
        return (Long) number;
    }

    /**
     * Round the charge amount according to rounding configuration in customer Rate plan
     * 
     * @param usageRatingDto rating dto
     * @param charge charge to be rounded
     * @return Rounded charge
     */
    private BigDecimal round(UsageRatingDto usageRatingDto, BigDecimal charge) {
        switch (usageRatingDto.getRoundingMode()) {
        case UP:
            charge = charge.setScale(usageRatingDto.getRounding().intValue(), RoundingMode.UP);
            break;
        case DOWN:
            charge = charge.setScale(usageRatingDto.getRounding().intValue(), RoundingMode.DOWN);
            break;
        case NONE:
            charge = charge.round(new MathContext(usageRatingDto.getRounding().intValue()));
        }
        return charge;
    }

    /**
     * Get rounding mode to use in BigDecimal division
     * 
     * @param usageRatingDto rating dto
     * @return Rounding mode to use
     */
    private RoundingMode getRoundingMode(UsageRatingDto usageRatingDto) {
        switch (usageRatingDto.getRoundingMode()) {
        case UP:
            return RoundingMode.UP;
        case DOWN:
            return RoundingMode.DOWN;
        case NONE:
            return RoundingMode.HALF_UP;
        }
        return RoundingMode.HALF_UP;
    }

}