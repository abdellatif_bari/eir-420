package com.eir.script;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.lang3.StringUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.model.billing.AccountingCode;
import org.meveo.model.billing.InvoiceSubCategory;
import org.meveo.model.catalog.RecurringChargeTemplate;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.shared.DateUtils;
import org.meveo.security.CurrentUser;
import org.meveo.security.MeveoUser;
import org.meveo.service.billing.impl.AccountingCodeService;
import org.meveo.service.catalog.impl.InvoiceSubCategoryService;
import org.meveo.service.catalog.impl.RecurringChargeTemplateService;
import org.meveo.service.catalog.impl.ServiceTemplateService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.script.Script;

/**
 * Script to import service definitions
 */
@Stateless
public class SlaImportScript extends Script {

    private static final long serialVersionUID = -3467788957890975696L;

    @Inject
    private CustomTableService customTableService;

    @Inject
    private AccountingCodeService accountingCodeService;

    @Inject
    private ServiceTemplateService serviceTemplateService;

    @Inject
    private RecurringChargeTemplateService recurringChargeTemplateService;

    @Inject
    private InvoiceSubCategoryService invoiceSubCategoryService;

    @Inject
    @CurrentUser
    protected MeveoUser currentUser;

    @SuppressWarnings("unchecked")
    @Override
    public void execute(Map<String, Object> initContext) throws BusinessException {

        Map<String, Object> line = (Map<String, Object>) initContext.get(Script.CONTEXT_RECORD);

        Map<String, Object> values = new HashMap<>();

        values.put("code", line.get("sla_code"));
        values.put("description", line.get("sla_description"));
        values.put("percent", line.get("percent"));
        values.put("valid_from", DateUtils.parseDate(line.get("valid_from")));
        values.put("valid_to", line.get("valid_to") != null ? DateUtils.parseDate(line.get("valid_to")) : null);
        values.put("updated", new Date());
        values.put("updated_by", currentUser.getUserName());

        if (values.get("valid_from") == null) {
            throw new ValidationException("Failed to parse date Valid from date " + line.get("valid_from"));
        }

        if (!StringUtils.isBlank((String) line.get("valid_to")) && values.get("valid_to") == null) {
            throw new ValidationException("Failed to parse date Valid to date " + line.get("valid_to"));
        }

        if (values.get("valid_to") != null && !((Date) values.get("valid_to")).after((Date) values.get("valid_from"))) {
            throw new ValidationException("Valid from date is after Valid to date");
        }

        AccountingCode accountingCode = accountingCodeService.findByCode((String) line.get("accounting_code"));
        if (accountingCode == null) {
            throw new ValidationException("AccountingCode '" + line.get("accounting_code") + "' not found");
        }
        values.put("accounting_code", accountingCode.getId());

        InvoiceSubCategory invoiceSubCategory = invoiceSubCategoryService.findByCode((String) line.get("invoice_sub_cat"));
        if (invoiceSubCategory == null) {
            throw new ValidationException("InvoiceSubCategory '" + line.get("invoice_sub_cat") + "' not found");
        }
        values.put("invoice_sub_cat", invoiceSubCategory.getId());

        RecurringChargeTemplate chargeTemplate = null;
        if (!StringUtils.isBlank((String) line.get("charge_code"))) {
            chargeTemplate = recurringChargeTemplateService.findByCode((String) line.get("charge_code"));
            if (chargeTemplate == null) {
                throw new ValidationException("RecurringChargeTemplate '" + line.get("charge_code") + "' not found");
            }
            values.put("charge_id", chargeTemplate.getId());
        }

        ServiceTemplate serviceTemplate = null;
        if (!StringUtils.isBlank((String) line.get("service_code"))) {
            serviceTemplate = serviceTemplateService.findByCode((String) line.get("service_code"));
            if (serviceTemplate == null) {
                throw new ValidationException("ServiceTemplate '" + line.get("service_code") + "' not found");
            }
            values.put("service_id", serviceTemplate.getId());
        }

        customTableService.create("eir_or_sla", values);

    }
}