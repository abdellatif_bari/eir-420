package com.eir.script.migration;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.meveo.service.script.Script;
import com.eir.service.migration.ServicesMigrationServiceAPI;

/**
 * Script for migrating EIR Services to OC
 * 
 * @author Mohammed Amine TAZI
 */
@Stateless
public class ServicesMigrationScript extends Script {

    private static final long serialVersionUID = -6288568811084129324L;

    @EJB
    private ServicesMigrationServiceAPI servicesMigrationServiceAPI;

    @SuppressWarnings("unchecked")
    @Override
    public void execute(Map<String, Object> initContext) {
        List<Map<String, Object>> records = null;
        if(initContext.get("record") instanceof List) {
            records = (List<Map<String, Object>>) initContext.get("record");
        } else {
            records = Arrays.asList((Map<String, Object>) initContext.get("record"));
        }

        servicesMigrationServiceAPI.migrateServices("ACCESS",records);
    }
}