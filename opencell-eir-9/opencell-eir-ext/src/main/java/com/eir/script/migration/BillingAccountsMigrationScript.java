package com.eir.script.migration;

import java.util.Date;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.collections.MapUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ElementNotFoundException;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.Auditable;
import org.meveo.model.billing.AccountStatusEnum;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.BillingCycle;
import org.meveo.model.billing.Country;
import org.meveo.model.billing.TradingCountry;
import org.meveo.model.billing.TradingLanguage;
import org.meveo.model.crm.Customer;
import org.meveo.model.crm.Provider;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.model.sequence.GenericSequence;
import org.meveo.model.shared.Address;
import org.meveo.model.shared.ContactInformation;
import org.meveo.model.shared.DateUtils;
import org.meveo.model.tax.TaxCategory;
import org.meveo.service.admin.impl.CountryService;
import org.meveo.service.billing.impl.BillingCycleService;
import org.meveo.service.billing.impl.TradingCountryService;
import org.meveo.service.billing.impl.TradingLanguageService;
import org.meveo.service.crm.impl.CustomerService;
import org.meveo.service.crm.impl.ProviderService;
import org.meveo.service.payments.impl.CustomerAccountService;
import org.meveo.service.script.Script;
import org.meveo.service.tax.TaxCategoryService;
import org.meveo.util.ApplicationProvider;

import com.eir.service.billing.impl.BillingAccountService;

/**
 * Script for migrating EIR billing accounts to OC
 * @author Hatim OUDAD
 */
@Stateless
public class BillingAccountsMigrationScript extends Script {

    private static final long serialVersionUID = -6288568811084129326L;

    @Inject
    private CustomerAccountService customerAccountService;
    
    @Inject
    private CustomerService customerService;
    
    @Inject
    private BillingAccountService billingAccountService;
    
    @Inject
    private CountryService countryService;
    
    @Inject
    private TaxCategoryService taxCategoryService;
    
    @Inject
    private BillingCycleService billingCycleService;
    
    @Inject
    private TradingLanguageService tradingLanguageService;
    
    @Inject
    private TradingCountryService tradingCountryService;
    
    @Inject
    private ProviderService providerService;
    
    @Inject
    @ApplicationProvider
    private Provider appProvider;

    
    @Override
    public void execute(Map<String, Object> initContext) {
        try {
            @SuppressWarnings("unchecked")
            Map<String, Object> record = (Map<String, Object>) initContext.get("record");
           
            if(MapUtils.isEmpty(record)) {
                throw new BusinessException("Empty billing account record!");
            }
            
            
            String customerCode = (String) record.get("CUSTOMER_CODE");
            String customerAccountCode = (String) record.get("CUSTOMER_ACCOUNT_CODE");
            String billingAccountCode = (String) record.get("BILLING_ACCOUNT_CODE");
            String billingAccountNumber = (String) record.get("BILLING_ACCOUNT_NUMBER");
            String billingAccountName = (String) record.get("BILLING_ACCOUNT_NAME");
            String address1 = (String) record.get("ADDRESS_1");
            String address2 = (String) record.get("ADDRESS_2");
            String address3 = (String) record.get("ADDRESS_3");
            String city = (String) record.get("ADDRESS_CITY");
            String zipCode = (String) record.get("ADDRESS_ZIPCODE");
            String countryName = (String) record.get("ADDRESS_COUNTRY");
            String status = (String) record.get("STATUS");
            String oaoId = (String) record.get("OAO_ID");
            String taxCategoryCode = (String) record.get("TAX_CATEGORY");
            String serviceType = (String) record.get("SERVICE_TYPE");
            String billingCycleCode = (String) record.get("BILLING_CYCLE");
            String lastBill= (String)record.get("BILLDATELAST");
            String vatAuthno = (String) record.get("VAT_AUTH_NO");
            String vatExpiry = (String) record.get("VAT_EXPIRY");
            String registrationNo = (String) record.get("REGISTRATION_NO");
            String email = (String) record.get("EMAIL");
            String hubDirectory = (String) record.get("HUB_DIRECTORY");
            String deliveryType = (String) record.get("DELIVERY_TYPE");
            Date created= DateUtils.parseDateWithPattern((String)record.get("CREATED"), "yyyyMMdd");
            

            if(billingAccountService.findByCode(billingAccountCode) != null) {
            	throw new BusinessException("Billing Account code already exists : " + billingAccountCode);
            }
           
            //Customer
            Customer customer = customerService.findByCode(customerCode);
            if(customer == null) {
                throw new ElementNotFoundException(customerCode, "customer");
            }
            //Customer account
            CustomerAccount customerAccount = customerAccountService.findByCode(customerAccountCode);
            if(customerAccount == null) {
            	throw new ElementNotFoundException(customerAccountCode, "customer account");
            }
            //Tax category
            TaxCategory taxCategory = taxCategoryService.findByCode(taxCategoryCode);
            if(taxCategory == null) {
            	throw new ElementNotFoundException(taxCategoryCode, "tax Category");
            }
            //Billing cycle
            BillingCycle billingCycle = billingCycleService.findByCode(billingCycleCode);
            if(billingCycle == null) {
                throw new ElementNotFoundException(billingCycleCode, "billing Cycle");
            }
            
            
            BillingAccount billingAccount = new BillingAccount();
            billingAccount.setCode(billingAccountCode);
            billingAccount.setCustomerAccount(customerAccount);
            billingAccount.setExternalRef1(billingAccountNumber);
            billingAccount.setDescription(billingAccountName);
            if(!status.equalsIgnoreCase("ACTIVE") && !status.equalsIgnoreCase("CEASED")){
            	throw new ElementNotFoundException(status, "status");
            }            
            billingAccount.setStatus(status.equalsIgnoreCase("ACTIVE")? AccountStatusEnum.ACTIVE : AccountStatusEnum.TERMINATED);
            billingAccount.setCfValue("OAO_ID",oaoId);
            billingAccount.setTaxCategory(taxCategory);
            billingAccount.setCfValue("SERVICE_TYPE",serviceType);
            billingAccount.setBillingCycle(billingCycle);
            
            if(!StringUtils.isBlank(lastBill)) {
	            Date billDateLast = DateUtils.parseDateWithPattern(lastBill, "yyyyMMdd");
	            Date nextPeriodStartDate = billingCycle.getCalendar().nextPeriodStartDate(billDateLast);
	            if(nextPeriodStartDate != null) {
	                billingAccount.setNextInvoiceDate(nextPeriodStartDate);
	            } else {
	                billingAccount.setNextInvoiceDate(billingCycle.getCalendar().nextCalendarDate(billDateLast));
	            }
            }
            billingAccount.setRegistrationNo(registrationNo);
            billingAccount.setCfValue("VAT_AUTH_NUMBER",vatAuthno);
            billingAccount.setCfValue("VAT_AUTH_EXP_DATE",vatExpiry);
            billingAccount.setCfValue("HUB_DIRECTORY",hubDirectory);
            if(!deliveryType.equalsIgnoreCase("EMAIL") && !deliveryType.equalsIgnoreCase("POST") &&!deliveryType.equalsIgnoreCase("OAO_HUB") &&!deliveryType.equalsIgnoreCase("NON_DELIVERY")) {
            	throw new ElementNotFoundException(deliveryType, "delivery type");
            }
            billingAccount.setCfValue("BILL_DELIVERY_TYPE",deliveryType.toUpperCase());
            Auditable auditable = new Auditable();
            if (created == null) {
            	auditable.setCreated(new Date());            	
			} else {
			    billingAccount.setSubscriptionDate(created);
			}
            auditable.setCreated(created);
            auditable.setCreator("import");
            billingAccount.setAuditable(auditable);
            
            //mail
            ContactInformation contactInformation= new ContactInformation();
            contactInformation.setEmail(email);
            billingAccount.setContactInformation(contactInformation);
            
            //Address
            Address address= new Address();
            address.setAddress1(address1);
            address.setAddress2(address2);
            address.setAddress3(address3);
            address.setCity(city);
            address.setZipCode(zipCode);
            Country country= countryService.findByName(countryName);
            if(country == null) {
                throw new ElementNotFoundException(countryName, "country");
            }
            address.setCountry(country);
            billingAccount.setAddress(address);
            
            //Language
            TradingLanguage tradingLanguage= tradingLanguageService.findByTradingLanguageCode("ENG");
            if(tradingLanguage == null) {
                throw new ElementNotFoundException("ENG", "Trading language");
            }
			billingAccount.setTradingLanguage(tradingLanguage);
			
			//Country
            TradingCountry tradingcountry= tradingCountryService.findByCode("IE");
            if(tradingcountry == null) {
            	throw new ElementNotFoundException("IE", "trading country");
            }
			billingAccount.setTradingCountry(tradingcountry);
			
            
            billingAccountService.create(billingAccount);
            

			//Sequence
            if (billingAccountNumber == null || !billingAccountNumber.startsWith("4")) {
            	throw new BusinessException("Billing account was not provided or do not start with 4");
			}
            Provider provider = providerService.findById(Provider.CURRENT_PROVIDER_ID, true);
            GenericSequence genericSequence= provider.getCustomerNoSequence();
			billingAccountNumber= billingAccountNumber.substring(1);
			if (genericSequence == null || genericSequence.getCurrentSequenceNb() < Long.parseLong(billingAccountNumber)) {
				genericSequence.setCurrentSequenceNb(Long.parseLong(billingAccountNumber)+ 1L);
				provider.setCustomerNoSequence(genericSequence);
				providerService.update(provider);
			}
			
			
        } catch (Exception e) {
            log.error("error adding billing account {} ", e.getMessage(), e);
            if (e instanceof BusinessException) {
                throw e;
            } else {
                // wrap the exception in a business exception and throwing it
                throw new BusinessException(e.getMessage());
            }
        }

    }
}