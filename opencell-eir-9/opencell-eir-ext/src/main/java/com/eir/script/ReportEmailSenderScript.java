package com.eir.script;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.io.FileUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.ParamBeanFactory;
import org.meveo.service.communication.impl.EmailSender;
import org.meveo.service.script.Script;

/**
 * This class sends generated report extract to the e-mails configured in the report
 * 
 * @author Mohammed Amine TAZI
 */
@Stateless
public class ReportEmailSenderScript extends Script {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = 1L;

    @Inject
    private EmailSender emailSender;

    @Inject
    protected ParamBeanFactory paramBeanFactory;
    
    private static final String SUBJECT = "SUBJECT";
    private static final String BODY = "BODY";
    private static final String FROM = "FROM";
    private static final String EMAIL = "EMAIL";
    private static final String OUTPUT_DIR = "OUTPUT_DIR";
    private static final String ARCHIVE_DIR = "ARCHIVE_DIR";

    /**
     * script main execute method.
     *
     * @param methodContext method context params
     * @throws BusinessException business exception
     */
    @Override
    public void execute(Map<String, Object> methodContext) throws BusinessException {

        if(methodContext.get(EMAIL) == null) {
            return;
        }
        String emailList = (String) methodContext.get(EMAIL);
        List<String> emails = Arrays.asList(emailList.split(";"));
        
        String sender = methodContext.get(FROM) != null ? (String) methodContext.get(FROM) : "";
        String subject = methodContext.get(SUBJECT) != null ? (String) methodContext.get(SUBJECT) : "";
        String body = methodContext.get(BODY) != null ? (String) methodContext.get(BODY) : "";

        if(methodContext.get(OUTPUT_DIR) == null || methodContext.get(ARCHIVE_DIR) == null) {
            throw new BusinessException("OUTPUT_DIR and ARCHIVE_DIR cannot be empty in job parameters!");
        }
        String outputDir = paramBeanFactory.getChrootDir() + (String) methodContext.get(OUTPUT_DIR);
        String archiveDir = paramBeanFactory.getChrootDir() + (String) methodContext.get(ARCHIVE_DIR);
        
        sendReportByMail(subject, body, outputDir, sender, emails);
       
        //Archive sent reports
        try {
            for(File file : new File(outputDir).listFiles()) {
                FileUtils.moveFileToDirectory(file, new File(archiveDir), true);   
            }
        } catch(IOException ex) {
            throw new BusinessException(ex);
        }
    }

    private void sendReportByMail(String subject, String body, String outputDir, String sender, List<String> recipients) throws BusinessException {  
        List<File> files = null;
        // Verify that actual file exists
        File attachment = new File(outputDir);
        if (!attachment.exists() || attachment.list().length == 0) {
            throw new BusinessException("Directory not found or contains no file to send :" + outputDir);
        }
        files = Arrays.asList(attachment.listFiles());
        
        emailSender.send(sender, Arrays.asList(sender), recipients, null, null, subject, body, null, files, null, false);    
    }
}
