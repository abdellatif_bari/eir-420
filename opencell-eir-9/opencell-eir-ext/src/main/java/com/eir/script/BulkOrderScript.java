package com.eir.script;

import com.eir.api.order.OrderApi;
import com.eir.commons.enums.OrderProcessingTypeEnum;
import org.apache.commons.collections4.MapUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.api.commons.Utils;
import org.meveo.api.dto.order.AttributeDto;
import org.meveo.api.dto.order.OrderDto;
import org.meveo.api.dto.order.OrderLineDto;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.script.Script;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * @author Hatim OUDAD
 */
@Stateless
public class BulkOrderScript extends Script {

    @Inject
    private OrderApi orderApi;

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void execute(Map<String, Object> initContext) {
        try {
            Map<String, Object> orderGroup = (Map<String, Object>) initContext.get("record");

            if (MapUtils.isEmpty(orderGroup)) {
                throw new BusinessException(String.format("Parameter record is missing"));
            }

            OrderDto orderDto = mapRecordFields(orderGroup);

            // executing order
            executeOrder(orderDto);

        } catch (Exception e) {
            log.error("error on process bulk order ", e.getMessage(), e);
            if (e instanceof BusinessException) {
                throw e;
            } else {
                // wrap the exception in a business exception and throwing it
                throw new BusinessException(e);
            }
        }

    }

    /**
     * Map record fields
     *
     * @param orderGroup the order group
     */
    private OrderDto mapRecordFields(Map<String, Object> orderGroup) {

        OrderDto orderDto = new OrderDto();

        Map<String, Object> wbRecord = (Map<String, Object>) orderGroup.get("wb_record");

        // Order
        orderDto.setTransactionId((String) wbRecord.get("Transaction_Id"));
        orderDto.setOrderingSystem((String) wbRecord.get("Ordering_System"));
        orderDto.setOrderCode((String) wbRecord.get("Order_Id"));
        orderDto.setRefOrderCode((String) wbRecord.get("Ref_Order_Id"));

        // bill Item
        List<Map<String, Object>> itemsGroup = (List<Map<String, Object>>) orderGroup.get("items_group");

        for (Map<String, Object> itemGroup : itemsGroup) {
            List<Map<String, Object>> billItems = (ArrayList<Map<String, Object>>) itemGroup.get("bill_item");
            List<Map<String, Object>> attributes = (ArrayList<Map<String, Object>>) itemGroup.get("attribute_record");
            if (billItems != null && !billItems.isEmpty()) {
                for (Map<String, Object> billItem : billItems) {

                    OrderLineDto orderLineDto = new OrderLineDto();

                    orderLineDto.setItemId(StringUtils.isBlank(billItem.get("Item_Id")) ? null : (String) billItem.get("Item_Id"));
                    orderLineDto.setOperatorId(StringUtils.isBlank(billItem.get("Operator_Id")) ? null : (String) billItem.get("Operator_Id"));
                    orderLineDto.setOperatorOrderNumber(StringUtils.isBlank(billItem.get("Operator_Order_Number")) ? null : (String) billItem.get("Operator_Order_Number"));
                    orderLineDto.setItemComponentId(StringUtils.isBlank(billItem.get("Item_Component_Id")) ? null : (String) billItem.get("Item_Component_Id"));
                    orderLineDto.setItemVersion(StringUtils.isBlank(billItem.get("Item_Version")) ? null : (String) billItem.get("Item_Version"));
                    orderLineDto.setItemType(StringUtils.isBlank(billItem.get("Item_Type")) ? null : (String) billItem.get("Item_Type"));
                    orderLineDto.setAgentCode(StringUtils.isBlank(billItem.get("Agent_Code")) ? null : (String) billItem.get("Agent_Code"));
                    orderLineDto.setUan(StringUtils.isBlank(billItem.get("UAN")) ? null : (String) billItem.get("UAN"));
                    orderLineDto.setMasterAccountNumber(StringUtils.isBlank(billItem.get("Master_Account_Number")) ? null : (String) billItem.get("Master_Account_Number"));
                    orderLineDto.setSubscriptionCode(StringUtils.isBlank(billItem.get("Service_Id")) ? null : (String) billItem.get("Service_Id"));
                    orderLineDto.setRefSubscriptionCode(StringUtils.isBlank(billItem.get("Ref_Service_Id")) ? null : (String) billItem.get("Ref_Service_Id"));
                    orderLineDto.setXRefSubscriptionCode(StringUtils.isBlank(billItem.get("Xref_Service_Id")) ? null : (String) billItem.get("Xref_Service_Id"));
                    orderLineDto.setBillCode(StringUtils.isBlank(billItem.get("Bill_Code")) ? null : (String) billItem.get("Bill_Code"));
                    orderLineDto.setBillAction(StringUtils.isBlank(billItem.get("Action")) ? null : (String) billItem.get("Action"));
                    orderLineDto.setBillActionQualifier(StringUtils.isBlank(billItem.get("Action_Qualifier")) ? null : (String) billItem.get("Action_Qualifier"));
                    orderLineDto.setMainServiceBillCode(StringUtils.isBlank(billItem.get("Main_Service_Bill_Code")) ? null : (String) billItem.get("Main_Service_Bill_Code"));
                    orderLineDto.setBillQuantity(StringUtils.isBlank(billItem.get("Quantity")) ? null : (String) billItem.get("Quantity"));
                    orderLineDto.setBillEffectDate(
                            DateUtils.parseDateWithPattern(StringUtils.isBlank(billItem.get("Effect_Date")) ? null : (String) billItem.get("Effect_Date"), Utils.DATE_TIME_PATTERN));

                    // Attribute
                    if (attributes != null && billItems.indexOf(billItem) == billItems.size() - 1) {
                        for (Map<String, Object> attribute : attributes) {

                            AttributeDto attributeDto = new AttributeDto();

                            attributeDto.setCode(StringUtils.isBlank(attribute.get("Code")) ? null : (String) attribute.get("Code"));
                            attributeDto.setValue(StringUtils.isBlank(attribute.get("Value")) ? null : (String) attribute.get("Value"));
                            attributeDto.setUnit(StringUtils.isBlank(attribute.get("Unit")) ? null : (String) attribute.get("Unit"));
                            attributeDto.setComponentId(StringUtils.isBlank(attribute.get("Component_Id")) ? null : (String) attribute.get("Component_Id"));
                            attributeDto.setAttributeType(StringUtils.isBlank(attribute.get("Attribute_Type")) ? null : (String) attribute.get("Attribute_Type"));
                            orderLineDto.getAttributes().add(attributeDto);
                        }
                    }
                    orderDto.getOrderLines().add(orderLineDto);
                }
            }
        }

        return orderDto;
    }

    /**
     * Execute the corresponding order
     *
     * @throws BusinessException the business exception
     */
    public void executeOrder(OrderDto orderDto) throws BusinessException {
        orderApi.processBulkOrder(orderDto);
    }
}