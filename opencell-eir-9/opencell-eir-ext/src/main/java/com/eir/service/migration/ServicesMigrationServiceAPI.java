package com.eir.service.migration;

import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.meveo.jpa.JpaAmpNewTx;

@Stateless
public class ServicesMigrationServiceAPI extends ServicesMigrationService {

    @JpaAmpNewTx
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void migrateServices(String type, List<Map<String, Object>> records) {    
        super.migrateServices(type, records);
    }
}
