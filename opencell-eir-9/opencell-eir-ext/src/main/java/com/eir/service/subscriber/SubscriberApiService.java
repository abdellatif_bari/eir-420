package com.eir.service.subscriber;

import com.eir.commons.enums.ChargeTypeEnum;
import com.eir.commons.enums.OrderLineAttributeNamesEnum;
import com.eir.commons.enums.customfields.ServiceInstanceCFsEnum;
import com.eir.commons.enums.customfields.ServiceParametersCFEnum;
import com.eir.commons.enums.customfields.ServiceTemplateCFsEnum;
import com.eir.script.OneShotAndRecurringRatingScript;
import com.eir.service.billing.impl.BillingAccountService;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.NoPricePlanException;
import org.meveo.api.commons.ErrorEnum;
import org.meveo.api.commons.Utils;
import org.meveo.api.dto.BaseDto;
import org.meveo.api.dto.subscriber.QuoteAttributeDto;
import org.meveo.api.dto.subscriber.QuoteDetailDto;
import org.meveo.api.dto.subscriber.QuoteDto;
import org.meveo.api.dto.subscriber.QuotesRequestDto;
import org.meveo.api.dto.subscriber.QuotesResponseDto;
import org.meveo.api.dto.subscriber.ServiceChargesRequestDto;
import org.meveo.api.dto.subscriber.ServiceChargesResponseDto;
import org.meveo.commons.utils.PersistenceUtils;
import org.meveo.commons.utils.StringUtils;
import org.meveo.jpa.JpaAmpNewTx;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.ChargeInstance;
import org.meveo.model.billing.RecurringChargeInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.WalletOperation;
import org.meveo.model.catalog.Calendar;
import org.meveo.model.catalog.CalendarPeriod;
import org.meveo.model.catalog.CalendarYearly;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.OfferTemplateCategory;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.crm.Customer;
import org.meveo.model.crm.EntityReferenceWrapper;
import org.meveo.service.base.NativePersistenceService;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.catalog.impl.CalendarService;
import org.meveo.service.catalog.impl.CalendarYearlyService;
import org.meveo.service.catalog.impl.OfferTemplateService;
import org.meveo.service.catalog.impl.ServiceTemplateService;
import org.meveo.service.custom.CustomTableService;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * EIR Subscriber API service
 *
 * @author Mohammed Amine TAZI
 * @author Hatim OUDAD
 */
@Stateless
public class SubscriberApiService extends NativePersistenceService {

    @Inject
    private CustomTableService customTableService;

    @Inject
    private SubscriptionService subscriptionService;

    @Inject
    private ServiceTemplateService serviceTemplateService;

    @Inject
    private OfferTemplateService offerTemplateService;

    @Inject
    private BillingAccountService billingAccountService;

    @Inject
    protected OneShotAndRecurringRatingScript ratingScript;

    @Inject
    protected CalendarService calendarService;

    @Inject
    protected CalendarYearlyService calendarYearlyService;

    @Inject
    private SubscriberApiService subscriberApiService;

    /**
     * list charges
     *
     * @param serviceChargesRequestDto
     * @return service charges
     */
    public ServiceChargesResponseDto listServiceCharges(ServiceChargesRequestDto serviceChargesRequestDto) {
        validateFields(serviceChargesRequestDto);
        Subscription subscription = retrieveSubscription(serviceChargesRequestDto);
        ServiceChargesResponseDto serviceChargesResponseDto = constructChargesResponse(serviceChargesRequestDto,
                subscription);
        return serviceChargesResponseDto;
    }

    /**
     * Construct Charges Response
     *
     * @param serviceChargesRequestDto
     * @param subscription
     * @return charge response
     */
    public ServiceChargesResponseDto constructChargesResponse(ServiceChargesRequestDto serviceChargesRequestDto,
                                                              Subscription subscription) {
        try {
            ServiceChargesResponseDto serviceChargesResponseDto = new ServiceChargesResponseDto();
            serviceChargesResponseDto.setSpeciallyAssessed("N");
            calculateConnectionAmount(serviceChargesRequestDto, serviceChargesResponseDto, subscription);
            calculateExcessCost(serviceChargesRequestDto, serviceChargesResponseDto, subscription);
            calculateAnnualRentalAmount(serviceChargesRequestDto, serviceChargesResponseDto, subscription);
            serviceChargesResponseDto.setAction(serviceChargesRequestDto.getAction());
            serviceChargesResponseDto.setChargeQueryId(serviceChargesRequestDto.getChargeQueryId());
            serviceChargesResponseDto.setTransactionId(serviceChargesRequestDto.getTransactionId());
            return serviceChargesResponseDto;
        } catch (Exception ex) {
            serviceChargesRequestDto.setErrorCode(ErrorEnum.GENERIC_API_EXCEPTION);
            serviceChargesRequestDto.setErrorMessage("Problem calculating amounts");
            throw new BusinessException(ex);
        }
    }

    /**
     * calculate charge connection amount
     *
     * @param serviceChargesRequestDto
     * @param serviceChargesResponseDto
     * @param subscription
     */
    @SuppressWarnings("unchecked")
    private void calculateConnectionAmount(ServiceChargesRequestDto serviceChargesRequestDto,
                                           ServiceChargesResponseDto serviceChargesResponseDto, Subscription subscription) {
        BigDecimal connectionAmount = new BigDecimal(0);
        String query = "SELECT wo.* from billing_wallet_operation wo, billing_charge_instance ci WHERE wo.subscription_id = :subId "
                + " AND ci.id = wo.charge_instance_id AND ci.charge_type IN ('O','S') ";
        List<WalletOperation> wos = null;
        if (StringUtils.isNotBlank(serviceChargesRequestDto.getOrderId())) {
            query = query + " AND ci.order_number = :order_number";
            wos = (List<WalletOperation>) getEntityManager().createNativeQuery(query, WalletOperation.class)
                    .setParameter("subId", subscription.getId())
                    .setParameter("order_number", serviceChargesRequestDto.getOrderId()).getResultList();
        } else {
            wos = (List<WalletOperation>) getEntityManager().createNativeQuery(query, WalletOperation.class)
                    .setParameter("subId", subscription.getId()).getResultList();
        }

        Map<String, String> cfValues = null;
        String amtooc = null;
        String chargeType = null;
        for (WalletOperation wo : wos) {
            if (wo.getServiceInstance() == null
                    || wo.getServiceInstance().getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name()) == null) {
                continue;
            }
            cfValues = (Map<String, String>) wo.getServiceInstance()
                    .getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());
            amtooc = (String) cfValues.get(ServiceParametersCFEnum.AMTOOC.getLabel());
            chargeType = (String) cfValues.get(ServiceParametersCFEnum.CHARGE_TYPE.getLabel());
            if (!"CW".equalsIgnoreCase(chargeType)) {
                if (StringUtils.isNotBlank(amtooc)) {
                    serviceChargesResponseDto.setSpeciallyAssessed("Y");
                    connectionAmount = connectionAmount.add(new BigDecimal(amtooc));
                } else if (wo.getRawAmountWithoutTax() != null && wo.getRawAmountWithoutTax().doubleValue() != 0) {
                    connectionAmount = connectionAmount.add(wo.getRawAmountWithoutTax());
                } else if (wo.getAmountWithoutTax().doubleValue() != 0) {
                    connectionAmount = connectionAmount.add(wo.getAmountWithoutTax());
                }
            }
        }
        if (connectionAmount.doubleValue() == 0) {
            serviceChargesResponseDto.setConnectionAmount("0000000.00");
        } else {
            connectionAmount = connectionAmount.setScale(2, RoundingMode.FLOOR);
            serviceChargesResponseDto.setConnectionAmount(connectionAmount.toString());
        }
    }

    /**
     * calculate Excess Cost
     *
     * @param serviceChargesRequestDto
     * @param serviceChargesResponseDto
     * @param subscription
     */
    @SuppressWarnings("unchecked")
    private void calculateExcessCost(ServiceChargesRequestDto serviceChargesRequestDto,
                                     ServiceChargesResponseDto serviceChargesResponseDto, Subscription subscription) {
        BigDecimal excessCost = new BigDecimal(0);
        String query = "SELECT wo.* from billing_wallet_operation wo, billing_charge_instance ci WHERE wo.subscription_id = :subId "
                + " AND ci.id = wo.charge_instance_id AND ci.charge_type IN ('O','S') ";

        List<WalletOperation> wos = (List<WalletOperation>) getEntityManager()
                .createNativeQuery(query, WalletOperation.class).setParameter("subId", subscription.getId())
                .getResultList();
        Map<String, String> cfValues = null;
        String chargeType = null;
        for (WalletOperation wo : wos) {
            if (wo.getServiceInstance() == null
                    || wo.getServiceInstance().getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name()) == null) {
                continue;
            }
            cfValues = (Map<String, String>) wo.getServiceInstance()
                    .getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());
            chargeType = (String) cfValues.get(ServiceParametersCFEnum.CHARGE_TYPE.getLabel());
            if ("CW".equalsIgnoreCase(chargeType)) {
                if (wo.getRawAmountWithoutTax() != null && wo.getRawAmountWithoutTax().doubleValue() != 0) {
                    excessCost = excessCost.add(wo.getRawAmountWithoutTax());
                } else if (wo.getAmountWithoutTax().doubleValue() != 0) {
                    excessCost = excessCost.add(wo.getAmountWithoutTax());
                }
            }
        }
        if (excessCost.doubleValue() == 0) {
            serviceChargesResponseDto.setExcessCost("0000000.00");

        } else {
            excessCost = excessCost.setScale(2, RoundingMode.FLOOR);
            serviceChargesResponseDto.setExcessCost(excessCost.toString());

        }
    }

    /**
     * calculate Annual Rental Amount
     *
     * @param serviceChargesRequestDto
     * @param serviceChargesResponseDto
     * @param subscription
     */
    @SuppressWarnings("unchecked")
    private void calculateAnnualRentalAmount(ServiceChargesRequestDto serviceChargesRequestDto,
                                             ServiceChargesResponseDto serviceChargesResponseDto, Subscription subscription) {
        BigDecimal annualRental = new BigDecimal(0);

        String query = "SELECT wo.* from billing_wallet_operation wo, billing_charge_instance ci WHERE wo.subscription_id = :subId "
                + " AND ci.id = wo.charge_instance_id AND ci.charge_type = 'R' and wo.code not in (select code from eir_or_sla) order by end_date desc";

        List<WalletOperation> wos = (List<WalletOperation>) getEntityManager()
                .createNativeQuery(query, WalletOperation.class).setParameter("subId", subscription.getId())
                .getResultList();
        Map<String, String> cfValues = null;
        String amt = null;
        List<String> charges = new ArrayList<>();
        for (WalletOperation wo : wos) {
            if (charges.contains(wo.getCode())) {
                continue;
            } else {
                charges.add(wo.getCode());
            }
            if (wo.getServiceInstance() == null
                    || wo.getServiceInstance().getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name()) == null) {
                continue;
            }
            cfValues = (Map<String, String>) wo.getServiceInstance()
                    .getCfValue(ServiceInstanceCFsEnum.SERVICE_PARAMETERS.name());
            amt = (String) cfValues.get(ServiceParametersCFEnum.AMT.getLabel());

            ChargeInstance chargeInstance = (ChargeInstance) PersistenceUtils
                    .initializeAndUnproxy(wo.getChargeInstance());
            RecurringChargeInstance rec = null;
            if (chargeInstance instanceof RecurringChargeInstance) {
                rec = (RecurringChargeInstance) chargeInstance;

            }

            log.warn("{} : date {}->{} price {} calendar {} multiplier {}", wo.getCode(), wo.getStartDate(),
                    wo.getEndDate(), wo.getAmountWithoutTax(), rec != null ? rec.getCalendar().getCode() : "no cal",
                    getCalendarMultiplier(chargeInstance));
            if (StringUtils.isNotBlank(amt)) {
                annualRental = annualRental.add(new BigDecimal(amt));
                serviceChargesResponseDto.setSpeciallyAssessed("Y");
            } else if (wo.getRawAmountWithoutTax() != null && wo.getRawAmountWithoutTax().doubleValue() != 0) {
                annualRental = annualRental
                        .add(wo.getRawAmountWithoutTax().multiply(getCalendarMultiplier(chargeInstance)));
            } else if (wo.getAmountWithoutTax().doubleValue() != 0) {
                annualRental = annualRental
                        .add(wo.getAmountWithoutTax().multiply(getCalendarMultiplier(chargeInstance)));
            }
        }
        if (annualRental.doubleValue() == 0) {
            serviceChargesResponseDto.setAnnualRental("0000000.00");

        } else {
            annualRental = annualRental.setScale(2, RoundingMode.FLOOR);
            serviceChargesResponseDto.setAnnualRental(annualRental.toString());

        }
    }

    /**
     * get Calendar Multiplier
     *
     * @param chargeInstance
     * @return calendar multiplier
     */
    private BigDecimal getCalendarMultiplier(ChargeInstance chargeInstance) {
        if (chargeInstance instanceof RecurringChargeInstance) {

            return getCalendarMultiplier(((RecurringChargeInstance) chargeInstance).getCalendar());
        }
        return new BigDecimal(1);
    }

    /**
     * validate fields
     *
     * @param baseDto
     */
    public void validateFields(BaseDto baseDto) {
        // Charges
        if (baseDto instanceof ServiceChargesRequestDto) {
            ServiceChargesRequestDto serviceChargesRequestDto = (ServiceChargesRequestDto) baseDto;
            validateField(serviceChargesRequestDto, "Transaction_id", serviceChargesRequestDto.getTransactionId(), "",
                    String.class, 15, true);
            validateField(serviceChargesRequestDto, "Ordering_System", serviceChargesRequestDto.getOrderingSystem(), "",
                    String.class, 5, true);
            validateField(serviceChargesRequestDto, "Charge_Query_Id", serviceChargesRequestDto.getChargeQueryId(), "",
                    String.class, 15, true);
            validateField(serviceChargesRequestDto, "Order_Id", serviceChargesRequestDto.getOrderId(), "", String.class,
                    15, false);
            validateField(serviceChargesRequestDto, "Action", serviceChargesRequestDto.getAction(), "C", String.class,
                    1, true);
            validateField(serviceChargesRequestDto, "Account_Number", serviceChargesRequestDto.getAccountNumber(), "",
                    String.class, 11, false);
            validateField(serviceChargesRequestDto, "Subscriber_Id", serviceChargesRequestDto.getSubscriberId(), "",
                    String.class, 16, true);
            validateField(serviceChargesRequestDto, "Bill_Code", serviceChargesRequestDto.getBillCode(), "",
                    String.class, 100, false);

            if (!Arrays.asList("UG", "WSLAM").contains(serviceChargesRequestDto.getOrderingSystem())) {
                setBadRequestError(serviceChargesRequestDto, ErrorEnum.BAD_REQUEST_INVALID_VALUE, "Ordering_System");
            }

            if (StringUtils.isNotBlank(serviceChargesRequestDto.getBillCode())
                    && serviceTemplateService.findByCode(serviceChargesRequestDto.getBillCode()) == null) {
                serviceChargesRequestDto.setErrorCode(ErrorEnum.BAD_REQUEST_INVALID_VALUE);
                serviceChargesRequestDto
                        .setErrorMessage("Bill Code not found : " + serviceChargesRequestDto.getBillCode());
                throw new BusinessException(serviceChargesRequestDto.getErrorMessage());
            }

            if (!"WSLAM".equalsIgnoreCase(serviceChargesRequestDto.getOrderingSystem())
                    && StringUtils.isNotBlank(serviceChargesRequestDto.getAccountNumber())
                    && billingAccountService.findByNumber(serviceChargesRequestDto.getAccountNumber()) == null) {
                serviceChargesRequestDto.setErrorCode(ErrorEnum.BAD_REQUEST_INVALID_VALUE);
                serviceChargesRequestDto
                        .setErrorMessage("Account number not found : " + serviceChargesRequestDto.getAccountNumber());
                throw new BusinessException(serviceChargesRequestDto.getErrorMessage());
            }

            if (StringUtils.isNotBlank(serviceChargesRequestDto.getErrorMessage())) {
                throw new BusinessException(serviceChargesRequestDto.getErrorMessage());
            }
            // Quotes
        } else if (baseDto instanceof QuotesRequestDto) {
            QuotesRequestDto quotesRequestDto = (QuotesRequestDto) baseDto;
            validateField(quotesRequestDto, "Transaction_id", quotesRequestDto.getTransactionId(), "", String.class, 15,
                    true);
            validateField(quotesRequestDto, "Ordering_System", quotesRequestDto.getOrderingSystem(), "", String.class,
                    5, true);
            validateField(quotesRequestDto, "Quote_Query_Id", quotesRequestDto.getQuoteQueryId(), "", String.class, 15,
                    true);
            validateField(quotesRequestDto, "Order_Number", quotesRequestDto.getOrderNumber(), "", String.class, 20,
                    false);
            validateField(quotesRequestDto, "Order_Id", quotesRequestDto.getOrderId(), "", String.class, 15, false);
            for (QuoteDetailDto quoteDetailDto : quotesRequestDto.getQuoteDetails()) {
                validateField(quotesRequestDto, "Operator_Id", quoteDetailDto.getOperatorId(), "", String.class, 10,
                        false);
                validateField(quotesRequestDto, "Bill_Code", quoteDetailDto.getBillCode(), "", String.class, 100, true);
                validateField(quotesRequestDto, "Action", quoteDetailDto.getAction(), "Q", String.class, 1, true);
                validateField(quotesRequestDto, "Quote_Type", quoteDetailDto.getQuoteType(), "SINGLE", String.class, 10,
                        true);
                validateField(quotesRequestDto, "Master_Account_Number", quoteDetailDto.getMasterAccountNumber(), "",
                        String.class, 10, false);
                validateField(quotesRequestDto, "Action_Qualifier", quoteDetailDto.getBillActionQualifier(), "",
                        String.class, 50, false);
                validateField(quotesRequestDto, "Quantity", quoteDetailDto.getBillQuantity(), "", String.class, 5,
                        true);
                validateField(quotesRequestDto, "Effect_Date", quoteDetailDto.getBillEffectDate(), "", String.class, 19,
                        true);
                for (QuoteAttributeDto quoteAttributeDto : quoteDetailDto.getAttributes()) {

                    validateField(quotesRequestDto, "Code", quoteAttributeDto.getCode(), "", String.class, 50, true);
                    validateField(quotesRequestDto, "Value", quoteAttributeDto.getValue(), "", String.class, 80, true);
                    validateField(quotesRequestDto, "Unit", quoteAttributeDto.getUnit(), "", String.class, 10, false);
                }
            }

            if (!Arrays.asList("UG", "OMS", "WSLAM").contains(quotesRequestDto.getOrderingSystem())) {
                setBadRequestError(quotesRequestDto, ErrorEnum.BAD_REQUEST_INVALID_VALUE, "Ordering_System");
            }

            if (StringUtils.isNotBlank(quotesRequestDto.getErrorMessage())) {
                throw new BusinessException(quotesRequestDto.getErrorMessage());
            }
        }
    }

    /**
     * store request
     *
     * @param serviceChargesRequestDto
     * @param serviceChargesResponseDto
     */
    @JpaAmpNewTx
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void storeRequest(ServiceChargesRequestDto serviceChargesRequestDto,
                             ServiceChargesResponseDto serviceChargesResponseDto) {

        try {
            if (serviceChargesResponseDto == null) {
                serviceChargesResponseDto = new ServiceChargesResponseDto();
                serviceChargesResponseDto.setErrorCode(serviceChargesRequestDto.getErrorCode());
                serviceChargesResponseDto.setErrorMessage(serviceChargesRequestDto.getErrorMessage());
            }
            ErrorEnum errorCode = serviceChargesRequestDto.getErrorCode();
            String errorMessage = serviceChargesRequestDto.getErrorMessage();

            serviceChargesRequestDto.setErrorCode(null);
            serviceChargesRequestDto.setErrorMessage(null);
            Map<String, Object> values = new HashMap<>();
            String xmlRequest = Utils.getXmlFromDto(serviceChargesRequestDto);
            if (!StringUtils.isBlank(xmlRequest)) {
                xmlRequest = Utils.escapePipeCharacter(xmlRequest);
            }
            serviceChargesRequestDto.setErrorCode(errorCode);
            serviceChargesRequestDto.setErrorMessage(errorMessage);
            String xmlResponse = Utils.getXmlFromDto(serviceChargesResponseDto);
            if (!StringUtils.isBlank(xmlResponse)) {
                xmlResponse = Utils.escapePipeCharacter(xmlResponse);
            }

            if (serviceChargesRequestDto.getOrderingSystem() == null) {
                serviceChargesRequestDto.setOrderingSystem("");
            }
            values.put("Ordering_system_Charge_Query_Id", serviceChargesRequestDto.getOrderingSystem().concat("_")
                    .concat(serviceChargesRequestDto.getChargeQueryId()));
            values.put("request_date", new Date());
            values.put("body_request", xmlRequest);
            if (serviceChargesRequestDto.getErrorCode() == null
                    || serviceChargesRequestDto.getErrorCode().getStatus() == null) {
                values.put("status", 200);
            } else {
                values.put("status", serviceChargesRequestDto.getErrorCode().getStatus().getStatusCode());
            }
            values.put("response", xmlResponse);
            customTableService.create("EIR_SUBSCRIBER_CHARGE_REQUEST", values);
        } catch (Exception ex) {
            serviceChargesRequestDto.setErrorCode(ErrorEnum.ORDER_ALREADY_EXISTS);
            serviceChargesRequestDto.setErrorMessage("Duplicate key when storing request on DB");
            throw new BusinessException(ex);
        }
    }

    /**
     * retrieve subscription
     *
     * @param serviceChargesRequestDto
     * @return
     */
    public Subscription retrieveSubscription(ServiceChargesRequestDto serviceChargesRequestDto) {
        if ("UG".equalsIgnoreCase(serviceChargesRequestDto.getOrderingSystem())) {
            List<Subscription> subscriptions = subscriptionService
                    .findByCodeLike(serviceChargesRequestDto.getSubscriberId());
            if (subscriptions == null || subscriptions.isEmpty()) {
                serviceChargesRequestDto.setErrorCode(ErrorEnum.BAD_REQUEST_INVALID_VALUE);
                serviceChargesRequestDto
                        .setErrorMessage("Subscription not found : " + serviceChargesRequestDto.getSubscriberId());
                throw new BusinessException(serviceChargesRequestDto.getErrorMessage());
            }
            if (subscriptions != null & subscriptions.size() == 1) {
                return subscriptions.get(0);
            } else if (StringUtils.isBlank(serviceChargesRequestDto.getAccountNumber())) {
                String subscriptionWithPrefix = getSubscriberIdWithPrefix(serviceChargesRequestDto.getSubscriberId(),
                        serviceChargesRequestDto.getBillCode());
                Subscription subscription = subscriptionService.findByCode(subscriptionWithPrefix);
                if (subscription == null) {
                    serviceChargesRequestDto.setErrorCode(ErrorEnum.BAD_REQUEST_INVALID_VALUE);
                    serviceChargesRequestDto.setErrorMessage("Subscription not found : " + subscriptionWithPrefix);
                    throw new BusinessException(serviceChargesRequestDto.getErrorMessage());
                }
                return subscription;
            } else {

                int nbSubscriptions = 0;
                Subscription subscriptionFound = null;

                for (Subscription subscription : subscriptions) {
                    if (subscription.getUserAccount().getBillingAccount().getExternalRef1()
                            .equals(serviceChargesRequestDto.getAccountNumber())) {
                        subscriptionFound = subscription;
                        nbSubscriptions++;
                    }
                }
                if (nbSubscriptions == 1) {
                    return subscriptionFound;
                } else if (nbSubscriptions == 0) {
                    serviceChargesRequestDto.setErrorCode(ErrorEnum.BAD_REQUEST_INVALID_VALUE);
                    serviceChargesRequestDto.setErrorMessage("No subscription "
                            + serviceChargesRequestDto.getSubscriberId() + " found matching account number : "
                            + serviceChargesRequestDto.getAccountNumber());
                    throw new BusinessException(serviceChargesRequestDto.getErrorMessage());
                } else {
                    serviceChargesRequestDto.setErrorCode(ErrorEnum.BAD_REQUEST_INVALID_VALUE);
                    serviceChargesRequestDto.setErrorMessage("Many subscriptions "
                            + serviceChargesRequestDto.getSubscriberId() + " found matching account number : "
                            + serviceChargesRequestDto.getAccountNumber());
                    throw new BusinessException(serviceChargesRequestDto.getErrorMessage());
                }
            }
        } else {
            Subscription subscription = subscriptionService.findByCode(serviceChargesRequestDto.getSubscriberId());
            if (subscription == null) {
                serviceChargesRequestDto.setErrorCode(ErrorEnum.BAD_REQUEST_INVALID_VALUE);
                serviceChargesRequestDto
                        .setErrorMessage("Subscription not found : " + serviceChargesRequestDto.getSubscriberId());
                throw new BusinessException(serviceChargesRequestDto.getErrorMessage());
            }
            return subscription;
        }
    }

    /**
     * get Subscriber Id With Prefix
     *
     * @param subscriptionCode
     * @param billCode
     * @return
     */
    private String getSubscriberIdWithPrefix(String subscriptionCode, String billCode) {
        try {
            ServiceTemplate serviceTemplate = serviceTemplateService.findByCode(billCode);
            if (serviceTemplate != null) {
                // get Prefix from catalog
                EntityReferenceWrapper subscriptionPrefix = (EntityReferenceWrapper) serviceTemplate
                        .getCfValue(ServiceTemplateCFsEnum.SUBSCRIBER_PREFIX.name());

                if (subscriptionPrefix != null && !StringUtils.isBlank(subscriptionPrefix.getCode())) {
                    return subscriptionPrefix.getCode().replace("_", "") + "_" + subscriptionCode;
                }
            }
            return subscriptionCode;
        } catch (Exception ex) {
            log.error("Problem extracting Subscription prefix");
            return subscriptionCode;
        }
    }

    /**
     * Validate fields
     *
     * @param fieldValue         Field value
     * @param fieldExpectedValue field expected value
     * @param fieldType          Field type
     * @param fieldSize          Field size
     * @param mandatory          Is field mandatory
     * @return Field Value
     */
    @SuppressWarnings("rawtypes")
    private boolean validateField(BaseDto baseDto, String fieldName, Object fieldValue, Object fieldExpectedValue,
                                  Class fieldType, Integer fieldSize, boolean mandatory) {

        if (!mandatory && StringUtils.isBlank(fieldValue)) {
            return true;
        }

        // Check required parameter
        if (mandatory && StringUtils.isBlank(fieldValue)) {
            setBadRequestError(baseDto, ErrorEnum.BAD_REQUEST_MISSING_PARAMETER, fieldName);
            return false;
        }

        // Check expected value
        if (!StringUtils.isBlank(fieldExpectedValue)
                && (!fieldExpectedValue.equals(fieldValue) || (fieldExpectedValue instanceof String
                && !((String) fieldExpectedValue).equalsIgnoreCase((String) fieldValue)))) {
            setBadRequestError(baseDto, ErrorEnum.BAD_REQUEST_INVALID_VALUE, fieldName);
            return false;
        }

        if (!StringUtils.isBlank(fieldValue) && fieldValue instanceof String) {

            // Check value is valid depending on type
            if ((fieldType.equals(Integer.class) && !Utils.isInteger((String) fieldValue))
                    || (fieldType.equals(Double.class) && !Utils.isDouble((String) fieldValue))
                    || (fieldType.equals(BigDecimal.class) && !Utils.isInteger((String) fieldValue))) {
                setBadRequestError(baseDto, ErrorEnum.BAD_REQUEST_INVALID_VALUE, fieldName);
                return false;
            }

            // Check field size
            if (fieldSize != null && ((String) fieldValue).length() > fieldSize) {
                setBadRequestError(baseDto, ErrorEnum.BAD_REQUEST_INVALID_SIZE, fieldName);
                return false;
            }
        }
        return true;
    }

    /**
     * Set bad request error
     *
     * @param quotesRequestDto The order Dto
     * @param errorEnum        The error enum
     * @param fieldName        The field name
     */
    private void setBadRequestError(BaseDto baseDto, ErrorEnum errorEnum, String fieldName) {
        String errorMessage = null;
        if (errorEnum == ErrorEnum.BAD_REQUEST_MISSING_PARAMETER) {
            errorMessage = String.format("The following parameter is required : %s", fieldName);
        } else if (errorEnum == ErrorEnum.BAD_REQUEST_INVALID_VALUE) {
            errorMessage = String.format("The following parameter contains invalid value : %s", fieldName);
        } else if (errorEnum == ErrorEnum.BAD_REQUEST_INVALID_SIZE) {
            errorMessage = String.format("The following parameter has invalid size : %s", fieldName);
        }
        baseDto.setErrorCode(errorEnum);
        baseDto.setErrorMessage(errorMessage);
    }

    /**
     * list quotes
     *
     * @param quotesRequestDto
     * @return quotes
     */
    public QuotesResponseDto listQuotes(QuotesRequestDto quotesRequestDto) {
        validateFields(quotesRequestDto);
        QuotesResponseDto quotesResponseDto = constructQuotesResponse(quotesRequestDto);
        return quotesResponseDto;
    }

    /**
     * store quote request
     *
     * @param quotesRequestDto
     * @param quotesResponseDto
     */
    @JpaAmpNewTx
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void storeRequest(QuotesRequestDto quotesRequestDto, QuotesResponseDto quotesResponseDto) {
        try {
            if (quotesResponseDto == null) {
                quotesResponseDto = new QuotesResponseDto();
                quotesResponseDto.setErrorCode(quotesRequestDto.getErrorCode());
                quotesResponseDto.setErrorMessage(quotesRequestDto.getErrorMessage());
            }
            ErrorEnum errorCode = quotesRequestDto.getErrorCode();
            String errorMessage = quotesRequestDto.getErrorMessage();

            quotesRequestDto.setErrorCode(null);
            quotesRequestDto.setErrorMessage(null);
            Map<String, Object> values = new HashMap<>();
            String xmlRequest = Utils.getXmlFromDto(quotesRequestDto);
            if (!StringUtils.isBlank(xmlRequest)) {
                xmlRequest = Utils.escapePipeCharacter(xmlRequest);
            }

            quotesRequestDto.setErrorCode(errorCode);
            quotesRequestDto.setErrorMessage(errorMessage);
            String xmlResponse = Utils.getXmlFromDto(quotesResponseDto);
            if (!StringUtils.isBlank(xmlResponse)) {
                xmlResponse = Utils.escapePipeCharacter(xmlResponse);
            }

            if (quotesRequestDto.getOrderingSystem() == null) {
                quotesRequestDto.setOrderingSystem("");
            }
            values.put("Ordering_system_Quote_Query_Id",
                    quotesRequestDto.getOrderingSystem().concat("_").concat(quotesRequestDto.getQuoteQueryId()));
            values.put("request_date", new Date());
            values.put("body_request", xmlRequest);
            if (quotesRequestDto.getErrorCode() == null || quotesRequestDto.getErrorCode().getStatus() == null) {
                values.put("status", 200);
            } else {
                values.put("status", quotesRequestDto.getErrorCode().getStatus().getStatusCode());
            }
            values.put("response", xmlResponse);
            customTableService.create("EIR_SUBSCRIBER_QUOTE_REQUEST", values);
        } catch (Exception ex) {
            quotesRequestDto.setErrorCode(ErrorEnum.ORDER_ALREADY_EXISTS);
            quotesRequestDto.setErrorMessage("Duplicate key when storing request on DB");
            throw new BusinessException(ex);
        }

    }

    /**
     * Construct Quotes Response
     *
     * @param quotesRequestDto
     * @return response
     */
    private QuotesResponseDto constructQuotesResponse(QuotesRequestDto quotesRequestDto) {

        BigDecimal connectionAmount = new BigDecimal(0);
        BigDecimal annualRental = new BigDecimal(0);

        for (QuoteDetailDto quoteDetailDto : quotesRequestDto.getQuoteDetails()) {

            // Billing account
            BillingAccount billingAccount = null;
            if (!StringUtils.isBlank(quoteDetailDto.getMasterAccountNumber())) {
                if (quotesRequestDto.getOrderingSystem().equals("UG")) {
                    billingAccount = billingAccountService.findByCode(quoteDetailDto.getMasterAccountNumber());
                } else {
                    billingAccount = billingAccountService.findByNumber(quoteDetailDto.getMasterAccountNumber());
                    if (billingAccount == null) {
                        billingAccount = billingAccountService.findByCode(quoteDetailDto.getMasterAccountNumber());
                    }
                }
                if (billingAccount == null) {
                    quotesRequestDto.setErrorCode(ErrorEnum.BAD_REQUEST_INVALID_VALUE);
                    quotesRequestDto
                            .setErrorMessage("billing account not found : " + quoteDetailDto.getMasterAccountNumber());
                    throw new BusinessException(quotesRequestDto.getErrorMessage());
                }
            }

            // Service
            ServiceTemplate serviceTemplate = serviceTemplateService.findByCode(quoteDetailDto.getBillCode());
            if (serviceTemplate == null) {
                quotesRequestDto.setErrorCode(ErrorEnum.BAD_REQUEST_INVALID_VALUE);
                quotesRequestDto.setErrorMessage("service not found : " + quoteDetailDto.getBillCode());
                throw new BusinessException(quotesRequestDto.getErrorMessage());
            }

            // Offer
            List<OfferTemplate> offerList = offerTemplateService.findByServiceTemplate(serviceTemplate);
            if (offerList == null || offerList.isEmpty()) {
                quotesRequestDto.setErrorCode(ErrorEnum.BAD_REQUEST_INVALID_VALUE);
                quotesRequestDto.setErrorMessage("offer not found for the service : " + quoteDetailDto.getBillCode());
                throw new BusinessException(quotesRequestDto.getErrorMessage());
            }

            Map<String, Object> serviceParameters = getServiceParameters(quotesRequestDto, quoteDetailDto, offerList);

            //Customer
            Customer cust;
            if (billingAccount == null) {
                cust = new Customer();
            } else {
                cust = billingAccount.getCustomerAccount().getCustomer();
            }

            // recurring
            if (serviceTemplate.getServiceRecurringCharges() != null
                    && !serviceTemplate.getServiceRecurringCharges().isEmpty()) {
                try {quoteDetailDto.getAttributesMap().get(ServiceParametersCFEnum.AMT.name());
                    String amt = quoteDetailDto.getAttribute(ServiceParametersCFEnum.AMT.name());
                    String amtooc = quoteDetailDto.getAttribute(ServiceParametersCFEnum.AMTOOC.name());
                    if (StringUtils.isNotBlank(amt)) {
                        annualRental = annualRental.add(new BigDecimal(amt));
                    } else if(StringUtils.isNotBlank(amtooc)) {
                        annualRental = annualRental.add(new BigDecimal(amtooc));
                    } else {
                        annualRental = annualRental.add(subscriberApiService.getAnnualRental(annualRental, quoteDetailDto, serviceTemplate,
                                offerList, serviceParameters, cust));
                    }
                } catch (NoPricePlanException e) {
                    log.debug("No annual rental tariff matched");
                }
            }
            // One shot
            if (serviceTemplate.getServiceSubscriptionCharges() != null
                    && !serviceTemplate.getServiceSubscriptionCharges().isEmpty()) {
                try {
                    connectionAmount = connectionAmount.add(subscriberApiService.getConnectionAmount(connectionAmount, quoteDetailDto,
                            serviceTemplate, offerList, serviceParameters, cust));
                } catch (NoPricePlanException e) {
                    log.debug("No annual rental tariff matched");
                }
            }
            // No charge assigned for the bill code
            if (annualRental.equals(new BigDecimal(0)) && connectionAmount.equals(new BigDecimal(0))) {
                quotesRequestDto.setErrorCode(ErrorEnum.GENERIC_API_EXCEPTION);
                quotesRequestDto
                        .setErrorMessage("No charge assigned for the bill code : " + quoteDetailDto.getBillCode());
                throw new BusinessException(quotesRequestDto.getErrorMessage());
            }

        }
        // Response
        QuotesResponseDto quotesResponseDto = new QuotesResponseDto();
        quotesResponseDto.setOrderId(quotesRequestDto.getOrderId());
        quotesResponseDto.setTransactionId(quotesRequestDto.getTransactionId());
        quotesResponseDto.setOrderingSystem(quotesRequestDto.getOrderingSystem());
        quotesResponseDto.setOrderNumber(quotesRequestDto.getOrderNumber());
        quotesResponseDto.setQuoteQueryId(quotesRequestDto.getQuoteQueryId());
        QuoteDto quoteDto = new QuoteDto();
        if (annualRental.doubleValue() == 0) {
            quoteDto.setAnnualRental("0000000.00");

        } else {
            quoteDto.setAnnualRental(annualRental.toString());
        }

        if (connectionAmount.doubleValue() == 0) {
            quoteDto.setConnectionAmount("0000000.00");

        } else {
            quoteDto.setConnectionAmount(connectionAmount.toString());
        }
        quotesResponseDto.getQuoteDtoList().add(quoteDto);
        return quotesResponseDto;
    }

    /**
     * @param connectionAmount
     * @param quoteDetailDto
     * @param serviceTemplate
     * @param offerList
     * @param serviceParameters
     * @param cust
     * @return
     */
    @JpaAmpNewTx
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public BigDecimal getConnectionAmount(BigDecimal connectionAmount, QuoteDetailDto quoteDetailDto,
                                          ServiceTemplate serviceTemplate, List<OfferTemplate> offerList, Map<String, Object> serviceParameters,
                                          Customer cust) {
        connectionAmount = ratingScript.estimateChargeAmount(cust, offerList.get(0), serviceTemplate, ChargeTypeEnum.O,
                serviceParameters, new BigDecimal(quoteDetailDto.getBillQuantity()), quoteDetailDto.getBillEffectDate(),
                1);

        return connectionAmount;
    }

    /**
     * @param annualRental
     * @param quoteDetailDto
     * @param serviceTemplate
     * @param offerList
     * @param serviceParameters
     * @param cust
     * @return
     */
    @JpaAmpNewTx
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public BigDecimal getAnnualRental(BigDecimal annualRental, QuoteDetailDto quoteDetailDto,
                                      ServiceTemplate serviceTemplate, List<OfferTemplate> offerList, Map<String, Object> serviceParameters,
                                      Customer cust) {

        annualRental = ratingScript.estimateChargeAmount(cust, offerList.get(0), serviceTemplate, ChargeTypeEnum.R,
                serviceParameters, new BigDecimal(quoteDetailDto.getBillQuantity()), quoteDetailDto.getBillEffectDate(),
                java.util.Calendar.YEAR);

        return annualRental;
    }

    /**
     * @param quotesRequestDto
     * @param quoteDetailDto
     * @param offerList
     * @param quoteAttributeDto
     * @return service parameters
     */
    protected Map<String, Object> getServiceParameters(QuotesRequestDto quotesRequestDto, QuoteDetailDto quoteDetailDto,
                                                       List<OfferTemplate> offerList) {

        Map<String, Object> serviceParameters = new HashMap<>();

        OfferTemplateCategory offerCategory = offerList.get(0).getOfferTemplateCategories().get(0);
        if (offerCategory == null) {
            quotesRequestDto.setErrorCode(ErrorEnum.BAD_REQUEST_INVALID_VALUE);
            quotesRequestDto.setErrorMessage("offer category not found for :" + quoteDetailDto.getBillCode());
            throw new BusinessException(quotesRequestDto.getErrorMessage());
        }

        /**
         * Attribute Aggregate (for all Bill Items)
         */
        serviceParameters.put(ServiceParametersCFEnum.QUANTITY.getLabel(), quoteDetailDto.getBillQuantity());
        serviceParameters.put(ServiceParametersCFEnum.ACTION_QUALIFIER.getLabel(),
                quoteDetailDto.getBillActionQualifier());

        /**
         * Attribute Aggregate (UG Orders)
         */
        if (offerCategory.getCode().equals("EIR_OTC_ACCESS")) {
            serviceParameters.put(ServiceParametersCFEnum.EXCHANGE.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.UG_EXCHANGE.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.REGION.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.UG_REGION.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.DENSITY.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.UG_DENSITY.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.CLASS_OF_SERVICE.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.UG_CLASS_OF_SERVICE.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.EF.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.UG_EF.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.AF.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.UG_AF.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.SLA.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.UG_SLA.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.BANDWIDTH.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.UG_BANDWIDTH.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.ORDER_TYPE.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.UG_ORDER_TYPE.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.EGRESS_GROUP.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.UG_EGRESS_GROUP.getLabel()));

        } else if (offerCategory.getCode().equals("EIR_OTC_DATA")) {

            /**
             * Attribute Aggregate (for all OMS Bill Items)
             */
            serviceParameters.put(ServiceParametersCFEnum.PRICE_PLAN.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_PRICE_PLAN.getLabel()));
            String amt = quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_AMT.getLabel());
            serviceParameters.put(ServiceParametersCFEnum.AMT.getLabel(), amt);
            serviceParameters.put(ServiceParametersCFEnum.AMTOOC.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_AMTOOC.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.DISTANCE.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_DISTANCE.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.RATING_SCHEME.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_RATING_SCHEME.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.BANDWIDTH.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_BANDWIDTH.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.TRANSMISSION.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_TRANSMISSION.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.ZONE_A.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_ZONE_A.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.ZONE_B.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_ZONE_B.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.REGION.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_REGION.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.SLA.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_SLA.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.SERVICE_CATEGORY.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_SERVICE_CATEGORY.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.ANALOGUE_QUALITY.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_ANALOGUE_QUALITY.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.ANALOGUE_RATE.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_ANALOGUE_RATE.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.CPE_TYPE.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_TYPE.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.CPE_DESCRIPTION.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_DESCRIPTION.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.ETS_TYPE.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_ETS_TYPE.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.IND_01.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_01.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.IND_02.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_02.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.IND_03.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_03.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.IND_04.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_04.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.IND_05.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_05.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.IND_06.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_06.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.IND_07.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_07.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.IND_08.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_08.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.IND_09.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_09.getLabel()));
            serviceParameters.put(ServiceParametersCFEnum.IND_10.getLabel(),
                    quoteDetailDto.getAttribute(OrderLineAttributeNamesEnum.OMS_CHARGE_CPE_IND_10.getLabel()));
        }
        return serviceParameters;

    }

    /**
     * get Calendar Multiplier
     *
     * @param chargeInstance
     * @return calendar multiplier
     */
    private BigDecimal getCalendarMultiplier(Calendar calendar) {
        calendar = (Calendar) PersistenceUtils.initializeAndUnproxy(calendar);
        if ("YEARLY".equalsIgnoreCase(calendar.getCalendarType())) {
            CalendarYearly calendarYearly = (CalendarYearly) calendar;
            return new BigDecimal(calendarYearly.getDays().size());
        }
        if ("PERIOD".equalsIgnoreCase(calendar.getCalendarType())) {

            CalendarPeriod calendarPeriod = (CalendarPeriod) calendar;
            if (calendarPeriod.getPeriodLength() == 1 && calendarPeriod.getPeriodUnit() == 5) {
                return new BigDecimal(365);
            }
            if (calendarPeriod.getPeriodLength() == 1 && calendarPeriod.getPeriodUnit() == 2) {
                return new BigDecimal(12);
            }
            if (calendarPeriod.getPeriodLength() == 2 && calendarPeriod.getPeriodUnit() == 2) {
                return new BigDecimal(6);
            }
            if (calendarPeriod.getPeriodLength() == 3 && calendarPeriod.getPeriodUnit() == 2) {
                return new BigDecimal(4);
            }
            if (calendarPeriod.getPeriodLength() == 6 && calendarPeriod.getPeriodUnit() == 2) {
                return new BigDecimal(2);
            }
        }

        return new BigDecimal(1);
    }
}