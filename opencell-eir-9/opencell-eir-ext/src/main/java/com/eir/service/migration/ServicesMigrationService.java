package com.eir.service.migration;

import java.math.BigInteger;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.hibernate.Session;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.api.commons.Utils;
import org.meveo.commons.utils.StringUtils;
import org.meveo.jpa.EntityManagerProvider;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.base.BaseService;

public class ServicesMigrationService extends BaseService {

    @Inject
    private EntityManagerProvider entityManagerProvider;

    private PreparedStatement ps_query_service_instance = null;

    private Long serviceInstanceSeq = null;

    private PreparedStatement ps_query_rec_charge_instance = null;

    private PreparedStatement ps_query_sub_charge_instance = null;

    private PreparedStatement ps_query_term_charge_instance = null;

    private PreparedStatement ps_query_usg_charge_instance = null;

    private Long chargeInstanceSeq = null;

    public void migrateServices(String type, List<Map<String, Object>> records) {

        List<String> validationErrors = new ArrayList<String>();

        Map<String, Long> serviceTemplateMap = new HashMap<String, Long>();
        Map<String, String> serviceTemplateDescriptionMap = new HashMap<String, String>();
        Map<String, Long[]> subscriptionMap = new HashMap<String, Long[]>();
        Map<String, Date> nextChargeMap = new HashMap<String, Date>();
        Map<String, Long> serviceTemplateChargeRecMap = new HashMap<String, Long>();
        Map<String, Long> serviceTemplateChargeSubMap = new HashMap<String, Long>();
        Map<String, Long> serviceTemplateChargeUsgMap = new HashMap<String, Long>();
        Map<String, Long> serviceTemplateChargeTrmMap = new HashMap<String, Long>();

        // Load and validate services
        List<String> serviceCodes = records.stream().map(record -> ((String) record.get("service")).toLowerCase()).distinct().collect(Collectors.toList());

        @SuppressWarnings("unchecked")
        List<Object[]> serviceTemplates = getEntityManager().createNativeQuery(
                "SELECT a.code,a.id,a.description,coalesce(b.id,0) AS rec, coalesce(c.id,0) sub, coalesce(d.id,0) usg, coalesce(e.id,0) trm from cat_service_template a"
                        + " FULL OUTER JOIN  cat_serv_rec_charge_template b on a.id = b.service_template_id"
                        + " FULL OUTER JOIN  cat_serv_sub_charge_template c on a.id = c.service_template_id"
                        + " FULL OUTER JOIN  cat_serv_usage_charge_template d on a.id = d.service_template_id"
                        + " FULL OUTER JOIN  cat_serv_trm_charge_template e on a.id = e.service_template_id where lower(code) in :codes")
                .setParameter("codes", serviceCodes).getResultList();

        for (Object[] serviceTemplate : serviceTemplates) {
            serviceTemplateMap.put(((String) serviceTemplate[0]).toUpperCase(), ((BigInteger) serviceTemplate[1]).longValue());
            serviceTemplateDescriptionMap.put(((String) serviceTemplate[0]).toUpperCase(), ((String) serviceTemplate[2]));
            serviceTemplateChargeRecMap.put(((String) serviceTemplate[0]).toUpperCase(), ((BigInteger) serviceTemplate[3]).longValue());
            serviceTemplateChargeSubMap.put(((String) serviceTemplate[0]).toUpperCase(), ((BigInteger) serviceTemplate[4]).longValue());
            serviceTemplateChargeUsgMap.put(((String) serviceTemplate[0]).toUpperCase(), ((BigInteger) serviceTemplate[5]).longValue());
            serviceTemplateChargeTrmMap.put(((String) serviceTemplate[0]).toUpperCase(), ((BigInteger) serviceTemplate[6]).longValue());
        }

        // Check for invalid service codes
        if (serviceCodes.size() != serviceTemplates.size()) {
            for (String code : serviceCodes) {
                if (!serviceTemplateMap.containsKey(code.toUpperCase())) {
                    validationErrors.add("Service template '" + code + "' not found");
                }
            }
        }

        // Load and validate subscriptions
        List<String> subscriptionCodes = records.stream().map(record -> ((String) record.get("subscription")).toLowerCase()).distinct().collect(Collectors.toList());

        @SuppressWarnings("unchecked")
        List<Object[]> subscriptions = getEntityManager().createNativeQuery(
                "select s.code, s.id, trading_country_id, trading_currency_id , seller_id, user_account_id, ba.subscription_date from billing_billing_account ba join ar_customer_account ca on ca.id = ba.customer_account_id join billing_user_account u on ba.id = u.billing_account_id join billing_subscription s on u.id = s.user_account_id and lower(s.code) in :codes")
                .setParameter("codes", subscriptionCodes).getResultList();

        for (Object[] subscription : subscriptions) {
            subscriptionMap.put(((String) subscription[0]).toUpperCase(), new Long[]{((BigInteger) subscription[1]).longValue(), ((BigInteger) subscription[2]).longValue(), ((BigInteger) subscription[3]).longValue(),
                    ((BigInteger) subscription[4]).longValue(), ((BigInteger) subscription[5]).longValue()});
            nextChargeMap.put(((String) subscription[0]).toUpperCase(), (Date) subscription[6]);
        }

        // Check for invalid subscription codes
        if (subscriptionCodes.size() != subscriptions.size()) {
            for (String code : subscriptionCodes) {
                if (!subscriptionMap.containsKey(code.toUpperCase())) {
                    validationErrors.add("Subscription '" + code + "' not found");
                }
            }
        }

        if (!validationErrors.isEmpty()) {
            throw new ValidationException(validationErrors.toString());
        }

        serviceInstanceSeq = ((BigInteger) getEntityManager().createNativeQuery("SELECT last_value FROM billing_service_instance_seq").getSingleResult()).longValue();

        chargeInstanceSeq = ((BigInteger) getEntityManager().createNativeQuery("SELECT last_value FROM billing_charge_instance_seq").getSingleResult()).longValue();

        Session hibernateSession = getEntityManager().unwrap(Session.class);

        hibernateSession.doWork(new org.hibernate.jdbc.Work() {

            @Override
            public void execute(Connection connection) throws SQLException {
                ps_query_service_instance = null;
                ps_query_rec_charge_instance = null;
                ps_query_sub_charge_instance = null;
                ps_query_term_charge_instance = null;
                ps_query_usg_charge_instance = null;

                try {
                    String query_service_instance;
                    String query_rec_charge_instance;
                    String query_sub_charge_instance;
                    String query_term_charge_instance;
                    String query_usg_charge_instance;
                    String service = "";
                    String subscriptionCode = "";
                    String item_id = "";
                    String item_component_id = "";
                    String quantity = "";
                    String subs_date = "";
                    String end_of_engagement = "";
                    Date nextChargeDate;
                    String nextcharge_date = "";
                    String bandwidth = "";
                    String density = "";
                    String region = "";
                    String sla = "";
                    String cos = "";
                    String ef = "";
                    String af = "";
                    String exchange_code = "";
                    String range_start = "";
                    String range_end = "";
                    String range_start_as_number = "";
                    String range_end_as_number = "";
                    String cfValues = "";
                    String action_qualifier;
                    String rating_scheme;
                    String item_type;
                    String status;
                    String state;
                    String price_plan;
                    String transmission;
                    String commitment_period;
                    String zone_a;
                    String zone_b;
                    String service_category;
                    String analogue_quality;
                    String analogue_rate;
                    String cpe_type;
                    String cpe_description;
                    String ets_type;
                    String ind_01;
                    String ind_02;
                    String ind_03;
                    String ind_04;
                    String ind_05;
                    String ind_06;
                    String ind_07;
                    String ind_08;
                    String ind_09;
                    String ind_10;
                    String amt;
                    String amtooc;
                    String distance;
                    String charge_type;
                    String charge_id;
                    String not_before_date;
                    String commitment_treatment;
                    String sla_ind;
                    String service_type;
                    String sgn_std_code;
                    String site_general_number;
                    String suborder;

                    for (Map<String, Object> record : records) {
                        subscriptionCode = nil(record.get("subscription"));
                        service = nil(record.get("service"));
                        item_id = nil(record.get("item_id"));
                        item_component_id = nil(record.get("item_component_id"));
                        quantity = nil(record.get("quantity"));
                        subs_date = formatDate(nil(record.get("subs_date")));
                        end_of_engagement = formatDate(nil(record.get("end_of_engagement"))); 
                        if(StringUtils.isBlank(end_of_engagement)) {
                            end_of_engagement = null;
                        }
                        nextcharge_date = nil(record.get("nextcharge_date"));
                        if ("".equals(nextcharge_date)) {
                            nextChargeDate = nextChargeMap.get(subscriptionCode.toUpperCase());
                            if (nextChargeDate == null) {
                                nextcharge_date = null;
                            } else {
                                nextcharge_date = DateUtils.formatDateWithPattern(nextChargeDate, "yyyyMMdd");
                            }
                        }
                        bandwidth = nil(record.get("bandwidth"));
                        density = nil(record.get("density"));
                        region = nil(record.get("region"));
                        sla = nil(record.get("sla"));
                        cos = nil(record.get("cos"));
                        ef = nil(record.get("ef"));
                        af = nil(record.get("af"));
                        exchange_code = nil(record.get("exchange_code"));
                        range_start = nil(record.get("range_start"));
                        range_end = nil(record.get("range_end"));
                        range_start_as_number = range_start.replaceAll("[^\\d]", "");
                        range_end_as_number = range_end.replaceAll("[^\\d]", "");
                        action_qualifier = nil(record.get("action_qualifier"));
                        rating_scheme = nil(record.get("rating_scheme"));
                        item_type = nil(record.get("item_type"));
                        status = nil(record.get("status"));
                        state = nil(record.get("state"));
                        price_plan = nil(record.get("price_plan"));
                        transmission = nil(record.get("transmission"));
                        commitment_period = nil(record.get("commitment_period"));
                        zone_a = nil(record.get("zone_a"));
                        zone_b = nil(record.get("zone_b"));
                        service_category = nil(record.get("service_category"));
                        analogue_quality = nil(record.get("analogue_quality"));
                        analogue_rate = nil(record.get("analogue_rate"));
                        cpe_type = nil(record.get("cpe_type"));
                        cpe_description = nil(record.get("cpe_description"));
                        ets_type = nil(record.get("ets_type"));
                        ind_01 = nil(record.get("ind_01"));
                        ind_02 = nil(record.get("ind_02"));
                        ind_03 = nil(record.get("ind_03"));
                        ind_04 = nil(record.get("ind_04"));
                        ind_05 = nil(record.get("ind_05"));
                        ind_06 = nil(record.get("ind_06"));
                        ind_07 = nil(record.get("ind_07"));
                        ind_08 = nil(record.get("ind_08"));
                        ind_09 = nil(record.get("ind_09"));
                        ind_10 = nil(record.get("ind_10"));
                        amt = nil(record.get("amt"));
                        amtooc = nil(record.get("amtooc"));
                        distance = nil(record.get("distance"));
                        charge_type = nil(record.get("charge_type"));
                        charge_id = nil(record.get("charge_id"));
                        not_before_date = nil(record.get("not_before_date"));
                        if (not_before_date.length() > 0) {
                            not_before_date = DateUtils.changeFormat(not_before_date, "yyyy-MM-dd'T'00:00:00", Utils.DATE_TIME_PATTERN);
                        }
                        commitment_treatment = nil(record.get("commitment_treatment"));
                        sla_ind = nil(record.get("sla_ind"));
                        if(StringUtils.isBlank(sla)) {
                            sla = sla_ind;
                        }
                        service_type = nil(record.get("service_type"));
                        sgn_std_code = nil(record.get("sgn_std_code"));
                        site_general_number = nil(record.get("site_general_number"));
                        suborder = nil(record.get("suborder"));

                        cfValues = "{\"SERVICE_PARAMETERS\":[{\"from\":\"" + DateUtils.changeFormat(subs_date, "yyyyMMdd", "yyyy-MM-dd'T'00:00:00") + "\",\"priority\":1," + "\"mapString\":{\"" + item_id + "|"
                                + item_component_id + "\":\"" + charge_type + "|" + status + "|" + state + "||" + charge_id + "|" + price_plan + "||" + distance + "||" + bandwidth + "|" + transmission + "|"
                                + commitment_period + "|" + zone_a + "|" + zone_b + "|" + region + "|" + service_category + "|" + analogue_quality + "|" + analogue_rate + "|" + cpe_type + "|" + cpe_description + "|"
                                + ets_type + "|" + ind_01 + "|" + ind_02 + "|" + ind_03 + "|" + ind_04 + "|" + ind_05 + "|" + ind_06 + "|" + ind_07 + "|" + ind_08 + "|" + ind_09 + "|" + ind_10 + "|" + amt + "|" + amtooc
                                + "|" + quantity + "|" + not_before_date + "|" + service_type + "||" + commitment_treatment + "||" + suborder + "|" + site_general_number + "|" + cos + "|" + ef + "|" + af + "|"
                                + exchange_code + "|" + action_qualifier + "|" + rating_scheme + "|" + density + "|" + sla + "||||" + sgn_std_code + "|" + range_start + "|" + range_end
                                + "|\"}}],\"ITEM_COMPONENT_ID\":[{\"string\":\"" + item_component_id + "\"}]";
                        
                        if(!StringUtils.isBlank(range_start_as_number)) {
                            cfValues += ",\"RANGE_START\":[{\"long\":\"" + Long.valueOf(range_start_as_number) + "\"}]";
                        }
                        if(!StringUtils.isBlank(range_end_as_number)) {
                            cfValues += ",\"RANGE_END\":[{\"long\":\"" + Long.valueOf(range_end_as_number) + "\"}]";
                        }
                        
                        cfValues +="}";

                        // create service instance
                        query_service_instance = "INSERT INTO BILLING_SERVICE_INSTANCE " + "(id, version,description, created, code, quantity, status, status_date, subscription_date,"
                                + " service_template_id, subscription_id, uuid, cf_values,end_agrement_date) " + "VALUES (?,1,?, NOW(),?, ?, 'ACTIVE', to_timestamp(?,'YYYYMMDD')," + " to_timestamp(?,'YYYYMMDD'),"
                                + "?,?,?,?,to_timestamp(?,'YYYYMMDD'));";

                        if (ps_query_service_instance == null) {
                            ps_query_service_instance = connection.prepareStatement(query_service_instance);
                        }

                        ps_query_service_instance.setLong(1, ++serviceInstanceSeq);
                        ps_query_service_instance.setString(2, serviceTemplateDescriptionMap.get(service.toUpperCase()));
                        ps_query_service_instance.setString(3, service);
                        ps_query_service_instance.setLong(4, Long.parseLong(quantity));
                        ps_query_service_instance.setString(5, subs_date);
                        ps_query_service_instance.setString(6, subs_date);
                        ps_query_service_instance.setLong(7, serviceTemplateMap.get(service.toUpperCase()));
                        ps_query_service_instance.setLong(8, subscriptionMap.get(subscriptionCode.toUpperCase())[0]);
                        ps_query_service_instance.setString(9, UUID.randomUUID().toString());
                        ps_query_service_instance.setString(10, cfValues);
                        ps_query_service_instance.setString(11, end_of_engagement);
                        ps_query_service_instance.addBatch();

                        // Rec Charge Instance
                        query_rec_charge_instance = "INSERT INTO BILLING_CHARGE_INSTANCE " + "(id, version, created, code, description, charge_date, is_prepaid, status, status_date, "
                                + "charge_template_id, trading_country, trading_currency, seller_id, subscription_id, user_account_id, charge_type, quantity, "
                                + "next_charge_date, subscription_date, service_instance_id, uuid, calendar_id, apply_in_advance, charged_to_date, priority) "
                                + "select ?, 1, NOW(), ?, ?, to_timestamp(?,'YYYYMMDD'), 0, 'ACTIVE', NOW(), ch.id,?, "
                                + "?,?,?, ?,'R', ?, to_timestamp(?,'YYYYMMDD'), to_timestamp(?,'YYYYMMDD'), ?, ?, ch.calendar_id, 1, to_timestamp(?,'YYYYMMDD'), 1"
                                + " from cat_charge_template ch, cat_serv_rec_charge_template sc where ch.id = sc.charge_template_id and sc.service_template_id = ?";

                        if (ps_query_rec_charge_instance == null) {
                            ps_query_rec_charge_instance = connection.prepareStatement(query_rec_charge_instance);
                        }
                        if (serviceTemplateChargeRecMap.get(service) != 0) {
                            ps_query_rec_charge_instance.setLong(1, ++chargeInstanceSeq);
                            ps_query_rec_charge_instance.setString(2, service + "_RC");
                            ps_query_rec_charge_instance.setString(3, serviceTemplateDescriptionMap.get(service.toUpperCase()));
                            ps_query_rec_charge_instance.setString(4, nextcharge_date);
                            ps_query_rec_charge_instance.setLong(5, subscriptionMap.get(subscriptionCode.toUpperCase())[1]);
                            ps_query_rec_charge_instance.setLong(6, subscriptionMap.get(subscriptionCode.toUpperCase())[2]);
                            ps_query_rec_charge_instance.setLong(7, subscriptionMap.get(subscriptionCode.toUpperCase())[3]);
                            ps_query_rec_charge_instance.setLong(8, subscriptionMap.get(subscriptionCode.toUpperCase())[0]);
                            ps_query_rec_charge_instance.setLong(9, subscriptionMap.get(subscriptionCode.toUpperCase())[4]);
                            ps_query_rec_charge_instance.setLong(10, Long.parseLong(quantity));
                            ps_query_rec_charge_instance.setString(11, nextcharge_date);
                            ps_query_rec_charge_instance.setString(12, subs_date);
                            ps_query_rec_charge_instance.setLong(13, serviceInstanceSeq);
                            ps_query_rec_charge_instance.setString(14, UUID.randomUUID().toString());
                            ps_query_rec_charge_instance.setString(15, nextcharge_date);
                            ps_query_rec_charge_instance.setLong(16, serviceTemplateMap.get(service.toUpperCase()));
                            ps_query_rec_charge_instance.addBatch();
                        }
                        // SUB Charge Instance
                        query_sub_charge_instance = "INSERT INTO BILLING_CHARGE_INSTANCE " + "(id, version,created, code, description, charge_date, is_prepaid, status, status_date, "
                                + "charge_template_id, trading_country, trading_currency, seller_id, subscription_id, user_account_id, charge_type, quantity, "
                                + "next_charge_date, subscription_date, service_instance_id, uuid, calendar_id, apply_in_advance, charged_to_date, priority) "
                                + "select ?, 1, NOW(), ?, ?, NULL, 0, 'CLOSED', NOW(), ch.id,?,?,?,?,?, 'S', ?, NULL, NULL, ?, ?, NULL, 1, NULL, 1"
                                + " from cat_charge_template ch, cat_serv_sub_charge_template sc where ch.id = sc.charge_template_id and sc.service_template_id = ?";

                        if (ps_query_sub_charge_instance == null) {
                            ps_query_sub_charge_instance = connection.prepareStatement(query_sub_charge_instance);
                        }

                        if (serviceTemplateChargeSubMap.get(service) != 0) {
                            ps_query_sub_charge_instance.setLong(1, ++chargeInstanceSeq);
                            ps_query_sub_charge_instance.setString(2, service + "_NRC");
                            ps_query_sub_charge_instance.setString(3, serviceTemplateDescriptionMap.get(service.toUpperCase()));
                            ps_query_sub_charge_instance.setLong(4, subscriptionMap.get(subscriptionCode.toUpperCase())[1]);
                            ps_query_sub_charge_instance.setLong(5, subscriptionMap.get(subscriptionCode.toUpperCase())[2]);
                            ps_query_sub_charge_instance.setLong(6, subscriptionMap.get(subscriptionCode.toUpperCase())[3]);
                            ps_query_sub_charge_instance.setLong(7, subscriptionMap.get(subscriptionCode.toUpperCase())[0]);
                            ps_query_sub_charge_instance.setLong(8, subscriptionMap.get(subscriptionCode.toUpperCase())[4]);
                            ps_query_sub_charge_instance.setLong(9, Long.parseLong(quantity));
                            ps_query_sub_charge_instance.setLong(10, serviceInstanceSeq);
                            ps_query_sub_charge_instance.setString(11, UUID.randomUUID().toString());
                            ps_query_sub_charge_instance.setLong(12, serviceTemplateMap.get(service.toUpperCase()));
                            ps_query_sub_charge_instance.addBatch();
                        }
                        // Usage Charge Instance
                        query_usg_charge_instance = "INSERT INTO BILLING_CHARGE_INSTANCE(id, version, created, code, description, charge_date, is_prepaid, status, status_date, "
                                + "charge_template_id, trading_country, trading_currency, seller_id, subscription_id, user_account_id, charge_type, quantity, "
                                + "next_charge_date, subscription_date, service_instance_id, uuid, calendar_id, apply_in_advance, charged_to_date, priority) "
                                + "select ?,1, NOW(), ch.code, ch.description, NULL, 0, 'ACTIVE', NOW(), ch.id,  "
                                + "?,?,?,?,?, 'U', ?, NULL, NULL, ?, ?, NULL, 1, NULL, 1"
                                + " from cat_charge_template ch, cat_serv_usage_charge_template sc where ch.id = sc.charge_template_id and sc.service_template_id = ?";

                        if (ps_query_usg_charge_instance == null) {
                            ps_query_usg_charge_instance = connection.prepareStatement(query_usg_charge_instance);
                        }

                        if (serviceTemplateChargeUsgMap.get(service) != 0) {
                            ps_query_usg_charge_instance.setLong(1, ++chargeInstanceSeq);
                            ps_query_usg_charge_instance.setLong(2, subscriptionMap.get(subscriptionCode.toUpperCase())[1]);
                            ps_query_usg_charge_instance.setLong(3, subscriptionMap.get(subscriptionCode.toUpperCase())[2]);
                            ps_query_usg_charge_instance.setLong(4, subscriptionMap.get(subscriptionCode.toUpperCase())[3]);
                            ps_query_usg_charge_instance.setLong(5, subscriptionMap.get(subscriptionCode.toUpperCase())[0]);
                            ps_query_usg_charge_instance.setLong(6, subscriptionMap.get(subscriptionCode.toUpperCase())[4]);
                            ps_query_usg_charge_instance.setLong(7, Long.parseLong(quantity));
                            ps_query_usg_charge_instance.setLong(8, serviceInstanceSeq);
                            ps_query_usg_charge_instance.setString(9, UUID.randomUUID().toString());
                            ps_query_usg_charge_instance.setLong(10, serviceTemplateMap.get(service.toUpperCase()));
                            ps_query_usg_charge_instance.addBatch();
                        }
                        // Term Charge Instance
                        query_term_charge_instance = "INSERT INTO BILLING_CHARGE_INSTANCE " + "(id,version, created, code, description, charge_date, is_prepaid, status, status_date, "
                                + "charge_template_id, trading_country, trading_currency, seller_id, subscription_id, user_account_id, charge_type, quantity, "
                                + "next_charge_date, subscription_date, service_instance_id, uuid, calendar_id, apply_in_advance, charged_to_date, priority) "
                                + "select ?, 1, NOW(), ch.code, ch.description, NULL, 0, 'INACTIVE', NOW(), ch.id, "
                                + "?,?,?,?,?, 'T', ?, NULL, NULL, ?, ?, NULL, 1, NULL, 1"
                                + " from cat_charge_template ch, cat_serv_trm_charge_template sc where ch.id = sc.charge_template_id and sc.service_template_id = ?";

                        if (ps_query_term_charge_instance == null) {
                            ps_query_term_charge_instance = connection.prepareStatement(query_term_charge_instance);
                        }
                        if (serviceTemplateChargeTrmMap.get(service) != 0) {
                            ps_query_term_charge_instance.setLong(1, ++chargeInstanceSeq);
                            ps_query_term_charge_instance.setLong(2, subscriptionMap.get(subscriptionCode.toUpperCase())[1]);
                            ps_query_term_charge_instance.setLong(3, subscriptionMap.get(subscriptionCode.toUpperCase())[2]);
                            ps_query_term_charge_instance.setLong(4, subscriptionMap.get(subscriptionCode.toUpperCase())[3]);
                            ps_query_term_charge_instance.setLong(5, subscriptionMap.get(subscriptionCode.toUpperCase())[0]);
                            ps_query_term_charge_instance.setLong(6, subscriptionMap.get(subscriptionCode.toUpperCase())[4]);
                            ps_query_term_charge_instance.setLong(7, Long.parseLong(quantity));
                            ps_query_term_charge_instance.setLong(8, serviceInstanceSeq);
                            ps_query_term_charge_instance.setString(9, UUID.randomUUID().toString());
                            ps_query_term_charge_instance.setLong(10, serviceTemplateMap.get(service.toUpperCase()));
                            ps_query_term_charge_instance.addBatch();
                        }
                    }

                    if (ps_query_service_instance != null) {
                        ps_query_service_instance.executeBatch();
                    }
                    if (ps_query_rec_charge_instance != null) {
                        ps_query_rec_charge_instance.executeBatch();
                    }
                    if (ps_query_sub_charge_instance != null) {
                        ps_query_sub_charge_instance.executeBatch();
                    }
                    if (ps_query_term_charge_instance != null) {
                        ps_query_term_charge_instance.executeBatch();
                    }
                    if (ps_query_usg_charge_instance != null) {
                        ps_query_usg_charge_instance.executeBatch();
                    }

                } catch (Exception e) {
                    if (e instanceof BatchUpdateException) {
                        throw new BusinessException(e.getMessage());
                    }
                    log.error("Failed to bulk insert : ", e.getMessage());
                    throw e;
                } finally {
                    if (ps_query_service_instance != null) {
                        ps_query_service_instance.close();
                    }
                    if (ps_query_rec_charge_instance != null) {
                        ps_query_rec_charge_instance.close();
                    }
                    if (ps_query_sub_charge_instance != null) {
                        ps_query_sub_charge_instance.close();
                    }
                    if (ps_query_term_charge_instance != null) {
                        ps_query_term_charge_instance.close();
                    }
                    if (ps_query_usg_charge_instance != null) {
                        ps_query_usg_charge_instance.close();
                    }
                }
                try {
                    getEntityManager().createNativeQuery("SELECT setval('billing_service_instance_seq', :seq, true);").setParameter("seq", serviceInstanceSeq).getSingleResult();
                    getEntityManager().createNativeQuery("SELECT setval('billing_charge_instance_seq', :seq, true);").setParameter("seq", chargeInstanceSeq).getSingleResult();
                } catch (Exception e) {
                    throw e;
                }
            }
        });
    }

    private String formatDate(String date) {
        if (DateUtils.parseDateWithPattern(date, "yyyyMMdd") != null) {
            return date;
        } else {
            return DateUtils.changeFormat(date, "dd/MM/yyyy HH:mm:ss", "yyyyMMdd");
        }
    }

    private String nil(Object text) {
        if (text != null) {
            return Utils.escapePipeCharacter((String) text);
        }
        return "";
    }

    /**
     * Return an entity manager for a current provider
     *
     * @return Entity manager
     */
    public EntityManager getEntityManager() {
        return entityManagerProvider.getEntityManager().getEntityManager();
    }
}