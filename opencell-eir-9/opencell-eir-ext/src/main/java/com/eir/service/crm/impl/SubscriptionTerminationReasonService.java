package com.eir.service.crm.impl;

import org.meveo.commons.utils.QueryBuilder;
import org.meveo.model.billing.SubscriptionTerminationReason;
import org.meveo.service.base.BusinessService;

import javax.ejb.Stateless;
import javax.persistence.Query;
import java.util.List;

/**
 * EIR subscription termination reason service
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirSubscriptionTerminationReasonService")
public class SubscriptionTerminationReasonService extends BusinessService<SubscriptionTerminationReason> {

    @SuppressWarnings("unchecked")
    public List<SubscriptionTerminationReason> listReasons() {

        Query query = new QueryBuilder(SubscriptionTerminationReason.class, "c", null).getQuery(getEntityManager());
        return query.getResultList();
    }

}

