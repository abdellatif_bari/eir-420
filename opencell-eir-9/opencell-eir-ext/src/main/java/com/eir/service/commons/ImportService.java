package com.eir.service.commons;

import com.eir.commons.enums.MatchingWildcardEnum;
import com.eir.commons.enums.ReferenceTableTypeEnum;
import com.eir.commons.enums.StagingRecordActionEnum;
import com.eir.commons.enums.TransformationRulesEnum;
import com.eir.commons.utils.BusinessRulesValidator;
import com.eir.commons.utils.HashGenerationException;
import com.eir.commons.utils.TransformationRule;
import com.eir.commons.utils.TransformationRulesFactory;
import com.eir.commons.utils.TransformationRulesHandler;
import org.apache.commons.collections.MapUtils;
import org.hibernate.Session;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.api.commons.Utils;
import org.meveo.commons.utils.ListUtils;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.customEntities.CustomEntityTemplate;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.base.NativePersistenceService;
import org.meveo.service.crm.impl.CustomFieldTemplateService;
import org.meveo.service.custom.CustomTableService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

/**
 * EIR Import service
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirImportService")
public class ImportService extends NativePersistenceService {

    @Inject
    private TransformationRulesFactory transformationRulesFactory;
    @Inject
    private BusinessRulesValidator businessRulesValidator;
    @Inject
    private CustomTableService customTableService;
    @Inject
    private CustomFieldTemplateService customFieldTemplateService;

    /**
     * Indicates if the staging record in not yet checked.
     */
    public static final String UNCHECKED_RECORD = "<unchecked>";


    /**
     * Validate staging record
     *
     * @param importType the import type
     * @param fileId     the file id
     * @throws BusinessException the business exception
     */
    public void validateRecord(String importType, Long fileId) throws BusinessException {
        TransformationRulesHandler transformationRulesHandler = getTransformationRules(importType);
        List<Map<String, Object>> stagingRecords = findStagingRecordsByFileId(transformationRulesHandler.getStageTableName(), fileId);
        for (Map<String, Object> entry : stagingRecords) {
            entry.put(transformationRulesHandler.getErrorReasonFieldName(), null);
            businessRulesValidator.validateRecord(entry, transformationRulesHandler, true, validateBusinessRules(), importType);
        }
        customTableService.update(transformationRulesHandler.getStageTableName(), stagingRecords);
    }

    /**
     * Validate staging record
     *
     * @param importType    the import type
     * @param stagingRecord the staging record
     * @throws BusinessException the business exception
     */
    public void validateRecord(String importType, Map<String, Object> stagingRecord) throws BusinessException {
        TransformationRulesHandler transformationRulesHandler = getTransformationRules(importType);
        businessRulesValidator.validateRecord(stagingRecord, transformationRulesHandler, false, validateBusinessRules(), importType);
        //customTableService.create(transformationRulesHandler.getStageTableName(), stagingRecord);
        createRecord(transformationRulesHandler.getStageTableName(), transformationRulesHandler.getStageTableFields(), new ArrayList<>(Arrays.asList(stagingRecord)));
    }

    /**
     * Activate staging record
     *
     * @param importType the import type
     * @param fileId     the file id
     * @throws BusinessException the business exception
     */
    public void activateRecord(String importType, Long fileId) throws BusinessException {
        long startTerminateTime = System.currentTimeMillis();
        long endTerminateTime = startTerminateTime;
        log.debug("Starting The activation part ...........");
        TransformationRulesHandler transformationRulesHandler = getTransformationRules(importType);
        List<Map<String, Object>> stagingRecords = findStagingRecordsByFileId(transformationRulesHandler.getStageTableName(), fileId);
        for (Map<String, Object> entry : stagingRecords) {
            if (!StringUtils.isBlank(entry.get("error_reason"))) {
                throw new BusinessException("The file cannot be activated as long as it contains invalid records");
            }
        }

        List<String> excludedCriterion = new ArrayList<>(Arrays.asList("valid_to", "accounting_code_id", "updated", "updated_by", "file_name"));


        List<Map<String, Object>> createdValidRecords = new ArrayList<>();
        List<Map<String, Object>> updatedValidRecords = new ArrayList<>();
        List<Map<String, Object>> ignoredValidRecords = new ArrayList<>();
        Date now = new Date();
        for (Map<String, Object> entry : stagingRecords) {
            // Apply the generic validation
            Map<String, Object> stagingRecord = businessRulesValidator.acceptRecord(entry, transformationRulesHandler.getTransformationRules());

            // Apply the custom validation
            transformationRulesHandler.getActivateFunction().apply(new Object[]{importType, entry});

            if (StagingRecordActionEnum.MODIFY.name().equalsIgnoreCase((String) entry.get("record_action"))) {
                Map<String, Object> validRecord = businessRulesValidator.getValidRecord(stagingRecord, transformationRulesHandler, excludedCriterion, true);

                if (validRecord != null) {
                    String filename = validRecord.get("file_name") + "," + stagingRecord.get("file_name");
                    Date valid_to = (Date) validRecord.get("valid_to");

                    // Store the staging record in the valid one.
                    validRecord.putAll(stagingRecord);
                    validRecord.put("updated", now);
                    validRecord.put("updated_by", currentUser.getUserName());
                    validRecord.put("file_name", filename);
                    // if the old valid_to is not null then keep it.
                    if (valid_to != null) {
                        validRecord.put("valid_to", valid_to);
                    }
                    updatedValidRecords.add(validRecord);
                } else {
                    ignoredValidRecords.add(validRecord);
                }
            } else {
                createdValidRecords.add(stagingRecord);
            }
        }


        if (!createdValidRecords.isEmpty() || !updatedValidRecords.isEmpty() || !ignoredValidRecords.isEmpty()) {
            if (!createdValidRecords.isEmpty() || !updatedValidRecords.isEmpty()) {

                List<Long> newRecordIds = new ArrayList<>();
                List<Long> modifiedRecordIds = new ArrayList<>();

                if (!createdValidRecords.isEmpty()) {
                    //customTableService.create(transformationRulesHandler.getValidTableName(), transformationRulesHandler.getValidTableName(), createdValidRecords);
                    createRecord(transformationRulesHandler.getValidTableName(), transformationRulesHandler.getValidTableFields(), createdValidRecords);
                    newRecordIds = getValidRecordIdsByFileName(transformationRulesHandler.getValidTableName(), (String) createdValidRecords.get(0).get("file_name"));
                }

                if (!updatedValidRecords.isEmpty()) {
                    updateRecord(transformationRulesHandler.getValidTableName(), transformationRulesHandler.getValidTableFields(),
                            new ArrayList<>(Arrays.asList("accounting_code_id", "updated", "updated_by", "file_name", "priority", "valid_to")),
                            updatedValidRecords);
                    modifiedRecordIds = updatedValidRecords.stream().map(updatedValidRecord -> ((Number) updatedValidRecord.get(NativePersistenceService.FIELD_ID)).longValue()).collect(Collectors.toList());
                }

                endTerminateTime = System.currentTimeMillis();
                log.debug("The activation (add and modify) part took " + (endTerminateTime - startTerminateTime) + " milliseconds");
                // To avoid counting the modified records in case the update is executed before the select query
                newRecordIds.removeAll(modifiedRecordIds);
                // Apply the custom re-rating
                transformationRulesHandler.getRerateFunction().apply(new Object[]{transformationRulesHandler.getValidTableName(), newRecordIds, modifiedRecordIds});
            }
            cancelStagingRecords(transformationRulesHandler.getStageTableName(), fileId);
        } else {
            throw new BusinessException("There's no record to activate");
        }
        endTerminateTime = System.currentTimeMillis();
        log.debug("The whole activation part took " + (endTerminateTime - startTerminateTime) + " milliseconds");
    }

    /**
     * Find the valid records by file name
     *
     * @param validTableName valid table name
     * @param fileName       the file name.
     * @return list of staging record ids
     */
    public List<Long> getValidRecordIdsByFileName(String validTableName, String fileName) {
        List<Long> listRecordIds = null;
        if (!StringUtils.isBlank(fileName)) {
            String sql = "SELECT id FROM " + validTableName + " vt where vt.file_name=:fileName";
            Query query = getEntityManager().createNativeQuery(sql);
            query.setParameter("fileName", fileName);
            listRecordIds = ((List<BigInteger>) query.getResultList()).stream().map(d -> d.longValue()).collect(Collectors.toList());
        }
        return listRecordIds;
    }

    /**
     * Cancel staging records
     *
     * @param stagingTableName staging table name
     * @param fileId           the file id
     */
    public void cancelStagingRecords(String stagingTableName, Long fileId) {
        String sql = "delete from " + stagingTableName + " where file_id=:fileId ";
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter("fileId", fileId).executeUpdate();
    }

    /**
     * Check if staging records exist
     *
     * @param fileId           the file id
     * @param stagingTableName staging table name
     * @return true is the staging records exist, false if else.
     */
    public boolean isStagingRecordsExist(String stagingTableName, Long fileId) {
        String sql = "select count(*) from " + stagingTableName + "  where file_id=:fileId";
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter("fileId", fileId);
        return ((BigInteger) query.getSingleResult()).longValue() > 0;
    }

    /**
     * Validate reference data
     *
     * @param transformationRulesHandler the transformation rules handler
     * @param fileId                     the file id
     * @param excludedCriterion          the excluded criterion
     * @return true is the reference data is valid, false if else.
     */
    public boolean validateReferenceData(TransformationRulesHandler transformationRulesHandler, Long fileId, List<String> excludedCriterion) {

        if (transformationRulesHandler == null || transformationRulesHandler.getTransformationRules() == null
                || transformationRulesHandler.getTransformationRules().isEmpty()) {
            return true;
        }

        for (TransformationRule transformationRule : transformationRulesHandler.getTransformationRules()) {
            if (StringUtils.isBlank(transformationRule.getValidFieldName()) || (excludedCriterion != null && excludedCriterion.contains(transformationRule.getValidFieldName()))) {
                continue;
            }
            if (!StringUtils.isBlank(transformationRule.getReferenceTable()) && (transformationRule.getReferenceTableType() == ReferenceTableTypeEnum.TABLE ||
                    transformationRule.getReferenceTableType() == ReferenceTableTypeEnum.CUSTOM_TABLE ||
                    transformationRule.getReferenceTableType() == ReferenceTableTypeEnum.CUSTOM_ENTITY)) {
                String sql;
                if (transformationRule.getReferenceTableType() == ReferenceTableTypeEnum.CUSTOM_ENTITY) {
                    sql = "update " + transformationRulesHandler.getStageTableName() + " set " + transformationRulesHandler.getErrorReasonFieldName() +
                            "= 'No matching " + transformationRule.getReferenceTable() + " was found' " +
                            "where file_id=:fileId and (" + transformationRulesHandler.getErrorReasonFieldName() + " is null or "
                            + transformationRulesHandler.getErrorReasonFieldName() + " = '') ";
                    if (String.class.isAssignableFrom(transformationRule.getStageFieldType())) {
                        sql = sql + " and (LOWER(" + transformationRule.getStageFieldName() + ") not in ( " +
                                "select LOWER(" + transformationRule.getReferenceFromField() + ")  from cust_cei  where cet_code = '" + transformationRule.getReferenceTable() + "'" +
                                " and LOWER(" + transformationRule.getReferenceFromField() + ") = LOWER(" + transformationRule.getStageFieldName() + "))";
                    } else {
                        sql = sql + " and (" + transformationRule.getStageFieldName() + " not in ( " +
                                "select " + transformationRule.getReferenceFromField() + "  from cust_cei  where cet_code = '" + transformationRule.getReferenceTable() + "'" +
                                " and " + transformationRule.getReferenceFromField() + " = " + transformationRule.getStageFieldName() + ")";
                    }

                } else {
                    sql = "update " + transformationRulesHandler.getStageTableName() + " set " + transformationRulesHandler.getErrorReasonFieldName() +
                            "= 'No matching " + transformationRule.getReferenceTable() + " was found' " +
                            "where file_id=:fileId and (" + transformationRulesHandler.getErrorReasonFieldName() + " is null or "
                            + transformationRulesHandler.getErrorReasonFieldName() + " = '') ";
                    if (String.class.isAssignableFrom(transformationRule.getStageFieldType())) {
                        sql = sql + " and (LOWER(" + transformationRule.getStageFieldName() + ") not in ( " +
                                "select LOWER(" + transformationRule.getReferenceFromField() + ") from " + transformationRule.getReferenceTable() + " where LOWER(" +
                                transformationRule.getReferenceFromField() + ") = LOWER(" + transformationRule.getStageFieldName() + "))";
                    } else {
                        sql = sql + " and (" + transformationRule.getStageFieldName() + " not in ( " +
                                "select " + transformationRule.getReferenceFromField() + " from " + transformationRule.getReferenceTable() + " where " +
                                transformationRule.getReferenceFromField() + " = " + transformationRule.getStageFieldName() + ")";
                    }
                }
                if (!StringUtils.isBlank(transformationRule.getReferenceTableAdditionalCriteria())) {
                    sql = sql + " or LOWER(" + transformationRule.getStageFieldName() + ") not in (" + transformationRule.getReferenceTableAdditionalCriteria() + ")";
                }
                sql = sql + ")";
                if (!transformationRule.isValidFieldMandatory() && transformationRule.isWildcardFieldValue()) {
                    sql = sql + "and " + transformationRule.getStageFieldName() + "!='" + MatchingWildcardEnum.ASTERISK.getLabel() + "'";
                    sql = sql + "and " + transformationRule.getStageFieldName() + "!='" + MatchingWildcardEnum.PERCENT.getLabel() + "'";
                    sql = sql + "and " + transformationRule.getStageFieldName() + "!='" + MatchingWildcardEnum.NULL.getLabel() + "'";
                }
                Query query = getEntityManager().createNativeQuery(sql);
                int affectedRows = query.setParameter("fileId", fileId).executeUpdate();
                if (affectedRows > 0) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Get transformation rules handler
     *
     * @param importType the import type
     * @return the transformation rules handler
     */
    public TransformationRulesHandler getTransformationRules(String importType) {
        TransformationRulesHandler transformationRulesHandler = null;
        if (TransformationRulesEnum.contains(importType)) {
            transformationRulesHandler = transformationRulesFactory.getInstance(TransformationRulesEnum.valueOfIgnoreCase(importType));
        }
        if (transformationRulesHandler == null) {
            throw new BusinessException("The type import is not recognized");
        }
        return transformationRulesHandler;
    }

    /**
     * Find the staging records by file id
     *
     * @param stagingTableName staging table name
     * @param fileId           the file id
     * @return list of staging records
     * @throws BusinessException the business exception
     */
    public List<Map<String, Object>> findStagingRecordsByFileId(String stagingTableName, Long fileId) {
        Map<String, Object> filters = new HashMap<>();
        filters.put("file_id", fileId);
        List<Map<String, Object>> stagingRecords = customTableService.list(stagingTableName, new PaginationConfiguration(filters));
        if (stagingRecords == null || stagingRecords.isEmpty()) {
            throw new BusinessException("There is no record in the staging table for this file");
        }
        return stagingRecords;
    }

    /**
     * Find the staging records by file name
     *
     * @param stagingTableName staging table name
     * @param fileName         the file name.
     * @return list of staging records
     */
    public List<String> findStagingRecordsByFileName(String stagingTableName, String fileName) {
        List<String> listRecordCodes = null;
        if (!StringUtils.isBlank(fileName)) {
            String sql = "SELECT code FROM " + stagingTableName + " ts inner join flat_file ff on ts.file_id = ff.id and ff.file_original_name=:fileName";
            Query query = getEntityManager().createNativeQuery(sql);
            query.setParameter("fileName", fileName);
            listRecordCodes = query.getResultList();
        }
        return listRecordCodes;
    }

    /**
     * The callback function for business rules validation
     *
     * @return the business rules validation result
     */
    protected BiFunction<Map<String, Object>, Object[], Object> validateBusinessRules() {
        return this::validateBusinessRules;
    }

    /**
     * Validate business rules
     *
     * @param record     the staging record
     * @param parameters the parameters list to pass to the validation function
     * @return the result of business validation.
     * @throws ValidationException the validation exception
     */
    public String validateBusinessRules(Map<String, Object> record, Object... parameters) throws ValidationException {
        TransformationRulesHandler transformationRulesHandler = getTransformationRules((String) parameters[0]);

        // Apply custom validation
        if (transformationRulesHandler.getValidateFunction() != null) {
            transformationRulesHandler.getValidateFunction().apply(new Object[]{record, transformationRulesHandler});
        }

        if (StringUtils.isBlank(record.get(transformationRulesHandler.getErrorReasonFieldName()))) {
            record.put(transformationRulesHandler.getErrorReasonFieldName(), UNCHECKED_RECORD);
        }

        //Convert record values.
        CustomEntityTemplate cet = customTableService.getCET(transformationRulesHandler.getStageTableName());
        Map<String, CustomFieldTemplate> cfts = customFieldTemplateService.findByAppliesTo(cet.getAppliesTo());

        for (Map.Entry<String, Object> valueEntry : record.entrySet()) {
            String key = valueEntry.getKey();
            Object value = valueEntry.getValue();
            CustomFieldTemplate cft = cfts.get(key);
            Class dataClass = cft.getFieldType().getDataClass();

            if (value != null && !dataClass.isAssignableFrom(value.getClass())) {
                try {
                    if (dataClass.equals(BigDecimal.class)) {
                        record.put(key, new BigDecimal((String) value));
                    } else if (dataClass.equals(Double.class)) {
                        record.put(key, Double.valueOf((String) value));
                    } else if (dataClass.equals(Long.class)) {
                        record.put(key, Long.valueOf((String) value));
                    } else if (dataClass.equals(Boolean.class)) {
                        record.put(key, Boolean.valueOf((String) value));
                    } else if (dataClass.equals(Date.class)) {
                        record.put(key, DateUtils.parseDateWithPattern((String) value, Utils.DATE_DEFAULT_PATTERN));
                    } else {
                        record.put(key, value);
                    }
                } catch (NumberFormatException nfe) {
                    record.put(key, null);
                }
            }
            if (cft.isValueRequired() && StringUtils.isBlank(record.get(key))) {
                throw new ValidationException("The " + key + " has invalid value.");
            }
        }
        return null;
    }

    /**
     * Validate staging records.
     *
     * @param importType the import type
     * @param fileId     the file id
     * @throws BusinessException the business exception
     */
    public void validateRecords(String importType, Long fileId) throws BusinessException {
        try {
            TransformationRulesHandler transformationRulesHandler = getTransformationRules(importType);
            if (isStagingRecordsExist(transformationRulesHandler.getStageTableName(), fileId)) {
                transformationRulesHandler.getValidateFunction().apply(new Object[]{importType, fileId});
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
    }

    /**
     * Create records in database
     *
     * @param tableName   the table name
     * @param tableFields the table fields
     * @param records     the  records list
     */
    public void createRecord(String tableName, Map<String, Object> tableFields, List<Map<String, Object>> records) {

        if (StringUtils.isBlank(tableName) || MapUtils.isEmpty(tableFields) || ListUtils.isEmtyCollection(records)) {
            return;
        }

        List<String> columns = new ArrayList<>();
        List<String> parameters = new ArrayList<>();
        for (Map.Entry<String, Object> valueEntry : tableFields.entrySet()) {
            String fieldName = valueEntry.getKey();
            columns.add(fieldName);
            parameters.add("?");
        }

        if (!columns.isEmpty()) {
            String sql = "insert into " + tableName + " (" + String.join(",", columns)
                    + ") VALUES (" + String.join(",", parameters) + ")";

            Session hibernateSession = getEntityManager().unwrap(Session.class);
            hibernateSession.doWork(connection -> {
                try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

                    for (int i = 0; i < records.size(); i++) {
                        Map<String, Object> record = records.get(i);
                        for (int j = 0; j < columns.size(); j++) {
                            String fieldName = columns.get(j);
                            Object fieldValue = record.get(fieldName);
                            if (fieldValue == null) {
                                fieldValue = tableFields.get(fieldName);
                            }
                            if (fieldValue instanceof String) {
                                preparedStatement.setString(j + 1, (String) fieldValue);
                            } else if (fieldValue instanceof Long) {
                                preparedStatement.setLong(j + 1, (Long) fieldValue);
                            } else if (fieldValue instanceof Double) {
                                preparedStatement.setDouble(j + 1, (Double) fieldValue);
                            } else if (fieldValue instanceof BigInteger) {
                                preparedStatement.setInt(j + 1, ((BigInteger) fieldValue).intValue());
                            } else if (fieldValue instanceof Integer) {
                                preparedStatement.setInt(j + 1, ((Integer) fieldValue).intValue());
                            } else if (fieldValue instanceof BigDecimal) {
                                preparedStatement.setBigDecimal(j + 1, (BigDecimal) fieldValue);
                            } else if (fieldValue instanceof Date) {
                                preparedStatement.setTimestamp(j + 1, new Timestamp(((Date) fieldValue).getTime()));
                            } else if (fieldValue instanceof Boolean) {
                                preparedStatement.setBoolean(j + 1, (Boolean) fieldValue);
                            } else if (fieldValue == null) {
                                preparedStatement.setNull(j + 1, Types.NULL);
                            }
                        }
                        preparedStatement.addBatch();
                        if (i > 0 && i % 500 == 0) {
                            preparedStatement.executeBatch();
                        }
                    }
                    preparedStatement.executeBatch();

                } catch (SQLException e) {
                    log.error("Failed to insert with sql {}", sql, e);
                    throw e;
                }
            });
        }
    }

    /**
     * Update records in database
     *
     * @param tableName     the table name
     * @param tableFields   the table fields
     * @param updatedFields the updated fields
     * @param records       the  records list
     */
    public void updateRecord(String tableName, Map<String, Object> tableFields, List<String> updatedFields, List<Map<String, Object>> records) {

        if (StringUtils.isBlank(tableName) || MapUtils.isEmpty(tableFields) || ListUtils.isEmtyCollection(updatedFields) || ListUtils.isEmtyCollection(records)) {
            return;
        }
        List<String> setFields = new ArrayList<>();
        List<String> fieldNames = new ArrayList<>();
        for (Map.Entry<String, Object> valueEntry : tableFields.entrySet()) {
            String fieldName = valueEntry.getKey();
            if (updatedFields.contains(fieldName) && !fieldName.equals(FIELD_ID)) {
                setFields.add(fieldName + "=?");
                fieldNames.add(fieldName);
            }
        }


        if (!setFields.isEmpty()) {

            String sql = "update " + tableName + " set " + String.join(",", setFields) + "  where id=?";

            Session hibernateSession = getEntityManager().unwrap(Session.class);
            hibernateSession.doWork(connection -> {
                try (PreparedStatement preparedStatement = connection.prepareStatement(sql)) {

                    for (int i = 0; i < records.size(); i++) {
                        Map<String, Object> record = records.get(i);
                        for (int j = 0; j < fieldNames.size(); j++) {
                            String fieldName = fieldNames.get(j);
                            Object fieldValue = record.get(fieldName);
                            if (fieldValue == null) {
                                fieldValue = tableFields.get(fieldName);
                            }
                            if (fieldValue instanceof String) {
                                preparedStatement.setString(j + 1, (String) fieldValue);
                            } else if (fieldValue instanceof Long) {
                                preparedStatement.setLong(j + 1, (Long) fieldValue);
                            } else if (fieldValue instanceof Double) {
                                preparedStatement.setDouble(j + 1, (Double) fieldValue);
                            } else if (fieldValue instanceof BigInteger) {
                                preparedStatement.setInt(j + 1, ((BigInteger) fieldValue).intValue());
                            } else if (fieldValue instanceof Integer) {
                                preparedStatement.setInt(j + 1, ((Integer) fieldValue).intValue());
                            } else if (fieldValue instanceof BigDecimal) {
                                preparedStatement.setBigDecimal(j + 1, (BigDecimal) fieldValue);
                            } else if (fieldValue instanceof Date) {
                                preparedStatement.setTimestamp(j + 1, new Timestamp(((Date) fieldValue).getTime()));
                            } else if (fieldValue instanceof Boolean) {
                                preparedStatement.setBoolean(j + 1, (Boolean) fieldValue);
                            } else if (fieldValue == null) {
                                preparedStatement.setNull(j + 1, Types.NULL);
                            }
                        }
                        preparedStatement.setLong(fieldNames.size() + 1, ((Number) record.get(FIELD_ID)).longValue());
                        preparedStatement.addBatch();
                        if (i > 0 && i % 500 == 0) {
                            preparedStatement.executeBatch();
                        }
                    }
                    preparedStatement.executeBatch();

                } catch (SQLException e) {
                    log.error("Failed to insert with sql {}", sql, e);
                    throw e;
                }
            });
        }
    }

    /**
     * Create record
     *
     * @param tableName the table name
     * @param record    the  record
     */
    private void createRecord(String tableName, Map<String, Object> record) {

        List<String> columns = new ArrayList<>();
        List<Object> values = new ArrayList<>();
        List<String> parameters = new ArrayList<>();
        for (Map.Entry<String, Object> valueEntry : record.entrySet()) {
            String fieldName = valueEntry.getKey();
            Object fieldValue = valueEntry.getValue();
            if (fieldValue != null) {
                columns.add(fieldName);
                values.add(fieldValue);
                parameters.add("?");
            }
        }

        if (!columns.isEmpty()) {
            String sql = "insert into " + tableName + " (" + String.join(",", columns)
                    + ") VALUES (" + String.join(",", parameters) + ")";
            Query query = getEntityManager().createNativeQuery(sql);
            for (int i = 0; i < values.size(); i++) {
                query.setParameter(i + 1, values.get(i));
            }
            query.executeUpdate();
        }
    }

    /**
     * Update record
     *
     * @param tableName the table name
     * @param record    the  record
     */
    private void updateRecord(String tableName, Map<String, Object> record) {

        List<String> fields = new ArrayList<>();
        for (Map.Entry<String, Object> valueEntry : record.entrySet()) {
            String fieldName = valueEntry.getKey();
            Object fieldValue = valueEntry.getValue();
            if (fieldValue != null) {
                fields.add(fieldName + "=:" + fieldName);
            } else {
                fields.add(fieldName + "=null");
            }
        }

        if (!fields.isEmpty()) {
            String sql = "update " + tableName + " set " + String.join(",", fields) + "  where id=:id";
            Query query = getEntityManager().createNativeQuery(sql);
            for (String fieldName : record.keySet()) {
                if (record.get(fieldName) != null) {
                    query.setParameter(fieldName, record.get(fieldName));
                }
            }
            query.executeUpdate();
        }
    }

    /**
     * Set the hash code
     *
     * @param record                     the tariff record
     * @param transformationRulesHandler the transformation rules handler
     * @throws BusinessException the business exception
     */
    public void setHashCode(Map<String, Object> record, TransformationRulesHandler transformationRulesHandler) throws BusinessException {
        StringBuilder recordCode = new StringBuilder();
        for (TransformationRule transformationRule : transformationRulesHandler.getTransformationRules()) {
            if (transformationRule.isMatchedField() && !StringUtils.isBlank(record.get(transformationRule.getStageFieldName()))) {
                recordCode.append(record.get(transformationRule.getStageFieldName()) + "|@|");
            }
        }
        if (recordCode.length() > 0) {
            try {
                record.put("hash_code", Utils.generateMD5(recordCode.toString()));
            } catch (HashGenerationException e) {
                throw new BusinessException(e.getMessage(), e);
            }
        }
    }

    /**
     * Set the priority
     *
     * @param record                     the tariff record
     * @param transformationRulesHandler the transformation rules handler
     */
    public void setPriority(Map<String, Object> record, TransformationRulesHandler transformationRulesHandler) {
        StringBuilder recordPriority = new StringBuilder();
        for (TransformationRule transformationRule : transformationRulesHandler.getTransformationRules()) {
            Integer fieldPriority = businessRulesValidator.getFieldPriority(transformationRule, record.get(transformationRule.getStageFieldName()));
            if (fieldPriority != null) {
                recordPriority.append(fieldPriority);
            }
        }
        if (recordPriority.length() > 0) {
            record.put("priority", recordPriority.toString());
        }
    }

}