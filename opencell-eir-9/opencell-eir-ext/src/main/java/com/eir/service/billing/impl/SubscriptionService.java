package com.eir.service.billing.impl;

import org.meveo.commons.utils.QueryBuilder;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.SubscriptionStatusEnum;

import javax.ejb.Stateless;
import javax.enterprise.inject.Specializes;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

/**
 * EIR subscription service
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirSubscriptionService")
@Specializes
public class SubscriptionService extends org.meveo.service.billing.impl.SubscriptionService {

    /**
     * Find subscription by code like.
     *
     * @param subscriptionCode the subscription code
     * @return the list
     */
    @SuppressWarnings("unchecked")
    public List<Subscription> findNonTerminatedSubscriptionsByCodeWithoutPrefix(String subscriptionCode) {

        if (subscriptionCode != null && subscriptionCode.matches(".*\\d.*")) {
            subscriptionCode = subscriptionCode.replaceAll("[^\\d]", "");
            QueryBuilder queryBuilder = new QueryBuilder(Subscription.class, "a", null);
            queryBuilder.addSqlCriterion("bigIntFromJson(cfValues, SERVICE_ID, long) = " + new BigInteger(subscriptionCode), null, null);
            //queryBuilder.addCriterion("code", "like", "%" + subscriptionCode, true);
            queryBuilder.addCriterion("status", "<>", SubscriptionStatusEnum.RESILIATED, false);
            queryBuilder.addCriterion("status", "<>", SubscriptionStatusEnum.CANCELED, false);
            return queryBuilder.getQuery(getEntityManager()).getResultList();
        }

        return new ArrayList<Subscription>();

    }

    /**
     * Find subscriptions by UAN.
     *
     * @param uan UAN to use for search
     * @return the list of subscriptions matching the UAN
     */
    @SuppressWarnings("unchecked")
    public List<Subscription> findSubscriptionsByUAN(String uan) {
        String queryString = "SELECT * FROM BILLING_SUBSCRIPTION WHERE "
                + "cf_values\\:\\:JSON->'UAN'->0->>'string' = :uan";
        Query query = getEntityManager().createNativeQuery(queryString, Subscription.class);
        query.setParameter("uan", uan);
        return query.getResultList();
    }

    /**
     * Count active subscriptions by UAN.
     *
     * @param uan UAN to use for search
     * @return the number of active subscriptions matching the UAN
     */
    public Long countActiveSubscriptionsByUAN(String uan) {
        String queryString = "SELECT count(ID) FROM BILLING_SUBSCRIPTION WHERE status = 'ACTIVE' AND "
                + "cf_values\\:\\:JSON->'UAN'->0->>'string' = :uan";
        Query query = getEntityManager().createNativeQuery(queryString);
        query.setParameter("uan", uan);
        return ((BigInteger) query.getSingleResult()).longValue();
    }

    /**
     * Find active subscriptions by code like.
     *
     * @param subscriptionCode the subscription code
     * @return Subscription list
     */
    @SuppressWarnings("unchecked")
    public List<Subscription> findActiveSubscriptionsByCodeWithoutPrefix(String subscriptionCode) {
        if (subscriptionCode != null && subscriptionCode.matches(".*\\d.*")) {
            subscriptionCode = subscriptionCode.replaceAll("[^\\d]", "");
            QueryBuilder queryBuilder = new QueryBuilder(Subscription.class, "a", null);
            queryBuilder.addSqlCriterion("bigIntFromJson(cfValues, SERVICE_ID, long) = " + new BigInteger(subscriptionCode), null, null);
            // queryBuilder.addCriterion("code", "like", "%" + subscriptionCode, true);
            queryBuilder.addCriterion("status", "=", SubscriptionStatusEnum.ACTIVE, false);
            return queryBuilder.getQuery(getEntityManager()).setMaxResults(1).getResultList();
        }
        return new ArrayList<Subscription>();
    }

    /**
     * Find by original code
     *
     * @param subscriptionCode the subscription code
     * @return the subscription
     */
    public Subscription findByOriginalCode(String subscriptionCode) {
        if (!StringUtils.isBlank(subscriptionCode)) {
            Subscription subscription = findByCode(subscriptionCode);
            if (subscription != null) {
                return subscription;
            }
            String queryString = "select * from billing_subscription where (cf_values\\:\\:JSON->'ORIGINAL_CODE'->0->>'string') = :subscriptionCode";
            Query query = getEntityManager().createNativeQuery(queryString, Subscription.class);
            query.setParameter("subscriptionCode", subscriptionCode);
            List<Subscription> subscriptions = query.getResultList();
            if (!subscriptions.isEmpty()) {
                return subscriptions.get(0);
            }
        }
        return null;
    }
}

