package com.eir.service.mediation.impl;

import org.meveo.api.commons.Utils;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.billing.Subscription;
import org.meveo.model.mediation.Access;

import javax.ejb.Stateless;
import javax.enterprise.inject.Specializes;
import javax.persistence.Query;
import java.util.Date;
import java.util.List;

/**
 * EIR access service
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirAccessService")
@Specializes
public class AccessService extends org.meveo.service.medina.impl.AccessService {

    /**
     * Get list of access point by user id
     *
     * @param accessUserId the access user id.
     * @return List of Access
     */
    public List<Access> getAccessByUserId(String accessUserId) {
        List<Access> listAccess = null;
        if (!StringUtils.isBlank(accessUserId)) {
            Query query = getEntityManager()
                    // .createQuery("SELECT a from Access a left join fetch a.subscription where a.disabled=false and regexp_replace(a.accessUserId, '.*_|-', '', 'g')=:accessUserId");
                    .createQuery("SELECT a from Access a left join fetch a.subscription where a.disabled=false and regexp_replace(a.accessUserId, '-', '', 'g')=:accessUserId");
            query.setParameter("accessUserId", accessUserId);
            listAccess = (List<Access>) query.getResultList();
        }
        return listAccess;
    }

    /**
     * Gets the access by user id and date.
     *
     * @param accessUserId the access user id
     * @param eventDate    the event date
     * @return the access by user id and date
     */
    public List<Access> getAccess(String accessUserId, Date eventDate) {
        List<Access> listAccess = null;
        if (!StringUtils.isBlank(accessUserId)) {
            String accessUserIdCondition = "a.accessUserId=:accessUserId";
            if (Utils.isInteger(accessUserId)) {
                accessUserIdCondition = "regexp_replace(regexp_replace(a.accessUserId, '^[0]', '', 'g'), '-', '', 'g')=:accessUserId";
            }
            Query query = getEntityManager()
                    .createQuery("SELECT a from Access a where a.disabled=false and " + accessUserIdCondition +
                            " and ((a.startDate is null or a.startDate <= :eventDate) and (a.endDate is null or a.endDate > :eventDate))");
            query.setParameter("accessUserId", accessUserId);
            query.setParameter("eventDate", eventDate);
            listAccess = (List<Access>) query.getResultList();
        }
        return listAccess;
    }

    /**
     * Gets the access by subscription, user id and date.
     *
     * @param accessUserId the access user id
     * @return the access by user id and date
     */
    public Access getAccess(Subscription subscription, String accessUserId) {
        Access access = null;
        if (!StringUtils.isBlank(accessUserId)) {
            Query query = getEntityManager()
                    .createQuery("SELECT a from Access a where a.disabled=false and a.subscription=:subscription and " +
                            "a.accessUserId=:accessUserId and a.startDate is null ");
            query.setParameter("subscription", subscription);
            query.setParameter("accessUserId", accessUserId);
            access = ((List<Access>) query.getResultList()).get(0);
        }
        return access;
    }
}

