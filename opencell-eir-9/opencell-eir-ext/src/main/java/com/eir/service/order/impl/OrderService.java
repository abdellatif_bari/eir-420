package com.eir.service.order.impl;

import com.eir.commons.enums.customfields.OrderCFsEnum;
import com.eir.commons.enums.customtables.AssociationCustomTableEnum;
import com.eir.commons.enums.customtables.EndPointCustomTableEnum;
import com.eir.service.commons.UtilService;
import org.hibernate.Query;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.api.commons.Utils;
import org.meveo.api.custom.CustomTableApi;
import org.meveo.api.dto.custom.CustomTableRecordDto;
import org.meveo.api.dto.custom.UnitaryCustomTableDataDto;
import org.meveo.api.dto.order.OrderDto;
import org.meveo.api.dto.order.OrderLineDto;
import org.meveo.api.exception.MeveoApiException;
import org.meveo.commons.utils.QueryBuilder;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.BusinessEntity;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.customEntities.CustomEntityTemplate;
import org.meveo.model.order.Order;
import org.meveo.model.order.OrderStatusEnum;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.base.BusinessService;
import org.meveo.service.custom.CustomTableService;
import org.primefaces.model.SortOrder;

import javax.ejb.Stateless;
import javax.enterprise.inject.Specializes;
import javax.inject.Inject;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * EIR order service
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirOrderService")
@Specializes
public class OrderService extends org.meveo.service.order.OrderService {

    @Inject
    private CustomTableService customTableService;
    @Inject
    private CustomTableApi customTableApi;
    @Inject
    private UtilService utilService;

    /**
     * Find complementary order.
     *
     * @param orderCode the order code
     * @return complementary order
     */
    public Order findComplementaryOrder(String orderCode) {
        if (!StringUtils.isBlank(orderCode)) {
            QueryBuilder queryBuilder = new QueryBuilder(Order.class, "o", null);
            queryBuilder.addCriterion("code", "!=", orderCode, true);
            queryBuilder.addCriterionInList("status", new ArrayList<>(Arrays.asList(OrderStatusEnum.DEFERRED, OrderStatusEnum.COMPLETED)), " in ", false);
            //queryBuilder.addCriterion("cf_values", "like", "%<Ref_Order_Id>" + orderCode + "</Ref_Order_Id>%", true);
            queryBuilder.addSqlCriterion("varcharFromJson(cfValues, " + OrderCFsEnum.ORDER_REF_ID.name() + ", string) =:refOrderCode", "refOrderCode", orderCode);
            List<Order> orders = queryBuilder.getQuery(getEntityManager()).getResultList();
            if (!orders.isEmpty()) {
                return orders.get(0);
            }
        }
        return null;
    }

    /**
     * Find pending rejected waiting orders.
     *
     * @param orderDto    the order dto
     * @param onlySubList the only sub list
     * @return the list
     */
    public List<String> findPendingRejectedWaitingOrders(OrderDto orderDto, Boolean onlySubList) {
        List<OrderLineDto> lines = orderDto.getOrderLines();
        if (lines == null || lines.isEmpty()) {
            return null;
        }
        //If all Service Ids beginning with 00- , the order is not to be held. And also not consider 00- service Ids in general order
//        Optional<OrderLineDto> specialOrderLine = lines.stream()
//                .filter(orderLine -> orderLine.getOriginalSubscriptionCode() == null || !orderLine.getOriginalSubscriptionCode().startsWith("00-"))
//                .filter(orderLine -> orderLine.getSubscriptionCode() == null || !orderLine.getSubscriptionCode().startsWith("00-"))
//                .findAny();
//        
//        if (specialOrderLine.isEmpty()) {
//            return null;
//        }
//                
//        lines = lines.stream()
//                .filter(orderLine -> orderLine.getOriginalSubscriptionCode() == null || !orderLine.getOriginalSubscriptionCode().startsWith("00-"))
//                .filter(orderLine -> orderLine.getSubscriptionCode() == null || !orderLine.getSubscriptionCode().startsWith("00-"))
//                .collect(Collectors.toList());

        List<String> listServiceId = new ArrayList<String>();
        for (OrderLineDto orderLine : lines) {
            if (StringUtils.isNotBlank(orderLine.getOriginalSubscriptionCode())) {
                if (!orderLine.getOriginalSubscriptionCode().startsWith("00-") && !listServiceId.contains(orderLine.getOriginalSubscriptionCode())) {
                    listServiceId.add(orderLine.getOriginalSubscriptionCode());
                }
            } else if (StringUtils.isNotBlank(orderLine.getSubscriptionCode())) {
                if (!orderLine.getSubscriptionCode().startsWith("00-") && !listServiceId.contains(orderLine.getSubscriptionCode())) {
                    listServiceId.add(orderLine.getSubscriptionCode());
                }
            }

            if (StringUtils.isNotBlank(orderLine.getRefSubscriptionCode()) && !orderLine.getRefSubscriptionCode().startsWith("00-") && !listServiceId.contains(orderLine.getRefSubscriptionCode())) {
                listServiceId.add(orderLine.getRefSubscriptionCode());
            }
        }

        if (listServiceId.isEmpty()) {
            return null;
        }
        String query = "SELECT code FROM ord_order where code <> :orderCode AND status in ('PENDING','REJECTED','WAITING') AND cf_values\\:\\:jsonb->'SERVICE_IDS'->0->'listString' ^| array[:serviceIds]";
        if ((listServiceId.size() + 4) < Utils.MAX_NUMBER_JDBC_PSG_PARAMETERS) {
            return getEntityManager().createNativeQuery(query.replace(":serviceIds", getStringFromList(listServiceId))).setParameter("orderCode", orderDto.getOrderCode()).setMaxResults(1).getResultList();
        } else {
            List<String> finalList = new ArrayList<>();
            List<String> subList;
            int maxServiceIdParams = Utils.MAX_NUMBER_JDBC_PSG_PARAMETERS - 4; // We have 4 parameters  + serviceId  list          
            int fromIndex = 0;
            int toIndex = maxServiceIdParams;
            while (fromIndex < listServiceId.size()) {
                List<String> listServiceId2 = listServiceId.subList(fromIndex, toIndex);
                subList = getEntityManager().createNativeQuery(query.replace(":serviceIds", getStringFromList(listServiceId2))).setParameter("orderCode", orderDto.getOrderCode()).setMaxResults(1).getResultList();
                if (subList != null && subList.size() > 0) {
                    if (onlySubList) {
                        return subList;
                    }
                    finalList.addAll(subList);
                }
                fromIndex = toIndex;
                if (listServiceId.size() > maxServiceIdParams + fromIndex) {
                    toIndex = maxServiceIdParams + fromIndex;
                } else {
                    toIndex = listServiceId.size();
                }
            }
            return finalList;
        }
    }

    /**
     * Indicates is there an endpoint that overlaps with provided date range
     *
     * @param orderLineDto   the order line Dto
     * @param subscriptionId the subscription id
     * @param lastEndPoint   the last endpoint
     * @param startDate      the start date
     * @return true is there an endpoint that overlaps with provided date range else if not.
     */
    public boolean isOverlapEndPoint(OrderLineDto orderLineDto, Long subscriptionId, Map<String, Object> lastEndPoint, Date startDate) {
        List<Map<String, Object>> endPoints = getOverlapEndPoints(orderLineDto, subscriptionId);
        if (endPoints != null && !endPoints.isEmpty()) {
            if (endPoints.size() == 1 && lastEndPoint != null && ((BigInteger) lastEndPoint.get(EndPointCustomTableEnum.ID.getLabel()))
                    .compareTo((BigInteger) endPoints.get(0).get(EndPointCustomTableEnum.ID.getLabel())) == 0 &&
                    endPoints.get(0).get(EndPointCustomTableEnum.EPSTART.getLabel()) != null &&
                    startDate.compareTo((Date) endPoints.get(0).get(EndPointCustomTableEnum.EPSTART.getLabel())) > 0 &&
                    endPoints.get(0).get(EndPointCustomTableEnum.EP_END_DATE.getLabel()) == null) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Get the endpoints list that overlaps with provided date range
     *
     * @param orderLineDto   the order line Dto
     * @param subscriptionId the subscription id
     * @return the endpoints list that overlaps with provided date range.
     */
    public List<Map<String, Object>> getOverlapEndPoints(OrderLineDto orderLineDto, Long subscriptionId) {
        QueryBuilder queryBuilder = customTableService.getQuery(EndPointCustomTableEnum.getCustomTableCode(), null);
        queryBuilder.addCriterion(EndPointCustomTableEnum.SUBSCRIPTION_ID.getLabel(), "=", subscriptionId, false);
        queryBuilder.addCriterion(EndPointCustomTableEnum.EPEND.getLabel(), "=", orderLineDto.getAttribute(EndPointCustomTableEnum.EPEND.name()), true);

        utilService.addOverlapCriteria(queryBuilder, EndPointCustomTableEnum.EPSTART.getLabel(), EndPointCustomTableEnum.EP_END_DATE.getLabel(),
                DateUtils.parseDateWithPattern(orderLineDto.getAttribute(EndPointCustomTableEnum.EPSTART.name()), Utils.DATE_TIME_PATTERN), null);

        Query query = queryBuilder.getNativeQuery(getEntityManager(), true);
        List<Map<String, Object>> endPoints = query.list();
        if (endPoints != null && !endPoints.isEmpty()) {
            return endPoints;
        }
        return null;
    }

    /**
     * Indicates is there an association that overlaps with provided date range
     *
     * @param orderLineDto    the order line Dto
     * @param subscriptionId  the subscription id
     * @param lastAssociation the last association
     * @return true is there an association that overlaps with provided date range else if not.
     */
    public boolean isOverlapAssociation(OrderLineDto orderLineDto, Long subscriptionId, Map<String, Object> lastAssociation) {
        List<Map<String, Object>> associations = getOverlapAssociations(orderLineDto, subscriptionId);
        if (associations != null && !associations.isEmpty()) {
            if (associations.size() == 1 && lastAssociation != null && ((BigInteger) lastAssociation.get(AssociationCustomTableEnum.ID.getLabel()))
                    .compareTo((BigInteger) associations.get(0).get(AssociationCustomTableEnum.ID.getLabel())) == 0 &&
                    associations.get(0).get(AssociationCustomTableEnum.ASSOCSTART.getLabel()) != null &&
                    DateUtils.parseDateWithPattern(orderLineDto.getAttribute(AssociationCustomTableEnum.ASSOCSTART.name()), Utils.DATE_TIME_PATTERN)
                            .compareTo((Date) associations.get(0).get(AssociationCustomTableEnum.ASSOCSTART.getLabel())) > 0 &&
                    associations.get(0).get(AssociationCustomTableEnum.ASSOC_END_DATE.getLabel()) == null) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Get the associations list that overlaps with provided date range
     *
     * @param orderLineDto   the order line Dto
     * @param subscriptionId the subscription id
     * @return the associations list that overlaps with provided date range.
     */
    public List<Map<String, Object>> getOverlapAssociations(OrderLineDto orderLineDto, Long subscriptionId) {
        QueryBuilder queryBuilder = customTableService.getQuery(AssociationCustomTableEnum.getCustomTableCode(), null);
        queryBuilder.addCriterion(AssociationCustomTableEnum.SUBSCRIPTION_ID.getLabel(), "=", subscriptionId, false);
        queryBuilder.addCriterion(AssociationCustomTableEnum.ASSOCEND.getLabel(), "=", orderLineDto.getAttribute(AssociationCustomTableEnum.ASSOCEND.name()), true);

        utilService.addOverlapCriteria(queryBuilder, AssociationCustomTableEnum.ASSOCSTART.getLabel(), AssociationCustomTableEnum.ASSOC_END_DATE.getLabel(),
                DateUtils.parseDateWithPattern(orderLineDto.getAttribute(AssociationCustomTableEnum.ASSOCSTART.name()), Utils.DATE_TIME_PATTERN), null);

        Query query = queryBuilder.getNativeQuery(getEntityManager(), true);
        List<Map<String, Object>> associations = query.list();
        if (associations != null && !associations.isEmpty()) {
            return associations;
        }
        return null;
    }

    /**
     * Find the latest version for an end point
     *
     * @param subscriptionId the subscription id
     * @param endpointEnd    the endpoint end
     * @return the latest version for an end point
     */
    public Map<String, Object> findLastVersionEndPoint(Long subscriptionId, String endpointEnd) {
        if (subscriptionId != null && !StringUtils.isBlank(endpointEnd)) {
            List<Map<String, Object>> listEndPoints;
            Map<String, Object> filters = new HashMap<>();
            filters.put(EndPointCustomTableEnum.SUBSCRIPTION_ID.getLabel(), subscriptionId);
            filters.put(EndPointCustomTableEnum.EPEND.getLabel(), endpointEnd);
            PaginationConfiguration paginationConfiguration = new PaginationConfiguration(filters, EndPointCustomTableEnum.EPSTART.getLabel(), SortOrder.DESCENDING);
            listEndPoints = customTableService.list(EndPointCustomTableEnum.getCustomTableCode(), paginationConfiguration);
            if (!listEndPoints.isEmpty()) {
                return listEndPoints.get(0);
            }
        }
        return null;
    }

    /**
     * Find the latest version for an association
     *
     * @param subscriptionId the subscription id
     * @param associationEnd the association end
     * @return the latest version for an association
     */
    public Map<String, Object> findLastVersionAssociation(Long subscriptionId, String associationEnd) {
        if (subscriptionId != null && !StringUtils.isBlank(associationEnd)) {
            List<Map<String, Object>> listAssociations;
            Map<String, Object> filters = new HashMap<>();
            filters.put(AssociationCustomTableEnum.SUBSCRIPTION_ID.getLabel(), subscriptionId);
            filters.put(AssociationCustomTableEnum.ASSOCEND.getLabel(), associationEnd);
            PaginationConfiguration paginationConfiguration = new PaginationConfiguration(filters, AssociationCustomTableEnum.ASSOCSTART.getLabel(), SortOrder.DESCENDING);
            listAssociations = customTableService.list(AssociationCustomTableEnum.getCustomTableCode(), paginationConfiguration);
            if (!listAssociations.isEmpty()) {
                return listAssociations.get(0);
            }
        }
        return null;
    }

    /**
     * Find the latest version for an end point
     *
     * @param subscriptionId the subscription id
     * @param epend          the end point end
     * @param epstart        the end point end start date
     * @return the latest version for an end point
     */
    public Map<String, Object> findEndPoint(Long subscriptionId, String epend, Date epstart) {
        QueryBuilder queryBuilder = customTableService.getQuery(EndPointCustomTableEnum.getCustomTableCode(), null);
        queryBuilder.addCriterion(EndPointCustomTableEnum.EPSTART.getLabel(), " <= ", epstart, false, false);
        queryBuilder.addSqlCriterion("(" + EndPointCustomTableEnum.EP_END_DATE.getLabel() + ">=:epstart OR " + EndPointCustomTableEnum.EP_END_DATE.getLabel() +
                " IS NULL )", "epstart", epstart);
        queryBuilder.addCriterion(EndPointCustomTableEnum.SUBSCRIPTION_ID.getLabel(), "=", subscriptionId, false);
        queryBuilder.addCriterion(EndPointCustomTableEnum.EPEND.getLabel(), "=", epend, true);
        queryBuilder.addOrderMultiCriterion(EndPointCustomTableEnum.EPSTART.getLabel(), false, EndPointCustomTableEnum.EP_END_DATE.getLabel(), false);
        Query query = queryBuilder.getNativeQuery(getEntityManager(), true);
        List<Map<String, Object>> listEndPoints = query.list();
        if (!listEndPoints.isEmpty()) {
            return listEndPoints.get(0);
        }
        return null;
    }

    /**
     * Find the latest version for an end point
     *
     * @param subscriptionId the subscription id
     * @param endPointsEnd   the end points end
     * @param epstart        the end point end start date
     * @return the latest version for an end point
     */
    public List<Map<String, Object>> findEndPoints(Long subscriptionId, List<String> endPointsEnd, Date epstart) {
        QueryBuilder queryBuilder = customTableService.getQuery(EndPointCustomTableEnum.getCustomTableCode(), null);
        queryBuilder.addCriterion(EndPointCustomTableEnum.EPSTART.getLabel(), " <= ", epstart, false, false);
        queryBuilder.addSqlCriterion("(" + EndPointCustomTableEnum.EP_END_DATE.getLabel() + ">=:epstart OR " + EndPointCustomTableEnum.EP_END_DATE.getLabel() +
                " IS NULL )", "epstart", epstart);
        queryBuilder.addCriterion(EndPointCustomTableEnum.SUBSCRIPTION_ID.getLabel(), "=", subscriptionId, true);
        queryBuilder.addCriterionInList(EndPointCustomTableEnum.EPTYPE.getLabel(), endPointsEnd, " in ", false);
        queryBuilder.addOrderMultiCriterion(EndPointCustomTableEnum.EPSTART.getLabel(), false, EndPointCustomTableEnum.EP_END_DATE.getLabel(), false);
        Query query = queryBuilder.getNativeQuery(getEntityManager(), true);
        return query.list();
    }

    /**
     * Update the end point
     *
     * @param values the end point values
     * @throws BusinessException the business exception
     */
    public void updateEndPoint(Map<String, Object> values) throws BusinessException {
        CustomEntityTemplate cet = customTableService.getCET(EndPointCustomTableEnum.getCustomTableCode());
        Map<String, CustomFieldTemplate> cfts = customTableService.retrieveAndValidateCfts(cet, false);
        List<Map<String, Object>> newValues = customTableService.convertValues(Arrays.asList(values), cfts.values(), false);
        customTableService.update(cet.getDbTablename(), newValues.get(0));
    }

    /**
     * Update the association
     *
     * @param values the association values
     * @throws BusinessException the business exception
     */
    public void updateAssociation(Map<String, Object> values) throws BusinessException {
        CustomEntityTemplate cet = customTableService.getCET(AssociationCustomTableEnum.getCustomTableCode());
        Map<String, CustomFieldTemplate> cfts = customTableService.retrieveAndValidateCfts(cet, false);
        List<Map<String, Object>> newValues = customTableService.convertValues(Arrays.asList(values), cfts.values(), false);
        customTableService.update(cet.getDbTablename(), newValues.get(0));
    }

    /**
     * Save the end point values
     *
     * @param values a end point values
     * @throws MeveoApiException the meveo api exception
     * @throws BusinessException the business exception
     */
    public void saveEndPoint(Map<String, Object> values) throws MeveoApiException, BusinessException {
        UnitaryCustomTableDataDto endPoint = new UnitaryCustomTableDataDto();
        endPoint.setCustomTableCode(EndPointCustomTableEnum.getCustomTableCode());
        CustomTableRecordDto value = new CustomTableRecordDto(values);
        endPoint.setValue(value);
        customTableApi.create(endPoint);
    }

    /**
     * Save the association values
     *
     * @param values a association values
     * @throws MeveoApiException the meveo api exception
     * @throws BusinessException the business exception
     */
    public void saveAssociation(Map<String, Object> values) throws MeveoApiException, BusinessException {
        UnitaryCustomTableDataDto endPoint = new UnitaryCustomTableDataDto();
        endPoint.setCustomTableCode(AssociationCustomTableEnum.getCustomTableCode());
        CustomTableRecordDto value = new CustomTableRecordDto(values);
        endPoint.setValue(value);
        customTableApi.create(endPoint);
    }

    /**
     * Get the order
     *
     * @param orderCode the order code
     * @return the order
     * @throws BusinessException the business exception
     */
    public Order getOrder(String orderCode) throws BusinessException {
        Order order = findByCode(orderCode);
        if (order == null) {
            throw new ValidationException("The order " + orderCode + " is not found");
        }
        if (order.getStatus() == OrderStatusEnum.COMPLETED) {
            throw new ValidationException("The order " + orderCode + " is already completed!");
        }
        if (order.getStatus() == OrderStatusEnum.CANCELLED) {
            throw new ValidationException("The order " + orderCode + " is already cancelled!");
        }
        if (order.getStatus() == OrderStatusEnum.IN_PROGRESS) {
            throw new ValidationException("The order " + orderCode + " is being processed, try to reprocess / release it later");
        }
        return order;
    }

    /**
     * Get the order Dto
     *
     * @param orderCode the order code
     * @return the order Dto
     * @throws BusinessException the business exception
     */
    public OrderDto getOrderDto(String orderCode) throws BusinessException {
        return getOrderDto(orderCode, null);
    }

    /**
     * Get the order Dto
     *
     * @param orderCode the order code
     * @param xmlSchema the xml schema
     * @return the order Dto
     * @throws BusinessException the business exception
     */
    public OrderDto getOrderDto(String orderCode, String xmlSchema) throws BusinessException {
        Order order = getOrder(orderCode);
        return getOrderDto(order, xmlSchema);
    }

    /**
     * Get the order Dto.
     *
     * @param order     the order
     * @param xmlSchema the xml schema
     * @return the order Dto
     * @throws BusinessException the business exception
     */
    public OrderDto getOrderDto(Order order, String xmlSchema) throws BusinessException {
        String xmlOrder = (String) order.getCfValue("ORDER_ORIGINAL_MESSAGE");
        if (StringUtils.isBlank(xmlOrder)) {
            throw new BusinessException("The ORDER_ORIGINAL_MESSAGE custom field is missing");
        }
        //xmlOrder = xmlOrder.replaceAll(Pattern.quote("\\"), "|");
        OrderDto orderDto = Utils.getDtoFromXml(xmlOrder, OrderDto.class, xmlSchema);
        orderDto.setOrderId(order.getId());
        orderDto.setStatus(order.getStatus());
        orderDto.setOriginalXmlOrder(xmlOrder);
        return orderDto;
    }

    /**
     * Gets the held orders to reprocess.
     *
     * @param orderCode the order code
     * @return the held orders to reprocess
     */
    public List<String> getHeldOrdersToReprocess(String orderCode) {
        String query = "SELECT code FROM ord_order where code <> :code AND status = 'HELD' AND (cf_values\\:\\:JSON->'HELD_BY'->0->>'string') = :code";
        return getEntityManager().createNativeQuery(query).setParameter("code", orderCode).getResultList();
    }

    /**
     * Find waiting orders.
     *
     * @param orderCode  the order code
     * @param circuitIds the circuit ids
     * @return the list of waiting orders that the circuit id matches with service_id field
     */
    @SuppressWarnings("unchecked")
    public List<Long> findWaitingOrders(String orderCode, List circuitIds) {
        String query = "SELECT id FROM ord_order where code <> :code AND status = 'WAITING' AND cf_values\\:\\:jsonb->'SERVICE_IDS'->0->'listString'  ^| array[:serviceIds]";
        List<BigInteger> orderCodes = getEntityManager().createNativeQuery(query.replace(":serviceIds", getStringFromList(circuitIds))).setParameter("code", orderCode).getResultList();
        return orderCodes.stream().map(d -> d.longValue()).collect(Collectors.toList());
    }

    /**
     * Gets the waiting orders.
     *
     * @return the waiting orders
     */
    public List<Order> getWaitingOrders() {
        QueryBuilder queryBuilder = new QueryBuilder(Order.class, "a", null);
        queryBuilder.addCriterion("status", "=", OrderStatusEnum.WAITING, false);
        queryBuilder.addOrderCriterion("code", true);
        return queryBuilder.getQuery(getEntityManager()).getResultList();
    }

    /**
     * Check if sla configuration exist
     *
     * @param serviceTemplateId the service template id
     * @param slaCode           the sla code
     * @param slaDate           the sla date
     * @return true if the sla configuration exist
     */
    public boolean isSlaConfigurationExist(Long serviceTemplateId, String slaCode, Date slaDate) {
        QueryBuilder queryBuilder = customTableService.getQuery("eir_or_sla", null);
        queryBuilder.addCriterion("service_id", "=", serviceTemplateId, false, true);
        queryBuilder.addCriterion("code", "=", slaCode, false);
        utilService.addOverlapCriteria(queryBuilder, "valid_from", "valid_to", slaDate, null);

        Query query = queryBuilder.getNativeQuery(getEntityManager(), true);
        List<Map<String, Object>> slaConfigs = query.list();
        if (slaConfigs != null && !slaConfigs.isEmpty()) {
            return true;
        }
        return false;
    }

    private String getStringFromList(List<String> list) {
        if (list != null && list.size() > 0) {
            String result = "'" + list.get(0) + "'";
            for (int i = 1; i < list.size(); i++) {
                result += ",'" + list.get(i) + "'";
            }
            return result;
        }

        return "''";
    }

    /**
     * Find the business entity by it's id or it's code.
     *
     * @param businessService the business service
     * @param orderDto        the order Dto
     * @param code            the entity code
     * @param <E>             the business entity
     * @return the business entity
     */
    public <E extends BusinessEntity> E findEntityByIdOrCode(BusinessService<E> businessService, OrderDto orderDto, String code) {
        if (businessService == null || orderDto == null || StringUtils.isBlank(code)) {
            return null;
        }
        E entity = null;
        String key = businessService.getEntityClass().getSimpleName() + "_" + code;
        Long id = (Long) orderDto.getEntities().get(key);
        if (id != null) {
            entity = businessService.findById(id);
        }
        if (entity == null) {
            entity = businessService.findByCode(code);
            if (entity != null) {
                orderDto.getEntities().put(key, entity.getId());
            }
        }
        return entity;
    }

    /**
     * Store the new entity code in the cache and remove the old one.
     *
     * @param businessService the business service
     * @param entity          the entity
     * @param orderDto        the order Dto
     * @param newCode         the new code
     * @param <E>             the business entity
     * @return the business entity
     */
    public <E extends BusinessEntity> E updateEntityCode(BusinessService<E> businessService, E entity, OrderDto orderDto, String newCode) {
        if (businessService == null || entity == null || orderDto == null || StringUtils.isBlank(newCode)) {
            return null;
        }
        String oldCode = entity.getCode();
        entity.setCode(newCode);
        entity = businessService.update(entity);
        String simpleName = businessService.getEntityClass().getSimpleName();
        orderDto.getEntities().remove(simpleName + "_" + oldCode);
        orderDto.getEntities().put(simpleName + "_" + newCode, entity.getId());
        return entity;
    }
}

