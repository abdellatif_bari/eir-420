package com.eir.service.migration;

import java.util.List;
import java.util.Map;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.meveo.jpa.JpaAmpNewTx;

@Stateless
public class SubscriptionsMigrationServiceAPI extends SubscriptionsMigrationService {

    @JpaAmpNewTx
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public void migrateSubscriptions(List<Map<String, Object>> records) {    
        super.migrateSubscriptions(records);
    }
}
