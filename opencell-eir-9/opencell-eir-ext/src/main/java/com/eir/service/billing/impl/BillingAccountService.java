package com.eir.service.billing.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.enterprise.inject.Specializes;
import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.model.sequence.GenericSequence;
import org.meveo.model.sequence.SequenceTypeEnum;
import org.meveo.service.billing.impl.ServiceSingleton;

/**
 * EIR billing account service
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirBillingAccountService")
@Specializes
public class BillingAccountService extends org.meveo.service.billing.impl.BillingAccountService {

    @Inject
    ServiceSingleton serviceSingleton;

    /**
     * find billing account by his number
     *
     * @param billingAccountNumber the billing account number
     * @return the billing account
     * @throws BusinessException the business exception
     */
    public BillingAccount findByNumber(String billingAccountNumber) throws BusinessException {
        if (StringUtils.isBlank(billingAccountNumber)) {
            return null;
        }
        Query query = getEntityManager().createQuery("SELECT ba from BillingAccount ba  where  ba.externalRef1=:billingAccountNumber");
        query.setParameter("billingAccountNumber", billingAccountNumber);
        try {
            return (BillingAccount) query.getSingleResult();
        } catch (NoResultException e) {
            //throw new BusinessException("The billing account is not found for a number : " + billingAccountNumber);
            return null;
        } catch (NonUniqueResultException e) {
            throw new BusinessException("there are multiple billing accounts with the same number " + billingAccountNumber);
        }
    }

    /**
     * find billing accounts by his number
     *
     * @param billingAccountNumber the billing account number
     * @return the billing account
     */
    public List<BillingAccount> findBillingAccountsByNumber(String billingAccountNumber) {
        if (StringUtils.isBlank(billingAccountNumber)) {
            return null;
        }
        TypedQuery<BillingAccount> query = getEntityManager().createQuery("SELECT ba from BillingAccount ba  where  ba.externalRef1=:billingAccountNumber", BillingAccount.class);
        query.setParameter("billingAccountNumber", billingAccountNumber);
        return query.getResultList();
    }

    /**
     * Get billing accounts by externalRef1
     *
     * @param sequenceNumber the sequence number
     * @return the billing accounts list
     */
    public List<BillingAccount> findBillingAccountsByExternalRef1(String sequenceNumber) {
        Map<String, Object> filters = new HashMap<>();
        filters.put("externalRef1", sequenceNumber);
        PaginationConfiguration paginationConfiguration = new PaginationConfiguration(filters);
        return list(paginationConfiguration);
    }

    /**
     * Get next sequence
     *
     * @return the next sequence number
     * @throws BusinessException the business exception
     */
    public String getNextSequence() throws BusinessException {
        String sequenceNumber = getNextSequenceNumber();

        //TODO : To remove after the environment has stabilized
        // check if there is non billing account with same number
        List<BillingAccount> billingAccounts = findBillingAccountsByExternalRef1(sequenceNumber);
        while (billingAccounts != null && !billingAccounts.isEmpty()) {
            sequenceNumber = getNextSequenceNumber();
            billingAccounts = findBillingAccountsByExternalRef1(sequenceNumber);
        }
        return sequenceNumber;
    }

    /**
     * Get next sequence number
     *
     * @return the next sequence number
     * @throws BusinessException the business exception
     */
    private String getNextSequenceNumber() throws BusinessException {
        GenericSequence genericSequence = serviceSingleton.getNextSequenceNumber(SequenceTypeEnum.CUSTOMER_NO);
        // return "4" + String.format("%08d", genericSequence.getCurrentSequenceNb());
        String sequenceNumber = StringUtils.getLongAsNChar(genericSequence.getCurrentSequenceNb(), genericSequence.getSequenceSize());
        return genericSequence.getPrefix() + sequenceNumber;
    }
}