package com.eir.service.commons;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;

/**
 * A quick cached data solution for statefull processing
 * 
 * @author Andrius Karpavicius
 *
 */
@Stateless
public class CachedData implements Serializable {

    private static final long serialVersionUID = 5735998182970564984L;

    private static Map<String, Object> data = new HashMap<>();

    /**
     * Retrieve cached data by a key
     * 
     * @param <T> value class
     * @param topic A topic to separate data types
     * @param key Key
     * @return Cached data corresponding to a key
     */
    @SuppressWarnings("unchecked")
    public <T> T get(String topic, String key) {
        return (T) data.get(topic + "_" + key);
    }

    /**
     * Store data
     * 
     * @param topic A topic to separate data types
     * @param key Key
     * @param value Value
     */
    public void put(String topic, String key, Object value) {
        data.put(topic + "_" + key, value);
    }

    /**
     * Clear cached data of given topics (optional)
     * 
     * @param topics A list of topics to clear
     */
    public void clear(String... topics) {
        if (topics.length == 0) {
            data = new HashMap<String, Object>();
        }
    }
}