package com.eir.service.taxMapping;

import java.math.BigInteger;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.DatePeriod;
import org.meveo.model.billing.Tax;
import org.meveo.model.shared.DateUtils;
import org.meveo.model.tax.TaxCategory;
import org.meveo.model.tax.TaxClass;
import org.meveo.model.tax.TaxMapping;
import org.meveo.service.base.NativePersistenceService;
import org.meveo.service.catalog.impl.TaxService;
import org.meveo.service.custom.CustomTableService;
import org.meveo.service.tax.TaxCategoryService;
import org.meveo.service.tax.TaxClassService;
import org.meveo.service.tax.TaxMappingService;

@Stateless(name = "EirTaxMapService")
public class TaxMapService extends NativePersistenceService {

	@Inject
	private TaxCategoryService taxCategoryService;

	@Inject
	private TaxClassService taxClassService;

	@Inject
	private TaxService taxService;

	@Inject
	private CustomTableService customTableService;

	@Inject
	private TaxMappingService taxMappingService;

	/**
	 * Get pending tax mappings by file name
	 *
	 * @param fileName the file name.
	 * @return pending tax mapping codes
	 */
	public List<String> getPendingTaxMappingsByFileName(String fileName) {
		List<String> listTaxMappingsCodes = null;
		if (!StringUtils.isBlank(fileName)) {
			String sql = "SELECT code FROM EIR_TAX_MAPPING_STAGE ts inner join flat_file ff on ts.file_id = ff.id and ff.file_original_name =:fileName";
			Query query = getEntityManager().createNativeQuery(sql);
			query.setParameter("fileName", fileName);
			listTaxMappingsCodes = query.getResultList();
		}
		return listTaxMappingsCodes;
	}

	/**
	 * Indicate is the provided tax mapping plan is overlap with an existing tax
	 * mapping or not
	 *
	 * @return true is the provided tax mapping is overlap with an existing tax
	 *         mapping else if not.
	 * @throws ParseException
	 */
	public boolean isOverlapTaxStage(Map<String, Object> taxMappingStageMap, Long fileId) throws ParseException {
		if (StringUtils.isBlank(taxMappingStageMap.get("tax_category")) && taxMappingStageMap.get("valid_from") == null
				&& !StringUtils.isBlank(taxMappingStageMap.get("valid_from"))
				&& taxMappingStageMap.get("valid_to") == null
				&& !StringUtils.isBlank(taxMappingStageMap.get("valid_to"))) {
			return false;
		}
		// modify action with an end date
		if (taxMappingStageMap.get("valid_from") != null && !StringUtils.isBlank(taxMappingStageMap.get("valid_from"))
				&& taxMappingStageMap.get("valid_to") != null
				&& !StringUtils.isBlank(taxMappingStageMap.get("valid_to"))) {

			List<String> taxMappingsStage = null;
			String sql = "select * from EIR_TAX_MAPPING_STAGE a  where a.tax_class = :a_tax_class and a.tax_category = :a_tax_category "
					+ "and (( a.valid_from IS NULL and a.valid_to IS NULL) "
					+ "or  ( a.valid_from IS NULL and a.valid_to>:valid_from) "
					+ "or (a.valid_to IS NULL and a.valid_from<:valid_to) "
					+ "or (a.valid_from IS NOT NULL and a.valid_to IS NOT NULL "
					+ "and ((a.valid_from<=:valid_from "
					+ "and :valid_from<a.valid_to) "
					+ "or (:valid_from<=a.valid_from "
					+ "and a.valid_from<:valid_to))))";

			if (fileId != null) {
				sql = sql + " and a.file_id != :file_id";
			}

			Query query = getEntityManager().createNativeQuery(sql);
			query.setParameter("a_tax_class", taxMappingStageMap.get("tax_class"));
			query.setParameter("a_tax_category", taxMappingStageMap.get("tax_category"));
			query.setParameter("valid_from",
					(Date)taxMappingStageMap.get("valid_from"),
					TemporalType.DATE);
			query.setParameter("valid_to",
					(Date)taxMappingStageMap.get("valid_to"),
					TemporalType.DATE);
			if (fileId != null) {
				query.setParameter("file_id", taxMappingStageMap.get("file_id"));
			}

			taxMappingsStage = query.getResultList();

			if (taxMappingsStage != null && !taxMappingsStage.isEmpty()) {
				return false;
			}
		}

		// add action without an end date
		if (taxMappingStageMap.get("valid_from") != null && !StringUtils.isBlank(taxMappingStageMap.get("valid_from"))
				&& taxMappingStageMap.get("valid_to") == null
				&& !StringUtils.isBlank(taxMappingStageMap.get("valid_to"))) {

			List<String> taxMappingsStage = null;


			String sql = "select * from EIR_TAX_MAPPING_STAGE a  where a.tax_class = :a_tax_class and a.tax_category = :a_tax_category "
					+ "and ((a.valid_from IS NULL and a.valid_to IS NULL) "
					+ "or (a.valid_from<=:valid_from and :valid_from<a.valid_to) "
					+ "or (a.valid_from<=:valid_from and a.valid_to IS NULL) "
					+ "or (a.valid_from IS NULL and :valid_from<a.valid_to))";

			if (fileId != null) {
				sql = sql + " and a.file_id != :file_id";
			}

			Query query = getEntityManager().createNativeQuery(sql);
			query.setParameter("a_tax_class", taxMappingStageMap.get("tax_class"));
			query.setParameter("a_tax_category", taxMappingStageMap.get("tax_category"));
			query.setParameter("valid_from",
					(Date)taxMappingStageMap.get("valid_from"),
					TemporalType.DATE);
			if (fileId != null) {
				query.setParameter("file_id", taxMappingStageMap.get("file_id"));
			}
			taxMappingsStage = query.getResultList();

			if (taxMappingsStage != null && !taxMappingsStage.isEmpty()) {
				return false;
			}
		}

		return true;
	}

	public boolean isOverlapTaxActive(Map<String, Object> taxMappingStageMap, Long fileId) throws ParseException {

		Map<String, Object> validFilters = getValidFilters(taxMappingStageMap);
		PaginationConfiguration validPaginationConfiguration = new PaginationConfiguration(validFilters);
		List<TaxMapping> activeTax = taxMappingService.list(validPaginationConfiguration);
		if (activeTax != null && !activeTax.isEmpty()) {
			return false;
		}
		return true;
	}

	/**
	 * Get filters
	 *
	 * @return the filters
	 * @throws ParseException
	 */
	public Map<String, Object> getFilters(Map<String, Object> taxMappingStageMap, Long fileId) throws ParseException {
		Map<String, Object> filters = new HashMap<String, Object>();

		if (!StringUtils.isBlank(taxMappingStageMap.get("tax_category"))) {
			filters.put("tax_category", taxMappingStageMap.get("tax_category"));
			filters.put("tax_class", taxMappingStageMap.get("tax_class"));
		}

		// If only validTo date is provided, a search will return tax mappings valid
		// from
		// today to a given date.
		if (taxMappingStageMap.get("valid_from") == null && taxMappingStageMap.get("valid_to") != null
				&& !StringUtils.isBlank(taxMappingStageMap.get("valid_from"))
				&& !StringUtils.isBlank(taxMappingStageMap.get("valid_to"))) {
			taxMappingStageMap.put("valid_from", (new Date()));
		}

		// search by a single date
		if (taxMappingStageMap.get("valid_from") != null && !StringUtils.isBlank(taxMappingStageMap.get("valid_from"))
				&& taxMappingStageMap.get("valid_to") == null
				&& !StringUtils.isBlank(taxMappingStageMap.get("valid_to"))) {

			filters.put("minmaxOptionalRange valid_from valid_to",
					(Date)taxMappingStageMap.get("valid_from"));

			// search by date range
		} else if (taxMappingStageMap.get("valid_from") != null
				&& !StringUtils.isBlank(taxMappingStageMap.get("valid_from"))
				&& taxMappingStageMap.get("valid_to") != null
				&& !StringUtils.isBlank(taxMappingStageMap.get("valid_to"))) {

			filters.put("overlapOptionalRange valid_from valid_to", new Date[] {
					(Date)taxMappingStageMap.get("valid_from"),
					(Date)taxMappingStageMap.get("valid_to") });
		}

		if (fileId != null) {
			filters.put("ne " + "file_id", taxMappingStageMap.get("file_id"));
		}

		return filters;
	}

	/**
	 * Find the pending tax mapping in the other files
	 *
	 * @param fileId the file id
	 * @param tax    class the tax class
	 * @param tax    category the tax category
	 * @return list of pending tax mapping
	 */
	public List<Map<String, Object>> findPendingTaxMapping(Long fileId, String taxClass, String taxCategory) {
		Map<String, Object> filters = new HashMap<>();
		filters.put("ne " + "file_id", fileId);
		filters.put("tax_class", taxClass);
		filters.put("tax_category", taxCategory);
		return customTableService.list("EIR_TAX_MAPPING_STAGE", new PaginationConfiguration(filters));
	}

	/**
	 * Find the pending tax mapping in the other files
	 *
	 * @param fileId the file id
	 * @param tax    class the tax class
	 * @param tax    category the tax category
	 * @return list of pending tax mapping
	 */
	public List<Map<String, Object>> findPendingTaxSameFile(Long fileId, String taxClass, String taxCategory,
			String action) {
		Map<String, Object> filters = new HashMap<>();
		filters.put("file_id", fileId);
		filters.put("tax_class", taxClass);
		filters.put("tax_category", taxCategory);
		filters.put("record_action", action);
		return customTableService.list("EIR_TAX_MAPPING_STAGE", new PaginationConfiguration(filters));
	}

	/**
	 * Find the active tax mapping in staging table to modify
	 * 
	 * @param fileId
	 * @param taxClass
	 * @param taxCategory
	 * @param tax
	 * @param validFrom
	 * @return
	 */
	public List<TaxMapping> findActiveTaxMappings(String taxClass, String taxCategory, String tax) {
		Map<String, Object> filters = new HashMap<>();
		filters.put("chargeTaxClass.code", taxClass);
		filters.put("accountTaxCategory.code", taxCategory);
		// filters.put("tax.code", tax);
		filters.put("SQL", "(valid.to is NULL)");
		return taxMappingService.list(new PaginationConfiguration(filters));
	}

	/**
	 * Find the active tax mapping in staging table to modify
	 * 
	 * @param fileId
	 * @param taxClass
	 * @param taxCategory
	 * @param tax
	 * @param validFrom
	 * @return
	 */
	public List<TaxMapping> findActiveTaxMappingsWithEndDate(String taxClass, String taxCategory, String tax,
			Date validFrom) {
		Map<String, Object> filters = new HashMap<>();
		filters.put("chargeTaxClass.code", taxClass);
		filters.put("accountTaxCategory.code", taxCategory);
		// filters.put("tax.code", tax);
		filters.put("SQL", "(valid.to is not NULL)");
		return taxMappingService.list(new PaginationConfiguration(filters));
	}

	/**
	 * find Tax Mappings With End Date
	 * 
	 * @param fileId
	 * @param taxClass
	 * @param taxCategory
	 * @param action
	 * @return Tax Mappings With End Date
	 */
	public List<Map<String, Object>> findTaxMappingsWithEndDate(Long fileId, String taxClass, String taxCategory,
			String action) {
		Map<String, Object> filters = new HashMap<>();
		filters.put("record_action", action);
		// filters.put("ne " + "file_id", fileId);
		filters.put("tax_class", taxClass);
		filters.put("tax_category", taxCategory);
		return customTableService.list("EIR_TAX_MAPPING_STAGE", new PaginationConfiguration(filters));
	}

	/**
	 * Find the invalid pending tax mapping in staging table
	 *
	 * @param fileId   the file id
	 * @param taxClass tax class
	 * @param billCode the bill code
	 * @return list of invalid pending tax mappings
	 */
	public List<Map<String, Object>> findInvalidTaxMappings(Long fileId, String taxClass, String taxCategory) {
		Map<String, Object> filters = new HashMap<>();
		filters.put("file_id", fileId);
		filters.put("tax_class", taxClass);
		filters.put("tax_category", taxCategory);
		filters.put("SQL", "(error_reason is not NULL)");
		return customTableService.list("EIR_TAX_MAPPING_STAGE", new PaginationConfiguration(filters));
	}

	/**
	 * Find the tax mapping stage by file id
	 *
	 * @param fileId the file id
	 * @return list of tax mappings
	 */
	public List<Map<String, Object>> findTaxMappingStageByFileId(Long fileId) {
		Map<String, Object> filters = new HashMap<>();
		filters.put("file_id", fileId);
		return customTableService.list("EIR_TAX_MAPPING_STAGE", new PaginationConfiguration(filters));
	}

	/**
	 * validate tax mappings
	 * 
	 * @param taxMappingStageMap
	 * @throws ParseException
	 */
	public void validateTaxMapping(Map<String, Object> taxMappingStageMap, Long fileId) throws ParseException {

		// check tax category
		TaxCategory taxCategory = taxCategoryService.findByCode((String) taxMappingStageMap.get("tax_category"));
		if (taxCategory == null) {
			taxMappingStageMap.put("error_reason",
					"The tax category : " + taxMappingStageMap.get("tax_category") + " does not exist");
			return;
		}
		taxMappingStageMap.put("tax_category", taxCategory.getCode());

		// check tax class
		TaxClass taxClass = taxClassService.findByCode((String) taxMappingStageMap.get("tax_class"));
		if (taxClass == null) {
			taxMappingStageMap.put("error_reason",
					"The tax class : " + taxMappingStageMap.get("tax_class") + " does not exist");
			return;
		}
		taxMappingStageMap.put("tax_class", taxClass.getCode());

		// check tax
		Tax tax = taxService.findByCode((String) taxMappingStageMap.get("tax"));
		if (tax == null) {
			taxMappingStageMap.put("error_reason", "The tax  : " + taxMappingStageMap.get("tax") + " does not exist");
			return;
		}
		taxMappingStageMap.put("tax", tax.getCode());

		// Check if there is the combination (tax class/tax category) already exists in
		// another file in staging table.
		if (fileId != null) {
			taxMappingStageMap.put("file_id", ((BigInteger) taxMappingStageMap.get("file_id")).longValue());
		}
		List<Map<String, Object>> pendingTaxmappings = findPendingTaxMapping((Long) taxMappingStageMap.get("file_id"),
				(String) taxMappingStageMap.get("tax_class"), (String) taxMappingStageMap.get("tax_category"));
		if (pendingTaxmappings != null && !pendingTaxmappings.isEmpty()) {
			throw new BusinessException("There is the same tax class " + taxMappingStageMap.get("tax_class")
					+ " and the same tax category " + taxMappingStageMap.get("tax_category") + " in another file");
		}

		// In Modify action Check if there is the end date is null and start date is not
		// earlier than start date
		if (((String) taxMappingStageMap.get("record_action")).equalsIgnoreCase("MODIFY")) {
			if (taxMappingStageMap.get("valid_to") == null
					&& !StringUtils.isBlank(taxMappingStageMap.get("valid_to"))) {
				taxMappingStageMap.put("error_reason", "The end date can't be null on Modify action ");
				return;
			}
			List<TaxMapping> activeTaxmappings = findActiveTaxMappings((String) taxMappingStageMap.get("tax_class"),
					(String) taxMappingStageMap.get("tax_category"), (String) taxMappingStageMap.get("tax"));
			if (activeTaxmappings == null || activeTaxmappings.isEmpty()) {
				taxMappingStageMap.put("error_reason",
						"The end date for this tax mapping is already assigned or doesn't exist in active table");
				return;
			} else {
				DatePeriod datePeriod = activeTaxmappings.get(0).getValid();
				if (datePeriod.getFrom().after(
						(Date)taxMappingStageMap.get("valid_to"))) {
					taxMappingStageMap.put("error_reason",
							"The end date cannot earlier than the start date in active table");
					return;
				}
			}
		}

		// In ADD action Check if there is a tax mapping with the same tax class and the
		// same tax category that don't a end date in the same file
		if (((String) taxMappingStageMap.get("record_action")).equalsIgnoreCase("ADD")) {

			// check in same file
			pendingTaxmappings = findPendingTaxSameFile((Long) taxMappingStageMap.get("file_id"),
					(String) taxMappingStageMap.get("tax_class"), (String) taxMappingStageMap.get("tax_category"),
					"ADD");
			if (pendingTaxmappings != null && !pendingTaxmappings.isEmpty()) {

				for (Map<String, Object> entry : pendingTaxmappings) {
					if (entry.get("id").equals(taxMappingStageMap.get("id"))) {
						continue;
					}
					Date entryFrom= new Date(((java.sql.Timestamp)entry.get("valid_from")).getTime());
					Date validFrom= (Date) taxMappingStageMap.get("valid_from");
					
					if (entry.get("valid_to") == null || StringUtils.isBlank(entry.get("valid_to"))) {
						if (taxMappingStageMap.get("valid_to") == null
								|| StringUtils.isBlank(taxMappingStageMap.get("valid_to"))
								|| ((Date)taxMappingStageMap.get("valid_to"))
												.after(entryFrom)) {
							taxMappingStageMap.put("error_reason",
									"There is the same tax class " + taxMappingStageMap.get("tax_class")
											+ " and the same tax category " + taxMappingStageMap.get("tax_category")
											+ " in same file active in same period");
						}
						return;
					} else if (((validFrom).before(
									new Date(((java.sql.Timestamp)entry.get("valid_to")).getTime()))
							&& (validFrom).after(entryFrom))
							|| ((validFrom).before(new Date(((java.sql.Timestamp)entry.get("valid_to")).getTime()))
									&& (taxMappingStageMap.get("valid_to") == null
							|| StringUtils.isBlank(taxMappingStageMap.get("valid_to"))))) {
						taxMappingStageMap.put("error_reason",
								"There is the same tax class " + taxMappingStageMap.get("tax_class")
										+ " and the same tax category " + taxMappingStageMap.get("tax_category")
										+ "  active in same period");
						return;
					}
				}
			}

			// check in active tax mappings
			List<TaxMapping> activeTaxWithEndDate = findActiveTaxMappingsWithEndDate(
					(String) taxMappingStageMap.get("tax_class"), (String) taxMappingStageMap.get("tax_category"),
					(String) taxMappingStageMap.get("tax"),
					(Date)taxMappingStageMap.get("valid_from"));
			if (activeTaxWithEndDate != null && !activeTaxWithEndDate.isEmpty()) {
				for (TaxMapping taxMapping : activeTaxWithEndDate) {
					Date endDate = taxMapping.getValid().getTo();
					if (endDate != null) {
						if (((Date) taxMappingStageMap.get("valid_from"))
								.before(endDate)) {
							taxMappingStageMap.put("error_reason",
									"There is the same tax class " + taxMappingStageMap.get("tax_class")
											+ " and the same tax category " + taxMappingStageMap.get("tax_category")
											+ "  with end date later than start date in active table");
							return;
						}
					}
				}
			}

			// check in active tax mappings
			List<TaxMapping> activeTaxWithoutEndDate = findActiveTaxMappings(
					(String) taxMappingStageMap.get("tax_class"), (String) taxMappingStageMap.get("tax_category"),
					(String) taxMappingStageMap.get("tax"));
			if (activeTaxWithoutEndDate != null && !activeTaxWithoutEndDate.isEmpty()) {

				List<Map<String, Object>> taxmappings = findTaxMappingsWithEndDate(
						(Long) taxMappingStageMap.get("file_id"), (String) taxMappingStageMap.get("tax_class"),
						(String) taxMappingStageMap.get("tax_category"), "MODIFY");
				for (Map<String, Object> entry : taxmappings) {

					if (((Date)taxMappingStageMap.get("valid_from"))
							.before(new Date(((java.sql.Timestamp)entry.get("valid_to")).getTime()))) {
						
						taxMappingStageMap.put("error_reason",
								"There is the same tax class " + taxMappingStageMap.get("tax_class")
										+ " and the same tax category " + taxMappingStageMap.get("tax_category")
										+ "  with end date later than start date in a MODIFY record");
						return;
					}
				}
				if (taxmappings == null || taxmappings.isEmpty()) {
					taxMappingStageMap.put("error_reason",
							"There is the same tax class " + taxMappingStageMap.get("tax_class")
									+ " and the same tax category " + taxMappingStageMap.get("tax_category")
									+ "  already active");
					return;
				}
			}
		}

		// Check the provided tax mapping is overlap with an existing tax mapping in
		// active table or not
		if (!isOverlapTaxActive(taxMappingStageMap, fileId)
				&& !((String) taxMappingStageMap.get("record_action")).equalsIgnoreCase("MODIFY")) {

			List<Map<String, Object>> taxmappings = findTaxMappingsWithEndDate((Long) taxMappingStageMap.get("file_id"),
					(String) taxMappingStageMap.get("tax_class"), (String) taxMappingStageMap.get("tax_category"),
					"MODIFY");
			if (taxmappings != null && !taxmappings.isEmpty()) {
				for (Map<String, Object> entry : taxmappings) {

					if (((Date) taxMappingStageMap.get("valid_from")).before(new Date(((java.sql.Timestamp)entry.get("valid_to")).getTime()))) {
						taxMappingStageMap.put("error_reason",
								"There is the same tax class " + taxMappingStageMap.get("tax_class")
										+ " and the same tax category " + taxMappingStageMap.get("tax_category")
										+ "  with end date later than start date in a MODIFY record");
						return;
					}
				}
			}
		}

		// Check the provided tax mapping is overlap with an existing tax mapping in
		// stage table or not
		if (!isOverlapTaxStage(taxMappingStageMap, fileId)
				&& !((String) taxMappingStageMap.get("record_action")).equalsIgnoreCase("MODIFY")) {
			taxMappingStageMap.put("error_reason",
					"The tax category " + taxMappingStageMap.get("tax_category") + " and tax class "
							+ taxMappingStageMap.get("tax_class") + " is overlapping with an existing tax mapping");
			return;
		}

		if (!(taxMappingStageMap.get("record_action").equals("ADD")
				|| taxMappingStageMap.get("record_action").equals("MODIFY"))) {
			taxMappingStageMap.put("error_reason", "Correct Record action is missing");
			return;
		}

		if (!StringUtils.isBlank(taxMappingStageMap.get("error_reason"))) {
			return;
		}

		// Check if there is the same tax class and the same tax category in current
		// file in error.
		pendingTaxmappings = findInvalidTaxMappings((Long) taxMappingStageMap.get("file_id"),
				(String) taxMappingStageMap.get("tax_class"), (String) taxMappingStageMap.get("tax_category"));
		if (pendingTaxmappings != null && !pendingTaxmappings.isEmpty()) {
			taxMappingStageMap.put("error_reason",
					"There are the same tax class " + taxMappingStageMap.get("tax_class")
							+ " and the same tax category " + taxMappingStageMap.get("tax_category")
							+ " in the same file who are invalid");
			return;
		}

		// Check if a record cannot have a stop date earlier than the start date of that
		// record
		if (taxMappingStageMap.get("valid_from") != null && !StringUtils.isBlank(taxMappingStageMap.get("valid_from"))
				&& taxMappingStageMap.get("valid_to") != null
				&& !StringUtils.isBlank(taxMappingStageMap.get("valid_to"))
				&& ((Date)taxMappingStageMap.get("valid_to")).before(
						(Date)taxMappingStageMap.get("valid_from"))) {
			taxMappingStageMap.put("error_reason", "The end date cannot earlier than the start date");
			return;
		}

	}

	/**
	 * Get filters
	 *
	 * @return the filters
	 * @throws ParseException
	 */
	public Map<String, Object> getValidFilters(Map<String, Object> taxMappingStageMap) throws ParseException {
		Map<String, Object> filters = new HashMap<String, Object>();

		if (!StringUtils.isBlank(taxMappingStageMap.get("tax_category"))) {
			filters.put("accountTaxCategory.code", taxMappingStageMap.get("tax_category"));
			filters.put("chargeTaxClass.code", taxMappingStageMap.get("tax_class"));
		}


		// search by a single date
		if (taxMappingStageMap.get("valid_from") != null && !StringUtils.isBlank(taxMappingStageMap.get("valid_from"))
				&& taxMappingStageMap.get("valid_to") == null
				&& !StringUtils.isBlank(taxMappingStageMap.get("valid_to"))) {
			filters.put("minmaxOptionalRange valid.from valid.to ",
					(Date)taxMappingStageMap.get("valid_from"));

			// search by date range
		} else if (taxMappingStageMap.get("valid_from") != null
				&& !StringUtils.isBlank(taxMappingStageMap.get("valid_from"))
				&& taxMappingStageMap.get("valid_to") != null
				&& !StringUtils.isBlank(taxMappingStageMap.get("valid_to"))) {
			filters.put("overlapOptionalRange valid.from valid.to", new Date[] {
					(Date)taxMappingStageMap.get("valid_from"),
					(Date)taxMappingStageMap.get("valid_to") });
		}

		return filters;
	}

}
