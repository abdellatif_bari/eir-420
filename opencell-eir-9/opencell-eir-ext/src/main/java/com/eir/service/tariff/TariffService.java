package com.eir.service.tariff;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;

import org.apache.commons.io.FilenameUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.api.commons.Utils;
import org.meveo.api.dto.flatfile.ExportDto;
import org.meveo.commons.utils.QueryBuilder;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.catalog.ChargeTemplate;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.OfferTemplateCategory;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.base.NativePersistenceService;
import org.meveo.service.base.expressions.NativeExpressionFactory;
import org.meveo.service.billing.impl.ReratingInfo;
import org.meveo.service.billing.impl.ReratingService;
import org.meveo.service.catalog.impl.OfferTemplateService;

import com.eir.commons.enums.ChargeTypeEnum;
import com.eir.commons.enums.StagingRecordActionEnum;
import com.eir.commons.enums.TransformationRulesEnum;
import com.eir.commons.utils.BusinessRulesValidator;
import com.eir.commons.utils.TransformationRulesFactory;
import com.eir.commons.utils.TransformationRulesHandler;
import com.eir.script.OneShotAndRecurringRatingScript;
import com.eir.service.commons.CachedData;
import com.eir.service.commons.ImportService;
import com.eir.service.commons.UtilService;

/**
 * EIR tariff service
 *
 * @author Abdellatif BARI
 */
@Stateless
public class TariffService extends NativePersistenceService {

    @Inject
    private BusinessRulesValidator businessRulesValidator;
    @Inject
    private ImportService importService;
    @Inject
    private OfferTemplateService offerTemplateService;
    @Inject
    private ReratingService reratingService;
    @Inject
    private TransformationRulesFactory transformationRulesFactory;
    @Inject
    private UtilService utilService;
    
    @Inject 
    private CachedData cachedData;



    /**
     * Validate function
     *
     * @param parameters the validation parameters
     * @return the result of tariff validation.
     * @throws ValidationException the validation exception
     */
    public String validate(Object... parameters) throws ValidationException {
        if (parameters[0] instanceof Map<?, ?>) {
            return validateTariff((Map<String, Object>) parameters[0], (TransformationRulesHandler) parameters[1]);
        }
        return validateTariffs((String) parameters[0], (Long) parameters[1]);
    }

    /**
     * Activate function
     *
     * @param parameters the activation parameters
     * @return the result of tariff activation.
     * @throws ValidationException the validation exception
     */
    public String activate(Object... parameters) throws ValidationException {
        String offerType = (String) parameters[0];
        Map<String, Object> stagingTariff = (Map<String, Object>) parameters[1];

        // Check the compatibility between offer type of tariff and the provided offer type
        String offerCode = (String) stagingTariff.get("offer");
        OfferTemplate OfferTemplate = cachedData.get("offer", offerCode);
        if (OfferTemplate == null) {
            OfferTemplate = offerTemplateService.findByCode(offerCode);
            if (OfferTemplate == null) {
                throw new BusinessException("The offer  : " + offerCode + " does not exist");
            }
            cachedData.put("offer", offerCode, OfferTemplate);
        }
        List<OfferTemplateCategory> offerTemplateCategories = OfferTemplate.getOfferTemplateCategories();
        if (offerTemplateCategories == null || offerTemplateCategories.isEmpty()) {
            throw new BusinessException("The offer category : " + stagingTariff.get("offer") + " does not exist");
        }
        String stagingTariffOfferType = (String) ((offerTemplateCategories.get(0)).getCfValue("offerType"));
        if (!offerType.equalsIgnoreCase(stagingTariffOfferType)) {
            throw new BusinessException("Tariff offer type (" + stagingTariffOfferType + ") and provided offer type (" + offerType + ") mismatch");
        }
        return null;
    }

    /**
     * Re-rate function
     *
     * @param parameters the re-rating parameters
     * @return the result of tariff re-rating.
     * @throws ValidationException the validation exception
     */
    @SuppressWarnings("unchecked")
    public String rerate(Object... parameters) throws ValidationException {
        if (parameters == null || parameters.length < 2) {
            return null;
        }

        boolean triggerRerate = paramBean.getBooleanValue("eir.orTariff.rerate", true);
        if (!triggerRerate) {
            return null;
        }

        String validTableName = (String) parameters[0];
        List<Long> newRecordIds = (List<Long>) parameters[1];
        List<Long> modifiedRecordIds = (List<Long>) parameters[2];

        if (StringUtils.isBlank(validTableName) || ((newRecordIds == null || newRecordIds.isEmpty()) && (modifiedRecordIds == null || modifiedRecordIds.isEmpty()))) {
            return null;
        }

        if (newRecordIds != null && modifiedRecordIds != null) {
            newRecordIds.removeAll(modifiedRecordIds);
        }

        Map<String, ReratingInfo> reratingInfos = new HashMap<String, ReratingInfo>();
        if (newRecordIds != null && !newRecordIds.isEmpty()) {


            double number = newRecordIds.size() * 1.0 / Utils.MAX_NUMBER_JDBC_PSG_PARAMETERS;
            int n = ((Double) Math.ceil(number)).intValue(); // Round up
            for (int i = 0; i < n; i++) {
                int end = Utils.MAX_NUMBER_JDBC_PSG_PARAMETERS * (i + 1);
                if (end > newRecordIds.size()) {
                    end = newRecordIds.size();
                }

                List<Long> ids = newRecordIds.subList(Utils.MAX_NUMBER_JDBC_PSG_PARAMETERS * i, end);

                String sql = "select offer_id, service_id, charge_type, min(valid_from), tariff_plan_id from " + validTableName + " where id in :ids group by offer_id, service_id, charge_type, tariff_plan_id";
                Query query = getEntityManager().createNativeQuery(sql).setParameter("ids", ids);
                List<Object[]> resultList = query.getResultList();
                addReratingInfos(reratingInfos, resultList);
            }
        }

        if (modifiedRecordIds != null && !modifiedRecordIds.isEmpty()) {

            double number = modifiedRecordIds.size() * 1.0 / Utils.MAX_NUMBER_JDBC_PSG_PARAMETERS;
            int n = ((Double) Math.ceil(number)).intValue(); // Round up
            for (int i = 0; i < n; i++) {
                int end = Utils.MAX_NUMBER_JDBC_PSG_PARAMETERS * (i + 1);
                if (end > modifiedRecordIds.size()) {
                    end = modifiedRecordIds.size();
                }

                List<Long> ids = modifiedRecordIds.subList(Utils.MAX_NUMBER_JDBC_PSG_PARAMETERS * i, end);

                String sql = "select offer_id, service_id, charge_type, min(valid_to), tariff_plan_id from " + validTableName
                        + " where valid_to is not null and id in :ids group by offer_id, service_id, charge_type, tariff_plan_id";
                Query query = getEntityManager().createNativeQuery(sql).setParameter("ids", ids);
                List<Object[]> resultList = query.getResultList();
                addReratingInfos(reratingInfos, resultList);
            }
        }

       
        if (!reratingInfos.isEmpty()) {
            reratingService.rerate(reratingInfos.values(), false);
        }
        return null;
    }

    /**
     * Update the re-rating infos list. Use the earliest date for rerating for every offer, service, charge type and tariff plan combination
     *
     * @param reratingInfos A map of current re-rating information with offer id,service id, charge type and tariff plan id as a key and rerating info as a value
     * @param resultList    the query result list
     * @return the re-rating infos list
     */
    private void addReratingInfos(Map<String, ReratingInfo> reratingInfos, List<Object[]> resultList) {

        if (resultList == null || resultList.isEmpty()) {
            return;
        }
        String key = null;
        for (Object[] result : resultList) {
            long tariffPlanId = Long.valueOf(((BigInteger) result[4]).longValue());

            key = ((BigInteger) result[0]).toString() + ((BigInteger) result[1]) + ((String) result[2]) + result[4];
            ReratingInfo reratingInfo = reratingInfos.get(key);
            if (reratingInfo == null) {
                reratingInfos.put(key,
                        new ReratingInfo(getEntityManager().getReference(OfferTemplate.class, Long.valueOf(((BigInteger) result[0]).longValue())),
                                getEntityManager().getReference(ServiceTemplate.class, Long.valueOf(((BigInteger) result[1]).longValue())),
                                ChargeTypeEnum.R.name().equalsIgnoreCase((String) result[2]) ? ChargeTemplate.ChargeMainTypeEnum.RECURRING : ChargeTemplate.ChargeMainTypeEnum.ONESHOT, (Date) result[3],
                                "CF." + OneShotAndRecurringRatingScript.CFT_TARIFF_PLAN_ID, tariffPlanId));
            } else if (reratingInfo.getFromDate().after((Date) result[3])) {
                reratingInfo.setFromDate((Date) result[3]);
            }
        }
    }

    /**
     * Validate tariff
     *
     * @param record                     the tariff record
     * @param transformationRulesHandler the transformation rules handler
     * @return the result of tariff validation.
     * @throws ValidationException the validation exception
     */
    public String validateTariff(Map<String, Object> record, TransformationRulesHandler transformationRulesHandler) throws ValidationException {

        /**
         * Check if there is the combination (tariff plan/billcode) already exists in another file in staging table.
         */

        String cacheKey = record.get("file_id") + "_" + record.get("tariff_plan") + "_" + record.get("service");
        Long count = cachedData.get("recCount", cacheKey);
        if (count == null) {
            String sql = "select count(*) from EIR_OR_TARIFF_STAGE stage where stage.service=:service and stage.tariff_plan=:tariffPlan and stage.file_id!=:fileId";
            Query query = getEntityManager().createNativeQuery(sql);
            query.setParameter("service", record.get("service"));
            query.setParameter("tariffPlan", record.get("tariff_plan"));
            query.setParameter("fileId", record.get("file_id"));
            BigInteger valueMatched = (BigInteger) query.getSingleResult();
            count = valueMatched.longValue();
            cachedData.put("recCount", cacheKey, count);
        }
        if (count > 0) {
            String errorMessage = "There is the same billCode " + record.get("service") + " and the same tariffPlan "
                    + record.get("tariff_plan") + " in another file";
            throw new ValidationException(errorMessage);
        }

        importService.setHashCode(record, transformationRulesHandler);

        return null;
    }

    /**
     * Activate tariff
     *
     * @param offerType The offer type
     * @param fileId    the file id
     * @throws BusinessException the business exception
     */
    public void activateTariff(String offerType, Long fileId) throws BusinessException {
        importService.activateRecord(offerType, fileId);
    }

    /**
     * Cancel tariff
     *
     * @param fileId the file id
     * @throws BusinessException the business exception
     */
    public void cancelTariff(Long fileId) throws BusinessException {
        importService.cancelStagingRecords("EIR_OR_TARIFF_STAGE", fileId);
    }

    /**
     * Validate tariffs.
     *
     * @param offerType the offer type
     * @param fileId    the file id
     * @return the result of tariffs validation.
     * @throws BusinessException the business exception
     */
    public String validateTariffs(String offerType, Long fileId) throws BusinessException {
        try {
            TransformationRulesHandler transformationRulesHandler = importService.getTransformationRules(offerType);
            List<String> excludedCriterion = null;

            String sql = "update EIR_OR_TARIFF_STAGE set error_reason = null where file_id=:fileId";
            Query query = getEntityManager().createNativeQuery(sql);
            query.setParameter("fileId", fileId).executeUpdate();

            /**
             * Check if a record have a type charge other than R and O.
             */
            sql = "update EIR_OR_TARIFF_STAGE set error_reason='The record charge type is incorrect' where file_id=:fileId " +
                    "and (error_reason is null or error_reason = '') " +
                    "and LOWER(charge_type) != LOWER('" + ChargeTypeEnum.R.name() + "') and LOWER(charge_type) != LOWER('" + ChargeTypeEnum.O.name() + "') " +
                    "and LOWER(charge_type) != LOWER('" + ChargeTypeEnum.C.name() + "')";
            query = getEntityManager().createNativeQuery(sql);
            query.setParameter("fileId", fileId).executeUpdate();

            /**
             * Check if a record have an action other than ADD and MODIFY.
             */
            sql = "update EIR_OR_TARIFF_STAGE set error_reason='The record action is incorrect' where file_id=:fileId " +
                    "and (error_reason is null or error_reason = '') " +
                    "and LOWER(record_action) != LOWER('" + StagingRecordActionEnum.ADD.name() + "') and LOWER(record_action) != LOWER('" + StagingRecordActionEnum.MODIFY.name() + "')";
            query = getEntityManager().createNativeQuery(sql);
            query.setParameter("fileId", fileId).executeUpdate();

            /**
             * Check if a record have a stop date earlier than the start date of that record.
             */
            sql = "update EIR_OR_TARIFF_STAGE set error_reason='The end date cannot earlier than the start date' where file_id=:fileId " +
                    "and (error_reason is null or error_reason = '') " +
                    "and ((valid_from is null and valid_to is null)  or (valid_from is not null and valid_to is not null " +
                    "and valid_from > valid_to))";
            query = getEntityManager().createNativeQuery(sql);
            query.setParameter("fileId", fileId).executeUpdate();

            /**
             * Check if there is the same billCode and the same tariffPlan in current file with error.
             */
            sql = "update EIR_OR_TARIFF_STAGE set error_reason=concat('There are the same billCode ', service, ' and the same tariffPlan ', " +
                    "tariff_plan, ' in the same file who are invalid') where (error_reason is null or error_reason = '') and id in (" +
                    "select stage1.id from EIR_OR_TARIFF_STAGE stage1 inner join EIR_OR_TARIFF_STAGE stage2 on stage1.id != stage2.id " +
                    "and LOWER(stage1.service) = LOWER(stage2.service) and LOWER(stage1.tariff_plan) = LOWER(stage2.tariff_plan) and stage1.file_id = stage2.file_id " +
                    "and stage2.error_reason is not null where stage1.file_id=:fileId)";
            query = getEntityManager().createNativeQuery(sql);
            query.setParameter("fileId", fileId).executeUpdate();

            /**
             * Check if there is a not null end date for 'modify' action in staging table.
             */
            excludedCriterion = new ArrayList<>(Arrays.asList("record_action", "valid_to", "file_id"));
            String exactMatchingJoin = " left_table.id != right_table.id and " + businessRulesValidator.getExactMatchingCriteriaForStageTable(transformationRulesHandler,
                    "left_table", "right_table", excludedCriterion, true);

            sql = "update EIR_OR_TARIFF_STAGE set error_reason='The MODIFY action can not update the staging tariff having the not null end date' " +
                    "where file_id=:fileId and (error_reason is null or error_reason = '') and id in (" +
                    "select left_table.id from EIR_OR_TARIFF_STAGE left_table inner join EIR_OR_TARIFF_STAGE right_table on " + exactMatchingJoin +
                    " where LOWER(left_table.record_action) = LOWER('MODIFY') and right_table.valid_to is not null) ";
            query = getEntityManager().createNativeQuery(sql);
            query.setParameter("fileId", fileId).executeUpdate();

            /**
             * Check if there is not valid tariff for the stage one in the 'modify' action case.
             */
            excludedCriterion = new ArrayList<>(Arrays.asList("accounting_code_id", "valid_to", "file_name", "updated", "updated_by"));
            exactMatchingJoin = businessRulesValidator.getExactMatchingCriteriaForValidTable(transformationRulesHandler, "left_table", "right_table",
                    excludedCriterion, true);

            sql = "update EIR_OR_TARIFF_STAGE set error_reason='No valid tariff was found for this MODIFY action' " +
                    "where file_id=:fileId and LOWER(record_action) = LOWER('MODIFY') and (error_reason is null or error_reason = '') and id not in (" +
                    "select left_table.id from EIR_OR_TARIFF_STAGE left_table inner join EIR_OR_TARIFF right_table on " + exactMatchingJoin + ")";
            query = getEntityManager().createNativeQuery(sql);
            query.setParameter("fileId", fileId).executeUpdate();

            /**
             * Check overlapping in the staging table
             */
            String exactMatchingADDJoin = "(LOWER(left_table.record_action) = LOWER('ADD') and left_table.id != right_table.id and " +
                    businessRulesValidator.getExactMatchingCriteriaForStageTable(transformationRulesHandler,
                            "left_table", "right_table", null, false) + ")";

            excludedCriterion = new ArrayList<>(Arrays.asList("record_action", "accounting_code", "valid_from", "valid_to", "file_id"));
            String exactMatchingMODIFYJoin = "(LOWER(left_table.record_action) = LOWER('MODIFY') and left_table.id != right_table.id and " +
                    businessRulesValidator.getExactMatchingCriteriaForStageTable(transformationRulesHandler,
                            "left_table", "right_table", excludedCriterion, true) + ")";

            //String overlappingCondition = "LOWER(left_table.record_action) = LOWER('ADD') " +
            String overlappingCondition = "((right_table.valid_from is null and right_table.valid_to is null) " +
                    "or (right_table.valid_from is null and left_table.valid_from is null) " +
                    "or (right_table.valid_to is null and left_table.valid_to is null) " +
                    "or (right_table.valid_from <= left_table.valid_from and left_table.valid_from< right_table.valid_to) " +
                    "or (right_table.valid_from is null and right_table.valid_to > left_table.valid_from) " +
                    "or (right_table.valid_to is null and right_table.valid_from < left_table.valid_to ) " +
                    "or (right_table.valid_from is not null and right_table.valid_to is not null " +
                    "and ((right_table.valid_from <= left_table.valid_from and left_table.valid_from < right_table.valid_to " +
                    "or (left_table.valid_from<= right_table.valid_from and right_table.valid_from< left_table.valid_to))))) " +
                    "and left_table.id > right_table .id " +
                    "order by left_table.id  desc";

            sql = "update EIR_OR_TARIFF_STAGE set error_reason=concat('The tariff ', tariff_plan, ' is overlap with an existing tariff in staging table')" +
                    "where file_id=:fileId and ( error_reason is null or error_reason = '') and id in (" +
                    "select left_table.id from EIR_OR_TARIFF_STAGE left_table inner join EIR_OR_TARIFF_STAGE right_table on (" + exactMatchingMODIFYJoin + " or " + exactMatchingADDJoin + ") where " + overlappingCondition + ")";
            query = getEntityManager().createNativeQuery(sql);
            query.setParameter("fileId", fileId).executeUpdate();

            /**
             * Check reference data
             */
            importService.validateReferenceData(transformationRulesHandler, fileId, null);

            /**
             * Check if there is a not null end date for 'modify' action in valid table.
             */
            excludedCriterion = new ArrayList<>(Arrays.asList("valid_to", "file_name", "updated", "updated_by"));
            exactMatchingJoin = businessRulesValidator.getExactMatchingCriteriaForValidTable(transformationRulesHandler, "left_table", "right_table",
                    excludedCriterion, true);

            sql = "update EIR_OR_TARIFF_STAGE set error_reason='The MODIFY action can not update the valid tariff having the not null end date'" +
                    "where file_id=:fileId and (error_reason is null or error_reason = '') and id in (" +
                    "select left_table.id from EIR_OR_TARIFF_STAGE left_table inner join EIR_OR_TARIFF right_table on " + exactMatchingJoin +
                    " where LOWER(left_table.record_action) = LOWER('MODIFY') and right_table.valid_to is not null) ";
            query = getEntityManager().createNativeQuery(sql);
            query.setParameter("fileId", fileId).executeUpdate();

            /**
             * Check overlapping in the valid table
             */
            excludedCriterion = new ArrayList<>(Arrays.asList("accounting_code_id", "valid_to", "file_name", "updated", "updated_by"));
            String exactMatchingModifyJoin = " LOWER(right_table1.record_action) = LOWER('MODIFY') and " + businessRulesValidator.getExactMatchingCriteriaForValidTable(transformationRulesHandler,
                    "right_table1", "left_table1", excludedCriterion, true);

            String right_table = "(select * from EIR_OR_TARIFF valid_tariff where valid_tariff.id not in (select left_table1.id from EIR_OR_TARIFF left_table1 inner join EIR_OR_TARIFF_STAGE right_table1 " +
                    "on " + exactMatchingModifyJoin + "))";

            exactMatchingJoin = businessRulesValidator.getExactMatchingCriteriaForValidTable(transformationRulesHandler, "left_table", "right_table",
                    null, false);

            overlappingCondition = "LOWER(left_table.record_action) = LOWER('ADD') " +
                    "and ((right_table.valid_from is null and right_table.valid_to is null) " +
                    "or (right_table.valid_from is null and left_table.valid_from is null) " +
                    "or (right_table.valid_to is null and left_table.valid_to is null) " +
                    "or (right_table.valid_from <= left_table.valid_from and left_table.valid_from< right_table.valid_to) " +
                    "or (right_table.valid_from is null and right_table.valid_to > left_table.valid_from) " +
                    "or (right_table.valid_to is null and right_table.valid_from < left_table.valid_to ) " +
                    "or (right_table.valid_from is not null and right_table.valid_to is not null " +
                    "and ((right_table.valid_from <= left_table.valid_from and left_table.valid_from < right_table.valid_to " +
                    "or (left_table.valid_from<= right_table.valid_from and right_table.valid_from< left_table.valid_to))))) " +
                    "order by left_table.id desc";


            sql = "update EIR_OR_TARIFF_STAGE set error_reason=concat('The tariff ', tariff_plan, ' is overlap with an existing tariff in valid table')" +
                    "where file_id=:fileId and (error_reason is null or error_reason = '') and " +
                    "id in ( select left_table.id from EIR_OR_TARIFF_STAGE left_table inner join " + right_table + " right_table on " + exactMatchingJoin + " where " + overlappingCondition + ") ";
            query = getEntityManager().createNativeQuery(sql);
            query.setParameter("fileId", fileId).executeUpdate();
        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
        return null;
    }

    /**
     * Create tariff file from custom table by the filtered data
     *
     * @param exportDto the export Dto
     * @return the tariff file
     * @throws BusinessException business exception
     */
    public File exportTariff(ExportDto exportDto) throws BusinessException {
        try {
            TransformationRulesHandler transformationRulesHandler = transformationRulesFactory.getInstance(TransformationRulesEnum.valueOfIgnoreCase(exportDto.getExportType()));
            if (transformationRulesHandler == null || transformationRulesHandler.getTransformationRules() == null ||
                    transformationRulesHandler.getTransformationRules().isEmpty()) {
                throw new BusinessException("the transformation rules between stage and valid table are missing");
            }

            LinkedHashMap<String, String> fieldNamesDescriptions = utilService.getFieldNamesDescriptions(exportDto.getFileFormatCode());

            List<String> fieldNames = new ArrayList<>();
            Collection<String> fieldDescriptions = new ArrayList<>();
            if (fieldNamesDescriptions != null && !fieldNamesDescriptions.isEmpty()) {
                fieldNames = new ArrayList(fieldNamesDescriptions.keySet());
                fieldDescriptions = fieldNamesDescriptions.values();
            }

            String sql = null;
            if (exportDto.isExportValidTable()) {
                sql = "select tariff.id, service_template.code as service, tariff.charge_type, calendar.code as rec_calendar, tariff.description, " +
                        "accounting_code.code as accounting_code, sub_cat.code as invoice_subcat, tax_class.code as tax_class, tariff.average_speed, " +
                        "tariff.action_qualifier, tariff.quantity_from, tariff.quantity_to, tariff.max_rated_quantity, tariff.price_plan, tariff.rating_scheme, " +
                        "tariff.bandwidth, tariff.transmission, tariff.commitment_period, tariff.zone_a, tariff.zone_b, tariff.region, tariff.sla, " +
                        "tariff.service_category, tariff.analogue_quality, tariff.analogue_rate, tariff.cpe_type, tariff.cpe_description, tariff.ets_type, " +
                        "tariff.ind_01, tariff.ind_02, tariff.ind_03, tariff.ind_04, tariff.ind_05, tariff.ind_06, tariff.ind_07, tariff.ind_08, " +
                        "tariff.ind_09, tariff.ind_10, tariff.distance_from, tariff.distance_to, tariff.distance_unit, tariff.quantity_unit, " +
                        "tariff.density, tariff.class_of_service, tariff.ef, tariff.af, tariff.exchange_class, tariff.order_type, tariff.egress_group, tariff.price, " +
                        //"to_char(tariff.valid_from, 'DD/MM/YYYY HH24:MI:SS') as valid_from, to_char(tariff.valid_to, 'DD/MM/YYYY HH24:MI:SS') as valid_to, " +
                        "tariff.valid_from, tariff.valid_to, tariff_plan.code as tariff_plan, offer_template.code as offer " +
                        "from EIR_OR_TARIFF tariff " +
                        "inner join cat_offer_template offer_template on offer_template.id = tariff.offer_id and offer_template.type = 'OFFER' " +
                        "inner join cat_product_offer_tmpl_cat ass on ass.product_id = offer_template.id " +
                        "inner join cat_offer_template_category cat on cat.id = ass.offer_template_cat_id and LOWER(cat.cf_values\\:\\:json->'offerType'->0->>'string') = LOWER('" + exportDto.getExportType() + "')" +
                        "inner join EIR_OR_TARIFF_PLAN tariff_plan on tariff_plan.id = tariff.tariff_plan_id " +
                        "inner join cat_service_template service_template on service_template.id = tariff.service_id " +
                        "inner join cat_calendar calendar on calendar.id = tariff.rec_calendar " +
                        "inner join billing_accounting_code accounting_code on accounting_code.id = tariff.accounting_code_id " +
                        "inner join billing_invoice_sub_cat sub_cat on sub_cat.id = tariff.invoice_subcat_id " +
                        "inner join billing_tax_class tax_class on tax_class.id = tariff.tax_class ";
            } else {
                sql = "select tariff.id, tariff.service, tariff.charge_type, tariff.rec_calendar, tariff.description, " +
                        "tariff.accounting_code, tariff.invoice_subcat, tariff.tax_class, tariff.average_speed, " +
                        "tariff.action_qualifier, tariff.quantity_from, tariff.quantity_to, tariff.max_rated_quantity, tariff.price_plan, tariff.rating_scheme, " +
                        "tariff.bandwidth, tariff.transmission, tariff.commitment_period, tariff.zone_a, tariff.zone_b, tariff.region, tariff.sla, " +
                        "tariff.service_category, tariff.analogue_quality, tariff.analogue_rate, tariff.cpe_type, tariff.cpe_description, tariff.ets_type, " +
                        "tariff.ind_01, tariff.ind_02, tariff.ind_03, tariff.ind_04, tariff.ind_05, tariff.ind_06, tariff.ind_07, tariff.ind_08, " +
                        "tariff.ind_09, tariff.ind_10, tariff.distance_from, tariff.distance_to, tariff.distance_unit, tariff.quantity_unit, " +
                        "tariff.density, tariff.class_of_service, tariff.ef, tariff.af, tariff.exchange_class, tariff.order_type, tariff.egress_group, tariff.price, " +
                        //"to_char(tariff.valid_from, 'DD/MM/YYYY HH24:MI:SS') as valid_from, to_char(tariff.valid_to, 'DD/MM/YYYY HH24:MI:SS') as valid_to, " +
                        "tariff.valid_from, tariff.valid_to, tariff.tariff_plan, tariff.offer " +
                        "from EIR_OR_TARIFF_STAGE tariff " +
                        "inner join cat_offer_template offer_template on offer_template.code = tariff.offer and offer_template.type = 'OFFER' " +
                        "inner join cat_product_offer_tmpl_cat ass on ass.product_id = offer_template.id " +
                        "inner join cat_offer_template_category cat on cat.id = ass.offer_template_cat_id and LOWER(cat.cf_values\\:\\:json->'offerType'->0->>'string') = LOWER('" + exportDto.getExportType() + "')";
            }


            QueryBuilder queryBuilder = new QueryBuilder(sql, "tariff");
            if (exportDto.getPagingAndFiltering() != null) {
                Map<String, Object> filters = exportDto.getPagingAndFiltering().getFilters();

                if (filters != null && !filters.isEmpty()) {
                    NativeExpressionFactory nativeExpressionFactory = new NativeExpressionFactory(queryBuilder, "tariff");
                    filters.keySet().stream()
                            .filter(key -> filters.get(key) != null)
                            .forEach(key -> nativeExpressionFactory.addFilters(key, filters.get(key)));
                    queryBuilder.addPaginationConfiguration(new PaginationConfiguration(filters), "tariff");
                }
            }

            org.hibernate.Query query = queryBuilder.getNativeQuery(getEntityManager(), true);
            List<Map<String, Object>> tariffs = query.list();

            // Create temp file.
            File file = new File(System.getProperty("java.io.tmpdir"), FilenameUtils.removeExtension(exportDto.getFileName()) + ".csv");

            if (file.exists()) {
                file.delete();
            }

            // Delete temp file when program exits.
            file.deleteOnExit();

            // Write to temp file
            BufferedWriter out = new BufferedWriter(new FileWriter(file));

            if (fieldDescriptions != null) {
                out.write(String.join(",", fieldDescriptions));
                out.write(System.lineSeparator());
            }


            if (tariffs != null && !tariffs.isEmpty()) {
                for (Map<String, Object> record : tariffs) {
                    List<String> fieldValues = new ArrayList<>();
                    for (String field : fieldNames) {
                        Object value = record.get(field) != null ? record.get(field) : "";
                        if (value instanceof Date) {
                            fieldValues.add(DateUtils.formatDateWithPattern((Date) value, Utils.DATE_TIME_PATTERN));
                        } else if (value instanceof BigDecimal) {
                            fieldValues.add(((BigDecimal) value).toPlainString());
                        } else {
                            fieldValues.add(String.valueOf(value));
                        }
                    }
                    if (!fieldValues.isEmpty()) {
                        //out.write(String.join(",", fieldValues));
                        out.write(fieldValues.stream().map(s -> "\"" + s + "\"").collect(Collectors.joining(",")));
                        out.write(System.lineSeparator());
                    }
                }
            }
            out.close();
            return file;
        } catch (IOException ex) {
            throw new BusinessException("Error during the export file  " + exportDto.getFileName() + " : " + ex.getMessage(), ex);
        }
    }    
}