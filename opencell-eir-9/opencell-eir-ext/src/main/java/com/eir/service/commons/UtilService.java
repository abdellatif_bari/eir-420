package com.eir.service.commons;

import com.eir.commons.enums.ChargeTypeEnum;
import com.eir.commons.enums.customfields.SubscriptionCFsEnum;
import com.eir.script.OneShotAndRecurringRatingScript;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.NoChargeException;
import org.meveo.admin.exception.NoPricePlanException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.api.commons.Utils;
import org.meveo.api.dto.billing.ChargeInitializationParametersDto;
import org.meveo.api.dto.order.OrderDto;
import org.meveo.commons.utils.QueryBuilder;
import org.meveo.commons.utils.StringUtils;
import org.meveo.jpa.EntityManagerWrapper;
import org.meveo.jpa.MeveoJpa;
import org.meveo.model.DatePeriod;
import org.meveo.model.ICustomFieldEntity;
import org.meveo.model.admin.FileFormat;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.OneShotChargeInstance;
import org.meveo.model.billing.RecurringChargeInstance;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.catalog.Calendar;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.crm.custom.CustomFieldTypeEnum;
import org.meveo.model.crm.custom.CustomFieldValue;
import org.meveo.model.shared.DateUtils;
import org.meveo.security.CurrentUser;
import org.meveo.security.MeveoUser;
import org.meveo.service.admin.impl.FileFormatService;
import org.meveo.service.billing.impl.WalletOperationService;
import org.meveo.service.catalog.impl.CalendarService;
import org.meveo.service.crm.impl.CustomFieldTemplateService;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSContext;
import javax.jms.JMSProducer;
import javax.jms.Queue;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * EIR util service
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirUtilService")
public class UtilService {

    @Inject
    @MeveoJpa
    private EntityManagerWrapper emWrapper;
    @Inject
    protected CustomFieldTemplateService customFieldTemplateService;
    @Inject
    protected OneShotAndRecurringRatingScript ratingScript;
    @Inject
    protected CalendarService calendarService;
    @Inject
    private WalletOperationService walletOperationService;
    @Inject
    private FileFormatService fileFormatService;
    @Inject
    @CurrentUser
    protected MeveoUser currentUser;
    @Resource(lookup = "java:/jms/queue/OrderQueue")
    private Queue orderQueue;
    @Inject
    private JMSContext jmsContext;

    /**
     * Add overlap criteria
     *
     * @param queryBuilder       the query builder
     * @param startDateFieldName the start date field name
     * @param endDateFieldName   the end date field name
     * @param startDate          the start date
     * @param endDate            the end date
     */
    public void addOverlapCriteria(QueryBuilder queryBuilder, String startDateFieldName, String endDateFieldName, Date startDate, Date endDate) {
        StringBuffer criteria = new StringBuffer();
        if (startDate == null || endDate == null) {
            if (startDate == null) {
                criteria.append(startDateFieldName).append(" is null ");
            }
            if (endDate == null) {
                if (criteria.length() > 0) {
                    criteria.append(" or ");
                }
                criteria.append(endDateFieldName).append(" is null ");
            }
            criteria.insert(0, "(");
            criteria.append(")");
        }
        if (criteria.length() > 0) {
            criteria.append(" or ");
        }

        criteria.append("(").append(startDateFieldName).append(" is null and ").append(endDateFieldName).append(" is null ").append(")");
        if (startDate != null) {
            criteria.append(" or (").append(startDateFieldName).append(" is null and ").append(endDateFieldName).append(" > :startDate)");
            if (endDate == null) {
                criteria.append(" or (").append(startDateFieldName).append(" <= :startDate and :startDate < ").append(endDateFieldName).append(")");
                criteria.append(" or (").append(startDateFieldName).append(" <= :startDate and ").append(endDateFieldName).append(" is null ").append(")");
                criteria.append(" or (").append(startDateFieldName).append(" is null ").append(" and :startDate < ").append(endDateFieldName).append(")");
            }
        }
        if (endDate != null) {
            criteria.append(" or (").append(endDateFieldName).append(" is null and ").append(startDateFieldName).append(" < :endDate)");
            if (startDate == null) {
                criteria.append(" or (").append(startDateFieldName).append(" < :endDate and :endDate < ").append(endDateFieldName).append(")");
                criteria.append(" or (").append(startDateFieldName).append(" < :endDate and ").append(endDateFieldName).append(" is null ").append(")");
                criteria.append(" or (").append(startDateFieldName).append(" is null ").append(" and :endDate < ").append(endDateFieldName).append(")");
            }
        }
        if (startDate != null && endDate != null) {
            criteria.append(" or ((").append(startDateFieldName).append(" is not null and ").append(endDateFieldName).append(" is not null ").append(")");
            criteria.append(" and ((").append(startDateFieldName).append(" <= :startDate and :startDate < ").append(endDateFieldName).append(")");
            criteria.append(" or (:startDate <= ").append(startDateFieldName).append(" and ").append(startDateFieldName).append(" < :endDate)))");
        }
        criteria.insert(0, "(");
        criteria.append(")");
        List<Object> parameters = new ArrayList();
        if (startDate != null) {
            parameters.add("startDate");
            parameters.add(startDate);
        }
        if (endDate != null) {
            parameters.add("endDate");
            parameters.add(endDate);
        }
        queryBuilder.addSqlCriterionMultiple(criteria.toString(), parameters.toArray());
    }

    /**
     * Parse multi-value value from string to a map of values
     *
     * @param entity the entity
     * @param cfCode the CF code
     * @param value  the value
     * @return
     */
    public Object getCFValue(ICustomFieldEntity entity, String cfCode, String value) {
        CustomFieldTemplate cft = customFieldTemplateService.findByCodeAndAppliesTo(cfCode, entity);
        if (cft != null && cft.getFieldType() == CustomFieldTypeEnum.MULTI_VALUE) {
            return cft.deserializeMultiValue(value, null);
        }
        return null;
    }

    /**
     * Parse multi-value value from map of values to a string
     *
     * @param entity    the entity
     * @param cfCode    the CF code
     * @param mapValues the map values
     * @return
     */
    public String getCFValue(ICustomFieldEntity entity, String cfCode, Map<String, Object> mapValues) {
        CustomFieldTemplate cft = customFieldTemplateService.findByCodeAndAppliesTo(cfCode, entity);
        if (cft != null && cft.getFieldType() == CustomFieldTypeEnum.MULTI_VALUE) {
            return cft.serializeMultiValue(mapValues);
        }
        return null;
    }

    /**
     * Get attribute name
     *
     * @param entity        the entity
     * @param cfCode        the CF code
     * @param effectDate    the effect date
     * @param attributeName the attribute name
     * @return the attribute name
     */
    @SuppressWarnings("unchecked")
    public String getValueAttribute(ICustomFieldEntity entity, String cfCode, Date effectDate, String attributeName) {
        Map<String, String> cfValue = (Map<String, String>) entity.getCfValue(cfCode, effectDate);
        if (cfValue != null) {
            for (Map.Entry<String, String> entry : cfValue.entrySet()) {
                Map<String, String> value = (Map<String, String>) getCFValue(entity, cfCode, entry.getValue());
                if (value != null && !value.isEmpty()) {
                    return value.get(attributeName);
                }
            }
        }
        return null;
    }

    /**
     * Get a RAW single custom field value entity for a given custom field for a given date.
     *
     * @param cfCode Custom field code
     * @param date   Date
     * @return CF value entity
     */
    public CustomFieldValue getCfValue(Map<String, List<CustomFieldValue>> valuesByCode, String cfCode, Date date) {
        if (valuesByCode == null) {
            return null;
        }
        List<CustomFieldValue> cfValues = valuesByCode.get(cfCode);
        if (cfValues != null && !cfValues.isEmpty()) {
            CustomFieldValue valueFound = null;
            for (CustomFieldValue cfValue : cfValues) {
                if (cfValue.getPeriod() == null && (valueFound == null || valueFound.getPriority() < cfValue.getPriority())) {
                    valueFound = cfValue;

                } else if (cfValue.getPeriod() != null && cfValue.getPeriod().isCorrespondsToPeriod(date)) {
                    if (valueFound == null || valueFound.getPriority() < cfValue.getPriority()) {
                        valueFound = cfValue;
                    }
                }
            }
            return valueFound;
        }

        return null;
    }

    /**
     * Get last CF version
     *
     * @param entity               the entity
     * @param cfCode               the cf code
     * @param effectDate           the effect date
     * @param ignorePeriodsOverlap ignore periods overlap
     * @return last CF version
     * @throws ValidationException the validation exception
     */
    public CustomFieldValue getLastCfVersion(ICustomFieldEntity entity, String cfCode, Date effectDate, boolean ignorePeriodsOverlap)
            throws ValidationException {

        CustomFieldValue lastCfValue = null;
        CustomFieldValue overlapCfValue = null;

        if (entity != null) {
            if (entity.getCfValues() != null) {
                Map<String, List<CustomFieldValue>> valuesByCode = entity.getCfValues().getValuesByCode();
                if (valuesByCode != null && valuesByCode.get(cfCode) != null) {
                    List<CustomFieldValue> customFieldValueList = valuesByCode.get(cfCode);
                    for (CustomFieldValue customFieldValue : customFieldValueList) {
                        if (customFieldValue != null && customFieldValue.getPeriod() != null) {
                            if (!ignorePeriodsOverlap && customFieldValue.getPeriod().isCorrespondsToPeriod(effectDate, null, false)) {
                                if (customFieldValue.getPeriod().getTo() != null || effectDate.before(customFieldValue.getPeriod().getFrom())) {
                                    overlapCfValue = customFieldValue;
                                    break;
                                }
                            }
                            if (customFieldValue.getPeriod().getTo() == null && (lastCfValue == null || lastCfValue.getPriority() < customFieldValue.getPriority())) {
                                lastCfValue = customFieldValue;
                            }
                        }
                    }
                }
            }

            if (overlapCfValue != null) {
                // A matching period From-To was found. Please change the value of new period instead
                String period = "";
                if (overlapCfValue.getPeriod() != null) {
                    String from = DateUtils.formatDateWithPattern(overlapCfValue.getPeriod().getFrom(), Utils.DATE_DEFAULT_PATTERN);
                    String to = DateUtils.formatDateWithPattern(overlapCfValue.getPeriod().getTo(), Utils.DATE_DEFAULT_PATTERN);
                    if (!StringUtils.isBlank(from) && !StringUtils.isBlank(to)) {
                        period = from + "-" + to;
                    } else {
                        period = from;
                    }
                }
                throw new ValidationException("There is already a matching period (" + period + ") in the " + cfCode + " matrix for the new period (" +
                        DateUtils.formatDateWithPattern(effectDate, Utils.DATE_DEFAULT_PATTERN) + "). Please change the value of new period instead");
            } else {
                return lastCfValue;
            }
        }
        return null;
    }

    /**
     * Add the new version of CF value
     *
     * @param entity     the entity
     * @param effectDate the effect date
     * @param cfCode     the CF code
     * @param endDate    the end date
     * @param cfValue    the CF value
     */
    public void addNewVersionCfValue(ICustomFieldEntity entity, Date effectDate, String cfCode, Date endDate, Map<String, String> cfValue) {
        DatePeriod datePeriod = new DatePeriod(DateUtils.truncateTime(effectDate), DateUtils.truncateTime(endDate));
        entity.setCfValue(cfCode, datePeriod, -1, cfValue);
    }

    /**
     * Update the CF value
     *
     * @param entity      the entity
     * @param effectDate  the effect date
     * @param cfCode      the CF code
     * @param endDate     the end date
     * @param cfValue     the CF value
     * @param lastCfValue the last CF value
     */
    public void updateCfValue(ICustomFieldEntity entity, Date effectDate, String cfCode, Date endDate, Map<String, String> cfValue, CustomFieldValue lastCfValue) {

        if (cfValue != null) {
            if (lastCfValue != null) {
                // End Date should be set to the Effective Date/Time on the bill item minus one second
                //lastCfValue.getPeriod().setTo(Utils.addSecondsToDate(effectDate, -1));
                lastCfValue.getPeriod().setTo(effectDate);
            }
            addNewVersionCfValue(entity, effectDate, cfCode, endDate, cfValue);
        }
    }

    /**
     * Get charge initialization parameters from a tariff line
     *
     * @param offerTemplate     the offer template
     * @param serviceTemplate   the service template
     * @param billingAccount    the billing account
     * @param serviceParameters the service parameters CF
     * @param operationDate     the operation date
     * @param quantity          the quantity
     * @param chargeType        charge type
     * @return charge initialization parameters from a tariff line
     * @throws ValidationException the validation exception
     */
    public ChargeInitializationParametersDto getChargeInitParametersFromTariff(OfferTemplate offerTemplate, ServiceTemplate serviceTemplate,
                                                                               BillingAccount billingAccount, Map<String, Object> serviceParameters,
                                                                               Date operationDate, BigDecimal quantity, ChargeTypeEnum chargeType) throws ValidationException {
        try {

            List<OneShotAndRecurringRatingScript.TariffRecordGroup> tariffRecordGroups = ratingScript.getMatchingTariffRecords(
                    billingAccount.getCustomerAccount().getCustomer(), offerTemplate,
                    serviceTemplate, chargeType, quantity, operationDate,
                    chargeType.isOneShot() ? null : new DatePeriod(operationDate, DateUtils.addDaysToDate(operationDate, 1)),
                    serviceParameters, false);

            if (tariffRecordGroups != null && !tariffRecordGroups.isEmpty() && tariffRecordGroups.get(0).getTariffRecords() != null &&
                    !tariffRecordGroups.get(0).getTariffRecords().isEmpty()) {
                return new ChargeInitializationParametersDto(tariffRecordGroups.get(0).getTariffRecords().get(0).getDescription(),
                        tariffRecordGroups.get(0).getRecurringCalendarId());
            }
        } catch (NoChargeException | NoPricePlanException ex) {
            throw new ValidationException(ex.getMessage());
        }
        return null;
    }

    /**
     * Update recurring charge instances
     *
     * @param serviceInstance      the service instance
     * @param chargeInitParameters the charge init parameters
     */
    public void updateRecurringChargeInstances(ServiceInstance serviceInstance, ChargeInitializationParametersDto chargeInitParameters) {
        if (serviceInstance.getRecurringChargeInstances() != null && !serviceInstance.getRecurringChargeInstances().isEmpty()) {
            Date rateUntilDate = serviceInstance.getSubscriptionDate();

            for (RecurringChargeInstance recurringChargeInstance : serviceInstance.getRecurringChargeInstances()) {
                recurringChargeInstance.setCode(serviceInstance.getCode() + "_RC");
                recurringChargeInstance.setCalendar(calendarService.findById(chargeInitParameters.getRecurringCalendarId()));
                recurringChargeInstance.setDescription(chargeInitParameters.getDescription());

                // In case of apply charge at the end of period, calculate the next period start date
                if (!walletOperationService.isApplyInAdvance(recurringChargeInstance)) {
                    Calendar calendar = CalendarService.initializeCalendar(recurringChargeInstance.getCalendar(), recurringChargeInstance.getSubscriptionDate(), recurringChargeInstance, serviceInstance);
                    Date nextPeriodStart = calendar.nextCalendarDate(serviceInstance.getSubscriptionDate());
                    if (nextPeriodStart != null && (rateUntilDate == null || nextPeriodStart.after(rateUntilDate))) {
                        rateUntilDate = nextPeriodStart;
                    }
                }
            }

            // Force to apply the first recurring charge when date is in the future
            if (rateUntilDate.after(new Date())) {
                java.util.Calendar calendar = java.util.Calendar.getInstance();
                calendar.setTime(rateUntilDate);
                calendar.add(java.util.Calendar.SECOND, 1);
                serviceInstance.setRateUntilDate(calendar.getTime());
            }
        }
    }

    /**
     * Update subscription charge instances
     *
     * @param serviceInstance      the service instance
     * @param chargeInitParameters the charge init parameters
     */
    public void updateSubscriptionChargeInstances(ServiceInstance serviceInstance, ChargeInitializationParametersDto chargeInitParameters) {
        if (serviceInstance.getSubscriptionChargeInstances() != null && !serviceInstance.getSubscriptionChargeInstances().isEmpty()) {
            for (OneShotChargeInstance oneShotChargeInstance : serviceInstance.getSubscriptionChargeInstances()) {
                oneShotChargeInstance.setCode(serviceInstance.getCode() + "_NRC");
                oneShotChargeInstance.setDescription(chargeInitParameters.getDescription());
            }
        }
    }


    /**
     * Get the field names descriptions from configuration Template of provided file format
     *
     * @param fileFormatCode the file format code
     * @return the field names descriptions
     */
    public LinkedHashMap<String, String> getFieldNamesDescriptions(String fileFormatCode) {
        try {
            LinkedHashMap<String, String> fieldNames = null;
            FileFormat fileFormat = fileFormatService.findByCode(fileFormatCode);
            if (fileFormat != null && !StringUtils.isBlank(fileFormat.getConfigurationTemplate())) {
                DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();

                Document document = documentBuilder.parse(new InputSource(new StringReader(fileFormat.getConfigurationTemplate())));

                XPathFactory xpf = XPathFactory.newInstance();
                XPath xpath = xpf.newXPath();
                XPathExpression fieldsExpression = xpath.compile("/beanio/stream/record[@name ='header']/field");
                NodeList fieldsList = (NodeList) fieldsExpression.evaluate(document, XPathConstants.NODESET);

                if (fieldsList != null && fieldsList.getLength() > 0) {
                    fieldNames = new LinkedHashMap<>();
                    for (int i = 0; i < fieldsList.getLength(); i++) {
                        Element fieldElement = (Element) fieldsList.item(i);
                        fieldNames.put(fieldElement.getAttribute("name"), fieldElement.getAttribute("default"));
                    }
                }
            }
            return fieldNames;
        } catch (ParserConfigurationException | SAXException | IOException | XPathExpressionException ex) {
            throw new BusinessException("Error during the extraction field names from configuration Template of file format " + fileFormatCode + " : " + ex.getMessage(), ex);
        }
    }

    /**
     * Set SERVICE_ID CF value.
     *
     * @param subscription the subscription
     */
    public void setServiceIdCF(Subscription subscription, String subscriptionCode) {
        if (!StringUtils.isBlank(subscriptionCode) && subscriptionCode.matches(".*\\d.*")) {
            subscription.setCfValue(SubscriptionCFsEnum.SERVICE_ID.name(), Long.valueOf(subscriptionCode.replaceAll("[^\\d]", "")));
        }
    }


    /**
     * Set SERVICE_ID CF value.
     *
     * @param subscription the subscription
     */
    public void setServiceIdCF(Subscription subscription) {
        setServiceIdCF(subscription, subscription.getCode());
    }

    /**
     * Set the current user
     *
     * @param orderDto the order Dto
     * @return the JMS Producer
     */
    private JMSProducer getJMSProducer(OrderDto orderDto) {
        MeveoUser lastCurrentUser = currentUser.unProxy();
        orderDto.setProviderCode(lastCurrentUser.getProviderCode());
        orderDto.setUserName(lastCurrentUser.getUserName());
        return jmsContext.createProducer();
    }

    /**
     * Send order to the order queue
     *
     * @param orderDto the order Dto
     */
    public void sendToOrderQueue(OrderDto orderDto) {
        getJMSProducer(orderDto).send(orderQueue, orderDto);
    }

    /**
     * Send order to the order queue
     *
     * @param orderDto the order Dto
     */
    public void sendToOrderQueue(OrderDto orderDto, int priority, int delay) {
        getJMSProducer(orderDto).setPriority(priority).setDeliveryDelay(delay).send(orderQueue, orderDto);
    }
}