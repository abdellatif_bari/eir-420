package com.eir.service.catalog.impl;

import org.meveo.model.catalog.OfferServiceTemplate;
import org.meveo.model.catalog.ServiceTemplate;

import javax.ejb.Stateless;
import javax.enterprise.inject.Specializes;
import javax.persistence.Query;
import java.util.List;

/**
 * EIR service template service
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirServiceTemplateService")
@Specializes
public class ServiceTemplateService extends org.meveo.service.catalog.impl.ServiceTemplateService {

    public List<OfferServiceTemplate> getOfferServiceTemplates(ServiceTemplate serviceTemplate) {
        if (serviceTemplate != null) {
            Query query = getEntityManager().createQuery("from OfferServiceTemplate offerService where offerService.serviceTemplate = :serviceTemplate");
            query.setParameter("serviceTemplate", serviceTemplate);
            return query.getResultList();
        }
        return null;
    }

}

