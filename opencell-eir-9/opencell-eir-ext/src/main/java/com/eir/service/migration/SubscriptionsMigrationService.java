package com.eir.service.migration;

import java.math.BigInteger;
import java.sql.BatchUpdateException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.inject.Inject;
import javax.persistence.EntityManager;

import org.apache.commons.lang3.StringUtils;
import org.hibernate.Session;
import org.meveo.admin.exception.BusinessException;
import org.meveo.jpa.EntityManagerProvider;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.base.BaseService;

public class SubscriptionsMigrationService extends BaseService {

    @Inject
    private EntityManagerProvider entityManagerProvider;
    
    private PreparedStatement ps_query_account_entity = null;
    private PreparedStatement ps_query_user_account = null;
    private PreparedStatement ps_query_wallet_operation = null;
    private PreparedStatement ps_query_update_user_account = null;
    private PreparedStatement ps_query_subscription_with_engDate = null;
    private PreparedStatement ps_query_subscription_without_engDate = null;
    private PreparedStatement ps_query_sub_endpoint = null;
    private PreparedStatement ps_query_sub_association = null; 
    private PreparedStatement ps_query_access_point = null; 

    private Long accountEntitySeq = null;
    private Long billingWalletSeq = null;
    private Long billingSubscriptionSeq = null;
    private Long subEndpointSeq = null;
    private Long subAssociationSeq = null;
    private Long accessPointSeq = null;

    private BigInteger sellerId = null;
    private Map<String,Long> offers = new HashMap<>();
    private Map<String,Long> billingAccounts = new HashMap<>();

    @SuppressWarnings("unchecked")
    public void migrateSubscriptions(List<Map<String, Object>> records) { 
        
        try {
            accountEntitySeq =  ((BigInteger)getEntityManager()
                    .createNativeQuery("SELECT last_value FROM account_entity_seq")
                    .getSingleResult()).longValue();
            billingWalletSeq = ((BigInteger)getEntityManager()
                    .createNativeQuery("SELECT last_value FROM billing_wallet_seq")
                    .getSingleResult()).longValue();
            billingSubscriptionSeq = ((BigInteger)getEntityManager()
                    .createNativeQuery("SELECT last_value FROM billing_subscription_seq")
                    .getSingleResult()).longValue();
            subEndpointSeq = ((BigInteger)getEntityManager()
                    .createNativeQuery("SELECT last_value FROM eir_sub_endpoint_seq")
                    .getSingleResult()).longValue();
            subAssociationSeq = ((BigInteger)getEntityManager()
                    .createNativeQuery("SELECT last_value FROM eir_sub_association_seq")
                    .getSingleResult()).longValue();           
            accessPointSeq = ((BigInteger)getEntityManager()
                    .createNativeQuery("SELECT last_value FROM medina_access_seq")
                    .getSingleResult()).longValue();

            try {
                sellerId = (BigInteger) getEntityManager().createNativeQuery("SELECT id FROM account_entity WHERE account_type = 'ACCT_S' AND code ='OPENEIR'")
                    .getSingleResult();
            } catch(Exception ex) {
                throw new BusinessException("Seller 'OPENEIR' not found!");
            }
            
            try {
                List<Object[]> offerTemplates = getEntityManager().createNativeQuery("select code,id from cat_offer_template")
                    .getResultList();
                
                for (Object[] offerTemplate : offerTemplates) {                   
                    offers.put((String)offerTemplate[0], ((BigInteger)offerTemplate[1]).longValue());                    
                }
            } catch(Exception ex) {
                throw new BusinessException("Problem retrieving offers from DB!");
            }
            
            records.stream().map(record -> record.get("billing_account_code")).forEach(billingAccountCode->{
                try {
                    List<Object[]> billingAccount = getEntityManager().createNativeQuery("SELECT code,id FROM account_entity WHERE code= :billingAccountCode AND account_type='ACCT_BA' ")
                        .setParameter("billingAccountCode", billingAccountCode)
                        .getResultList();
                    billingAccounts.put((String) billingAccount.get(0)[0],((BigInteger) billingAccount.get(0)[1]).longValue());                   
                } catch(Exception ex) {
                    throw new BusinessException("Billing account not found : " + billingAccountCode);
                }
            });
        } catch(Exception e) {
            throw e;
        }
        Session hibernateSession = getEntityManager().unwrap(Session.class);

        hibernateSession.doWork(new org.hibernate.jdbc.Work() {

            @Override
            public void execute(Connection connection) throws SQLException {
                ps_query_account_entity = null;
                ps_query_user_account = null;
                ps_query_wallet_operation = null;
                ps_query_wallet_operation = null;
                ps_query_update_user_account = null;
                ps_query_subscription_with_engDate = null;
                ps_query_subscription_without_engDate = null;
                ps_query_sub_endpoint = null;
                ps_query_sub_association = null;
                ps_query_access_point = null;
                try {
                    for(Map<String, Object> record : records) {
                     //   String type = nil(record.get("type"));
                        String code = nil(record.get("code"));
                        String offer = nil(record.get("offer"));
                        String billingAccountCode = nil(record.get("billing_account_code"));
                        String status = nil(record.get("status"));
                        String subscribed = nil(record.get("subscribed"));
                        String eoengDate = nil(record.get("eoeng_date"));
                        String oaoOrderno = nil(record.get("oao_orderno"));
                        String agentCode = nil(record.get("agent_code"));
                        String uan = nil(record.get("uan"));
                        String aendpointid = nil(record.get("aendpointid"));
                        String a_name = nil(record.get("a_name"));
                        String a_address_1 = nil(record.get("a_address_1"));
                        String a_address_2 = nil(record.get("a_address_2"));
                        String a_address_3 = nil(record.get("a_address_3"));
                        String a_address_city = nil(record.get("a_address_city"));
                        String a_zipcode = nil(record.get("a_zipcode"));
                        String a_country = nil(record.get("a_country"));
                        String bendpointid = nil(record.get("bendpointid"));
                        String b_name = nil(record.get("b_name"));
                        String b_address_1 = nil(record.get("b_address_1"));
                        String b_address_2 = nil(record.get("b_address_2"));
                        String b_address_3 = nil(record.get("b_address_3"));
                        String b_address_city = nil(record.get("b_address_city"));
                        String b_zipcode = nil(record.get("b_zipcode"));
                        String b_country = nil(record.get("b_country"));
                        String associationa_subsid = nil(record.get("associationa_subsid"));
                        String associationb_subsid = nil(record.get("associationb_subsid"));
                        String associationc_subsid = nil(record.get("associationc_subsid"));
                                                
                        String cfValues = "{\"OAO_ORDERNO\":[{\"string\":\"" + oaoOrderno + "\"}],"
                                +  "\"UAN\":[{\"from\":\""+ DateUtils.changeFormat(subscribed, "yyyyMMdd", "yyyy-MM-dd'T'00:00:00") + "\",\"priority\":0,\"string\":\""+ uan +"\"}],"
                                + "\"AGENT_CODE\":[{\"string\": \""+ agentCode +"\"}]}";
                        
                        if (code != null && code.matches(".*\\d.*")) {
                            String subscriptionCode = code.replaceAll("[^\\d]", "");
                            cfValues = "{\"OAO_ORDERNO\":[{\"string\":\"" + oaoOrderno + "\"}],"
                                    +  "\"UAN\":[{\"from\":\""+ DateUtils.changeFormat(subscribed, "yyyyMMdd", "yyyy-MM-dd'T'00:00:00") + "\",\"priority\":0,\"string\":\""+ uan +"\"}],"
                                    + "\"AGENT_CODE\":[{\"string\": \""+ agentCode +"\"}],\"SERVICE_ID\":[{\"long\":"+Long.valueOf(subscriptionCode)+"}]}";
                        }
                        
                        if("ACTIVATED".equalsIgnoreCase(status)) {
                            status = "ACTIVE";
                        }                      
                        
                        // create account_entity
                        String query_account_entity = "INSERT INTO account_entity(id,version,code,account_type,created,uuid) "
                                 + " VALUES(?,1,?,'ACCT_UA', NOW(),?)";
                        
                        //create user_account
                        String query_user_account = "insert into billing_user_account(id,billing_account_id,status,status_date,subscription_date) "
                                + "values(?,(SELECT id FROM account_entity WHERE code= ? AND account_type='ACCT_BA') " 
                                + ",'ACTIVE',to_timestamp(?,'YYYYMMDD'),to_timestamp(?,'YYYYMMDD'))";
                
                        //create wallet_operation
                        String query_wallet_operation = "INSERT INTO billing_wallet(id,version,created,user_account_id,code) "
                                + " VALUES(?,1,NOW(),?,'PRINCIPAL')";
                
                        // update wallet operation in user account
                        String query_update_user_account = "UPDATE billing_user_account SET wallet_id = ? " 
                                + " WHERE id = ?";
                        
                        //create subscription with engagement date
                        String query_subscription_with_engDate = "INSERT INTO billing_subscription(id,version,created,status_date,subscription_date,code,end_agrement_date,status,offer_id,user_account_id,uuid,seller_id,cf_values) "
                                + " VALUES(?,1,NOW(),to_timestamp(?,'YYYYMMDD'),to_timestamp(?,'YYYYMMDD'),?, "
                                + " to_timestamp(?,'YYYYMMDD'),?,?,?"
                                + ",?,?,?)";
            
                        //create subscription without engagement date
                        String query_subscription_without_engDate = "INSERT INTO billing_subscription(id,version,created,status_date,subscription_date,code,status,offer_id,user_account_id,uuid,seller_id,cf_values) "
                                + " VALUES(?,1,NOW(),to_timestamp(?,'YYYYMMDD'),to_timestamp(?,'YYYYMMDD'),?, "
                                + " ?,?,?,?,?,?)";
                
                        //create subEndPoint
                        String query_sub_endpoint = "INSERT INTO eir_sub_endpoint(id,subscription_id,ep_id,ep_end,address_line_1,address_line_2,address_line_3,postal_district,country,ep_name,eir_code,ep_version) "
                             + " VALUES(?,?,?,?,?,?,?,?,?,?,?,1)"; 
                                  
                        //create subAssociation A
                        String query_sub_association = "INSERT INTO eir_sub_association(id,subscription_id,assoc_circ_id,assoc_end,address_line_1,address_line_2,address_line_3,postal_district,country,eir_code,assoc_version) "
                             + " VALUES(?,?,?,?,?,?,?,?,?,?,1)";  
                                                
                        //create access point
                        String query_access_point = "INSERT INTO medina_access(id,version,acces_user_id,start_date,end_date,subscription_id,uuid,created)" 
                             + " VALUES (?,1,?,to_timestamp(?,'YYYYMMDD'),NULL,?,?,NOW())";
                        if(ps_query_account_entity == null) {
                            ps_query_account_entity = connection.prepareStatement(query_account_entity);
                        }
                        if(ps_query_user_account == null) {
                            ps_query_user_account = connection.prepareStatement(query_user_account);
                        }
                        if(ps_query_wallet_operation == null) {
                            ps_query_wallet_operation = connection.prepareStatement(query_wallet_operation);
                        }
                        if(ps_query_update_user_account == null) {
                            ps_query_update_user_account = connection.prepareStatement(query_update_user_account);
                        }
                        if(ps_query_subscription_with_engDate == null) {
                            ps_query_subscription_with_engDate = connection.prepareStatement(query_subscription_with_engDate);
                        }
                        if(ps_query_subscription_without_engDate == null) {
                            ps_query_subscription_without_engDate = connection.prepareStatement(query_subscription_without_engDate);
                        }
                        if(ps_query_sub_endpoint == null) {
                            ps_query_sub_endpoint = connection.prepareStatement(query_sub_endpoint);
                        }
                        if(ps_query_sub_association == null) {
                            ps_query_sub_association = connection.prepareStatement(query_sub_association);
                        }             
                        if(ps_query_access_point == null) {
                            ps_query_access_point = connection.prepareStatement(query_access_point);
                        }                    
              
                        ps_query_account_entity.setLong(1, ++accountEntitySeq);
                        ps_query_account_entity.setString(2,code);
                        ps_query_account_entity.setString(3,UUID.randomUUID().toString());
                        ps_query_account_entity.addBatch();
                        
                        ps_query_user_account.setLong(1, accountEntitySeq);
                        ps_query_user_account.setString(2,billingAccountCode);
                        ps_query_user_account.setString(3,subscribed);
                        ps_query_user_account.setString(4,subscribed);
                        ps_query_user_account.addBatch();
                        
                        ps_query_wallet_operation.setLong(1, ++billingWalletSeq);
                        ps_query_wallet_operation.setLong(2,accountEntitySeq);
                        ps_query_wallet_operation.addBatch();
    
                        ps_query_update_user_account.setLong(1, billingWalletSeq);
                        ps_query_update_user_account.setLong(2,accountEntitySeq);
                        ps_query_update_user_account.addBatch();
    
                        if(eoengDate != null && ! "".equals(eoengDate)) {
                            ps_query_subscription_with_engDate.setLong(1, ++billingSubscriptionSeq);
                            ps_query_subscription_with_engDate.setString(2,subscribed);
                            ps_query_subscription_with_engDate.setString(3,subscribed);
                            ps_query_subscription_with_engDate.setString(4,code);
                            ps_query_subscription_with_engDate.setString(5,eoengDate);
                            ps_query_subscription_with_engDate.setString(6,status);
                            ps_query_subscription_with_engDate.setLong(7,offers.get(offer));
                            ps_query_subscription_with_engDate.setLong(8,accountEntitySeq);
                            ps_query_subscription_with_engDate.setString(9,UUID.randomUUID().toString());
                            ps_query_subscription_with_engDate.setLong(10,sellerId.longValue());
                            ps_query_subscription_with_engDate.setString(11,cfValues);
                            ps_query_subscription_with_engDate.addBatch();
                        } else {
                            ps_query_subscription_without_engDate.setLong(1, ++billingSubscriptionSeq);
                            ps_query_subscription_without_engDate.setString(2,subscribed);
                            ps_query_subscription_without_engDate.setString(3,subscribed);
                            ps_query_subscription_without_engDate.setString(4,code);
                            ps_query_subscription_without_engDate.setString(5,status);
                            ps_query_subscription_without_engDate.setLong(6,offers.get(offer));
                            ps_query_subscription_without_engDate.setLong(7,accountEntitySeq);
                            ps_query_subscription_without_engDate.setString(8,UUID.randomUUID().toString());
                            ps_query_subscription_without_engDate.setLong(9,sellerId.longValue());
                            ps_query_subscription_without_engDate.setString(10,cfValues);
                            ps_query_subscription_without_engDate.addBatch();
                        }
                        ps_query_sub_endpoint.setLong(1, ++subEndpointSeq);
                        ps_query_sub_endpoint.setLong(2,billingSubscriptionSeq);
                        ps_query_sub_endpoint.setString(3,aendpointid);
                        ps_query_sub_endpoint.setString(4,"A");
                        ps_query_sub_endpoint.setString(5,a_address_1);
                        ps_query_sub_endpoint.setString(6,a_address_2);
                        ps_query_sub_endpoint.setString(7,a_address_3);
                        ps_query_sub_endpoint.setString(8,a_address_city);
                        ps_query_sub_endpoint.setString(9,a_country);
                        ps_query_sub_endpoint.setString(10,a_name);
                        ps_query_sub_endpoint.setString(11,a_zipcode);
                        ps_query_sub_endpoint.addBatch();     
                        
                        ps_query_sub_endpoint.setLong(1, ++subEndpointSeq);
                        ps_query_sub_endpoint.setLong(2,billingSubscriptionSeq);
                        ps_query_sub_endpoint.setString(3,bendpointid);
                        ps_query_sub_endpoint.setString(4, "B");
                        ps_query_sub_endpoint.setString(5,b_address_1);
                        ps_query_sub_endpoint.setString(6,b_address_2);
                        ps_query_sub_endpoint.setString(7,b_address_3);
                        ps_query_sub_endpoint.setString(8,b_address_city);
                        ps_query_sub_endpoint.setString(9,b_country);
                        ps_query_sub_endpoint.setString(10,b_name);
                        ps_query_sub_endpoint.setString(11,b_zipcode);
                        ps_query_sub_endpoint.addBatch();                
                        
                        //Association A
                        if(StringUtils.isNotBlank(associationa_subsid)) {
                            ps_query_sub_association.setLong(1, ++subAssociationSeq);
                            ps_query_sub_association.setLong(2,billingSubscriptionSeq);
                            ps_query_sub_association.setString(3,associationa_subsid);
                            ps_query_sub_association.setString(4, "A");
                            ps_query_sub_association.setString(5,a_address_1);
                            ps_query_sub_association.setString(6,a_address_2);
                            ps_query_sub_association.setString(7,a_address_3);
                            ps_query_sub_association.setString(8,a_address_city);
                            ps_query_sub_association.setString(9,a_country);
                            ps_query_sub_association.setString(10,a_zipcode);
                            ps_query_sub_association.addBatch();
                        }
                        
                        //Association B
                        if(StringUtils.isNotBlank(associationb_subsid)) {
                            ps_query_sub_association.setLong(1, ++subAssociationSeq);
                            ps_query_sub_association.setLong(2,billingSubscriptionSeq);
                            ps_query_sub_association.setString(3,associationb_subsid);
                            ps_query_sub_association.setString(4, "B");
                            ps_query_sub_association.setString(5,a_address_1);
                            ps_query_sub_association.setString(6,a_address_2);
                            ps_query_sub_association.setString(7,a_address_3);
                            ps_query_sub_association.setString(8,a_address_city);
                            ps_query_sub_association.setString(9,a_country);
                            ps_query_sub_association.setString(10,a_zipcode);
                            ps_query_sub_association.addBatch();
                        }

                        //Association C
                        if(StringUtils.isNotBlank(associationc_subsid)) {
                            ps_query_sub_association.setLong(1, ++subAssociationSeq);
                            ps_query_sub_association.setLong(2,billingSubscriptionSeq);
                            ps_query_sub_association.setString(3,associationc_subsid);
                            ps_query_sub_association.setString(4, "C");
                            ps_query_sub_association.setString(5,a_address_1);
                            ps_query_sub_association.setString(6,a_address_2);
                            ps_query_sub_association.setString(7,a_address_3);
                            ps_query_sub_association.setString(8,a_address_city);
                            ps_query_sub_association.setString(9,a_country);
                            ps_query_sub_association.setString(10,a_zipcode);
                            ps_query_sub_association.addBatch();
                        }
                        
                        ps_query_access_point.setLong(1, ++accessPointSeq);
                        ps_query_access_point.setString(2,code);
                        ps_query_access_point.setString(3,subscribed);
                        ps_query_access_point.setLong(4,billingSubscriptionSeq);
                        ps_query_access_point.setString(5,UUID.randomUUID().toString());
                        ps_query_access_point.addBatch();

                    }                    
                    
                    if(ps_query_account_entity != null) {
                        ps_query_account_entity.executeBatch();
                    }
                    if(ps_query_user_account != null) {
                        ps_query_user_account.executeBatch();
                    }
                    if(ps_query_wallet_operation != null) {
                        ps_query_wallet_operation.executeBatch();
                    }
                    if(ps_query_update_user_account != null) {
                        ps_query_update_user_account.executeBatch();
                    }
                    if(ps_query_subscription_with_engDate != null) {
                        ps_query_subscription_with_engDate.executeBatch();
                    }
                    if(ps_query_subscription_without_engDate != null) {
                        ps_query_subscription_without_engDate.executeBatch();
                    }
                    if(ps_query_sub_endpoint != null) {
                        ps_query_sub_endpoint.executeBatch();
                    }

                    if(ps_query_sub_association != null) {
                        ps_query_sub_association.executeBatch();
                    }                    
                    if(ps_query_access_point != null) {
                        ps_query_access_point.executeBatch();
                    }                    

                } catch(Exception e) {
                    if(e instanceof BatchUpdateException) {
                        throw new BusinessException(e.getMessage());
                    }
                    log.error("Failed to bulk insert : ", e.getMessage());
                    throw e;
                } finally {
                    if(ps_query_account_entity != null) {
                        ps_query_account_entity.close();
                    }
                    if(ps_query_user_account != null) {
                        ps_query_user_account.close();
                    }
                    if(ps_query_wallet_operation != null) {
                        ps_query_wallet_operation.close();
                    }
                    if(ps_query_update_user_account != null) {
                        ps_query_update_user_account.close();
                    }
                    if(ps_query_subscription_with_engDate != null) {
                        ps_query_subscription_with_engDate.close();
                    }
                    if(ps_query_subscription_without_engDate != null) {
                        ps_query_subscription_without_engDate.close();
                    }
                    if(ps_query_sub_endpoint != null) {
                        ps_query_sub_endpoint.close();
                    }

                    if(ps_query_sub_association != null) {
                        ps_query_sub_association.close();
                    }                    
                    
                    if(ps_query_access_point != null) {
                        ps_query_access_point.close();
                    }                    
                }
                try {
                    getEntityManager()
                        .createNativeQuery("SELECT setval('account_entity_seq', :seq, true);")
                        .setParameter("seq", accountEntitySeq)
                        .getSingleResult();
                    getEntityManager()
                            .createNativeQuery("SELECT setval('billing_wallet_seq', :seq, true);")
                            .setParameter("seq", billingWalletSeq)
                            .getSingleResult();                    
                    getEntityManager()
                            .createNativeQuery("SELECT setval('billing_subscription_seq', :seq, true);")
                            .setParameter("seq", billingSubscriptionSeq)
                            .getSingleResult();                    
                    getEntityManager()
                            .createNativeQuery("SELECT setval('eir_sub_endpoint_seq', :seq, true);")
                            .setParameter("seq", subEndpointSeq)
                            .getSingleResult();                    
                    getEntityManager()
                            .createNativeQuery("SELECT setval('eir_sub_association_seq', :seq, true);")
                            .setParameter("seq", subAssociationSeq)
                            .getSingleResult(); 
                    getEntityManager()
                            .createNativeQuery("SELECT setval('medina_access_seq', :seq, true);")
                            .setParameter("seq", accessPointSeq)
                            .getSingleResult(); 
                } catch(Exception e) {
                    throw e;
                }
            }
        });
    }
    private String nil(Object text) {
        if (text != null) {
            return (String) text;
        }
        return "";
    }

    /**
     * Return an entity manager for a current provider
     *
     * @return Entity manager
     */
    public EntityManager getEntityManager() {
        return entityManagerProvider.getEntityManager().getEntityManager();
    }
}