package com.eir.service.keycloak;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.ejb.Stateless;
import javax.enterprise.inject.Specializes;

import org.apache.commons.lang3.StringUtils;
import org.keycloak.KeycloakPrincipal;
import org.keycloak.KeycloakSecurityContext;
import org.keycloak.OAuth2Constants;
import org.keycloak.admin.client.Keycloak;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.admin.client.resource.RealmResource;
import org.keycloak.representations.idm.UserRepresentation;
import org.meveo.admin.exception.BusinessException;
import org.meveo.keycloak.client.KeycloakAdminClientConfig;

/**
 * EIR Keycloak client service
 *
 * @author Mohammed Amine TAZI
 */
@Stateless(name = "EirKeycloakAdminClientService")
@Specializes
public class KeycloakAdminClientService extends org.meveo.keycloak.client.KeycloakAdminClientService {

    @Resource
    private SessionContext securityContext;
        
    public List<UserRepresentation> listUsers() throws BusinessException {    
        KeycloakPrincipal<?> keycloakPrincipal = (KeycloakPrincipal<?>) securityContext.getCallerPrincipal();
        KeycloakSecurityContext session = keycloakPrincipal.getKeycloakSecurityContext();    
        KeycloakAdminClientConfig keycloakAdminClientConfig = loadConfig();
        Keycloak keycloak = getKeycloakClient(session, keycloakAdminClientConfig);
        // Get realm
        RealmResource realmResource = keycloak.realm(keycloakAdminClientConfig.getRealm());
        try {
            return realmResource.users().list();  
            
        } catch (Exception e) {
            throw new BusinessException("Unable to list roles from KC");
        }
    }
    
    public Map<String,String> getGroups() {
        Map<String,String> groups = new HashMap<>();
        KeycloakPrincipal<?> keycloakPrincipal = (KeycloakPrincipal<?>) securityContext.getCallerPrincipal();
        KeycloakSecurityContext session = keycloakPrincipal.getKeycloakSecurityContext();    
        KeycloakAdminClientConfig keycloakAdminClientConfig = loadConfig();
        Keycloak keycloak = getKeycloakClient(session, keycloakAdminClientConfig);
        // Get realm
        RealmResource realmResource = keycloak.realm(keycloakAdminClientConfig.getRealm());
        try {
            List<UserRepresentation> users = realmResource.users().list();  
            for (UserRepresentation user : users) {              
                String groupLabel = realmResource.users().get(user.getId()).groups().stream().map(group-> group.getName()).collect(Collectors.joining(","));
                groups.put(user.getId(), groupLabel);
            }
            return groups;

        } catch (Exception e) {
            throw new BusinessException("Unable to get groups from KC");
        }
    }
    public Map<String,String> getClientRoles() {
        Map<String,String> clientRoles = new HashMap<>();
        KeycloakPrincipal<?> keycloakPrincipal = (KeycloakPrincipal<?>) securityContext.getCallerPrincipal();
        KeycloakSecurityContext session = keycloakPrincipal.getKeycloakSecurityContext();    
        KeycloakAdminClientConfig keycloakAdminClientConfig = loadConfig();
        Keycloak keycloak = getKeycloakClient(session, keycloakAdminClientConfig);
        // Get realm
        RealmResource realmResource = keycloak.realm(keycloakAdminClientConfig.getRealm());
        try {
            List<UserRepresentation> users = realmResource.users().list();  
            for (UserRepresentation user : users) {              
                if(realmResource.users().get(user.getId()).roles().getAll().getClientMappings() != null) {
                    String clientRolesLabel = StringUtils.join(realmResource.users().get(user.getId()).roles().getAll().getClientMappings().keySet(),",");
                    clientRoles.put(user.getId(), clientRolesLabel);
                }
            }
            return clientRoles;

        } catch (Exception e) {
            throw new BusinessException("Unable to get client roles from KC");
        }
    }
    
    private Keycloak getKeycloakClient(KeycloakSecurityContext session, KeycloakAdminClientConfig keycloakAdminClientConfig) {

        KeycloakBuilder keycloakBuilder = KeycloakBuilder.builder().serverUrl(keycloakAdminClientConfig.getServerUrl()).realm(keycloakAdminClientConfig.getRealm())
            .grantType(OAuth2Constants.CLIENT_CREDENTIALS).clientId(keycloakAdminClientConfig.getClientId()).clientSecret(keycloakAdminClientConfig.getClientSecret())
            .authorization(session.getTokenString());

        return keycloakBuilder.build();
    } 
}