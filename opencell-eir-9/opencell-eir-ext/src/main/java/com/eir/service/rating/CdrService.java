package com.eir.service.rating;

import com.eir.commons.enums.StagingRecordActionEnum;
import com.eir.commons.enums.TransformationRulesEnum;
import com.eir.commons.utils.BusinessRulesValidator;
import com.eir.commons.utils.TransformationRulesHandler;
import com.eir.service.commons.ImportService;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.service.base.NativePersistenceService;
import org.meveo.service.billing.impl.ReratingInfo;
import org.meveo.service.billing.impl.ReratingService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.Query;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * EIR CDR service
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirCdrService")
public class CdrService extends NativePersistenceService {

    @Inject
    private BusinessRulesValidator businessRulesValidator;
    @Inject
    private ImportService importService;
    @Inject
    private ReratingService reratingService;


    /**
     * The callback function for CDR validation
     *
     * @return the CDR validation result
     */
    public Function<Object[], Object> validate() {
        return this::validate;
    }

    /**
     * The callback function for CDR activation
     *
     * @return the CDR activation result
     */
    public Function<Object[], Object> activate() {
        return this::activate;
    }

    /**
     * The callback function for tariff re-rating
     *
     * @return the tariff re-rating result
     */
    public Function<Object[], Object> rerate() {
        return this::rerate;
    }

    /**
     * Validate function
     *
     * @param parameters the validation parameters
     * @return the result of CDR validation.
     * @throws ValidationException the validation exception
     */
    public String validate(Object... parameters) throws ValidationException {
        if (parameters[0] instanceof Map<?, ?>) {
            return validateCdr((Map<String, Object>) parameters[0], (TransformationRulesHandler) parameters[1]);
        }
        return validateCdrs((Long) parameters[1]);
    }

    /**
     * Activate function
     *
     * @param parameters the activation parameters
     * @return the result of CDR activation.
     * @throws ValidationException the validation exception
     */
    public String activate(Object... parameters) throws ValidationException {
        return null;
    }

    /**
     * Re-rate function
     *
     * @param parameters the re-rating parameters
     * @return the result of tariff re-rating.
     * @throws ValidationException the validation exception
     */
    public String rerate(Object... parameters) throws ValidationException {
        List<ReratingInfo> reratingInfos = new ArrayList<>();
        reratingService.rerate(reratingInfos, false);
        return null;
    }

    /**
     * Validate CDR
     *
     * @param record                     the CDR record
     * @param transformationRulesHandler the transformation rules handler
     * @return the result of cdr validation.
     * @throws ValidationException the validation exception
     */
    public String validateCdr(Map<String, Object> record, TransformationRulesHandler transformationRulesHandler) throws ValidationException {
        /**
         * Check if there is the combination (rate plan/traffic Class/Time Period) already exists in another file in staging table.
         */
        String sql = "select count(*) from EIR_U_TARIFF_STAGE stage where stage.rate_plan=:ratePlan and stage.traffic_class=:trafficClass and stage.time_period=:timePeriod and stage.file_id!=:fileId";
        Query query = getEntityManager().createNativeQuery(sql);
        query.setParameter("ratePlan", record.get("rate_plan"));
        query.setParameter("trafficClass", record.get("traffic_class"));
        query.setParameter("timePeriod", record.get("time_period"));
        query.setParameter("fileId", record.get("file_id"));
        BigInteger valueMatched = (BigInteger) query.getSingleResult();

        if (valueMatched.longValue() > 0) {
            String errorMessage = "There is the same rate plan " + record.get("rate_plan") + " and the same trafficClass "
                    + record.get("traffic_class") + " and the same time period "
                    + record.get("time_period") + " in another file";
            throw new ValidationException(errorMessage);
        }

        importService.setHashCode(record, transformationRulesHandler);

        return null;
    }

    /**
     * Activate CDR
     *
     * @param fileId the file id
     * @throws BusinessException the business exception
     */
    public void activateCdr(Long fileId) throws BusinessException {
        importService.activateRecord(TransformationRulesEnum.CDR.name(), fileId);
    }

    /**
     * Cancel CDR
     *
     * @param fileId the file id
     * @throws BusinessException the business exception
     */
    public void cancelCdr(Long fileId) throws BusinessException {
        importService.cancelStagingRecords("EIR_U_TARIFF_STAGE", fileId);
    }

    /**
     * Validate CDRs.
     *
     * @param fileId the file id
     * @return the result of CDRs validation.
     * @throws BusinessException the business exception
     */
    public String validateCdrs(Long fileId) throws BusinessException {
        try {
            TransformationRulesHandler transformationRulesHandler = importService.getTransformationRules(TransformationRulesEnum.CDR.name());
            List<String> excludedCriterion = null;
            String sql = "update EIR_U_TARIFF_STAGE set error_reason = null where file_id=:fileId";
            Query query = getEntityManager().createNativeQuery(sql);
            query.setParameter("fileId", fileId).executeUpdate();

            /**
             * Check if a record have an action other than ADD and MODIFY.
             */
            sql = "update EIR_U_TARIFF_STAGE set error_reason='The record action is incorrect' where file_id=:fileId " +
                    "and (error_reason is null or error_reason = '')" +
                    "and LOWER(record_action) != LOWER('" + StagingRecordActionEnum.ADD.name() + "') and LOWER(record_action) != LOWER('" + StagingRecordActionEnum.MODIFY.name() + "')";
            query = getEntityManager().createNativeQuery(sql);
            int affectedRows = query.setParameter("fileId", fileId).executeUpdate();
            if (affectedRows > 0) {
                return null;
            }

            /**
             * Check if a record cannot have a stop date earlier than the start date of that record.
             */
            sql = "update EIR_U_TARIFF_STAGE set error_reason='The end date cannot earlier than the start date' where file_id=:fileId " +
                    "and (error_reason is null or error_reason = '')" +
                    "and ((valid_from is null and valid_to is null)  or (valid_from is not null and valid_to is not null " +
                    "and valid_from > valid_to))";
            query = getEntityManager().createNativeQuery(sql);
            affectedRows = query.setParameter("fileId", fileId).executeUpdate();
            if (affectedRows > 0) {
                return null;
            }

            /**
             * Check if there is the same rate plan and the same tariff class and the same time period in current file in error.
             */
            sql = "update EIR_U_TARIFF_STAGE set error_reason=concat('There are the same rate plan', rate_plan, ' and the same trafficClass ', " +
                    "traffic_class,'and the same time period', time_period, ' in the same file who are invalid') where (error_reason is null or error_reason = '') and id in (" +
                    "select stage1.id from EIR_U_TARIFF_STAGE stage1 inner join EIR_U_TARIFF_STAGE stage2 on stage1.id != stage2.id " +
                    "and stage1.rate_plan = stage2.rate_plan  and stage1.traffic_class = stage2.traffic_class and stage1.time_period = stage2.time_period and stage1.file_id = stage2.file_id " +
                    "and stage2.error_reason is not null where stage1.file_id=:fileId)";
            query = getEntityManager().createNativeQuery(sql);
            affectedRows = query.setParameter("fileId", fileId).executeUpdate();
            if (affectedRows > 0) {
                return null;
            }


            /**
             * Check if there is a not null end date for 'modify' action in staging table.
             */
            excludedCriterion = new ArrayList<>(Arrays.asList("record_action", "valid_to", "file_id"));
            String exactMatchingJoin = " left_table.id != right_table.id and " + businessRulesValidator.getExactMatchingCriteriaForStageTable(transformationRulesHandler,
                    "left_table", "right_table", excludedCriterion, true);

            sql = "update EIR_U_TARIFF_STAGE set error_reason='The MODIFY action can not update the staging CDR tariff having the not null end date'" +
                    "where file_id=:fileId and (error_reason is null or error_reason = '') and id in (" +
                    "select left_table.id from EIR_U_TARIFF_STAGE left_table inner join EIR_U_TARIFF_STAGE right_table on " + exactMatchingJoin +
                    " where LOWER(left_table.record_action) = LOWER('MODIFY') and right_table.valid_to is not null) ";
            query = getEntityManager().createNativeQuery(sql);
            affectedRows = query.setParameter("fileId", fileId).executeUpdate();
            if (affectedRows > 0) {
                return null;
            }

            /**
             * Check end date of existent is start date new  in the active table
             */
           /* String exactMatchADDJoin = "(LOWER(left_table.record_action) = LOWER('ADD') and left_table.id != right_table.id and " +
                    businessRulesValidator.getExactMatchingCriteriaForStageTable(transformationRulesHandler,
                            "left_table", "right_table", null, false) + ")";

            excludedCriterion = new ArrayList<>(Arrays.asList("record_action", "accounting_code", "valid_from", "valid_to", "file_id"));
            

            String dateCondition = " (right_table.valid_to != left_table.valid_from) " +
            		"order by left_table.id  desc";

            sql = "update EIR_U_TARIFF_STAGE set error_reason=concat('The start date is different from end date of existant rate plan')" +
                    "where file_id=:fileId and ( error_reason is null or error_reason = '') and id in (" +
                    "select left_table.id from EIR_U_TARIFF_STAGE left_table inner join EIR_U_TARIFF_STAGE right_table on (" + exactMatchADDJoin + ") where " + dateCondition + ")";
            query = getEntityManager().createNativeQuery(sql);
            affectedRows = query.setParameter("fileId", fileId).executeUpdate();
            if (affectedRows > 0) {
                return null;
            }*/


            /**
             * Check overlapping in the staging table
             */
            String exactMatchingADDJoin = "(LOWER(left_table.record_action) = LOWER('ADD') and left_table.id != right_table.id and " +
                    businessRulesValidator.getExactMatchingCriteriaForStageTable(transformationRulesHandler,
                            "left_table", "right_table", null, false) + ")";

            excludedCriterion = new ArrayList<>(Arrays.asList("record_action", "accounting_code", "valid_from", "valid_to", "file_id"));
            String exactMatchingMODIFYJoin = "(LOWER(left_table.record_action) = LOWER('MODIFY') and left_table.id != right_table.id and " +
                    businessRulesValidator.getExactMatchingCriteriaForStageTable(transformationRulesHandler,
                            "left_table", "right_table", excludedCriterion, true) + ")";

            String overlappingCondition = "((right_table.valid_from is null and right_table.valid_to is null) " +
                    "or (right_table.valid_from is null and left_table.valid_from is null) " +
                    "or (right_table.valid_to is null and left_table.valid_to is null) " +
                    "or (right_table.valid_from <= left_table.valid_from and left_table.valid_from< right_table.valid_to) " +
                    "or (right_table.valid_from is null and right_table.valid_to > left_table.valid_from) " +
                    "or (right_table.valid_to is null and right_table.valid_from < left_table.valid_to ) " +
                    "or (right_table.valid_from is not null and right_table.valid_to is not null " +
                    "and ((right_table.valid_from <= left_table.valid_from and left_table.valid_from < right_table.valid_to " +
                    "or (left_table.valid_from<= right_table.valid_from and right_table.valid_from< left_table.valid_to))))) " +
                    "and left_table.id > right_table .id " +
                    "order by left_table.id  desc";

            sql = "update EIR_U_TARIFF_STAGE set error_reason=concat('The CDR ', rate_plan, ' is overlap with an existing tariff in staging table')" +
                    "where file_id=:fileId and ( error_reason is null or error_reason = '') and id in (" +
                    "select left_table.id from EIR_U_TARIFF_STAGE left_table inner join EIR_U_TARIFF_STAGE right_table on (" + exactMatchingMODIFYJoin + " or " + exactMatchingADDJoin + ") where " + overlappingCondition + ")";
            query = getEntityManager().createNativeQuery(sql);
            affectedRows = query.setParameter("fileId", fileId).executeUpdate();
            if (affectedRows > 0) {
                return null;
            }

            /**
             * Check if there is a not null end date for 'modify' action in valid table.
             */
            excludedCriterion = new ArrayList<>(Arrays.asList("valid_to", "file_name", "updated", "updated_by"));
            exactMatchingJoin = businessRulesValidator.getExactMatchingCriteriaForValidTable(transformationRulesHandler, "left_table", "right_table", excludedCriterion, true);

            sql = "update EIR_U_TARIFF_STAGE set error_reason='The MODIFY action can not update the valid tariff having the not null end date'" +
                    "where file_id=:fileId and (error_reason is null or error_reason = '') and id in (" +
                    "select left_table.id from EIR_U_TARIFF_STAGE left_table inner join EIR_U_TARIFF right_table on " + exactMatchingJoin +
                    " where LOWER(left_table.record_action) = LOWER('MODIFY') and right_table.valid_to is not null) ";
            query = getEntityManager().createNativeQuery(sql);
            query.setParameter("fileId", fileId).executeUpdate();

            /**
             * Check if there is not valid tariff for the stage one in the 'modify' action case.
             */
            excludedCriterion = new ArrayList<>(Arrays.asList("accounting_code_id", "valid_to", "file_name", "updated", "updated_by"));
            exactMatchingJoin = businessRulesValidator.getExactMatchingCriteriaForValidTable(transformationRulesHandler, "left_table", "right_table", excludedCriterion, true);

            sql = "update EIR_U_TARIFF_STAGE set error_reason='No valid tariff was found for this MODIFY action' " +
                    "where file_id=:fileId and LOWER(record_action) = LOWER('MODIFY') and (error_reason is null or error_reason = '') and id not in (" +
                    "select left_table.id from EIR_U_TARIFF_STAGE left_table inner join EIR_U_TARIFF right_table on " + exactMatchingJoin + ")";
            query = getEntityManager().createNativeQuery(sql);
            query.setParameter("fileId", fileId).executeUpdate();

            /**
             * Check overlapping in the valid table
             */
            excludedCriterion = new ArrayList<>(Arrays.asList("record_action", "valid_from", "valid_to", "accounting_code", "file_id"));
            String exactMatchingModifyJoin = " LOWER(right_table.record_action) = LOWER('MODIFY') and " + businessRulesValidator.getExactMatchingCriteriaForStageTable(transformationRulesHandler,
                    "left_table", "right_table", excludedCriterion, true);

            exactMatchingJoin = businessRulesValidator.getExactMatchingCriteriaForValidTable(transformationRulesHandler, "left_table", "right_table", null, false);

            overlappingCondition = "LOWER(left_table.record_action) = LOWER('ADD') " +
                    "and ((right_table.valid_from is null and right_table.valid_to is null) " +
                    "or (right_table.valid_from is null and left_table.valid_from is null) " +
                    "or (right_table.valid_to is null and left_table.valid_to is null) " +
                    "or (right_table.valid_from <= left_table.valid_from and left_table.valid_from< right_table.valid_to) " +
                    "or (right_table.valid_from is null and right_table.valid_to > left_table.valid_from) " +
                    "or (right_table.valid_to is null and right_table.valid_from < left_table.valid_to ) " +
                    "or (right_table.valid_from is not null and right_table.valid_to is not null " +
                    "and ((right_table.valid_from <= left_table.valid_from and left_table.valid_from < right_table.valid_to " +
                    "or (left_table.valid_from<= right_table.valid_from and right_table.valid_from< left_table.valid_to))))) " +
                    "order by left_table.id  desc";


            sql = "update EIR_U_TARIFF_STAGE set error_reason=concat('The CDR rate plan ', rate_plan, ' is overlap with an existing tariff in valid table')" +
                    "where file_id=:fileId and (error_reason is null or error_reason = '') and " +
                    "id not in (select left_table.id from EIR_U_TARIFF_STAGE left_table inner join EIR_U_TARIFF_STAGE right_table on " + exactMatchingModifyJoin + ") and " +
                    "id in ( select left_table.id from EIR_U_TARIFF_STAGE left_table inner join EIR_U_TARIFF right_table on " + exactMatchingJoin + " where " + overlappingCondition + ") ";
            query = getEntityManager().createNativeQuery(sql);
            affectedRows = query.setParameter("fileId", fileId).executeUpdate();
            if (affectedRows > 0) {
                return null;
            }

            /**
             * Check reference data
             */
            if (!importService.validateReferenceData(transformationRulesHandler, fileId, null)) {
                return null;
            }
        } catch (Exception e) {
            throw new BusinessException(e.getMessage());
        }
        return null;
    }
}