package com.eir.service.tariff;

import com.eir.commons.enums.customtables.TariffPlanCustomTableEnum;
import com.eir.commons.enums.customtables.TariffPlanMapCustomTableEnum;
import com.eir.service.commons.UtilService;
import org.hibernate.Query;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.api.custom.CustomTableApi;
import org.meveo.api.dto.custom.CustomTableRecordDto;
import org.meveo.api.dto.custom.UnitaryCustomTableDataDto;
import org.meveo.commons.utils.QueryBuilder;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.customEntities.CustomEntityTemplate;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.base.NativePersistenceService;
import org.meveo.service.custom.CustomTableService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * EIR tariff plan service
 *
 * @author Abdellatif BARI
 */
@Stateless(name = "EirTariffPlanService")
public class TariffPlanService extends NativePersistenceService {

    @Inject
    private CustomTableApi customTableApi;
    @Inject
    private CustomTableService customTableService;
    @Inject
    private UtilService utilService;


    /**
     * Find the tariff plan by code
     *
     * @param tariffPlanCode a tariff plan code
     * @return list of tariff plans
     * @throws BusinessException the business exception
     */
    public Map<String, Object> findTariffPlanByCode(String tariffPlanCode) throws BusinessException {
        Map<String, Object> filters = new HashMap<>();
        filters.put(TariffPlanCustomTableEnum.CODE.getLabel(), tariffPlanCode);
        List<Map<String, Object>> tariffPlans = customTableService.list(TariffPlanCustomTableEnum.getCustomTableCode(), new PaginationConfiguration(filters));
        if (tariffPlans != null && !tariffPlans.isEmpty()) {
            if (tariffPlans.size() > 1) {
                throw new BusinessException("More than one tariff plan with code " + tariffPlanCode + " is found");
            }
            return tariffPlans.get(0);
        }
        return null;
    }

    /**
     * Find the tariff plans maps
     *
     * @param tariffPlanDto a tariff plan Dto
     * @return list of tariff plans maps
     */
    public List<Map<String, Object>> findTariffPlanMaps(UnitaryCustomTableDataDto tariffPlanDto) {
        Map<String, Object> filters = new HashMap<>();
        filters.put(TariffPlanMapCustomTableEnum.CUSTOMER_ID.getLabel(), null);
        filters.put(TariffPlanMapCustomTableEnum.OFFER_ID.getLabel(), Long.valueOf((String) tariffPlanDto.getValue().getValues().get(TariffPlanMapCustomTableEnum.OFFER_ID.getLabel())));
        //filters.put(TariffPlanMapCustomTableEnum.TARIFF_PLAN_ID.getLabel(), tariffPlanDto.getValue().getId());
        return customTableService.list(TariffPlanMapCustomTableEnum.getCustomTableCode(), new PaginationConfiguration(filters));
    }

    /**
     * Insert the tariff plans map
     *
     * @param tariffPlanDto a tariff plan Dto
     * @param validFrom     the valid from
     * @throws BusinessException the business exception
     */
    public void insertTariffPlanMap(UnitaryCustomTableDataDto tariffPlanDto, Date validFrom) throws BusinessException {
        UnitaryCustomTableDataDto tariffPlanMapDto = new UnitaryCustomTableDataDto();
        tariffPlanMapDto.setCustomTableCode(TariffPlanMapCustomTableEnum.getCustomTableCode());
        CustomTableRecordDto value = new CustomTableRecordDto();
        LinkedHashMap<String, Object> values = new LinkedHashMap<>();
        values.put(TariffPlanMapCustomTableEnum.OFFER_ID.getLabel(), tariffPlanDto.getValue().getValues().get(TariffPlanMapCustomTableEnum.OFFER_ID.getLabel()));
        values.put(TariffPlanMapCustomTableEnum.TARIFF_PLAN_ID.getLabel(), tariffPlanDto.getValue().getId());
        values.put(TariffPlanMapCustomTableEnum.VALID_FROM.getLabel(), validFrom);
        value.setValues(values);
        tariffPlanMapDto.setValue(value);
        customTableApi.create(tariffPlanMapDto);
    }

    /**
     * Update the tariff plans map
     *
     * @param tariffPlanDto a tariff plan Dto
     * @param values        the tariff plan values
     * @param validFrom     the valid from
     * @throws BusinessException the business exception
     */
    public void updateTariffPlanMap(UnitaryCustomTableDataDto tariffPlanDto, Map<String, Object> values, Date validFrom) throws BusinessException {
        Long tariffPlanId = tariffPlanDto.getValue().getId();
        values.put(TariffPlanMapCustomTableEnum.TARIFF_PLAN_ID.getLabel(), tariffPlanId);
        values.put(TariffPlanMapCustomTableEnum.VALID_FROM.getLabel(), validFrom);

        CustomEntityTemplate cet = customTableService.getCET(TariffPlanMapCustomTableEnum.getCustomTableCode());
        Map<String, CustomFieldTemplate> cfts = customTableService.retrieveAndValidateCfts(cet, false);
        List<Map<String, Object>> newValues = customTableService.convertValues(Arrays.asList(values), cfts.values(), false);
        customTableService.update(cet.getDbTablename(), newValues.get(0));
    }

    /**
     * Create or update tariff plan map
     *
     * @param tariffPlanMapDto
     * @throws BusinessException the business exception
     */
    public void createOrUpdateTariffPlanMap(UnitaryCustomTableDataDto tariffPlanMapDto) throws BusinessException {
        Long tariffPlanMapId = tariffPlanMapDto.getValue().getId();
        QueryBuilder queryBuilder = customTableService.getQuery(TariffPlanMapCustomTableEnum.getCustomTableCode(), null);

        if (tariffPlanMapId != null) {
            queryBuilder.addCriterion(TariffPlanMapCustomTableEnum.ID.getLabel(), " != ", tariffPlanMapId, false, false);
        }
        queryBuilder.addCriterion(TariffPlanMapCustomTableEnum.CUSTOMER_ID.getLabel(), "=", tariffPlanMapDto.getValue().getValues().get(TariffPlanMapCustomTableEnum.CUSTOMER_ID.getLabel()), false);
        queryBuilder.addCriterion(TariffPlanMapCustomTableEnum.OFFER_ID.getLabel(), "=", tariffPlanMapDto.getValue().getValues().get(TariffPlanMapCustomTableEnum.OFFER_ID.getLabel()), false);

        Date startDate = DateUtils.parseDate(tariffPlanMapDto.getValue().getValues().get(TariffPlanMapCustomTableEnum.VALID_FROM.getLabel()));
        Date endDate = DateUtils.parseDate(tariffPlanMapDto.getValue().getValues().get(TariffPlanMapCustomTableEnum.VALID_TO.getLabel()));

        utilService.addOverlapCriteria(queryBuilder, TariffPlanMapCustomTableEnum.VALID_FROM.getLabel(), TariffPlanMapCustomTableEnum.VALID_TO.getLabel(),
                startDate, endDate);

        Query query = queryBuilder.getNativeQuery(getEntityManager(), true);
        List<Map<String, Object>> tariffPlanMaps = query.list();
        if (!tariffPlanMaps.isEmpty()) {
            throw new BusinessException("The provided tariff plan map is overlap with an existing one");
        }

        tariffPlanMapDto.getValue().getValues().put(TariffPlanMapCustomTableEnum.VALID_FROM.getLabel(), startDate);
        tariffPlanMapDto.getValue().getValues().put(TariffPlanMapCustomTableEnum.VALID_TO.getLabel(), endDate);

        if (tariffPlanMapId == null) {
            customTableApi.create(tariffPlanMapDto);
        } else {
            customTableApi.update(tariffPlanMapDto);
        }
    }
}

