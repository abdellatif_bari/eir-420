/*
 * (C) Copyright 2015-2016 Opencell SAS (http://opencellsoft.com/) and contributors.
 * (C) Copyright 2009-2014 Manaty SARL (http://manaty.net/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * This program is not suitable for any direct or indirect application in MILITARY industry
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eir.commons.enums;

import com.eir.commons.enums.customtables.CustomTableEnum;
import com.eir.commons.enums.customtables.StageCDRCustomTableEnum;
import com.eir.commons.enums.customtables.StageTariffCustomTableEnum;
import com.eir.commons.enums.customtables.ValidCDRCustomTableEnum;
import com.eir.commons.enums.customtables.ValidTariffCustomTableEnum;
import com.eir.commons.utils.TransformationRule;
import com.eir.commons.utils.TransformationRulesHandler;
import org.meveo.commons.utils.StringUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * EIR Transformation rules enum
 *
 * @author Abdellatif BARI
 */
public enum TransformationRulesEnum {

    ACCESS {
        @Override
        public TransformationRulesHandler getInstance() {
            List<TransformationRule> transformationRules = new ArrayList<TransformationRule>();
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.RECORD_ACTION.getLabel(), String.class, null, null, null, null, null, null, null, true, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.TARIFF_PLAN.getLabel(), String.class, ValidTariffCustomTableEnum.TARIFF_PLAN_ID.getLabel(), Long.class, "eir_or_tariff_plan", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.OFFER.getLabel(), String.class, ValidTariffCustomTableEnum.OFFER_ID.getLabel(), Long.class, "cat_offer_template", ReferenceTableTypeEnum.TABLE, "code", "id", "(select LOWER(o.code) from cat_offer_template o inner join cat_product_offer_tmpl_cat ass on ass.product_id = o.id inner join cat_offer_template_category cat on (cat.id = ass.offer_template_cat_id and LOWER(cat.cf_values\\:\\:json->'offerType'->0->>'string') = 'access') where LOWER(o.code) = LOWER(offer))", true, true, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.SERVICE.getLabel(), String.class, ValidTariffCustomTableEnum.SERVICE_ID.getLabel(), Long.class, "cat_service_template", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.CHARGE_TYPE.getLabel(), String.class, ValidTariffCustomTableEnum.CHARGE_TYPE.getLabel(), String.class, "", null, "", "", null, true, false, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.REC_CALENDAR.getLabel(), String.class, ValidTariffCustomTableEnum.REC_CALENDAR.getLabel(), Long.class, "cat_calendar", ReferenceTableTypeEnum.TABLE, "code", "id", null, false, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.DESCRIPTION.getLabel(), String.class, ValidTariffCustomTableEnum.DESCRIPTION.getLabel(), String.class, "", null, "", "", null, false, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.ACCOUNTING_CODE.getLabel(), String.class, ValidTariffCustomTableEnum.ACCOUNTING_CODE_ID.getLabel(), Long.class, "billing_accounting_code", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.INVOICE_SUBCAT.getLabel(), String.class, ValidTariffCustomTableEnum.INVOICE_SUBCAT_ID.getLabel(), Long.class, "billing_invoice_sub_cat", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.TAX_CLASS.getLabel(), String.class, ValidTariffCustomTableEnum.TAX_CLASS.getLabel(), Long.class, "billing_tax_class", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.ACTION_QUALIFIER.getLabel(), String.class, ValidTariffCustomTableEnum.ACTION_QUALIFIER.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.DENSITY.getLabel(), String.class, ValidTariffCustomTableEnum.DENSITY.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.REGION.getLabel(), String.class, ValidTariffCustomTableEnum.REGION.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.SLA.getLabel(), String.class, ValidTariffCustomTableEnum.SLA.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.CLASS_OF_SERVICE.getLabel(), String.class, ValidTariffCustomTableEnum.CLASS_OF_SERVICE.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.EF.getLabel(), String.class, ValidTariffCustomTableEnum.EF.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.AF.getLabel(), String.class, ValidTariffCustomTableEnum.AF.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.EXCHANGE_CLASS.getLabel(), String.class, ValidTariffCustomTableEnum.EXCHANGE_CLASS.getLabel(), String.class, "Exchange_Class", ReferenceTableTypeEnum.CUSTOM_ENTITY, "code", "code", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.BANDWIDTH.getLabel(), String.class, ValidTariffCustomTableEnum.BANDWIDTH.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.ORDER_TYPE.getLabel(), String.class, ValidTariffCustomTableEnum.ORDER_TYPE.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.EGRESS_GROUP.getLabel(), String.class, ValidTariffCustomTableEnum.EGRESS_GROUP.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.PRICE.getLabel(), Double.class, ValidTariffCustomTableEnum.PRICE.getLabel(), BigDecimal.class, "", null, "", "", null, true, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.VALID_FROM.getLabel(), Date.class, ValidTariffCustomTableEnum.VALID_FROM.getLabel(), Date.class, "", null, "", "", null, true, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.VALID_TO.getLabel(), Date.class, ValidTariffCustomTableEnum.VALID_TO.getLabel(), Date.class, "", null, "", "", null, false, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.FILE_ID.getLabel(), Long.class, ValidTariffCustomTableEnum.FILE_NAME.getLabel(), String.class, "flat_file", ReferenceTableTypeEnum.TABLE, "id", "file_original_name", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.FILE_ID.getLabel(), Long.class, ValidTariffCustomTableEnum.UPDATED.getLabel(), Date.class, "flat_file", ReferenceTableTypeEnum.TABLE, "id", "updated", null, false, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.FILE_ID.getLabel(), Long.class, ValidTariffCustomTableEnum.UPDATED_BY.getLabel(), String.class, "flat_file", ReferenceTableTypeEnum.TABLE, "id", "updater", null, false, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.HASH_CODE.getLabel(), String.class, ValidTariffCustomTableEnum.HASH_CODE.getLabel(), String.class, "", null, "", "", null, false, false, false, false));

            TransformationRulesHandler transformationRulesHandler = new TransformationRulesHandler(CustomTableEnum.EIR_OR_TARIFF_STAGE.getCode(), CustomTableEnum.EIR_OR_TARIFF.getCode(), ValidTariffCustomTableEnum.VALID_FROM.getLabel(), ValidTariffCustomTableEnum.VALID_TO.getLabel(), StageTariffCustomTableEnum.ERROR_REASON.getLabel(), true, transformationRules);
            return transformationRulesHandler;
        }
    },
    DATA {
        @Override
        public TransformationRulesHandler getInstance() {
            List<TransformationRule> transformationRules = new ArrayList<TransformationRule>();
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.RECORD_ACTION.getLabel(), String.class, null, null, null, null, null, null, null, true, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.TARIFF_PLAN.getLabel(), String.class, ValidTariffCustomTableEnum.TARIFF_PLAN_ID.getLabel(), Long.class, "eir_or_tariff_plan", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.OFFER.getLabel(), String.class, ValidTariffCustomTableEnum.OFFER_ID.getLabel(), Long.class, "cat_offer_template", ReferenceTableTypeEnum.TABLE, "code", "id", "(select LOWER(o.code) from cat_offer_template o inner join cat_product_offer_tmpl_cat ass on ass.product_id = o.id inner join cat_offer_template_category cat on (cat.id = ass.offer_template_cat_id and LOWER(cat.cf_values\\:\\:json->'offerType'->0->>'string') = 'data') where LOWER(o.code) = LOWER(offer))", true, true, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.SERVICE.getLabel(), String.class, ValidTariffCustomTableEnum.SERVICE_ID.getLabel(), Long.class, "cat_service_template", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.CHARGE_TYPE.getLabel(), String.class, ValidTariffCustomTableEnum.CHARGE_TYPE.getLabel(), String.class, "", null, "", "", null, true, false, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.REC_CALENDAR.getLabel(), String.class, ValidTariffCustomTableEnum.REC_CALENDAR.getLabel(), Long.class, "cat_calendar", ReferenceTableTypeEnum.TABLE, "code", "id", null, false, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.DESCRIPTION.getLabel(), String.class, ValidTariffCustomTableEnum.DESCRIPTION.getLabel(), String.class, "", null, "", "", null, false, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.ACCOUNTING_CODE.getLabel(), String.class, ValidTariffCustomTableEnum.ACCOUNTING_CODE_ID.getLabel(), Long.class, "billing_accounting_code", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.INVOICE_SUBCAT.getLabel(), String.class, ValidTariffCustomTableEnum.INVOICE_SUBCAT_ID.getLabel(), Long.class, "billing_invoice_sub_cat", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.TAX_CLASS.getLabel(), String.class, ValidTariffCustomTableEnum.TAX_CLASS.getLabel(), Long.class, "billing_tax_class", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.ACTION_QUALIFIER.getLabel(), String.class, ValidTariffCustomTableEnum.ACTION_QUALIFIER.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.PRICE_PLAN.getLabel(), String.class, ValidTariffCustomTableEnum.PRICE_PLAN.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.RATING_SCHEME.getLabel(), String.class, ValidTariffCustomTableEnum.RATING_SCHEME.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.BANDWIDTH.getLabel(), String.class, ValidTariffCustomTableEnum.BANDWIDTH.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.TRANSMISSION.getLabel(), String.class, ValidTariffCustomTableEnum.TRANSMISSION.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.COMMITMENT_PERIOD.getLabel(), String.class, ValidTariffCustomTableEnum.COMMITMENT_PERIOD.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.ZONE_A.getLabel(), String.class, ValidTariffCustomTableEnum.ZONE_A.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.ZONE_B.getLabel(), String.class, ValidTariffCustomTableEnum.ZONE_B.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.REGION.getLabel(), String.class, ValidTariffCustomTableEnum.REGION.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.SLA.getLabel(), String.class, ValidTariffCustomTableEnum.SLA.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.SERVICE_CATEGORY.getLabel(), String.class, ValidTariffCustomTableEnum.SERVICE_CATEGORY.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.ANALOGUE_QUALITY.getLabel(), String.class, ValidTariffCustomTableEnum.ANALOGUE_QUALITY.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.ANALOGUE_RATE.getLabel(), String.class, ValidTariffCustomTableEnum.ANALOGUE_RATE.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.CPE_TYPE.getLabel(), String.class, ValidTariffCustomTableEnum.CPE_TYPE.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.CPE_DESCRIPTION.getLabel(), String.class, ValidTariffCustomTableEnum.CPE_DESCRIPTION.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.ETS_TYPE.getLabel(), String.class, ValidTariffCustomTableEnum.ETS_TYPE.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.IND_01.getLabel(), String.class, ValidTariffCustomTableEnum.IND_01.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.IND_02.getLabel(), String.class, ValidTariffCustomTableEnum.IND_02.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.IND_03.getLabel(), String.class, ValidTariffCustomTableEnum.IND_03.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.IND_04.getLabel(), String.class, ValidTariffCustomTableEnum.IND_04.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.IND_05.getLabel(), String.class, ValidTariffCustomTableEnum.IND_05.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.IND_06.getLabel(), String.class, ValidTariffCustomTableEnum.IND_06.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.IND_07.getLabel(), String.class, ValidTariffCustomTableEnum.IND_07.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.IND_08.getLabel(), String.class, ValidTariffCustomTableEnum.IND_08.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.IND_09.getLabel(), String.class, ValidTariffCustomTableEnum.IND_09.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.IND_10.getLabel(), String.class, ValidTariffCustomTableEnum.IND_10.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.DISTANCE_FROM.getLabel(), Long.class, ValidTariffCustomTableEnum.DISTANCE_FROM.getLabel(), Long.class, "", null, "", "", null, false, false, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.DISTANCE_TO.getLabel(), Long.class, ValidTariffCustomTableEnum.DISTANCE_TO.getLabel(), Long.class, "", null, "", "", null, false, false, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.DISTANCE_UNIT.getLabel(), Long.class, ValidTariffCustomTableEnum.DISTANCE_UNIT.getLabel(), Long.class, "", null, "", "", null, false, false, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.PRICE.getLabel(), Double.class, ValidTariffCustomTableEnum.PRICE.getLabel(), BigDecimal.class, "", null, "", "", null, true, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.VALID_FROM.getLabel(), Date.class, ValidTariffCustomTableEnum.VALID_FROM.getLabel(), Date.class, "", null, "", "", null, true, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.VALID_TO.getLabel(), Date.class, ValidTariffCustomTableEnum.VALID_TO.getLabel(), Date.class, "", null, "", "", null, false, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.FILE_ID.getLabel(), Long.class, ValidTariffCustomTableEnum.FILE_NAME.getLabel(), String.class, "flat_file", ReferenceTableTypeEnum.TABLE, "id", "file_original_name", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.FILE_ID.getLabel(), Long.class, ValidTariffCustomTableEnum.UPDATED.getLabel(), Date.class, "flat_file", ReferenceTableTypeEnum.TABLE, "id", "updated", null, false, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.FILE_ID.getLabel(), Long.class, ValidTariffCustomTableEnum.UPDATED_BY.getLabel(), String.class, "flat_file", ReferenceTableTypeEnum.TABLE, "id", "updater", null, false, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.HASH_CODE.getLabel(), String.class, ValidTariffCustomTableEnum.HASH_CODE.getLabel(), String.class, "", null, "", "", null, false, false, false, false));

            TransformationRulesHandler transformationRulesHandler = new TransformationRulesHandler(CustomTableEnum.EIR_OR_TARIFF_STAGE.getCode(), CustomTableEnum.EIR_OR_TARIFF.getCode(), ValidTariffCustomTableEnum.VALID_FROM.getLabel(), ValidTariffCustomTableEnum.VALID_TO.getLabel(), StageTariffCustomTableEnum.ERROR_REASON.getLabel(), true, transformationRules);
            return transformationRulesHandler;
        }
    },
    MUB {
        @Override
        public TransformationRulesHandler getInstance() {
            List<TransformationRule> transformationRules = new ArrayList<TransformationRule>();
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.RECORD_ACTION.getLabel(), String.class, null, null, null, null, null, null, null, true, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.TARIFF_PLAN.getLabel(), String.class, ValidTariffCustomTableEnum.TARIFF_PLAN_ID.getLabel(), Long.class, "eir_or_tariff_plan", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.OFFER.getLabel(), String.class, ValidTariffCustomTableEnum.OFFER_ID.getLabel(), Long.class, "cat_offer_template", ReferenceTableTypeEnum.TABLE, "code", "id", "(select LOWER(o.code) from cat_offer_template o inner join cat_product_offer_tmpl_cat ass on ass.product_id = o.id inner join cat_offer_template_category cat on (cat.id = ass.offer_template_cat_id and LOWER(cat.cf_values\\:\\:json->'offerType'->0->>'string') = 'mub') where LOWER(o.code) = LOWER(offer))", true, true, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.SERVICE.getLabel(), String.class, ValidTariffCustomTableEnum.SERVICE_ID.getLabel(), Long.class, "cat_service_template", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.CHARGE_TYPE.getLabel(), String.class, ValidTariffCustomTableEnum.CHARGE_TYPE.getLabel(), String.class, "", null, "", "", null, true, false, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.REC_CALENDAR.getLabel(), String.class, ValidTariffCustomTableEnum.REC_CALENDAR.getLabel(), Long.class, "cat_calendar", ReferenceTableTypeEnum.TABLE, "code", "id", null, false, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.DESCRIPTION.getLabel(), String.class, ValidTariffCustomTableEnum.DESCRIPTION.getLabel(), String.class, "", null, "", "", null, false, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.ACCOUNTING_CODE.getLabel(), String.class, ValidTariffCustomTableEnum.ACCOUNTING_CODE_ID.getLabel(), Long.class, "billing_accounting_code", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.INVOICE_SUBCAT.getLabel(), String.class, ValidTariffCustomTableEnum.INVOICE_SUBCAT_ID.getLabel(), Long.class, "billing_invoice_sub_cat", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.TAX_CLASS.getLabel(), String.class, ValidTariffCustomTableEnum.TAX_CLASS.getLabel(), Long.class, "billing_tax_class", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.AVERAGE_SPEED.getLabel(), String.class, ValidTariffCustomTableEnum.AVERAGE_SPEED.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.PRICE.getLabel(), Double.class, ValidTariffCustomTableEnum.PRICE.getLabel(), BigDecimal.class, "", null, "", "", null, true, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.VALID_FROM.getLabel(), Date.class, ValidTariffCustomTableEnum.VALID_FROM.getLabel(), Date.class, "", null, "", "", null, true, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.VALID_TO.getLabel(), Date.class, ValidTariffCustomTableEnum.VALID_TO.getLabel(), Date.class, "", null, "", "", null, false, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.FILE_ID.getLabel(), Long.class, ValidTariffCustomTableEnum.FILE_NAME.getLabel(), String.class, "flat_file", ReferenceTableTypeEnum.TABLE, "id", "file_original_name", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.FILE_ID.getLabel(), Long.class, ValidTariffCustomTableEnum.UPDATED.getLabel(), Date.class, "flat_file", ReferenceTableTypeEnum.TABLE, "id", "updated", null, false, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.FILE_ID.getLabel(), Long.class, ValidTariffCustomTableEnum.UPDATED_BY.getLabel(), String.class, "flat_file", ReferenceTableTypeEnum.TABLE, "id", "updater", null, false, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.HASH_CODE.getLabel(), String.class, ValidTariffCustomTableEnum.HASH_CODE.getLabel(), String.class, "", null, "", "", null, false, false, false, false));

            TransformationRulesHandler transformationRulesHandler = new TransformationRulesHandler(CustomTableEnum.EIR_OR_TARIFF_STAGE.getCode(), CustomTableEnum.EIR_OR_TARIFF.getCode(), ValidTariffCustomTableEnum.VALID_FROM.getLabel(), ValidTariffCustomTableEnum.VALID_TO.getLabel(), StageTariffCustomTableEnum.ERROR_REASON.getLabel(), true, transformationRules);
            return transformationRulesHandler;
        }
    },
    MISC {
        @Override
        public TransformationRulesHandler getInstance() {
            List<TransformationRule> transformationRules = new ArrayList<TransformationRule>();
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.RECORD_ACTION.getLabel(), String.class, null, null, null, null, null, null, null, true, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.TARIFF_PLAN.getLabel(), String.class, ValidTariffCustomTableEnum.TARIFF_PLAN_ID.getLabel(), Long.class, "eir_or_tariff_plan", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.OFFER.getLabel(), String.class, ValidTariffCustomTableEnum.OFFER_ID.getLabel(), Long.class, "cat_offer_template", ReferenceTableTypeEnum.TABLE, "code", "id", "(select LOWER(o.code) from cat_offer_template o inner join cat_product_offer_tmpl_cat ass on ass.product_id = o.id inner join cat_offer_template_category cat on (cat.id = ass.offer_template_cat_id and LOWER(cat.cf_values\\:\\:json->'offerType'->0->>'string') = 'misc') where LOWER(o.code) = LOWER(offer))", true, true, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.SERVICE.getLabel(), String.class, ValidTariffCustomTableEnum.SERVICE_ID.getLabel(), Long.class, "cat_service_template", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.CHARGE_TYPE.getLabel(), String.class, ValidTariffCustomTableEnum.CHARGE_TYPE.getLabel(), String.class, "", null, "", "", null, true, false, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.REC_CALENDAR.getLabel(), String.class, ValidTariffCustomTableEnum.REC_CALENDAR.getLabel(), Long.class, "cat_calendar", ReferenceTableTypeEnum.TABLE, "code", "id", null, false, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.DESCRIPTION.getLabel(), String.class, ValidTariffCustomTableEnum.DESCRIPTION.getLabel(), String.class, "", null, "", "", null, false, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.ACCOUNTING_CODE.getLabel(), String.class, ValidTariffCustomTableEnum.ACCOUNTING_CODE_ID.getLabel(), Long.class, "billing_accounting_code", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.INVOICE_SUBCAT.getLabel(), String.class, ValidTariffCustomTableEnum.INVOICE_SUBCAT_ID.getLabel(), Long.class, "billing_invoice_sub_cat", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.TAX_CLASS.getLabel(), String.class, ValidTariffCustomTableEnum.TAX_CLASS.getLabel(), Long.class, "billing_tax_class", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.ACTION_QUALIFIER.getLabel(), String.class, ValidTariffCustomTableEnum.ACTION_QUALIFIER.getLabel(), String.class, "", null, "", "", null, false, false, true, true));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.QUANTITY_FROM.getLabel(), Long.class, ValidTariffCustomTableEnum.QUANTITY_FROM.getLabel(), Long.class, "", null, "", "", null, false, false, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.QUANTITY_TO.getLabel(), Long.class, ValidTariffCustomTableEnum.QUANTITY_TO.getLabel(), Long.class, "", null, "", "", null, false, false, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.MAX_RATED_QUANTITY.getLabel(), Long.class, ValidTariffCustomTableEnum.MAX_RATED_QUANTITY.getLabel(), Long.class, "", null, "", "", null, false, false, true, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.PRICE.getLabel(), Double.class, ValidTariffCustomTableEnum.PRICE.getLabel(), BigDecimal.class, "", null, "", "", null, true, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.VALID_FROM.getLabel(), Date.class, ValidTariffCustomTableEnum.VALID_FROM.getLabel(), Date.class, "", null, "", "", null, true, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.VALID_TO.getLabel(), Date.class, ValidTariffCustomTableEnum.VALID_TO.getLabel(), Date.class, "", null, "", "", null, false, false, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.FILE_ID.getLabel(), Long.class, ValidTariffCustomTableEnum.FILE_NAME.getLabel(), String.class, "flat_file", ReferenceTableTypeEnum.TABLE, "id", "file_original_name", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.FILE_ID.getLabel(), Long.class, ValidTariffCustomTableEnum.UPDATED.getLabel(), Date.class, "flat_file", ReferenceTableTypeEnum.TABLE, "id", "updated", null, false, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.FILE_ID.getLabel(), Long.class, ValidTariffCustomTableEnum.UPDATED_BY.getLabel(), String.class, "flat_file", ReferenceTableTypeEnum.TABLE, "id", "updater", null, false, true, false, false));
            transformationRules.add(new TransformationRule(StageTariffCustomTableEnum.HASH_CODE.getLabel(), String.class, ValidTariffCustomTableEnum.HASH_CODE.getLabel(), String.class, "", null, "", "", null, false, false, false, false));

            TransformationRulesHandler transformationRulesHandler = new TransformationRulesHandler(CustomTableEnum.EIR_OR_TARIFF_STAGE.getCode(), CustomTableEnum.EIR_OR_TARIFF.getCode(), ValidTariffCustomTableEnum.VALID_FROM.getLabel(), ValidTariffCustomTableEnum.VALID_TO.getLabel(), StageTariffCustomTableEnum.ERROR_REASON.getLabel(), true, transformationRules);
            return transformationRulesHandler;
        }
    },
    CDR {
        @Override
        public TransformationRulesHandler getInstance() {
            List<TransformationRule> transformationRules = new ArrayList<TransformationRule>();
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.RECORD_ACTION.getLabel(), String.class, null, null, null, null, null, null, null, true, false, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.RATE_PLAN.getLabel(), String.class, ValidCDRCustomTableEnum.RATE_PLAN_ID.getLabel(), Long.class, "EIR_U_TARIFF_PLAN", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, true, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.TRAFFIC_CLASS.getLabel(), String.class, ValidCDRCustomTableEnum.TRAFFIC_CLASS_ID.getLabel(), Long.class, "EIR_U_TRAFFIC_CLASS", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, true, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.DESCRIPTION.getLabel(), String.class, ValidCDRCustomTableEnum.DESCRIPTION.getLabel(), String.class, "", null, "", "", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.MIN_CHARGE.getLabel(), Double.class, ValidCDRCustomTableEnum.MIN_CHARGE.getLabel(), BigDecimal.class, "", null, "", "", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.CONNECTION_CHARGE.getLabel(), Double.class, ValidCDRCustomTableEnum.CONNECTION_CHARGE.getLabel(), BigDecimal.class, "", null, "", "", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.TIME_PERIOD.getLabel(), String.class, ValidCDRCustomTableEnum.TIME_PERIOD_ID.getLabel(), Long.class, "EIR_U_TIME_PERIOD", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, true, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.USAGE_CHARGE.getLabel(), Double.class, ValidCDRCustomTableEnum.USAGE_CHARGE.getLabel(), BigDecimal.class, "", null, "", "", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.UNIT.getLabel(), Long.class, ValidCDRCustomTableEnum.UNIT.getLabel(), Long.class, "", null, "", "", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.DURATION_ROUNDING.getLabel(), Long.class, ValidCDRCustomTableEnum.DURATION_ROUNDING.getLabel(), Long.class, "", null, "", "", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.MIN_DURATION.getLabel(), Long.class, ValidCDRCustomTableEnum.MIN_DURATION.getLabel(), Long.class, "", null, "", "", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.ACCOUNTING_CODE.getLabel(), String.class, ValidCDRCustomTableEnum.ACCOUNTING_CODE_ID.getLabel(), Long.class, "billing_accounting_code", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.INVOICE_SUBCAT.getLabel(), String.class, ValidCDRCustomTableEnum.INVOICE_SUBCAT_ID.getLabel(), Long.class, "billing_invoice_sub_cat", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.TAX_CLASS.getLabel(), String.class, ValidCDRCustomTableEnum.TAX_CLASS.getLabel(), Long.class, "billing_tax_class", ReferenceTableTypeEnum.TABLE, "code", "id", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.VALID_FROM.getLabel(), Date.class, ValidCDRCustomTableEnum.VALID_FROM.getLabel(), Date.class, "", null, "", "", null, true, true, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.VALID_TO.getLabel(), Date.class, ValidCDRCustomTableEnum.VALID_TO.getLabel(), Date.class, "", null, "", "", null, false, false, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.FILE_ID.getLabel(), Long.class, ValidCDRCustomTableEnum.FILE_NAME.getLabel(), String.class, "flat_file", ReferenceTableTypeEnum.TABLE, "id", "file_original_name", null, false, true, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.FILE_ID.getLabel(), Long.class, ValidCDRCustomTableEnum.UPDATED.getLabel(), Date.class, "flat_file", ReferenceTableTypeEnum.TABLE, "id", "updated", null, false, true, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.FILE_ID.getLabel(), Long.class, ValidCDRCustomTableEnum.UPDATED_BY.getLabel(), String.class, "flat_file", ReferenceTableTypeEnum.TABLE, "id", "updater", null, false, true, false, false));
            transformationRules.add(new TransformationRule(StageCDRCustomTableEnum.HASH_CODE.getLabel(), String.class, ValidCDRCustomTableEnum.HASH_CODE.getLabel(), String.class, "", null, "", "", null, false, false, false, false));

            TransformationRulesHandler transformationRulesHandler = new TransformationRulesHandler(CustomTableEnum.EIR_U_TARIFF_STAGE.getCode(), CustomTableEnum.EIR_U_TARIFF.getCode(), ValidCDRCustomTableEnum.VALID_FROM.getLabel(), ValidCDRCustomTableEnum.VALID_TO.getLabel(), StageCDRCustomTableEnum.ERROR_REASON.getLabel(), false, transformationRules);
            return transformationRulesHandler;
        }
    };

    private static Set<String> _names = new HashSet<>();

    static {
        for (TransformationRulesEnum choice : TransformationRulesEnum.values()) {
            _names.add(choice.name());
        }
    }

    public abstract TransformationRulesHandler getInstance();

    public static TransformationRulesEnum valueOfIgnoreCase(String name) {
        if (!StringUtils.isBlank(name)) {
            name = name.toUpperCase();
        }
        return valueOf(name);
    }

    public static boolean contains(String value) {
        return !StringUtils.isBlank(value) && _names.contains(value.toUpperCase());
    }
}
