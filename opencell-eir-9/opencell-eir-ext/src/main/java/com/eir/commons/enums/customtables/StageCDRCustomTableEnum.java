/*
 * (C) Copyright 2015-2016 Opencell SAS (http://opencellsoft.com/) and contributors.
 * (C) Copyright 2009-2014 Manaty SARL (http://manaty.net/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * This program is not suitable for any direct or indirect application in MILITARY industry
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eir.commons.enums.customtables;

/**
 * EIR stage CDR custom table enum
 *
 * @author Abdellatif BARI
 */
public enum StageCDRCustomTableEnum {

    ID("id"),
    RECORD_ACTION("record_action"),
    RATE_PLAN("rate_plan"),
    TRAFFIC_CLASS("traffic_class"),
    DESCRIPTION("description"),
    MIN_CHARGE("min_charge"),
    CONNECTION_CHARGE("connection_charge"),
    TIME_PERIOD("time_period"),
    USAGE_CHARGE("usage_charge"),
    UNIT("unit"),
    DURATION_ROUNDING("duration_rounding"),
    MIN_DURATION("min_duration"),
    VALID_FROM("valid_from"),
    VALID_TO("valid_to"),
    TAX_CLASS("tax_class"),
    INVOICE_SUBCAT("invoice_subcat"),
    ACCOUNTING_CODE("accounting_code"),
    FILE_ID("file_id"),
    ERROR_REASON("error_reason"),

    /**
     * Technical fields
     */
    HASH_CODE("hash_code");

    private String label;

    StageCDRCustomTableEnum(String label) {
        this.label = label;
    }

    public static String getCustomTableCode() {
        return "EIR_U_TARIFF_STAGE";
    }

    public String getLabel() {
        return this.label;
    }
}
