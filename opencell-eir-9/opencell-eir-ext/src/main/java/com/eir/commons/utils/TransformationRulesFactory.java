package com.eir.commons.utils;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.inject.Inject;

import org.apache.commons.collections.map.HashedMap;
import org.apache.commons.lang3.StringUtils;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.customEntities.CustomEntityTemplate;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.crm.impl.CustomFieldTemplateService;
import org.meveo.service.custom.CustomTableService;

import com.eir.commons.enums.TransformationRulesEnum;
import com.eir.service.rating.CdrService;
import com.eir.service.tariff.TariffService;

/**
 * EIR Transformation rules factory
 *
 * @author Abdellatif BARI
 */
@Stateless
public class TransformationRulesFactory {

    @Inject
    private TariffService tariffService;
    @Inject
    private CdrService cdrService;
    @Inject
    private CustomFieldTemplateService customFieldTemplateService;
    @Inject
    private CustomTableService customTableService;

    private static Map<TransformationRulesEnum, TransformationRulesHandler> transformationRulesHandlers = new HashMap<>();

    /**
     * Get transformation rules handler instance
     *
     * @param action the action
     * @return transformation rules handler instance
     */
    public TransformationRulesHandler getInstance(TransformationRulesEnum action) {
        TransformationRulesHandler transformationRulesHandler = null;
        if (action != null) {
            transformationRulesHandler = transformationRulesHandlers.get(action);
            if (transformationRulesHandler != null) {
                return transformationRulesHandler;
            }
            transformationRulesHandler = action.getInstance();
            if (transformationRulesHandler != null) {
                setTransformationRulesFunctions(transformationRulesHandler, action);
                transformationRulesHandlers.put(action, transformationRulesHandler);
            }
        }
        return transformationRulesHandler;
    }

    /**
     * Set transformation rules functions
     *
     * @param handler the transformation rules handler
     * @param action  the action
     */
    private void setTransformationRulesFunctions(TransformationRulesHandler handler, TransformationRulesEnum action) {
        switch (action) {
            case ACCESS:
            case DATA:
            case MUB:
            case MISC:
                handler.setValidateFunction(tariffService::validate);
                handler.setActivateFunction(tariffService::activate);
                handler.setRerateFunction(tariffService::rerate);
                handler.setStageTableFields(getFields(handler.getStageTableName()));
                handler.setValidTableFields(getFields(handler.getValidTableName()));
                break;
            case CDR:
                handler.setValidateFunction(cdrService::validate);
                handler.setActivateFunction(cdrService::activate);
                handler.setRerateFunction(cdrService::rerate);
                handler.setStageTableFields(getFields(handler.getStageTableName()));
                handler.setValidTableFields(getFields(handler.getValidTableName()));
                break;
        }
    }

    /**
     * List all fields with their default values of tableName
     *
     * @param tableName the table name
     * @return
     */
    private Map<String, Object> getFields(String tableName) {
        tableName = customTableService.addCurrentSchema(tableName);
        Map<String, Object> fields = new HashedMap();
        Map<String, CustomFieldTemplate> customFieldTemplateMap = customFieldTemplateService.findByAppliesTo(CustomEntityTemplate.CFT_PREFIX + "_" + tableName);
        for (String key : customFieldTemplateMap.keySet()) {
            CustomFieldTemplate cft = customFieldTemplateMap.get(key);
            Class clazz = cft.getFieldType().getDataClass();
            String defaultValueString = cft.getDefaultValue();
            if (StringUtils.isBlank(defaultValueString)) {
                fields.put(cft.getDbFieldname(), defaultValueString);
                continue;
            }
            Object defaultValue = defaultValueString;

            if (Long.class.equals(clazz)) {
                defaultValue = Long.valueOf(defaultValueString);
            } else if (Double.class.equals(clazz)) {
                defaultValue = Double.valueOf(defaultValueString);
            } else if (BigInteger.class.equals(clazz)) {
                defaultValue = new BigInteger(defaultValueString);
            } else if (Integer.class.equals(clazz)) {
                defaultValue = Integer.valueOf(defaultValueString);
            } else if (BigDecimal.class.equals(clazz)) {
                defaultValue = new BigDecimal(defaultValueString);
            } else if (Date.class.equals(clazz)) {
                defaultValue = DateUtils.parseDateWithPattern(defaultValueString, DateUtils.DATE_TIME_PATTERN);
            } else if (Boolean.class.equals(clazz)) {
                defaultValue = Boolean.valueOf(defaultValueString);
            }
            fields.put(cft.getDbFieldname(), defaultValue);
        }
        return fields;
    }
}
