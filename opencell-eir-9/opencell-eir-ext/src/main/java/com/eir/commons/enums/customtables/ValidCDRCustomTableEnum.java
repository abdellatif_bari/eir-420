/*
 * (C) Copyright 2015-2016 Opencell SAS (http://opencellsoft.com/) and contributors.
 * (C) Copyright 2009-2014 Manaty SARL (http://manaty.net/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * This program is not suitable for any direct or indirect application in MILITARY industry
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eir.commons.enums.customtables;

/**
 * EIR valid CDR custom table enum
 *
 * @author Abdellatif BARI
 */
public enum ValidCDRCustomTableEnum {

    ID("id"),
    RATE_PLAN_ID("rate_plan_id"),
    TRAFFIC_CLASS_ID("traffic_class_id"),
    DESCRIPTION("description"),
    MIN_CHARGE("min_charge"),
    CONNECTION_CHARGE("connection_charge"),
    TIME_PERIOD_ID("time_period_id"),
    USAGE_CHARGE("usage_charge"),
    UNIT("unit"),
    DURATION_ROUNDING("duration_rounding"),
    MIN_DURATION("min_duration"),
    VALID_FROM("valid_from"),
    VALID_TO("valid_to"),
    TAX_CLASS("tax_class"),
    INVOICE_SUBCAT_ID("invoice_subcat_id"),
    ACCOUNTING_CODE_ID("accounting_code_id"),
    FILE_NAME("file_name"),
    UPDATED("updated"),
    UPDATED_BY("updated_by"),
    ORIGIN_ID("origin_id"),

    /**
     * Technical fields
     */
    HASH_CODE("hash_code");

    private String label;

    ValidCDRCustomTableEnum(String label) {
        this.label = label;
    }

    public static String getCustomTableCode() {
        return "EIR_U_TARIFF";
    }

    public String getLabel() {
        return this.label;
    }
}
