/*
 * (C) Copyright 2015-2016 Opencell SAS (http://opencellsoft.com/) and contributors.
 * (C) Copyright 2009-2014 Manaty SARL (http://manaty.net/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * This program is not suitable for any direct or indirect application in MILITARY industry
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eir.commons.enums.customtables;

/**
 * EIR Custom table enum
 *
 * @author Abdellatif BARI
 */
public enum CustomTableEnum {

    SERVICE_TYPE("SERVICE_TYPE"),
    PRODUCT_SET("Product_Set"),
    SUBSCRIBER_PREFIX("Subscriber_prefix"),
    END_POINT(EndPointCustomTableEnum.getCustomTableCode()),
    ASSOCIATION(AssociationCustomTableEnum.getCustomTableCode()),
    TARIFF_PLAN(TariffPlanCustomTableEnum.getCustomTableCode()),
    TARIFF_PLAN_MAP(TariffPlanMapCustomTableEnum.getCustomTableCode()),
    EIR_OR_TARIFF(ValidTariffCustomTableEnum.getCustomTableCode()),
    EIR_OR_TARIFF_STAGE(StageTariffCustomTableEnum.getCustomTableCode()),
    EIR_U_TARIFF(ValidCDRCustomTableEnum.getCustomTableCode()),
    EIR_U_TARIFF_STAGE(StageCDRCustomTableEnum.getCustomTableCode());


    private String code;

    CustomTableEnum(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}
