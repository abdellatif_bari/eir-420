package com.eir.commons.utils;

import java.util.List;
import java.util.Map;
import java.util.function.Function;

/**
 * EIR Transformation rules handler
 *
 * @author Abdellatif BARI
 */
public class TransformationRulesHandler {

    /**
     * The stage table name
     */
    private String stageTableName;

    /**
     * The stage table type
     */
    private String validTableName;

    /**
     * The valid from field name
     */
    private String validFromFieldName;

    /**
     * The valid to field name
     */
    private String validToFieldName;

    /**
     * The error reason field name
     */
    private String errorReasonFieldName;

    /**
     * Indicates if the matching during the search use the hash field
     */
    private boolean matchingWithHash;

    /**
     * The stage table Fields
     */
    private Map<String, Object> stageTableFields;

    /**
     * The valid table Fields
     */
    private Map<String, Object> validTableFields;

    /**
     * The transformation rules list
     */
    private List<TransformationRule> transformationRules;

    /**
     * The validate function
     */
    private Function<Object[], Object> validateFunction;

    /**
     * The activate function
     */
    private Function<Object[], Object> activateFunction;

    /**
     * The re-rate function
     */
    private Function<Object[], Object> rerateFunction;

    /**
     * Transformation rules handler constructor
     *
     * @param stageTableName       the stage table name
     * @param validTableName       the valid table name
     * @param validFromFieldName   the valid from field name
     * @param validToFieldName     the valid to field name
     * @param errorReasonFieldName the error reason field name
     * @param matchingWithHash     the matching with hash
     * @param transformationRules  the transformation rules list
     */
    public TransformationRulesHandler(String stageTableName, String validTableName, String validFromFieldName, String validToFieldName, String errorReasonFieldName,
                                      boolean matchingWithHash, List<TransformationRule> transformationRules) {
        this.stageTableName = stageTableName;
        this.validTableName = validTableName;
        this.validFromFieldName = validFromFieldName;
        this.validToFieldName = validToFieldName;
        this.errorReasonFieldName = errorReasonFieldName;
        this.matchingWithHash = matchingWithHash;
        this.transformationRules = transformationRules;
    }

    /**
     * Gets the stageTableName
     *
     * @return the stageTableName
     */
    public String getStageTableName() {
        return stageTableName;
    }

    /**
     * Sets the stageTableName.
     *
     * @param stageTableName the stageTableName
     */
    public void setStageTableName(String stageTableName) {
        this.stageTableName = stageTableName;
    }

    /**
     * Gets the validTableName
     *
     * @return the validTableName
     */
    public String getValidTableName() {
        return validTableName;
    }

    /**
     * Sets the validTableName.
     *
     * @param validTableName the validTableName
     */
    public void setValidTableName(String validTableName) {
        this.validTableName = validTableName;
    }

    /**
     * Gets the validFromFieldName
     *
     * @return the validFromFieldName
     */
    public String getValidFromFieldName() {
        return validFromFieldName;
    }

    /**
     * Sets the validFromFieldName.
     *
     * @param validFromFieldName the validFromFieldName
     */
    public void setValidFromFieldName(String validFromFieldName) {
        this.validFromFieldName = validFromFieldName;
    }

    /**
     * Gets the validToFieldName
     *
     * @return the validToFieldName
     */
    public String getValidToFieldName() {
        return validToFieldName;
    }

    /**
     * Sets the validToFieldName.
     *
     * @param validToFieldName the validToFieldName
     */
    public void setValidToFieldName(String validToFieldName) {
        this.validToFieldName = validToFieldName;
    }

    /**
     * Gets the errorReasonFieldName
     *
     * @return the errorReasonFieldName
     */
    public String getErrorReasonFieldName() {
        return errorReasonFieldName;
    }

    /**
     * Sets the errorReasonFieldName.
     *
     * @param errorReasonFieldName the errorReasonFieldName
     */
    public void setErrorReasonFieldName(String errorReasonFieldName) {
        this.errorReasonFieldName = errorReasonFieldName;
    }

    /**
     * Sets the matchingWithHash.
     *
     * @param matchingWithHash the matchingWithHash
     */
    public void setMatchingWithHash(boolean matchingWithHash) {
        this.matchingWithHash = matchingWithHash;
    }

    /**
     * Gets the matchingWithHash
     *
     * @return the matchingWithHash
     */
    public boolean isMatchingWithHash() {
        return matchingWithHash;
    }

    /**
     * Gets the transformationRules
     *
     * @return the transformationRules
     */
    public List<TransformationRule> getTransformationRules() {
        return transformationRules;
    }

    /**
     * Sets the transformationRules.
     *
     * @param transformationRules the transformationRules
     */
    public void setTransformationRules(List<TransformationRule> transformationRules) {
        this.transformationRules = transformationRules;
    }

    /**
     * Gets the stageTableFields
     *
     * @return the stageTableFields
     */
    public Map<String, Object> getStageTableFields() {
        return stageTableFields;
    }

    /**
     * Sets the stageTableFields.
     *
     * @param stageTableFields the stageTableFields
     */
    public void setStageTableFields(Map<String, Object> stageTableFields) {
        this.stageTableFields = stageTableFields;
    }

    /**
     * Gets the validTableFields
     *
     * @return the validTableFields
     */
    public Map<String, Object> getValidTableFields() {
        return validTableFields;
    }

    /**
     * Sets the validTableFields.
     *
     * @param validTableFields the validTableFields
     */
    public void setValidTableFields(Map<String, Object> validTableFields) {
        this.validTableFields = validTableFields;
    }

    /**
     * Gets the validateFunction
     *
     * @return the validateFunction
     */
    public Function<Object[], Object> getValidateFunction() {
        return validateFunction;
    }

    /**
     * Sets the validateFunction.
     *
     * @param validateFunction the validateFunction
     */
    public void setValidateFunction(Function<Object[], Object> validateFunction) {
        this.validateFunction = validateFunction;
    }

    /**
     * Gets the activateFunction
     *
     * @return the activateFunction
     */
    public Function<Object[], Object> getActivateFunction() {
        return activateFunction;
    }

    /**
     * Sets the activateFunction.
     *
     * @param activateFunction the activateFunction
     */
    public void setActivateFunction(Function<Object[], Object> activateFunction) {
        this.activateFunction = activateFunction;
    }

    /**
     * Gets the rerateFunction
     *
     * @return the rerateFunction
     */
    public Function<Object[], Object> getRerateFunction() {
        return rerateFunction;
    }

    /**
     * Sets the rerateFunction.
     *
     * @param rerateFunction the rerateFunction
     */
    public void setRerateFunction(Function<Object[], Object> rerateFunction) {
        this.rerateFunction = rerateFunction;
    }
}
