/*
 * (C) Copyright 2015-2016 Opencell SAS (http://opencellsoft.com/) and contributors.
 * (C) Copyright 2009-2014 Manaty SARL (http://manaty.net/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * This program is not suitable for any direct or indirect application in MILITARY industry
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eir.commons.enums;

import org.meveo.model.billing.ChargeInstance;
import org.meveo.model.billing.OneShotChargeInstance;
import org.meveo.model.billing.RecurringChargeInstance;
import org.meveo.model.billing.TerminationChargeInstance;
import org.meveo.model.billing.UsageChargeInstance;

/**
 * EIR Charge type enum
 *
 * @author Abdellatif BARI
 */
public enum ChargeTypeEnum {

    R("Recurring", false), O("OneShot", true), U("Usage", false), C("Cease", true);

    private String label;
    private boolean isOneShot;

    ChargeTypeEnum(String label, boolean isOneShot) {
        this.label = label;
        this.isOneShot = isOneShot;
    }

    public String getLabel() {
        return this.label;
    }

    /**
     * @return Is this a one shot charge
     */
    public boolean isOneShot() {
        return isOneShot;
    }

    public static ChargeTypeEnum getChargeType(ChargeInstance chargeInstance) {
        if (chargeInstance instanceof TerminationChargeInstance) {
            return C;
        } else if (chargeInstance instanceof OneShotChargeInstance) {
            return O;
        } else if (chargeInstance instanceof RecurringChargeInstance) {
            return R;
        } else if (chargeInstance instanceof UsageChargeInstance) {
            return U;
        }
        return null;
    }
}
