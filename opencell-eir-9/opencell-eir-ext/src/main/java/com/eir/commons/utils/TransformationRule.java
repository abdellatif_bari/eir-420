package com.eir.commons.utils;

import com.eir.commons.enums.ReferenceTableTypeEnum;

/**
 * EIR Transformation rule
 *
 * @author Abdellatif BARI
 */
public class TransformationRule {

    /**
     * The stage field name
     */
    private String stageFieldName;

    /**
     * The stage field type
     */
    private Class stageFieldType;

    /**
     * The valid field name
     */
    private String validFieldName;

    /**
     * The valid field type
     */
    private Class validFieldType;

    /**
     * The reference table
     */
    private String referenceTable;

    /**
     * The reference table type
     */
    private ReferenceTableTypeEnum referenceTableType;

    /**
     * The reference from field
     */
    private String referenceFromField;

    /**
     * The reference to field
     */
    private String referenceToField;

    /**
     * The additional criteria on reference table
     */
    private String referenceTableAdditionalCriteria;

    /**
     * Indicates is the stage field is mandatory or not
     */
    private boolean stageFieldMandatory;

    /**
     * Indicates is the valid field is mandatory or not
     */
    private boolean validFieldMandatory;

    /**
     * Indicates is the stage or valid field must be taken in account to find the tariff or not
     */
    private boolean matchedField;

    /**
     * Indicates is the field value can be matched or searched by wildcard : '*', '%', '<null>'
     */
    private boolean wildcardFieldValue;

    /**
     * Transformation Rule constructor
     *
     * @param stageFieldName                   the stage field name
     * @param stageFieldType                   the stage field type
     * @param validFieldName                   the valid field name
     * @param validFieldType                   the valid field type
     * @param referenceTable                   the reference table
     * @param referenceTableType               the reference table type
     * @param referenceFromField               the reference from field
     * @param referenceToField                 the reference to field
     * @param referenceTableAdditionalCriteria the additional criteria on reference table
     * @param stageFieldMandatory              Indicates if stage field is mandatory or not
     * @param validFieldMandatory              Indicates if valid field is mandatory or not
     * @param matchedField                     Indicates is the stage or valid field must be taken in account to find the tariff or not
     */
    public TransformationRule(String stageFieldName, Class stageFieldType, String validFieldName, Class validFieldType, String referenceTable,
                              ReferenceTableTypeEnum referenceTableType,
                              String referenceFromField,
                              String referenceToField,
                              String referenceTableAdditionalCriteria,
                              boolean stageFieldMandatory,
                              boolean validFieldMandatory,
                              boolean matchedField,
                              boolean wildcardFieldValue) {
        super();
        this.stageFieldName = stageFieldName;
        this.stageFieldType = stageFieldType;
        this.validFieldName = validFieldName;
        this.validFieldType = validFieldType;
        this.referenceTable = referenceTable;
        this.referenceTableType = referenceTableType;
        this.referenceFromField = referenceFromField;
        this.referenceToField = referenceToField;
        this.referenceTableAdditionalCriteria = referenceTableAdditionalCriteria;
        this.stageFieldMandatory = stageFieldMandatory;
        this.validFieldMandatory = validFieldMandatory;
        this.matchedField = matchedField;
        this.wildcardFieldValue = wildcardFieldValue;
    }

    /**
     * Gets the stageFieldName
     *
     * @return the stageFieldName
     */
    public String getStageFieldName() {
        return stageFieldName;
    }

    /**
     * Sets the stageFieldName.
     *
     * @param stageFieldName the stageFieldName
     */
    public void setStageFieldName(String stageFieldName) {
        this.stageFieldName = stageFieldName;
    }

    /**
     * Gets the stageFieldType
     *
     * @return the stageFieldType
     */
    public Class getStageFieldType() {
        return stageFieldType;
    }

    /**
     * Sets the stageFieldType.
     *
     * @param stageFieldType the stageFieldType
     */
    public void setStageFieldType(Class stageFieldType) {
        this.stageFieldType = stageFieldType;
    }

    /**
     * Gets the validFieldName
     *
     * @return the validFieldName
     */
    public String getValidFieldName() {
        return validFieldName;
    }

    /**
     * Sets the validFieldName.
     *
     * @param validFieldName the validFieldName
     */
    public void setValidFieldName(String validFieldName) {
        this.validFieldName = validFieldName;
    }

    /**
     * Gets the validFieldType
     *
     * @return the validFieldType
     */
    public Class getValidFieldType() {
        return validFieldType;
    }

    /**
     * Sets the validFieldType.
     *
     * @param validFieldType the validFieldType
     */
    public void setValidFieldType(Class validFieldType) {
        this.validFieldType = validFieldType;
    }

    /**
     * Gets the referenceTable
     *
     * @return the referenceTable
     */
    public String getReferenceTable() {
        return referenceTable;
    }

    /**
     * Sets the referenceTable.
     *
     * @param referenceTable the referenceTable
     */
    public void setReferenceTable(String referenceTable) {
        this.referenceTable = referenceTable;
    }

    /**
     * Gets the referenceTableType
     *
     * @return the referenceTableType
     */
    public ReferenceTableTypeEnum getReferenceTableType() {
        return referenceTableType;
    }

    /**
     * Sets the referenceTableType.
     *
     * @param referenceTableType the referenceTableType
     */
    public void setReferenceTableType(ReferenceTableTypeEnum referenceTableType) {
        this.referenceTableType = referenceTableType;
    }

    /**
     * Gets the referenceFromField
     *
     * @return the referenceFromField
     */
    public String getReferenceFromField() {
        return referenceFromField;
    }

    /**
     * Sets the referenceFromField.
     *
     * @param referenceFromField the referenceFromField
     */
    public void setReferenceFromField(String referenceFromField) {
        this.referenceFromField = referenceFromField;
    }

    /**
     * Gets the referenceToField
     *
     * @return the referenceToField
     */
    public String getReferenceToField() {
        return referenceToField;
    }

    /**
     * Sets the referenceToField.
     *
     * @param referenceToField the referenceToField
     */
    public void setReferenceToField(String referenceToField) {
        this.referenceToField = referenceToField;
    }

    /**
     * Gets the referenceTableAdditionalCriteria
     *
     * @return the referenceTableAdditionalCriteria
     */
    public String getReferenceTableAdditionalCriteria() {
        return referenceTableAdditionalCriteria;
    }

    /**
     * Sets the referenceTableAdditionalCriteria.
     *
     * @param referenceTableAdditionalCriteria the referenceTableAdditionalCriteria
     */
    public void setReferenceTableAdditionalCriteria(String referenceTableAdditionalCriteria) {
        this.referenceTableAdditionalCriteria = referenceTableAdditionalCriteria;
    }

    /**
     * Gets the stageFieldMandatory
     *
     * @return the stageFieldMandatory
     */
    public boolean isStageFieldMandatory() {
        return stageFieldMandatory;
    }

    /**
     * Sets the stageFieldMandatory.
     *
     * @param stageFieldMandatory the stageFieldMandatory
     */
    public void setStageFieldMandatory(boolean stageFieldMandatory) {
        this.stageFieldMandatory = stageFieldMandatory;
    }

    /**
     * Gets the validFieldMandatory
     *
     * @return the validFieldMandatory
     */
    public boolean isValidFieldMandatory() {
        return validFieldMandatory;
    }

    /**
     * Sets the validFieldMandatory.
     *
     * @param validFieldMandatory the validFieldMandatory
     */
    public void setValidFieldMandatory(boolean validFieldMandatory) {
        this.validFieldMandatory = validFieldMandatory;
    }

    /**
     * Gets the matchedField
     *
     * @return the matchedField
     */
    public boolean isMatchedField() {
        return matchedField;
    }

    /**
     * Sets the matchedField.
     *
     * @param matchedField the matchedField
     */
    public void setMatchedField(boolean matchedField) {
        this.matchedField = matchedField;
    }

    /**
     * Gets the wildcardFieldValue
     *
     * @return the wildcardFieldValue
     */
    public boolean isWildcardFieldValue() {
        return wildcardFieldValue;
    }

    /**
     * Sets the wildcardFieldValue.
     *
     * @param wildcardFieldValue the wildcardFieldValue
     */
    public void setWildcardFieldValue(boolean wildcardFieldValue) {
        this.wildcardFieldValue = wildcardFieldValue;
    }
}
