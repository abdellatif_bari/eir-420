/*
 * (C) Copyright 2015-2016 Opencell SAS (http://opencellsoft.com/) and contributors.
 * (C) Copyright 2009-2014 Manaty SARL (http://manaty.net/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * This program is not suitable for any direct or indirect application in MILITARY industry
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eir.commons.enums.customfields;

/**
 * EIR service parameters CF Enum
 *
 * @author Abdellatif BARI
 */
public enum ServiceParametersCFEnum {

    /**
     * Attribute Aggregate (for all Bill Items)
     */
    ITEM_ID("item_id"),
    ITEM_COMPONENT_ID("item_component_id"),
    REGION("region"),
    QUANTITY("quantity"),
    SLA("sla"),
    XREF("xref"),
    ACTION_QUALIFIER("action_qualifier"),
    REF_SERVICE_ID("ref_service_id"),

    /**
     * Attribute Aggregate (for all OMS Bill Items)
     */
    AMT("amt"),
    AMTOOC("amtooc"),

    /**
     * Attribute Aggregate (UG Orders)
     */
    EXCHANGE("exchange"),
    //REGION("region"),
    DENSITY("density"),
    RANGE_START("range_start"),
    RANGE_END("range_end"),
    CLASS_OF_SERVICE("class_of_service"),
    EF("ef"),
    AF("af"),
    //SLA("sla"),
    //QUANTITY("quantity"),
    ORDER_TYPE("order_type"),
    EGRESS_GROUP("egress_group"),
    MAIN_SERVICE_BILL_CODE("mainServiceBillCode"),
    OPERATOR_ORD_NUMBER("operator_ord_number"),
    ORDER_ID("order_id"),
    AGENT_CODE("agent_code"),
    END_ENGAGEMENT_DATE("end_engagement_date"),


    /**
     * Attribute Aggregate (for all OMS Bill Items)
     */
    STATE("state"),
    STATUS("status"),
    PROCESSING_STATUS("processing_status"),
    COMPONENT_ID("component_id"),
    NOT_BEFORE_DATE("not_before_date"),
    COMMITMENT_TREATMENT("commitment_treatment"),
    PRICE_PLAN("price_plan"),

    /**
     * Attribute Aggregate (for OMS Main Subscription Details Bill Items)
     */
    //SLA_TYPE("SLA_TYPE"), OMS_MAIN_SLAPRICEPLAN("SLAPRICEPLAN"),
    SERVICE_TYPE("service_type"),
    COMMITMENT_PERIOD("commitment_period"),
    //NOT_BEFORE_DATE("not_before_date"),
    //COMMITMENT_TREATMENT("commitment_treatment"),
    SUBORDER("suborder"),
    //PRICE_PLAN("price_plan"),
    //AMT("amt"),
    //AMTOOC("amtooc"),
    SITE_GENERAL_NUMBER("site_general_number"),
    SGN_STD_CODE("sgn_std_code"),
    RENTAL_LIABILITY_DATE("rentalLiabilityDate"),
    RENTAL_SLA_PREMIUM_LIABILITY_DATE("slaLiabilityDate"),

    /**
     * Attribute Aggregate (for OMS Charges)
     */
    CHARGE_TYPE("charge_type"),
    //NOT_BEFORE_DATE("not_before_date"),
    //COMMITMENT_TREATMENT("commitment_treatment"),
    CHARGE_ID("charge_id"),
    DISTANCE("distance"),
    //PRICE_PLAN("price_plan"),
    RATING_SCHEME("rating_scheme"),
    BANDWIDTH("bandwidth"),
    TRANSMISSION("transmission"),
    //COMMITMENT_PERIOD("commitment_period"),
    ZONE_A("zone_a"),
    ZONE_B("zone_b"),
    //REGION("region"),
    SLA_IND("sla_ind"),
    SERVICE_CATEGORY("service_category"),
    ANALOGUE_QUALITY("analogue_quality"),
    ANALOGUE_RATE("analogue_rate"),
    CPE_TYPE("cpe_type"),
    CPE_DESCRIPTION("cpe_description"),
    ETS_TYPE("ets_type"),
    IND_01("ind_01"),
    IND_02("ind_02"),
    IND_03("ind_03"),
    IND_04("ind_04"),
    IND_05("ind_05"),
    IND_06("ind_06"),
    IND_07("ind_07"),
    IND_08("ind_08"),
    IND_09("ind_09"),
    IND_10("ind_10");
    //AMT("amt"),
    //AMTOOC("amtooc")


    private String label;

    ServiceParametersCFEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }
}
