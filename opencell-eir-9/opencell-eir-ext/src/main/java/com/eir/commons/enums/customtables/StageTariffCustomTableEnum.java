/*
 * (C) Copyright 2015-2016 Opencell SAS (http://opencellsoft.com/) and contributors.
 * (C) Copyright 2009-2014 Manaty SARL (http://manaty.net/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * This program is not suitable for any direct or indirect application in MILITARY industry
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eir.commons.enums.customtables;

/**
 * EIR stage tariff custom table enum
 *
 * @author Abdellatif BARI
 */
public enum StageTariffCustomTableEnum {

    /**
     * Common for all offers
     */
    ID("id"),
    RECORD_ACTION("record_action"),
    TARIFF_PLAN("tariff_plan"),
    OFFER("offer"),
    SERVICE("service"),
    CHARGE_TYPE("charge_type"),
    REC_CALENDAR("rec_calendar"),
    DESCRIPTION("description"),
    ACCOUNTING_CODE("accounting_code"),
    INVOICE_SUBCAT("invoice_subcat"),
    TAX_CLASS("tax_class"),

    /**
     * Common for ACCESS, DATA, and MISC offers
     */
    ACTION_QUALIFIER("action_qualifier"),

    /**
     * Common for ACCESS and DATA offers
     */
    REGION("region"),
    SLA("sla"),

    /**
     * ACCESS offer
     */
    DENSITY("density"),
    CLASS_OF_SERVICE("class_of_service"),
    EF("ef"),
    AF("af"),
    EXCHANGE_CLASS("exchange_class"),
    ORDER_TYPE("order_type"),
    EGRESS_GROUP("egress_group"),

    /**
     * DATA offer
     */
    PRICE_PLAN("price_plan"),
    RATING_SCHEME("rating_scheme"),
    BANDWIDTH("bandwidth"),
    TRANSMISSION("transmission"),
    COMMITMENT_PERIOD("commitment_period"),
    ZONE_A("zone_a"),
    ZONE_B("zone_b"),
    SERVICE_CATEGORY("service_category"),
    ANALOGUE_QUALITY("analogue_quality"),
    ANALOGUE_RATE("analogue_rate"),
    CPE_TYPE("cpe_type"),
    CPE_DESCRIPTION("cpe_description"),
    ETS_TYPE("ets_type"),
    IND_01("ind_01"),
    IND_02("ind_02"),
    IND_03("ind_03"),
    IND_04("ind_04"),
    IND_05("ind_05"),
    IND_06("ind_06"),
    IND_07("ind_07"),
    IND_08("ind_08"),
    IND_09("ind_09"),
    IND_10("ind_10"),
    DISTANCE_FROM("distance_from"),
    DISTANCE_TO("distance_to"),
    DISTANCE_UNIT("distance_unit"),


    /**
     * MUB offer
     */
    AVERAGE_SPEED("average_speed"),

    /**
     * MISC offer
     */
    QUANTITY_FROM("quantity_from"),
    QUANTITY_TO("quantity_to"),
    QUANTITY_UNIT("quantity_unit"),
    MAX_RATED_QUANTITY("max_rated_quantity"),


    /**
     * Common for all offers
     */
    PRICE("price"),
    VALID_FROM("valid_from"),
    VALID_TO("valid_to"),
    FILE_ID("file_id"),
    ORIGIN_ID("origin_id"),
    ERROR_REASON("error_reason"),

    /**
     * Technical fields
     */
    HASH_CODE("hash_code");

    private String label;

    StageTariffCustomTableEnum(String label) {
        this.label = label;
    }

    public static String getCustomTableCode() {
        return "EIR_OR_TARIFF_STAGE";
    }

    public String getLabel() {
        return this.label;
    }
}
