/*
 * (C) Copyright 2015-2016 Opencell SAS (http://opencellsoft.com/) and contributors.
 * (C) Copyright 2009-2014 Manaty SARL (http://manaty.net/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * This program is not suitable for any direct or indirect application in MILITARY industry
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eir.commons.enums.customtables;

/**
 * EIR association custom table enum
 *
 * @author Abdellatif BARI
 */
public enum AssociationCustomTableEnum {

    ID("id"),
    SUBSCRIPTION_ID("subscription_id"),
    ITEM_ID("item_id"),
    ITEM_COMPONENT_ID("item_component_id"),
    ASSOCCIRCID("assoc_circ_id"),
    ASSOCTYPE("assoc_type"),
    ASSOCEND("assoc_end"),
    ASSOCTRIGGER("assoc_trigger"),
    ASSOCVERSION("assoc_version"),
    ASSOCSTATUS("assoc_status"),
    ASSOCSTATE("assoc_state"),
    ASSOCORDER("assoc_order"),
    ASSOCSTART("assoc_start_date"),
    ASSOC_END_DATE("assoc_end_date"),
    ADDRESS_LINE_1("address_line_1"),
    ADDRESS_LINE_2("address_line_2"),
    ADDRESS_LINE_3("address_line_3"),
    ADDRESS_LINE_4("address_line_4"),
    ADDRESS_LINE_5("address_line_5"),
    COUNTY("county"),
    POSTAL_DISTRICT("postal_district"),
    COUNTRY("country"),
    EIR_CODE("eir_code"),
    NAME("name");

    private String label;

    AssociationCustomTableEnum(String label) {
        this.label = label;
    }

    public static String getCustomTableCode() {
        return "EIR_SUB_ASSOCIATION";
    }

    public String getLabel() {
        return this.label;
    }
}
