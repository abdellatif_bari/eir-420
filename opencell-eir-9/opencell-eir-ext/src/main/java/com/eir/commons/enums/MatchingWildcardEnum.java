/*
 * (C) Copyright 2015-2016 Opencell SAS (http://opencellsoft.com/) and contributors.
 * (C) Copyright 2009-2014 Manaty SARL (http://manaty.net/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * This program is not suitable for any direct or indirect application in MILITARY industry
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eir.commons.enums;

import org.meveo.commons.utils.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * EIR matching and ranking Wildcards enum
 *
 * @author Abdellatif BARI
 */
public enum MatchingWildcardEnum {

    /**
     * Match a NULL value only
     */
    NULL(2, "<null>"),

    /**
     * Match any not null value
     */
    ASTERISK(3, "*"),

    /**
     * Match any value, including NULL
     */
    PERCENT(4, "%");

    private static Map<String, MatchingWildcardEnum> _values = new HashMap();

    static {
        for (MatchingWildcardEnum choice : MatchingWildcardEnum.values()) {
            _values.put(choice.label.toUpperCase(), choice);
        }
    }

    private int priority;
    private String label;

    MatchingWildcardEnum(int priority, String label) {
        this.priority = priority;
        this.label = label;
    }

    public static MatchingWildcardEnum getByLabel(String value) {
        if (StringUtils.isBlank(value)) {
            return null;
        }
        return _values.get(value.toUpperCase());
    }

    public static boolean contains(String value) {
        return !StringUtils.isBlank(value) && _values.containsKey(value.toUpperCase());
    }

    public int getPriority() {
        return this.priority;
    }

    public String getLabel() {
        return this.label;
    }
}
