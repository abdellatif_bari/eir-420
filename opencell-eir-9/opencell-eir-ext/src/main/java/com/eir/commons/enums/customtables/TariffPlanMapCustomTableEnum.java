/*
 * (C) Copyright 2015-2016 Opencell SAS (http://opencellsoft.com/) and contributors.
 * (C) Copyright 2009-2014 Manaty SARL (http://manaty.net/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * This program is not suitable for any direct or indirect application in MILITARY industry
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eir.commons.enums.customtables;

/**
 * EIR tariff plan map custom table enum
 *
 * @author Abdellatif BARI
 */
public enum TariffPlanMapCustomTableEnum {

    ID("id"),
    CUSTOMER_ID("customer_id"),
    OFFER_ID("offer_id"),
    VALID_FROM("valid_from"),
    VALID_TO("valid_to"),
    TARIFF_PLAN_ID("tariff_plan_id");

    private String label;

    TariffPlanMapCustomTableEnum(String label) {
        this.label = label;
    }

    public static String getCustomTableCode() {
        return "EIR_OR_TARIFF_PLAN_MAP";
    }

    public String getLabel() {
        return this.label;
    }
}
