package com.eir.commons.utils;

import com.eir.commons.enums.MatchingWildcardEnum;
import com.eir.commons.enums.ReferenceTableTypeEnum;
import com.eir.service.commons.CachedData;
import com.eir.service.commons.UtilService;
import org.hibernate.Query;
import org.meveo.admin.exception.ValidationException;
import org.meveo.admin.util.pagination.PaginationConfiguration;
import org.meveo.api.commons.Utils;
import org.meveo.commons.utils.QueryBuilder;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.base.NativePersistenceService;
import org.meveo.service.custom.CustomTableService;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;

/**
 * EIR business rules validator
 *
 * @author Abdellatif BARI
 */
@Stateless
public class BusinessRulesValidator extends NativePersistenceService {

    @Inject
    private CustomTableService customTableService;
    
    @Inject
    private UtilService utilService;

    @Inject 
    private CachedData cachedData;
    
    /**
     * Validate record.
     *
     * @param record                     the table record
     * @param transformationRulesHandler the transformation rules handler
     * @param checkReferenceData         Indicates if the references data will be checked or not
     * @param validateBusinessRules      the validate business rules function
     * @param parameters                 the parameters list to pass to the validation function
     * @throws ValidationException the validation exception
     */
    public void validateRecord(Map<String, Object> record, TransformationRulesHandler transformationRulesHandler, boolean checkReferenceData,
                               BiFunction<Map<String, Object>, Object[], Object> validateBusinessRules, Object... parameters) throws ValidationException {

        if (transformationRulesHandler == null || transformationRulesHandler.getTransformationRules() == null || transformationRulesHandler.getTransformationRules().isEmpty()) {
            return;
        }
        if (checkReferenceData) {
            for (TransformationRule transformationRule : transformationRulesHandler.getTransformationRules()) {
                if (!StringUtils.isBlank(transformationRule.getReferenceTable())) {
                    if (!transformationRule.isValidFieldMandatory() && transformationRule.isWildcardFieldValue() &&
                            String.class.isAssignableFrom(transformationRule.getStageFieldType()) &&
                            MatchingWildcardEnum.contains((String) record.get(transformationRule.getStageFieldName()))) {
                        continue;
                    }

                    Object matchedValue = getMatchedValue(record, transformationRule);

                    if (matchedValue == null) {
                        String errorMessage = "No matching " + transformationRule.getReferenceTable() + " was found";
                        if (!StringUtils.isBlank(record.get(transformationRulesHandler.getErrorReasonFieldName()))) {
                            errorMessage = record.get(transformationRulesHandler.getErrorReasonFieldName()) + "," + errorMessage;
                        }
                        record.put(transformationRulesHandler.getErrorReasonFieldName(), errorMessage);
                    }
                }
            }
        }

        // Applying the validate business rules
        validateBusinessRules.apply(record, parameters);
    }

    /**
     * Get field priority
     *
     * @param transformationRule the transformation rules
     * @param fieldValue         the field value
     * @return the field priority
     */
    public Integer getFieldPriority(TransformationRule transformationRule, Object fieldValue) {
        // if the field value is taken into account in rating and it can be wildcard value we determine it's priority
        if (!StringUtils.isBlank(fieldValue) && transformationRule.isMatchedField() && transformationRule.isWildcardFieldValue() &&
                String.class.isAssignableFrom(transformationRule.getStageFieldType())) {
            if (MatchingWildcardEnum.contains((String) fieldValue)) {
                return MatchingWildcardEnum.getByLabel((String) fieldValue).getPriority();
            }
            return 1;
        }
        return null;
    }

    /**
     * Accept record and return valid record
     *
     * @param record              the table record
     * @param transformationRules the transformation rules
     * @return the valid record
     * @throws ValidationException the validation exception
     */
    public Map<String, Object> acceptRecord(Map<String, Object> record, List<TransformationRule> transformationRules) throws ValidationException {

        Map<String, Object> dataToAccept = new HashMap();

        StringBuilder recordPriority = new StringBuilder();

        for (TransformationRule transformationRule : transformationRules) {
            if (StringUtils.isBlank(transformationRule.getValidFieldName())) {
                continue;
            }

            Integer fieldPriority = getFieldPriority(transformationRule, record.get(transformationRule.getStageFieldName()));
            if (fieldPriority != null) {
                recordPriority.append(fieldPriority);
            }

            if (!StringUtils.isBlank(transformationRule.getReferenceTable())) {
                if (!transformationRule.isValidFieldMandatory() && transformationRule.isWildcardFieldValue() &&
                        String.class.isAssignableFrom(transformationRule.getStageFieldType()) &&
                        MatchingWildcardEnum.contains((String) record.get(transformationRule.getStageFieldName()))) {
                    dataToAccept.put(transformationRule.getValidFieldName(), record.get(transformationRule.getStageFieldName()));
                    continue;
                }


                Object matchedValue = getMatchedValue(record, transformationRule);
                if (matchedValue == null) {
                    throw new ValidationException("Can not accept tariffs: No matching " + transformationRule.getReferenceTable() + " was found");
                }
                dataToAccept.put(transformationRule.getValidFieldName(), matchedValue);
            } else {
                Object acceptValue = record.get(transformationRule.getStageFieldName());
                if (acceptValue != null && !transformationRule.getStageFieldType().isAssignableFrom(transformationRule.getValidFieldType())) {
                    if (StringUtils.isBlank(acceptValue)) {
                        acceptValue = null;
                    } else if (String.class.isAssignableFrom(transformationRule.getStageFieldType()) &&
                            BigDecimal.class.isAssignableFrom(transformationRule.getValidFieldType())) {
                        acceptValue = new BigDecimal((String) acceptValue);
                    } else if (String.class.isAssignableFrom(transformationRule.getStageFieldType()) &&
                            Long.class.isAssignableFrom(transformationRule.getValidFieldType())) {
                        acceptValue = Long.valueOf((String) acceptValue);
                    } else if (String.class.isAssignableFrom(transformationRule.getStageFieldType()) &&
                            Integer.class.isAssignableFrom(transformationRule.getValidFieldType())) {
                        acceptValue = Integer.valueOf((String) acceptValue);
                    } else if (String.class.isAssignableFrom(transformationRule.getStageFieldType()) &&
                            Date.class.isAssignableFrom(transformationRule.getValidFieldType())) {
                        acceptValue = DateUtils.parseDateWithPattern((String) acceptValue, Utils.DATE_DEFAULT_PATTERN);
                    }
                }
                dataToAccept.put(transformationRule.getValidFieldName(), acceptValue);
            }
        }
        dataToAccept.put("priority", recordPriority.toString());
        return dataToAccept;
    }

    /**
     * Get the matched value
     *
     * @param record             the record
     * @param transformationRule the transformation rule
     * @return the matched value
     */
    private Object getMatchedValue(Map<String, Object> record, TransformationRule transformationRule) {

        String cacheKey = transformationRule.getReferenceTableType().name() + "_" + transformationRule.getReferenceTable() + "_" + transformationRule.getStageFieldName() + "_"
                + transformationRule.getValidFieldName() + "_" + record.get(transformationRule.getStageFieldName());

        Object matchedValue = cachedData.get("ref", cacheKey);
        if (matchedValue != null) {
            return matchedValue;
        }

        EntityManager em = getEntityManager();
        List<Object> result = null;
        String query;
        if (transformationRule.getReferenceTableType() == ReferenceTableTypeEnum.TABLE ||
                transformationRule.getReferenceTableType() == ReferenceTableTypeEnum.CUSTOM_TABLE) {
            query = "select " + transformationRule.getReferenceToField() + " from " + transformationRule.getReferenceTable() + " where ";
            if (String.class.isAssignableFrom(transformationRule.getStageFieldType())) {
                query = query + "LOWER(" + transformationRule.getReferenceFromField() + ")=LOWER('" + record.get(transformationRule.getStageFieldName()) + "')";
            } else {
                query = query + transformationRule.getReferenceFromField() + "=" + record.get(transformationRule.getStageFieldName());
            }
            result = em.createNativeQuery(query).getResultList();
            
        } else if (transformationRule.getReferenceTableType() == ReferenceTableTypeEnum.CUSTOM_ENTITY) {
            query = "select " + transformationRule.getReferenceToField() + " from cust_cei  where cet_code = '" + transformationRule.getReferenceTable() + "'";
            if (String.class.isAssignableFrom(transformationRule.getStageFieldType())) {
                query = query + " and LOWER(" + transformationRule.getReferenceFromField() + ")=LOWER('" + record.get(transformationRule.getStageFieldName()) + "')";
            } else {
                query = query + " and " + transformationRule.getReferenceFromField() + "=" + record.get(transformationRule.getStageFieldName());
            }
            result = em.createNativeQuery(query).getResultList();
        }
        matchedValue = (result != null && !result.isEmpty()) ? result.get(0) : null;
        cachedData.put("ref", cacheKey, matchedValue);
        return matchedValue;
    }

    /**
     * Get the matched value
     *
     * @param fieldValue         the field value
     * @param transformationRule the transformation rule
     * @return the matched value
     */
    public Object getMatchedValue(Object fieldValue, TransformationRule transformationRule) {
        EntityManager em = getEntityManager();
        Object matchedValue = null;
        String query;
        List<Object> resultList = null;
        if (transformationRule.getReferenceTableType() == ReferenceTableTypeEnum.TABLE ||
                transformationRule.getReferenceTableType() == ReferenceTableTypeEnum.CUSTOM_TABLE) {
            query = "select " + transformationRule.getReferenceFromField() + " from " + transformationRule.getReferenceTable() + " where ";
            if (String.class.isAssignableFrom(transformationRule.getValidFieldType())) {
                query = query + "LOWER(" + transformationRule.getReferenceToField() + ")=LOWER('" + fieldValue + "')";
            } else {
                query = query + transformationRule.getReferenceToField() + "=" + fieldValue;
            }
            resultList = em.createNativeQuery(query).getResultList();
        } else if (transformationRule.getReferenceTableType() == ReferenceTableTypeEnum.CUSTOM_ENTITY) {
            query = "select " + transformationRule.getReferenceFromField() + " from cust_cei  where cet_code = '" + transformationRule.getReferenceTable() + "'";
            if (String.class.isAssignableFrom(transformationRule.getValidFieldType())) {
                query = query + " and LOWER(" + transformationRule.getReferenceToField() + ")=LOWER('" + fieldValue + "')";
            } else {
                query = query + " and " + transformationRule.getReferenceToField() + "=" + fieldValue;
            }
            resultList = em.createNativeQuery(query).getResultList();
        }
        if (resultList != null && !resultList.isEmpty()) {
            matchedValue = resultList.get(0);
        }
        return matchedValue;
    }

    /**
     * Get wildcard sql for the field in input
     *
     * @param fieldName  the field name
     * @param fieldValue the field value
     * @return wildcard sql for the field in input
     */
    private String getWildcardSql(String fieldName, Object fieldValue) {
        String sql;
        if (fieldValue == null) {
            sql = "(" + fieldName + "='" + MatchingWildcardEnum.PERCENT.getLabel() + "' or " + fieldName + "='" +
                    MatchingWildcardEnum.NULL.getLabel() + "')";
        } else {
            sql = "(" + fieldName + "='" + MatchingWildcardEnum.ASTERISK.getLabel() + "' or " + fieldName + "='" +
                    MatchingWildcardEnum.PERCENT.getLabel() + "' or LOWER(" + fieldName + ")=LOWER('" + fieldValue + "'))";
        }
        return sql;
    }

    /**
     * Add criterion
     *
     * @param queryBuilder the query builder
     * @param fieldType    the field type
     * @param fieldName    the field name
     * @param fieldValue   the field value
     */
    private void addCriterion(QueryBuilder queryBuilder, Class fieldType, String fieldName, Object fieldValue) {
        if (fieldType != null && String.class.isAssignableFrom(fieldType)) {
            queryBuilder.addSql(getWildcardSql(fieldName, fieldValue));
        } else {
            queryBuilder.addCriterion(fieldName, " = ", fieldValue, false, false);
        }
    }

    /**
     * Add filter
     *
     * @param filters            the filters
     * @param fieldType          the field type
     * @param fieldName          the field name
     * @param fieldValue         the field value
     * @param wildcardFieldValue indicates if the Field value can be wildcard
     */
    private void addFilter(Map<String, Object> filters, Class fieldType, String fieldName, Object fieldValue, boolean wildcardFieldValue) {
        String filter = (String) filters.get("SQL");
        filter = !StringUtils.isBlank(filter) ? filter + " and " : "";
        if (wildcardFieldValue && fieldType != null && String.class.isAssignableFrom(fieldType)) {
            filter = filter + getWildcardSql(fieldName, fieldValue);
            filters.put("SQL", filter);
        } else {
            filters.put(fieldName, fieldValue);
        }
    }

    /**
     * Get overlap records list with the provided stage record
     *
     * @param record                     the record
     * @param transformationRulesHandler the transformation rules handler
     * @param excludedCriterion          the excluded criterion
     * @return overlap stage list
     */
    public List<Map<String, Object>> getOverlapStageRecords(Map<String, Object> record, TransformationRulesHandler transformationRulesHandler, List<String> excludedCriterion) {

        if (transformationRulesHandler == null || transformationRulesHandler.getTransformationRules() == null || transformationRulesHandler.getTransformationRules().isEmpty()) {
            return null;
        }
        QueryBuilder queryBuilder = customTableService.getQuery(transformationRulesHandler.getStageTableName(), null);

        if (record.get("id") != null && (excludedCriterion == null || !excludedCriterion.contains("id"))) {
            queryBuilder.addCriterion("id", " != ", record.get("id"), false, false);
        }
        List<TransformationRule> transformationRules = transformationRulesHandler.getTransformationRules();

        for (TransformationRule transformationRule : transformationRules) {
            if (excludedCriterion == null || !excludedCriterion.contains(transformationRule.getStageFieldName())) {
                addCriterion(queryBuilder, transformationRule.getStageFieldType(), transformationRule.getStageFieldName(), record.get(transformationRule.getStageFieldName()));
            }
        }
        utilService.addOverlapCriteria(queryBuilder, transformationRulesHandler.getValidFromFieldName(), transformationRulesHandler.getValidToFieldName(),
                DateUtils.parseDateWithPattern((String) record.get(transformationRulesHandler.getValidFromFieldName()), Utils.DATE_DEFAULT_PATTERN),
                DateUtils.parseDateWithPattern((String) record.get(transformationRulesHandler.getValidToFieldName()), Utils.DATE_DEFAULT_PATTERN));

        Query query = queryBuilder.getNativeQuery(getEntityManager(), true);
        return query.list();
    }


    /**
     * Get overlap records list with the provided valid record
     *
     * @param record                     the record
     * @param transformationRulesHandler the transformation rules handler
     * @param excludedCriterion          the excluded criterion
     * @return overlap stage list
     */
    public List<Map<String, Object>> getOverlapValidRecords(Map<String, Object> record, TransformationRulesHandler transformationRulesHandler, List<String> excludedCriterion) {

        if (transformationRulesHandler == null || transformationRulesHandler.getTransformationRules() == null || transformationRulesHandler.getTransformationRules().isEmpty()) {
            return null;
        }
        EntityManager em = getEntityManager();
        List<TransformationRule> transformationRules = transformationRulesHandler.getTransformationRules();

        QueryBuilder queryBuilder = customTableService.getQuery(transformationRulesHandler.getValidTableName(), null);


        for (TransformationRule transformationRule : transformationRules) {
            if (StringUtils.isBlank(transformationRule.getValidFieldName()) || (excludedCriterion != null && excludedCriterion.contains(transformationRule.getStageFieldName()))) {
                continue;
            }
            if (!StringUtils.isBlank(transformationRule.getReferenceTable())) {
                Object matchedValue = getMatchedValue(record, transformationRule);
                if (matchedValue != null) {
                    addCriterion(queryBuilder, transformationRule.getValidFieldType(), transformationRule.getValidFieldName(), matchedValue);
                }
            } else {
                addCriterion(queryBuilder, transformationRule.getValidFieldType(), transformationRule.getValidFieldName(), record.get(transformationRule.getStageFieldName()));
            }
        }

        utilService.addOverlapCriteria(queryBuilder, transformationRulesHandler.getValidFromFieldName(), transformationRulesHandler.getValidToFieldName(),
                DateUtils.parseDateWithPattern((String) record.get(transformationRulesHandler.getValidFromFieldName()), Utils.DATE_DEFAULT_PATTERN),
                DateUtils.parseDateWithPattern((String) record.get(transformationRulesHandler.getValidToFieldName()), Utils.DATE_DEFAULT_PATTERN));

        Query query = queryBuilder.getNativeQuery(getEntityManager(), true);
        return query.list();
    }

    /**
     * Get valid record
     *
     * @param record                     the record
     * @param transformationRulesHandler the transformation rules handler
     * @param excludedCriterion          the excluded criterion
     * @param allRecordFields            indicates if we take all fields of the record in the search or just the matched fields
     * @return valid record
     */
    public Map<String, Object> getValidRecord(Map<String, Object> record, TransformationRulesHandler transformationRulesHandler, List<String> excludedCriterion,
                                              boolean allRecordFields) {

        if (transformationRulesHandler == null || transformationRulesHandler.getTransformationRules() == null ||
                transformationRulesHandler.getTransformationRules().isEmpty()) {
            return null;
        }
        Map<String, Object> filters = new HashMap();
        List<TransformationRule> transformationRules = transformationRulesHandler.getTransformationRules();

        for (TransformationRule transformationRule : transformationRules) {
            if (isRequiredCriterion(transformationRule.getValidFieldName(), transformationRulesHandler, transformationRule, excludedCriterion, allRecordFields)) {
                addFilter(filters, transformationRule.getValidFieldType(), transformationRule.getValidFieldName(), record.get(transformationRule.getValidFieldName()),
                        transformationRule.isWildcardFieldValue());
            }
        }

        filters.put(transformationRulesHandler.getValidToFieldName(), null);
        List<Map<String, Object>> records = customTableService.list(transformationRulesHandler.getValidTableName(), new PaginationConfiguration(filters));
        if (!records.isEmpty()) {
            return records.get(0);
        }
        return null;
    }


    /**
     * Add criterion
     *
     * @param criteria        the criteria
     * @param leftTableAlias  the left table alias
     * @param rightTableAlias the right table alias
     * @param fieldName       the field name
     * @param fieldType       the field type
     */
    private void addCriterion(StringBuffer criteria, String leftTableAlias, String rightTableAlias, String fieldName, Class fieldType) {
        if (criteria != null && criteria.length() > 0) {
            criteria.append(" and ");
        }

        if (Long.class.isAssignableFrom(fieldType)) {
            criteria.append("(").append("coalesce(").append(leftTableAlias).append(".").append(fieldName).append(", 0)");
            criteria.append("=").append("coalesce(").append(rightTableAlias).append(".").append(fieldName).append(", 0))");
        } else if (String.class.isAssignableFrom(fieldType)) {
            criteria.append("(LOWER(").append(leftTableAlias).append(".").append(fieldName).append(")=LOWER(").append(rightTableAlias).append(".").append(fieldName).append("))");
        } else {
            criteria.append("(").append(leftTableAlias).append(".").append(fieldName).append("=").append(rightTableAlias).append(".").append(fieldName).append(")");
        }


    }

    /**
     * Get exact matching criteria for staging table
     *
     * @param transformationRulesHandler the transformation rules handler
     * @param leftTableAlias             the left table alias
     * @param rightTableAlias            the right table alias
     * @param excludedCriterion          the excluded criterion
     * @param allRecordFields            indicates if we take all fields of the record in the search or just the matched fields
     * @return exact matching criteria for staging table
     */
    public String getExactMatchingCriteriaForStageTable(TransformationRulesHandler transformationRulesHandler, String leftTableAlias, String rightTableAlias,
                                                        List<String> excludedCriterion, boolean allRecordFields) {
        if (transformationRulesHandler == null || transformationRulesHandler.getTransformationRules() == null ||
                transformationRulesHandler.getTransformationRules().isEmpty()) {
            return null;
        }
        StringBuffer criteria = new StringBuffer();
        List<TransformationRule> transformationRules = transformationRulesHandler.getTransformationRules();
        for (TransformationRule transformationRule : transformationRules) {
            if (isRequiredCriterion(transformationRule.getStageFieldName(), transformationRulesHandler, transformationRule, excludedCriterion, allRecordFields)) {
                addCriterion(criteria, leftTableAlias, rightTableAlias, transformationRule.getStageFieldName(), transformationRule.getStageFieldType());
            }
        }
        return criteria.toString();
    }

    /**
     * Return exact matching criteria for reference tables
     *
     * @param transformationRulesHandler the transformation rules handler
     * @param leftTableAlias             the left table alias
     * @param rightTableAlias            the right table alias
     * @param excludedCriterion          the excluded criterion
     * @param allRecordFields            indicates if we take all fields of the record in the search or just the matched fields
     * @return exact matching criteria for valid table
     */
    public String getExactMatchingCriteriaForValidTable(TransformationRulesHandler transformationRulesHandler, String leftTableAlias, String rightTableAlias,
                                                        List<String> excludedCriterion, boolean allRecordFields) {

        if (transformationRulesHandler == null || transformationRulesHandler.getTransformationRules() == null
                || transformationRulesHandler.getTransformationRules().isEmpty()) {
            return null;
        }

        StringBuffer criteria = new StringBuffer();

        for (TransformationRule transformationRule : transformationRulesHandler.getTransformationRules()) {
            if (!isRequiredCriterion(transformationRule.getValidFieldName(), transformationRulesHandler, transformationRule, excludedCriterion, allRecordFields)) {
                continue;
            }
            if (criteria.length() > 0 && transformationRulesHandler.getTransformationRules().indexOf(transformationRule) > 0) {
                criteria.append(" and ");
            }
            if (!StringUtils.isBlank(transformationRule.getReferenceTable())) {
                if (!transformationRule.isValidFieldMandatory()) {
                    criteria.append("(");
                }
                if (transformationRule.getReferenceTableType() == ReferenceTableTypeEnum.TABLE || transformationRule.getReferenceTableType() == ReferenceTableTypeEnum.CUSTOM_TABLE) {
                    criteria.append(rightTableAlias).append(".").append(transformationRule.getValidFieldName()).append(" in (");
                    criteria.append("select ").append(transformationRule.getReferenceToField()).append(" from ").append(transformationRule.getReferenceTable());
                    if (String.class.isAssignableFrom(transformationRule.getStageFieldType())) {
                        criteria.append(" where LOWER(").append(transformationRule.getReferenceFromField()).append(")=LOWER(").append(leftTableAlias).append(".").append(transformationRule.getStageFieldName()).append("))");
                    } else {
                        criteria.append(" where ").append(transformationRule.getReferenceFromField()).append("= ").append(leftTableAlias).append(".").append(transformationRule.getStageFieldName()).append(")");
                    }
                } else if (transformationRule.getReferenceTableType() == ReferenceTableTypeEnum.CUSTOM_ENTITY) {
                    criteria.append(rightTableAlias).append(".").append(transformationRule.getValidFieldName()).append(" in (");
                    criteria.append("select ").append(transformationRule.getReferenceToField()).append(" from cust_cei where cet_code = '").append(transformationRule.getReferenceTable()).append("'");
                    if (String.class.isAssignableFrom(transformationRule.getStageFieldType())) {
                        criteria.append(" and LOWER(").append(transformationRule.getReferenceFromField()).append(")=LOWER(").append(leftTableAlias).append(".").append(transformationRule.getStageFieldName()).append("))");
                    } else {
                        criteria.append(" and ").append(transformationRule.getReferenceFromField()).append("=").append(leftTableAlias).append(".").append(transformationRule.getStageFieldName()).append(")");
                    }
                }
                if (!transformationRule.isValidFieldMandatory() && transformationRule.isWildcardFieldValue()) {
                    criteria.append("or (").append(rightTableAlias).append(".").append(transformationRule.getValidFieldName()).append(" in (");
                    criteria.append("'").append(MatchingWildcardEnum.ASTERISK.getLabel()).append("','").append(MatchingWildcardEnum.PERCENT.getLabel()).append("','");
                    criteria.append(MatchingWildcardEnum.NULL.getLabel()).append("')))");
                }
            } else {
                if (Long.class.isAssignableFrom(transformationRule.getStageFieldType()) &&
                        Long.class.isAssignableFrom(transformationRule.getValidFieldType())) {
                    criteria.append("coalesce(").append(leftTableAlias).append(".").append(transformationRule.getStageFieldName()).append(", 0)");
                    criteria.append(" = ").append("coalesce(").append(rightTableAlias).append(".").append(transformationRule.getValidFieldName()).append(", 0)");
                } else if (String.class.isAssignableFrom(transformationRule.getStageFieldType())) {
                    criteria.append("LOWER(").append(leftTableAlias).append(".").append(transformationRule.getStageFieldName());
                    criteria.append(")=LOWER(").append(rightTableAlias).append(".").append(transformationRule.getValidFieldName()).append(")");
                } else {
                    criteria.append(leftTableAlias).append(".").append(transformationRule.getStageFieldName());
                    criteria.append(" = ").append(rightTableAlias).append(".").append(transformationRule.getValidFieldName());
                }
            }
        }
        return criteria.toString();
    }

    /**
     * Check if the field can be considered in the search
     *
     * @param fieldName                  the field name
     * @param transformationRulesHandler the transformation rules handler
     * @param transformationRule         the transformation rule
     * @param excludedCriterion          the excluded criterion
     * @param allRecordFields            indicates if we take all fields of the record in the search or just the matched fields
     * @return true if the field will be taken into consideration in the search
     */
    private boolean isRequiredCriterion(String fieldName, TransformationRulesHandler transformationRulesHandler, TransformationRule transformationRule,
                                        List<String> excludedCriterion, boolean allRecordFields) {
        boolean matchingWithHash = transformationRulesHandler.isMatchingWithHash() && transformationRule.isMatchedField();
        boolean match = allRecordFields ? true : transformationRule.isMatchedField() || "hash_code".equalsIgnoreCase(fieldName);
        if (StringUtils.isBlank(fieldName) || matchingWithHash || !match || (excludedCriterion != null && excludedCriterion.contains(fieldName))) {
            return false;
        }
        return true;
    }
}