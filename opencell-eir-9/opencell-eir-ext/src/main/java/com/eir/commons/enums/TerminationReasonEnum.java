/*
 * (C) Copyright 2015-2016 Opencell SAS (http://opencellsoft.com/) and contributors.
 * (C) Copyright 2009-2014 Manaty SARL (http://manaty.net/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * This program is not suitable for any direct or indirect application in MILITARY industry
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eir.commons.enums;

import org.meveo.model.billing.OverrideProrataEnum;

/**
 * EIR Termination reason enum
 *
 * @author Abdellatif BARI
 */
public enum TerminationReasonEnum {
    /**
     * Termination without overriding prorata and with agreement application
     */
    TR_NO_OVERRIDE_AGR("TR_NO_OVERRIDE_AGR", OverrideProrataEnum.NO_OVERRIDE, true, false, false, false),

    /**
     * Termination without overriding prorata and with agreement and recurring refund application
     */
    TR_NO_OVERRIDE_AGR_RREFUND("TR_NO_OVERRIDE_AGR_RREFUND", OverrideProrataEnum.NO_OVERRIDE, true, true, false, false),

    /**
     * Termination without overriding prorata and with agreement, recurring refund and charge application
     */
    TR_NO_OVERRIDE_AGR_RREFUND_CHARGE("TR_NO_OVERRIDE_AGR_RREFUND_CHARGE", OverrideProrataEnum.NO_OVERRIDE, true, true, true, false),

    /**
     * Termination without overriding prorata and with agreement, recurring refund, charge and oneshot refund application
     */
    TR_NO_OVERRIDE_AGR_RREFUND_CHARGE_OREFUND("TR_NO_OVERRIDE_AGR_RREFUND_CHARGE_OREFUND", OverrideProrataEnum.NO_OVERRIDE, true, true, true, true),

    /**
     * Termination without overriding prorata and with agreement and charge application
     */
    TR_NO_OVERRIDE_AGR_CHARGE("TR_NO_OVERRIDE_AGR_CHARGE", OverrideProrataEnum.NO_OVERRIDE, true, false, true, false),

    /**
     * Termination without overriding prorata and with recurring refund
     */
    TR_NO_OVERRIDE_RREFUND("TR_NO_OVERRIDE_RREFUND", OverrideProrataEnum.NO_OVERRIDE, false, true, false, false),

    /**
     * Termination without overriding prorata and with recurring refund and apply service
     */
    TR_NO_OVERRIDE_RREFUND_SERVICE("TR_NO_OVERRIDE_RREFUND", OverrideProrataEnum.NO_OVERRIDE, false, true, false, false, true),

    /**
     * Termination without overriding prorata and with recurring refund and charge application
     */
    TR_NO_OVERRIDE_RREFUND_CHARGE("TR_NO_OVERRIDE_RREFUND_CHARGE", OverrideProrataEnum.NO_OVERRIDE, false, true, true, false),

    /**
     * Termination without overriding prorata and without charges and no refund
     */
    TR_NO_OVERRIDE_NONE("TR_NO_OVERRIDE_NONE", OverrideProrataEnum.NO_OVERRIDE, false, false, false, false),

    /**
     * Termination without overriding prorata and with agreement and recurring refund one shots
     */
    TR_FAILED_INSTALL("TR_FAILED_INSTALL", OverrideProrataEnum.NO_OVERRIDE, false, true, false, true),

    /**
     * Termination with prorata and with agreement and recurring refund one shots
     */
    TR_TOS("TR_TOS", OverrideProrataEnum.PRORATA, false, true, false, false);

    private String code;
    private OverrideProrataEnum overrideProrata;
    private boolean applyAgreement;
    private boolean applyReimbursment;
    private boolean applyTerminationCharges;
    private boolean reimburseOneshots;
    private boolean applyService;

    TerminationReasonEnum(String code, OverrideProrataEnum overrideProrata, boolean applyAgreement, boolean applyReimbursment,
                          boolean applyTerminationCharges, boolean reimburseOneshots) {
        this.code = code;
        this.overrideProrata = overrideProrata;
        this.applyAgreement = applyAgreement;
        this.applyReimbursment = applyReimbursment;
        this.applyTerminationCharges = applyTerminationCharges;
        this.reimburseOneshots = reimburseOneshots;
    }

    TerminationReasonEnum(String code, OverrideProrataEnum overrideProrata, boolean applyAgreement, boolean applyReimbursment,
                          boolean applyTerminationCharges, boolean reimburseOneshots, boolean applyService) {
        this.code = code;
        this.overrideProrata = overrideProrata;
        this.applyAgreement = applyAgreement;
        this.applyReimbursment = applyReimbursment;
        this.applyTerminationCharges = applyTerminationCharges;
        this.reimburseOneshots = reimburseOneshots;
        this.applyService = applyService;
    }

    public String getCode() {
        return code;
    }

    public boolean isApplyTerminationCharges() {
        return applyTerminationCharges;
    }

    public boolean isApplyService() {
        return applyService;
    }
}
