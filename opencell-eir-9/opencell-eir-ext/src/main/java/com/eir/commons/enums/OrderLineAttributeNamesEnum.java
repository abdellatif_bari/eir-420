/*
 * (C) Copyright 2015-2016 Opencell SAS (http://opencellsoft.com/) and contributors.
 * (C) Copyright 2009-2014 Manaty SARL (http://manaty.net/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * This program is not suitable for any direct or indirect application in MILITARY industry
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eir.commons.enums;

/**
 * EIR order line attribute names Enum
 *
 * @author Abdellatif BARI
 */
public enum OrderLineAttributeNamesEnum {

    /**
     * Attribute (for all Bill Items)
     */
    ITEM_ID("Item_Id"),
    OPERATOR_ID("Operator_Id"),
    OPERATOR_ORDER_NUMBER("Operator_Order_Number"),
    ITEM_COMPONENT_ID("Item_Component_Id"),
    ITEM_VERSION("Item_Version"),
    ITEM_TYPE("Item_Type"),
    AGENT_CODE("Agent_Code"),
    UAN("UAN"),
    MASTER_ACCOUNT_NUMBER("Master_Account_Number"),
    SERVICE_ID("Service_Id"),
    REF_SERVICE_ID("Ref_Service_Id"),
    XREF_SERVICE_ID("Xref_Service_Id"),
    BILL_CODE("Bill_Code"),
    ACTION("Action"),
    ACTION_QUALIFIER("Action_Qualifier"),
    QUANTITY("Quantity"),
    EFFECT_DATE("Effect_Date"),

    /**
     * Attribute Aggregate (UG Orders)
     */
    UG_EXCHANGE("EXCHANGE"),
    UG_REGION("REGION"),
    UG_DENSITY("DENSITY"),
    UG_RANGE_START("RANGE_START"),
    UG_RANGE_END("RANGE_END"),
    UG_NEW_RANGE_START("NEW_RANGE_START"),
    UG_NEW_RANGE_END("NEW_RANGE_END"),
    UG_CLASS_OF_SERVICE("CLASS_OF_SERVICE"),
    UG_EF("EF"),
    UG_AF("AF"),
    UG_SLA("SLA"),
    UG_BANDWIDTH("BANDWIDTH"),
    UG_A_END_ARD_ID("A_END_ARD_ID"),
    UG_B_END_ARD_ID("B_END_ARD_ID"),
    UG_ORDER_TYPE("ORDER_TYPE"),
    UG_EGRESS_GROUP("EGRESS_GROUP"),
    
    /**
     * eBill attribute (UG order)
     */
    ORDER_ACTION("Order_Action"),

    /**
     * Attribute Aggregate (WSLAM Rebates)
     */
    WSLAM_REBATE_FROM_DATE("REBATE_FROM_DATE"),
    WSLAM_REBATE_TO_DATE("REBATE_TO_DATE"),
    WSLAM_REBATE_AMOUNT("REBATE_AMOUNT"),

    /**
     * Attribute Aggregate (for all OMS Bill Items)
     */
    OMS_STATE("STATE"),
    OMS_STATUS("STATUS"),
    OMS_PROCESSING_STATUS("ProcessingStatus"),
    OMS_COMPONENT_ID("ComponentID"),
    OMS_COMMITMENT_PERIOD("COMMITMENT_PERIOD"),
    OMS_PRICE_PLAN("PRICE_PLAN"),
    OMS_AMT("AMT"),
    OMS_AMTOOC("AMTOOC"),

    /**
     * Attribute Aggregate (for OMS Main Subscription Details Bill Items)
     */
    OMS_MAIN_SLA_IND("SLA_IND"),
    //OMS_MAIN_SLA_TYPE("SLA_TYPE"),
    OMS_MAIN_SERVICE_TYPE("SERVICE_TYPE"),
    //OMS_MAIN_COMMITMENT_PERIOD("COMMITMENT_PERIOD"),
    OMS_MAIN_NOT_BEFORE_DATE("NOTBEFOREDATE"),
    OMS_MAIN_COMMITMENT_TREATMENT("CMMITMNTTREATMENT"),
    OMS_MAIN_SUBORDER("SUBORDER"),
    //OMS_MAIN_PRICE_PLAN("PRICE_PLAN"),
    //OMS_MAIN_AMT("AMT"),
    //OMS_MAIN_AMTOOC("AMTOOC"),
    OMS_MAIN_SITE_GENERAL_NUMBER("SITE_GENERAL_NUMBER"),
    OMS_MAIN_SGN_STD_CODE("SGN STD Code"),


    /**
     * Attribute Aggregate (for OMS End Points)
     */
    OMS_EP_EPVERSION("EPVERSION"),
    OMS_EP_EPADDRESS("EPADDRESS"),
    OMS_EP_EPNAME("EPNAME"),
    OMS_EP_EPTYPE("EPTYPE"),
    OMS_EP_EPSTATE("EPSTATE"),
    OMS_EP_LINE1("LINE1"),
    OMS_EP_LINE2("LINE2"),
    OMS_EP_LINE3("LINE3"),
    OMS_EP_LINE4("LINE4"),
    OMS_EP_LINE5("LINE5"),
    OMS_EP_COUNTY("County"),
    OMS_EP_POSTAL_DISTRICT("Postal District"),
    OMS_EP_COUNTRY_CODE("COUNTRY_CODE"),
    OMS_EP_EIRCODE("eircode"),
    OMS_EP_EPORDER("EPORDER"),
    OMS_EP_EPEND("EPEND"),
    OMS_EP_EPZONE("EPZONE"),
    OMS_EP_EPSTART("EPSTART"),
    OMS_EP_EPSTATUS("EPSTATUS"),
    OMS_EP_EPID("EPID"),


    /**
     * Attribute Aggregate (for OMS Associations)
     */
    OMS_ASS_ASSOCVERSION("ASSOCVERSION"),
    OMS_ASS_ASSOCCIRCID("ASSOCCIRCID"),
    OMS_ASS_ASSOCTYPE("ASSOCTYPE"),
    OMS_ASS_ASSOCSTATE("ASSOCSTATE"),
    OMS_ASS_ASSOCORDER("ASSOCORDER"),
    OMS_ASS_ASSOCEND("ASSOCEND"),
    //OMS_ASS_ASSOCTRIGGER("ASSOCTRIGGER"),
    OMS_ASS_ASSOCSTATUS("ASSOCSTATUS"),
    OMS_ASS_ASSOCSTART("ASSOCSTART"),

    /**
     * Attribute Aggregate (for OMS Remarks)
     */
    OMS_REMARK_ID("REMARK_ID"),
    OMS_REMARK_DESCRIPTION("DESCRIPTION"),
    OMS_REMARK_ELEMENT_ID("ELEMENT_ID"),
    OMS_REMARK_ELEMENT_TYPE("ELEMENT_TYPE"),
    OMS_REMARK_ORDER_ID("ORDER_ID"),

    /**
     * Attribute Aggregate (for OMS Charges)
     */
    OMS_CHARGE_TYPE("CHARGETYPE"),
    //OMS_CHARGE_NOTBEFOREDATE("NOTBEFOREDATE"),
    //OMS_CHARGE_COMMITMENT_TREATMENT("CMMITMNTTREATMENT"),
    OMS_CHARGE_ID("CHARGEID"),
    OMS_CHARGE_DISTANCE("DISTANCE"),
    //OMS_CHARGE_PRICE_PLAN("PRICE_PLAN"),
    OMS_CHARGE_RATING_SCHEME("RATING_SCHEME"),
    OMS_CHARGE_BANDWIDTH("BANDWIDTH"),
    OMS_CHARGE_TRANSMISSION("TRANSMISSION"),
    //OMS_CHARGE_COMMITMENT_PERIOD("COMMITMENT_PERIOD"),
    //OMS_CHARGE_COMMITMENT_TREATMENT("CMMITMNTTREATMENT"),
    //OMS_CHARGE_NOTBEFOREDATE("NOTBEFOREDATE"),
    OMS_CHARGE_ZONE_A("ZONE_A"),
    OMS_CHARGE_ZONE_B("ZONE_B"),
    OMS_CHARGE_REGION("REGION"),
    OMS_CHARGE_SLA("SLA"),
    OMS_CHARGE_SERVICE_CATEGORY("SERVICE_CATEGORY"),
    OMS_CHARGE_ANALOGUE_QUALITY("ANALOGUE_QUALITY"),
    OMS_CHARGE_ANALOGUE_RATE("ANALOGUE_RATE"),
    OMS_CHARGE_CPE_TYPE("CPE_TYPE"),
    OMS_CHARGE_CPE_DESCRIPTION("CPE_DESCRIPTION"),
    OMS_CHARGE_ETS_TYPE("ETS_TYPE"),
    OMS_CHARGE_CPE_IND_01("IND_01"),
    OMS_CHARGE_CPE_IND_02("IND_02"),
    OMS_CHARGE_CPE_IND_03("IND_03"),
    OMS_CHARGE_CPE_IND_04("IND_04"),
    OMS_CHARGE_CPE_IND_05("IND_05"),
    OMS_CHARGE_CPE_IND_06("IND_06"),
    OMS_CHARGE_CPE_IND_07("IND_07"),
    OMS_CHARGE_CPE_IND_08("IND_08"),
    OMS_CHARGE_CPE_IND_09("IND_09"),
    OMS_CHARGE_CPE_IND_10("IND_10"),
    //OMS_CHARGE_AMT("AMT"),
    //OMS_CHARGE_AMTOOC("AMTOOC")
    ;

    private String label;

    OrderLineAttributeNamesEnum(String label) {
        this.label = label;
    }

    public String getLabel() {
        return this.label;
    }
}
