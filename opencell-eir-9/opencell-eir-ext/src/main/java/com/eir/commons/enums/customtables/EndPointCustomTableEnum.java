/*
 * (C) Copyright 2015-2016 Opencell SAS (http://opencellsoft.com/) and contributors.
 * (C) Copyright 2009-2014 Manaty SARL (http://manaty.net/) and contributors.
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * This program is not suitable for any direct or indirect application in MILITARY industry
 * See the GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package com.eir.commons.enums.customtables;

/**
 * EIR end point custom table enum
 *
 * @author Abdellatif BARI
 */
public enum EndPointCustomTableEnum {

    ID("id"),
    SUBSCRIPTION_ID("subscription_id"),
    ITEM_ID("item_id"),
    ITEM_COMPONENT_ID("item_component_id"),
    EPID("ep_id"),
    EPNAME("ep_name"),
    EPTYPE("ep_type"),
    EPEND("ep_end"),
    EPVERSION("ep_version"),
    EPSTATUS("ep_status"),
    EPSTATE("ep_state"),
    EPORDER("ep_order"),
    EPSTART("ep_start_date"),
    EP_END_DATE("ep_end_date"),
    EPADDRESS("ep_address"),
    ADDRESS_LINE_1("address_line_1"),
    ADDRESS_LINE_2("address_line_2"),
    ADDRESS_LINE_3("address_line_3"),
    ADDRESS_LINE_4("address_line_4"),
    ADDRESS_LINE_5("address_line_5"),
    COUNTY("county"),
    POSTAL_DISTRICT("postal_district"),
    COUNTRY("country"),
    EIR_CODE("eir_code");

    private String label;

    EndPointCustomTableEnum(String label) {
        this.label = label;
    }

    public static String getCustomTableCode() {
        return "EIR_SUB_ENDPOINT";
    }

    public String getLabel() {
        return this.label;
    }
}
