package com.eir.jms;

import com.eir.api.order.OrderApi;
import com.eir.cache.OrderCacheContainerProvider;
import com.eir.commons.enums.OrderProcessingTypeEnum;
import com.eir.service.commons.UtilService;
import com.eir.service.order.impl.OrderService;
import org.meveo.admin.exception.BusinessException;
import org.meveo.api.dto.order.OrderDto;
import org.meveo.commons.utils.StringUtils;
import org.meveo.commons.utils.ThreadUtils;
import org.meveo.model.order.Order;
import org.meveo.model.order.OrderStatusEnum;
import org.meveo.security.keycloak.CurrentUserProvider;
import org.slf4j.Logger;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.AsyncResult;
import javax.ejb.Asynchronous;
import javax.ejb.MessageDriven;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;

/**
 * A Message Driven Bean to handle order processing or reprocessing. Messages are read from a topic "java:/jms/queue/OrderQueue".
 *
 * @author Andrius Karpavicius
 */
@MessageDriven(name = "OrderQueueMonitor", activationConfig = {@ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "java:/jms/queue/OrderQueue"),
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Queue"),
        @ActivationConfigProperty(propertyName = "acknowledgeMode", propertyValue = "Auto-acknowledge")})

public class OrderQueueMonitor implements MessageListener {

    @Inject
    private Logger log;
    @Inject
    private OrderApi orderApi;
    @Inject
    private CurrentUserProvider currentUserProvider;
    @Inject
    private OrderCacheContainerProvider orderCacheContainerProvider;
    @Inject
    private UtilService utilService;
    @Inject
    protected OrderService orderService;

    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void onMessage(Message message) {

        if (message instanceof ObjectMessage) {
            String orderCode = null;

            try {
                OrderDto orderDto = (OrderDto) (((ObjectMessage) message).getObject());
                if (orderDto != null) {
                    orderCode = orderDto.getOrderCode();
                    if (orderDto.getProcessingType() == OrderProcessingTypeEnum.CREATE || orderDto.getProcessingType() == OrderProcessingTypeEnum.BULK) {
                        log.info("Received to process order {}", orderCode);
                        processOrder(orderApi::createOrder, this::sendBackToQueue, orderDto);
                    } else if (orderDto.getProcessingType() == OrderProcessingTypeEnum.REPROCESS) {
                        log.info("Received to reprocess order {}", orderCode);
                        processOrder(orderApi::reprocessOrder, this::sendBackToQueue, orderDto);
                    } else if (orderDto.getProcessingType() == OrderProcessingTypeEnum.RELEASE) {
                        log.info("Received to release order {}", orderCode);
                        processOrder(orderApi::releaseOrder, this::sendBackToQueue, orderDto);
                    }
                }
            } catch (Exception e) {
                log.error("Failed to process Order {}", orderCode, e);
            }

        } else {
            log.warn("Unhandled order processing event message type: " + message.getClass().getName());
        }

    }

    /**
     * Process order
     *
     * @param processFunction         the process function to be executed
     * @param customSendBackToProcess the custom send back to process function to be executed
     * @param parameters              the function parameters
     * @param <T>
     * @param <U>
     * @param <V>
     * @throws BusinessException the business exception
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public <T, U, V> void processOrder(Function<T, U> processFunction, Function<T, V> customSendBackToProcess, Object... parameters) throws BusinessException {
        if (parameters == null || parameters.length < 1 || parameters[0] == null) {
            return;
        }
        T dto = (T) parameters[0];
        OrderDto orderDto = (OrderDto) dto;
        if (orderDto != null) {
            Order order = !StringUtils.isBlank(orderDto.getOrderId()) ? orderService.findById(orderDto.getOrderId()) : null;
            if (order != null && order.getStatus() == OrderStatusEnum.COMPLETED) {
                return;
            }

            orderDto.addSubscriptionCodes(orderDto.getOrderLines());
            boolean isOrderAlreadyCached = orderCacheContainerProvider.addToCache(orderDto);
            if (isOrderAlreadyCached) {
                if (customSendBackToProcess != null) {
                    customSendBackToProcess.apply(dto);
                } else {
                    sendBackToProcess(processFunction, parameters);
                }
            } else {
                try {
                    currentUserProvider.forceAuthentication((orderDto).getUserName(), (orderDto).getProviderCode());
                    // if the order is not in progress or pending status then set it in progress status before starting the processing
                    if (order != null && order.getStatus() != OrderStatusEnum.PENDING && order.getStatus() != OrderStatusEnum.IN_PROGRESS) {
                        orderApi.setInProgressStatus(order);
                    }
                    processFunction.apply(dto);
                } finally {
                    orderCacheContainerProvider.removeFomCache(orderDto, orderDto.getOrderCode());
                }
            }
        }
    }

    /**
     * Send back order in the queue
     *
     * @param orderDto the order Dto
     * @return the future of order Dto
     * @throws BusinessException the business exception
     */
    @Asynchronous
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public Future<OrderDto> sendBackToQueue(OrderDto orderDto) throws BusinessException {
        ThreadUtils.sleepSafe(TimeUnit.SECONDS, 5);
        utilService.sendToOrderQueue(orderDto);
        return new AsyncResult<>(orderDto);
    }

    /**
     * Send back order to process
     *
     * @param processFunction the process function to be executed
     * @param parameters      the function parameters
     * @param <T>
     * @param <U>
     * @return the future of order Dto
     * @throws BusinessException the business exception
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public <T, U> Future<OrderDto> sendBackToProcess(Function<T, U> processFunction, Object... parameters) throws BusinessException {
        if (parameters == null || parameters.length < 1) {
            return new AsyncResult<>(null);
        }
        OrderDto orderDto = (OrderDto) parameters[0];
        ThreadUtils.sleepSafe(TimeUnit.SECONDS, 5);
        processOrder(processFunction, null, parameters);
        return new AsyncResult<>(orderDto);
    }

    /**
     * Process order
     *
     * @param processFunction the process function to be executed
     * @param parameters      the function parameters
     * @param <T>
     * @param <U>
     * @throws BusinessException the business exception
     */
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public <T, U> void processOrder(Function<T, U> processFunction, Object... parameters) throws BusinessException {
        processOrder(processFunction, null, parameters);
    }
}
