delete from eir_or_tariff;
delete from eir_or_tariff_plan;
delete from EIR_OR_TARIFF_PLAN_MAP;

-- Insert into tariff plan

INSERT INTO eir_or_tariff_plan (code,description,offer_id) select '95_Usage_DSL','95th Percentile usage per port for DSL', id from cat_offer_template where code='EIR_MUB' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select '95_Usage_PURE','95th Percentile usage per port for Pure (WL)(Fibre)', id from cat_offer_template where code='EIR_MUB' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select '95_Usage_RAP','95th Percentile usage per port for RAP (Fibre)', id from cat_offer_template where code='EIR_MUB' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select '95_Usage_THREE','95th Percentile usage per port for Three (WL)(Fibre)', id from cat_offer_template where code='EIR_MUB' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select '95_Usage_WHL','95th Percentile usage per port for White Label (Fibre)', id from cat_offer_template where code='EIR_MUB' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'SAM Pricelist','Meter Usage Billing Pricelist (SAM)', id from cat_offer_template where code='EIR_MUB' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'WHITE LABEL pricelist','Meter Usage Billing Pricelist (White Label)', id from cat_offer_template where code='EIR_MUB' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'CGA Bitstream','Current Generation Access Bitstream', id from cat_offer_template where code='CGA Bitstream' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'CGA Bitstream Pure','Current Generation Access Bitstream for Pure', id from cat_offer_template where code='CGA Bitstream' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'CGA Bitstream O2','Current Generation Access Bitstream for O2', id from cat_offer_template where code='CGA Bitstream' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'CGA Bitstream IFA','Current Generation Access Bitstream for IFA', id from cat_offer_template where code='CGA Bitstream' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'Line Share ','Line Share ', id from cat_offer_template where code='Line Share' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'NGA BP FTC','Next Generation Access Bitstream Plus Fibre to the Cabinet', id from cat_offer_template where code='NGA BP FTC' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'NGA BP FTC PURE','Next Generation Access Bitstream Plus Fibre to the Cabinet - Pure', id from cat_offer_template where code='NGA BP FTC' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'NGA BP FTC O2','Next Generation Access Bitstream Plus Fibre to the Cabinet - O2', id from cat_offer_template where code='NGA BP FTC' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'NGA BP FTC IFA','Next Generation Access Bitstream Plus Fibre to the Cabinet - IFA', id from cat_offer_template where code='NGA BP FTC' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'NGA BP FTH','Next Generation Access Bitstream Plus Fibre to the Home', id from cat_offer_template where code='NGA BP FTH' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'NGA BP FTH PURE','Next Generation Access Bitstream Plus Fibre to the Home - Pure', id from cat_offer_template where code='NGA BP FTH' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'NGA BP FTH 02','Next Generation Access Bitstream Plus Fibre to the Home - O2', id from cat_offer_template where code='NGA BP FTH' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'NGA BP FTH IFA','Next Generation Access Bitstream Plus Fibre to the Home - IFA', id from cat_offer_template where code='NGA BP FTH' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'NGA VUA FTC','Next Generation Access Virtual Unbundled Access Fibre to the Cabinet', id from cat_offer_template where code='NGA VUA FTC' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'NGA VUA FTH','Next Generation Access Virtual Unbundled Access Fibre to the Home', id from cat_offer_template where code='NGA VUA FTH' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'Per VC','Bitstream Virtual Circuits', id from cat_offer_template where code='Per VC' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'ULMP','Un-bundled Local Metallic Path ', id from cat_offer_template where code='ULMP' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'VEA','Very high DSL Ethernet Access Service', id from cat_offer_template where code='VEA' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'VUA','Virtual Unbundled Access ', id from cat_offer_template where code='VUA' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'WLR','Wholesale Line Rental', id from cat_offer_template where code='WLR' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'OMS Plan 1','OMS Tariff 1', id from cat_offer_template where code='OMSOFFER' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'OMS Plan 2','OMS Tariff 2', id from cat_offer_template where code='OMSOFFER' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'RWO','Repayment Works Orders', id from cat_offer_template where code='MISC' limit 1;
INSERT INTO eir_or_tariff_plan (code,description,offer_id) select 'AGENCY','Other Miscellanous Charges', id from cat_offer_template where code='MISC' limit 1;

-- insert into tariff plan map
insert into EIR_OR_TARIFF_PLAN_MAP (offer_id, tariff_plan_id, valid_from) select *, date '2018-01-28' from (select  distinct offer_id, first_value(id)  over (partition by offer_id) from eir_or_tariff_plan) a;


-- insert into tariffs

delete from eir_or_tariff;

insert into eir_or_tariff (tariff_plan_id, offer_id, service_id,charge_type, description, action_qualifier, rating_scheme,
						  bandwidth, transmission_ind, commit_period_ind, zone_a_ind, zone_b_ind, region, sla_ind, service_category,
						  analogue_quality, analogue_rate,cpe_type, cpe_description_ind, ets_type_ind, ind_1, ind_2,ind_3,
						  ind_4, ind_5, ind_6, ind_7, ind_8, ind_9, ind_10, distance_from, distance_to, distance_unit, quantity_from, 
						   quantity_to, quantity_unit, max_rated_quantity, price_plan_ind, density, class_of_service, sla, ef, af, 
						   exchange_class, valid_from, valid_to, tax_class, average_speed, invoice_subcat_id, rec_calendar, price, 
						   accounting_code_id, filename, updated, updated_by)
select tm.tariff_plan_id, st.offer_template_id, st.service_template_id, 'O', 'Service '||st.service_template_id|| ' one shot charge',
'%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%',
null, null, null, null, null, null, null,'%','%','%','%','%','%','%', date '2018-01-28', null, tx.id, '%',ivsc.id,null,'5',ac.id,'test', now(), 'me'
from eir_or_tariff_plan_map tm, cat_offer_serv_templates st, billing_tax_class tx, billing_invoice_sub_cat ivsc,
billing_accounting_code ac
where tm.offer_id = st.offer_template_id
and tx.code='A1' and ivsc.code='ISCAT_ONESHOT' and ac.code='401000000' ;

insert into eir_or_tariff (tariff_plan_id, offer_id, service_id,charge_type, description, action_qualifier, rating_scheme,
						  bandwidth, transmission_ind, commit_period_ind, zone_a_ind, zone_b_ind, region, sla_ind, service_category,
						  analogue_quality, analogue_rate,cpe_type, cpe_description_ind, ets_type_ind, ind_1, ind_2,ind_3,
						  ind_4, ind_5, ind_6, ind_7, ind_8, ind_9, ind_10, distance_from, distance_to, distance_unit, quantity_from, 
						   quantity_to, quantity_unit, max_rated_quantity, price_plan_ind, density, class_of_service, sla, ef, af, 
						   exchange_class, valid_from, valid_to, tax_class, average_speed, invoice_subcat_id, rec_calendar, price, 
						   accounting_code_id, filename, updated, updated_by)
select tm.tariff_plan_id, st.offer_template_id, st.service_template_id, 'R', 'Service '||st.service_template_id|| ' recurring shot charge',
'%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%','%',
null, null, null, null, null, null, null,'%','%','%','%','%','%','%', date '2018-01-28', null, tx.id, '%',ivsc.id,cal.id,'5',ac.id,'test', now(), 'me'
from eir_or_tariff_plan_map tm, cat_offer_serv_templates st, billing_tax_class tx, billing_invoice_sub_cat ivsc,
billing_accounting_code ac, cat_calendar cal
where tm.offer_id = st.offer_template_id
and tx.code='A1' and ivsc.code='ISCAT_RECURRING' and ac.code='445510000' and cal.code='CAL_MONTHLY_1ST';







