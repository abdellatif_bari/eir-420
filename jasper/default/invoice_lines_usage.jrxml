<?xml version="1.0" encoding="UTF-8"?>
<jasperReport xmlns="http://jasperreports.sourceforge.net/jasperreports" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://jasperreports.sourceforge.net/jasperreports http://jasperreports.sourceforge.net/xsd/jasperreport.xsd" name="invoice_lines_usage" language="groovy" pageWidth="555" pageHeight="842" columnWidth="555" leftMargin="0" rightMargin="0" topMargin="0" bottomMargin="0" uuid="65a19249-db8f-4f97-9625-349e1a11cbe5">
	<property name="ireport.zoom" value="1.5"/>
	<property name="ireport.x" value="0"/>
	<property name="ireport.y" value="0"/>
	<parameter name="messagePath" class="java.lang.String" isForPrompting="false">
		<defaultValueExpression><![CDATA["./"]]></defaultValueExpression>
	</parameter>
	<parameter name="categoryInvoiceAggregateIndex" class="java.lang.Integer"/>
	<parameter name="subCategoryIndex" class="java.lang.Integer"/>
	<parameter name="categoryIndex" class="java.lang.Integer"/>
	<parameter name="userAccountIndex" class="java.lang.Integer"/>
	<queryString language="xPath">
		<![CDATA[/invoice/detail/userAccounts/userAccount[$P{userAccountIndex}]/categoryInvoiceAggregates/categoryInvoiceAggregate[$P{categoryInvoiceAggregateIndex}]/categories/category[$P{categoryIndex}]/subCategories/subCategory[$P{subCategoryIndex}]/line]]>
	</queryString>
	<field name="label" class="java.lang.String">
		<fieldDescription><![CDATA[label]]></fieldDescription>
	</field>
	<field name="periodStartDate" class="java.lang.String">
		<fieldDescription><![CDATA[@periodStartDate]]></fieldDescription>
	</field>
	<field name="periodEndDate" class="java.lang.String">
		<fieldDescription><![CDATA[@periodEndDate]]></fieldDescription>
	</field>
	<field name="usageDate" class="java.lang.String">
		<fieldDescription><![CDATA[usageDate]]></fieldDescription>
	</field>
	<field name="calls" class="java.lang.String">
		<fieldDescription><![CDATA[calls]]></fieldDescription>
	</field>
	<field name="quantity" class="java.lang.String">
		<fieldDescription><![CDATA[quantity]]></fieldDescription>
	</field>
	<field name="taxPercent" class="java.lang.String">
		<fieldDescription><![CDATA[@taxPercent]]></fieldDescription>
	</field>
	<field name="amountWithoutTax" class="java.lang.String">
		<fieldDescription><![CDATA[amountWithoutTax]]></fieldDescription>
	</field>
	<field name="totalAmountWithoutTax" class="java.lang.String">
		<fieldDescription><![CDATA[totalAmountWithoutTax]]></fieldDescription>
	</field>
	<field name="subCategoryLabel" class="java.lang.String">
		<fieldDescription><![CDATA[ancestor::subCategory/@label]]></fieldDescription>
	</field>
	<field name="categoryLabel" class="java.lang.String">
		<fieldDescription><![CDATA[ancestor::categories/category/@label]]></fieldDescription>
	</field>
	<field name="categoryAmountWithoutTax" class="java.lang.String">
		<fieldDescription><![CDATA[ancestor::categories/category/@amountWithoutTax]]></fieldDescription>
	</field>
	<background>
		<band splitType="Stretch"/>
	</background>
	<title>
		<band splitType="Stretch"/>
	</title>
	<pageHeader>
		<band splitType="Stretch"/>
	</pageHeader>
	<columnHeader>
		<band splitType="Stretch"/>
	</columnHeader>
	<detail>
		<band height="15" splitType="Stretch">
			<textField isBlankWhenNull="true">
				<reportElement x="23" y="0" width="132" height="15" uuid="1200a407-9e08-4c99-9131-19b8df8ce763"/>
				<textElement verticalAlignment="Middle">
					<font fontName="eir-Light" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{subCategoryLabel}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="155" y="0" width="75" height="15" uuid="ca381010-f7ee-431a-a99a-817d049b567d"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="eir-Light" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{periodStartDate} != null && !$F{periodStartDate}.isEmpty() ? $F{periodStartDate} : $F{usageDate}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="229" y="0" width="75" height="15" uuid="e4180843-a908-4c5a-80c1-b8df3ae7e985"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="eir-Light" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{periodEndDate}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement x="305" y="0" width="51" height="15" uuid="9812f63f-a514-425c-95aa-98c137298fb3"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="eir-Light" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{calls}]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement x="355" y="0" width="51" height="15" uuid="761376cb-2a16-4c66-ae80-a27839196071"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="eir-Light" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{quantity}]]></textFieldExpression>
			</textField>
			<textField isBlankWhenNull="true">
				<reportElement x="405" y="0" width="75" height="15" uuid="f2eeb94d-9bbd-423e-86e4-8c962c861eaa"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="eir-Light" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{taxPercent} ? $F{taxPercent} + "%" : ""]]></textFieldExpression>
			</textField>
			<textField pattern="" isBlankWhenNull="true">
				<reportElement x="457" y="0" width="98" height="15" uuid="21480657-edb5-4c9a-a416-923f07591a20"/>
				<textElement textAlignment="Center" verticalAlignment="Middle">
					<font fontName="eir-Light" size="8"/>
				</textElement>
				<textFieldExpression><![CDATA[$F{totalAmountWithoutTax}]]></textFieldExpression>
			</textField>
		</band>
	</detail>
	<columnFooter>
		<band splitType="Stretch"/>
	</columnFooter>
	<pageFooter>
		<band splitType="Stretch"/>
	</pageFooter>
	<summary>
		<band splitType="Stretch"/>
	</summary>
</jasperReport>
