package com.eir.script;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.ParamBeanFactory;
import org.meveo.commons.utils.PersistenceUtils;
import org.meveo.commons.utils.QueryBuilder;
import org.meveo.model.IEnable;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.InstanceStatusEnum;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.script.Script;

/**
 * The Class MubTrafficVolumeScript. PO2-288
 */
public class MubTrafficVolumeScript extends Script {

	private static final long serialVersionUID = 1L;

	/**
	 * service type Enum
	 */
	public enum ServiceTypeEnum {
		MAIN("Main"), ANCILLARY("Ancillary");

		private String label;

		ServiceTypeEnum(String label) {
			this.label = label;
		}

		public String getLabel() {
			return this.label;
		}
	}

	/** The Constant DATETIME_FORMAT. */
	private static final String DATETIME_FORMAT = "ddMMyyyyHHmmss";

	/** The param bean factory. */
	private ParamBeanFactory paramBeanFactory = (ParamBeanFactory) getServiceInterface("ParamBeanFactory");

	/** The subscription service. */
	private SubscriptionService subscriptionService = (SubscriptionService) getServiceInterface(
			SubscriptionService.class.getSimpleName());

	/** The NumberFormat. */
	private NumberFormat fmt = NumberFormat.getInstance(Locale.ENGLISH);

	/**
	 * script main execute method.
	 *
	 * @param methodContext method context params
	 * @throws BusinessException business exception
	 */
	@Override
	public void execute(Map<String, Object> methodContext) throws BusinessException {
		log.info("########## Starting Mub traffic volume script ... ");
		fmt.setGroupingUsed(false);
		fmt.setMaximumIntegerDigits(999);
		fmt.setMaximumFractionDigits(999);
		try {
			List<Subscription> subscriptions = findMubCircuitIds();
			if (subscriptions != null) {
				long startEpocTime = getMubStartEpocTime();
				long endEpocTime = getMubEndEpocTime();
				String mubMounth = getMubMounth();
//        		Date endEpocDate = new Date(endEpocTime);
				log.info("########## startEpocTime ... " + startEpocTime);
				log.info("########## endEpocTime ... " + endEpocTime);
				log.info("########## mubMounth ... " + mubMounth);
				log.info("########## subscriptions ... " + subscriptions.size());				
				Double trafficVolume;
				String trafficFolder = (String) methodContext.get("TRAFFIC_VOLUME_FOLDER");
				if (trafficFolder == null) {
					trafficFolder = "imports/MUB/traffic/input";
				}
				trafficFolder = paramBeanFactory.getChrootDir() + File.separator + trafficFolder;
				File f = new File(trafficFolder);
				if (!f.exists()) {
					f.mkdirs();
					log.info("Traffic folder created :" + trafficFolder);
				}
				String trafficFileName = trafficFolder + File.separator + "Traffic_Volume_" + mubMounth + "_"
						+ DateUtils.formatDateWithPattern(new Date(), DATETIME_FORMAT) + ".csv";
				String separator = ",";
				FileWriter fw = null;
				BufferedWriter bw = null;
				// Subscription subscription;
				for (Subscription subscription : subscriptions) {
					Map<String, String> cfMubFinalUsage = (Map<String, String>) subscription.getCfValuesNullSafe()
							.getValue("MUB_FINAL_USAGE");
					log.info("subscription :" + subscription.getCode());
					trafficVolume = getTrafficVolume(cfMubFinalUsage, startEpocTime, endEpocTime);
					log.info("########## trafficVolume ... " + trafficVolume);
					if (trafficVolume != null) {
						if (!(new File(trafficFileName)).exists()) {
							fw = new FileWriter(trafficFileName, true);
							bw = new BufferedWriter(fw);
						}
						bw.append(subscription.getUserAccount().getBillingAccount().getCode() + separator
								+ subscription.getCode() + separator + fmt.format(trafficVolume) + separator
								+ getBillCode(subscription) + separator + getRegion(subscription) + separator + mubMounth + "\r\n");
					}
				}
				if (bw != null) {
					bw.close();
				}
			}

		} catch (Exception e) {
			log.error(e.getMessage());
			throw new BusinessException(e.getMessage());
		}

	}

	/**
	 * Gets the region.
	 *
	 * @param subscription the subscription
	 * @return the region
	 */
	private String getRegion(Subscription subscription) {
		String mapSeparator = "|";
		for (ServiceInstance service : subscription.getServiceInstances()) {
			Object attributes = service.getCfValuesNullSafe()
					.getValue("CF_BILL_ATTRIBUTE");
			if (attributes != null && ((Map<String, String>)attributes).get("REGION") != null) {
				return ((Map<String, String>)attributes).get("REGION").split(Pattern.quote(mapSeparator))[0];
			}
		}
		return null;
	}

	/**
	 * Gets the bill code.
	 *
	 * @param subscription the subscription
	 * @return the bill code
	 */
	private String getBillCode(Subscription subscription) {
		OfferTemplate offer = PersistenceUtils.initializeAndUnproxy(subscription.getOffer());
		Map<String, String> tariffTable = (Map<String, String>) offer.getCfValuesNullSafe().getValue("CATALOG",
				subscription.getSubscriptionDate());

		if (tariffTable == null) {
			return null;
		}
		for (ServiceInstance service : subscription.getServiceInstances()) {
			if (tariffTable.get(service.getCode()) != null
					&& ServiceTypeEnum.MAIN.getLabel()
							.equalsIgnoreCase(tariffTable.get(service.getCode()).split(Pattern.quote("|"))[1])
					&& InstanceStatusEnum.ACTIVE.equals(service.getStatus())) {
				return service.getCode();
			}
		}
		return null;
	}

	/**
	 * Gets the traffic volume.
	 *
	 * @param cfMubFinaldUsage the cf mub finald usage
	 * @param startEpocTime    the start epoc time
	 * @param endEpocTime      the end epoc time
	 * @return the traffic volume
	 */
	private Double getTrafficVolume(Map<String, String> cfMubFinaldUsage, long startEpocTime, long endEpocTime) {
		if (cfMubFinaldUsage == null || cfMubFinaldUsage.size() == 0) {
			return null;
		}
		log.info("************** cfMubFinaldUsage : " + cfMubFinaldUsage);
		log.info("************** startEpocTime : " + startEpocTime);
		log.info("************** endEpocTime : " + endEpocTime);
		// filter the map to have only values between start and end epoc time
		Map<String, Double> collect = cfMubFinaldUsage.entrySet().stream().filter(
				map -> (Long.valueOf(map.getKey()) >= startEpocTime && Long.valueOf(map.getKey()) <= endEpocTime))
				.collect(Collectors.toMap(p -> p.getKey(),
						(p -> Double.valueOf((p.getValue().split(Pattern.quote("|"))[6]).replace(",", ".")))));

		if (collect != null && collect.size() > 0) {
			return collect.values().stream().mapToDouble(Double::valueOf).sum();
		}
		return null;
	}

	/**
	 * Find mub circuit ids.
	 *
	 * @return the list of subscriptions that contain MUB_FINAL_USAGE as CF
	 */
	@SuppressWarnings("unchecked")
	private List<Subscription> findMubCircuitIds() {
		QueryBuilder queryBuilder = new QueryBuilder(Subscription.class, "a", null);
		if (IEnable.class.isAssignableFrom(Subscription.class)) {
			queryBuilder.addBooleanCriterion("disabled", false);
		}
		queryBuilder.addCriterion("cf_values", "like", "%MUB_FINAL_USAGE%", true);
		return queryBuilder.getQuery(subscriptionService.getEntityManager()).getResultList();
	}

	/**
	 * Gets the mub start date.
	 *
	 * @return the mub start date
	 */
	private long getMubStartEpocTime() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
		calendar.set(Calendar.DATE, 1);
		return DateUtils.setDateToStartOfDay(calendar.getTime()).getTime() / 1000;
	}

	/**
	 * Gets the mub end date.
	 *
	 * @return the mub end date
	 */
	private long getMubEndEpocTime() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH) - 1);
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
		return DateUtils.setDateToEndOfDay(calendar.getTime()).getTime() / 1000;
	}

	private static String getMubMounth() {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		int mounth = calendar.get(Calendar.MONTH);
		String sMounth = mounth < 10 ? "0" + mounth : "" + mounth;
		return String.valueOf(calendar.get(Calendar.YEAR)) + sMounth;
	}

}
