package com.eir.script;

import java.io.Serializable;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.apache.commons.collections4.MapUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.notification.InboundRequest;
import org.meveo.model.order.OrderStatusEnum;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.script.Script;
import org.meveo.service.script.ScriptInstanceService;
import org.meveo.service.script.ScriptInterface;

/**
 *
 * @author Hatim OUDAD
 *
 */
public class UGOrderScript extends Script {

    private ScriptInstanceService scriptInstanceService = (ScriptInstanceService) getServiceInterface("ScriptInstanceService");

    /**
     * Emitter type.
     */
    private static final String OMS_EMITTER = "OMS";

    /**
     * UG Emitter.
     */
    private static final String UG_EMITTER = "UG";

    /**
     * Tariff attribute names Enum
     */
    public enum TariffAttributeNamesEnum {
        DENSITY, REGION, SLA, CLASS_SERVICE, EF, AF, EXCHANGE
    }

    /**
     * Date time pattern
     */
    public static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

    private OrderDTO orderDTO;

    @Override
    public void execute(Map<String, Object> initContext) {
        try {
            Map<String, Object> orderGroup = (Map<String, Object>) initContext.get("record");

            if (MapUtils.isEmpty(orderGroup)) {
                throw new BusinessException(String.format("Parameter record is missing"));
            }

            orderDTO = mapRecordFields(orderGroup);

            // executing order
            executeOrder();

        } catch (Exception e) {
            log.error("error on process bulck order ", e.getMessage(), e);
            if (e instanceof BusinessException) {
                throw e;
            } else {
                // wrap the exception in a business exception and throwing it
                throw new BusinessException(e);
            }
        }

    }

    /**
     * Map record fields
     *
     * @param orderGroup the order group
     */
    private OrderDTO mapRecordFields(Map<String, Object> orderGroup) {

        OrderDTO orderDto = new OrderDTO();

        Map<String, Object> wbRecord = (Map<String, Object>) orderGroup.get("wb_record");

        // Order
        orderDto.transactionId = (String) wbRecord.get("Transaction_Id");
        orderDto.orderingSystem = (String) wbRecord.get("Ordering_System");
        orderDto.orderId = (String) wbRecord.get("Order_Id");
        orderDto.refOrderId = (String) wbRecord.get("Ref_Order_Id");

        // bill Item
        List<Map<String, Object>> itemsGroup = (List<Map<String, Object>>) orderGroup.get("items_group");

        for (Map<String, Object> itemGroup : itemsGroup) {
            List<Map<String, Object>> billItems = (ArrayList<Map<String, Object>>) itemGroup.get("bill_item");
            List<Map<String, Object>> attributes = (ArrayList<Map<String, Object>>) itemGroup.get("attribute_record");
            if (billItems != null && !billItems.isEmpty()) {
                for (Map<String, Object> billItem : billItems) {

                    OrderLineDTO orderLineDto = new OrderLineDTO();

                    orderLineDto.itemId = StringUtils.isBlank(billItem.get("Item_Id")) ? null : (String) billItem.get("Item_Id");
                    orderLineDto.operatorId = StringUtils.isBlank(billItem.get("Operator_Id")) ? null : (String) billItem.get("Operator_Id");
                    orderLineDto.operatorOrderNumber = StringUtils.isBlank(billItem.get("Operator_Order_Number")) ? null : (String) billItem.get("Operator_Order_Number");
                    orderLineDto.itemComponentId = StringUtils.isBlank(billItem.get("Item_Component_Id")) ? null : (String) billItem.get("Item_Component_Id");
                    orderLineDto.itemVersion = StringUtils.isBlank(billItem.get("Item_Version")) ? null : (String) billItem.get("Item_Version");
                    orderLineDto.itemType = StringUtils.isBlank(billItem.get("Item_Type")) ? null : (String) billItem.get("Item_Type");
                    orderLineDto.agentCode = StringUtils.isBlank(billItem.get("Agent_Code")) ? null : (String) billItem.get("Agent_Code");
                    orderLineDto.uan = StringUtils.isBlank(billItem.get("UAN")) ? null : (String) billItem.get("UAN");
                    orderLineDto.masterAccountNumber = StringUtils.isBlank(billItem.get("Master_Account_Number")) ? null : (String) billItem.get("Master_Account_Number");
                    orderLineDto.serviceId = StringUtils.isBlank(billItem.get("Service_Id")) ? null : (String) billItem.get("Service_Id");
                    orderLineDto.refServiceId = StringUtils.isBlank(billItem.get("Ref_Service_Id")) ? null : (String) billItem.get("Ref_Service_Id");
                    orderLineDto.xRefServiceId = StringUtils.isBlank(billItem.get("Xref_Service_Id")) ? null : (String) billItem.get("Xref_Service_Id");
                    orderLineDto.billCode = StringUtils.isBlank(billItem.get("Bill_Code")) ? null : (String) billItem.get("Bill_Code");
                    orderLineDto.billAction = StringUtils.isBlank(billItem.get("Action")) ? null : (String) billItem.get("Action");
                    orderLineDto.billActionQualifier = StringUtils.isBlank(billItem.get("Action_Qualifier")) ? null : (String) billItem.get("Action_Qualifier");
                    orderLineDto.billQuantity = StringUtils.isBlank(billItem.get("Quantity")) ? null : (String) billItem.get("Quantity");
                    orderLineDto.billEffectDate = DateUtils.parseDateWithPattern(StringUtils.isBlank(billItem.get("Effect_Date")) ? null : (String) billItem.get("Effect_Date"),
                        DATE_TIME_PATTERN);

                    // Attribute
                    if (attributes != null) {
                        for (Map<String, Object> attribute : attributes) {

                            AttributeDTO attributeDto = new AttributeDTO();

                            attributeDto.code = StringUtils.isBlank(attribute.get("Code")) ? null : (String) attribute.get("Code");
                            attributeDto.value = StringUtils.isBlank(attribute.get("Value")) ? null : (String) attribute.get("Value");
                            attributeDto.unit = StringUtils.isBlank(attribute.get("Unit")) ? null : (String) attribute.get("Unit");
                            attributeDto.componentId = StringUtils.isBlank(attribute.get("Component_Id")) ? null : (String) attribute.get("Component_Id");
                            attributeDto.attributeType = StringUtils.isBlank(attribute.get("Attribute_Type")) ? null : (String) attribute.get("Attribute_Type");
                            log.info("attributeDto", attributeDto.code);
                            orderLineDto.attributes.add(attributeDto);
                        }
                    }
                    orderDto.orderLines.add(orderLineDto);
                }
            }
        }

        return orderDto;
    }

    /**
     * Get XML from DTO.
     *
     * @param object the object to be converted to xml
     * @param <T> the generic type of object to be converted to xml
     * @return the xml
     * @throws BusinessException the business exception
     */
    private <T> String getXmlFromDTO(T object) throws BusinessException {
        String xmlContent = null;
        try {
            // Create JAXB Context
            JAXBContext jaxbContext = JAXBContext.newInstance((Class<T>) object.getClass());

            // Create Marshaller
            Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

            // Required formatting??
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

            // Print XML String to Console
            StringWriter stringWriter = new StringWriter();

            // Write XML to StringWriter
            jaxbMarshaller.marshal(object, stringWriter);

            // get XML Content
            xmlContent = stringWriter.toString();
        } catch (JAXBException e) {
            log.error("Convert DTO to xml fail", e);
            throw new BusinessException(e.getMessage(), e);
        }
        return xmlContent;
    }

    /**
     * Execute the corresponding order
     *
     * @throws BusinessException the business exception
     */
    private void executeOrder() throws BusinessException {

        String orderXml = getXmlFromDTO(orderDTO);
        log.info(">>> XML: " + orderXml);
        if (OMS_EMITTER.equalsIgnoreCase(orderDTO.orderingSystem)) {
            executeScript("com.eir.script.PlaceOMSOrderScript", orderXml);
        } else if (UG_EMITTER.equalsIgnoreCase(orderDTO.orderingSystem)) {
            if (isNewTariff()) {
                executeScript("com.eir.script.NewPlaceOrderScript", orderXml);
            } else {
                executeScript("com.eir.script.PlaceOrderScript", orderXml);
            }
        } else {
            throw new BusinessException("'INVALID_VALUE' : The Ordering_System parameter contains invalid value");
        }
    }

    /**
     * Determine is new tariff
     *
     * @return true is new tariff, false is not.
     */
    private boolean isNewTariff() {
        for (OrderLineDTO orderLineDTO : orderDTO.orderLines) {
            String actionQualifier = orderLineDTO.billActionQualifier;

            String density = orderLineDTO.getAttribute(TariffAttributeNamesEnum.DENSITY.name());
            String region = orderLineDTO.getAttribute(TariffAttributeNamesEnum.REGION.name());
            String sla = orderLineDTO.getAttribute(TariffAttributeNamesEnum.SLA.name());
            String classService = orderLineDTO.getAttribute(TariffAttributeNamesEnum.CLASS_SERVICE.name());
            String ef = orderLineDTO.getAttribute(TariffAttributeNamesEnum.EF.name());
            String af = orderLineDTO.getAttribute(TariffAttributeNamesEnum.AF.name());

            if (!StringUtils.isBlank(actionQualifier) || !StringUtils.isBlank(density) || !StringUtils.isBlank(region) || !StringUtils.isBlank(sla)
                    || !StringUtils.isBlank(classService) || !StringUtils.isBlank(ef) || !StringUtils.isBlank(af)) {
                return true;
            }
        }
        return false;
    }

    public void executeScript(String scriptCode, String content) throws BusinessException {
        HashMap<String, Object> context = new HashMap<>();
        ScriptInterface script = scriptInstanceService.getScriptInstance(scriptCode);
        context.put("body", content);
        context.put("source", "csv");
        script.execute(context);
        if (context.get("event") != null) {
            InboundRequest inboundRequest = (InboundRequest) context.get("event");
            if (inboundRequest.getResponseStatus() != HttpURLConnection.HTTP_OK) {
                throw new BusinessException(inboundRequest.getResponseBody());
            }
        }
    }
}

/**
 * Formatted Order Object (Element tag names use InitCaps with underscores)
 *
 *
 */
@XmlRootElement(name = "Wholesale_Billing")
@XmlAccessorType(XmlAccessType.FIELD)
class OrderDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -5583104371622083869L;

    /**
     * The ordering system.
     */
    @XmlElement(name = "Ordering_System", required = true)
    String orderingSystem;

    /**
     * The order id.
     */
    @XmlElement(name = "Order_Id", required = true)
    String orderId;

    /**
     * The ref order id.
     */
    @XmlElement(name = "Ref_Order_Id", required = true)
    String refOrderId;

    /**
     * The transaction id.
     */
    @XmlElement(name = "Transaction_Id", required = true)
    String transactionId;

    /**
     * The status message.
     */
    @XmlElement(name = "Status_Message")
    String statusMessage;

    /**
     * The bill items.
     */
    @XmlElement(name = "Bill_Item")
    List<OrderLineDTO> orderLines = new ArrayList<>();

    @XmlTransient
    OrderStatusEnum status;

    @XmlTransient
    String errorMessage;

    public OrderDTO() {
    }

}

/**
 * Formatted Order Line Object
 *
 *
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
class OrderLineDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -5557326416352192100L;

    /**
     * The item id.
     */
    @XmlElement(name = "Item_Id", required = true)
    String itemId;

    /**
     * The operator id.
     */
    @XmlElement(name = "Operator_Id", required = true)
    String operatorId;

    /**
     * The operator id.
     */
    @XmlElement(name = "Item_Component_Id", required = true)
    String itemComponentId;

    /**
     * The item version.
     */
    @XmlElement(name = "Item_Version")
    String itemVersion;

    /**
     * The item type.
     */
    @XmlElement(name = "Item_Type")
    String itemType;

    /**
     * The agent code.
     */
    @XmlElement(name = "Agent_Code")
    String agentCode;

    /**
     * The uan.
     */
    @XmlElement(name = "UAN")
    String uan;

    /**
     * The master account number.
     */
    @XmlElement(name = "Master_Account_Number")
    String masterAccountNumber;

    /**
     * The service id.
     */
    @XmlElement(name = "Service_Id")
    String serviceId;

    /**
     * The ref service id.
     */
    @XmlElement(name = "Ref_Service_Id")
    String refServiceId;

    /**
     * The xref service id.
     */
    @XmlElement(name = "Xref_Service_Id")
    String xRefServiceId;

    /**
     * The bill code.
     */
    @XmlElement(name = "Bill_Code", required = true)
    String billCode;

    /**
     * The action.
     */
    @XmlElement(name = "Action", required = true)
    String billAction;

    /**
     * The action qualifier.
     */
    @XmlElement(name = "Action_Qualifier")
    String billActionQualifier;

    /**
     * The quantity.
     */
    @XmlElement(name = "Quantity", required = true)
    String billQuantity;

    /**
     * The effect date.
     */
    @XmlElement(name = "Effect_Date", required = true)
    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    Date billEffectDate;

    /**
     * The operator order number.
     */
    @XmlElement(name = "Operator_Order_Number")
    String operatorOrderNumber;

    /**
     * The attributes list.
     */
    @XmlElement(name = "Attribute")
    List<AttributeDTO> attributes = new ArrayList<>();

    @XmlTransient
    Map<String, String> attributesMap;

    public OrderLineDTO() {
    }

    /**
     * Get the order line attribute by code
     *
     * @param code the attribute code
     * @return the order line attribute by code
     */
    public String getAttribute(String code) {
        if (attributesMap == null) {
            attributesMap = new HashMap<>();
            for (AttributeDTO attributeDTO : attributes) {
                attributesMap.put(attributeDTO.code, attributeDTO.value);
            }
        }
        return attributesMap.get(code);
    }

}

/**
 * Formatted Order Line Attributes Object
 *
 *
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
class AttributeDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7076269761412221410L;

    /**
     * The code.
     */
    @XmlElement(name = "Code", required = true)
    String code;

    /**
     * The value.
     */
    @XmlElement(name = "Value", required = true)
    String value;

    /**
     * The unit.
     */
    @XmlElement(name = "Unit")
    String unit;

    /**
     * The component id.
     */
    @XmlElement(name = "Component_Id")
    String componentId;

    /**
     * The attribute type.
     */
    @XmlElement(name = "Attribute_Type")
    String attributeType;

    public AttributeDTO() {
    }

}

/**
 * This is Adaptor class which has main responsibility to convert from java.util.Date to format string of date.
 *
 *
 */
class DateTimeAdapter extends XmlAdapter<String, Date> {

    @Override
    public Date unmarshal(String xml) {
        return DateUtils.parseDateWithPattern(xml, UGOrderScript.DATE_TIME_PATTERN);
    }

    @Override
    public String marshal(Date object) {
        return DateUtils.formatDateWithPattern(object, UGOrderScript.DATE_TIME_PATTERN);
    }
}
