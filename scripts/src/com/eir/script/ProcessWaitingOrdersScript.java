package com.eir.script;

import java.io.Serializable;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.EjbUtils;
import org.meveo.commons.utils.QueryBuilder;
import org.meveo.model.ICustomFieldEntity;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.SubscriptionStatusEnum;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.crm.custom.CustomFieldTypeEnum;
import org.meveo.model.crm.custom.CustomFieldValue;
import org.meveo.model.order.Order;
import org.meveo.model.order.OrderStatusEnum;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.crm.impl.CustomFieldTemplateService;
import org.meveo.service.order.OrderService;
import org.meveo.service.script.Script;
import org.meveo.service.script.ScriptInstanceService;
import org.meveo.service.script.ScriptInterface;

public class ProcessWaitingOrdersScript extends Script {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// API Services
	private OrderService orderService = (OrderService) getServiceInterface(OrderService.class.getSimpleName());
	private SubscriptionService subscriptionService = (SubscriptionService) getServiceInterface(
			SubscriptionService.class.getSimpleName());
	private ScriptInstanceService scriptInstanceService = (ScriptInstanceService) EjbUtils
			.getServiceInterface(ScriptInstanceService.class.getSimpleName());
	private CustomFieldTemplateService customFieldTemplateService = (CustomFieldTemplateService) getServiceInterface(
			"CustomFieldTemplateService");

	public static final String REPROCESS_ORDER = "REPROCESS_ORDER";
	private static final String RELEASE_ORDER = "RELEASE_ORDER";

	/**
	 * Date time pattern
	 */
	public static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

	/**
	 * Entry Point for script
	 */
	@Override
	public void execute(Map<String, Object> context) throws BusinessException {

		log.info("******Starting of script ProcessWaitingOrdersScript******");
		try {
			List<Order> waitingOrders = getWaitingOrders();
			int nbWarn = 0;
			int nbOK = 0;
			String report = "";
			if (waitingOrders != null) {
				OrderDTO dto;
				List<String> matchedWaitingOrders;
				List<Subscription> activeSubscriptions;
				;
				Subscription subscription;
				boolean areAllSubscriptionsActive = true;
				Date latestSubscriptionDate;
				for (Order order : waitingOrders) {
					dto = getOrderDTO(order);
					List<String> assoccircidList = getAssoccircidAttribute(dto);
					areAllSubscriptionsActive = true;
					activeSubscriptions = new ArrayList<Subscription>();
					matchedWaitingOrders = new ArrayList<String>();
					latestSubscriptionDate = null;
					for (String assoccirId : assoccircidList) {
						subscription = subscriptionService.findByCode(assoccirId);
						if ((subscription == null || subscription.getStatus() != SubscriptionStatusEnum.ACTIVE)) {
							areAllSubscriptionsActive = false;
						} else {
							latestSubscriptionDate = (isNullDate(latestSubscriptionDate)
									|| (!isNullDate(subscription.getSubscriptionDate())
											&& subscription.getSubscriptionDate().after(latestSubscriptionDate)))
													? subscription.getSubscriptionDate()
													: latestSubscriptionDate;
							activeSubscriptions.add(subscription);
						}						
						List<String> waitingOrdersCodes = findWaitingOrders(order.getCode(), assoccirId);
						if(waitingOrdersCodes != null && waitingOrdersCodes.size() > 0) {
							matchedWaitingOrders.addAll(waitingOrdersCodes);
						}						
					}
					
					log.info("areAllSubscriptionsActive :"+areAllSubscriptionsActive);
					log.info("matchedWaitingOrders.size() :"+matchedWaitingOrders.size());
					log.info("activeSubscriptions.size() :"+activeSubscriptions.size());
					// If the three previous conditions were not verified, keep the order in waiting
					// status.
					if (!areAllSubscriptionsActive && matchedWaitingOrders.size() == 0) {
						nbWarn++;
						report.concat("Warning Order : " + order.getCode() + "\n");
					} else if (areAllSubscriptionsActive) { // extract "Subscription date" latest date from found
															// subscriptions.
						updateEffectiveDateAndReprocess(order, latestSubscriptionDate);
						nbOK++;
					} else if (activeSubscriptions.size() == 0 && matchedWaitingOrders.size() > 0) {
						Date lastEffectiveDate = getLastEffectiveDate(order,matchedWaitingOrders);
						updateEffectiveDate(matchedWaitingOrders, lastEffectiveDate);
						updateEffectiveDateAndReprocess(order, lastEffectiveDate);
						nbOK++;
						nbOK += matchedWaitingOrders.size();
					} else if (activeSubscriptions.size() > 0 && matchedWaitingOrders.size() > 0) {
						Date lastEffectiveDate = getLastEffectiveDate(order,matchedWaitingOrders);
						if (isNullDate(lastEffectiveDate) || (!isNullDate(latestSubscriptionDate)
								&& latestSubscriptionDate.after(lastEffectiveDate))) {
							lastEffectiveDate = latestSubscriptionDate;
						}
						updateEffectiveDate(matchedWaitingOrders, lastEffectiveDate);
						updateEffectiveDateAndReprocess(order, lastEffectiveDate);
						nbOK++;
						nbOK += matchedWaitingOrders.size();
					}
				}
				context.put(JOB_RESULT_NB_WARN, nbWarn);
				context.put(JOB_RESULT_NB_OK, nbOK);
				context.put(JOB_RESULT_REPORT, report);
			}
		} catch (Exception e) {
			log.error(e.getMessage());
			throw new BusinessException(e.getMessage());

		} finally {
			log.info("******Ending of script ProcessWaitingOrdersScript******");
		}
	}

	private boolean isNullDate(Date date) {
		return date == null || "".equals(""+date) || "null".equals(""+date);
	}

	/**
	 * Update effective date.
	 *
	 * @param matchedWaitingOrders the matched waiting orders
	 * @param lastEffectiveDate    the last effective date
	 */
	private void updateEffectiveDate(List<String> matchedWaitingOrders, Date lastEffectiveDate) {
		Order order;
		for (String code : matchedWaitingOrders) {
			order = orderService.findByCode(code);
			updateEffectiveDateAndReprocess(order, lastEffectiveDate);
		}

	}

	/**
	 * Gets the last effective date.
	 * @param order0 
	 *
	 * @param matchedWaitingOrders the matched waiting orders
	 * @return the last effective date
	 */
	private Date getLastEffectiveDate(Order order0, List<String> matchedWaitingOrders) {
		Order order;
		Date lastEffectiveDate = getOrderBillEffectDate(order0);
		Date orderEffectDate = null;
		for (String code : matchedWaitingOrders) {
			order = orderService.findByCode(code);
			orderEffectDate = getOrderBillEffectDate(order);
			if (isNullDate(lastEffectiveDate) || (!isNullDate(orderEffectDate) && orderEffectDate.after(lastEffectiveDate))) {
				lastEffectiveDate = orderEffectDate;
			}
		}
		return lastEffectiveDate;
	}

	/**
	 * Get order bill effect date
	 *
	 * @return the order bill effect date
	 */
	private Date getOrderBillEffectDate(Order refOrder) {
		if (refOrder != null) {
			String orderMessage = (String) refOrder.getCfValuesNullSafe()
					.getValue("ORDER_MESSAGE");
			
			Pattern pattern = Pattern.compile("<Effect_Date>(.*?)</Effect_Date>", Pattern.DOTALL);
			Matcher matcher = pattern.matcher(orderMessage);
			if (matcher.find()) {
				return DateUtils.parseDateWithPattern(matcher.group(1), DATE_TIME_PATTERN);
			}
			
//			String effectDate = StringUtils.str(orderMessage, "<Effect_Date>", "</Effect_Date>");;
//			for (Map.Entry<String, String> entry : orderItemsCF.entrySet()) {
//				Map<String, String> value = (Map<String, String>) getCFValue(refOrder, "ORDER_ITEMS", entry.getValue());
//				//String serviceType = value.get("Service_Type");
//				effectDate = value.get("Effect_Date");
//				if (!StringUtils.isBlank(effectDate)) {
//					return DateUtils.parseDateWithPattern(effectDate, DATE_TIME_PATTERN);
//				}
//			}
		}
		return null;
	}

	/**
	 * Parse multi-value value from string to a map of values
	 *
	 * @param entity
	 * @param cfCode
	 * @param value
	 * @return
	 */
	private Object getCFValue(ICustomFieldEntity entity, String cfCode, String value) {
		CustomFieldTemplate cft = customFieldTemplateService.findByCodeAndAppliesTo(cfCode, entity);
		if (cft != null && cft.getFieldType() == CustomFieldTypeEnum.MULTI_VALUE) {
			return cft.deserializeMultiValue(value, null);
		}
		return null;
	}

	private void updateEffectiveDateAndReprocess(Order order, Date date) {
		log.info("update Effective Date And Reprocess  Order: "+order.getCode()+" Status "+order.getStatus()+ "  new Date "+DateUtils.formatDateWithPattern(date, "dd/MM/yyyy HH:mm:ss"));
		if(order.getStatus() == OrderStatusEnum.WAITING) {
			String orderMessage = (String) order.getCfValuesNullSafe().getValue("ORDER_MESSAGE");
			if (orderMessage != null && !isNullDate(date)) {
				orderMessage = orderMessage.replaceAll("<Effect_Date>.*</Effect_Date>",
						"<Effect_Date>" + DateUtils.formatDateWithPattern(date, "dd/MM/yyyy HH:mm:ss") + "</Effect_Date>");
				order.getCfValuesNullSafe().setValue("ORDER_MESSAGE", orderMessage);
				orderService.update(order);
			}
			HashMap<String, Object> context = new HashMap<>();
			context.put(REPROCESS_ORDER, order.getCode());
			context.put(RELEASE_ORDER, order.getCode());
			if ("OMS".equals(order.getReceivedFromApp())) {
				executeScript("com.eir.script.PlaceOMSOrderScript", context);
			} else {
				executeScript("com.eir.script.PlaceOrderScript", context);
			}
		}		
	}

	/**
	 * Gets the waiting orders.
	 *
	 * @return the waiting orders
	 */
	private List<Order> getWaitingOrders() {
		QueryBuilder queryBuilder = new QueryBuilder(Order.class, "a", null);
		queryBuilder.addCriterion("status", "=",OrderStatusEnum.WAITING, false);
		queryBuilder.addOrderCriterion("code", true);
		return queryBuilder.getQuery(subscriptionService.getEntityManager()).getResultList();
	}

	/**
	 * Get order DTO
	 *
	 * @return The order DTO
	 * @throws BusinessException the business exception
	 */
	private OrderDTO getOrderDTO(Order order) throws BusinessException {
		String xmlOrder = (String) order.getCfValuesNullSafe().getValue("ORDER_MESSAGE");

		OrderDTO dto = null;
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(OrderDTO.class);
//            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
//            Schema Schema = schemaFactory.newSchema(new SAXSource(new InputSource(new StringReader(XML_SCHEMA))));
			Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
			// jaxbUnmarshaller.setSchema(Schema);
			dto = (OrderDTO) jaxbUnmarshaller.unmarshal(new StringReader(xmlOrder));
		} catch (JAXBException e) {
			log.error("Convert xml to DTO fail", e);
			throw new BusinessException(e.getMessage(), e);
		}
		return dto;
	}

	/**
	 * Get the order line attribute by code
	 *
	 * @return the order line attribute by code
	 */
	public List<String> getAssoccircidAttribute(OrderDTO dto) {
		List<String> assoccirIdList = new ArrayList<String>();
		for (OrderLineDTO lineDto : dto.orderLines) {
			for (AttributeDTO attributeDTO : lineDto.attributes) {
				if ("ASSOCCIRCID".equals(attributeDTO.code)) {
					assoccirIdList.add(attributeDTO.value);
				}
			}
		}
		return assoccirIdList;
	}

	/**
	 * Find waiting orders.
	 *
	 * @param orderId   the order id
	 * @param serviceId the service id
	 * @return the list of waiting orders that the circuit id matches with
	 *         service_id field
	 */
	@SuppressWarnings("unchecked")
	private List<String> findWaitingOrders(String orderId, String circuitId) {
		String query = "SELECT code FROM ord_order where code <> '" + orderId + "' AND status in ('WAITING')"
				+ " AND cf_values like '%<Service_Id>" + circuitId + "</Service_Id>%'";
		return orderService.getEntityManager().createNativeQuery(query).getResultList();
	}

	/**
	 * Execute script.
	 *
	 * @param scriptCode the script code
	 * @param context    the context
	 */
	private void executeScript(String scriptCode, Map<String, Object> context) {
		try {
			ScriptInterface script = scriptInstanceService.getScriptInstance(scriptCode);
			script.execute(context);
		} catch (Exception ex) {
			log.error("Failed to reprocess order", ex);
		}

	}

}

/**
 * Formatted Order Object (Element tag names use InitCaps with underscores)
 *
 * @author Abdellatif BARI
 */
@XmlRootElement(name = "Wholesale_Billing")
@XmlAccessorType(XmlAccessType.FIELD)
class OrderDTO implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = -2510086889557547061L;

	/**
	 * The ordering system.
	 */
	@XmlElement(name = "Ordering_System", required = true)
	String orderingSystem;

	/**
	 * The order id.
	 */
	@XmlElement(name = "Order_Id", required = true)
	String orderId;

	/**
	 * The ref order id.
	 */
	@XmlElement(name = "Ref_Order_Id", required = true)
	String refOrderId;

	/**
	 * The transaction id.
	 */
	@XmlElement(name = "Transaction_Id", required = true)
	String transactionId;

	/**
	 * The status message.
	 */
	@XmlElement(name = "Status_Message")
	String statusMessage;

	/**
	 * The bill items.
	 */
	@XmlElement(name = "Bill_Item")
	List<OrderLineDTO> orderLines = new ArrayList<>();

	// order details text included in CF : ORDER_DETAILS
	Map<String, String> getOrderDetails() {
		Map<String, String> orderDetails = new HashMap<>();
		orderDetails.put("" + orderId, orderingSystem + CustomFieldValue.MATRIX_KEY_SEPARATOR + refOrderId
				+ CustomFieldValue.MATRIX_KEY_SEPARATOR + transactionId);
		return orderDetails;
	}
}

/**
 * Formatted Order Line Object
 *
 * @author hznibar
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
class OrderLineDTO implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 5057667346724418958L;

	/**
	 * The item id.
	 */
	@XmlElement(name = "Item_Id", required = true)
	String itemId;

	/**
	 * The operator id.
	 */
	@XmlElement(name = "Operator_Id", required = true)
	String operatorId;

	/**
	 * The operator id.
	 */
	@XmlElement(name = "Item_Component_Id", required = true)
	String itemComponentId;

	/**
	 * The item version.
	 */
	@XmlElement(name = "Item_Version")
	String itemVersion;

	/**
	 * The item type.
	 */
	@XmlElement(name = "Item_Type")
	String itemType;

	/**
	 * The agent code.
	 */
	@XmlElement(name = "Agent_Code")
	String agentCode;

	/**
	 * The uan.
	 */
	@XmlElement(name = "UAN")
	String uan;

	/**
	 * The master account number.
	 */
	@XmlElement(name = "Master_Account_Number")
	String masterAccountNumber;

	/**
	 * The service id.
	 */
	@XmlElement(name = "Service_Id")
	String serviceId;

	/**
	 * The ref service id.
	 */
	@XmlElement(name = "Ref_Service_Id")
	String refServiceId;

	/**
	 * The xref service id.
	 */
	@XmlElement(name = "Xref_Service_Id")
	String xRefServiceId;

	/**
	 * The bill code.
	 */
	@XmlElement(name = "Bill_Code", required = true)
	String billCode;

	/**
	 * The action.
	 */
	@XmlElement(name = "Action", required = true)
	String billAction;

	/**
	 * The action qualifier.
	 */
	@XmlElement(name = "Action_Qualifier")
	String billActionQualifier;

	/**
	 * The quantity.
	 */
	@XmlElement(name = "Quantity", required = true)
	String billQuantity;

	/**
	 * The effect date.
	 */
	@XmlElement(name = "Effect_Date", required = true)
	@XmlJavaTypeAdapter(DateTimeAdapter.class)
	Date billEffectDate;

	/**
	 * The special connect.
	 */
	@XmlElement(name = "Special_Connect")
	String specialConnect;

	/**
	 * The special rental.
	 */
	@XmlElement(name = "Special_Rental")
	String specialRental;

	/**
	 * The remark id.
	 */
	@XmlElement(name = "REMARK_ID")
	String remarkId;

	/**
	 * The element type.
	 */
	@XmlElement(name = "ELEMENT_TYPE")
	String elementType;

	/**
	 * The element id.
	 */
	@XmlElement(name = "ELEMENT_ID")
	String elementId;

	/**
	 * The description.
	 */
	@XmlElement(name = "DESCRIPTION")
	String description;

	/**
	 * The operator order number.
	 */
	@XmlElement(name = "Operator_Order_Number")
	String operatorOrderNumber;

	/**
	 * The attributes list.
	 */
	@XmlElement(name = "Attribute")
	List<AttributeDTO> attributes = new ArrayList<>();

	@XmlTransient
	String deducedServiceId;
	@XmlTransient
	String billType;
	@XmlTransient
	OfferTemplate offerTemplate;
	@XmlTransient
	BillingAccount billingAccount;
	@XmlTransient
	Subscription subscription;
	@XmlTransient
	Map<String, String> catalog;
	@XmlTransient
	Map<String, String> attributesMap;
	@XmlTransient
	String chargeType;

	String getDetails() {
		StringBuilder sb = new StringBuilder();
		String action = billAction;
		if ("COS".equalsIgnoreCase(action) || "TOS".equalsIgnoreCase(action)) {
			action = "C";
		}
		if ("CNS".equalsIgnoreCase(action) || "TNS".equalsIgnoreCase(action)) {
			action = "P";
		}
		String offerCode = offerTemplate != null ? offerTemplate.getCode() : "";
		sb.append(operatorId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(itemComponentId)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(itemVersion)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(itemType)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(agentCode)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(uan).append(CustomFieldValue.MATRIX_KEY_SEPARATOR)
				.append(masterAccountNumber).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(serviceId)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(refServiceId)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(xRefServiceId)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billCode)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(action)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billActionQualifier)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billQuantity)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billEffectDate)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billType)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(offerCode)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(operatorOrderNumber)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
		return sb.toString();
	}
}

@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
class AttributeDTO implements Serializable {

	/**
	 * The Constant serialVersionUID.
	 */
	private static final long serialVersionUID = 1735352520076029445L;

	/**
	 * The code.
	 */
	@XmlElement(name = "Code", required = true)
	String code;

	/**
	 * The value.
	 */
	@XmlElement(name = "Value", required = true)
	String value;

	/**
	 * The unit.
	 */
	@XmlElement(name = "Unit")
	String unit;

	/**
	 * The component id.
	 */
	@XmlElement(name = "Component_Id")
	String componentId;

	/**
	 * The attribute type.
	 */
	@XmlElement(name = "Attribute_Type")
	String attributeType;

	String getDetails() {
		StringBuilder sb = new StringBuilder();
		sb.append(value).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(unit)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(componentId)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(attributeType)
				.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
		return sb.toString();
	}
}

/**
 * This is Adaptor class which has main responsibility to convert from
 * java.util.Date to format string of date.
 *
 * @author hznibar
 */
class DateTimeAdapter extends XmlAdapter<String, Date> {

	private static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

	@Override
	public Date unmarshal(String xml) {
		return DateUtils.parseDateWithPattern(xml, DATE_TIME_PATTERN);
	}

	@Override
	public String marshal(Date object) {
		return DateUtils.formatDateWithPattern(object, DATE_TIME_PATTERN);
	}
}
