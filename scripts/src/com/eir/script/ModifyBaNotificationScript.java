package com.eir.script;

import java.util.Map;

import org.meveo.admin.exception.BusinessException;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.customEntities.CustomEntityInstance;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.custom.CustomEntityInstanceService;
import org.meveo.service.script.Script;

/**
 * Delete ServiceType Script
 * 
 * @author hznibar
 *
 */
public class ModifyBaNotificationScript extends Script {
	
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	
	private BillingAccountService billingAccountService = (BillingAccountService) getServiceInterface(
			BillingAccountService.class.getSimpleName());

	@Override
	public void execute(Map<String, Object> initContext) throws BusinessException {
		log.info("#####################Starting of script ModifyBaNotificationScript");
		try {
			BillingAccount newBa = (BillingAccount) initContext.get("event");
			if(newBa != null && newBa.getId() != null) {
				BillingAccount oldBa = billingAccountService.findById(newBa.getId());
				String oldServiceType = (String) oldBa.getCfValuesNullSafe().getValue("SERVICE_TYPE");
				String newServiceType = (String) newBa.getCfValuesNullSafe().getValue("SERVICE_TYPE");
				log.info("old serviceType :"+oldServiceType);
				log.info("new serviceType :"+newServiceType);
				if(oldServiceType != null && !oldServiceType.equals(newServiceType)) {
					Map<String, String> oldNotes = (Map<String, String>) oldBa.getCfValuesNullSafe().getValue("BA_NOTES");
					Map<String, String> newNotes = (Map<String, String>) newBa.getCfValuesNullSafe().getValue("BA_NOTES");
					log.info("oldNotes :"+oldNotes);
					log.info("newNotes :"+newNotes);
					if(newNotes == null || newNotes.size() == 0 || (oldNotes != null && newNotes.size() <= oldNotes.size())) {
						throw new BusinessException("Please insert a comment in the Billing Account Notes regarding change of Service Type.");
					}
				}
				
			}
			
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());

		}
	}
}