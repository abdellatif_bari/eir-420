package com.eir.script;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Base64;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Pattern;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.Query;
import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.transform.sax.SAXSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.apache.commons.httpclient.util.DateUtil;
import org.apache.commons.lang3.SerializationUtils;
import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.RatingScriptExecutionErrorException;
import org.meveo.admin.exception.ValidationException;
import org.meveo.commons.utils.ParamBean;
import org.meveo.commons.utils.ParamBeanFactory;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.BaseEntity;
import org.meveo.model.DatePeriod;
import org.meveo.model.ICustomFieldEntity;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.ChargeInstance;
import org.meveo.model.billing.InstanceStatusEnum;
import org.meveo.model.billing.OneShotChargeInstance;
import org.meveo.model.billing.RecurringChargeInstance;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.SubscriptionStatusEnum;
import org.meveo.model.billing.SubscriptionTerminationReason;
import org.meveo.model.billing.UserAccount;
import org.meveo.model.billing.WalletOperation;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.OneShotChargeTemplate;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.crm.CustomFieldTemplate;
import org.meveo.model.crm.Customer;
import org.meveo.model.crm.custom.CustomFieldTypeEnum;
import org.meveo.model.crm.custom.CustomFieldValue;
import org.meveo.model.customEntities.CustomEntityInstance;
import org.meveo.model.mediation.Access;
import org.meveo.model.notification.InboundRequest;
import org.meveo.model.order.Order;
import org.meveo.model.order.OrderStatusEnum;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.admin.impl.SellerService;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.billing.impl.ChargeInstanceService;
import org.meveo.service.billing.impl.OneShotChargeInstanceService;
import org.meveo.service.billing.impl.RatingService;
import org.meveo.service.billing.impl.RecurringChargeInstanceService;
import org.meveo.service.billing.impl.ServiceInstanceService;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.billing.impl.UserAccountService;
import org.meveo.service.billing.impl.WalletOperationService;
import org.meveo.service.catalog.impl.ChargeTemplateService;
import org.meveo.service.catalog.impl.OfferTemplateService;
import org.meveo.service.catalog.impl.RecurringChargeTemplateService;
import org.meveo.service.catalog.impl.ServiceTemplateService;
import org.meveo.service.crm.impl.CustomFieldInstanceService;
import org.meveo.service.crm.impl.CustomFieldTemplateService;
import org.meveo.service.crm.impl.ProviderService;
import org.meveo.service.crm.impl.SubscriptionTerminationReasonService;
import org.meveo.service.custom.CustomEntityInstanceService;
import org.meveo.service.medina.impl.AccessService;
import org.meveo.service.order.OrderService;
import org.meveo.service.payments.impl.CustomerAccountService;
import org.meveo.service.script.Script;
import org.meveo.service.script.ScriptInstanceService;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 * EIR Place OMS Order Script
 *
 * @author Abdellatif BARI
 */
public class PlaceOMSOrderScript extends Script {

    // API Services
    private CustomerAccountService customerAccountService = (CustomerAccountService) getServiceInterface("CustomerAccountService");
    private OrderService orderService = (OrderService) getServiceInterface("OrderService");
    private BillingAccountService billingAccountService = (BillingAccountService) getServiceInterface("BillingAccountService");
    private SubscriptionService subscriptionService = (SubscriptionService) getServiceInterface("SubscriptionService");
    private OfferTemplateService offerTemplateService = (OfferTemplateService) getServiceInterface("OfferTemplateService");
    private ParamBeanFactory paramBeanFactory = (ParamBeanFactory) getServiceInterface("ParamBeanFactory");
    private UserAccountService userAccountService = (UserAccountService) getServiceInterface("UserAccountService");
    private SellerService sellerService = (SellerService) getServiceInterface("SellerService");
    private ServiceTemplateService serviceTemplateService = (ServiceTemplateService) getServiceInterface("ServiceTemplateService");
    private ProviderService providerService = (ProviderService) getServiceInterface("ProviderService");
    private ServiceInstanceService serviceInstanceService = (ServiceInstanceService) getServiceInterface("ServiceInstanceService");
    private ChargeTemplateService<OneShotChargeTemplate> chargeTemplateService = (ChargeTemplateService) getServiceInterface("ChargeTemplateService");
    private OneShotChargeInstanceService oneShotChargeInstanceService = (OneShotChargeInstanceService) getServiceInterface("OneShotChargeInstanceService");
    private CustomFieldInstanceService customFieldInstanceService = (CustomFieldInstanceService) getServiceInterface("CustomFieldInstanceService");
    private CustomFieldTemplateService customFieldTemplateService = (CustomFieldTemplateService) getServiceInterface("CustomFieldTemplateService");
    private CustomEntityInstanceService customEntityInstanceService = (CustomEntityInstanceService) getServiceInterface("CustomEntityInstanceService");
    private SubscriptionTerminationReasonService subscriptionTerminationReasonService = (SubscriptionTerminationReasonService) getServiceInterface(
        "SubscriptionTerminationReasonService");
    private AccessService accessService = (AccessService) getServiceInterface("AccessService");
    private ScriptInstanceService scriptInstanceService = (ScriptInstanceService) getServiceInterface("ScriptInstanceService");
    private RatingService ratingService = (RatingService) getServiceInterface("RatingService");
    private RecurringChargeTemplateService recurringChargeTemplateService = (RecurringChargeTemplateService) getServiceInterface("RecurringChargeTemplateService");
    private RecurringChargeInstanceService recurringChargeInstanceService = (RecurringChargeInstanceService) getServiceInterface("RecurringChargeInstanceService");

    // TODO: to be removed
    private WalletOperationService walletOperationService = (WalletOperationService) getServiceInterface("WalletOperationService");
    private ChargeInstanceService<ChargeInstance> chargeInstanceService = (ChargeInstanceService) getServiceInterface("ChargeInstanceService");

    // variables
    // TODO : Create a custom table for all constants or one custom field for each constant on provider config.

    /**
     * Date time pattern
     */
    public static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

    /**
     * Date time pattern
     */
    public static final String DATE_DEFAULT_PATTERN = "dd/MM/yyyy";

    /**
     * inbound request event parameter.
     */
    private static final String INBOUND_REQUEST_EVENT_PARAMETER = "event";

    /**
     * inbound request reprocess order parameter.
     */
    private static final String INBOUND_REQUEST_REPROCESS_ORDER_PARAMETER = "REPROCESS_ORDER";
    private static final String INBOUND_REQUEST_RELEASE_ORDER_PARAMETER = "RELEASE_ORDER";

    /**
     * Emitter type.
     */
    private static final String OMS_EMITTER = "OMS";

    /**
     * Openeir seller code.
     */
    private static final String OPENEIR_SELLER_CODE = "OPENEIR";

    /**
     * Misc charge template Code.
     */
    private static final String MISC_CHARGE_TEMPLATE_CODE = "MISC_CHARGE";

    /**
     * Misc Offer Code.
     */
    private static final String MISC_OFFER_TEMPLATE_CODE = "MISC_WB";

    /**
     * EIR Offer Code.
     */
    private static final String EIR_OFFER_TEMPLATE_CODE = "OF_EIR_TEMPLATE";

    /**
     * NGA FTH Offer Code.
     */
    private static final String NGA_FTH_OFFER_TEMPLATE_CODE = "NGA_FTH";

    /**
     * Eircode address api url
     */
    private static final String EIRCODE_ADDRESS_API_URL = "eir.eircode.address.api.url";

    /**
     * Eircode address api url
     */
    private static final String ARD_ADDRESS_API_URL = "eir.ard.address.api.url";

    /**
     * The Constant ORDER_WF.
     */
    private static final String ORDER_WF = "ORDER_WF";

    /**
     * Service main termination reason
     */
    private static final String TERMINATION_REASON_MAIN = "TR_AGREEMENT_REFUND";

    /**
     * Service ancillary termination reason
     */
    private static final String TERMINATION_REASON_ANCILLARY = "TR_AGREEMENT_REFUND";

    /**
     * The Constant CET_CIRCUIT_TYPE_MAPPING.
     */
    private static final String CET_CIRCUIT_TYPE_MAPPING = "CIRCUIT_TYPE_MAPPING";

    private InboundRequest inboundRequest = null;
    private String reprocessOrder = null;
    private String releaseOrder = null;
    private Order order;
    private OrderDTO orderDTO;
    private RefOrderDTO refOrderDTO;
    private Map<String, Subscription> newSubscriptions = new HashMap<>();
    private Set<UserAccount> newUserAccounts = new HashSet<>();
    /** Allows to store the incoming xml order **/
    private String originalXmlOrder;

    @Override
    public void execute(Map<String, Object> parametres) throws BusinessException {
        try {
            log.info("##################### Starting of script PlaceOMSOrderScript #####################");

            initContext(parametres);

            // validate order (Validation of Control-Flow)
            validateOrder();

            // executing order
            executeOrder();

            // persisting order
            persistOrder();

            // build response
            buildResponse();

            log.info("##################### Ending of script PlaceOMSOrderScript #####################");

        } catch (Exception e) {
            // build failure response and throw the business exception
            buildFailureResponse(e);
            if (e instanceof BusinessException) {
                throw e;
            } else {
                // wrap the exception in a business exception and throwing it
                throw new BusinessException(e);
            }
        }
    }

    /**
     * Set bad request error
     *
     * @param errorEnum The error enum
     * @param fieldName The field name
     */
    private void setBadRequestError(ErrorEnum errorEnum, String fieldName) {
        String errorMessage = null;
        if (errorEnum == ErrorEnum.BAD_REQUEST_MISSING_PARAMETER) {
            errorMessage = String.format("The following parameter is required : %s", fieldName);
        } else if (errorEnum == ErrorEnum.BAD_REQUEST_INVALID_VALUE) {
            errorMessage = String.format("The following parameter contains invalid value : %s", fieldName);
        } else if (errorEnum == ErrorEnum.BAD_REQUEST_INVALID_SIZE) {
            errorMessage = String.format("The following parameter has invalid size : %s", fieldName);
        }
        setError(errorEnum, errorMessage);
    }

    /**
     * Set order error
     *
     * @param errorEnum The error enum
     * @param errorMessage The error message
     */
    private void setError(ErrorEnum errorEnum, String errorMessage) {
        if (orderDTO != null) {
            orderDTO.errorCode = errorEnum;
            orderDTO.errorMessage = errorMessage;
        }
    }

    private boolean isReprocessingContext() {
        return !StringUtils.isBlank(reprocessOrder);
    }

    /**
     * Build response
     */
    private void buildResponse() {
        if (orderDTO.errorCode != null && orderDTO.errorCode.getStatus() != HttpURLConnection.HTTP_OK) {
            buildFailureResponse(null);
        } else {
            buildSuccessResponse();
        }
    }

    /**
     * Build success response
     */
    private void buildSuccessResponse() {
        if (inboundRequest != null) {
            inboundRequest.setResponseContentType("application/json");
            inboundRequest.setResponseBody("{\"status\": \"SUCCESS\",\"message\": \"\"}");
            inboundRequest.setResponseStatus(HttpURLConnection.HTTP_OK);
        }
    }

    /**
     * Build failure response
     *
     * @param e The exception
     */
    private void buildFailureResponse(Exception e) {
        log.info("##################### error in  PlaceOMSOrderScript");
        if (inboundRequest != null) {
            inboundRequest.setResponseContentType("application/json");
            ErrorEnum errorCode = null;
            String errorMessage = null;

            if (orderDTO != null) {
                errorCode = orderDTO.errorCode;
                errorMessage = orderDTO.errorMessage;
            }

            if (errorCode == null || e != null) {
                errorCode = ErrorEnum.GENERIC_API_EXCEPTION;
                errorMessage = "Exception occurred at the API level";
            }

            /*
             * if(e != null && StringUtils.isBlank(errorMessage)){ //errorMessage = e.getMessage() != null ? e.getMessage() : e + ""; }
             */

            inboundRequest.setResponseBody("{\"status\": \"FAILURE\",\"errorCode\": \"" + errorCode.getCode() + "\",\"message\": \"" + errorMessage + "\"}");
            inboundRequest.setResponseStatus(errorCode.getStatus());
        }
    }

    /**
     * Init context
     *
     * @param parametres parametres
     * @throws BusinessException the business exception
     */
    private void initContext(Map<String, Object> parametres) throws BusinessException {
        inboundRequest = (InboundRequest) parametres.get(INBOUND_REQUEST_EVENT_PARAMETER);
        if (inboundRequest == null) {
            inboundRequest = new InboundRequest();
            String body = (String) parametres.get("body");
            if (!StringUtils.isBlank(body)) {
                inboundRequest.setBody(body);
            }
            parametres.put(INBOUND_REQUEST_EVENT_PARAMETER, inboundRequest);
        }
        reprocessOrder = (String) parametres.get(INBOUND_REQUEST_REPROCESS_ORDER_PARAMETER);
        releaseOrder = (String) parametres.get(INBOUND_REQUEST_RELEASE_ORDER_PARAMETER);
        orderDTO = getOrderDTO();
        // Sort Order lines
        sortOrderLines();
    }

    /**
     * Get xml order
     *
     * @return The xml order
     * @throws BusinessException the business exception
     */
    private String getXmlOrder() throws BusinessException {
        String xmlOrder = inboundRequest.getBody();
        originalXmlOrder = xmlOrder;
        if (isReprocessingContext()) {
            order = orderService.findByCode(reprocessOrder);
            if (order == null) {
                throw new BusinessException("The order " + reprocessOrder + " is not found");
            }
            log.info("Reprocessing order : " + reprocessOrder);
            xmlOrder = (String) order.getCfValuesNullSafe().getValue("ORDER_MESSAGE");
            originalXmlOrder = (String) order.getCfValuesNullSafe().getValue("ORDER_ORIGINAL_MESSAGE");
        }
        if (!StringUtils.isBlank(xmlOrder)) {
            xmlOrder = xmlOrder.replaceAll(Pattern.quote("|"), "/");
        } else {
            throw new BusinessException("The provided order xml is empty");
        }
        return xmlOrder;
    }

    /**
     * Get order DTO
     *
     * @return The order DTO
     * @throws BusinessException the business exception
     */
    private OrderDTO getOrderDTO() throws BusinessException {
        String xmlOrder = getXmlOrder();
        return getDTOFromXml(xmlOrder, OrderDTO.class);
    }

    /**
     * Get DTO from xml
     *
     * @return The DTO
     * @throws BusinessException the business exception
     */
    private <T> T getDTOFromXml(String xml, Class<T> jaxbClass) throws BusinessException {
        T dto = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(jaxbClass);
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema Schema = schemaFactory.newSchema(new SAXSource(new InputSource(new StringReader(XML_SCHEMA))));
            Unmarshaller jaxbUnmarshaller = null;
            jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            // jaxbUnmarshaller.setSchema(Schema);
            dto = (T) jaxbUnmarshaller.unmarshal(new StringReader(xml));
        } catch (JAXBException | SAXException e) {
            log.error("Convert xml to DTO fail", e);
            throw new BusinessException(e.getMessage(), e);
        }
        return dto;
    }

    /**
     * Get XML from DTO.
     *
     * @param object the object to be converted to xml
     * @param <T> the generic type of object to be converted to xml
     * @return the xml
     * @throws BusinessException the business exception
     */
    private <T> String getXmlFromDTO(T object) throws BusinessException {
        String xmlContent = null;
        try {
            if (object != null) {
                // Create JAXB Context
                JAXBContext jaxbContext = JAXBContext.newInstance((Class<T>) object.getClass());

                // Create Marshaller
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

                // Required formatting??
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

                // Print XML String to Console
                StringWriter stringWriter = new StringWriter();

                // Write XML to StringWriter
                jaxbMarshaller.marshal(object, stringWriter);

                // get XML Content
                xmlContent = stringWriter.toString();
            }
        } catch (JAXBException e) {
            log.error("Convert DTO to xml fail", e);
            throw new BusinessException(e.getMessage(), e);
        }
        return xmlContent;
    }

    /**
     * Charges do not apply for End Point item types so the bill code will be set to EP Charges do not apply for Association item types so the bill code will be set to AS Charges
     * do not apply for End Point item types so the bill code will be set to RMK
     *
     * @param orderLineDTO the The order line DTO
     * @return true if the charges can be apply for the current item type
     */
    private boolean applyingCharges(OrderLineDTO orderLineDTO) {
        if (ItemTypeEnum.END_POINT.getLabel().equalsIgnoreCase(orderLineDTO.itemType) || ItemTypeEnum.ASSOCIATION.getLabel().equalsIgnoreCase(orderLineDTO.itemType)
                || ItemTypeEnum.REMARKS.getLabel().equalsIgnoreCase(orderLineDTO.itemType)) {
            return false;
        }
        return true;
    }

    /**
     * validate order line prerequisites
     *
     * @param orderLineDTO the The order line DTO
     */
    private void validateOrderLinePrerequisites(OrderLineDTO orderLineDTO) {

        orderLineDTO.offerTemplate = getOfferTemplate(orderLineDTO);

        // Check Offer
        if (orderLineDTO.offerTemplate != null) {
            log.info("The current offer is " + orderLineDTO.offerTemplate.getCode());
            if (orderLineDTO.catalog == null) {
                setError(ErrorEnum.INVALID_DATA, "Catalog is not found for date " + orderLineDTO.billEffectDate);
                return;
            }
            if (orderLineDTO.masterAccountNumber == null) {
                String billingAccountCode = getProductSetPrefixCode(orderLineDTO);
                orderLineDTO.billingAccount = billingAccountService.findByCode(billingAccountCode);
                if (orderLineDTO.billingAccount == null) {
                    setError(ErrorEnum.INVALID_DATA, "The billing account is not found");
                    return;
                }
                orderLineDTO.masterAccountNumber = orderLineDTO.billingAccount.getExternalRef1();
            }
        } else if (applyingCharges(orderLineDTO)) {
            setError(ErrorEnum.INVALID_DATA, "The offer is not found for a billCode :" + orderLineDTO.billCode);
            return;
        }

        // Check Customer (Operator_Id)
        CustomerAccount customerAccount = customerAccountService.findByCode(getProductSetPrefixCode(orderLineDTO));
        if (customerAccount == null) {
            customerAccount = customerAccountService.findByCode(orderLineDTO.operatorId);
        }
        if (customerAccount == null) {
            setError(ErrorEnum.INVALID_DATA, "The customer account " + orderLineDTO.operatorId + " is not found");
            return;
        }

        Customer customer = customerAccount.getCustomer();
        if (customer == null) {
            setError(ErrorEnum.INVALID_DATA, "The customer of the customer account " + orderLineDTO.operatorId + " is not found");
            return;
        }

        // Check Billing Account
        if (!StringUtils.isBlank(orderLineDTO.masterAccountNumber)) {
            // orderLineDTO.billingAccount = billingAccountService.findByCode(orderLineDTO.masterAccountNumber);
            orderLineDTO.billingAccount = (orderLineDTO.billingAccount == null) ? findByNumber(orderLineDTO.masterAccountNumber) : orderLineDTO.billingAccount;
            if (orderLineDTO.billingAccount == null) {
                // setError(ErrorEnum.INVALID_DATA, "The billing account is not found");
                return;
            } else {
                if (!orderLineDTO.billingAccount.getCustomerAccount().equals(customerAccount)) {
                    setError(ErrorEnum.INVALID_DATA, "Customer Account and billing account mismatch");
                    return;
                }
            }
        }

        // Subscription
        if (!StringUtils.isBlank(orderLineDTO.serviceId)) {
            orderLineDTO.deducedServiceId = getServiceIdWithPrefix(orderLineDTO);
            Subscription subscription = subscriptionService.findByCode(orderLineDTO.deducedServiceId);
            if (subscription != null) {
                orderLineDTO.subscription = subscription;
                if (subscription.getUserAccount() != null && subscription.getUserAccount().getBillingAccount() != null) {
                    if (orderLineDTO.billingAccount != null) {
                        if (!subscription.getUserAccount().getBillingAccount().equals(orderLineDTO.billingAccount)) {
                            setError(ErrorEnum.INVALID_DATA, "Subscription and billing account mismatch");
                            return;
                        }
                    } else {
                        orderLineDTO.billingAccount = subscription.getUserAccount().getBillingAccount();
                        // orderLineDTO.masterAccountNumber = orderLineDTO.billingAccount.getCode();
                        orderLineDTO.masterAccountNumber = orderLineDTO.billingAccount.getExternalRef1();
                    }
                }

                if (orderLineDTO.offerTemplate != null && orderLineDTO.offerTemplate != orderLineDTO.subscription.getOffer()) {
                    log.info("The subscription offer is :" + orderLineDTO.subscription.getOffer().getCode());
                    setError(ErrorEnum.INVALID_DATA,
                        "The subscription " + orderLineDTO.deducedServiceId + " is not associated with the offer " + orderLineDTO.offerTemplate.getCode());
                    return;
                }
            } else if (ItemTypeEnum.SUBSCRIPTION_MAIN_DETAILS.getLabel().equalsIgnoreCase(orderLineDTO.itemType)) {
                // a new subscription with id orderLineDTO.serviceId will be created
                newSubscriptions.put(orderLineDTO.serviceId, new Subscription());
            } else {
                if (!newSubscriptions.containsKey(orderLineDTO.serviceId)) {
                    setError(ErrorEnum.INVALID_DATA, "The subscription " + orderLineDTO.deducedServiceId + " is not found");
                    return;
                }
            }
        } else {
            // } else if (!ItemTypeEnum.SUBSCRIPTION_MAIN_DETAILS.getLabel().equalsIgnoreCase(orderLineDTO.itemType)) {
            setError(ErrorEnum.INVALID_DATA, "The Service_Id parameter is missing");
            return;
        }

        // Misc Charge.
        if (BillActionEnum.M.name().equalsIgnoreCase(orderLineDTO.billAction)) {
            if (StringUtils.isBlank(orderLineDTO.masterAccountNumber)) {
                setError(ErrorEnum.INVALID_DATA, "Missing Master_Account_Number parameter");
                return;
            }
            orderLineDTO.serviceId = "MISC_" + orderLineDTO.masterAccountNumber;
            Subscription subscription = subscriptionService.findByCode(orderLineDTO.serviceId);
            if (subscription != null) {
                orderLineDTO.subscription = subscription;
                if (subscription.getUserAccount() != null && subscription.getUserAccount().getBillingAccount() != null) {
                    orderLineDTO.billingAccount = subscription.getUserAccount().getBillingAccount();
                    // orderLineDTO.masterAccountNumber = orderLineDTO.billingAccount.getCode();
                    orderLineDTO.masterAccountNumber = orderLineDTO.billingAccount.getExternalRef1();
                }
            } else {
                setError(ErrorEnum.INVALID_DATA, "The subscription " + orderLineDTO.serviceId + " is not found");
                return;
            }
        }
    }

    /**
     * Validate order line
     *
     * @param orderLineDTO the order line DTO
     */
    private void validateOrderLine(OrderLineDTO orderLineDTO) {
        /** Item_Id **/
        if (StringUtils.isBlank(orderLineDTO.itemId)) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_MISSING_PARAMETER, "Item_Id");
            return;
        }
        if (orderLineDTO.itemId.length() > 50) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Item_Id");
            return;
        }

        /** Operator_Id **/
        if (StringUtils.isBlank(orderLineDTO.operatorId)) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_MISSING_PARAMETER, "Operator_Id");
            return;
        }
        if (orderLineDTO.operatorId.length() > 10) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Operator_Id");
            return;
        }

        /** Item_Component_Id **/
        if (StringUtils.isBlank(orderLineDTO.itemComponentId)) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_MISSING_PARAMETER, "Item_Component_Id");
            return;
        }
        if (orderLineDTO.itemComponentId.length() > 15) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Item_Component_Id");
            return;
        }

        /** Item_Version **/
        if (!StringUtils.isBlank(orderLineDTO.itemVersion)) {
            if (!isInteger(orderLineDTO.itemVersion)) {
                setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_VALUE, "Item_Version");
                return;
            }
            if (orderLineDTO.itemVersion.length() > 3) {
                setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Item_Version");
                return;
            }
        }

        /** Item_Type **/
        if (!StringUtils.isBlank(orderLineDTO.itemType) && orderLineDTO.itemType.length() > 50) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Item_Type");
            return;
        }

        /** Agent_Code **/
        if (!StringUtils.isBlank(orderLineDTO.agentCode) && orderLineDTO.agentCode.length() > 8) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Agent_Code");
            return;
        }

        /** UAN **/
        if (!StringUtils.isBlank(orderLineDTO.uan) && orderLineDTO.uan.length() > 8) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "UAN");
            return;
        }

        /** Master_Account_Number **/
        if (!StringUtils.isBlank(orderLineDTO.masterAccountNumber) && orderLineDTO.masterAccountNumber.length() > 20) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Master_Account_Number");
            return;
        }

        /** Service_Id **/
        if (!StringUtils.isBlank(orderLineDTO.serviceId) && orderLineDTO.serviceId.length() > 30) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Service_Id");
            return;
        }

        /** Ref_Service_Id **/
        if (!StringUtils.isBlank(orderLineDTO.refServiceId) && orderLineDTO.refServiceId.length() > 30) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Ref_Service_Id");
            return;
        }

        /** Xref_Service_Id **/
        if (!StringUtils.isBlank(orderLineDTO.xRefServiceId) && orderLineDTO.xRefServiceId.length() > 30) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Xref_Service_Id");
            return;
        }

        /** Bill_Code **/
        if (StringUtils.isBlank(orderLineDTO.billCode)) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_MISSING_PARAMETER, "Bill_Code");
            return;
        }
        if (orderLineDTO.billCode.length() > 100) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Bill_Code");
            return;
        }

        /** Action **/
        if (StringUtils.isBlank(orderLineDTO.billAction)) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_MISSING_PARAMETER, "Action");
            return;
        }
        if (!BillActionEnum.contains(orderLineDTO.billAction)) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_VALUE, "Action");
            return;
        }
        if (orderLineDTO.billAction.length() > 10) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Action");
            return;
        }

        /** Action_Qualifier **/
        if (!StringUtils.isBlank(orderLineDTO.billActionQualifier) && orderLineDTO.billActionQualifier.length() > 50) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Action_Qualifier");
            return;
        }

        /** Quantity **/
        if (StringUtils.isBlank(orderLineDTO.billQuantity)) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_MISSING_PARAMETER, "Quantity");
            return;
        }
        if (!isInteger(orderLineDTO.billQuantity)) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_VALUE, "Quantity");
            return;
        }
        if (orderLineDTO.billQuantity.length() > 5) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Quantity");
            return;
        }

        /** Effect_Date **/
        if (orderLineDTO.billEffectDate == null) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_VALUE, "Effect_Date");
            return;
        }

        /** Special_Connect **/
        if (!StringUtils.isBlank(orderLineDTO.specialConnect)) {
            if (!isInteger(orderLineDTO.specialConnect)) {
                setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_VALUE, "Special_Connect");
                return;
            }
            if (orderLineDTO.specialConnect.length() > 20) {
                setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Special_Connect");
                return;
            }
        }

        /** Special_Rental **/
        if (!StringUtils.isBlank(orderLineDTO.specialRental)) {
            if (!isInteger(orderLineDTO.specialRental)) {
                setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_VALUE, "Special_Rental");
                return;
            }
            if (orderLineDTO.specialRental.length() > 20) {
                setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Special_Rental");
                return;
            }
        }

        /** Operator_Order_Number **/
        if (!StringUtils.isBlank(orderLineDTO.operatorOrderNumber) && orderLineDTO.operatorOrderNumber.length() > 20) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Operator_Order_Number");
            return;
        }

        for (int j = 0; j < orderLineDTO.attributes.size(); j++) {
            AttributeDTO attributeDTO = orderLineDTO.attributes.get(j);

            /** Code **/
            if (StringUtils.isBlank(attributeDTO.code)) {
                setBadRequestError(ErrorEnum.BAD_REQUEST_MISSING_PARAMETER, "Code");
                return;
            }
            if (attributeDTO.code.length() > 50) {
                setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Code");
                return;
            }

            /** Value **/
            if (StringUtils.isBlank(attributeDTO.value)) {
                setBadRequestError(ErrorEnum.BAD_REQUEST_MISSING_PARAMETER, "Value");
                return;
            }
            if (attributeDTO.value.length() > 50) {
                setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Value");
                return;
            }
            if (SubMainDetailsAttributeNamesEnum.AMT.name().equalsIgnoreCase(attributeDTO.code) || ChargesAttributeNamesEnum.AMT.name().equalsIgnoreCase(attributeDTO.code)) {
                if (!isDouble(attributeDTO.value)) {
                    setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_VALUE, "Value");
                    return;
                }
            }
            if (ChargesAttributeNamesEnum.QUANTITY.name().equalsIgnoreCase(attributeDTO.code)) {
                if (!isInteger(attributeDTO.value)) {
                    setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_VALUE, "Value");
                    return;
                }
            }

            /** Unit **/
            if (!StringUtils.isBlank(attributeDTO.unit) && attributeDTO.unit.length() > 10) {
                setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Unit");
                return;
            }

            /** Component_Id **/
            if (!StringUtils.isBlank(attributeDTO.componentId) && attributeDTO.componentId.length() > 15) {
                setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Component_Id");
                return;
            }

            /** Attribute_Type **/
            if (!StringUtils.isBlank(attributeDTO.attributeType) && attributeDTO.attributeType.length() > 50) {
                setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Attribute_Type");
                return;
            }
        }
        validateOrderLinePrerequisites(orderLineDTO);
    }

    /**
     * Validate order
     */
    private void validateOrder() {

        /** Ordering_System **/
        if (StringUtils.isBlank(orderDTO.orderingSystem)) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_MISSING_PARAMETER, "Ordering_System");
            return;
        }
        if (!OMS_EMITTER.equalsIgnoreCase(orderDTO.orderingSystem)) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_VALUE, "Ordering_System");
            return;
        }
        if (orderDTO.orderingSystem.length() > 5) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Ordering_System");
            return;
        }

        /** Order_Id **/
        if (StringUtils.isBlank(orderDTO.orderId)) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_MISSING_PARAMETER, "Order_Id");
            return;
        }
        if (orderDTO.orderId.length() > 15) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Ordering_System");
            return;
        }

        /** Ref_Order_Id **/
        if (!StringUtils.isBlank(orderDTO.refOrderId) && orderDTO.refOrderId.length() > 15) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Ref_Order_Id");
            return;
        }

        /** Transaction_Id **/
        if (StringUtils.isBlank(orderDTO.transactionId)) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_MISSING_PARAMETER, "Transaction_Id");
            return;
        }
        if (orderDTO.transactionId.length() > 15) {
            setBadRequestError(ErrorEnum.BAD_REQUEST_INVALID_SIZE, "Transaction_Id");
            return;
        }

        /** Order_Id **/
        if (!isReprocessingContext()) {
            // check if order already exists
            if (orderService.findByCode(orderDTO.orderId) != null) {
                setError(ErrorEnum.ORDER_ALREADY_EXISTS, "The order " + orderDTO.orderId + " already exists");
                return;
            }
        }

        for (OrderLineDTO orderLineDTO : orderDTO.orderLines) {
            if (orderDTO.errorCode != null) {
                return;
            }
            validateOrderLine(orderLineDTO);
        }
    }

    /**
     * Create Order from incoming data
     */
    private Order createOrder() {
        Order order = new Order();
        // order.setStatus(OrderStatusEnum.PENDING);
        order.setCode(orderDTO.orderId);
        order.setReceivedFromApp(orderDTO.orderingSystem);
        return order;
    }

    /**
     * Execute created order by activating/changing/ceasing services in subscription
     *
     * @throws BusinessException the business exception
     */
    private void executeOrder() throws BusinessException {

        // Don't execute order if there is at least one error.
        if (orderDTO.errorCode != null) {
            return;
        }

        // check if is a deferred, held or waiting order and not execute the order except if it's a releasing context.

        if (StringUtils.isBlank(releaseOrder) && (isDeferredOrder() || isHeldOrder() || isWaitingOrder())) {
            return;
        }

        processOrder();
    }

    /**
     * To prcess the order in new Transaction and if an error is found we Rollback all order items. but we can save the order with error in the next service by joining the current
     * transaction.
     *
     * @throws BusinessException the business exception
     */
    private void processOrder() throws BusinessException {

        // try {
        // TODO : begin new nested Transaction here.

        for (OrderLineDTO orderLineDTO : orderDTO.orderLines) {

            if (orderLineDTO.subscription == null && newSubscriptions.containsKey(orderLineDTO.serviceId) && newSubscriptions.get(orderLineDTO.serviceId).getId() != null) {
                orderLineDTO.subscription = newSubscriptions.get(orderLineDTO.serviceId);
            }

            String status = orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.STATUS.name());

            switch (BillActionEnum.valueOf(orderLineDTO.billAction)) {
            // OMS Provide
            case PR:
                provideOrder(orderLineDTO);
                break;
            // OMS Change
            case CH:
                changeOrder(orderLineDTO);
                break;
            // OMS Cease
            case CE:
                ceaseOrder(orderLineDTO);
                break;
            // Cancel
            case M:
                // MISC Charge
                miscCharge(orderLineDTO);
                break;
            }

            if (orderDTO.errorCode != null) {
                break;
            }
        }

        if (orderDTO.errorCode != null) {
            // TODO : Rollback the new Transaction here.
        } else {
            // TODO : Commit the new Transaction here.PersistenceUtils
        }
        /*
         * } catch (Exception e) { //TODO : Rollback the new Transaction here. if (e instanceof BusinessException) { throw e; } else { //wrap the exception in a business exception
         * and throwing it throw new BusinessException(e); } }
         */
    }

    /**
     * Update order cfs
     */
    private void updateOrderCfs() {
        // store Order Items and attributes in CF at Order level
        Map<String, String> orderItemsCF = new HashMap<>();
        Map<String, String> orderItemAttributesCF = new HashMap<>();

        // Populate order attributes
        for (OrderLineDTO orderLineDTO : orderDTO.orderLines) {

            // store order items and their attributes in adequate CFs
            orderItemsCF.put(orderLineDTO.itemId, orderLineDTO.getDetails());
            for (AttributeDTO attributeDTO : orderLineDTO.attributes) {
                orderItemAttributesCF.put(orderLineDTO.itemId + CustomFieldValue.MATRIX_KEY_SEPARATOR + attributeDTO.code, attributeDTO.getDetails());
            }
        }

        // create the order with related CFs
        order.getCfValuesNullSafe().setValue("ORDER_DETAILS", orderDTO.getOrderDetails());
        order.getCfValuesNullSafe().setValue("ORDER_ITEMS", orderItemsCF);
        order.getCfValuesNullSafe().setValue("ORDER_ITEM_ATTRIBUTES", orderItemAttributesCF);
        order.getCfValuesNullSafe().setValue("ORDER_MESSAGE", getXmlFromDTO(orderDTO));
        order.getCfValuesNullSafe().setValue("ORDER_ORIGINAL_MESSAGE", originalXmlOrder);

        if (orderDTO.orderLines != null && !orderDTO.orderLines.isEmpty()) {
            order.getCfValuesNullSafe().setValue("ORDER_SERVICE_ID", orderDTO.orderLines.get(0).deducedServiceId);
        }
    }

    /**
     * To be removed after switching to jar
     */
    private void removeNewEntites() {

        if (newSubscriptions != null && !newSubscriptions.isEmpty()) {
            for (Map.Entry<String, Subscription> entry : newSubscriptions.entrySet()) {
                Subscription subscription = entry.getValue();
                if (subscription != null && subscription.getId() != null) {
                    List<ServiceInstance> serviceInstances = subscription.getServiceInstances();
                    if (serviceInstances != null && !serviceInstances.isEmpty()) {
                        for (ServiceInstance serviceInstance : serviceInstances) {
                            if (serviceInstance != null) {
                                // List<ChargeInstance> chargeInstances = serviceInstance.getChargeInstances();
                                Query query = chargeInstanceService.getEntityManager()
                                    .createQuery("SELECT ci from ChargeInstance ci  where ci.serviceInstance.id=:serviceInstanceId");
                                query.setParameter("serviceInstanceId", serviceInstance.getId());
                                List<ChargeInstance> chargeInstances = (List<ChargeInstance>) query.getResultList();

                                if (chargeInstances != null && !chargeInstances.isEmpty()) {
                                    log.info("chargeInstances.size()" + chargeInstances.size());
                                    for (ChargeInstance chargeInstance : chargeInstances) {

                                        if (chargeInstanceService.findById(chargeInstance.getId()) == null) {
                                            continue;
                                        }
                                        // List<WalletOperation> walletOperations = recurringChargeInstance.getWalletOperations();

                                        query = walletOperationService.getEntityManager()
                                            .createQuery("SELECT wo from WalletOperation wo  where wo.chargeInstance.id=:chargeInstanceId");
                                        query.setParameter("chargeInstanceId", chargeInstance.getId());
                                        List<WalletOperation> walletOperations = (List<WalletOperation>) query.getResultList();

                                        log.info("walletOperations.size()" + walletOperations.size());
                                        if (walletOperations != null && !walletOperations.isEmpty()) {
                                            for (WalletOperation walletOperation : walletOperations) {
                                                // ratedTransactionService.remove(walletOperation.getRatedTransaction());
                                                // ratedTransactionService.getEntityManager().remove(walletOperation.getRatedTransaction());
                                                // walletOperationService.remove(walletOperation);
                                                walletOperationService.getEntityManager().remove(walletOperation);
                                            }
                                        }
                                        log.info("chargeInstance.getId()" + chargeInstance.getId());
                                        log.info("chargeInstance.getCode()" + chargeInstance.getCode());
                                        log.info("chargeInstance.getServiceInstance().getId()" + chargeInstance.getServiceInstance().getId());
                                        log.info("chargeInstance.getServiceInstance().getCode()" + chargeInstance.getServiceInstance().getCode());
                                        // chargeInstanceService.remove(recurringChargeInstance);
                                        chargeInstanceService.getEntityManager().remove(chargeInstance);
                                    }
                                }
                                // serviceInstanceService.remove(serviceInstance);
                                serviceInstanceService.getEntityManager().remove(serviceInstance);
                            }
                        }

                    }
                    // subscriptionService.remove(subscription);
                    subscriptionService.getEntityManager().remove(subscription);
                }
            }
        }
        if (newUserAccounts != null && !newUserAccounts.isEmpty()) {
            for (UserAccount userAccount : newUserAccounts) {
                if (userAccount != null && userAccount.getId() != null) {
                    userAccountService.getEntityManager().remove(userAccount);
                }
            }
        }
    }

    /**
     * Persist order
     *
     * @throws BusinessException the business exception
     */
    private void persistOrder() throws BusinessException {

        if (orderDTO.errorCode != null && orderDTO.errorCode.getStatus() != HttpURLConnection.HTTP_OK) {
            return;
        }

        if (!isReprocessingContext()) {
            order = createOrder();
        }
        if (orderDTO.errorCode != null) {

            // TODO : Remove this code after switching to jar
            removeNewEntites();

            updateOrder(OrderStatusEnum.REJECTED);
        } else if (orderDTO.status == OrderStatusEnum.DEFERRED || orderDTO.status == OrderStatusEnum.HELD || orderDTO.status == OrderStatusEnum.WAITING) {
            updateOrder(orderDTO.status);
        } else {
            log.info("Order completed : " + order.getCode());
            order.setStatus(OrderStatusEnum.COMPLETED);
            order.setCompletionDate(new Date());
        }

        updateOrderCfs();

        if (order.getId() == null) {
            orderService.create(order);
        } else {
            orderService.update(order);
        }
    }

    /**
     * Update order
     *
     * @param status the order status
     */
    private void updateOrder(OrderStatusEnum status) {
        orderDTO.statusMessage = orderDTO.errorMessage;
        log.info("Order " + status.name() + " : " + orderDTO.orderId);
        log.info("Status message : " + orderDTO.errorMessage);
        order.setStatus(status);
        order.setStatusMessage(orderDTO.errorMessage);
        order.getCfValuesNullSafe().setValue("ORDER_ERROR_MESSAGE", orderDTO.errorMessage);
    }

    /**
     * Sort orderLines
     */
    private void sortOrderLines() {

        // 1 : Charge Items with a STATUS=AC must be processed first/before any of the other bill items on the order
        Collections.sort(orderDTO.orderLines, new Comparator<OrderLineDTO>() {
            @Override
            public int compare(OrderLineDTO orderLineDTO1, OrderLineDTO orderLineDTO2) {
                return getPosition(orderLineDTO1) - getPosition(orderLineDTO2);
            }

            private int getPosition(OrderLineDTO orderLineDTO) {
                String status = orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.STATUS.name());
                if (BillActionEnum.PR.name().equalsIgnoreCase(orderLineDTO.billAction)) {
                    if (ItemTypeEnum.SUBSCRIPTION_MAIN_DETAILS.getLabel().equalsIgnoreCase(orderLineDTO.itemType)) {
                        return 1;
                    }
                    return 2;
                } else if (BillActionEnum.CH.name().equalsIgnoreCase(orderLineDTO.billAction) && ItemStatusEnum.AC.name().equalsIgnoreCase(status)) {
                    return 3;
                } else if (BillActionEnum.CH.name().equalsIgnoreCase(orderLineDTO.billAction)) {
                    return 4;
                } else if (BillActionEnum.CE.name().equalsIgnoreCase(orderLineDTO.billAction) && ItemStatusEnum.CE.name().equalsIgnoreCase(status)) {
                    return 5;
                } else if (BillActionEnum.CE.name().equalsIgnoreCase(orderLineDTO.billAction)) {
                    return 6;
                }
                return 7;
            }
        });
    }

    /**
     * Provide order
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void provideOrder(OrderLineDTO orderLineDTO) throws BusinessException {
        switch (ItemTypeEnum.get(orderLineDTO.itemType)) {
        // OMS Provide: Subscription Main Details
        case SUBSCRIPTION_MAIN_DETAILS:
            provideSubscriptionMainDetails(orderLineDTO);
            break;
        // OMS Provide: End Points
        case END_POINT:
            provideEndPoints(orderLineDTO);
            break;
        // OMS Provide: Associations
        case ASSOCIATION:
            provideAssociations(orderLineDTO);
            break;
        // OMS Orders: Process Service Charges
        case CHARGE:
            processServiceCharges(orderLineDTO);
            break;
        // Provide: Remarks
        case REMARKS:
            provideRemarks(orderLineDTO);
            break;
        }
    }

    /**
     * Change order
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void changeOrder(OrderLineDTO orderLineDTO) throws BusinessException {
        switch (ItemTypeEnum.get(orderLineDTO.itemType)) {
        // OMS Change: Subscription Main Details
        case SUBSCRIPTION_MAIN_DETAILS:
            changeSubscriptionMainDetails(orderLineDTO);
            break;
        // OMS Change: End Points
        case END_POINT:
            changeEndPoints(orderLineDTO);
            break;
        // OMS Change: Associations
        case ASSOCIATION:
            changeAssociations(orderLineDTO);
            break;
        // OMS Orders: Process Service Charges
        case CHARGE:
            processServiceCharges(orderLineDTO);
            break;
        }
    }

    /**
     * Cease order
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void ceaseOrder(OrderLineDTO orderLineDTO) throws BusinessException {
        switch (ItemTypeEnum.get(orderLineDTO.itemType)) {
        // OMS Cease Circuit
        case SUBSCRIPTION_MAIN_DETAILS:
            ceaseCircuit(orderLineDTO);
            break;
        case CHARGE:
            ceaseAncillary(orderLineDTO);
            break;
        }
    }

    /**
     * find billing account by his number
     *
     * @param billingAccountNumber the billing account number
     * @return the billing account
     */
    public BillingAccount findByNumber(String billingAccountNumber) {
        Query query = billingAccountService.getEntityManager().createQuery("SELECT ba from BillingAccount ba  where  ba.externalRef1=:billingAccountNumber");
        query.setParameter("billingAccountNumber", billingAccountNumber);
        try {
            return (BillingAccount) query.getSingleResult();
        } catch (NoResultException e) {
            setError(ErrorEnum.INVALID_DATA, "The billing account is not found");
            return null;
        } catch (NonUniqueResultException e) {
            setError(ErrorEnum.INVALID_DATA, "there are multiple billing accounts with the same number " + billingAccountNumber);
            return null;
        }
    }

    /**
     * Get retries number
     *
     * @return the retries number
     */
    private int getRetriesNumber() {
        ParamBean paramBean = paramBeanFactory.getInstance();
        try {
            return Integer.parseInt(paramBean.getProperty("eir.retries.number.address.api", "1"));
        } catch (NumberFormatException ex) {
        }
        return 1;
    }

    /**
     * Get retry delay
     *
     * @return the retry delay
     */
    private int getRetryDelay() {
        ParamBean paramBean = paramBeanFactory.getInstance();
        try {
            return Integer.parseInt(paramBean.getProperty("eir.retry.delay.address.api", "2000"));
        } catch (NumberFormatException ex) {
        }
        return 2000;
    }

    /**
     * Validate address
     *
     * @param addressInfoResponseDTO the addressInfoResponse DTO
     */
    private void validateAddress(AddressInfoResponseDTO addressInfoResponseDTO) {

        // Validate the response adheres to the data contract and if not raise an error
        if (addressInfoResponseDTO == null) {
            setError(ErrorEnum.INVALID_DATA, "Missing address Info Response");
            return;
        }

        // Address Info aggregate Occurs 1-n
        if (addressInfoResponseDTO.addressInfos == null || addressInfoResponseDTO.addressInfos.isEmpty()) {
            setError(ErrorEnum.INVALID_DATA, "Missing address Info");
            return;
        }

        // Address details aggregate Occurs 1-1
        AddressInfoDTO addressInfoDTO = addressInfoResponseDTO.addressInfos.get(0);
        if (addressInfoDTO.address == null) {
            setError(ErrorEnum.INVALID_DATA, "Missing address");
            return;
        }

        // Unit details aggregate Occurs 1-1
        if (addressInfoDTO.address.unit == null) {
            setError(ErrorEnum.INVALID_DATA, "Missing unit");
            return;
        }

        if (StringUtils.isBlank(addressInfoDTO.address.unit.addressUnitId)) {
            setError(ErrorEnum.INVALID_DATA, "Missing Address_Unit_Id attribute");
            return;
        }

        // Information for a particular building as it occurs in the address Occurs 1-1
        if (addressInfoDTO.address.building == null) {
            setError(ErrorEnum.INVALID_DATA, "Missing building");
            return;
        }

        // Information relating to County Occurs 1-1
        if (addressInfoDTO.address.county == null) {
            setError(ErrorEnum.INVALID_DATA, "Missing county");
            return;
        }

        if (StringUtils.isBlank(addressInfoDTO.address.county.countyId)) {
            setError(ErrorEnum.INVALID_DATA, "Missing County_Id attribute");
            return;
        }

        if (StringUtils.isBlank(addressInfoDTO.address.county.countyName)) {
            setError(ErrorEnum.INVALID_DATA, "Missing County_Name attribute");
            return;
        }
    }

    /**
     * Format the address
     *
     * @param addressInfoResponseDTO the addressInfoResponse DTO
     * @return the formated address
     */
    private String formatAddress(AddressInfoResponseDTO addressInfoResponseDTO) {

        StringBuilder address = new StringBuilder();
        StringBuilder addressLine1 = new StringBuilder();
        StringBuilder addressLine2 = new StringBuilder();
        StringBuilder addressLine3 = new StringBuilder();
        StringBuilder addressLine4 = new StringBuilder();
        String addressLine5 = "";
        String countyAddress = "";
        String postalDistrict = "";
        String eircode = "";
        String countyAddressBis = "";

        AddressInfoDTO addressInfoDTO = addressInfoResponseDTO.addressInfos.get(0);
        BuildingDTO building = addressInfoDTO.address.building;
        UnitDTO unit = addressInfoDTO.address.unit;
        CountyDTO county = addressInfoDTO.address.county;

        // addressLine1
        if (!StringUtils.isBlank(building.buildingNumber)) {
            addressLine1.append(building.buildingNumber).append(" ");
        }
        if (!StringUtils.isBlank(building.fromBuildingNumber)) {
            addressLine1.append(building.fromBuildingNumber).append(" ");
        }
        if (!StringUtils.isBlank(building.toBuildingNumber)) {
            addressLine1.append(building.toBuildingNumber).append(" ");
        }
        if (!StringUtils.isBlank(unit.unitName)) {
            addressLine1.append((unit.unitName)).append(" ");
        }
        if (!StringUtils.isBlank(unit.unitNumber)) {
            addressLine1.append((unit.unitNumber)).append(" ");
        }
        if (!StringUtils.isBlank(unit.floorNumber)) {
            addressLine1.append((unit.floorNumber)).append(" ");
        }
        if (!StringUtils.isBlank(unit.eircode)) {
            addressLine1.append((unit.eircode)).append(" ");
        }

        // addressLine2
        if (!StringUtils.isBlank(building.buildingName)) {
            addressLine2.append(building.buildingName);
        }

        if (addressInfoDTO.address.street != null) {
            StreetDTO street = addressInfoDTO.address.street;

            // addressLine3
            if (!StringUtils.isBlank(street.streetName)) {
                addressLine3.append(street.streetName).append(" ");
            }
            if (!StringUtils.isBlank(street.streetSuffix)) {
                addressLine3.append(street.streetSuffix).append(" ");
            }
            if (!StringUtils.isBlank(street.directionalQualifierName)) {
                addressLine3.append(street.directionalQualifierName).append(" ");
            }
            if (!StringUtils.isBlank(street.geographicalQualifierName)) {
                addressLine3.append(street.geographicalQualifierName).append(" ");
            }

            // addressLine4
            if (!StringUtils.isBlank(street.townlandName)) {
                addressLine4.append(street.townlandName);
            }
        }

        // addressLine5
        if (!StringUtils.isBlank(county.townName)) {
            addressLine5 = county.townName;
        }

        // county
        if (!StringUtils.isBlank(county.townName)) {
            countyAddress = county.townName;
        }

        // postalDistrict
        if (!StringUtils.isBlank(county.postalDistrict)) {
            postalDistrict = county.postalDistrict;
        }

        // eircode
        if (!StringUtils.isBlank(unit.eircode)) {
            eircode = unit.eircode;
        }

        address.append(addressLine1.toString()).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(addressLine2.toString()).append(CustomFieldValue.MATRIX_KEY_SEPARATOR)
            .append(addressLine3.toString()).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(addressLine4.toString()).append(CustomFieldValue.MATRIX_KEY_SEPARATOR)
            .append(addressLine5).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(countyAddress).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(postalDistrict)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(countyAddressBis).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(eircode);

        return address.toString();
    }

    /**
     * Validate and format address
     *
     * @param addressInfoResponseDTO the addressInfoResponse DTO
     * @return the validated address
     */
    private String ValidateAndFormatAddress(AddressInfoResponseDTO addressInfoResponseDTO) {

        validateAddress(addressInfoResponseDTO);
        if (orderDTO.errorCode != null) {
            return null;
        }
        return formatAddress(addressInfoResponseDTO);
    }

    /**
     * Get the API url
     *
     * @param apiUrlProperty the API url property
     * @param addressId the address id
     * @return the API url
     */
    private String getApiUrl(String apiUrlProperty, String addressId) {

        ParamBean paramBean = paramBeanFactory.getInstance();

        String addressApiUrl = paramBean.getProperty(apiUrlProperty, null);

        if (StringUtils.isBlank(addressApiUrl)) {
            return null;
        }

        if (addressApiUrl.endsWith(File.separator)) {
            addressApiUrl = addressApiUrl.substring(0, addressApiUrl.lastIndexOf(File.separator));
        }
        addressApiUrl = addressApiUrl + File.separator + addressId;
        return addressApiUrl;
    }

    /**
     * Get the Eir address details
     *
     * @param addressId the address id
     * @return the Eir address details
     */
    private AddressInfoResponseDTO getEirAddressDetails(String addressId) {

        // An Eircode is a unique 7-character code consisting of letters and numbers. Each Eircode consists of a 3-character
        // routing key to identify the area and a 4-character unique identifier for each address, for example, A65 F4E2.

        /* /Address_Info/Eircode/{Eircode} */
        String apiUrlProperty = EIRCODE_ADDRESS_API_URL;
        if (isInteger(addressId)) {

            // ARD Id is a numeric code (it does not contain letters)

            /* /Address_Info/Ard_Id/{Ard_Id} */
            apiUrlProperty = ARD_ADDRESS_API_URL;
        }

        String addressApiUrl = getApiUrl(apiUrlProperty, addressId);
        if (StringUtils.isBlank(addressApiUrl)) {
            setError(ErrorEnum.INVALID_DATA, "Missing " + apiUrlProperty + "API url");
            return null;
        }

        AddressInfoResponseDTO addressInfoResponseDTO = null;
        try {
            addressInfoResponseDTO = getAddressDetails(addressApiUrl);
        } catch (ValidationException ve) {
            setError(ErrorEnum.GENERIC_API_EXCEPTION, ve.getMessage());
        }

        if (addressInfoResponseDTO == null) {
            setError(ErrorEnum.INVALID_DATA, "Address " + EndPointAttributeNamesEnum.EPADDRESS.name() + " not found on broker");
            return null;
        }

        return addressInfoResponseDTO;
    }

    /**
     * Get address info response
     *
     * @param httpConn the http URL connection
     * @return the address info response
     * @throws ValidationException the validation exception
     */
    private AddressInfoResponseDTO getAddressInfoResponse(HttpURLConnection httpConn) throws ValidationException {
        AddressInfoResponseDTO addressInfoResponseDTO = null;
        BufferedReader br = null;
        try {
            br = new BufferedReader(new InputStreamReader(httpConn.getInputStream()));
            String line;
            final StringBuilder sb = new StringBuilder();
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            if (httpConn.getContentType().startsWith("application/xml")) {
                addressInfoResponseDTO = getDTOFromXml(sb.toString(), AddressInfoResponseDTO.class);
            } else {
                log.error("Fail to get address details from eir API : " + sb.toString());
                setError(ErrorEnum.GENERIC_API_EXCEPTION, "Fail to get address details from eir API");
            }
        } catch (Exception e) {
            log.error("Fail to get address details from eir API : " + e.getMessage());
            throw new ValidationException("Fail to get address details from eir API");
        } finally {
            try {
                if (br != null)
                    br.close();
            } catch (IOException e) {
                log.error("Fail to get address details from eir API : " + e.getMessage());
                throw new ValidationException("Fail to get address details from eir API");
            }
        }
        return addressInfoResponseDTO;
    }

    /**
     * Get address details
     *
     * @param apiUrl the api url
     * @return the address details
     * @throws ValidationException the validation exception
     */
    private AddressInfoResponseDTO getAddressDetails(String apiUrl) throws ValidationException {

        AddressInfoResponseDTO addressInfoResponseDTO = null;

        if (!StringUtils.isBlank(apiUrl)) {
            try {

                int retriesNumber = getRetriesNumber();
                int retryDelay = getRetryDelay();

                int status = 0;
                int retry = 0;
                boolean delay = false;

                do {
                    if (delay) {
                        Thread.sleep(retryDelay);
                    }
                    HttpURLConnection httpConn = null;
                    try {
                        URL url = new URL(apiUrl);
                        httpConn = (HttpURLConnection) url.openConnection();
                        if (httpConn != null)
                            httpConn.disconnect();
                        httpConn.setDoOutput(true);
                        httpConn.setRequestProperty("Content-Type", "application/json");
                        httpConn.setRequestProperty("Accept", "application/json");
                        httpConn.setRequestMethod("GET");
                        httpConn.setUseCaches(false);

                        /* To remove */
                        String userCredentials = "opencell.admin:opencell.admin";
                        String basicAuth = "Basic " + new String(Base64.getEncoder().encode(userCredentials.getBytes()));
                        httpConn.setRequestProperty("Authorization", basicAuth);
                        /* To remove */

                        status = httpConn.getResponseCode();

                        switch (status) {
                        case HttpURLConnection.HTTP_OK:
                            log.info("OK : HTTP code : " + status);
                            // Parse Response.
                            addressInfoResponseDTO = getAddressInfoResponse(httpConn);
                            break; // if response is returned exit, fine, go on
                        case HttpURLConnection.HTTP_BAD_REQUEST:
                            log.info("Invalid request details : HTTP error code : " + status);
                            break;// retry
                        case HttpURLConnection.HTTP_NOT_FOUND:
                            log.info("No data found for request : HTTP error code : " + status);
                            break;// retry
                        case HttpURLConnection.HTTP_INTERNAL_ERROR:
                            log.info("General Exception or Timeout on backend. : HTTP error code : " + status);
                            break;// retry
                        case HttpURLConnection.HTTP_GATEWAY_TIMEOUT:
                            log.info("Gateway timeout : HTTP error code : " + status);
                            break;// retry
                        case HttpURLConnection.HTTP_UNAVAILABLE:
                            log.info("Unavailable : HTTP error code : " + status);
                            break;// retry, server is unstable
                        default:
                            log.info("Failed : HTTP error code : " + status);
                            break; // retry
                        }
                        // retry
                        retry++;
                        log.info("Failed retry " + retry + "/" + retriesNumber);
                        delay = true;
                    } catch (MalformedURLException e) {
                        log.error("Fail to get address details from eir API : " + e.getMessage());
                        throw new ValidationException("Malformed URL API to get eir address");
                    } catch (IOException e) {
                        log.error("Fail to get address details from eir API : " + e.getMessage());
                        throw new ValidationException("Fail to get address details from eir API");
                    } catch (Exception e) {
                        log.error("Fail to get address details from eir API : " + e.getMessage());
                        throw new ValidationException("Fail to get address details from eir API");
                    } finally {
                        if (httpConn != null) {
                            httpConn.disconnect();
                        }
                    }

                } while (status != HttpURLConnection.HTTP_OK && retry < retriesNumber);

            } catch (InterruptedException e) {
                log.error("Fail to get address details from eir API : " + e.getMessage());
                throw new ValidationException("Fail to get address details from eir API");
            }
        }

        return addressInfoResponseDTO;
    }

    /**
     * Get end point CF value
     *
     * @param orderLineDTO the order line DTO
     * @param address the address
     * @return the end point CF value
     */
    private Map<String, String> getEndPointCfValue(OrderLineDTO orderLineDTO, String address) {
        Map<String, String> cfValue = new HashMap<>();

        StringBuilder keys = new StringBuilder();
        keys.append(orderLineDTO.itemId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.itemComponentId)) {
            keys.append(orderLineDTO.itemComponentId);
        }

        StringBuilder values = new StringBuilder();
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPID.name()))) {
            values.append(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPID.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPTYPE.name()))) {
            values.append(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPTYPE.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPEND.name()))) {
            values.append(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPEND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPADDRESS.name()))) {
            values.append(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPADDRESS.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPVERSION.name()))) {
            values.append(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPVERSION.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPSTATUS.name()))) {
            values.append(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPSTATUS.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPSTART.name()))) {
            values.append(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPSTART.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPSTATE.name()))) {
            values.append(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPSTATE.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPORDER.name()))) {
            values.append(orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPORDER.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(address);

        cfValue.put(keys.toString(), values.toString());
        return cfValue;
    }

    /**
     * Get association CF value
     *
     * @param orderLineDTO the order line DTO
     * @param address the address
     * @return association CF value
     */
    private Map<String, String> getAssociationCfValue(OrderLineDTO orderLineDTO, String address) {
        Map<String, String> cfValue = new HashMap<>();

        StringBuilder keys = new StringBuilder();
        keys.append(orderLineDTO.itemId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.itemComponentId)) {
            keys.append(orderLineDTO.itemComponentId);
        }

        StringBuilder values = new StringBuilder();
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCCIRCID.name()))) {
            values.append(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCCIRCID.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCTYPE.name()))) {
            values.append(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCTYPE.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCEND.name()))) {
            values.append(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCEND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCTRIGGER.name()))) {
            values.append(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCTRIGGER.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCVERSION.name()))) {
            values.append(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCVERSION.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCSTATUS.name()))) {
            values.append(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCSTATUS.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCSTART.name()))) {
            values.append(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCSTART.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCSTATE.name()))) {
            values.append(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCSTATE.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCORDER.name()))) {
            values.append(orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCORDER.name()));
        }
        if (!StringUtils.isBlank(address)) {
            values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(address);
        }

        cfValue.put(keys.toString(), values.toString());
        return cfValue;
    }

    /**
     * Update end point CF
     *
     * @param orderLineDTO the order line DTO
     * @param endDate the end date
     * @param epend the epend
     * @param address the address
     * @param lastCfValue the last CF value
     */
    private void updateEndPointCf(OrderLineDTO orderLineDTO, Date endDate, String epend, String address, CustomFieldValue lastCfValue) {
        if (EndEnum.A.name().equalsIgnoreCase(epend) || EndEnum.B.name().equalsIgnoreCase(epend)) {
            Map<String, String> cfValue = getEndPointCfValue(orderLineDTO, address);
            updateCfValue(orderLineDTO.subscription, orderLineDTO.billEffectDate, "END_POINT_" + epend.toUpperCase(), endDate, cfValue, lastCfValue);
        }
    }

    /**
     * find CF value by key
     *
     * @param subscription the subscription
     * @param cfCode the CF code
     * @param keyCfValue the key CF value
     * @return
     */
    private CustomFieldValue findCfValueByKey(Subscription subscription, String cfCode, String keyCfValue) {

        if (subscription.getCfValuesNullSafe() != null) {
            Map<String, List<CustomFieldValue>> valuesByCode = subscription.getCfValuesNullSafe().getValuesByCode();
            if (valuesByCode != null && valuesByCode.get(cfCode) != null) {
                List<CustomFieldValue> customFieldValueList = valuesByCode.get(cfCode);
                for (CustomFieldValue customFieldValue : customFieldValueList) {

                    if (customFieldValue != null) {
                        Map<String, String> cfValue = (Map<String, String>) customFieldValue.getValue();
                        if (cfValue.containsKey(keyCfValue)) {
                            return customFieldValue;
                        }

                    }
                }
            }
        }
        return null;
    }

    /**
     * Create subscription
     *
     * @param orderLineDTO the order line DTO
     * @return the subscription
     */
    private Subscription createSubscription(OrderLineDTO orderLineDTO) {

        UserAccount userAccount = userAccountService.findByCode(orderLineDTO.deducedServiceId);
        // if user account doesn't exist, create new one with code = Service_Id
        if (userAccount == null) {
            userAccount = new UserAccount();
            userAccount.setCode(orderLineDTO.deducedServiceId);
            userAccountService.createUserAccount(orderLineDTO.billingAccount, userAccount);
            newUserAccounts.add(userAccount);
        }

        Subscription subscription = new Subscription();
        subscription.setCode(orderLineDTO.deducedServiceId);
        subscription.setSeller(sellerService.findByCode(OPENEIR_SELLER_CODE));
        subscription.setUserAccount(userAccountService.findByCode(orderLineDTO.deducedServiceId));
        subscription.setOffer(orderLineDTO.offerTemplate);
        Date billEffectDate = DateUtils.truncateTime(orderLineDTO.billEffectDate);
        subscription.setSubscriptionDate(billEffectDate);
        subscription.setStatusDate(billEffectDate);

        /** 2 : execute PO2-491 [OMS Min Term: Determine End of Engagement Date] **/
        Date endEngagementDate = DateUtils.truncateTime(getEndEngagementDate(orderLineDTO));
        subscription.setEndAgreementDate(endEngagementDate);

        DatePeriod datePeriod = new DatePeriod(orderLineDTO.billEffectDate, null);
        subscription.getCfValuesNullSafe().setValue("UAN", datePeriod, -1, orderLineDTO.uan);
        subscriptionService.create(subscription);
        createAccessPoint(orderLineDTO, subscription);
        newSubscriptions.put(orderLineDTO.serviceId, subscription);
        return subscription;
    }

    /**
     * Create access point
     *
     * @param orderLineDTO the order line DTO
     * @param subscription the subscription
     */
    private void createAccessPoint(OrderLineDTO orderLineDTO, Subscription subscription) {
        Access accessPoint = new Access();
        accessPoint.setAccessUserId(orderLineDTO.deducedServiceId);
        accessPoint.setStartDate(orderLineDTO.billEffectDate);
        accessPoint.setSubscription(subscription);
        subscription.getAccessPoints().add(accessPoint);
        subscriptionService.update(subscription);
    }

    /**
     * Get subscription main details CF value
     *
     * @param orderLineDTO the order line DTO
     * @return the subscription main details CF value
     */
    private Map<String, String> getSubscriptionMainDetailsCfValue(OrderLineDTO orderLineDTO) {
        Map<String, String> cfValue = new HashMap<>();

        StringBuilder keys = new StringBuilder();
        keys.append(orderLineDTO.itemId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.itemComponentId)) {
            keys.append(orderLineDTO.itemComponentId);
        }

        StringBuilder values = new StringBuilder();
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.SERVICE_TYPE.name()))) {
            values.append(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.SERVICE_TYPE.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.STATUS.name()))) {
            values.append(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.STATUS.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.STATE.name()))) {
            values.append(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.STATE.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.PROCESSINGSTATUS.name()))) {
            values.append(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.PROCESSINGSTATUS.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.COMMITMENT_PERIOD.name()))) {
            values.append(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.COMMITMENT_PERIOD.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.CMMITMNTTREATMENT.name()))) {
            values.append(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.CMMITMNTTREATMENT.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.XREF.name()))) {
            values.append(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.XREF.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.SUBORDER.name()))) {
            values.append(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.SUBORDER.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.PRICE_PLAN.name()))) {
            values.append(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.PRICE_PLAN.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.AMT.name()))) {
            // AMT. When present, the value should override the charge from the OC Tariff Plan
            values.append(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.AMT.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.PRICEPLAN.name()))) {
            values.append(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.PRICEPLAN.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.SITE_GENERAL_NUMBER.name()))) {
            values.append(orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.SITE_GENERAL_NUMBER.name()));
        }

        cfValue.put(keys.toString(), values.toString());
        return cfValue;
    }

    /**
     * Get service template
     *
     * @param orderLineDTO the order line DTO
     * @return the service template
     */
    private ServiceTemplate getServiceTemplate(OrderLineDTO orderLineDTO) {
        BigDecimal tariff = null;
        String serviceCode = null;
        String exchangeClass = getExchangeClass(orderLineDTO);
        if (ChargeTypeEnum.O.name().equalsIgnoreCase(orderLineDTO.chargeType)) {
            serviceCode = ServiceCodeEnum.WBO_O_SERVICE.name();
            tariff = getTariff(orderLineDTO, ChargeTypeEnum.O.name(), exchangeClass);
        } else if (ChargeTypeEnum.R.name().equalsIgnoreCase(orderLineDTO.chargeType)) {
            serviceCode = ServiceCodeEnum.WBO_R_SERVICE.name();
            tariff = getTariff(orderLineDTO, ChargeTypeEnum.R.name(), exchangeClass);
        } else {
            serviceCode = ServiceCodeEnum.WBO_OR_SERVICE.name();
            tariff = getTariff(orderLineDTO, ChargeTypeEnum.O.name(), exchangeClass);
            if (tariff == null) {
                tariff = getTariff(orderLineDTO, ChargeTypeEnum.R.name(), exchangeClass);
            }
        }

        if (tariff == null) {
            setError(ErrorEnum.INVALID_DATA, "Tariff is not found for " + orderLineDTO.billCode);
            return null;
        }

        ServiceTemplate serviceTemplate = serviceTemplateService.findByCode(serviceCode);
        if (serviceTemplate == null) {
            setError(ErrorEnum.INVALID_DATA, "The service template is not found");
        }
        return serviceTemplate;
    }

    /**
     * Get the main service instance
     *
     * @param subscription the subscription
     * @return the main service instance
     */
    private ServiceInstance getMainServiceInstance(Subscription subscription) {
        if (subscription != null && subscription.getServiceInstances() != null) {
            for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
                String serviceType = (String) serviceInstance.getCfValuesNullSafe().getValue("SERVICE_TYPE");
                if (!StringUtils.isBlank(serviceType) && ServiceTypeEnum.MAIN.getLabel().equalsIgnoreCase(serviceType.trim())) {
                    return serviceInstance;
                }
            }
        }
        return null;
    }

    /**
     * Cease all ancillary services of subscription
     *
     * @param orderLineDTO the order line DTO
     */
    private void ceaseAllAncillaryServices(OrderLineDTO orderLineDTO) {
        if (orderDTO.errorCode != null) {
            return;
        }
        if (orderLineDTO.subscription != null && orderLineDTO.subscription.getServiceInstances() != null) {
            for (ServiceInstance serviceInstance : orderLineDTO.subscription.getServiceInstances()) {
                if (serviceInstance.getStatus() == InstanceStatusEnum.ACTIVE) {
                    String serviceType = (String) serviceInstance.getCfValuesNullSafe().getValue("SERVICE_TYPE");
                    if (!StringUtils.isBlank(serviceType) && ServiceTypeEnum.ANCILLARY.getLabel().equalsIgnoreCase(serviceType.trim())) {
                        ceaseAncillary(orderLineDTO, serviceInstance);
                    }
                }
            }
        }
    }

    /**
     * Cease circuit
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void ceaseCircuit(OrderLineDTO orderLineDTO) throws BusinessException {

        /** execute PO2-413 [OMS Cease Circuit] **/

        String state = orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.STATE.name());

        /** Step 4 : Check if the STATE of the incoming Subscription Main Details bill item = CA **/
        if (StateEnum.CA.name().equalsIgnoreCase(state)) {
            /** Step 5 : If the STATE = CA, then execute PO2-484: [OMS Cancelled Orders] **/

            /** execute PO2-486 [OMS Cancelled Orders] **/

        } else {

            /** Step 6 : check to see if a REF_ORDER exists **/
            if (!StringUtils.isBlank(orderDTO.refOrderId)) {
                /**
                 * Step 7 : using the value in the tag, search the completed orders in the system to determine if the (referenced) order has been successfully processed
                 **/
                /** Step 8 : Was a complimentary order found? **/
                if (refOrderDTO != null && refOrderDTO.refOrder != null
                        && (refOrderDTO.refOrder.getStatus() == OrderStatusEnum.COMPLETED || refOrderDTO.refOrder.getStatus() == OrderStatusEnum.DEFERRED)) {

                    /** Step 10 : Release the complementary order for processing **/
                    refOrderDTO.refOrder.setStatus(OrderStatusEnum.PENDING);
                    // TODO : uncomment this when going to the jar
                    // orderService.update(refOrderDTO.refOrder);

                    /** Step 10A : If the STATE (of the Complimentary Order) on the Main Subscription Details = CA, go to step 12 (i.e. do not execute step 11) **/
                    String stateRefOrder = getMainItemAttribute(ChargesAttributeNamesEnum.STATE.name());

                    if (!StateEnum.CA.name().equalsIgnoreCase(stateRefOrder)) {
                        /** Step 11 : Retrieve Effect Date from REF_ORDER (PR) order and set this as the Cease Effect Date **/
                        orderLineDTO.ceaseEffectDate = refOrderDTO.getMainItemElement(OrderItemsNamesEnum.Effect_Date.name());
                    }
                } else {
                    /** Step 9 : set the status of the CE order to DEFERRED **/
                    orderDTO.status = OrderStatusEnum.DEFERRED;
                    return;
                }

            }

            /** Step 12 : Using the Service Id from the order, locate the subscriber **/
            /** Step 13 : Was the subscriber found? **/
            if (orderLineDTO.subscription == null) {
                /** Step 14 : If the subscriber was NOT found, send the order to Error Management **/
                setError(ErrorEnum.INVALID_DATA, "The subscription " + orderLineDTO.deducedServiceId + " is not found");
                return;
            }

            if (orderLineDTO.subscription.getStatus() == SubscriptionStatusEnum.RESILIATED) {
                return;
            }

            /** Step 15 : If the subscriber was found, check if unprocessed Charge bill items exist **/
            /** Step 16 : If unprocessed Charge bill items exist, check if the STATUS of the Charge bill item = AC **/
            /** Step 17 : If STATUS =AC, execute PO2-474: [OMS Orders: Process Service Charges] **/
            /** Step 18 : If STATUS is NOT =AC, Obtain Item Component Id from the CE bill item **/
            // all the unprocessed Charge bill items are treated first before making the cease circuit

            /** Step 20 : Check if active Ancillary Services exist on the subscriber **/
            /** Step 21 : If an active service exists, obtain Item Component Id from the Ancillary Service **/
            /** Step 19 : Execute PO2-490: [OMS Cease Ancillary] **/
            // itemType = Subscription main details && action = CE
            ceaseAllAncillaryServices(orderLineDTO);

            /** Step 22 : Update the Attribute Values of the End Points **/
            ceaseEndPoints(orderLineDTO);

            /** Step 23 : Update the Attribute Values of the Associations **/
            ceaseAssociations(orderLineDTO);

            /** Step 24 : Update the Main Subscription Details Attributes with the values from the bill item **/
            // subscriptionMainDetails(orderLineDTO);

            /** Step 25 : Execute PO2-694: [OMS Cease Primary] **/
            ceasePrimary(orderLineDTO);

            /** Step 26 : Cease the Subscriber **/
            ceaseSubscription(orderLineDTO);

            // TODO : remove this when going to the jar
            if (refOrderDTO != null && refOrderDTO.refOrder != null && refOrderDTO.refOrder.getStatus() == OrderStatusEnum.PENDING) {
                orderService.update(refOrderDTO.refOrder);
            }
        }
    }

    /**
     * Change end point
     *
     * @param orderLineDTO the order line DTO
     * @param epend the epend attribute
     * @throws BusinessException the business exception
     */
    private void changeEndPoint(OrderLineDTO orderLineDTO, String epend) throws BusinessException {
        CustomFieldValue lastCfValue = getLastCfVersion(orderLineDTO.subscription, "END_POINT_" + epend.toUpperCase(), orderLineDTO.billEffectDate, false);
        if (orderDTO.errorCode != null) {
            return;
        }

        Map<String, String> endPointCF = null;
        if (lastCfValue != null && lastCfValue.getValue() != null) {
            Map<String, String> cfValue = (Map<String, String>) lastCfValue.getValue();

            for (Map.Entry<String, String> entry : cfValue.entrySet()) {
                Map<String, String> value = (Map<String, String>) getCFValue(orderLineDTO.subscription, "END_POINT_" + epend.toUpperCase(), entry.getValue());
                if (value != null && !value.isEmpty()) {
                    endPointCF = value;
                    break;
                }
            }
        }

        Map<String, Object> newEndPointValues = new HashMap<>(endPointCF);
        newEndPointValues.put(EndPointAttributeNamesEnum.EPSTATUS.name(), ItemStatusEnum.CE.name());

        Map<String, String> newEndPointCF = new HashMap<>();
        newEndPointCF.put(orderLineDTO.itemId + CustomFieldValue.MATRIX_KEY_SEPARATOR + orderLineDTO.itemComponentId,
            getCFValue(orderLineDTO.subscription, "END_POINT_" + epend.toUpperCase(), newEndPointValues));
        updateCfValue(orderLineDTO.subscription, orderLineDTO.billEffectDate, "END_POINT_" + epend.toUpperCase(), orderLineDTO.rentalLiabilityDate, newEndPointCF, lastCfValue);
    }

    /**
     * Change association
     *
     * @param orderLineDTO the order line DTO
     * @param assocend the
     * @throws BusinessException the business exception
     */
    private void changeAssociation(OrderLineDTO orderLineDTO, String assocend) throws BusinessException {
        CustomFieldValue lastCfValue = getLastCfVersion(orderLineDTO.subscription, "ASSOCIATION_" + assocend.toUpperCase(), orderLineDTO.billEffectDate, false);
        if (orderDTO.errorCode != null) {
            return;
        }

        Map<String, String> associationCF = null;
        if (lastCfValue != null && lastCfValue.getValue() != null) {
            Map<String, String> cfValue = (Map<String, String>) lastCfValue.getValue();

            for (Map.Entry<String, String> entry : cfValue.entrySet()) {
                Map<String, String> value = (Map<String, String>) getCFValue(orderLineDTO.subscription, "ASSOCIATION_" + assocend.toUpperCase(), entry.getValue());
                if (value != null && !value.isEmpty()) {
                    associationCF = value;
                    break;
                }
            }
        }

        Map<String, Object> newAssociationValues = new HashMap<>(associationCF);
        newAssociationValues.put(AssociationAttributeNamesEnum.ASSOCSTATUS.name(), ItemStatusEnum.CE.name());

        Map<String, String> newAssociationCF = new HashMap<>();
        newAssociationCF.put(orderLineDTO.itemId + CustomFieldValue.MATRIX_KEY_SEPARATOR + orderLineDTO.itemComponentId,
            getCFValue(orderLineDTO.subscription, "ASSOCIATION_" + assocend.toUpperCase(), newAssociationValues));
        updateCfValue(orderLineDTO.subscription, orderLineDTO.billEffectDate, "ASSOCIATION_" + assocend.toUpperCase(), orderLineDTO.rentalLiabilityDate, newAssociationCF,
            lastCfValue);
    }

    /**
     * Cease change end points
     *
     * @throws BusinessException the business exception
     */
    private void ceaseEndPoints(OrderLineDTO orderLineDTO) throws BusinessException {
        if (orderDTO.errorCode != null) {
            return;
        }
        Map<String, String> endPointA = (Map<String, String>) orderLineDTO.subscription.getCfValuesNullSafe().getValue("END_POINT_A", orderLineDTO.billEffectDate);
        if (endPointA != null && !endPointA.isEmpty()) {
            changeEndPoint(orderLineDTO, EndEnum.A.name());
        }
        Map<String, String> endPointB = (Map<String, String>) orderLineDTO.subscription.getCfValuesNullSafe().getValue("END_POINT_B", orderLineDTO.billEffectDate);
        if (endPointB != null && !endPointB.isEmpty()) {
            changeEndPoint(orderLineDTO, EndEnum.B.name());
        }
    }

    /**
     * Cease associations case
     *
     * @throws BusinessException the business exception
     */
    private void ceaseAssociations(OrderLineDTO orderLineDTO) throws BusinessException {
        if (orderDTO.errorCode != null) {
            return;
        }
        Map<String, String> associationA = (Map<String, String>) orderLineDTO.subscription.getCfValuesNullSafe().getValue("ASSOCIATION_A", orderLineDTO.billEffectDate);
        if (associationA != null && !associationA.isEmpty()) {
            changeAssociation(orderLineDTO, EndEnum.A.name());
        }
        Map<String, String> associationB = (Map<String, String>) orderLineDTO.subscription.getCfValuesNullSafe().getValue("ASSOCIATION_B", orderLineDTO.billEffectDate);
        if (associationB != null && !associationB.isEmpty()) {
            changeAssociation(orderLineDTO, EndEnum.B.name());
        }
    }

    /**
     * cease subscription
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void ceaseSubscription(OrderLineDTO orderLineDTO) throws BusinessException {
        if (orderDTO.errorCode != null) {
            return;
        }

        if (orderLineDTO.subscription.getStatus() == SubscriptionStatusEnum.RESILIATED) {
            return;
        }

        Map<String, Date> accessPointsMap = new HashMap<>();
        List<Access> accessPoints = orderLineDTO.subscription.getAccessPoints();
        if (accessPoints != null && !accessPoints.isEmpty()) {
            for (Access a : accessPoints) {
                accessPointsMap.put(a.getAccessUserId(), a.getEndDate());
            }
        }

        SubscriptionTerminationReason terminationReason = null;
        try {
            terminationReason = subscriptionTerminationReasonService.findByCodeReason(TERMINATION_REASON_MAIN);
        } catch (Exception e) {

        }
        subscriptionService.terminateSubscription(orderLineDTO.subscription, orderLineDTO.billEffectDate, terminationReason, orderLineDTO.operatorOrderNumber);
        // Add OLD_ prefix to subscription code in order to can create the new one with same code
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
        String oldPrefix = "OLD" + sdf.format(new Date().getTime());
        UserAccount userAccount = userAccountService.findByCode(orderLineDTO.serviceId);
        userAccountService.userAccountTermination(userAccount, orderLineDTO.billEffectDate, terminationReason);
        userAccount.setCode(oldPrefix + "_" + userAccount.getCode());
        userAccountService.update(userAccount);

        if (!accessPointsMap.isEmpty()) {
            for (Access access : accessPoints) {
                Date endDate = accessPointsMap.get(access.getAccessUserId());
                if (endDate != null) {
                    access.setEndDate(endDate);
                    accessService.update(access);
                }
            }
        }

        orderLineDTO.subscription.setCode(oldPrefix + "_" + orderLineDTO.subscription.getCode());
        subscriptionService.update(orderLineDTO.subscription);
        log.info("Subscription terminated : " + orderLineDTO.subscription.getCode());
    }

    /**
     * Get cease effect date
     *
     * @param orderLineDTO the order line DTO
     * @param serviceInstance the service instance
     * @return cease effect date
     */
    private Date getCeaseEffectDate(OrderLineDTO orderLineDTO, ServiceInstance serviceInstance) {
        Date ceaseEffectDate = orderLineDTO.ceaseEffectDate;
        if (ceaseEffectDate == null) {
            ceaseEffectDate = orderLineDTO.billEffectDate;
        } else if (ceaseEffectDate.before(serviceInstance.getSubscription().getSubscriptionDate())) {
            setError(ErrorEnum.INVALID_DATA, "Termination date can not be before the subscription date for the service " + serviceInstance.getCode());
            return null;
        }
        return ceaseEffectDate;
    }

    /**
     * Terminate ancillary service
     *
     * @param orderLineDTO the order line DTO
     * @param ancillaryService the ancillary service
     * @throws BusinessException the business exception
     */
    private void terminateAncillaryService(OrderLineDTO orderLineDTO, ServiceInstance ancillaryService) throws BusinessException {
        if (orderDTO.errorCode != null) {
            return;
        }
        try {
            Date ceaseEffectDate = getCeaseEffectDate(orderLineDTO, ancillaryService);
            if (ceaseEffectDate != null) {
                SubscriptionTerminationReason terminationReasonAncillary = subscriptionTerminationReasonService.findByCodeReason(TERMINATION_REASON_ANCILLARY);
                serviceInstanceService.terminateService(ancillaryService, DateUtils.truncateTime(ceaseEffectDate), terminationReasonAncillary, orderLineDTO.operatorOrderNumber);
            }
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }

    /**
     * Terminate primary service
     *
     * @param orderLineDTO the order line DTO
     * @param primaryService the primary service
     * @throws BusinessException the business exception
     */
    private void terminatePrimaryService(OrderLineDTO orderLineDTO, ServiceInstance primaryService) throws BusinessException {
        if (orderDTO.errorCode != null) {
            return;
        }
        try {
            Date ceaseEffectDate = getCeaseEffectDate(orderLineDTO, primaryService);
            if (ceaseEffectDate != null) {
                SubscriptionTerminationReason terminationReasonAncillary = subscriptionTerminationReasonService.findByCodeReason(TERMINATION_REASON_MAIN);
                serviceInstanceService.terminateService(primaryService, DateUtils.truncateTime(ceaseEffectDate), terminationReasonAncillary, orderLineDTO.operatorOrderNumber);
            }
        } catch (Exception e) {
            throw new BusinessException(e);
        }
    }

    /**
     * Get catalog
     *
     * @param billCode the bill code
     * @param billEffectDate the bill effect date
     * @param offerTemplate the offer template
     * @return the catalog
     */
    private Map<String, String> getCatalog(String billCode, Date billEffectDate, OfferTemplate offerTemplate) {
        Map<String, String> catalog = null;
        String keyToMatch;
        if (!StringUtils.isBlank(billCode) && billEffectDate != null && offerTemplate != null) {
            String chargeType = ChargeTypeEnum.R.name();
            keyToMatch = billCode + CustomFieldValue.MATRIX_KEY_SEPARATOR + chargeType;
            catalog = (Map<String, String>) customFieldInstanceService.getCFValueByClosestMatch(offerTemplate, OfferTemplateOmsCFsEnum.NEW_CATALOG.name(), billEffectDate,
                keyToMatch);
            if (catalog == null) {
                chargeType = ChargeTypeEnum.O.name();
                keyToMatch = billCode + CustomFieldValue.MATRIX_KEY_SEPARATOR + chargeType;
                catalog = (Map<String, String>) customFieldInstanceService.getCFValueByClosestMatch(offerTemplate, OfferTemplateOmsCFsEnum.NEW_CATALOG.name(), billEffectDate,
                    keyToMatch);
            }
        }
        return catalog;
    }

    /**
     * Apply service to apply.
     *
     * @param orderLineDTO the order line DTO
     * @param serviceToApply the service to apply
     * @throws BusinessException the business exception
     */
    private void applyServiceToApply(OrderLineDTO orderLineDTO, String serviceToApply) throws BusinessException {
        if (serviceToApply != null) {
            OfferTemplate miscOfferTemplate = offerTemplateService.findByCode(MISC_OFFER_TEMPLATE_CODE);
            if (miscOfferTemplate == null) {
                setError(ErrorEnum.INVALID_DATA, "The offer template MISC_WB is not found");
                return;
            }

            Map<String, String> catalog = getCatalog(serviceToApply, orderLineDTO.billEffectDate, miscOfferTemplate);
            if (catalog == null) {
                setError(ErrorEnum.INVALID_DATA, "The service " + serviceToApply + " is not found for date " + orderLineDTO.billEffectDate);
                return;
            }

            OneShotChargeTemplate chargetemplate = chargeTemplateService.findByCode(MISC_CHARGE_TEMPLATE_CODE);
            if (chargetemplate == null) {
                setError(ErrorEnum.INVALID_DATA, "The one-shot charge " + MISC_CHARGE_TEMPLATE_CODE + " is not found");
                return;
            }

            OrderLineDTO oneShotOrderLineDTO = SerializationUtils.clone(orderLineDTO);
            oneShotOrderLineDTO.billCode = serviceToApply;
            BigDecimal oneShotTariff = getOneShotTariff(oneShotOrderLineDTO);
            if (oneShotTariff == null) {
                setError(ErrorEnum.INVALID_DATA, "The tariff " + serviceToApply + " is not found for a misc charge");
                return;
            }

            oneShotChargeInstanceService.oneShotChargeApplication(orderLineDTO.subscription, chargetemplate, null, orderLineDTO.billEffectDate, oneShotTariff, null, null,
                serviceToApply, "0", null, orderLineDTO.operatorOrderNumber, true);
        }
    }

    /**
     * Calculate cease charges
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void CalculateCeaseCharges(OrderLineDTO orderLineDTO) throws BusinessException {
        if (orderDTO.errorCode != null) {
            return;
        }
        String migrationCode = (String) orderLineDTO.catalog.get(Catalog.MIGRATION_CODE.name());
        if (StringUtils.isBlank(migrationCode)) {
            migrationCode = "*|*|=0";
        }

        Map<String, String> cf = (Map<String, String>) providerService.getProvider().getCfValuesNullSafe().getValue("MIGRATION_MATRIX");
        if (cf == null) {
            // setError(ErrorEnum.INVALID_DATA, "Migration Matrix is not found");
            return;
        }

        String serviceMigrationKey = getProductMigrationRules(migrationCode, migrationCode, cf);
        log.info("#######serviceMigrationKey########" + serviceMigrationKey);
        String ceaseData = cf.get(serviceMigrationKey);

        if (ceaseData == null) {
            // setError(ErrorEnum.INVALID_DATA, "Migration Matrix lacks cease data");
            return;
        }

        Map<String, String> value = (Map<String, String>) getCFValue(providerService.getProvider(), "MIGRATION_MATRIX", ceaseData);
        String serviceToApply = value.get(ServiceMigrationAttributeNamesEnum.BILL_CODE_TO_APPLY);

        applyServiceToApply(orderLineDTO, serviceToApply);

    }

    /**
     * Calculate rental charges
     *
     * @param orderLineDTO the order line DTO
     * @param serviceInstance the service instance
     * @throws BusinessException the business exception
     */
    private void calculateRentalCharges(OrderLineDTO orderLineDTO, ServiceInstance serviceInstance) throws BusinessException {
        if (orderDTO.errorCode != null) {
            return;
        }
        if (orderLineDTO.rentalLiabilityDate != null && orderLineDTO.rentalLiabilityDate.after(new Date())) {
            for (RecurringChargeInstance recurringChargeInstance : serviceInstance.getRecurringChargeInstances()) {
                recurringChargeInstanceService.applyRecurringCharge(recurringChargeInstance.getId(), orderLineDTO.rentalLiabilityDate, true);
            }
        }
    }

    private void updateCharges(OrderLineDTO orderLineDTO, ServiceInstance ancillaryService) {
        if (orderDTO.errorCode != null) {
            return;
        }

        Map<String, String> chargesCF = getCharges(orderLineDTO, ancillaryService);
        if (orderDTO.errorCode != null) {
            return;
        }

        /** Step 10 : If a bill item (to cease the ancillary service) does NOT exist **/
        if (chargesCF == null) {
            /**
             * Step 7 : Create a new version of the attributes group and populate the Charge Group Attributes with the values from the bill item. Use the effect date from step 6 as
             * the end date of the group version
             **/
            createNewVersionChargesCf(orderLineDTO, ancillaryService, null);
        } else {
            /**
             * Step 11 : If a bill item (to cease the ancillary service) does exist, create a new version of the Charge Attribute Group using existing attribute values and set
             * status = CE
             **/
            Map<String, Object> newChargesValues = new HashMap<>(chargesCF);
            newChargesValues.put(ChargesAttributeNamesEnum.STATUS.name(), ItemStatusEnum.CE.name());

            Map<String, String> newChargesCF = new HashMap<>();

            String itemId = null;
            String itemComponentId = null;
            if (ItemTypeEnum.SUBSCRIPTION_MAIN_DETAILS.getLabel().equalsIgnoreCase(orderLineDTO.itemType)) {
                // Cease Circuit case
                itemId = (String) ancillaryService.getCfValuesNullSafe().getValue("ITEM_ID");
                itemComponentId = (String) ancillaryService.getCfValuesNullSafe().getValue("ITEM_COMPONENT_ID");
            } else {
                itemId = orderLineDTO.itemId;
                itemComponentId = orderLineDTO.itemComponentId;
            }

            newChargesCF.put(itemId + CustomFieldValue.MATRIX_KEY_SEPARATOR + itemComponentId,
                getCFValue(ancillaryService, ServiceInstanceOmsCFsEnum.CHARGES.name(), newChargesValues));
            createNewVersionChargesCf(orderLineDTO, ancillaryService, newChargesCF);
        }
    }

    /**
     * Cease Ancillary
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void ceaseAncillary(OrderLineDTO orderLineDTO) throws BusinessException {
        ServiceInstance ancillaryService = getAncillaryServiceInstance(orderLineDTO);
        ceaseAncillary(orderLineDTO, ancillaryService);
    }

    /**
     * Cease Ancillary
     *
     * @param orderLineDTO the order line DTO
     * @param ancillaryService the ancillary service
     * @throws BusinessException the business exception
     */
    private void ceaseAncillary(OrderLineDTO orderLineDTO, ServiceInstance ancillaryService) throws BusinessException {
        if (ancillaryService == null || ancillaryService.getStatus() == InstanceStatusEnum.TERMINATED) {
            return;
        }

        /** Step 6 : If the Main Subscription Details bill item status is NOT = CE, then use the Effect Date from the bill item as the Rental Liability Date **/
        /**
         * Step 8 : If the Main Subscription Details bill item status = CE, then use the Effect Date from the Main Subscription bill item on the CE order
         **/
        orderLineDTO.rentalLiabilityDate = orderLineDTO.billEffectDate;
        String status = orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.STATUS.name());
        if (ItemTypeEnum.SUBSCRIPTION_MAIN_DETAILS.getLabel().equalsIgnoreCase(orderLineDTO.itemType) && ItemStatusEnum.CE.name().equalsIgnoreCase(status)) {
            /** Step 9 : Execute PO2-312: [OMS Determine Rental Liability Date] **/
            orderLineDTO.rentalLiabilityDate = getRentalLiabilityDate(orderLineDTO);
        }
        ancillaryService.getCfValuesNullSafe().setValue(ServiceInstanceOmsCFsEnum.RENTAL_LIABILITY_DATE.name(), orderLineDTO.rentalLiabilityDate);
        ancillaryService.setEndAgreementDate(DateUtils.truncateTime(orderLineDTO.rentalLiabilityDate));

        /** Step 5 : Check if the Main Subscription Details bill item status = CE **/
        /**
         * Step 7 : Create a new version of the attributes group and populate the Charge Group Attributes with the values from the bill item. Use the effect date from step 6 as the
         * end date of the group version
         **/
        /** Step 10 : If a bill item (to cease the ancillary service) does NOT exist **/
        /**
         * Step 11 : If a bill item (to cease the ancillary service) does exist, create a new version of the Charge Attribute Group using existing attribute values and set status =
         * CE
         **/
        updateCharges(orderLineDTO, ancillaryService);

        /** Step 12 : Cease the service instance and use the effective date (from either step 6 or step 8) as the cease date **/
        terminateAncillaryService(orderLineDTO, ancillaryService);

        /** Step 13 : Calculate Rental Charges to up until the Rental Liability Date **/
        // calculateRentalCharges(orderLineDTO, ancillaryService);

        /** Step 14 : Calculate Cease Charges **/
        CalculateCeaseCharges(orderLineDTO);

        /** Step 15 : Rebate any charges that were paid In-Advance. Charges must be off-set against other charges outstanding for the subscriber services **/

    }

    /**
     * Create new version Main Subscription Details
     *
     * @param orderLineDTO the order line DTO
     * @param mainServiceInstance the main service instance
     * @throws BusinessException the business exception
     */
    private void CreateNewVersionMainSubscriptionDetails(OrderLineDTO orderLineDTO, ServiceInstance mainServiceInstance) throws BusinessException {
        CustomFieldValue lastCfValue = getLastCfVersion(mainServiceInstance, ServiceInstanceOmsCFsEnum.MAIN_SUBSCRIPTION_DETAILS.name(), orderLineDTO.billEffectDate, false);
        if (orderDTO.errorCode != null) {
            return;
        }
        Date endDate = orderLineDTO.rentalLiabilityDate;
        updateSubscriptionMainDetailsCf(mainServiceInstance, orderLineDTO, endDate, lastCfValue);
        mainServiceInstance.getCfValuesNullSafe().setValue(ServiceInstanceOmsCFsEnum.RENTAL_LIABILITY_DATE.name(), orderLineDTO.rentalLiabilityDate);
        // mainServiceInstance.setEndAgreementDate(rentalLiabilityDate);
        serviceInstanceService.update(mainServiceInstance);
    }

    /**
     * Cease Primary
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void ceasePrimary(OrderLineDTO orderLineDTO) throws BusinessException {
        if (orderDTO.errorCode != null) {
            return;
        }

        /** Step 1 : Obtain the Effect Date from the Main Subscription bill item on the CE order **/
        // orderLineDTO.billEffectDate

        /** Step 2 : Execute PO2-312: [OMS Determine Rental Liability Date] **/
        orderLineDTO.rentalLiabilityDate = getRentalLiabilityDate(orderLineDTO);

        ServiceInstance mainServiceInstance = getMainServiceInstance(orderLineDTO.subscription);
        if (mainServiceInstance == null) {
            setError(ErrorEnum.INVALID_DATA, "The main service is not found on subscription" + orderLineDTO.subscription.getCode());
            return;
        }

        if (mainServiceInstance.getStatus() == InstanceStatusEnum.TERMINATED) {
            return;
        }
        mainServiceInstance.setEndAgreementDate(DateUtils.truncateTime(orderLineDTO.rentalLiabilityDate));

        /** Step 3 : Create a new version Main Subscription Details Group **/
        CreateNewVersionMainSubscriptionDetails(orderLineDTO, mainServiceInstance);

        /** Step 4 : Cease the Primary Service instance **/
        terminatePrimaryService(orderLineDTO, mainServiceInstance);

        /** Step 5 : Calculate Rental Charges to up until the Rental Liability Date **/
        // calculateRentalCharges(orderLineDTO, mainServiceInstance);

        /** Step 6 : Calculate Cease Charges **/
        CalculateCeaseCharges(orderLineDTO);

        /** Step 7 : Rebate any charges that were paid In-Advance. Charges must be off-set against other charges outstanding for the subscriber services **/
    }

    /**
     * Subscription main details
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void subscriptionMainDetails(OrderLineDTO orderLineDTO) throws BusinessException {
        if (orderDTO.errorCode != null) {
            return;
        }
        boolean isMain = ServiceTypeEnum.MAIN.getLabel().equalsIgnoreCase(getServiceType(orderLineDTO));
        if (!isMain) {
            setError(ErrorEnum.INVALID_DATA, "The provided bill code (" + orderLineDTO.billCode + ") is not a main service");
            return;
        }

        /**
         * Validate the bill item quantity
         */
        if (!validateInputQuantity(orderLineDTO)) {
            return;
        }

        // If an existing subscriber is found
        if (orderLineDTO.subscription != null) {
            /** Validate data for change subscription main details **/
            String state = orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.STATE.name());

            // If the bill code on the (Main Subscription Details) bill item is different to the bill code on the subscriber,
            // then send the order to Error Management

            ServiceInstance serviceInstance = getMainServiceInstance(orderLineDTO.subscription);
            String billCodeServiceMain = serviceInstance != null ? serviceInstance.getCode() : null;

            if (StringUtils.isBlank(billCodeServiceMain)) {
                setError(ErrorEnum.INVALID_DATA, "The main service doesn't found on the subscriber");
                return;
            }
            if (!orderLineDTO.billCode.equals(billCodeServiceMain)) {
                setError(ErrorEnum.INVALID_DATA,
                    "The provided bill code (" + orderLineDTO.billCode + ") is different to the bill code (" + billCodeServiceMain + ") on the subscriber");
                return;
            }

            // if STATUS is CE, or STATE is CA, End date can be set to the Bill Item Effect_Date
            String status = orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.STATUS.name());
            Date endDate = (ItemStatusEnum.CE.name().equalsIgnoreCase(status) || StateEnum.CA.name().equalsIgnoreCase(state)) ? orderLineDTO.billEffectDate : null;

            // if the STATE of the incoming Subscription Main Details is not CA
            if (!StateEnum.CA.name().equalsIgnoreCase(state)) {
                /** execute PO2-412 [OMS Change: Subscription Main Details] **/

                CustomFieldValue lastCfValue = getLastCfVersion(serviceInstance, ServiceInstanceOmsCFsEnum.MAIN_SUBSCRIPTION_DETAILS.name(), orderLineDTO.billEffectDate, false);
                if (orderDTO.errorCode != null) {
                    return;
                }

                /** Store Main Subscription Details CF **/
                updateSubscriptionMainDetailsCf(serviceInstance, orderLineDTO, endDate, lastCfValue);
                serviceInstanceService.update(serviceInstance);

                reRateWalletOperation(orderLineDTO, serviceInstance);
            } else {
                /** if the STATE of the incoming Subscription Main Details = CA **/
                /** execute PO2-486 [OMS Cancelled Orders] **/
            }

        } else {

            /** Validate data for provide subscription main details **/
            String calendar = (String) orderLineDTO.catalog.get(Catalog.CALENDAR.name());
            if (StringUtils.isBlank(calendar)) {
                setError(ErrorEnum.INVALID_DATA, "The calendar attribute is missing in catalog for the offer " + orderLineDTO.offerTemplate.getCode());
                return;
            }
            // PO2-528 : Validation on Provide order to ensure service_id not in use on DDI/MSN Ancillary Service
            // FIXME : isValidRanges loop all eir subscriptions ==> performance problem
            /*
             * if (!isValidRanges(orderLineDTO.deducedServiceId, null)) { setError(ErrorEnum.INVALID_DATA, "ServiceId " + orderLineDTO.deducedServiceId +
             * " already in use on DDI/MSN Ancillary Service"); return; }
             */

            ServiceTemplate serviceTemplate = getServiceTemplate(orderLineDTO);
            if (orderDTO.errorCode != null) {
                return;
            }

            /** 1 : execute PO2-128 [OMS Provide: Subscription Main Details] **/

            if (orderLineDTO.offerTemplate != null && !orderLineDTO.offerTemplate.containsServiceTemplate(serviceTemplate)) {
                log.info("The service offer is :" + orderLineDTO.offerTemplate.getCode());
                setError(ErrorEnum.INVALID_DATA, "The Service " + orderLineDTO.billCode + " is not associated with the offer " + orderLineDTO.offerTemplate.getCode());
                return;
            }

            orderLineDTO.subscription = createSubscription(orderLineDTO);

            // using the Bill Code, find the corresponding offer in the Opencell product catalog
            // and activate the respective primary service against the subscriber
            ServiceInstance serviceInstance = activateService(orderLineDTO, serviceTemplate, ServiceTypeEnum.MAIN.getLabel());

            // Store Main Subscription Details CF
            updateSubscriptionMainDetailsCf(serviceInstance, orderLineDTO, null, null);
            serviceInstanceService.update(serviceInstance);
            subscriptionService.update(orderLineDTO.subscription);
        }
    }

    /**
     * Get new bill item tariff
     *
     * @param orderLineDTO the order line DTO
     * @return the new bill item tariff
     */
    private Double getNewBillItemTariff(OrderLineDTO orderLineDTO) {
        String amt = ItemTypeEnum.SUBSCRIPTION_MAIN_DETAILS.getLabel().equalsIgnoreCase(orderLineDTO.itemType)
                ? orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.AMT.name())
                : orderLineDTO.getAttribute(ChargesAttributeNamesEnum.AMT.name());
        if (!StringUtils.isBlank(amt)) {
            if (!StringUtils.isBlank(amt)) {
                try {
                    return Double.valueOf(amt);
                } catch (NumberFormatException nfe) {
                }
            }
        }
        return null;
    }

    /**
     * Update tariff or quantity
     *
     * @param orderLineDTO the order line DTO
     * @param serviceInstance the service instance
     */
    private void setSystemCalculatedQuantity(OrderLineDTO orderLineDTO, ServiceInstance serviceInstance) {
        serviceInstance.getCfValuesNullSafe().setValue(ServiceInstanceOmsCFsEnum.SYSTEM_CALCULATED_QUANTITY.name(), null);
        /** Store system calculated quantity CF **/
        BigDecimal quantity = getSystemCalculatedQuantity(orderLineDTO, serviceInstance);
        if (quantity != null && quantity.compareTo(BigDecimal.ZERO) != 0) {
            serviceInstance.setQuantity(quantity);
            serviceInstance.getCfValuesNullSafe().setValue(ServiceInstanceOmsCFsEnum.SYSTEM_CALCULATED_QUANTITY.name(), quantity);
        }
    }

    /**
     * Update tariff or quantity
     * 
     * @param orderLineDTO the order line DTO
     * @param serviceInstance the service instance
     * @throws BusinessException the business exception
     */
    private void updateTariffOrQuantity(OrderLineDTO orderLineDTO, ServiceInstance serviceInstance) throws BusinessException {
        serviceInstance.getCfValuesNullSafe().setValue(ServiceInstanceOmsCFsEnum.NEW_TARIFF.name(), null);
        serviceInstance.getCfValuesNullSafe().setValue(ServiceInstanceOmsCFsEnum.SYSTEM_CALCULATED_QUANTITY.name(), null);

        Double newTariff = getNewBillItemTariff(orderLineDTO);
        if (newTariff != null) {
            serviceInstance.getCfValuesNullSafe().setValue(ServiceInstanceOmsCFsEnum.NEW_TARIFF.name(), newTariff);
        } else if (ItemTypeEnum.CHARGE.getLabel().equalsIgnoreCase(orderLineDTO.itemType)) {
            setSystemCalculatedQuantity(orderLineDTO, serviceInstance);
        }
    }

    /**
     * Rerate wallet operation
     *
     * @param orderLineDTO the order line DTO
     * @param serviceInstance the service instance
     * @throws BusinessException the business exception
     */
    private void reRateWalletOperation(OrderLineDTO orderLineDTO, ServiceInstance serviceInstance) throws BusinessException {
        if (BillActionEnum.CH.name().equalsIgnoreCase(orderLineDTO.billAction)) {
            updateTariffOrQuantity(orderLineDTO, serviceInstance);
            Double newTariff = (Double) serviceInstance.getCfValuesNullSafe().getValue(ServiceInstanceOmsCFsEnum.NEW_TARIFF.name());
            // BigDecimal oldQuantity = getOldQuantity(orderLineDTO, serviceInstance);
            Double systemCalculatedQuantity = (Double) serviceInstance.getCfValuesNullSafe().getValue(ServiceInstanceOmsCFsEnum.SYSTEM_CALCULATED_QUANTITY.name());
            if (newTariff != null || (systemCalculatedQuantity != null && systemCalculatedQuantity != 0)) {
                reRateWalletOperation(orderLineDTO, serviceInstance, orderLineDTO.billEffectDate);
            }
        }
    }

    private void executeRatingScript(Long walletOperationId, Map<String, Object> context) throws RatingScriptExecutionErrorException {

        WalletOperation walletOperation = walletOperationService.findById(walletOperationId);
        String scriptInstanceCode = "com.eir.script.OrderRatingScript";
        try {
            log.debug("Will execute rating script " + scriptInstanceCode);
            scriptInstanceService.executeCached(walletOperation, scriptInstanceCode, context);

        } catch (BusinessException e) {
            throw new RatingScriptExecutionErrorException("Failed when run script " + scriptInstanceCode + ", info " + e.getMessage(), e);
        }
    }

    /**
     * Pro-Rate recurring charge
     *
     * @param orderLineDTO the order line DTO
     * @param walletOperation the wallet operation
     * @param billEffectDate the effect date
     * @throws BusinessException the business exception
     */
    private Map<String, Object> proRateRecurringCharge(OrderLineDTO orderLineDTO, WalletOperation walletOperation, Date billEffectDate) throws BusinessException {
        Map<String, Object> context = new HashMap<>();

        ServiceInstance serviceInstance = walletOperation.getChargeInstance().getServiceInstance();
        Double newTariff = (Double) serviceInstance.getCfValuesNullSafe().getValue(ServiceInstanceOmsCFsEnum.NEW_TARIFF.name());
        Double systemCalculatedQuantity = (Double) serviceInstance.getCfValuesNullSafe().getValue(ServiceInstanceOmsCFsEnum.SYSTEM_CALCULATED_QUANTITY.name());
        BigDecimal newQuantity = getNewQuantity(orderLineDTO);

        BigDecimal tariff = walletOperation.getUnitAmountWithoutTax();
        BigDecimal quantity = walletOperation.getQuantity();

        Date StartDate = DateUtils.truncateTime(walletOperation.getStartDate());
        Date endDate = DateUtils.truncateTime(walletOperation.getEndDate());
        billEffectDate = DateUtils.truncateTime(billEffectDate);

        BigDecimal beforeEffectDatePart = BigDecimal.valueOf(DateUtils.daysBetween(StartDate, billEffectDate));
        BigDecimal afterEffectDatePart = BigDecimal.valueOf(DateUtils.daysBetween(billEffectDate, endDate));
        BigDecimal billingCycle = BigDecimal.valueOf(DateUtils.daysBetween(walletOperation.getStartDate(), endDate));

        if (billingCycle.compareTo(BigDecimal.ZERO) > 0) {
            if (newTariff != null && newTariff > 0) {
                tariff = tariff.multiply(beforeEffectDatePart);
                tariff = tariff.add(new BigDecimal(newTariff).multiply(afterEffectDatePart));
                tariff = tariff.divide(billingCycle, BaseEntity.NB_DECIMALS, RoundingMode.HALF_UP);
                context.put("NEW_TARIFF", tariff);
            } else if (systemCalculatedQuantity != null) {

                quantity = quantity.multiply(beforeEffectDatePart);
                if (systemCalculatedQuantity > 0) {
                    quantity = quantity.add(newQuantity.multiply(afterEffectDatePart));
                } else {
                    quantity = quantity.add(new BigDecimal(systemCalculatedQuantity).multiply(afterEffectDatePart));
                }
                quantity = quantity.divide(billingCycle, BaseEntity.NB_DECIMALS, RoundingMode.HALF_UP);
                // get tariff from the catalog
                String exchangeClass = getExchangeClass(orderLineDTO);
                tariff = getTariff(orderLineDTO, ChargeTypeEnum.R.name(), exchangeClass);
                context.put("NEW_TARIFF", tariff);
                context.put("NEW_QUANTITY", quantity);
            }
        }
        return context;
    }

    private Map<String, Object> getNewRate(OrderLineDTO orderLineDTO, ServiceInstance serviceInstance) {
        Map<String, Object> context = new HashMap<>();
        Double newTariff = (Double) serviceInstance.getCfValuesNullSafe().getValue(ServiceInstanceOmsCFsEnum.NEW_TARIFF.name());
        Double systemCalculatedQuantity = (Double) serviceInstance.getCfValuesNullSafe().getValue(ServiceInstanceOmsCFsEnum.SYSTEM_CALCULATED_QUANTITY.name());
        BigDecimal newQuantity = getNewQuantity(orderLineDTO);

        if (newTariff != null && newTariff > 0) {
            context.put("NEW_TARIFF", new BigDecimal(newTariff));
        } else if (systemCalculatedQuantity != null && systemCalculatedQuantity != 0) {
            context.put("NEW_QUANTITY", newQuantity);
        }
        return context;
    }

    /**
     * Rerate wallet operation
     *
     * @param orderLineDTO the order line DTO
     * @param serviceInstance the service instance
     * @param billEffectDate the bill effect date
     * @throws BusinessException the business exception
     */
    private void reRateWalletOperation(OrderLineDTO orderLineDTO, ServiceInstance serviceInstance, Date billEffectDate) throws BusinessException {
        if (serviceInstance != null) {
            List<ChargeInstance> chargeInstances = serviceInstance.getChargeInstances();
            /*
             * Query query = chargeInstanceService.getEntityManager().createQuery("SELECT ci from ChargeInstance ci  where ci.serviceInstance.id=:serviceInstanceId");
             * query.setParameter("serviceInstanceId", serviceInstance.getId()); List<ChargeInstance> chargeInstances = (List<ChargeInstance>) query.getResultList();
             */
            if (chargeInstances != null && !chargeInstances.isEmpty()) {
                for (ChargeInstance chargeInstance : chargeInstances) {
                    // List<WalletOperation> walletOperations = chargeInstance.getWalletOperations();
                    Query query = null;
                    List<WalletOperation> walletOperations = null;

                    /** to calculate a Rebate amount for charges applied 'In-Advance' (on previous invoices) **/
                    if (chargeInstance instanceof RecurringChargeInstance) {
                        boolean isApplyInAdvance = ((RecurringChargeInstance) chargeInstance).getRecurringChargeTemplate().getApplyInAdvance() == null ? false
                                : ((RecurringChargeInstance) chargeInstance).getRecurringChargeTemplate().getApplyInAdvance();
                        if (StringUtils.isBlank(((RecurringChargeInstance) chargeInstance).getRecurringChargeTemplate().getApplyInAdvanceEl())) {
                            isApplyInAdvance = recurringChargeTemplateService.matchExpression(
                                ((RecurringChargeInstance) chargeInstance).getRecurringChargeTemplate().getApplyInAdvanceEl(), chargeInstance.getServiceInstance(),
                                ((RecurringChargeInstance) chargeInstance).getRecurringChargeTemplate());
                        }

                        if (isApplyInAdvance) {
                            query = walletOperationService.getEntityManager().createQuery(
                                "select wo from WalletOperation wo join wo.ratedTransaction rt where rt.status=org.meveo.model.billing.RatedTransactionStatusEnum.BILLED "
                                        + "and wo.chargeInstance.id=:chargeInstanceId and wo.endDate>:endDate");
                            query.setParameter("chargeInstanceId", chargeInstance.getId());
                            query.setParameter("endDate", billEffectDate);
                            walletOperations = (List<WalletOperation>) query.getResultList();
                            if (walletOperations != null && !walletOperations.isEmpty()) {
                                /**
                                 * get the diffrence between the total already paid and the total that should be paid and update the first waellet operation who is not yet billed
                                 **/
                            }
                        }
                    }

                    query = walletOperationService.getEntityManager()
                        .createQuery("select wo from WalletOperation wo join wo.ratedTransaction rt where rt.status!=org.meveo.model.billing.RatedTransactionStatusEnum.BILLED "
                                + "and wo.chargeInstance.id=:chargeInstanceId and wo.endDate>:endDate");
                    query.setParameter("chargeInstanceId", chargeInstance.getId());
                    query.setParameter("endDate", billEffectDate);
                    walletOperations = (List<WalletOperation>) query.getResultList();

                    Map<Long, Map<String, Object>> walletOperationMap = new HashMap<>();
                    if (walletOperations != null && !walletOperations.isEmpty()) {
                        for (WalletOperation walletOperation : walletOperations) {
                            Map<String, Object> context = new HashMap<>();
                            if (chargeInstance instanceof RecurringChargeInstance) {
                                if ((DateUtils.truncateTime(billEffectDate)).after(walletOperation.getStartDate()) && billEffectDate.before(walletOperation.getEndDate())) {
                                    context = proRateRecurringCharge(orderLineDTO, walletOperation, billEffectDate);
                                } else {
                                    context = getNewRate(orderLineDTO, serviceInstance);
                                }
                            } else {
                                context = getNewRate(orderLineDTO, serviceInstance);
                            }
                            walletOperationMap.put(walletOperation.getId(), context);
                        }
                        for (Map.Entry<Long, Map<String, Object>> entry : walletOperationMap.entrySet()) {
                            executeRatingScript(entry.getKey(), entry.getValue());
                        }
                    }
                }
            }
        }
    }

    /**
     * Get OMS Min Term for a customer & migration code.
     *
     * @param customerCode the customer code
     * @param migrationCode the migration code
     * @param effectDate Effect Date
     * @return Min Term configuartion
     */
    private Map<String, Object> getMinTerm(String customerCode, String migrationCode, Date effectDate) {
        Map<String, Object> minTerm = getOMSMinTerm(customerCode, migrationCode, effectDate);
        if (minTerm != null && !minTerm.isEmpty()) {
            return minTerm;
        }
        if (!StringUtils.isBlank(migrationCode)) {
            String[] keys = migrationCode.split(Pattern.quote("_"));
            if (keys.length > 1) {
                minTerm = getOMSMinTerm(customerCode, keys[0] + "_" + keys[1] + "_*", effectDate);
                if (minTerm != null && !minTerm.isEmpty()) {
                    return minTerm;
                }
                minTerm = getOMSMinTerm(customerCode, keys[0] + "_*_" + keys[2], effectDate);
                if (minTerm != null && !minTerm.isEmpty()) {
                    return minTerm;
                }
                minTerm = getOMSMinTerm(customerCode, "*_" + keys[1] + "_" + keys[2], effectDate);
                if (minTerm != null && !minTerm.isEmpty()) {
                    return minTerm;
                }
                minTerm = getOMSMinTerm(customerCode, keys[0] + "_*_*", effectDate);
                if (minTerm != null && !minTerm.isEmpty()) {
                    return minTerm;
                }
                minTerm = getOMSMinTerm(customerCode, "*_" + keys[1] + "_*", effectDate);
                if (minTerm != null && !minTerm.isEmpty()) {
                    return minTerm;
                }
                minTerm = getOMSMinTerm(customerCode, "*_*_" + keys[2], effectDate);
                if (minTerm != null && !minTerm.isEmpty()) {
                    return minTerm;
                }
            }
            if (keys.length > 0) {
                if (keys.length == 1) {
                    minTerm = getOMSMinTerm(customerCode, keys[0] + "_*_*", effectDate);
                    if (minTerm != null && !minTerm.isEmpty()) {
                        return minTerm;
                    }
                }
                minTerm = getOMSMinTerm(customerCode, keys[0] + "_*", effectDate);
                if (minTerm != null && !minTerm.isEmpty()) {
                    return minTerm;
                }
            }

            minTerm = getOMSMinTerm(customerCode, "*_*_*", effectDate);
            if (minTerm != null && !minTerm.isEmpty()) {
                return minTerm;
            }
        }
        minTerm = getOMSMinTerm(customerCode, "*", effectDate);
        if (minTerm != null && !minTerm.isEmpty()) {
            return minTerm;
        }
        minTerm = getOMSMinTerm("*", migrationCode, effectDate);
        if (minTerm != null && !minTerm.isEmpty()) {
            return minTerm;
        }
        return minTerm;
    }

    /**
     * Get OMS Min Term for a customer & migration code.
     *
     * @param customerCode the customer code
     * @param migrationCode the migration code
     * @param effectDate Effect Date
     * @return Min Term configuartion
     */
    private Map<String, Object> getOMSMinTerm(String customerCode, String migrationCode, Date effectDate) {
        Map<String, String> cf = (Map<String, String>) providerService.getProvider().getCfValuesNullSafe().getValue("OMS_MIN_TERM");
        if (cf == null) {
            return null;
        }
        for (Map.Entry<String, String> entry : cf.entrySet()) {

            String[] keys = entry.getKey().split(Pattern.quote(CustomFieldValue.MATRIX_KEY_SEPARATOR));
            if (keys.length != 4) {
                continue;
            }
            Date startDate = !StringUtils.isBlank(keys[MinTermAttributeNamesEnum.START_DATE.ordinal()])
                    ? DateUtils.parseDateWithPattern(keys[MinTermAttributeNamesEnum.START_DATE.ordinal()], DATE_DEFAULT_PATTERN)
                    : null;
            Date endDate = !StringUtils.isBlank(keys[MinTermAttributeNamesEnum.END_DATE.ordinal()])
                    ? DateUtils.parseDateWithPattern(keys[MinTermAttributeNamesEnum.END_DATE.ordinal()], DATE_DEFAULT_PATTERN)
                    : null;
            String oaoId = keys[MinTermAttributeNamesEnum.OAO.ordinal()];
            String productCode = keys[MinTermAttributeNamesEnum.PRODUCT_CODE.ordinal()];

            if (isSameAttributeValue(customerCode, oaoId) && isSameAttributeValue(migrationCode, productCode)) {
                if (startDate == null) {
                    continue;
                } else if (endDate == null) {
                    if (startDate.before(effectDate)) {
                        return (Map<String, Object>) getCFValue(providerService.getProvider(), "OMS_MIN_TERM", entry.getValue());
                    }
                } else {
                    if (startDate.before(effectDate) && endDate.after(effectDate)) {
                        return (Map<String, Object>) getCFValue(providerService.getProvider(), "OMS_MIN_TERM", entry.getValue());
                    }
                }

            }

        }
        return null;
    }

    /**
     * Get engagement duration for a customer & offer code.
     *
     * @param orderLineDTO the order line DTO
     * @return Min Term duration
     */
    private Integer getEngagementDuration(OrderLineDTO orderLineDTO) {
        String offerCode = orderLineDTO.offerTemplate != null ? orderLineDTO.offerTemplate.getCode() : null;
        Integer minTermDuration = getEngagementDuration(orderLineDTO.operatorId, offerCode, orderLineDTO.billEffectDate);
        if (minTermDuration != null) {
            return minTermDuration;
        }
        minTermDuration = getEngagementDuration(orderLineDTO.operatorId, "*", orderLineDTO.billEffectDate);
        if (minTermDuration != null) {
            return minTermDuration;
        }
        minTermDuration = getEngagementDuration("*", offerCode, orderLineDTO.billEffectDate);
        if (minTermDuration != null) {
            return minTermDuration;
        }
        return minTermDuration;
    }

    /**
     * Get engagement duration for a customer & offer code.
     *
     * @param customerCode the customer code
     * @param offerCode the offer code
     * @param effectDate Effect Date
     * @return Min Term duration
     */
    private Integer getEngagementDuration(String customerCode, String offerCode, Date effectDate) {
        Map<String, String> cf = (Map<String, String>) providerService.getProvider().getCfValuesNullSafe().getValue("ENGAGEMENT_DURATION");
        if (cf == null) {
            return null;
        }
        for (Map.Entry<String, String> entry : cf.entrySet()) {

            String[] keys = entry.getKey().split(Pattern.quote(CustomFieldValue.MATRIX_KEY_SEPARATOR));
            if (keys.length != 4) {
                continue;
            }
            String customer = keys[EngagementDurationAttributeNamesEnum.Customer.ordinal()];
            String offer = keys[EngagementDurationAttributeNamesEnum.Offer.ordinal()];
            Date startDate = !StringUtils.isBlank(keys[EngagementDurationAttributeNamesEnum.startDate.ordinal()])
                    ? DateUtils.parseDateWithPattern(keys[EngagementDurationAttributeNamesEnum.startDate.ordinal()], DATE_DEFAULT_PATTERN)
                    : null;
            Date endDate = !StringUtils.isBlank(keys[EngagementDurationAttributeNamesEnum.endDate.ordinal()])
                    ? DateUtils.parseDateWithPattern(keys[EngagementDurationAttributeNamesEnum.endDate.ordinal()], DATE_DEFAULT_PATTERN)
                    : null;

            if (isSameAttributeValue(customerCode, customer) && isSameAttributeValue(offerCode, offer)) {
                if (startDate == null) {
                    continue;
                } else if (endDate == null) {
                    if (startDate.before(effectDate)) {
                        Map<String, Object> value = (Map<String, Object>) getCFValue(providerService.getProvider(), "ENGAGEMENT_DURATION", entry.getValue());
                        return (Integer) value.get(EngagementDurationAttributeNamesEnum.minTerm.name());
                    }
                } else {
                    if (startDate.before(effectDate) && endDate.after(effectDate)) {
                        Map<String, Object> value = (Map<String, Object>) getCFValue(providerService.getProvider(), "ENGAGEMENT_DURATION", entry.getValue());
                        return (Integer) value.get(EngagementDurationAttributeNamesEnum.minTerm.name());
                    }
                }
            }
        }
        return null;
    }

    /**
     * Get OMS Min Term for a customer & migration code.
     *
     * @param customerCode the customer code
     * @param migrationCode the migration code
     * @param effectDate Effect Date
     * @return Min Term configuartion
     */
    private Map<String, Object> getMinTermVersionable(String customerCode, String migrationCode, Date effectDate) {

        String keyToMatch = customerCode + CustomFieldValue.MATRIX_KEY_SEPARATOR + migrationCode;
        Map<String, Object> cf = (Map<String, Object>) customFieldInstanceService.getCFValueByClosestMatch(providerService.getProvider(), "OMS_MIN_TERM", effectDate, keyToMatch);
        if (cf == null || cf.isEmpty()) {
            keyToMatch = CustomFieldValue.WILDCARD_MATCH_ALL + CustomFieldValue.MATRIX_KEY_SEPARATOR + migrationCode;
            cf = (Map<String, Object>) customFieldInstanceService.getCFValueByClosestMatch(providerService.getProvider(), "OMS_MIN_TERM", effectDate, keyToMatch);
        }
        if (cf == null || cf.isEmpty()) {
            keyToMatch = customerCode + CustomFieldValue.MATRIX_KEY_SEPARATOR + CustomFieldValue.WILDCARD_MATCH_ALL;
            cf = (Map<String, Object>) customFieldInstanceService.getCFValueByClosestMatch(providerService.getProvider(), "OMS_MIN_TERM", effectDate, keyToMatch);
        }

        if (cf == null || cf.isEmpty()) {
            return null;
        }
        return cf;
    }

    private Integer getMinimumTermPeriod(OrderLineDTO orderLineDTO) {
        // Using the bill code on the order, query the product catalogue and retrieve the Migration Code for the bill code
        String migrationCode = (String) orderLineDTO.catalog.get(Catalog.MIGRATION_CODE.name());
        Integer minimumTermPeriod = null;
        if (!StringUtils.isBlank(migrationCode)) {
            // This Migration Code (along with the OLO Id) is then used to find matching entries in the OMS Min Term configuration
            Map<String, Object> minTerm = getMinTerm(orderLineDTO.operatorId, migrationCode, orderLineDTO.billEffectDate);
            if (minTerm != null) {
                minimumTermPeriod = minTerm.get(MinTermAttributeNamesEnum.MINIMUM_TERM_PERIOD.name()) != null
                        ? ((Long) minTerm.get(MinTermAttributeNamesEnum.MINIMUM_TERM_PERIOD.name())).intValue()
                        : null;
            }
            // If a match is found (in the OMS Min Term configuration), add the 'Min Term Period' (months) from the record found to
            // the Order Effective Date and populate this as the End of Engagement Date against the subscriber
        }
        return minimumTermPeriod;
    }

    private Integer getNoticeCeasePeriod(OrderLineDTO orderLineDTO) {
        // Using the bill code on the order, query the product catalogue and retrieve the Migration Code for the bill code
        String migrationCode = (String) orderLineDTO.catalog.get(Catalog.MIGRATION_CODE.name());
        Integer noticeCeasePeriod = null;
        if (!StringUtils.isBlank(migrationCode)) {
            // This Migration Code (along with the OLO Id) is then used to find matching entries in the OMS Min Term configuration
            Map<String, Object> minTerm = getMinTerm(orderLineDTO.operatorId, migrationCode, orderLineDTO.billEffectDate);
            if (minTerm != null) {
                noticeCeasePeriod = minTerm.get(MinTermAttributeNamesEnum.NOTICE_CEASE_PERIOD.name()) != null
                        ? ((Long) minTerm.get(MinTermAttributeNamesEnum.NOTICE_CEASE_PERIOD.name())).intValue()
                        : null;
            }
            // If a match is found (in the OMS Min Term configuration), add the 'Min Term Period' (months) from the record found to
            // the Order Effective Date and populate this as the End of Engagement Date against the subscriber
        }
        return noticeCeasePeriod;
    }

    /**
     * Get the end of engagement date
     *
     * @param orderLineDTO the order line DTO
     * @return the end of engagement date
     */
    private Date getEndEngagementDate(OrderLineDTO orderLineDTO) {
        /** execute PO2-491 [OMS Min Term: Determine End of Engagement Date] **/

        /** Step 7 : populate the Order Effect Date as the End of Engagement Date against the subscriber **/
        Date endEngagementDate = orderLineDTO.billEffectDate;

        String commitmentPeriod = orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.COMMITMENT_PERIOD.name());
        String commitmentPeriodUnit = orderLineDTO.getUnitAttribute(SubMainDetailsAttributeNamesEnum.COMMITMENT_PERIOD.name());
        String cmmitmntTreatment = orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.CMMITMNTTREATMENT.name());

        /** Step 2 : Check if a value exists in the COMMITMENT TERM tag in the order submitted from OMS **/
        if (!StringUtils.isBlank(commitmentPeriod)) {

            /**
             * Step 3 : add the COMMITMENT TERM tag value (i.e. number of days) to the Order Effect Date and populate this in the End of Engagement Date against the subscriber
             **/
            if (isInteger(commitmentPeriod)) {
                if (PeriodUnitEnum.Days.name().equalsIgnoreCase(commitmentPeriodUnit)) {
                    endEngagementDate = DateUtils.addDaysToDate(endEngagementDate, Integer.valueOf(commitmentPeriod));
                } else if (PeriodUnitEnum.Months.name().equalsIgnoreCase(commitmentPeriodUnit)) {
                    endEngagementDate = DateUtils.addMonthsToDate(endEngagementDate, Integer.valueOf(commitmentPeriod));
                } else if (PeriodUnitEnum.Years.name().equalsIgnoreCase(commitmentPeriodUnit)) {
                    endEngagementDate = DateUtils.addYearsToDate(endEngagementDate, Integer.valueOf(commitmentPeriod));
                }
            }
        } else {
            /** Step 4 : check if a complementary order exists (i.e. REF_ORDER_ID) **/
            if (StringUtils.isBlank(orderDTO.refOrderId)) {

                /** Step 5 : look up the OMS Min Term table **/
                Integer minTermPeriod = getMinimumTermPeriod(orderLineDTO);

                if (minTermPeriod != null) {
                    /**
                     * Step 6 : add the 'Min Term Period' (months) from the record found to the Order Effective Date and populate this as the End of Engagement Date against the
                     * subscriber
                     **/
                    endEngagementDate = DateUtils.addMonthsToDate(endEngagementDate, minTermPeriod);
                }
            } else {
                /** Step 8 : If a complementary order exists (i.e. REF_ORDER_ID), check if a value exists in the COMMITMENT TREATMENT tag. **/
                if (StringUtils.isBlank(cmmitmntTreatment)) {

                    /** Step 5 : look up the OMS Min Term table **/
                    Integer minTermPeriod = getMinimumTermPeriod(orderLineDTO);

                    if (minTermPeriod != null) {
                        /**
                         * Step 6 : add the 'Min Term Period' (months) from the record found to the Order Effective Date and populate this as the End of Engagement Date against the
                         * subscriber
                         **/
                        endEngagementDate = DateUtils.addMonthsToDate(endEngagementDate, minTermPeriod);
                    }
                } else {
                    /** Step 9 : If the value in COMMITMENT TREATMENT is 'Continue' **/
                    if (!"Continue".equalsIgnoreCase(cmmitmntTreatment)) {

                        /** Step 5 : look up the OMS Min Term table **/
                        Integer minTermPeriod = getMinimumTermPeriod(orderLineDTO);

                        if (minTermPeriod != null) {
                            /**
                             * Step 6 : add the 'Min Term Period' (months) from the record found to the Order Effective Date and populate this as the End of Engagement Date against
                             * the subscriber
                             **/
                            endEngagementDate = DateUtils.addMonthsToDate(endEngagementDate, minTermPeriod);
                        }
                    } else {
                        /**
                         * Step 10 : Using the REF_ORDER_ID, locate the complementary order, retrieve the Service Id and use that to retrieve the ‘End of Engagement Date’
                         * associated with the Subscriber (found using that service id)
                         **/
                        if (refOrderDTO != null && refOrderDTO.refOrder != null) {

                            String associatedServiceId = (String) refOrderDTO.refOrder.getCfValuesNullSafe().getValue("ORDER_SERVICE_ID");
                            Subscription subscription = subscriptionService.findByCode(associatedServiceId);
                            if (subscription != null) {

                                /** Step 11 : Check if the 'End of Engagement Date' retrieved represents a date in the past. **/
                                if (subscription.getEndAgreementDate() != null && subscription.getEndAgreementDate().compareTo(new Date()) > 0) {

                                    /**
                                     * Step 12 : Populate the End of Engagement Date (retrieved from the subscriber circuit / via the REF_ORDER_ID on the complimentary order) as
                                     * the End of Engagement Date against the subscriber
                                     **/
                                    endEngagementDate = subscription.getEndAgreementDate();
                                }
                            }
                        }
                    }
                }
            }
        }
        return endEngagementDate;
    }

    /**
     * Get the notice to cease (NC) date
     *
     * @param orderLineDTO the order line DTO
     * @return the notice to cease (NC) date
     */
    private Date getNoticeToCeaseDate(OrderLineDTO orderLineDTO) {

        /** Step 9 : use the Order Effect Date as the Notice to Cease Date **/
        Date noticeToCeaseDate = orderLineDTO.billEffectDate;

        /** Step 1 : look up the OMS Min Term table **/
        Integer noticeCeasePeriod = getNoticeCeasePeriod(orderLineDTO);
        /** Step 2 : If a match is found (in the OMS Min Term configuration), retrieve the 'Notice to Cease Period' **/
        if (noticeCeasePeriod != null) {
            /** Step 3 : Check if a value exists in the NOTBEFOREDATE tag in the order submitted from OMS **/
            String notBeforeDateAttribute = orderLineDTO.getAttribute(ChargesAttributeNamesEnum.NOTBEFOREDATE.name());
            if (StringUtils.isBlank(notBeforeDateAttribute)) {
                /**
                 * Step 4 : If a value does NOT exist in NOTBEFOREDATE (or if the tag is not present on the order), add the 'Notice to Cease Period' (days) retrieved to the Order
                 * Effect Date and use the date derived as the Notice to Cease Date
                 **/
                noticeToCeaseDate = DateUtils.addDaysToDate(noticeToCeaseDate, noticeCeasePeriod);
            } else {
                /** Step 5 : If a value does exist in the NOTBEFOREDATE tag, subtract Effect Date from NOTBEFOREDATE **/
                Date notBeforeDate = DateUtils.parseDateWithPattern(notBeforeDateAttribute, PlaceOMSOrderScript.DATE_TIME_PATTERN);
                if (notBeforeDate != null) {
                    double days = DateUtils.daysBetween(noticeToCeaseDate, notBeforeDate);
                    /** Step 6 : Check if the result (i.e. the difference) is greater or equal to the 'Notice to Cease Period' **/
                    if (days >= noticeCeasePeriod) {
                        noticeToCeaseDate = notBeforeDate;
                    }
                }
            }

        } else {
            /**
             * Step 8 : If a match is NOT found (in the OMS Min Term configuration), check if a value exists in the NOTBEFOREDATE tag in the order submitted from OMS
             **/
            String notBeforeDateAttribute = orderLineDTO.getAttribute(ChargesAttributeNamesEnum.NOTBEFOREDATE.name());
            if (!StringUtils.isBlank(notBeforeDateAttribute)) {
                /** Step 7 : If a value does exist in the NOTBEFOREDATE tag, use this Not Before Date as the Notice to Cease Date **/
                Date notBeforeDate = DateUtils.parseDateWithPattern(notBeforeDateAttribute, DATE_TIME_PATTERN);
                if (notBeforeDate != null) {
                    noticeToCeaseDate = notBeforeDate;
                }
            }
        }
        return noticeToCeaseDate;
    }

    /**
     * Get the rental liability date
     *
     * @param orderLineDTO the order line DTO
     * @param noticeToCeaseDate the notice to cease (NC) date
     * @return the rental liability date
     */
    private Date getRentalLiabilityCeaseDate(OrderLineDTO orderLineDTO, Date noticeToCeaseDate) {
        /** Step 6 : Use the Notice to Cease Date (retrieved in step 3) as the Rental Liability Date **/
        Date rentalLiabilityDate = noticeToCeaseDate;
        if (orderLineDTO.subscription != null && orderLineDTO.subscription.getEndAgreementDate() != null) {
            /** Step 7 : Check if the subscribers 'End of Engagement Date' is greater than (i.e. occurs before) the 'Notice to Cease Date' (retrieved in step 3) **/
            if (noticeToCeaseDate == null || orderLineDTO.subscription.getEndAgreementDate().compareTo(noticeToCeaseDate) > 0) {
                /** Step 8 : Use the End of Engagement Date as the Rental Liability Date **/
                rentalLiabilityDate = orderLineDTO.subscription.getEndAgreementDate();
            }
        }
        return rentalLiabilityDate;
    }

    /**
     * Get ref order DTO
     * 
     * @param refOrder the ref order
     * @return the ref order DTO
     */
    private RefOrderDTO getRefOrderDTO(Order refOrder) {
        RefOrderDTO refOrderDTO = null;
        if (refOrder != null) {
            refOrderDTO = new RefOrderDTO();
            refOrderDTO.refOrder = refOrder;
            refOrderDTO.mainItem = getMainItemRefOrder(refOrder);
        }
        return refOrderDTO;
    }

    /**
     * Get subscription main details ref order
     *
     * @return subscription main details ref order
     */
    private Map<String, String> getMainItemRefOrder(Order refOrder) {
        if (refOrder != null) {
            String orderMessage = (String) refOrder.getCfValuesNullSafe().getValue("ORDER_MESSAGE");
            if (!StringUtils.isBlank(orderMessage)) {
                Map<String, String> orderItemsCF = (Map<String, String>) refOrder.getCfValuesNullSafe().getValue("ORDER_ITEMS");
                for (Map.Entry<String, String> entry : orderItemsCF.entrySet()) {
                    Map<String, String> value = (Map<String, String>) getCFValue(refOrder, "ORDER_ITEMS", entry.getValue());
                    String itemType = value.get(OrderItemsNamesEnum.Item_Type.name());
                    if (!StringUtils.isBlank(itemType) && ItemTypeEnum.SUBSCRIPTION_MAIN_DETAILS.getLabel().equalsIgnoreCase(itemType)) {
                        value.put(OrderItemsNamesEnum.Item_Id.name(), entry.getKey());
                        return value;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Get subscription main details attribute ref order
     *
     * @param attributeName the attribute name
     * @return subscription main details attribute ref order
     */
    public String getMainItemAttribute(String attributeName) {
        if (refOrderDTO != null && refOrderDTO.refOrder != null && refOrderDTO.mainItem != null && !refOrderDTO.mainItem.isEmpty()) {
            String itemId = refOrderDTO.mainItem.get(OrderItemsNamesEnum.Item_Id.name());
            if (!StringUtils.isBlank(itemId)) {
                String keyToMatch = itemId + CustomFieldValue.MATRIX_KEY_SEPARATOR + attributeName;
                Map<String, String> attribute = (Map<String, String>) customFieldInstanceService.getCFValueByClosestMatch(refOrderDTO.refOrder, "ORDER_ITEM_ATTRIBUTES",
                    keyToMatch);
                if (attribute != null && !attribute.isEmpty()) {
                    return attribute.get("Value");
                }
            }
        }
        return null;
    }

    /**
     * Get the rental liability date
     *
     * @param orderLineDTO the order line DTO
     * @return the rental liability date
     */
    private Date getRentalLiabilityDate(OrderLineDTO orderLineDTO) {

        Date rentalLiabilityDate = orderLineDTO.billEffectDate;
        Date noticeToCeaseDate = null;
        String cmmitmntTreatment = orderLineDTO.getAttribute(SubMainDetailsAttributeNamesEnum.CMMITMNTTREATMENT.name());

        /** Step 1 : Check if a complementary order exists (i.e. REF_ORDER_ID) **/
        if (refOrderDTO != null && refOrderDTO.refOrder != null) {

            /** Step 2 : retrieve the Order Effect Date from the Complementary order and use this as the Rental Liability Date **/
            rentalLiabilityDate = refOrderDTO.getMainItemElement(OrderItemsNamesEnum.Effect_Date.name());
        } else {

            /** Step 3 : Determine Notice to Cease (NC) Date **/

            /** execute PO2-670 [OMS Determine Notice to Cease (NC) Date] **/
            noticeToCeaseDate = getNoticeToCeaseDate(orderLineDTO);

            /** Step 4 : Check if the COMMITMENT TREATMENT tag exists and if it contains a value **/
            if (!StringUtils.isBlank(cmmitmntTreatment)) {

                /** Step 5 : If the value in the COMMITMENT TREATMENT tag is 'Forgive' **/
                if ("Forgive".equalsIgnoreCase(cmmitmntTreatment)) {

                    /** Step 6 : Use the Notice to Cease Date (retrieved in step 3) as the Rental Liability Date **/
                    rentalLiabilityDate = noticeToCeaseDate;
                } else {

                    /** Step 7 : Check if the subscribers 'End of Engagement Date' is greater than (i.e. occurs before) the 'Notice to Cease Date' (retrieved in step 3) **/
                    /** Step 6 : Use the Notice to Cease Date (retrieved in step 3) as the Rental Liability Date **/
                    /** Step 8 : Use the End of Engagement Date as the Rental Liability Date **/
                    rentalLiabilityDate = getRentalLiabilityCeaseDate(orderLineDTO, noticeToCeaseDate);
                }
            } else {

                /** Step 7 : Check if the subscribers 'End of Engagement Date' is greater than (i.e. occurs before) the 'Notice to Cease Date' (retrieved in step 3) **/
                /** Step 6 : Use the Notice to Cease Date (retrieved in step 3) as the Rental Liability Date **/
                /** Step 8 : Use the End of Engagement Date as the Rental Liability Date **/
                rentalLiabilityDate = getRentalLiabilityCeaseDate(orderLineDTO, noticeToCeaseDate);
            }

        }
        return rentalLiabilityDate;
    }

    /**
     * Update subscription main details CF
     *
     * @param serviceInstance the service instance
     * @param orderLineDTO the order line DTO
     * @param endDate the end date
     * @param lastCfValue the last CF value
     */
    private void updateSubscriptionMainDetailsCf(ServiceInstance serviceInstance, OrderLineDTO orderLineDTO, Date endDate, CustomFieldValue lastCfValue) {
        Map<String, String> cfValue = getSubscriptionMainDetailsCfValue(orderLineDTO);
        updateCfValue(serviceInstance, orderLineDTO.billEffectDate, ServiceInstanceOmsCFsEnum.MAIN_SUBSCRIPTION_DETAILS.name(), endDate, cfValue, lastCfValue);
    }

    /**
     * Provide Subscription Main Details
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void provideSubscriptionMainDetails(OrderLineDTO orderLineDTO) throws BusinessException {
        log.info("############### Start process the subscription main details bill item .....");
        subscriptionMainDetails(orderLineDTO);
        log.info("############### End process the subscription main details bill item .....");
    }

    /**
     * Change Subscription Main Details
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void changeSubscriptionMainDetails(OrderLineDTO orderLineDTO) throws BusinessException {
        log.info("############### Start process the change subscription main details bill items .....");
        subscriptionMainDetails(orderLineDTO);
        log.info("############### End process the change subscription main details bill items .....");
    }

    /**
     * Validate end point
     *
     * @param orderLineDTO the order line DTO
     */
    private void validateEndPoint(OrderLineDTO orderLineDTO) {

        // Which end of this circuit is the endpoint on: A = A end, B = B end, C = Circuit as a whole
        String epend = orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPEND.name());
        if (StringUtils.isBlank(epend)) {
            setError(ErrorEnum.INVALID_DATA, "Missing " + EndPointAttributeNamesEnum.EPEND.name() + " attribute");
            return;
        }

        if (!EndEnum.A.name().equalsIgnoreCase(epend) && !EndEnum.B.name().equalsIgnoreCase(epend) && !EndEnum.C.name().equalsIgnoreCase(epend)) {
            setError(ErrorEnum.INVALID_DATA, "unrecognized value " + epend + " for " + EndPointAttributeNamesEnum.EPEND.name() + " attribute");
            return;
        }

        // CU = Irish Customer Premises, OC = Foreign Customer Premises LE = Local Exchange, SE = Serving Exchange, OC = Intl/Partner point of presence
        String eptype = orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPTYPE.name());
        if (StringUtils.isBlank(eptype)) {
            setError(ErrorEnum.INVALID_DATA, "Missing " + EndPointAttributeNamesEnum.EPTYPE.name() + " attribute");
            return;
        }

    }

    /**
     * Get end point address
     *
     * @param orderLineDTO the order line DTO
     * @return the end point address
     */

    private String getEndPointAddress(OrderLineDTO orderLineDTO) {
        String address = null;
        String errorMessage = null;

        String eptype = orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPTYPE.name());
        if (EndPointTypeEnum.OC.name().equalsIgnoreCase(eptype)) {
            // retrieve address details from OC
            address = "";
        } else {
            // The Address ID of the endpoint. this will contain the ARD Id (e.g. 904694) or EIRCODE (e.g. D12W838)
            String epaddress = orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPADDRESS.name());
            if (StringUtils.isBlank(epaddress)) {
                errorMessage = "Missing " + EndPointAttributeNamesEnum.EPADDRESS.name() + " attribute";
                setError(ErrorEnum.INVALID_DATA, errorMessage);
                return null;
            }
            // retrieve address details from eir broker
            AddressInfoResponseDTO addressInfoResponseDTO = getEirAddressDetails(epaddress);
            if (orderDTO.errorCode != null) {
                return null;
            }
            address = ValidateAndFormatAddress(addressInfoResponseDTO);
            if (orderDTO.errorCode != null) {
                return null;
            }
        }
        return address;
    }

    /**
     * Provide End Points
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void provideEndPoints(OrderLineDTO orderLineDTO) throws BusinessException {
        log.info("############### Start process the end point bill items .....");

        // If EPEND = C the order line do not to be processed by Opencell
        String epend = orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPEND.name());
        if (EndEnum.C.name().equalsIgnoreCase(epend)) {
            return;
        }

        validateEndPoint(orderLineDTO);
        if (orderDTO.errorCode != null) {
            return;
        }

        String address = getEndPointAddress(orderLineDTO);
        if (orderDTO.errorCode != null) {
            return;
        }

        CustomFieldValue lastCfValue = getLastCfVersion(orderLineDTO.subscription, "END_POINT_" + epend.toUpperCase(), orderLineDTO.billEffectDate, false);
        if (orderDTO.errorCode != null) {
            return;
        }

        updateEndPointCf(orderLineDTO, null, epend, address, lastCfValue);
        subscriptionService.update(orderLineDTO.subscription);

        log.info("############### End process the end point bill items .....");
    }

    /**
     * Change End Points
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void changeEndPoints(OrderLineDTO orderLineDTO) throws BusinessException {
        log.info("############### Start process the change end point bill items .....");

        // If EPEND = C the order line do not to be processed by Opencell
        String epend = orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPEND.name());
        if (EndEnum.C.name().equalsIgnoreCase(epend)) {
            return;
        }

        validateEndPoint(orderLineDTO);
        if (orderDTO.errorCode != null) {
            return;
        }

        String address = getEndPointAddress(orderLineDTO);
        if (orderDTO.errorCode != null) {
            return;
        }

        // Ensure the item id value stored against it (in the End Point attribute group) is the same as the item id on the bill item
        // and if not create/log a data discrepancy error (Ref: PO2-459)
        StringBuilder keyCfValue = new StringBuilder();
        keyCfValue.append(orderLineDTO.itemId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.itemComponentId)) {
            keyCfValue.append(orderLineDTO.itemComponentId);
        }
        CustomFieldValue customFieldValue = findCfValueByKey(orderLineDTO.subscription, "END_POINT_" + epend.toUpperCase(), keyCfValue.toString());
        if (customFieldValue != null) {
            Map<String, String> cfValue = getEndPointCfValue(orderLineDTO, address);
            customFieldValue.setValue(cfValue);
            // subscriptionService.getEntityManager().refresh(subscriptionService.update(orderLineDTO.subscription));
            subscriptionService.update(orderLineDTO.subscription);
        } else {
            // log a data discrepancy error (Ref: PO2-459)
            Date endDate = null;
            String epstatus = orderLineDTO.getAttribute(EndPointAttributeNamesEnum.EPSTATUS.name());
            if (ItemStatusEnum.CE.name().equalsIgnoreCase(epstatus)) {
                endDate = orderLineDTO.billEffectDate;
            }

            CustomFieldValue lastCfValue = getLastCfVersion(orderLineDTO.subscription, "END_POINT_" + epend.toUpperCase(), orderLineDTO.billEffectDate, false);
            if (orderDTO.errorCode != null) {
                return;
            }

            updateEndPointCf(orderLineDTO, endDate, epend, address, lastCfValue);
            // subscriptionService.getEntityManager().refresh(subscriptionService.update(orderLineDTO.subscription));
            subscriptionService.update(orderLineDTO.subscription);
        }

        log.info("############### End process the change end point bill items .....");
    }

    /**
     * Provide Remarks
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void provideRemarks(OrderLineDTO orderLineDTO) throws BusinessException {
        log.info("############### Start process the remark bill items .....");

        Map<String, String> remark = (Map<String, String>) orderLineDTO.subscription.getCfValuesNullSafe().getValue(SubscriptionOmsCFsEnum.REMARK.name());
        if (remark == null) {
            remark = new HashMap<>();
        }

        StringBuilder key = new StringBuilder();
        key.append(orderLineDTO.itemId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.itemComponentId)) {
            key.append(orderLineDTO.itemComponentId);
        }

        StringBuilder value = new StringBuilder();
        value.append(orderDTO.orderId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        String effectiveDate = DateUtil.formatDate(orderLineDTO.billEffectDate, DATE_TIME_PATTERN);
        if (!StringUtils.isBlank(effectiveDate)) {
            value.append(effectiveDate);
        }
        value.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        String remarkId = orderLineDTO.getAttribute(remarkAttributeNamesEnum.REMARK_ID.name());
        if (!StringUtils.isBlank(remarkId)) {
            value.append(remarkId);
        }
        value.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        String elementType = orderLineDTO.getAttribute(remarkAttributeNamesEnum.ELEMENT_TYPE.name());
        if (!StringUtils.isBlank(elementType)) {
            value.append(elementType);
        }
        value.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        String elementId = orderLineDTO.getAttribute(remarkAttributeNamesEnum.ELEMENT_ID.name());
        if (!StringUtils.isBlank(elementId)) {
            value.append(elementId);
        }
        value.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        String description = orderLineDTO.getAttribute(remarkAttributeNamesEnum.DESCRIPTION.name());
        if (!StringUtils.isBlank(description)) {
            value.append(description);
        }

        remark.put(key.toString(), value.toString());
        orderLineDTO.subscription.getCfValuesNullSafe().setValue(SubscriptionOmsCFsEnum.REMARK.name(), remark);
        subscriptionService.update(orderLineDTO.subscription);

        log.info("############### End process the remark bill items .....");
    }

    /**
     * Get address
     *
     * @param entity the entity
     * @param cfCode the CF code
     * @param effectDate the effect date
     * @return the address
     */
    private String getAddress(ICustomFieldEntity entity, String cfCode, Date effectDate) {
        String address = null;
        Map<String, String> endPoint = (Map<String, String>) entity.getCfValuesNullSafe().getValue(cfCode, effectDate);
        if (endPoint != null) {
            for (Map.Entry<String, String> entry : endPoint.entrySet()) {
                Map<String, String> value = (Map<String, String>) getCFValue(entity, cfCode, entry.getValue());
                if (value != null && !value.isEmpty()) {
                    String eptype = value.get(EndPointAttributeNamesEnum.EPTYPE.name());
                    if (EndPointTypeEnum.OC.name().equalsIgnoreCase(eptype) || EndPointTypeEnum.CU.name().equalsIgnoreCase(eptype)) {
                        StringBuilder addressBuilder = new StringBuilder();
                        addressBuilder.append(value.get(EndPointAttributeNamesEnum.Address_Line_1.name())).append(CustomFieldValue.MATRIX_KEY_SEPARATOR)
                            .append(value.get(EndPointAttributeNamesEnum.Address_Line_2.name())).append(CustomFieldValue.MATRIX_KEY_SEPARATOR)
                            .append(value.get(EndPointAttributeNamesEnum.Address_Line_3.name())).append(CustomFieldValue.MATRIX_KEY_SEPARATOR)
                            .append(value.get(EndPointAttributeNamesEnum.Address_Line_4.name())).append(CustomFieldValue.MATRIX_KEY_SEPARATOR)
                            .append(value.get(EndPointAttributeNamesEnum.Address_Line_4.name())).append(CustomFieldValue.MATRIX_KEY_SEPARATOR)
                            .append(value.get(EndPointAttributeNamesEnum.County.name())).append(CustomFieldValue.MATRIX_KEY_SEPARATOR)
                            .append(value.get(EndPointAttributeNamesEnum.Postal_District.name())).append(CustomFieldValue.MATRIX_KEY_SEPARATOR)
                            .append(value.get(EndPointAttributeNamesEnum.Country1.name())).append(CustomFieldValue.MATRIX_KEY_SEPARATOR)
                            .append(value.get(EndPointAttributeNamesEnum.EIRCODE.name()));
                        return addressBuilder.toString();
                    }
                }
            }
        }
        return address;
    }

    /**
     * Update association CF
     *
     * @param orderLineDTO the order line DTO
     * @param endDate the endDate
     * @param epend the epend
     * @param address the address
     * @param lastCfValue the last CF value
     */
    private void updateAssociationCf(OrderLineDTO orderLineDTO, Date endDate, String epend, String address, CustomFieldValue lastCfValue) {
        if (!StringUtils.isBlank(epend)) {
            Map<String, String> cfValue = getAssociationCfValue(orderLineDTO, address);
            updateCfValue(orderLineDTO.subscription, orderLineDTO.billEffectDate, "ASSOCIATION_" + epend.toUpperCase(), endDate, cfValue, lastCfValue);
        }
    }

    /**
     * Update eptype
     *
     * @param endPoint the end point
     */
    private void updateEptype(Map<String, String> endPoint) {
        if (endPoint != null) {
            setAttributeValue(endPoint, EndEnum.AS.name(), EndPointAttributeNamesEnum.EPTYPE.ordinal());
        }
    }

    /**
     * Update association
     *
     * @param orderLineDTO the order line DTO
     * @param endPoint the end point
     * @param address the address
     */
    private void updateAssociation(OrderLineDTO orderLineDTO, Map<String, String> endPoint, String address) {
        String assocend = orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCEND.name());
        if (!StringUtils.isBlank(assocend)) {

            CustomFieldValue lastAssociationCfValue = getLastCfVersion(orderLineDTO.subscription, "ASSOCIATION_" + assocend.toUpperCase(), orderLineDTO.billEffectDate, false);
            if (orderDTO.errorCode != null) {
                return;
            }
            CustomFieldValue lastEndPointCfValue = null;
            if (endPoint != null) {
                lastEndPointCfValue = getLastCfVersion(orderLineDTO.subscription, "END_POINT_" + assocend.toUpperCase(), orderLineDTO.billEffectDate, true);
                if (orderDTO.errorCode != null) {
                    return;
                }
            }

            // Store the association
            updateAssociationCf(orderLineDTO, null, assocend, address, lastAssociationCfValue);

            if (endPoint != null) {
                // Set the EPTYPE = AS
                updateEptype(endPoint);

                // Copy the EndPoint fields from the relevant group on the found subscriber /overwrite the current subscriber's A Endpoint group,
                // to the End Point fields group on the current subscriber
                if (lastEndPointCfValue != null) {
                    lastEndPointCfValue.setValue(endPoint);
                } else {
                    addNewVersionCfValue(orderLineDTO.subscription, orderLineDTO.billEffectDate, "END_POINT_" + assocend.toUpperCase(), null, endPoint);
                }

                subscriptionService.update(orderLineDTO.subscription);
            }
        }
    }

    /**
     * Provide Associations
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void provideAssociations(OrderLineDTO orderLineDTO) throws BusinessException {
        log.info("############### Start process the association bill items .....");
        String assoctype = orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCTYPE.name());
        String assocend = orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCEND.name());
        String assoccircid = orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCCIRCID.name());
        String address = null;
        Map<String, String> endPoint = null;

        if (!EndEnum.A.name().equalsIgnoreCase(assocend) && !EndEnum.B.name().equalsIgnoreCase(assocend) && !EndEnum.C.name().equalsIgnoreCase(assocend)) {
            setError(ErrorEnum.INVALID_DATA,
                "The " + AssociationAttributeNamesEnum.ASSOCEND.name() + " value is not equal to " + EndEnum.A.name() + ", " + EndEnum.B.name() + " nor " + EndEnum.C.name());
            return;
        }

        // If ASSOCTYPE = CH or BR, and ASSOCEND is A, B or C, then using the value in the ASSOCCIRCID tag,
        // look up the associated circuit to obtain the address for the 'Association'

        if (orderLineDTO.subscription == null) {
            setError(ErrorEnum.INVALID_DATA, "The subscription " + orderLineDTO.deducedServiceId + " is not found");
            return;
        }

        if (AssociationTypesEnum.CH.name().equalsIgnoreCase(assoctype) || AssociationTypesEnum.BR.name().equalsIgnoreCase(assoctype)) {
            if (StringUtils.isBlank(assoccircid)) {
                setError(ErrorEnum.INVALID_DATA, "Missing " + AssociationAttributeNamesEnum.ASSOCCIRCID.name() + " parameter.");
                return;
            }

            Subscription associatedSubscription = subscriptionService.findByCode(assoccircid);
            if (associatedSubscription == null) {
                setError(ErrorEnum.INVALID_DATA, "The associated subscription " + assoccircid + " is not found");
                return;
            }

            String addressA = getAddress(associatedSubscription, SubscriptionOmsCFsEnum.END_POINT_A.name(), orderLineDTO.billEffectDate);
            String addressB = getAddress(associatedSubscription, SubscriptionOmsCFsEnum.END_POINT_B.name(), orderLineDTO.billEffectDate);

            if (addressA != null && addressB != null) {
                setError(ErrorEnum.INVALID_DATA,
                    "more than one " + EndPointTypeEnum.OC.name() + " or " + EndPointTypeEnum.CU.name() + " endpoint found in the associated subscriber");
                return;
            } else if (addressA == null && addressB == null) {
                setError(ErrorEnum.INVALID_DATA, "no " + EndPointTypeEnum.OC.name() + " or " + EndPointTypeEnum.CU.name() + " endpoint found in the associated subscriber");
                return;
            }

            if (addressA != null) {
                address = addressA;
                endPoint = new HashMap<>(
                    (Map<String, String>) associatedSubscription.getCfValuesNullSafe().getValue(SubscriptionOmsCFsEnum.END_POINT_A.name(), orderLineDTO.billEffectDate));
            }
            if (addressB != null) {
                address = addressB;
                endPoint = new HashMap<>(
                    (Map<String, String>) associatedSubscription.getCfValuesNullSafe().getValue(SubscriptionOmsCFsEnum.END_POINT_B.name(), orderLineDTO.billEffectDate));
            }

        }

        updateAssociation(orderLineDTO, endPoint, address);

        log.info("############### End process the association bill items .....");
    }

    /**
     * Change Associations
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void changeAssociations(OrderLineDTO orderLineDTO) throws BusinessException {
        log.info("############### Start process the change association bill items .....");

        String assocend = orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCEND.name());
        if (!EndEnum.A.name().equalsIgnoreCase(assocend) && !EndEnum.B.name().equalsIgnoreCase(assocend) && !EndEnum.C.name().equalsIgnoreCase(assocend)) {
            String errorMessage = "The " + AssociationAttributeNamesEnum.ASSOCEND.name() + " value is not equal to " + EndEnum.A.name() + ", " + EndEnum.B.name() + " nor "
                    + EndEnum.C.name();
            setError(ErrorEnum.INVALID_DATA, errorMessage);
            return;
        }

        Map<String, String> association = (Map<String, String>) orderLineDTO.subscription.getCfValuesNullSafe().getValue("ASSOCIATION_" + assocend.toUpperCase(),
            orderLineDTO.billEffectDate);
        String assocstatus = orderLineDTO.getAttribute(AssociationAttributeNamesEnum.ASSOCSTATUS.name());
        if (association == null || !ItemStatusEnum.CE.name().equalsIgnoreCase(assocstatus)) {
            // PO2-470
            provideAssociations(orderLineDTO);
        } else {
            // If the ASSOCSTATUS of the ASSOCIATION on the bill item = CE,
            // then set the end date of the respective Association against the subscriber and do not look up the EP to retrieve the address, etc
            if (ItemStatusEnum.CE.name().equalsIgnoreCase(assocstatus)) {

                CustomFieldValue lastCfValue = getLastCfVersion(orderLineDTO.subscription, "ASSOCIATION_" + assocend.toUpperCase(), orderLineDTO.billEffectDate, false);
                if (orderDTO.errorCode != null) {
                    return;
                }

                StringBuilder address = new StringBuilder();
                address.append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(CustomFieldValue.MATRIX_KEY_SEPARATOR)
                    .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(CustomFieldValue.MATRIX_KEY_SEPARATOR)
                    .append(CustomFieldValue.MATRIX_KEY_SEPARATOR);

                // Store the association
                updateAssociationCf(orderLineDTO, orderLineDTO.billEffectDate, assocend, address.toString(), lastCfValue);
                // subscriptionService.getEntityManager().refresh(subscriptionService.update(orderLineDTO.subscription));
                subscriptionService.update(orderLineDTO.subscription);
            }
        }

        log.info("############### End process the change association bill items .....");
    }

    /**
     * Get ancillary service instance
     *
     * @param orderLineDTO the order line DTO
     * @return the service instance
     */
    private ServiceInstance getAncillaryServiceInstance(OrderLineDTO orderLineDTO) {
        ServiceInstance ancillaryService = null;
        if (orderLineDTO.subscription != null) {
            List<ServiceInstance> serviceInstances = orderLineDTO.subscription.getServiceInstances();
            if (serviceInstances != null && !serviceInstances.isEmpty()) {
                for (ServiceInstance serviceInstance : serviceInstances) {
                    String itemComponentId = (String) serviceInstance.getCfValuesNullSafe().getValue("ITEM_COMPONENT_ID");
                    /** Step 5 [PO2-474] : Using the Item_Component_Id and Bill Code, attempt to locate the respective charge (i.e. ancillary service) against the subscriber **/

                    /** Step 4 [PO2-490] : Using the item component id, locate the subscriber service **/
                    if (!StringUtils.isBlank(itemComponentId) && itemComponentId.equalsIgnoreCase(orderLineDTO.itemComponentId)) {
                        ancillaryService = serviceInstance;
                        break;
                    }
                }
            }
        }
        return ancillaryService;
    }

    /**
     * Get charges Cf value
     *
     * @param orderLineDTO the order line DTO
     * @return the charges Cf value
     */
    private Map<String, String> getChargesCfValue(OrderLineDTO orderLineDTO) {
        Map<String, String> cfValue = new HashMap<>();

        StringBuilder keys = new StringBuilder();
        keys.append(orderLineDTO.itemId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.itemComponentId)) {
            keys.append(orderLineDTO.itemComponentId);
        }

        StringBuilder values = new StringBuilder();
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.CHARGE_TYPE.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.CHARGE_TYPE.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.STATUS.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.STATUS.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.STATE.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.STATE.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.COMPONENT_ID.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.COMPONENT_ID.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.CHARGE_ID.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.CHARGE_ID.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.PRICE_PLAN.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.PRICE_PLAN.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.PRICEPLAN.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.PRICEPLAN.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.DISTANCE.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.DISTANCE.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.SERVICE_TYPE.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.SERVICE_TYPE.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.PRICE_PLAN_IND.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.PRICE_PLAN_IND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.BANDWIDTH_IND.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.BANDWIDTH_IND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.TRANSMISSION_IND.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.TRANSMISSION_IND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.CMMITMNT_PERID_IND.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.CMMITMNT_PERID_IND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.ZONE_A_IND.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.ZONE_A_IND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.ZONE_B_IND.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.ZONE_B_IND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.REGION_IND.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.REGION_IND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.SLA_IND.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.SLA_IND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.SERV_CATEGORY_IND.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.SERV_CATEGORY_IND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.ANALOGUE_QUAL_IND.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.ANALOGUE_QUAL_IND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.ANALOGUE_RATE_IND.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.ANALOGUE_RATE_IND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.CPE_TYPE_IND.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.CPE_TYPE_IND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.CPE_DESCRIPTION_IND.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.CPE_DESCRIPTION_IND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.ETS_TYPE_IND.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.ETS_TYPE_IND.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_01.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_01.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_02.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_02.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_03.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_03.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_04.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_04.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_05.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_05.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_06.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_06.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_07.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_07.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_08.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_08.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_09.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_09.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_10.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.IND_10.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.AMT.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.AMT.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.QUANTITY.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.QUANTITY.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.COMMITMENT_PERIOD.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.COMMITMENT_PERIOD.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.NOTBEFOREDATE.name()))) {
            values.append(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.NOTBEFOREDATE.name()));
        }
        values.append(CustomFieldValue.MATRIX_KEY_SEPARATOR);

        cfValue.put(keys.toString(), values.toString());
        return cfValue;
    }

    /**
     * Update charges CF
     *
     * @param serviceInstance the service instance
     * @param orderLineDTO the order line DTO
     * @param endDate the end date
     * @param lastCfValue the last CF value
     * @param cfValue the cf value
     */
    private void updateChargesCf(ServiceInstance serviceInstance, OrderLineDTO orderLineDTO, Date endDate, CustomFieldValue lastCfValue, Map<String, String> cfValue) {
        if (cfValue == null) {
            cfValue = getChargesCfValue(orderLineDTO);
        }
        updateCfValue(serviceInstance, orderLineDTO.billEffectDate, ServiceInstanceOmsCFsEnum.CHARGES.name(), endDate, cfValue, lastCfValue);
    }

    /**
     * Get old quantity
     *
     * @param orderLineDTO the order line DTO
     * @return the new quantity
     */
    private BigDecimal getOldQuantity(OrderLineDTO orderLineDTO, ServiceInstance serviceInstance) {
        CustomFieldValue lastCfValue = getLastCfVersion(serviceInstance, ServiceInstanceOmsCFsEnum.CHARGES.name(), orderLineDTO.billEffectDate, true);
        if (lastCfValue != null && lastCfValue.getValue() != null) {
            Map<String, String> cfValue = (Map<String, String>) lastCfValue.getValue();

            for (Map.Entry<String, String> entry : cfValue.entrySet()) {
                Map<String, String> value = (Map<String, String>) getCFValue(serviceInstance, ServiceInstanceOmsCFsEnum.CHARGES.name(), entry.getValue());
                if (value != null && !value.isEmpty() && !StringUtils.isBlank(value.get(ChargesAttributeNamesEnum.QUANTITY.name()))) {
                    try {
                        return new BigDecimal(value.get(ChargesAttributeNamesEnum.QUANTITY.name()));
                    } catch (NumberFormatException e) {
                    }
                }
            }
        }
        return null;
    }

    /**
     * Get new quantity
     *
     * @param orderLineDTO the order line DTO
     * @return the new quantity
     */
    private BigDecimal getNewQuantity(OrderLineDTO orderLineDTO) {
        BigDecimal newQuantity = new BigDecimal(orderLineDTO.billQuantity);
        if (ItemTypeEnum.CHARGE.getLabel().equalsIgnoreCase(orderLineDTO.itemType) && !StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.QUANTITY.name()))) {
            newQuantity = new BigDecimal(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.QUANTITY.name()));
        }
        return newQuantity;
    }

    /**
     * Get the system calculated quantity
     *
     * @param orderLineDTO the order line DTO
     * @param serviceInstance the service instance
     * @return the system calculated quantity
     */
    private BigDecimal getSystemCalculatedQuantity(OrderLineDTO orderLineDTO, ServiceInstance serviceInstance) {

        BigDecimal oldQuantity = getOldQuantity(orderLineDTO, serviceInstance);
        BigDecimal newQuantity = getNewQuantity(orderLineDTO);

        /**
         * If a previous version does not exist (i.e. the Charge bill item results in the creation of a new/active ancillary service charge), use the quantity from the bill item as
         * 'system calculated quantity'
         */
        if (oldQuantity != null) {
            /** subtract the quantity value that previously against the respective subscriber ancillary service from the quantity value on the bill item **/
            newQuantity = newQuantity.subtract(oldQuantity);
        }
        return newQuantity;
    }

    /**
     * create new version charges CF
     *
     * @param orderLineDTO the order line DTO
     * @param serviceInstance the service instance
     * @param cfValue the cf value
     * @throws BusinessException the business exception
     * @throws BusinessException the business exception
     */
    private void createNewVersionChargesCf(OrderLineDTO orderLineDTO, ServiceInstance serviceInstance, Map<String, String> cfValue) throws BusinessException {
        CustomFieldValue lastCfValue = getLastCfVersion(serviceInstance, ServiceInstanceOmsCFsEnum.CHARGES.name(), orderLineDTO.billEffectDate, false);
        if (orderDTO.errorCode != null) {
            return;
        }

        /** Store system calculated quantity CF **/
        setSystemCalculatedQuantity(orderLineDTO, serviceInstance);

        /** Store Charges CF **/
        Date endDate = orderLineDTO.rentalLiabilityDate;
        updateChargesCf(serviceInstance, orderLineDTO, endDate, lastCfValue, cfValue);
        serviceInstanceService.update(serviceInstance);
    }

    /**
     * Validate input quantity
     *
     * @param orderLineDTO the order line DTO
     * @return true is valid quantity
     */
    private boolean validateInputQuantity(OrderLineDTO orderLineDTO) {
        Long maxQuantity = (Long) orderLineDTO.catalog.get(Catalog.MAX_QUANTITY.name());
        if (maxQuantity == null) {
            setError(ErrorEnum.INVALID_DATA, "The MAX_QUANTITY attribute is missing in catalog for the offer " + orderLineDTO.offerTemplate.getCode());
            return false;
        }

        if (new BigDecimal(maxQuantity).compareTo(getNewQuantity(orderLineDTO)) < 0) {
            setError(ErrorEnum.INVALID_DATA, "the quantity provided exceeds the maximum quantity allowed for the service :" + orderLineDTO.billCode);
            return false;
        }
        return true;
    }

    private Map<String, String> getCharges(OrderLineDTO orderLineDTO, ServiceInstance serviceInstance) {
        Map<String, String> chargesCF = null;

        if (serviceInstance != null) {
            /*
             * String keyToMatch = orderLineDTO.itemId + CustomFieldValue.MATRIX_KEY_SEPARATOR + orderLineDTO.itemComponentId; chargesCF = (Map<String, String>)
             * customFieldInstanceService.getCFValueByClosestMatch(serviceInstance, ServiceInstanceOmsCFsEnum.CHARGES.name(), orderLineDTO.billEffectDate, keyToMatch);
             */

            CustomFieldValue lastCfValue = getLastCfVersion(serviceInstance, ServiceInstanceOmsCFsEnum.CHARGES.name(), orderLineDTO.billEffectDate, false);
            if (orderDTO.errorCode != null) {
                return null;
            }
            if (lastCfValue != null && lastCfValue.getValue() != null) {
                Map<String, String> cfValue = (Map<String, String>) lastCfValue.getValue();

                for (Map.Entry<String, String> entry : cfValue.entrySet()) {
                    Map<String, String> value = (Map<String, String>) getCFValue(serviceInstance, ServiceInstanceOmsCFsEnum.CHARGES.name(), entry.getValue());
                    if (value != null && !value.isEmpty()) {
                        chargesCF = value;
                        break;
                    }
                }
            }
        }
        return chargesCF;
    }

    /**
     * Process Service Charges
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void processServiceCharges(OrderLineDTO orderLineDTO) throws BusinessException {
        log.info("############### Start process service charges .....");

        ServiceInstance mainServiceInstance = getMainServiceInstance(orderLineDTO.subscription);
        if (mainServiceInstance == null) {
            setError(ErrorEnum.INVALID_DATA, "The main service is not found on subscription" + orderLineDTO.subscription.getCode());
            return;
        }

        boolean isMain = ServiceTypeEnum.MAIN.getLabel().equalsIgnoreCase(getServiceType(orderLineDTO));
        if (isMain) {
            if (BillActionEnum.PR.name().equalsIgnoreCase(orderLineDTO.billAction)) {
                setError(ErrorEnum.INVALID_DATA, "There is already a main service with the same bill code (" + orderLineDTO.billCode + ") on this subscription");
            } else {
                setError(ErrorEnum.INVALID_DATA, "Can't change the charge of the main service (" + orderLineDTO.billCode + ")");
            }
            return;
        }

        /**
         * Validate the bill item quantity
         */
        if (!validateInputQuantity(orderLineDTO)) {
            return;
        }

        /*
         * // If the STATE of the Subscription Main Details is = CA, this will be addressed in PO2-486 String state = getValueAttribute(mainServiceInstance,
         * ServiceInstanceOmsCFsEnum.MAIN_SUBSCRIPTION_DETAILS.name(), orderLineDTO.billEffectDate, SubMainDetailsAttributeNamesEnum.STATE.name());
         */

        /**
         * If the STATE of the charge is = CA, this will be addressed in PO2-486
         */
        String state = orderLineDTO.getAttribute(ChargesAttributeNamesEnum.STATE.name());
        if (StateEnum.CA.name().equalsIgnoreCase(state)) {
            /** execute PO2-486 [OMS Cancelled Orders] **/
        } else {

            /**
             * Using the Item_Component_Id and Bill Code, attempt to locate the respective charge (i.e. ancillary service) against the subscriber
             */

            ServiceInstance serviceInstance = getAncillaryServiceInstance(orderLineDTO);
            /*
             * if (serviceInstance == null && BillActionEnum.CH.name().equalsIgnoreCase(orderLineDTO.billAction)) { setError(ErrorEnum.INVALID_DATA, "The service to change " +
             * orderLineDTO.billCode + " is not found on subscription" + orderLineDTO.subscription.getCode()); return; }
             */

            Map<String, String> chargesCF = getCharges(orderLineDTO, serviceInstance);
            if (chargesCF != null) {
                String status = chargesCF.get(ChargesAttributeNamesEnum.STATUS.name());
                if (ItemStatusEnum.CE.name().equalsIgnoreCase(status)) {
                    /** execute PO2-490 [OMS Cease Ancillary] **/
                    ceaseAncillary(orderLineDTO);
                    return;
                }
                if (ItemStatusEnum.AC.name().equalsIgnoreCase(status)) {
                    reRateWalletOperation(orderLineDTO, serviceInstance);

                    CustomFieldValue lastCfValue = getLastCfVersion(serviceInstance, ServiceInstanceOmsCFsEnum.CHARGES.name(), orderLineDTO.billEffectDate, false);

                    /** Store Charges CF **/
                    updateChargesCf(serviceInstance, orderLineDTO, null, lastCfValue, null);
                    serviceInstanceService.update(serviceInstance);
                }
            } else {
                // If the bill code on the (Charge) bill item is NOT found against the Subscriber Ancillary Service, or
                // If the bill code on the (Charge) bill item was found but the Item_Component_Id's found are different

                ServiceTemplate serviceTemplate = getServiceTemplate(orderLineDTO);
                if (orderDTO.errorCode != null) {
                    return;
                }

                String calendar = (String) orderLineDTO.catalog.get(Catalog.CALENDAR.name());
                if (StringUtils.isBlank(calendar)) {
                    setError(ErrorEnum.INVALID_DATA, "The calendar attribute is missing in catalog for the offer " + orderLineDTO.offerTemplate.getCode());
                    return;
                }

                // activate the ancillary service (i.e. create it) against the subscriber
                serviceInstance = activateService(orderLineDTO, serviceTemplate, ServiceTypeEnum.ANCILLARY.getLabel());

                // Rating :
                // Using the bill code on the bill item, find the Tariff Plan and calculate charges:

                // a) If a new version was created, calculate recurring charges for the existing / old Charge group attribute values

                // Referencing the history record (e.g. end date and attribute values), calculate (pro-rate OUT) the charges for the existing/old Charge attribute group values

                // b) Calculate recurring charges for the Charge group version attribute values (i.e. those from the bill item being processed)

                // Using the effective date for the latest Charge group version, calculate (pro-rate IN) the charges for the new Charge group attribute values

                // Store Charges CF
                updateChargesCf(serviceInstance, orderLineDTO, null, null, null);
                serviceInstanceService.update(serviceInstance);
                subscriptionService.update(orderLineDTO.subscription);
            }
        }

        log.info("############### End process service charges .....");
    }

    /**
     * MISC Charge
     *
     * @param orderLineDTO the order line DTO
     * @throws BusinessException the business exception
     */
    private void miscCharge(OrderLineDTO orderLineDTO) throws BusinessException {
        log.info("############### Start process the misc charge bill items .....");

        applyMiscOneShotCharge(orderLineDTO);

        log.info("############### End process the misc charge bill items .....");
    }

    /**
     * get one-Shot charge price
     *
     * @param orderLineDTO order line
     * @return the one-Shot charge price
     */
    private BigDecimal getOneShotChargePrice(OrderLineDTO orderLineDTO) {

        // case where the Service_Id is not provided in the xml order.
        if (StringUtils.isBlank(orderLineDTO.serviceId)) {
            boolean isChargeFound = false;
            Double chargePrice = null;
            for (AttributeDTO attributeDTO : orderLineDTO.attributes) {
                if ("Charge".equalsIgnoreCase(attributeDTO.code)) {
                    isChargeFound = true;
                    try {
                        chargePrice = new Double(attributeDTO.value);
                    } catch (Exception ex) {
                        chargePrice = null;
                    }
                    break;
                }
            }
            if (!isChargeFound) {
                setError(ErrorEnum.INVALID_DATA, "No Charge Attribute found");
                return null;
            }
            if (chargePrice == null) {
                setError(ErrorEnum.INVALID_DATA, "No price found for a misc charge");
                return null;
            }
            return new BigDecimal(chargePrice);
        }

        BigDecimal oneShotTariff = getOneShotTariff(orderLineDTO);

        if (oneShotTariff == null) {
            setError(ErrorEnum.INVALID_DATA, "No price found for a misc charge");
            return null;
        }
        return oneShotTariff;
    }

    /**
     * Get one shot tariff
     *
     * @param orderLineDTO the order line DTO
     * @return the one shot tariff
     */
    private BigDecimal getOneShotTariff(OrderLineDTO orderLineDTO) {
        try {
            String exchangeClass = getExchangeClass(orderLineDTO);
            return getTariff(orderLineDTO, "O", exchangeClass);
        } catch (NumberFormatException | IndexOutOfBoundsException e) {
        }
        return null;
    }

    /**
     * apply MiscOneShotCharge
     *
     * @param orderLineDTO order line
     */
    private void applyMiscOneShotCharge(OrderLineDTO orderLineDTO) {
        OneShotChargeTemplate chargetemplate = null;
        BigDecimal chargePrice = null;
        Map<String, String> catalog = null;

        OfferTemplate offerTemplate = offerTemplateService.findByCode(MISC_OFFER_TEMPLATE_CODE);
        if (offerTemplate == null) {
            setError(ErrorEnum.INVALID_DATA, "The offer template MISC_WB is not found");
            return;
        }

        catalog = getCatalog(orderLineDTO, ChargeTypeEnum.O.name());
        if (catalog == null) {
            setError(ErrorEnum.INVALID_DATA, "Catalog is not found for date " + orderLineDTO.billEffectDate);
            return;
        }

        chargetemplate = chargeTemplateService.findByCode(MISC_CHARGE_TEMPLATE_CODE);
        if (chargetemplate == null) {
            setError(ErrorEnum.INVALID_DATA, "The one-shot charge " + orderLineDTO.billCode + " is not found");
            return;
        }

        chargePrice = getOneShotChargePrice(orderLineDTO);

        OneShotChargeInstance oneShotChargeInstance = oneShotChargeInstanceService.oneShotChargeApplication(orderLineDTO.subscription, chargetemplate, null,
            orderLineDTO.billEffectDate, chargePrice, null, null, null, null, null, orderLineDTO.operatorOrderNumber, true);

        // TODO : to force the subscription to active status (check this point with product owner)
        if (orderLineDTO.subscription.getStatus() == SubscriptionStatusEnum.CREATED) {
            orderLineDTO.subscription.setStatus(SubscriptionStatusEnum.ACTIVE);
        }

        oneShotChargeInstance.setCode(orderLineDTO.billCode);
        String chargeDescription = catalog.get(Catalog.DESCRIPTION.name());
        if (!StringUtils.isBlank(chargeDescription)) {
            oneShotChargeInstance.setDescription(chargeDescription);
        }
        oneShotChargeInstanceService.update(oneShotChargeInstance);
    }

    /**
     * Add the provided seconds to date
     *
     * @param date the date
     * @param seconds the seconds to add to date
     * @return the result date
     */
    public static Date addSecondsToDate(Date date, Integer seconds) {
        Date result = null;

        if (date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.SECOND, seconds);
            result = calendar.getTime();
        }

        return result;
    }

    /**
     * error enum
     */
    public enum ErrorEnum {
        // The request could not be understood by the server due to malformed syntax.
        // The client SHOULD NOT repeat the request without modifications.
        BAD_REQUEST("BAD_REQUEST", HttpURLConnection.HTTP_BAD_REQUEST),

        BAD_REQUEST_MISSING_PARAMETER("MISSING_PARAMETER", HttpURLConnection.HTTP_BAD_REQUEST),

        BAD_REQUEST_INVALID_VALUE("INVALID_VALUE", HttpURLConnection.HTTP_BAD_REQUEST),

        BAD_REQUEST_INVALID_SIZE("INVALID_SIZE", HttpURLConnection.HTTP_BAD_REQUEST),

        // Conflict : Indicates that the request could not be processed because of conflict in the current state of the resource,
        // such as an edit conflict between multiple simultaneous updates.
        // FIXME : the correct error code is 409 instead of 500
        ORDER_ALREADY_EXISTS("ORDER_ALREADY_EXISTS", HttpURLConnection.HTTP_INTERNAL_ERROR),

        // This error condition may occur if an XML request body contains well-formed (i.e., syntactically correct), but semantically erroneous, XML instructions.
        // the code of this error is 422 but in the case of eir we still create the order successfully with 200 status but in error.
        INVALID_DATA("INVALID_DATA", HttpURLConnection.HTTP_OK),

        // Internal Server Error
        GENERIC_API_EXCEPTION("GENERIC_API_EXCEPTION", HttpURLConnection.HTTP_INTERNAL_ERROR);

        private String code;
        private int status;

        ErrorEnum(String code, int status) {
            this.code = code;
            this.status = status;
        }

        public String getCode() {
            return this.code;
        }

        public int getStatus() {
            return this.status;
        }
    }

    /**
     * Item type enum
     */
    public enum ItemTypeEnum {
        SUBSCRIPTION_MAIN_DETAILS("Subscription main details"), END_POINT("EndPoint"), ASSOCIATION("Association"), CHARGE("Charge"), REMARKS("Remark");

        private String label;

        private static final Map<String, ItemTypeEnum> ENUM_MAP;

        ItemTypeEnum(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }

        static {
            Map<String, ItemTypeEnum> map = new ConcurrentHashMap<>();
            for (ItemTypeEnum instance : ItemTypeEnum.values()) {
                map.put(instance.getLabel(), instance);
            }
            ENUM_MAP = Collections.unmodifiableMap(map);
        }

        public static ItemTypeEnum get(String label) {
            return ENUM_MAP.get(label);
        }
    }

    /**
     * Item status enum
     */
    public enum ItemStatusEnum {
        AC("Active"), CE("Ceased");

        private String label;

        ItemStatusEnum(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    /**
     * Bill action enum
     */
    public enum BillActionEnum {
        PR, CH, CE, M;

        private static Set<String> _values = new HashSet<>();

        static {
            for (BillActionEnum choice : BillActionEnum.values()) {
                _values.add(choice.name());
            }
        }

        public static boolean contains(String value) {
            return !StringUtils.isBlank(value) && _values.contains(value.toUpperCase());
        }
    }

    /**
     * Action qualifier enum
     */
    public enum ActionQualifierEnum {
        APR, DPR
    }

    /**
     * Charge type enum
     */
    public enum ChargeTypeEnum {
        R("Recurring"), O("OneShot");

        private String label;

        ChargeTypeEnum(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    /**
     * Which end of the circuit is the endpoint on
     */
    public enum EndEnum {
        A, B, C, AS
    }

    /**
     * State enum
     */
    public enum StateEnum {
        OR("Ordered"), AS("Assigned"), CA("Cancelled");

        private String label;

        StateEnum(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    /**
     * End Point Type Enum
     */
    public enum EndPointTypeEnum {
        CU("Irish Customer Premises"), OC("Foreign Customer Premises"), LE("Local Exchange"), SE("Serving Exchange"), PO("Intl/Partner point of presence");

        private String label;

        EndPointTypeEnum(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    /**
     * Customer custom fields Enum
     */
    public enum CustomerOmsCFsEnum {
        NEW_TARIFF
    }

    /**
     * Offer template custom fields Enum
     */
    public enum OfferTemplateOmsCFsEnum {
        CLASS_OF_SERVICE, NEW_CATALOG, NEW_TARIFF
    }

    /**
     * Service instance custom fields Enum
     */
    public enum ServiceInstanceOmsCFsEnum {
        MAIN_SUBSCRIPTION_DETAILS, CHARGES, NEW_TARIFF, SYSTEM_CALCULATED_QUANTITY, RENTAL_LIABILITY_DATE
    }

    /**
     * Subscription custom fields Enum
     */
    public enum SubscriptionOmsCFsEnum {
        END_POINT_A, END_POINT_B, ASSOCIATION_A, ASSOCIATION_B, ASSOCIATION_C, REMARK
    }

    /**
     * End point attribute names Enum
     */
    public enum SubMainDetailsAttributeNamesEnum {
        SERVICE_TYPE, STATUS, STATE, PROCESSINGSTATUS, COMMITMENT_PERIOD, CMMITMNTTREATMENT, XREF, SUBORDER, PRICE_PLAN, AMT, PRICEPLAN, SITE_GENERAL_NUMBER
    }

    /**
     * End point attribute names Enum
     */
    public enum EndPointAttributeNamesEnum {
        EPID, EPTYPE, EPEND, EPADDRESS, EPVERSION, EPSTATUS, EPSTART, EPSTATE, EPORDER, Address_Line_1, Address_Line_2, Address_Line_3, Address_Line_4, Address_Line_5, County, Postal_District, Country1, EIRCODE
    }

    /**
     * Association attribute names Enum
     */
    public enum AssociationAttributeNamesEnum {
        ASSOCCIRCID, ASSOCTYPE, ASSOCEND, ASSOCTRIGGER, ASSOCVERSION, ASSOCSTATUS, ASSOCSTART, ASSOCSTATE, ASSOCORDER
    }

    /**
     * Charges attribute names Enum
     */
    public enum ChargesAttributeNamesEnum {
        CHARGE_TYPE, STATUS, STATE, COMPONENT_ID, CHARGE_ID, PRICE_PLAN, PRICEPLAN, DISTANCE, SERVICE_TYPE, PRICE_PLAN_IND, BANDWIDTH_IND, TRANSMISSION_IND, CMMITMNT_PERID_IND, ZONE_A_IND, ZONE_B_IND, REGION_IND, SLA_IND, SERV_CATEGORY_IND, ANALOGUE_QUAL_IND, ANALOGUE_RATE_IND, CPE_TYPE_IND, CPE_DESCRIPTION_IND, ETS_TYPE_IND, IND_01, IND_02, IND_03, IND_04, IND_05, IND_06, IND_07, IND_08, IND_09, IND_10, AMT, QUANTITY, COMMITMENT_PERIOD, NOTBEFOREDATE
    }

    /**
     * Tariff attribute names Enum
     */
    public enum TariffAttributeNamesEnum {
        DENSITY, REGION, SLA, CLASS_SERVICE, EF, AF, EXCHANGE
    }

    /**
     * Order items attribute names Enum
     */
    public enum OrderItemsNamesEnum {
        Item_Id, Operator_Id, Item_Component_Id, Item_Version, Item_Type, Agent_Code, UAN, Master_Account_Nb, Service_Id, Ref_Service_Id, Xref_Service_Id, Bill_Code, Action, Action_Qualifier, Quantity, Effect_Date, Service_Type, Offer_Name, Op_Order_Number
    }

    /**
     * Association types Enum
     */
    public enum AssociationTypesEnum {
        CH("Channeling"), BR("Bearing"), AS("Associated"), DI("Diversity");

        private String label;

        AssociationTypesEnum(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    /**
     * Bill Attribute names Enum
     */
    public enum BillAttributeNamesEnum {
        Value, Unit, Effective_Date
    }

    /**
     * Service type Enum
     */
    public enum ServiceTypeEnum {
        MAIN("Main"), ANCILLARY("Ancillary");

        private String label;

        ServiceTypeEnum(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    /**
     * Service code Enum
     */
    public enum ServiceCodeEnum {
        WBO_O_SERVICE, WBO_R_SERVICE, WBO_OR_SERVICE
    }

    /**
     * Ancillary service Enum
     */
    public enum AncillaryServiceEnum {
        MSN, DDI
    }

    /**
     * The Catalog Enum
     */
    enum Catalog {
        DESCRIPTION, SERVICE_TYPE, CALENDAR, IN_ADVANCE, MAX_QUANTITY, INV_CAT, TAX_CLASS, PRODUCT_SET, MIGRATION_CODE, SUBSCRIBER_PREFIX;
    }

    /**
     * Engagement duration attribute names Enum
     */
    public enum EngagementDurationAttributeNamesEnum {
        Customer, Offer, startDate, endDate, minTerm
    }

    /**
     * Min Term attribute names Enum
     */
    public enum MinTermAttributeNamesEnum {
        START_DATE, END_DATE, OAO, PRODUCT_CODE, MINIMUM_TERM_PERIOD, NOTICE_CEASE_PERIOD
    }

    /**
     * Service migration attribute names Enum
     */
    public enum ServiceMigrationAttributeNamesEnum {
        EVENT, APPLY_MT_ON_PREVIOUS, CONNECTION_CHARGE_ON, RESET_ENGAGEMENT, BILL_CODE_TO_APPLY
    }

    /**
     * Remark attribute names Enum
     */
    public enum remarkAttributeNamesEnum {
        Item_Id, Item_Component_Id, Order_Id, Effective_Date, REMARK_ID, ELEMENT_TYPE, ELEMENT_ID, DESCRIPTION
    }

    /**
     * Period unit Enum
     */
    public enum PeriodUnitEnum {
        Days, Months, Years
    }

    /**
     * Charge type attributes enum
     */
    public enum ChargeTypeAttributesEnum {
        RN("Recurring"), IN("installation"), MO("Move fee"), CW("Cost of Work");

        private String label;

        ChargeTypeAttributesEnum(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    /**
     * Check number is double
     *
     * @param strNum the string number
     * @return true is the string is double.
     */
    private static boolean isDouble(String strNum) {
        try {
            Double.parseDouble(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    /**
     * Check number is integer
     *
     * @param strNum the string number
     * @return true is the string is integer.
     */
    private static boolean isInteger(String strNum) {
        try {
            Integer.parseInt(strNum);
        } catch (NumberFormatException | NullPointerException nfe) {
            return false;
        }
        return true;
    }

    /**
     * Get catalog
     *
     * @param orderLineDTO the order line DTO
     * @param offerTemplate the offer template
     * @return the catalog
     */
    private Map<String, Object> getCatalog(OrderLineDTO orderLineDTO, OfferTemplate offerTemplate) {
        Map<String, Object> catalog = null;
        String keyToMatch = null;
        if (offerTemplate != null) {
            String chargeType = ChargeTypeEnum.R.name();
            keyToMatch = orderLineDTO.billCode + CustomFieldValue.MATRIX_KEY_SEPARATOR + chargeType;
            catalog = (Map<String, Object>) customFieldInstanceService.getCFValueByClosestMatch(offerTemplate, OfferTemplateOmsCFsEnum.NEW_CATALOG.name(),
                orderLineDTO.billEffectDate, keyToMatch);
            if (catalog == null) {
                chargeType = ChargeTypeEnum.O.name();
                keyToMatch = orderLineDTO.billCode + CustomFieldValue.MATRIX_KEY_SEPARATOR + chargeType;
                catalog = (Map<String, Object>) customFieldInstanceService.getCFValueByClosestMatch(offerTemplate, OfferTemplateOmsCFsEnum.NEW_CATALOG.name(),
                    orderLineDTO.billEffectDate, keyToMatch);
            }
            if (catalog != null) {
                orderLineDTO.chargeType = chargeType;
            }
        }
        return catalog;
    }

    /**
     * Get catalog
     *
     * @param orderLineDTO the order line DTO
     * @param chargeType the charge type
     * @return the catalog
     */
    private Map<String, String> getCatalog(OrderLineDTO orderLineDTO, String chargeType) {
        Map<String, String> catalog = null;
        String keyToMatch = null;
        if (orderLineDTO.offerTemplate != null && !StringUtils.isBlank(chargeType)) {
            keyToMatch = orderLineDTO.billCode + CustomFieldValue.MATRIX_KEY_SEPARATOR + chargeType;
            catalog = (Map<String, String>) customFieldInstanceService.getCFValueByClosestMatch(orderLineDTO.offerTemplate, OfferTemplateOmsCFsEnum.NEW_CATALOG.name(),
                orderLineDTO.billEffectDate, keyToMatch);
        }
        return catalog;
    }

    /**
     * Get offer template
     *
     * @param orderLineDTO the order line DTO
     * @return offer template
     */
    private OfferTemplate getOfferTemplate(OrderLineDTO orderLineDTO) {
        if (!applyingCharges(orderLineDTO)) {
            return null;
        }
        List<OfferTemplate> offers = offerTemplateService.listActiveByDate(orderLineDTO.billEffectDate);
        Map<String, Object> catalog = null;
        String[] offerCodes = { MISC_OFFER_TEMPLATE_CODE, EIR_OFFER_TEMPLATE_CODE, NGA_FTH_OFFER_TEMPLATE_CODE };
        for (OfferTemplate offerTemplate : offers) {
            if (offerTemplate != null) {
                if (!Arrays.stream(offerCodes).anyMatch(offerTemplate.getCode()::equals)) {
                    catalog = getCatalog(orderLineDTO, offerTemplate);
                    if (catalog != null) {
                        orderLineDTO.catalog = catalog;
                        return offerTemplate;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Parse multi-value value from string to a map of values
     *
     * @param entity
     * @param cfCode
     * @param value
     * @return
     */
    private Object getCFValue(ICustomFieldEntity entity, String cfCode, String value) {
        CustomFieldTemplate cft = customFieldTemplateService.findByCodeAndAppliesTo(cfCode, entity);
        if (cft != null && cft.getFieldType() == CustomFieldTypeEnum.MULTI_VALUE) {
            return cft.deserializeMultiValue(value, null);
        }
        return null;
    }

    private String getCFValue(ICustomFieldEntity entity, String cfCode, Map<String, Object> mapValues) {
        CustomFieldTemplate cft = customFieldTemplateService.findByCodeAndAppliesTo(cfCode, entity);
        if (cft != null && cft.getFieldType() == CustomFieldTypeEnum.MULTI_VALUE) {
            return cft.serializeMultiValue(mapValues);
        }
        return null;
    }

    /**
     * Get attribute name
     *
     * @param entity the entity
     * @param cfCode the CF code
     * @param effectDate the effect date
     * @param attributeName the attribute name
     * @return the attribute name
     */
    private String getValueAttribute(ICustomFieldEntity entity, String cfCode, Date effectDate, String attributeName) {
        Map<String, String> cfValue = (Map<String, String>) entity.getCfValuesNullSafe().getValue(cfCode, effectDate);
        if (cfValue != null) {
            for (Map.Entry<String, String> entry : cfValue.entrySet()) {
                Map<String, String> value = (Map<String, String>) getCFValue(entity, cfCode, entry.getValue());
                if (value != null && !value.isEmpty()) {
                    return value.get(attributeName);
                }
            }
        }
        return null;
    }

    /**
     * Get a RAW single custom field value entity for a given custom field for a given date.
     *
     * @param cfCode Custom field code
     * @param date Date
     * @return CF value entity
     */
    private CustomFieldValue getCfValue(Map<String, List<CustomFieldValue>> valuesByCode, String cfCode, Date date) {
        if (valuesByCode == null) {
            return null;
        }
        List<CustomFieldValue> cfValues = valuesByCode.get(cfCode);
        if (cfValues != null && !cfValues.isEmpty()) {
            CustomFieldValue valueFound = null;
            for (CustomFieldValue cfValue : cfValues) {
                if (cfValue.getPeriod() == null && (valueFound == null || valueFound.getPriority() < cfValue.getPriority())) {
                    valueFound = cfValue;

                } else if (cfValue.getPeriod() != null && cfValue.getPeriod().isCorrespondsToPeriod(date)) {
                    if (valueFound == null || valueFound.getPriority() < cfValue.getPriority()) {
                        valueFound = cfValue;
                    }
                }
            }
            return valueFound;
        }

        return null;
    }

    /**
     * Get custom field value
     *
     * @param entity the entity
     * @param cfCode the CF code
     * @param date the date
     * @return CF value.
     */
    private CustomFieldValue getCustomFieldValue(ICustomFieldEntity entity, String cfCode, Date date) {
        Map<String, List<CustomFieldValue>> valuesByCode = entity.getCfValuesNullSafe().getValuesByCode();
        CustomFieldValue cfValue = getCfValue(valuesByCode, cfCode, date);
        return cfValue;
    }

    /**
     * Get value
     *
     * @param entity the entity
     * @param cfCode the CF code
     * @param date the date
     * @return CF value.
     */
    public Object getValue(ICustomFieldEntity entity, String cfCode, Date date) {
        CustomFieldValue cfValue = getCustomFieldValue(entity, cfCode, date);
        if (cfValue != null) {
            return cfValue.getValue();
        }
        return null;
    }

    private Object getValueByClosestMatch(ICustomFieldEntity entity, String cfCode, Date date, String keyToMatch) {
        Object value = getValue(entity, cfCode, date);
        Object valueMatched = ICustomFieldEntity.matchClosestValue(value, keyToMatch);
        return valueMatched;
    }

    private String getAttributeValue(Map<String, String> cfValue, int position) {
        if (cfValue != null) {
            for (Map.Entry<String, String> entry : cfValue.entrySet()) {
                String value = entry.getValue();
                String[] attributes = value.split(Pattern.quote(CustomFieldValue.MATRIX_KEY_SEPARATOR));
                try {
                    return attributes[position];
                } catch (IndexOutOfBoundsException e) {
                }
            }
        }
        return null;
    }

    /**
     * Set set attribute value
     *
     * @param cfValue the CF value
     * @param value the value
     * @param position the position
     */
    private void setAttributeValue(Map<String, String> cfValue, String value, int position) {
        if (cfValue != null) {
            for (Map.Entry<String, String> entry : cfValue.entrySet()) {
                String[] attributes = entry.getValue().split(Pattern.quote(CustomFieldValue.MATRIX_KEY_SEPARATOR));
                try {
                    attributes[position] = value;
                    cfValue.put(entry.getKey(), String.join(CustomFieldValue.MATRIX_KEY_SEPARATOR, attributes));
                } catch (IndexOutOfBoundsException e) {
                }
            }
        }
    }

    /**
     * Get last CF version
     *
     * @param entity the entity
     * @param cfCode the cf code
     * @param effectDate the effect date
     * @param ignorePeriodsOverlap ignore periods overlap
     * @return last CF version
     */
    private CustomFieldValue getLastCfVersion(ICustomFieldEntity entity, String cfCode, Date effectDate, boolean ignorePeriodsOverlap) {

        CustomFieldValue lastCfValue = null;
        CustomFieldValue sameCfValue = null;

        if (entity != null) {
            if (entity.getCfValuesNullSafe() != null) {
                Map<String, List<CustomFieldValue>> valuesByCode = entity.getCfValuesNullSafe().getValuesByCode();
                if (valuesByCode != null && valuesByCode.get(cfCode) != null) {
                    List<CustomFieldValue> customFieldValueList = valuesByCode.get(cfCode);
                    for (CustomFieldValue customFieldValue : customFieldValueList) {
                        if (customFieldValue != null && customFieldValue.getPeriod() != null) {
                            if (customFieldValue.getPeriod().isCorrespondsToPeriod(effectDate, null, true)) {
                                sameCfValue = customFieldValue;
                                break;
                            }
                            if (customFieldValue.getPeriod() != null && customFieldValue.getPeriod().getTo() == null
                                    && (lastCfValue == null || lastCfValue.getPriority() < customFieldValue.getPriority())) {
                                lastCfValue = customFieldValue;
                            }
                        }
                    }
                }
            }

            if (sameCfValue != null) {
                if (ignorePeriodsOverlap) {
                    // sameCfValue.setValue(cfValue);
                    return sameCfValue;
                }
                // A matching period From-To was found. Please change the value of existing period instead
                String period = "";
                if (sameCfValue.getPeriod() != null) {
                    String from = DateUtils.formatDateWithPattern(sameCfValue.getPeriod().getFrom(), DATE_DEFAULT_PATTERN);
                    String to = DateUtils.formatDateWithPattern(sameCfValue.getPeriod().getTo(), DATE_DEFAULT_PATTERN);
                    if (!StringUtils.isBlank(from) && !StringUtils.isBlank(to)) {
                        period = from + "-" + to;
                    } else {
                        period = !StringUtils.isBlank(from) ? from : to;
                    }
                }
                String errorMessage = "A matching period " + period + " was found. Please change the value of existing period instead";
                setError(ErrorEnum.INVALID_DATA, errorMessage);
                return null;

            } else {
                return lastCfValue;
            }
        }
        return null;
    }

    /**
     * Add the new version of CF value
     *
     * @param entity the entity
     * @param effectDate the effect date
     * @param cfCode the CF code
     * @param endDate the end date
     * @param cfValue the CF value
     */
    private void addNewVersionCfValue(ICustomFieldEntity entity, Date effectDate, String cfCode, Date endDate, Map<String, String> cfValue) {
        DatePeriod datePeriod = new DatePeriod(effectDate, endDate);
        entity.getCfValuesNullSafe().setValue(cfCode, datePeriod, -1, cfValue);
    }

    /**
     * Update the CF value
     *
     * @param entity the entity
     * @param effectDate the effect date
     * @param cfCode the CF code
     * @param endDate the end date
     * @param cfValue the CF value
     * @param lastCfValue the last CF value
     */
    private void updateCfValue(ICustomFieldEntity entity, Date effectDate, String cfCode, Date endDate, Map<String, String> cfValue, CustomFieldValue lastCfValue) {

        if (cfValue != null) {
            if (lastCfValue != null) {
                // End Date should be set to the Effective Date/Time on the bill item minus one second
                lastCfValue.getPeriod().setTo(addSecondsToDate(effectDate, -1));
            }
            addNewVersionCfValue(entity, effectDate, cfCode, endDate, cfValue);
        }
    }

    /**
     * Get ProductSet with prefix code attached
     *
     * @param orderLineDTO Order Line
     * @return ProductSet with prefix code
     */
    private String getProductSetPrefixCode(OrderLineDTO orderLineDTO) {
        String productSetPrefixCode = null;

        if (orderLineDTO.catalog != null) {
            String productSet = (String) orderLineDTO.catalog.get(Catalog.PRODUCT_SET.name());
            if (!StringUtils.isBlank(productSet)) {
                productSetPrefixCode = productSet.replace("_", "") + "_";
            }
        }
        productSetPrefixCode = productSetPrefixCode + orderLineDTO.operatorId;
        return productSetPrefixCode;
    }

    /**
     * Get service type
     *
     * @param orderLineDTO orderLine
     * @return Service type
     */
    private String getServiceType(OrderLineDTO orderLineDTO) {
        if (orderLineDTO.offerTemplate == null) {
            return null;
        }

        // check if catalog table exists
        if (orderLineDTO.catalog == null) {
            return null;
        }

        return (String) orderLineDTO.catalog.get(Catalog.SERVICE_TYPE.name());
    }

    /**
     * Get Service_Id with prefix attached
     *
     * @param orderLineDTO Order Line DTO
     * @return Prefixed Service Id
     */
    private String getServiceIdWithPrefix(OrderLineDTO orderLineDTO) {
        try {
            if (orderLineDTO.catalog != null) {
                // get Prefix from catalog
                String subscriptionPrefix = (String) orderLineDTO.catalog.get(Catalog.SUBSCRIBER_PREFIX.name());
                if (StringUtils.isBlank(subscriptionPrefix)) {
                    return orderLineDTO.serviceId;
                } else {
                    return subscriptionPrefix.replace("_", "") + "_" + orderLineDTO.serviceId;
                }
            }
            return orderLineDTO.serviceId;
        } catch (Exception ex) {
            return null;
        }
    }

    private Map<String, String> getTariffs(OrderLineDTO orderLineDTO) {
        // check STANDARD_CUSTOMER CF
        Customer customer = getCustomerByOperatorId(orderLineDTO.operatorId);
        Boolean standardCustomer = (Boolean) customer.getCfValuesNullSafe().getValue("STANDARD_CUSTOMER");

        Map<String, String> tariffs = null;
        if (standardCustomer != null && standardCustomer == false) {
            // get Tariff from customer level
            tariffs = (Map<String, String>) customer.getCfValuesNullSafe().getValue(CustomerOmsCFsEnum.NEW_TARIFF.name());
        } else if (orderLineDTO.offerTemplate != null) {
            // Standard
            tariffs = (Map<String, String>) orderLineDTO.offerTemplate.getCfValuesNullSafe().getValue(OfferTemplateOmsCFsEnum.NEW_TARIFF.name());
        }
        return tariffs;
    }

    /**
     * get range numbers
     *
     * @param serviceInstanceCode the service instance code
     * @return map of range numbers
     */
    private Map<Integer, Integer> getRangeNumbers(String serviceInstanceCode) {
        Map<Integer, Integer> rangeNumbers = new HashMap<>();

        List<ServiceInstance> serviceInstances = serviceInstanceService.findByCodeLike(serviceInstanceCode);
        for (ServiceInstance serviceInstance : serviceInstances) {
            if (serviceInstance.getCfValuesNullSafe().getValue("CF_BILL_ATTRIBUTE") != null) {
                Map<String, String> cf = (Map<String, String>) serviceInstance.getCfValuesNullSafe().getValue("CF_BILL_ATTRIBUTE");
                if (cf.get("RANGE_START") != null || cf.get("RANGE_END") != null) {

                    Integer rangeStart = null;
                    Map<String, String> valueStart = (Map<String, String>) getCFValue(serviceInstance, "RANGE_START", cf.get("RANGE_START"));
                    if (valueStart != null && valueStart.get(BillAttributeNamesEnum.Value) != null) {
                        try {
                            rangeStart = Integer.valueOf(valueStart.get(BillAttributeNamesEnum.Value).replaceAll("-", ""));
                        } catch (Exception e) {
                        }
                    }

                    Integer rangeEnd = null;
                    Map<String, String> valueEnd = (Map<String, String>) getCFValue(serviceInstance, "RANGE_END", cf.get("RANGE_END"));
                    if (valueEnd != null && valueEnd.get(BillAttributeNamesEnum.Value) != null) {
                        try {
                            rangeEnd = Integer.valueOf(valueEnd.get(BillAttributeNamesEnum.Value).replaceAll("-", ""));
                        } catch (Exception e) {
                        }
                    }

                    rangeStart = (rangeStart != null) ? rangeStart : rangeEnd;
                    rangeEnd = (rangeEnd != null) ? rangeEnd : rangeStart;

                    if (rangeStart != null && rangeEnd != null) {
                        rangeNumbers.put(Integer.valueOf(rangeStart), Integer.valueOf(rangeEnd));
                    }
                }
            }
        }
        return rangeNumbers;
    }

    /**
     * Check if new ranges are valid against the existing ones
     *
     * @param existingRangeStart the existing range start
     * @param existingRangeEnd the existing range end
     * @param newRangeStart the new range start
     * @param newRangeEnd the new range end
     * @return true if new ranges are valid against the existing ones
     */
    private boolean isValidRanges(Integer existingRangeStart, Integer existingRangeEnd, Integer newRangeStart, Integer newRangeEnd) {
        return !((newRangeStart != null && newRangeStart >= existingRangeStart && newRangeStart <= existingRangeEnd)
                || (newRangeEnd != null && newRangeEnd >= existingRangeStart && newRangeEnd <= existingRangeEnd));
    }

    /**
     * Check if new ranges are valid against the existing ones
     *
     * @param rangeStart the range start
     * @param rangeEnd the range end
     * @return true if new ranges are valid against the existing ones
     */
    private boolean isValidRanges(String rangeStart, String rangeEnd) {
        if (!StringUtils.isBlank(rangeStart) || !StringUtils.isBlank(rangeEnd)) {

            Map<Integer, Integer> rangeNumbers = new HashMap<>();

            rangeNumbers.putAll(getRangeNumbers(AncillaryServiceEnum.MSN.name()));
            rangeNumbers.putAll(getRangeNumbers(AncillaryServiceEnum.DDI.name()));

            Integer newRangeStart = null;
            if (!StringUtils.isBlank(rangeStart)) {
                try {
                    newRangeStart = Integer.valueOf(rangeStart.replaceAll("-", ""));
                } catch (NumberFormatException nfe) {
                }
            }

            Integer newRangeEnd = null;
            if (!StringUtils.isBlank(rangeEnd)) {
                try {
                    newRangeEnd = Integer.valueOf(rangeEnd.replaceAll("-", ""));
                } catch (NumberFormatException nfe) {
                }
            }

            // validating range numbers
            for (Map.Entry<Integer, Integer> entry : rangeNumbers.entrySet()) {
                if (!isValidRanges(entry.getKey(), entry.getValue(), newRangeStart, newRangeEnd)) {
                    String usedRanges = "";
                    usedRanges += newRangeStart != null ? newRangeStart + " " : "";
                    usedRanges += newRangeEnd != null ? newRangeEnd + " " : "";
                    log.error(usedRanges + "already in use at ranges: " + entry.getKey() + "  " + entry.getValue());
                    setError(ErrorEnum.INVALID_DATA, "NEW_RANGE_START and NEW_RANGE_END already exist or interfers with existing one");
                    return false;
                }
            }

            // check if new range not already taken
            List<Subscription> subs = subscriptionService.list();
            for (Subscription s : subs) {

                Integer newSubscriptionCode = null;
                try {
                    newSubscriptionCode = Integer.valueOf(s.getCode().replaceAll("-", ""));
                } catch (NumberFormatException nfe) {
                }

                return (!(newRangeStart != null && newRangeStart == newSubscriptionCode
                        || (newRangeStart != null && newRangeEnd != null && newRangeEnd >= newSubscriptionCode && newRangeStart <= newSubscriptionCode)));

            }
        }
        return true;
    }

    /**
     * Create service instance
     *
     * @param orderLineDTO the order line DTO
     * @param serviceTemplate the service template
     * @param serviceType the service type
     * @return service instance
     */
    private ServiceInstance createServiceInstance(OrderLineDTO orderLineDTO, ServiceTemplate serviceTemplate, String serviceType) {

        ServiceInstance serviceInstance = new ServiceInstance();
        serviceInstance.setVersion(3);
        serviceInstance.setCode(orderLineDTO.billCode);
        serviceInstance.setOrderNumber(orderLineDTO.operatorOrderNumber);
        serviceInstance.setDescription((String) orderLineDTO.catalog.get(Catalog.DESCRIPTION.name()));

        BigDecimal quantity = new BigDecimal(orderLineDTO.billQuantity);
        if (ItemTypeEnum.CHARGE.getLabel().equalsIgnoreCase(orderLineDTO.itemType) && !StringUtils.isBlank(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.QUANTITY.name()))) {
            quantity = new BigDecimal(orderLineDTO.getAttribute(ChargesAttributeNamesEnum.QUANTITY.name()));
        }
        serviceInstance.setQuantity(quantity);

        serviceInstance.setServiceTemplate(serviceTemplate);
        serviceInstance.setSubscription(orderLineDTO.subscription);
        Date billEffectDate = DateUtils.truncateTime(orderLineDTO.billEffectDate);
        serviceInstance.setSubscriptionDate(billEffectDate);

        setServiceInstanceCf(orderLineDTO, serviceInstance, serviceType);

        Integer agreementDuration = getEngagementDuration(orderLineDTO);
        if (agreementDuration != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(billEffectDate);
            calendar.add(Calendar.MONTH, agreementDuration);
            serviceInstance.setEndAgreementDate(calendar.getTime());
        }

        return serviceInstance;
    }

    /**
     * Activate service
     *
     * @param orderLineDTO the order line DTO
     * @param serviceType the service type
     * @return the service instance
     */
    private ServiceInstance activateService(OrderLineDTO orderLineDTO, ServiceTemplate serviceTemplate, String serviceType) {
        ServiceInstance serviceInstance = createServiceInstance(orderLineDTO, serviceTemplate, serviceType);
        serviceInstanceService.create(serviceInstance);

        // activate service
        // serviceInstanceService.serviceInstanciation(serviceInstance);
        serviceInstanceService.serviceInstanciation(serviceInstance, (String) orderLineDTO.catalog.get(Catalog.DESCRIPTION.name()), null, null, false);
        orderLineDTO.subscription.getServiceInstances().add(serviceInstance);
        serviceInstanceService.serviceActivation(serviceInstance, null, null);

        // renaming charge instance with bill code name + suffix
        for (OneShotChargeInstance oci : serviceInstance.getSubscriptionChargeInstances()) {
            oci.setCode(orderLineDTO.billCode + "_NRC");
        }
        for (RecurringChargeInstance rci : serviceInstance.getRecurringChargeInstances()) {
            rci.setCode(orderLineDTO.billCode + "_RC");
        }
        return serviceInstance;
    }

    /**
     * ################################################### START : inherited code from NewPlaceOrderScript ###################################################
     */

    private void setServiceInstanceCf(OrderLineDTO orderLineDTO, ServiceInstance serviceInstance, String serviceType) {
        serviceInstance.getCfValuesNullSafe().setValue("ITEM_ID", orderLineDTO.itemId);
        serviceInstance.getCfValuesNullSafe().setValue("ITEM_COMPONENT_ID", orderLineDTO.itemComponentId);
        serviceInstance.getCfValuesNullSafe().setValue("CF_BILL_CODE", orderLineDTO.billCode);
        serviceInstance.getCfValuesNullSafe().setValue("CF_BILL_DESCRIPTION", (String) orderLineDTO.catalog.get(Catalog.DESCRIPTION.name()));
        serviceInstance.getCfValuesNullSafe().setValue("ACTION_QUALIFIER", orderLineDTO.billActionQualifier);
        serviceInstance.getCfValuesNullSafe().setValue("SERVICE_TYPE", serviceType);
        serviceInstance.getCfValuesNullSafe().setValue("SLA", orderLineDTO.getAttribute("SLA"));
        serviceInstance.getCfValuesNullSafe().setValue("SLA_HISTORY", new DatePeriod(orderLineDTO.billEffectDate, null), 1, orderLineDTO.getAttribute("SLA"));
        // serviceInstance.getCfValuesNullSafe().setValue("CYCLE_FORWARD", (String) orderLineDTO.catalog.get(Catalog.IN_ADVANCE.name()));
        serviceInstance.getCfValuesNullSafe().setValue("CYCLE_FORWARD", "true");
        serviceInstance.getCfValuesNullSafe().setValue("PERIODICITY", orderLineDTO.catalog.get(Catalog.CALENDAR.name()));
        serviceInstance.getCfValuesNullSafe().setValue("SPECIAL_CONNECT", orderLineDTO.specialConnect);
        serviceInstance.getCfValuesNullSafe().setValue("SPECIAL_RENTAL", orderLineDTO.specialRental);
        serviceInstance.getCfValuesNullSafe().setValue("ORDERING_SYSTEM", orderDTO.orderingSystem);
        String exchangeClass = getExchangeClass(orderLineDTO);
        if (!StringUtils.isBlank(orderLineDTO.getAttribute("EXCHANGE"))) {
            serviceInstance.getCfValuesNullSafe().setValue("EXCHANGE_CLASS", exchangeClass);
        }

        // 473 : If a subsequent CH order bill item (Charge item type) is received with an AMT,
        // this new AMT will replace/override the old/existing AMT (from the previous bill item) for the recurring charge

        // 473 : If a subsequent CH order bill item (Charge item type) is received and an AMT is not present,
        // a revised charge will be calculated according to the Quantity (if present) and the Tariff Plan (as outlined in PO-474 and PO2-494)
        // for the recurring charge

        // 473 : One-Shot Charges: A CH order bill item will never be submitted to change the AMT of an existing/active subscriber service one-shot charge
        // orderLineDTO.billAction.equalsIgnoreCase(BillActionEnum.CH.name())

        // updateTariffOrQuantity(orderLineDTO, serviceInstance);

        // filling Bill attributes in service instance level
        Map<String, String> attributes = new HashMap<>();
        for (AttributeDTO attribute : orderLineDTO.attributes) {
            attributes.put(attribute.code,
                attribute.value + CustomFieldValue.MATRIX_KEY_SEPARATOR + attribute.unit + CustomFieldValue.MATRIX_KEY_SEPARATOR + orderLineDTO.billEffectDate);
        }
        if (!attributes.isEmpty()) {
            serviceInstance.getCfValuesNullSafe().setValue("CF_BILL_ATTRIBUTE", attributes);
        }
    }

    /**
     * Get offer code
     *
     * @param billCode the bill code.
     * @return offer code
     */
    private String getOfferCode(String billCode) {
        String offerCode = null;
        if (!StringUtils.isBlank(billCode)) {
            String query = "SELECT code FROM cat_offer_template where cf_values like '%" + billCode + "%' and code not in ('" + MISC_OFFER_TEMPLATE_CODE + "','"
                    + EIR_OFFER_TEMPLATE_CODE + "','" + NGA_FTH_OFFER_TEMPLATE_CODE + "')";
            List<String> offerCodes = offerTemplateService.getEntityManager().createNativeQuery(query).getResultList();
            if (offerCodes != null && !offerCodes.isEmpty()) {
                offerCode = offerCodes.get(0);
            }
        }
        return offerCode;
    }

    /**
     * Get the exchange class
     *
     * @param orderLineDTO Order line DTO
     * @return the exchange class
     */
    private String getExchangeClass(OrderLineDTO orderLineDTO) {
        String exchangeClass = null;
        String exchangeCode = orderLineDTO.getAttribute("EXCHANGE");
        if (!StringUtils.isBlank(exchangeCode)) {
            Map<String, String> exchangeDetails = (Map<String, String>) customFieldInstanceService.getCFValueByClosestMatch(providerService.getProvider(), "EXCHANGE_DETAILS",
                orderLineDTO.billEffectDate, exchangeCode);
            if (exchangeDetails != null) {
                exchangeClass = exchangeDetails.get("Exchange_Class");
            }
        }
        return exchangeClass;
    }

    private boolean isSameAttributeValue(String itemOrderAttributeValue, String cfAttributeValue) {
        boolean isSameValue = false;
        if ((StringUtils.isBlank(itemOrderAttributeValue) && (StringUtils.isBlank(cfAttributeValue) || "null".equalsIgnoreCase(cfAttributeValue)))
                || (!StringUtils.isBlank(itemOrderAttributeValue) && !StringUtils.isBlank(cfAttributeValue) && itemOrderAttributeValue.equalsIgnoreCase(cfAttributeValue))
                || ("*".equalsIgnoreCase(itemOrderAttributeValue) && !StringUtils.isBlank(cfAttributeValue)
                        && (cfAttributeValue.equalsIgnoreCase("*") || cfAttributeValue.equalsIgnoreCase("***")))) {
            isSameValue = true;
        }
        return isSameValue;
    }

    private BigDecimal getTariff(OrderLineDTO orderLineDTO, String chargeType, String exchangeClass) {

        Date subscriptionDate = orderLineDTO.billEffectDate;
        String billCode = orderLineDTO.billCode;
        String actionQualifier = orderLineDTO.billActionQualifier;
        String density = orderLineDTO.getAttribute(TariffAttributeNamesEnum.DENSITY.name());
        String region = orderLineDTO.getAttribute(TariffAttributeNamesEnum.REGION.name());
        String sla = orderLineDTO.getAttribute(TariffAttributeNamesEnum.SLA.name());
        String classService = orderLineDTO.getAttribute(TariffAttributeNamesEnum.CLASS_SERVICE.name());
        String ef = orderLineDTO.getAttribute(TariffAttributeNamesEnum.EF.name());
        String af = orderLineDTO.getAttribute(TariffAttributeNamesEnum.AF.name());

        try {
            Map<String, String> tariffs = getTariffs(orderLineDTO);
            String tariffKey = null;
            Date nearDate = new GregorianCalendar(1800, 01, 07).getTime();
            for (Map.Entry<String, String> entry : tariffs.entrySet()) {

                try {
                    boolean isTariffFound = false;
                    String[] attributes = entry.getKey().split(Pattern.quote(CustomFieldValue.MATRIX_KEY_SEPARATOR));
                    isTariffFound = isSameAttributeValue(billCode, attributes[0]) && isSameAttributeValue(chargeType, attributes[1])
                            && isSameAttributeValue(actionQualifier, attributes[2]) && isSameAttributeValue(density, attributes[3]) && isSameAttributeValue(region, attributes[4])
                            && isSameAttributeValue(sla, attributes[5]) && isSameAttributeValue(classService, attributes[6]) && isSameAttributeValue(ef, attributes[7])
                            && isSameAttributeValue(af, attributes[8]) && isSameAttributeValue(exchangeClass, attributes[9]);

                    if (isTariffFound) {
                        Date startDate = DateUtils.parseDateWithPattern(attributes[10], DATE_DEFAULT_PATTERN);
                        if (startDate != null) {
                            if (startDate.equals(subscriptionDate)) {
                                nearDate = startDate;
                                tariffKey = entry.getKey();
                                break;
                            } else if (startDate.before(subscriptionDate) && startDate.after(nearDate)) {
                                nearDate = startDate;
                                tariffKey = entry.getKey();
                            }
                        }
                    }

                } catch (IndexOutOfBoundsException e) {
                }

            }
            if (tariffKey != null) {
                return new BigDecimal(tariffs.get(tariffKey).split(Pattern.quote(CustomFieldValue.MATRIX_KEY_SEPARATOR))[2]);
            }
            return null;
        } catch (Exception ex) {
            log.info("Tariff not found for " + billCode + " with charge type " + chargeType + " in date " + orderLineDTO.billEffectDate);
            return null;
        }

    }

    /**
     * Get product migration rules
     *
     * @param from Migration Code Source
     * @param to Migration Code Target
     * @return Migration Code
     */
    private String getProductMigrationRules(String from, String to, Map<String, String> migrationMatrix) {
        log.info("FROM : " + from + " - TO : " + to);
        int score = 0;
        List<String> bestMatches = new ArrayList<String>();
        for (Map.Entry<String, String> entry : migrationMatrix.entrySet()) {
            String fromtoConditionRules = entry.getKey(); // 10_10_10|10_10_10|=0

            String condition = "";
            String[] fromtoConditionTab = fromtoConditionRules.split(Pattern.quote("|"));
            if (fromtoConditionTab.length < 2) {
                continue;
            } else if (fromtoConditionTab.length > 2) {
                condition = fromtoConditionTab[2];
            }

            String fRules = fromtoConditionTab[0];
            String tRules = fromtoConditionTab[1];

            String fromRules = fRules.trim();
            String toRules = tRules.trim();

            if (fromRules.trim().equals("*")) {
                fromRules = from.charAt(0) + fromRules;
            }
            if (toRules.trim().equals("*")) {
                toRules = to.charAt(0) + toRules;
            }

            fromRules = (fromRules.contains("**")) ? fromRules + " " : fromRules;
            toRules = (toRules.contains("**")) ? toRules + " " : toRules;

            String fromToCompare = fromRules.split("\\*")[0]; // 10_01_
            String toCompare = toRules.split("\\*")[0]; // 10_02_

            if (from.equals(fromToCompare) || from.startsWith(fromToCompare)) {
                if (to.equals(toCompare) || to.startsWith(toCompare)) {
                    if (score == (fromToCompare.length() + toCompare.length())) {
                        bestMatches.add(fRules + "|" + tRules + "|" + condition);
                    } else if (score < (fromToCompare.length() + toCompare.length())) {
                        score = fromToCompare.length() + toCompare.length();
                        bestMatches.clear();
                        bestMatches.add(fRules + "|" + tRules + "|" + condition);
                    }
                }
            }
        }
        log.info("bestMatches" + bestMatches);
        if (bestMatches.size() == 1) {
            return bestMatches.get(0);
        } else if (bestMatches.size() > 1) {

            int fromRR = 0;
            String[] fromTab = from.split(Pattern.quote("_"));
            if (fromTab.length >= 2) {
                try {
                    fromRR = Integer.parseInt(fromTab[2].trim());
                } catch (NumberFormatException ex) {
                }
            }

            int toRR = 0;
            String[] toTab = to.split(Pattern.quote("_"));
            if (toTab.length >= 2) {
                try {
                    toRR = Integer.parseInt(toTab[2].trim());
                } catch (NumberFormatException ex) {
                }
            }
            int condition = fromRR - toRR;
            for (String match : bestMatches) {
                String[] matchTab = match.split(Pattern.quote("|"));
                if (toTab.length < 2) {
                    continue;
                }
                String conditionRule = matchTab[2].trim(); // =0 <1 >1
                if (condition >= 1 && (conditionRule.contains(">1") || conditionRule.contains("RR-RR>1"))) {
                    return match;
                } else if (condition == 0 && (conditionRule.contains("=0") || conditionRule.contains("RR-RR=0"))) {
                    return match;
                } else if (condition < 1 && (conditionRule.contains("<1") || conditionRule.contains("RR-RR<1"))) {
                    return match;
                }
            }
        }
        return null;
    }

    /**
     * ################################################### END : inherited code from NewPlaceOrderScript ###################################################
     */

    /**
     * Order XML Schema
     */
    public static final String XML_SCHEMA = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n"
            + "<xs:schema xmlns:xs=\"http://www.w3.org/2001/XMLSchema\" elementFormDefault=\"qualified\" attributeFormDefault=\"unqualified\">\n"
            + "   <xs:element name=\"Wholesale_Billing\">\n" + "      <xs:complexType>\n" + "         <xs:sequence>\n"
            + "            <xs:element name=\"Transaction_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "            <xs:element name=\"Ordering_System\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "            <xs:element name=\"Order_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "            <xs:element name=\"Ref_Order_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n" + "            <xs:element name=\"Bill_Item\">\n"
            + "               <xs:complexType>\n" + "                  <xs:sequence>\n"
            + "                     <xs:element name=\"Item_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Operator_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Operator_Order_Number\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Item_Component_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Item_Version\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Item_Type\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Agent_Code\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"UAN\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Master_Account_Number\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Service_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Ref_Service_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Xref_Service_id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Bill_Code\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Action\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Action_Qualifier\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Quantity\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Effect_Date\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Special_Connect\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Special_Rental\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                     <xs:element name=\"Attribute\" maxOccurs=\"unbounded\">\n" + "                        <xs:complexType>\n"
            + "                           <xs:sequence>\n" + "                              <xs:element name=\"Code\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                              <xs:element name=\"Value\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                              <xs:element name=\"Unit\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                              <xs:element name=\"Component_Id\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                              <xs:element name=\"Attribute_Type\" type=\"xs:string\" minOccurs=\"0\" maxOccurs=\"1\" />\n"
            + "                           </xs:sequence>\n" + "                        </xs:complexType>\n" + "                     </xs:element>\n"
            + "                  </xs:sequence>\n" + "               </xs:complexType>\n" + "            </xs:element>\n" + "         </xs:sequence>\n"
            + "      </xs:complexType>\n" + "   </xs:element>\n" + "</xs:schema>";

    /**
     * Find pending rejected waiting orders.
     *
     * @param orderId the order id
     * @param serviceId the service id
     * @return the list
     */
    @SuppressWarnings("unchecked")
    public List<Order> findPendingRejectedWaitingOrders(String orderId, String serviceId) {
        String query = "SELECT code FROM ord_order where code <> '" + orderId + "' AND status in ('PENDING','REJECTED','WAITING')" + " AND cf_values like '%<Service_Id>"
                + serviceId + "</Service_Id>%'";
        return orderService.getEntityManager().createNativeQuery(query).getResultList();
    }

    /**
     * Checks if is held order.
     *
     * @return true, if is held order
     */
    private boolean isHeldOrder() {
        List<Order> orders;
        for (OrderLineDTO orderLineDTO : orderDTO.orderLines) {
            orders = findPendingRejectedWaitingOrders(orderDTO.orderId, orderLineDTO.serviceId);
            if (orders != null && !orders.isEmpty()) {
                orderDTO.status = OrderStatusEnum.HELD;
                orderDTO.errorMessage = "The service Id " + orderLineDTO.deducedServiceId + "is already exists against an order that is in Error Management : " + orders.get(0);
                return true;
            }
        }
        return false;
    }

    /**
     * Checks if is deferred order.
     *
     * @return true, if is deferred order
     */
    private boolean isDeferredOrder() {
        if (!StringUtils.isBlank(orderDTO.refOrderId)) {
            Order refOrder = orderService.findByCode(orderDTO.refOrderId);

            if (refOrder != null) {
                refOrderDTO = getRefOrderDTO(refOrder);
            }

            if (refOrder == null || (!OrderStatusEnum.COMPLETED.equals(refOrder.getStatus()) && !OrderStatusEnum.CANCELLED.equals(refOrder.getStatus()))) {
                for (OrderLineDTO orderLineDTO : orderDTO.orderLines) {
                    // if the action is PR and refOrderId is filled and order ( by refIdOreder) not found or has a status other than completed
                    if (BillActionEnum.PR.name().equals(orderLineDTO.billAction)) {
                        orderDTO.status = OrderStatusEnum.DEFERRED;
                        orderDTO.errorMessage = "The order is set to the status Deferred until the accompanying order " + orderDTO.refOrderId
                                + " is received and successfully processed";
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Checks if is waiting order.
     *
     * @return true, if is waiting order
     */
    private boolean isWaitingOrder() {
        if (StringUtils.isBlank(orderDTO.refOrderId)) {
            List<CustomEntityInstance> cetList = customEntityInstanceService.listByCet(CET_CIRCUIT_TYPE_MAPPING);
            CustomEntityInstance matchedCrtType;
            for (OrderLineDTO orderLineDTO : orderDTO.orderLines) {
                // if the action is PR and refOrderId is empty and serviceID matches a circuit_type_mapping
                if (BillActionEnum.PR.name().equals(orderLineDTO.billAction)) {
                    matchedCrtType = cetList.stream().filter(cet -> orderLineDTO.deducedServiceId.contains(cet.getCode())).findAny().orElse(null);
                    if (matchedCrtType != null) {
                        orderDTO.status = OrderStatusEnum.WAITING;
                        orderDTO.errorMessage = "Circuit id in the Service_Id tag is matched against an entry configured in the Billing Trigger Circuit Type Mapping table : "
                                + matchedCrtType.getCode();
                        return true;
                    }
                }
            }
        }
        return false;
    }

    /**
     * Gets the customer by operator id.
     *
     * @param operatorId the operator id
     * @return the customer by operator id
     */
    private Customer getCustomerByOperatorId(String operatorId) {
        List<CustomerAccount> customersAccount = customerAccountService.findByCodeLike(operatorId);
        if (customersAccount == null || customersAccount.isEmpty()) {
            return null;
        }
        return customersAccount.get(0).getCustomer(); // all CustomerAccount of operatorId have the same Customer
    }

}

/**
 * Formatted Order Object (Element tag names use InitCaps with underscores)
 *
 * @author Abdellatif BARI
 */
@XmlRootElement(name = "Wholesale_Billing")
@XmlAccessorType(XmlAccessType.FIELD)
class OrderDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -2510086889557547061L;

    /**
     * The ordering system.
     */
    @XmlElement(name = "Ordering_System", required = true)
    String orderingSystem;

    /**
     * The order id.
     */
    @XmlElement(name = "Order_Id", required = true)
    String orderId;

    /**
     * The ref order id.
     */
    @XmlElement(name = "Ref_Order_Id", required = true)
    String refOrderId;

    /**
     * The transaction id.
     */
    @XmlElement(name = "Transaction_Id", required = true)
    String transactionId;

    /**
     * The status message.
     */
    @XmlElement(name = "Status_Message")
    String statusMessage;

    /**
     * The bill items.
     */
    @XmlElement(name = "Bill_Item")
    List<OrderLineDTO> orderLines = new ArrayList<>();

    @XmlTransient
    OrderStatusEnum status;
    @XmlTransient
    PlaceOMSOrderScript.ErrorEnum errorCode;
    @XmlTransient
    String errorMessage;

    // order details text included in CF : ORDER_DETAILS
    Map<String, String> getOrderDetails() {
        Map<String, String> orderDetails = new HashMap<>();
        orderDetails.put("" + orderId, orderingSystem + CustomFieldValue.MATRIX_KEY_SEPARATOR + refOrderId + CustomFieldValue.MATRIX_KEY_SEPARATOR + transactionId);
        return orderDetails;
    }
}

/**
 * Formatted Order Line Object
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
class OrderLineDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 5057667346724418958L;

    /**
     * The item id.
     */
    @XmlElement(name = "Item_Id", required = true)
    String itemId;

    /**
     * The operator id.
     */
    @XmlElement(name = "Operator_Id", required = true)
    String operatorId;

    /**
     * The operator id.
     */
    @XmlElement(name = "Item_Component_Id", required = true)
    String itemComponentId;

    /**
     * The item version.
     */
    @XmlElement(name = "Item_Version")
    String itemVersion;

    /**
     * The item type.
     */
    @XmlElement(name = "Item_Type")
    String itemType;

    /**
     * The agent code.
     */
    @XmlElement(name = "Agent_Code")
    String agentCode;

    /**
     * The uan.
     */
    @XmlElement(name = "UAN")
    String uan;

    /**
     * The master account number.
     */
    @XmlElement(name = "Master_Account_Number")
    String masterAccountNumber;

    /**
     * The service id.
     */
    @XmlElement(name = "Service_Id")
    String serviceId;

    /**
     * The ref service id.
     */
    @XmlElement(name = "Ref_Service_Id")
    String refServiceId;

    /**
     * The xref service id.
     */
    @XmlElement(name = "Xref_Service_Id")
    String xRefServiceId;

    /**
     * The bill code.
     */
    @XmlElement(name = "Bill_Code", required = true)
    String billCode;

    /**
     * The action.
     */
    @XmlElement(name = "Action", required = true)
    String billAction;

    /**
     * The action qualifier.
     */
    @XmlElement(name = "Action_Qualifier")
    String billActionQualifier;

    /**
     * The quantity.
     */
    @XmlElement(name = "Quantity", required = true)
    String billQuantity;

    /**
     * The effect date.
     */
    @XmlElement(name = "Effect_Date", required = true)
    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    Date billEffectDate;

    /**
     * The special connect.
     */
    @XmlElement(name = "Special_Connect")
    String specialConnect;

    /**
     * The special rental.
     */
    @XmlElement(name = "Special_Rental")
    String specialRental;

    /**
     * The operator order number.
     */
    @XmlElement(name = "Operator_Order_Number")
    String operatorOrderNumber;

    /**
     * The attributes list.
     */
    @XmlElement(name = "Attribute")
    List<AttributeDTO> attributes = new ArrayList<>();

    @XmlTransient
    String deducedServiceId;
    @XmlTransient
    String billType;
    @XmlTransient
    OfferTemplate offerTemplate;
    @XmlTransient
    BillingAccount billingAccount;
    @XmlTransient
    Subscription subscription;
    @XmlTransient
    Map<String, Object> catalog;
    @XmlTransient
    Map<String, AttributeDTO> attributesMap;
    @XmlTransient
    String chargeType;
    @XmlTransient
    Date ceaseEffectDate;
    @XmlTransient
    Date rentalLiabilityDate;

    String getDetails() {
        StringBuilder sb = new StringBuilder();
        String offerCode = offerTemplate != null ? offerTemplate.getCode() : "";
        sb.append(operatorId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(itemComponentId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(itemVersion)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(itemType).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(agentCode)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(uan).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(masterAccountNumber)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(serviceId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(refServiceId)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(xRefServiceId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billCode)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billAction).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billActionQualifier)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billQuantity).append(CustomFieldValue.MATRIX_KEY_SEPARATOR)
            .append(DateUtils.formatDateWithPattern(billEffectDate, PlaceOMSOrderScript.DATE_TIME_PATTERN)).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billType)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(offerCode).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(operatorOrderNumber)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        return sb.toString();
    }

    /**
     * Get the order line attribute by code
     *
     * @param code the attribute code
     * @return the order line attribute by code
     */
    private AttributeDTO getAttributeDTO(String code) {
        if (attributesMap == null) {
            attributesMap = new HashMap<>();
            for (AttributeDTO attributeDTO : attributes) {
                if (attributeDTO != null && !StringUtils.isBlank(attributeDTO.code)) {
                    attributesMap.put(attributeDTO.code, attributeDTO);
                }
            }
        }
        return attributesMap.get(code);
    }

    /**
     * Get the order line attribute by code
     *
     * @param code the attribute code
     * @return the order line attribute by code
     */
    public String getAttribute(String code) {
        AttributeDTO attributeDto = getAttributeDTO(code);
        if (attributeDto != null) {
            return attributeDto.value;
        }
        return null;
    }

    /**
     * Get the order line unit attribute by code
     *
     * @param code the attribute code
     * @return the order line unit attribute by code
     */
    public String getUnitAttribute(String code) {
        AttributeDTO attributeDto = getAttributeDTO(code);
        if (attributeDto != null) {
            return attributeDto.unit;
        }
        return null;
    }

    /**
     * Set the order line attribute by code
     *
     * @param code the attribute code
     * @param value the attribute value
     */
    public void setAttribute(String code, String value) {
        if (attributesMap == null) {
            attributesMap = new HashMap<>();
        }
        AttributeDTO attributeDTO = attributesMap.get(code);
        if (attributeDTO == null) {
            attributeDTO = new AttributeDTO();
        }
        attributeDTO.value = value;
        attributesMap.put(code, attributeDTO);
    }
}

/**
 * Formatted Order Line Attributes Object
 *
 * @author abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
class AttributeDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1735352520076029445L;

    /**
     * The code.
     */
    @XmlElement(name = "Code", required = true)
    String code;

    /**
     * The value.
     */
    @XmlElement(name = "Value", required = true)
    String value;

    /**
     * The unit.
     */
    @XmlElement(name = "Unit")
    String unit;

    /**
     * The component id.
     */
    @XmlElement(name = "Component_Id")
    String componentId;

    /**
     * The attribute type.
     */
    @XmlElement(name = "Attribute_Type")
    String attributeType;

    String getDetails() {
        StringBuilder sb = new StringBuilder();
        sb.append(value).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(unit).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(componentId)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(attributeType).append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        return sb.toString();
    }
}

/**
 * Formatted Address Info Response Object
 *
 * @author Abdellatif BARI
 */
@XmlRootElement(name = "Address_Info_Response")
@XmlAccessorType(XmlAccessType.FIELD)
class AddressInfoResponseDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 872775769637725503L;

    /**
     * The address infos.
     */
    @XmlElement(name = "Address_Info")
    List<AddressInfoDTO> addressInfos = new ArrayList<>();
}

/**
 * Formatted Address Info Object
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
class AddressInfoDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 3389427307619533566L;

    /**
     * The address.
     */
    @XmlElement(name = "Address")
    AddressDTO address;
}

/**
 * Formatted Address Object
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
class AddressDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 8458141691439476869L;

    /**
     * The unit.
     */
    @XmlElement(name = "Unit")
    UnitDTO unit;

    /**
     * The building.
     */
    @XmlElement(name = "Building")
    BuildingDTO building;

    /**
     * The street.
     */
    @XmlElement(name = "Street")
    StreetDTO street;

    /**
     * The county.
     */
    @XmlElement(name = "County")
    CountyDTO county;
}

/**
 * Formatted Unit Object
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
class UnitDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 6401698713477030092L;

    /**
     * The address unit id.
     */
    @XmlElement(name = "Address_Unit_Id")
    String addressUnitId;

    /**
     * The unit number.
     */
    @XmlElement(name = "Unit_Number")
    String unitNumber;

    /**
     * The unit name.
     */
    @XmlElement(name = "Unit_Name")
    String unitName;

    /**
     * The unit type.
     */
    @XmlElement(name = "Unit_Type")
    String unitType;

    /**
     * The floor number.
     */
    @XmlElement(name = "Floor_Number")
    String floorNumber;

    /**
     * The eircode.
     */
    @XmlElement(name = "Eircode")
    String eircode;
}

/**
 * Formatted Building Object
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
class BuildingDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 7749431909837534109L;

    /**
     * The building number.
     */
    @XmlElement(name = "Building_Number")
    String buildingNumber;

    /**
     * The from building number.
     */
    @XmlElement(name = "From_Building_Number")
    String fromBuildingNumber;

    /**
     * The to building number.
     */
    @XmlElement(name = "To_Building_Number")
    String toBuildingNumber;

    /**
     * The building name.
     */
    @XmlElement(name = "Building_Name")
    String buildingName;
}

/**
 * Formatted Street Object
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
class StreetDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -3384661112714749034L;

    /**
     * The street id.
     */
    @XmlElement(name = "Street_Id")
    String streetId;

    /**
     * The street name.
     */
    @XmlElement(name = "Street_Name")
    String streetName;

    /**
     * The street suffix.
     */
    @XmlElement(name = "Street_Suffix")
    String streetSuffix;

    /**
     * The directional qualifier name.
     */
    @XmlElement(name = "Directional_Qualifier_Name")
    String directionalQualifierName;

    /**
     * The geographical qualifier name.
     */
    @XmlElement(name = "Geographical_Qualifier_Name")
    String geographicalQualifierName;

    /**
     * The townland name.
     */
    @XmlElement(name = "Townland_Name")
    String townlandName;
}

/**
 * Formatted County Object
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
class CountyDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -3323043462971078120L;

    /**
     * The county id.
     */
    @XmlElement(name = "County_Id")
    String countyId;

    /**
     * The county name.
     */
    @XmlElement(name = "County_Name")
    String countyName;

    /**
     * The town name.
     */
    @XmlElement(name = "Town_Name")
    String townName;

    /**
     * The postal district.
     */
    @XmlElement(name = "Postal_District")
    String postalDistrict;
}

class RefOrderDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 3527149084229998498L;

    /**
     * The ref order.
     */
    Order refOrder;

    /**
     * Subscription main details item.
     */
    Map<String, String> mainItem;

    /**
     * Get subscription main details attribute ref order
     *
     * @return subscription main details attribute ref order
     */
    public Date getMainItemElement(String elementName) {
        if (refOrder != null && mainItem != null && !mainItem.isEmpty()) {
            String effectDate = mainItem.get(elementName);
            if (!StringUtils.isBlank(effectDate)) {
                return DateUtils.parseDateWithPattern(mainItem.get(elementName), PlaceOMSOrderScript.DATE_TIME_PATTERN);
            }
        }
        return null;
    }

}

/**
 * This is Adaptor class which has main responsibility to convert from java.util.Date to format string of date.
 *
 * @author Abdellatif BARI
 */
class DateTimeAdapter extends XmlAdapter<String, Date> {

    @Override
    public Date unmarshal(String xml) {
        return DateUtils.parseDateWithPattern(xml, PlaceOMSOrderScript.DATE_TIME_PATTERN);
    }

    @Override
    public String marshal(Date object) {
        return DateUtils.formatDateWithPattern(object, PlaceOMSOrderScript.DATE_TIME_PATTERN);
    }
}
