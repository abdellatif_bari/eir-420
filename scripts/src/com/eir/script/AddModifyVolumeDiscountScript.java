package com.eir.script;

import java.util.Date;
import java.util.Map;

import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.CustomTableEvent;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.payments.impl.CustomerAccountService;
import org.meveo.service.script.Script;

/**
 * The Class AddModifyVolumeDiscountScript.
 *
 * @author hznibar
 */
public class AddModifyVolumeDiscountScript extends Script {
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	    
    private CustomerAccountService customerAccountService = (CustomerAccountService) getServiceInterface(CustomerAccountService.class.getSimpleName());
    private BillingAccountService billingAccountService = (BillingAccountService) getServiceInterface(BillingAccountService.class.getSimpleName());
	
	

	@Override
	public void execute(Map<String, Object> initContext) throws BusinessException {
		log.info("#####################Starting of script AddModifyVolumeDiscountScript");
		try {
			CustomTableEvent ce = (CustomTableEvent) initContext.get("event");            
            Map<String, Object> values = ce.getValues();
            //Customer Account
            CustomerAccount ca = customerAccountService.findById((Long) values.get("customer_account"));
            if (ca == null) {
                throw new BusinessException("No CustomerAccount found with ID = " + values.get("customer_account"));
            }
            
            // BillIng Account
            if(!StringUtils.isBlank(values.get("billing_account"))) {
                BillingAccount ba = billingAccountService.findById((Long) values.get("billing_account"));
                if (ba == null || !ba.getCustomerAccount().getCode().equals(ca.getCode())) {
                    throw new BusinessException("Incompatible billing account! ID = " + values.get("billing_account"));
                }
            }
            
            //Discount’s start date cannot be earlier than system date.
            if (values.get("valid_from") == null || ((Date) values.get("valid_from")).compareTo(new Date()) <= 0) {
                throw new BusinessException("Discount’s start date cannot be null or earlier than system date");
            }
            
            //End date cannot be earlier than system date or discount’s start date.
            if (values.get("valid_to") != null) {
                Date endDate = (Date) values.get("valid_to");
                if (endDate.compareTo(new Date()) <= 0 || endDate.compareTo((Date) values.get("valid_from")) <= 0) {
                    throw new BusinessException("end date should be null or in the future");
                }
            }
            
            //Charge Type 
            if(!"R".equals(values.get("charge_type")) && !"O".equals(values.get("charge_type")) && !"*".equals(values.get("charge_type"))) {
                throw new BusinessException("Charge Type should be 'O', 'R' or '*'");
            }
            
            //Discount Type
            if(!"TR".equals(values.get("discount_type")) && !"TH".equals(values.get("discount_type"))) {
                throw new BusinessException("Discount Type should be 'TR' or 'TH'");
            }
            
            /*
             * "code": "C1",
            "description": "Profile Code",
            "code": "C2",
            "description": "Customer Account",
            "code": "C3",
            "description": "Billing Account",
            "code": "C4",
            "description": "Charge Type",
            "code": "C5",
            "description": "Migration Code",
            "code": "C6",
            "description": "Start Date",
            "code": "C7",
            "description": "End Date",
            "code": "C8",
            "description": "Discount Type",
            "code": "C9",
            "description": "Discount %",
            "code": "C10",
            "description": "Description",
            "code": "C11",
            "description": "Band",
            "code": "C12",
            "description": "Accounting/GL code",
            "code": "C13",
            "description": "Invoice subcategory",
            "code": "C14",
            "description": "Aggregate By",
             */
			
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());

		}
	}
}