package com.eir.script;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.EjbUtils;
import org.meveo.model.notification.InboundRequest;
import org.meveo.model.order.Order;
import org.meveo.model.order.OrderStatusEnum;
import org.meveo.service.order.OrderService;
import org.meveo.service.script.Script;
import org.meveo.service.script.ScriptInstanceService;
import org.meveo.service.script.ScriptInterface;

public class ReprocessOrderScript extends Script {
    
    private static final long serialVersionUID = 1L;
    
    //Variables
    private static String errorCode;
    private static String message;
    private InboundRequest inboundRequest = null;
    
    //error codes
    private static final String ORDER_NOT_EXIST = "ORDER_NOT_EXIST";
    private static final String BAD_ORDER_STATUS = "BAD_ORDER_STATUS";
    
    //API Services
    private OrderService orderService = (OrderService) getServiceInterface(OrderService.class.getSimpleName());
    private ScriptInstanceService scriptInstanceService = (ScriptInstanceService) EjbUtils.getServiceInterface(ScriptInstanceService.class.getSimpleName());

    public static final String REPROCESS_ORDER = "REPROCESS_ORDER";

	private static final String RELEASE_ORDER = "RELEASE_ORDER";

    /**
     * Entry Point for script
     */
    @Override
    public void execute(Map<String, Object> initContext) throws BusinessException {

        log.info("******Starting of script ReprocessOrderScript******");
        
        inboundRequest = (InboundRequest) initContext.get("event");
        if(inboundRequest == null) {
            inboundRequest = new InboundRequest();
        }
        String orderMessage = (String) initContext.get("body");
        
        
        try {
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(orderMessage);      
            String orderId = (String) json.get("Order_Id");
            String status = (String) json.get("Status");
            String cancelReasonCode = (String) json.get("Cancel_Reason_Code");
            String cancelDescription = (String) json.get("Cancel_Description");
                      
            //check if order  exists
            Order order = orderService.findByCode(orderId);
            if(order == null) {        
                errorCode = ORDER_NOT_EXIST;
                message = "Order " + orderId + " doesn't exist";
                throw new BusinessException(message);
            }
                        
            //check if status is pending or waiting
            if((!(status.equalsIgnoreCase("PENDING"))) && (!(status.equalsIgnoreCase("WAITING"))) && (!(status.equalsIgnoreCase("CANCELLED"))) && (!(status.equalsIgnoreCase("RELEASED")))) { 
                message = "the status " + orderId + " is not PENDING nor WAITING nor CANCELLED nor RELEASED";
                errorCode = BAD_ORDER_STATUS;
                throw new BusinessException(message);
            }
            // response is successful
            inboundRequest.setResponseContentType("application/json");
            inboundRequest.setResponseBody("{\"status\": \"SUCCESS\",\"message\": \"\"}");
            inboundRequest.setResponseStatus(200);
            HashMap<String, Object> context = new HashMap<>();
            if(status.equalsIgnoreCase("PENDING")) {
                if((order.getStatus().equals(OrderStatusEnum.REJECTED)||(order.getStatus().equals(OrderStatusEnum.PENDING)) || (order.getStatus().equals(OrderStatusEnum.HELD)) ||(order.getStatus().equals(OrderStatusEnum.WAITING)))){ 
                    //change status from rejected to pending
                    order.setStatus(OrderStatusEnum.PENDING);
                    orderService.update(order);

                    context.put(REPROCESS_ORDER, orderId);
                    if ("OMS".equals(order.getReceivedFromApp())) {
        				executeScript("com.eir.script.PlaceOMSOrderScript", context);
        			} else {
        				executeScript("com.eir.script.PlaceOrderScript", context);
        			}
                    reprocessHeldOrders(orderId);                   
                }
            }else if (status.equalsIgnoreCase("WAITING")) {
                //change status to HELD
                order.setStatus(OrderStatusEnum.HELD);
                orderService.update(order);
                
            }else if (status.equalsIgnoreCase("Cancelled")) {
                //change status to CANCELLED
                order.setStatus(OrderStatusEnum.CANCELLED);
                //update cancel reason code and description
                order.getCfValuesNullSafe().setValue("CANCEL_REASON_CODE", cancelReasonCode);
                order.getCfValuesNullSafe().setValue("CANCEL_DESCRIPTION", cancelDescription);
                orderService.update(order);
                reprocessHeldOrders(orderId);        
            } else if (status.equalsIgnoreCase("RELEASED")) {
            	if(!order.getStatus().equals(OrderStatusEnum.DEFERRED) && !order.getStatus().equals(OrderStatusEnum.WAITING) && !order.getStatus().equals(OrderStatusEnum.HELD)){
            		message = "the status " + orderId + " is not DEFERRED nor WAITING nor HELD";
    				errorCode = BAD_ORDER_STATUS;
    				throw new BusinessException(message);
            	}
            	order.setStatus(OrderStatusEnum.PENDING);
    			// update release description
    			order.getCfValuesNullSafe().setValue("RELEASE_DESCRIPTION", json.get("Release_Description"));
    			orderService.update(order);
    			// response is successful
    			context.put(REPROCESS_ORDER, orderId);
    			context.put(RELEASE_ORDER, orderId);
                if ("OMS".equals(order.getReceivedFromApp())) {
    				executeScript("com.eir.script.PlaceOMSOrderScript", context);
    			} else {
    				executeScript("com.eir.script.PlaceOrderScript", context);
    			}
            	
            }
        } catch (Exception e) {
            log.info("#####################error in  ReprocessOrderScript");
            log.error(e.getMessage());
            inboundRequest.setResponseBody("{\"status\": \"FAILURE\",\"errorCode\": \"" + errorCode + "\",\"message\": \"" + message + "\"}");          
            inboundRequest.setResponseStatus(500);
            
        } finally {
            log.info("******Ending of script ReprocessOrderScript******");       
        }
    }
    

    private void reprocessHeldOrders(String orderId) {
        Order order = orderService.findByCode(orderId);
        if(order == null || order.getStatus().equals(OrderStatusEnum.COMPLETED) || order.getStatus().equals(OrderStatusEnum.CANCELLED) ) {
            List<String> heldOrders = getHeldOrdersToReprocess(orderId);
            if (heldOrders != null && heldOrders.size() > 0) {
                Order orderToReprocess;
                HashMap<String, Object> context;
                for(String code : heldOrders) {
                    orderToReprocess = orderService.findByCode(code);
                    context = new HashMap<>();
                    context.put(REPROCESS_ORDER, code);
                    if("OMS".equals(orderToReprocess.getReceivedFromApp())) {
                        executeScript("com.eir.script.PlaceOMSOrderScript", context);
                    } else {
                        executeScript("com.eir.script.PlaceOrderScript", context);
                    }
                }
            }
        }       
    }


    
    /**
     * Gets the held orders to reprocess.
     *
     * @param orderId the order id
     * @return the held orders to reprocess
     */
    @SuppressWarnings("unchecked")
    private List<String> getHeldOrdersToReprocess(String orderId) {
        String query =
                "SELECT code FROM ord_order where code <> '" + orderId + "' AND status = 'HELD'" + " AND status_message like '%Error Management : " + orderId+"'";
        return orderService.getEntityManager().createNativeQuery(query).getResultList();
    }


    public void executeScript(String scriptCode, Map<String, Object> context) {
        try {
        ScriptInterface script = scriptInstanceService.getScriptInstance(scriptCode);       
        script.execute(context);
        } catch(Exception ex) {
            log.error("Failed to reprocess order", ex);
        }

    }

}
