package com.eir.script;

import java.io.Serializable;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import javax.xml.validation.SchemaFactory;

import org.meveo.admin.exception.BusinessException;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.Subscription;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.crm.custom.CustomFieldValue;
import org.meveo.model.notification.InboundRequest;
import org.meveo.model.order.OrderStatusEnum;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.script.Script;
import org.meveo.service.script.ScriptInstanceService;

/**
 * EIR Place Generic Order Script
 *
 * @author Abdellatif BARI
 */
public class PlaceGenericOrderScript extends Script {

    private ScriptInstanceService scriptInstanceService = (ScriptInstanceService) getServiceInterface("ScriptInstanceService");

    /**
     * Date time pattern
     */
    public static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

    /**
     * inbound request event parameter.
     */
    private static final String INBOUND_REQUEST_EVENT_PARAMETER = "event";

    /**
     * OMS Emitter.
     */
    private static final String OMS_EMITTER = "OMS";

    /**
     * UG Emitter.
     */
    private static final String UG_EMITTER = "UG";

    private InboundRequest inboundRequest = null;
    private OrderDTO orderDTO;

    /**
     * Tariff attribute names Enum
     */
    public enum TariffAttributeNamesEnum {
        DENSITY, REGION, SLA, CLASS_SERVICE, EF, AF, EXCHANGE
    }

    @Override
    public void execute(Map<String, Object> parametres) {
        try {
            log.info("##################### Starting of script PlaceGenericOrderScript #####################");

            initContext(parametres);

            // executing order
            executeOrder(parametres);

            log.info("##################### Ending of script PlaceGenericOrderScript #####################");
        } catch (Exception e) {
            log.error("error in place generic order script ", e.getMessage(), e);
            if (e instanceof BusinessException) {
                throw e;
            } else {
                // wrap the exception in a business exception and throwing it
                throw new BusinessException(e);
            }
        }

    }

    /**
     * Init context
     *
     * @param parametres parametres
     * @throws BusinessException the business exception
     */
    private void initContext(Map<String, Object> parametres) throws BusinessException {
        inboundRequest = (InboundRequest) parametres.get(INBOUND_REQUEST_EVENT_PARAMETER);
        if (inboundRequest == null) {
            inboundRequest = new InboundRequest();
            String body = (String) parametres.get("body");
            if (!StringUtils.isBlank(body)) {
                inboundRequest.setBody(body);
            }
            parametres.put(INBOUND_REQUEST_EVENT_PARAMETER, inboundRequest);
        } else {
            // UG case
            parametres.put("code", inboundRequest.getCode());
            parametres.put("body", inboundRequest.getBody());
        }
        orderDTO = getOrderDTO();
    }

    /**
     * Get xml order
     *
     * @return The xml order
     * @throws BusinessException the business exception
     */
    private String getXmlOrder() throws BusinessException {
        String xmlOrder = inboundRequest.getBody();
        if (!StringUtils.isBlank(xmlOrder)) {
            xmlOrder = xmlOrder.replaceAll(Pattern.quote("|"), "/");
        } else {
            throw new BusinessException("The provided order xml is empty");
        }
        return xmlOrder;
    }

    /**
     * Get order DTO
     *
     * @return The order DTO
     * @throws BusinessException the business exception
     */
    private OrderDTO getOrderDTO() throws BusinessException {
        String xmlOrder = getXmlOrder();
        return getDTOFromXml(xmlOrder, OrderDTO.class);
    }

    /**
     * Get DTO from xml
     *
     * @return The DTO
     * @throws BusinessException the business exception
     */
    private <T> T getDTOFromXml(String xml, Class<T> jaxbClass) throws BusinessException {
        T dto = null;
        try {
            JAXBContext jaxbContext = JAXBContext.newInstance(jaxbClass);
            SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            // Schema Schema = schemaFactory.newSchema(new SAXSource(new InputSource(new StringReader(XML_SCHEMA))));
            Unmarshaller jaxbUnmarshaller = null;
            jaxbUnmarshaller = jaxbContext.createUnmarshaller();
            // jaxbUnmarshaller.setSchema(Schema);
            dto = (T) jaxbUnmarshaller.unmarshal(new StringReader(xml));
        } catch (JAXBException e) {
            log.error("Convert xml to DTO fail", e);
            throw new BusinessException(e.getMessage(), e);
        }
        return dto;
    }

    /**
     * Get XML from DTO.
     *
     * @param object the object to be converted to xml
     * @param <T> the generic type of object to be converted to xml
     * @return the xml
     * @throws BusinessException the business exception
     */
    private <T> String getXmlFromDTO(T object) throws BusinessException {
        String xmlContent = null;
        try {
            if (object != null) {
                // Create JAXB Context
                JAXBContext jaxbContext = JAXBContext.newInstance((Class<T>) object.getClass());

                // Create Marshaller
                Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

                // Required formatting??
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

                // Print XML String to Console
                StringWriter stringWriter = new StringWriter();

                // Write XML to StringWriter
                jaxbMarshaller.marshal(object, stringWriter);

                // get XML Content
                xmlContent = stringWriter.toString();
            }
        } catch (JAXBException e) {
            log.error("Convert DTO to xml fail", e);
            throw new BusinessException(e.getMessage(), e);
        }
        return xmlContent;
    }

    /**
     * Execute the corresponding order
     *
     * @param parametres the inbound request parametres
     * @throws BusinessException the business exception
     */
    private void executeOrder(Map<String, Object> parametres) throws BusinessException {

        if (OMS_EMITTER.equalsIgnoreCase(orderDTO.orderingSystem)) {
            scriptInstanceService.execute("com.eir.script.PlaceOMSOrderScript", parametres);
        } else if (UG_EMITTER.equalsIgnoreCase(orderDTO.orderingSystem)) {
            scriptInstanceService.execute("com.eir.script.PlaceUGOrderScript", parametres);
        } else {
            buildFailureResponse("INVALID_VALUE", "The following parameter contains invalid value : Ordering_System");
        }
    }

    /**
     * Determine is new tariff
     *
     * @return true is new tariff, false is not.
     */
    private boolean isNewTariff() {
        for (OrderLineDTO orderLineDTO : orderDTO.orderLines) {
            String actionQualifier = orderLineDTO.billActionQualifier;

            String density = orderLineDTO.getAttribute(TariffAttributeNamesEnum.DENSITY.name());
            String region = orderLineDTO.getAttribute(TariffAttributeNamesEnum.REGION.name());
            String sla = orderLineDTO.getAttribute(TariffAttributeNamesEnum.SLA.name());
            String classService = orderLineDTO.getAttribute(TariffAttributeNamesEnum.CLASS_SERVICE.name());
            String ef = orderLineDTO.getAttribute(TariffAttributeNamesEnum.EF.name());
            String af = orderLineDTO.getAttribute(TariffAttributeNamesEnum.AF.name());

            if (!StringUtils.isBlank(actionQualifier) || !StringUtils.isBlank(density) || !StringUtils.isBlank(region) || !StringUtils.isBlank(sla)
                    || !StringUtils.isBlank(classService) || !StringUtils.isBlank(ef) || !StringUtils.isBlank(af)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Build failure response
     */
    private void buildFailureResponse(String errorCode, String errorMessage) {
        log.info("##################### error in  PlaceOMSOrderScript");
        if (inboundRequest != null) {
            inboundRequest.setResponseContentType("application/json");
            inboundRequest.setResponseBody("{\"status\": \"FAILURE\",\"errorCode\": \"" + errorCode + "\",\"message\": \"" + errorMessage + "\"}");
            inboundRequest.setResponseStatus(HttpURLConnection.HTTP_BAD_REQUEST);
        }
    }

}

/**
 * Formatted Order Object (Element tag names use InitCaps with underscores)
 *
 * @author Abdellatif BARI
 */
@XmlRootElement(name = "Wholesale_Billing")
@XmlAccessorType(XmlAccessType.FIELD)
class OrderDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = -8131517572871425298L;

    /**
     * The ordering system.
     */
    @XmlElement(name = "Ordering_System", required = true)
    String orderingSystem;

    /**
     * The order id.
     */
    @XmlElement(name = "Order_Id", required = true)
    String orderId;

    /**
     * The ref order id.
     */
    @XmlElement(name = "Ref_Order_Id", required = true)
    String refOrderId;

    /**
     * The transaction id.
     */
    @XmlElement(name = "Transaction_Id", required = true)
    String transactionId;

    /**
     * The status message.
     */
    @XmlElement(name = "Status_Message")
    String statusMessage;

    /**
     * The bill items.
     */
    @XmlElement(name = "Bill_Item")
    List<OrderLineDTO> orderLines = new ArrayList<>();

    @XmlTransient
    OrderStatusEnum status;

    // order details text included in CF : ORDER_DETAILS
    Map<String, String> getOrderDetails() {
        Map<String, String> orderDetails = new HashMap<>();
        orderDetails.put("" + orderId, orderingSystem + CustomFieldValue.MATRIX_KEY_SEPARATOR + refOrderId + CustomFieldValue.MATRIX_KEY_SEPARATOR + transactionId);
        return orderDetails;
    }
}

/**
 * Formatted Order Line Object
 *
 * @author Abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
class OrderLineDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 4708299185262848186L;

    /**
     * The item id.
     */
    @XmlElement(name = "Item_Id", required = true)
    String itemId;

    /**
     * The operator id.
     */
    @XmlElement(name = "Operator_Id", required = true)
    String operatorId;

    /**
     * The operator id.
     */
    @XmlElement(name = "Item_Component_Id", required = true)
    String itemComponentId;

    /**
     * The item version.
     */
    @XmlElement(name = "Item_Version")
    String itemVersion;

    /**
     * The item type.
     */
    @XmlElement(name = "Item_Type")
    String itemType;

    /**
     * The agent code.
     */
    @XmlElement(name = "Agent_Code")
    String agentCode;

    /**
     * The uan.
     */
    @XmlElement(name = "UAN")
    String uan;

    /**
     * The master account number.
     */
    @XmlElement(name = "Master_Account_Number")
    String masterAccountNumber;

    /**
     * The service id.
     */
    @XmlElement(name = "Service_Id")
    String serviceId;

    /**
     * The ref service id.
     */
    @XmlElement(name = "Ref_Service_Id")
    String refServiceId;

    /**
     * The xref service id.
     */
    @XmlElement(name = "Xref_Service_Id")
    String xRefServiceId;

    /**
     * The bill code.
     */
    @XmlElement(name = "Bill_Code", required = true)
    String billCode;

    /**
     * The action.
     */
    @XmlElement(name = "Action", required = true)
    String billAction;

    /**
     * The action qualifier.
     */
    @XmlElement(name = "Action_Qualifier")
    String billActionQualifier;

    /**
     * The quantity.
     */
    @XmlElement(name = "Quantity", required = true)
    String billQuantity;

    /**
     * The effect date.
     */
    @XmlElement(name = "Effect_Date", required = true)
    @XmlJavaTypeAdapter(DateTimeAdapter.class)
    Date billEffectDate;

    /**
     * The special connect.
     */
    @XmlElement(name = "Special_Connect")
    String specialConnect;

    /**
     * The special rental.
     */
    @XmlElement(name = "Special_Rental")
    String specialRental;

    /**
     * The operator order number.
     */
    @XmlElement(name = "Operator_Order_Number")
    String operatorOrderNumber;

    /**
     * The attributes list.
     */
    @XmlElement(name = "Attribute")
    List<AttributeDTO> attributes = new ArrayList<>();

    @XmlTransient
    String deducedServiceId;
    @XmlTransient
    String billType;
    @XmlTransient
    OfferTemplate offerTemplate;
    @XmlTransient
    BillingAccount billingAccount;
    @XmlTransient
    Subscription subscription;
    @XmlTransient
    Map<String, String> catalog;
    @XmlTransient
    Map<String, String> attributesMap;
    @XmlTransient
    String chargeType;

    String getDetails() {
        StringBuilder sb = new StringBuilder();
        String action = billAction;
        if ("COS".equalsIgnoreCase(action) || "TOS".equalsIgnoreCase(action)) {
            action = "C";
        }
        if ("CNS".equalsIgnoreCase(action) || "TNS".equalsIgnoreCase(action)) {
            action = "P";
        }
        String offerCode = offerTemplate != null ? offerTemplate.getCode() : "";
        sb.append(operatorId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(itemComponentId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(itemVersion)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(itemType).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(agentCode)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(uan).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(masterAccountNumber)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(serviceId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(refServiceId)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(xRefServiceId).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billCode)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(action).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billActionQualifier)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billQuantity).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billEffectDate)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(billType).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(offerCode)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(operatorOrderNumber).append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        return sb.toString();
    }

    /**
     * Get the order line attribute by code
     *
     * @param code the attribute code
     * @return the order line attribute by code
     */
    public String getAttribute(String code) {
        if (attributesMap == null) {
            attributesMap = new HashMap<>();
            for (AttributeDTO attributeDTO : attributes) {
                attributesMap.put(attributeDTO.code, attributeDTO.value);
            }
        }
        return attributesMap.get(code);
    }

    /**
     * Set the order line attribute by code
     *
     * @param code the attribute code
     * @param value the attribute value
     */
    public void setAttribute(String code, String value) {
        if (attributesMap == null) {
            attributesMap = new HashMap<>();
        }
        attributesMap.put(code, value);
    }

}

/**
 * Formatted Order Line Attributes Object
 *
 * @author abdellatif BARI
 */
@XmlRootElement()
@XmlAccessorType(XmlAccessType.FIELD)
class AttributeDTO implements Serializable {

    /**
     * The Constant serialVersionUID.
     */
    private static final long serialVersionUID = 1888836652073239465L;

    /**
     * The code.
     */
    @XmlElement(name = "Code", required = true)
    String code;

    /**
     * The value.
     */
    @XmlElement(name = "Value", required = true)
    String value;

    /**
     * The unit.
     */
    @XmlElement(name = "Unit")
    String unit;

    /**
     * The component id.
     */
    @XmlElement(name = "Component_Id")
    String componentId;

    /**
     * The attribute type.
     */
    @XmlElement(name = "Attribute_Type")
    String attributeType;

    String getDetails() {
        StringBuilder sb = new StringBuilder();
        sb.append(value).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(unit).append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(componentId)
            .append(CustomFieldValue.MATRIX_KEY_SEPARATOR).append(attributeType).append(CustomFieldValue.MATRIX_KEY_SEPARATOR);
        return sb.toString();
    }
}

/**
 * This is Adaptor class which has main responsibility to convert from java.util.Date to format string of date.
 *
 * @author Abdellatif BARI
 */
class DateTimeAdapter extends XmlAdapter<String, Date> {

    @Override
    public Date unmarshal(String xml) {
        return DateUtils.parseDateWithPattern(xml, PlaceGenericOrderScript.DATE_TIME_PATTERN);
    }

    @Override
    public String marshal(Date object) {
        return DateUtils.formatDateWithPattern(object, PlaceGenericOrderScript.DATE_TIME_PATTERN);
    }
}