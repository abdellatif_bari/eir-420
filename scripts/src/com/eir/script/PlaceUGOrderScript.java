package com.eir.script;

import java.io.StringReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.regex.Pattern;

import javax.persistence.Query;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.meveo.admin.exception.BusinessException;
import org.meveo.admin.exception.RatingScriptExecutionErrorException;
import org.meveo.commons.utils.QueryBuilder;
import org.meveo.commons.utils.StringUtils;
import org.meveo.model.DatePeriod;
import org.meveo.model.IEnable;
import org.meveo.model.billing.BillingAccount;
import org.meveo.model.billing.ChargeInstance;
import org.meveo.model.billing.InstanceStatusEnum;
import org.meveo.model.billing.OneShotChargeInstance;
import org.meveo.model.billing.RecurringChargeInstance;
import org.meveo.model.billing.ServiceInstance;
import org.meveo.model.billing.Subscription;
import org.meveo.model.billing.SubscriptionStatusEnum;
import org.meveo.model.billing.SubscriptionTerminationReason;
import org.meveo.model.billing.UserAccount;
import org.meveo.model.billing.WalletOperation;
import org.meveo.model.catalog.OfferTemplate;
import org.meveo.model.catalog.OneShotChargeTemplate;
import org.meveo.model.catalog.ServiceTemplate;
import org.meveo.model.crm.Customer;
import org.meveo.model.crm.custom.CustomFieldValue;
import org.meveo.model.mediation.Access;
import org.meveo.model.notification.InboundRequest;
import org.meveo.model.order.Order;
import org.meveo.model.order.OrderItem;
import org.meveo.model.order.OrderStatusEnum;
import org.meveo.model.payments.CustomerAccount;
import org.meveo.model.shared.DateUtils;
import org.meveo.service.admin.impl.SellerService;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.billing.impl.OneShotChargeInstanceService;
import org.meveo.service.billing.impl.ServiceInstanceService;
import org.meveo.service.billing.impl.SubscriptionService;
import org.meveo.service.billing.impl.UserAccountService;
import org.meveo.service.catalog.impl.ChargeTemplateService;
import org.meveo.service.catalog.impl.OfferTemplateService;
import org.meveo.service.catalog.impl.ServiceTemplateService;
import org.meveo.service.crm.impl.CustomFieldInstanceService;
import org.meveo.service.crm.impl.ProviderService;
import org.meveo.service.crm.impl.SubscriptionTerminationReasonService;
import org.meveo.service.medina.impl.AccessService;
import org.meveo.service.order.OrderService;
import org.meveo.service.payments.impl.CustomerAccountService;
import org.meveo.service.script.Script;
import org.meveo.service.script.ScriptInstanceService;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

/**
 * EIR Place UG Order Script
 *
 * @author Mohammed Amine TAZI
 */
public class PlaceUGOrderScript extends Script {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    // main logger
    private static final Logger LOGGER = Logger.getLogger(PlaceUGOrderScript.class.getName());

    // API Services
    private CustomerAccountService customerAccountService = (CustomerAccountService) getServiceInterface(CustomerAccountService.class.getSimpleName());
    private ChargeTemplateService<OneShotChargeTemplate> chargeTemplateService = (ChargeTemplateService) getServiceInterface(ChargeTemplateService.class.getSimpleName());
    private OrderService orderService = (OrderService) getServiceInterface(OrderService.class.getSimpleName());
    private SellerService sellerService = (SellerService) getServiceInterface(SellerService.class.getSimpleName());
    private UserAccountService userAccountService = (UserAccountService) getServiceInterface(UserAccountService.class.getSimpleName());
    private BillingAccountService billingAccountService = (BillingAccountService) getServiceInterface(BillingAccountService.class.getSimpleName());
    private SubscriptionService subscriptionService = (SubscriptionService) getServiceInterface(SubscriptionService.class.getSimpleName());
    private OfferTemplateService offerTemplateService = (OfferTemplateService) getServiceInterface(OfferTemplateService.class.getSimpleName());
    private ServiceInstanceService serviceInstanceService = (ServiceInstanceService) getServiceInterface(ServiceInstanceService.class.getSimpleName());
    private ServiceTemplateService serviceTemplateService = (ServiceTemplateService) getServiceInterface(ServiceTemplateService.class.getSimpleName());
    private SubscriptionTerminationReasonService subscriptionTerminationReasonService = (SubscriptionTerminationReasonService) getServiceInterface(
        SubscriptionTerminationReasonService.class.getSimpleName());
    private ProviderService providerService = (ProviderService) getServiceInterface(ProviderService.class.getSimpleName());
    private OneShotChargeInstanceService oneShotChargeInstanceService = (OneShotChargeInstanceService) getServiceInterface(OneShotChargeInstanceService.class.getSimpleName());
    private AccessService accessService = (AccessService) getServiceInterface(AccessService.class.getSimpleName());
    private CustomFieldInstanceService customFieldInstanceService = (CustomFieldInstanceService) getServiceInterface(CustomFieldInstanceService.class.getSimpleName());
    private ScriptInstanceService scriptInstanceService = (ScriptInstanceService) getServiceInterface("ScriptInstanceService");
    // variables
    private InboundRequest inboundRequest = null;
    private static String errorCode;
    private static String message;
    private String orderMessage;
    private Boolean orderRejected = false;
    private String orderStatus;
    private String dataDiscrepancies = null;
    private String reprocessOrder = null;
    private String releaseOrder = null;
    private Map<String, List<String>> changeMap;
    private List<UserAccount> createdUsersAccount;
    private List<Subscription> createdSubscriptions;
    private static final String TERMINATION_REASON_MAIN = "TR_PRIMARY";
    private static final String TERMINATION_REASON_ANCILLARY = "TR_ANCILLARY";
    private static final String TERMINATION_REASON_NONE = "TR_NONE";
    private static final String TERMINATION_REASON_TOS = "TR_TOS";
    public static final String REPROCESS_ORDER = "REPROCESS_ORDER";
    public static final String RELEASE_ORDER = "RELEASE_ORDER";
    private static final String SUBSCRIPTION_PREFIX = "SUBSCRIPTION_PREFIX";
    public static final Integer SUBSCRIPTION_TRIAL_DAYS = 14;
    public static final String SEPARATOR = "|";

    // error codes
    private static final String MISSING_PARAMETER = "MISSING_PARAMETER";
    private static final String INVALID_SIZE = "INVALID_SIZE";
    private static final String INVALID_VALUE = "INVALID_VALUE";
    private static final String ORDER_ALREADY_EXISTS = "ORDER_ALREADY_EXISTS";
    private static final String GENERIC_API_EXCEPTION = "GENERIC_API_EXCEPTION";

    // TODO : Create for each constant one custom field on provider config.
    private static final String MISC_OFFER_TEMPLATE_CODE = "MISC_WB";
    private static final String MISC_CHARGE_TEMPLATE_CODE = "MISC_CHARGE";

    public static final String DATE_TIME_PATTERN = "dd/MM/yyyy HH:mm:ss";

    @Override
    public void execute(Map<String, Object> initContext) throws BusinessException {
        try {
            LOGGER.info("#####################Starting of script PlaceUGOrderScript");

            // receiving Provisioning data in XML Format
            inboundRequest = (InboundRequest) initContext.get("event");
            orderMessage = (String) initContext.get("body");
            reprocessOrder = (String) initContext.get(REPROCESS_ORDER);
            releaseOrder = (String) initContext.get(RELEASE_ORDER);
            Order order = null;
            orderRejected = false;
            orderStatus = null;
            errorCode = null;
            message = null;
            dataDiscrepancies = "";
            createdUsersAccount = new ArrayList<>();
            createdSubscriptions = new ArrayList<>();

            if (reprocessOrder != null) {
                LOGGER.info("Reprocessing order : " + reprocessOrder);
                order = orderService.findByCode(reprocessOrder);
                if (order != null) {
                    orderMessage = (String) order.getCfValuesNullSafe().getValue("ORDER_MESSAGE");
                }
            }
            if (orderMessage != null) {
                orderMessage = orderMessage.replaceAll(Pattern.quote("|"), "/");
            }

            // parsing order
            OrderDTO orderDTO = parseOrder();

            // Detect change or transfer
            detectChangeOrTransfer(orderDTO);

            // Detect change number
            detectChangeNumber(orderDTO);

            // Sort Order lines with pre-defined sequence
            sortOrderLines(orderDTO.orderLines);

            // check Data Discrepancies
            checkDataDiscrepancies(orderDTO);

            order = persisteOrder(orderDTO);

            executeOrder(orderDTO);

            if (StringUtils.isNotBlank(dataDiscrepancies)) {
                order.getCfValuesNullSafe().setValue("ORDER_DATA_DISCREPANCIES", dataDiscrepancies);
            }
            if (orderRejected) { // Order is rejected and needs more data
                order.getCfValuesNullSafe().setValue("ORDER_ERROR_MESSAGE", orderStatus);
                LOGGER.info("updating order");
                updateOrder(order, OrderStatusEnum.REJECTED);
                rollbackCreatedEntities();
            } else if (orderDTO.status == OrderStatusEnum.HELD) {
                updateOrder(order, OrderStatusEnum.HELD);
                rollbackCreatedEntities();
            } else {
                // change the order status to COMPLETED and set the completion date to execution
                // date
                order.setCompletionDate(new Date());
                updateOrder(order, OrderStatusEnum.COMPLETED);
                LOGGER.info("Order completed : " + order.getCode());
            }

            updateXmlOrderCF(order, orderDTO);

            // response is successful
            if (inboundRequest != null) {
                inboundRequest.setResponseContentType("application/json");
                inboundRequest.setResponseBody("{\"status\": \"SUCCESS\",\"message\": \"\"}");
                inboundRequest.setResponseStatus(200);
            }
            LOGGER.info("#####################Ending of script PlaceUGOrderScript");

        } catch (Exception e) {
            LOGGER.info("#####################error in  PlaceUGOrderScript");
            e.printStackTrace();
            if (initContext.get("source") != null && initContext.get("source").toString().equalsIgnoreCase("csv")) {
                LOGGER.info("source is csv");
                throw new BusinessException(e);
            } else {
                LOGGER.info("source is not csv");
            }
            if (inboundRequest != null) {
                inboundRequest.setResponseContentType("application/json");
                if (errorCode == null) {
                    errorCode = GENERIC_API_EXCEPTION;
                }
                if (message == null) {
                    message = "Exception occured at the API level";
                }
                inboundRequest.setResponseBody("{\"status\": \"FAILURE\",\"errorCode\": \"" + errorCode + "\",\"message\": \"" + message + "\"}");
                inboundRequest.setResponseStatus(500);
                if (MISSING_PARAMETER.equals(errorCode)) {
                    inboundRequest.setResponseStatus(400);
                }
            }
        }
    }

    /**
     * Update order.
     *
     * @param order the order
     * @param status the status
     */
    private void updateOrder(Order order, OrderStatusEnum status) {
        order.setStatus(status);
        order.setStatusMessage(orderStatus);
        orderService.update(order);
        LOGGER.info("Order " + order.getStatus() + " : " + order.getCode());
        LOGGER.info("Order Status : " + orderStatus);
    }

    /**
     * Rollback created entities.
     */
    private void rollbackCreatedEntities() {
        // rollback Created Entities (userAccount, subscriptions)
        LOGGER.info("Subscriptions to rollback :" + createdSubscriptions.size());
        LOGGER.info("Users Account to rollback :" + createdUsersAccount.size());
        for (Subscription sub : createdSubscriptions) {
            subscriptionService.getEntityManager().remove(sub);
        }
        for (UserAccount ua : createdUsersAccount) {
            userAccountService.getEntityManager().remove(ua);
        }
    }

    /**
     * Check data discrepancies.
     *
     * @param orderDTO the order DTO
     * @throws Exception the exception
     */
    private void checkDataDiscrepancies(OrderDTO orderDTO) throws Exception {
        // PO2-459
        for (OrderLineDTO orderLineDTO : orderDTO.orderLines) {

            // 2- The UAN on the bill item should be the same as the UAN against the subscriber, Except CU orders
            if (orderLineDTO.uan != null && !"CU".equals(orderLineDTO.billAction) && !"P".equalsIgnoreCase(orderLineDTO.getOriginalOrderAction())) {
                OfferTemplate offerTemplate = offerTemplateService.findByCode(orderLineDTO.offerName);
                Subscription oldSubscription = subscriptionService.findByCode(getServiceIdWithPrefix(orderLineDTO));
                if (oldSubscription != null && oldSubscription.getStatus() == SubscriptionStatusEnum.ACTIVE && oldSubscription.getCfValuesNullSafe().getValue("UAN") != null
                        && !orderLineDTO.uan.equals(oldSubscription.getCfValuesNullSafe().getValue("UAN"))) {
                    LOGGER.info(
                        "oldSubscription :" + oldSubscription.getCode() + " UAN: " + oldSubscription.getCfValuesNullSafe().getValue("UAN") + " New UAN : " + orderLineDTO.uan);
                    LOGGER.info("orderLineDTO " + orderLineDTO.getDetails());
                    rejectOrder("The UAN provided in the bill item is not the same as the UAN stored against the subscription " + oldSubscription.getCode());
                    return;
                }
            }

            // START_RANGE an END_RANGE attributes exist on bill items where the bill code is not for an MSN or DDI service
            Map<String, String> attributes = getRangesAttributes(orderLineDTO);
            if (attributes != null && attributes.size() > 0) {
                if (!AncillaryServiceEnum.MSN.name().equalsIgnoreCase(orderLineDTO.billCode) && !orderLineDTO.billCode.startsWith(AncillaryServiceEnum.DDI.name())) {
                    dataDiscrepancies += "RANGE_START or RANGE_END attribute exists on bill items where the bill code is not for an MSN or DDI service. \n";
                } else if (AncillaryServiceEnum.MSN.name().equalsIgnoreCase(orderLineDTO.billCode)
                        && (orderLineDTO.originalServiceId.equalsIgnoreCase(attributes.get("RANGE_START"))
                                || orderLineDTO.originalServiceId.equalsIgnoreCase(attributes.get("RANGE_END")))) {
                    rejectOrder("An MSN number cannot be the same as the Primary Service Id Number stored against the subscriber");
                    return;
                } else if (!"CN".equals(orderLineDTO.billAction) && !"C".equals(orderLineDTO.billAction) && orderLineDTO.isMain
                        && !isValidRanges(attributes.get("RANGE_START"), attributes.get("RANGE_END"), orderLineDTO.originalServiceId)) {
                    rejectOrder("RANGE_START or RANGE_END already exist or interfers with existing subscription");
                    return;
                }
            }
        }

    }

    /**
     * Detect change Number
     *
     * @param orderDTO incoming order
     * @throws BusinessException App exception
     */
    private void detectChangeNumber(OrderDTO orderDTO) {
        List<OrderLineDTO> orderLinesToCease = new ArrayList<>();
        List<OrderLineDTO> orderLinesToProvide = new ArrayList<>();

        for (OrderLineDTO orderLineDTO : orderDTO.orderLines) {
            if (orderLineDTO.xRefServiceId != null) {
                switch (orderLineDTO.billAction) {
                case "C":
                    orderLinesToCease.add(orderLineDTO);
                    break;
                case "P":
                    orderLinesToProvide.add(orderLineDTO);
                    break;
                }
            }
        }

        for (OrderLineDTO orderLineToCease : orderLinesToCease) {
            for (OrderLineDTO orderLineToProvide : orderLinesToProvide) {
                if (orderLineToCease.serviceId.equalsIgnoreCase(orderLineToProvide.xRefServiceId)
                        && orderLineToProvide.serviceId.equalsIgnoreCase(orderLineToCease.xRefServiceId)) {
                    orderLineToCease.billAction = "COS";
                    orderLineToProvide.billAction = "CNS";
                    LOGGER.info("change number detected for " + orderLineToCease.billCode + " and " + orderLineToProvide.billCode);
                }
            }
        }
    }

    /**
     * Detect change or transfer via Matrix
     *
     * @param orderDTO Incoming order
     * @throws Exception App Exception
     */
    private void detectChangeOrTransfer(OrderDTO orderDTO) throws Exception {
        changeMap = new HashMap<>();
        Map<String, List<OrderLineDTO>> orderMap = new HashMap<>();
        List<OrderLineDTO> orderLines;
        for (OrderLineDTO orderLineDTO : orderDTO.orderLines) {
            if (orderLineDTO.originalServiceId == null || !orderLineDTO.isMain
                    || (!("P".equalsIgnoreCase(orderLineDTO.billAction)) && !("C".equalsIgnoreCase(orderLineDTO.billAction)))) {
                continue;
            }
            if (orderMap.get(orderLineDTO.originalServiceId) != null) {
                orderLines = orderMap.get(orderLineDTO.originalServiceId);
            } else if (orderMap.get(orderLineDTO.xRefServiceId) != null) { // in case of transfer with change
                orderLines = orderMap.get(orderLineDTO.xRefServiceId);
            } else {
                orderLines = new ArrayList<>();
            }
            orderLines.add(orderLineDTO);
            orderMap.put(orderLineDTO.originalServiceId, orderLines);
        }
        boolean changeDetected = false;
        boolean ceaseDetected = false;
        boolean transferDetected = false;
        String ceasedOpperatorId;
        boolean isTransferAction = false;
        for (List<OrderLineDTO> orderLinesList : orderMap.values()) {
            if (orderLinesList.size() < 2) {
                continue;
            }
            changeDetected = false;
            ceaseDetected = false;
            transferDetected = false;
            ceasedOpperatorId = null;

            for (OrderLineDTO orderLineDTO : orderLinesList) {
                if ("C".equalsIgnoreCase(orderLineDTO.billAction)) {
                    ceaseDetected = true;
                    ceasedOpperatorId = orderLineDTO.operatorId;
                } else if ("P".equalsIgnoreCase(orderLineDTO.billAction) && ceaseDetected) {
                    if (orderLineDTO.operatorId.equals(ceasedOpperatorId)) {
                        changeDetected = true;
                    } else {
                        transferDetected = true;
                        isTransferAction = true;
                    }
                }
            }

            if (changeDetected || transferDetected) {
                for (OrderLineDTO orderLineDTO : orderLinesList) {
                    if ("C".equalsIgnoreCase(orderLineDTO.billAction)) {
                        if (changeDetected) {
                            List<String> billCodes = new ArrayList<>();
                            billCodes.add(getMigrationCode(orderLineDTO, offerTemplateService.findByCode(orderLineDTO.offerName)));
                            changeMap.put(orderLineDTO.serviceId, billCodes);
                            orderLineDTO.billAction = "COS";
                            LOGGER.info("Change detected : " + orderLineDTO.billCode + " will be ceased");
                        } else if (transferDetected) {
                            orderLineDTO.billAction = "TOS";
                            LOGGER.info("Transfer detected : " + orderLineDTO.billCode + " will be ceased");
                        }
                    } else if ("P".equalsIgnoreCase(orderLineDTO.billAction)) {
                        if (changeDetected) {
                            List<String> billCodes = changeMap.get(orderLineDTO.serviceId);
                            if (billCodes != null) {
                                billCodes.add(getMigrationCode(orderLineDTO, offerTemplateService.findByCode(orderLineDTO.offerName)));
                            }
                            changeMap.put(orderLineDTO.serviceId, billCodes);
                            orderLineDTO.billAction = "CNS";
                            LOGGER.info("Change detected : " + orderLineDTO.billCode + " will be provided");
                        } else if (transferDetected) {
                            orderLineDTO.billAction = "TNS";
                            LOGGER.info("Transfer detected : " + orderLineDTO.billCode + " will be provided");
                        }
                    }
                }
            }
        }
    }

    /**
     * Get product migration rules
     *
     * @param from Migration Code Source
     * @param to Migration Code Target
     * @return Migration Code
     */
    private static String getProductMigrationRules(String from, String to, Map<String, String> migrationMatrix) {
        LOGGER.info("FROM : " + from + " - TO : " + to);
        int score = 0;
        List<String> bestMatches = new ArrayList<String>();
        for (Map.Entry<String, String> entry : migrationMatrix.entrySet()) {
            String fromtoConditionRules = entry.getKey().toString(); // 10_10_10|10_10_10|=0
            String[] tab = fromtoConditionRules.split(Pattern.quote("|"));
            if (tab.length == 0) {
                continue;
            }

            String fRules = fromtoConditionRules.split(Pattern.quote("|"))[0];
            String tRules = fromtoConditionRules.split(Pattern.quote("|"))[1];

            String condition;
            if (tab.length == 2) {
                condition = "";
            } else {
                condition = fromtoConditionRules.split(Pattern.quote("|"))[2];
            }

            String fromRules = fRules.trim();
            String toRules = tRules.trim();

            if (fromRules.trim().equals("*")) {
                fromRules = from.charAt(0) + fromRules;
            }
            if (toRules.trim().equals("*")) {
                toRules = to.charAt(0) + toRules;
            }
            String fromToCompare = fromRules.split("\\*")[0]; // 10_01_
            String toCompare = toRules.split("\\*")[0]; // 10_02_

            if (from.equals(fromToCompare) || from.startsWith(fromToCompare)) {
                if (to.equals(toCompare) || to.startsWith(toCompare)) {
                    if (score == (fromToCompare.length() + toCompare.length())) {
                        bestMatches.add(fRules + "|" + tRules + "|" + condition);
                    } else if (score < (fromToCompare.length() + toCompare.length())) {
                        score = fromToCompare.length() + toCompare.length();
                        bestMatches.clear();
                        bestMatches.add(fRules + "|" + tRules + "|" + condition);
                    }
                }
            }
        }
        LOGGER.info("bestMatches" + bestMatches);
        if (bestMatches.size() == 1) {
            return bestMatches.get(0).toString();
        } else if (bestMatches.size() > 1) {
            if (StringUtils.isNotBlank(from) && StringUtils.isNotBlank(to)) {
                int fromRR = Integer.parseInt(from.split(Pattern.quote("_"))[2].trim());
                int toRR = Integer.parseInt(to.split(Pattern.quote("_"))[2].trim());
                int condition = fromRR - toRR;
                for (String match : bestMatches) {
                    String conditionRule = match.split(Pattern.quote("|"))[2].trim(); // =0 <1 >1
                    if (condition >= 1 && (conditionRule.contains(">1") || conditionRule.contains("RR-RR>1"))) {
                        return match;
                    } else if (condition == 0 && (conditionRule.contains("=0") || conditionRule.contains("RR-RR=0"))) {
                        return match;
                    } else if (condition < 1 && (conditionRule.contains("<1") || conditionRule.contains("RR-RR<1"))) {
                        return match;
                    }
                }
            }
        }
        return null;
    }

    /**
     * Sort orderLines with pre-defined sequence
     *
     * @param orderLines Bill items
     */
    private void sortOrderLines(List<OrderLineDTO> orderLines) {
        Collections.sort(orderLines, new Comparator<OrderLineDTO>() {
            @Override
            public int compare(OrderLineDTO l1, OrderLineDTO l2) {
                return getPosition(l1) - getPosition(l2);
            }

            private int getPosition(OrderLineDTO l1) {
                switch (l1.billAction) {
                case "TOS":
                    return 2; // transfer (cease) old main service
                case "TNS":
                    return 3; // transfer (provide) new main service
                case "COS":
                    return 2; // transfer (cease) old main service
                case "CNS":
                    return 3; // transfer (provide) new main service
                case "C":
                    if (l1.isMain) {
                        return 2;
                    } else {
                        return 1;
                    }
                case "P":
                    if (l1.isMain) {
                        return 3;
                    } else {
                        return 4;
                    }

                case "CU":
                    return 5; // change UAN
                case "CN":
                    return 6; // change Number
                case "SN":
                    return 7; // Swap Number
                case "M":
                    return 8; // Miscallenous charge
                case "MC":
                    return 9; // Miscallenous charge with new Price
                case "PR":
                    return 10; // Remark
                }
                return 11; // each other actions will be at the end of list
            }
        });

    }

    /**
     * Check if Customer Hierarchy is correct and exits in OC
     *
     * @param orderDTO Current order
     * @throws Exception App exception
     */
    private void checkCustomerHierarchy(OrderLineDTO orderLineDTO) throws Exception {
        // Check Customer (Operator_Id)
        CustomerAccount customerAccount = customerAccountService.findByCode(getProductSetPrefixCode(orderLineDTO, offerTemplateService.findByCode(orderLineDTO.offerName)));
        customerAccount = customerAccount == null ? customerAccountService.findByCode(orderLineDTO.operatorId) : customerAccount;

        if (customerAccount == null) {
            rejectOrder("The customer Account " + orderLineDTO.operatorId + " is not found");
            return;
        }
        if (customerAccount.getCustomer() == null) {
            rejectOrder("The customer of the Customer Account" + orderLineDTO.operatorId + " is not found");
            return;
        }
        // Check Billing Account (Master_Account_Number)
        LOGGER.info("Billing account : " + orderLineDTO.masterAccountNumber);
        BillingAccount billingAccount = billingAccountService.findByCode(orderLineDTO.masterAccountNumber);
        if (billingAccount == null) {
            rejectOrder("The billing account " + orderLineDTO.masterAccountNumber + " is not found");
        } // Check Billing Account associated to the Customer
        else if (!billingAccount.getCustomerAccount().getCode().equals(customerAccount.getCode())) {
            LOGGER.info("customerAccount :" + customerAccount.getCode());
            LOGGER.info("customerAccount of the BA :" + billingAccount.getCustomerAccount().getCode());
            rejectOrder("Incompatible billing account");
        }
    }

    /**
     * Gets the customer by operator id.
     *
     * @param operatorId the operator id
     * @return the customer by operator id
     */
    private Customer getCustomerByOperatorId(String operatorId) {
        LOGGER.info("Operator Id : " + operatorId);
        List<CustomerAccount> customersAccount = customerAccountService.findByCodeLike(operatorId);
        if (customersAccount == null || customersAccount.isEmpty()) {
            rejectOrder("The customer account " + operatorId + " is not found");
            return null;
        }
        return customersAccount.get(0).getCustomer(); // all Customer Account of the operatorId have the same Customer
    }

    /**
     * Create Order from incoming data
     *
     * @param orderDTO Current Order
     * @return Order created
     * @throws Exception App exception
     */
    private Order persisteOrder(OrderDTO orderDTO) throws Exception {
        Order order = orderService.findByCode(orderDTO.orderId.toString());
        if (reprocessOrder == null) {
            // check if order already exists
            if (order != null) {
                errorCode = ORDER_ALREADY_EXISTS;
                message = "Order " + orderDTO.orderId + " already exists";
                throw new Exception(message);
            }
            order = new Order();
        }
        // store Order Items and attributes in CF at Order level
        Map<String, String> orderItemsCF = new LinkedHashMap<>();
        Map<String, String> orderItemAttributesCF = new HashMap<>();

        // Populate order attributes
        List<OrderItem> orderItems = new ArrayList<>();
        // for each order item create it at the order level
        UserAccount userAccount = null;
        for (OrderLineDTO orderLineDTO : orderDTO.orderLines) {
            if (orderLineDTO.serviceId == null) {
                orderLineDTO.serviceId = "MISC_" + orderLineDTO.masterAccountNumber;
                if ("M".equalsIgnoreCase(orderLineDTO.billAction)) {
                    orderLineDTO.billAction = "MC";
                }
            }
            OfferTemplate offerTemplate = null;
            if("C".equalsIgnoreCase(orderLineDTO.billAction) && userAccountService.findByCode(orderLineDTO.serviceId) == null) {
                rejectOrder("The subscription is not found");
            }
            if (!"ALL".equalsIgnoreCase(orderLineDTO.billCode) && !OrderLineBillCodeEnum.RMK.name().equalsIgnoreCase(orderLineDTO.billCode)) {
                orderLineDTO.offerName = getOfferName(orderLineDTO);
                offerTemplate = offerTemplateService.findByCode(orderLineDTO.offerName);
                orderLineDTO.serviceId = getServiceIdWithPrefix(orderLineDTO);
                userAccount = userAccountService.findByCode(orderLineDTO.serviceId);
                BillingAccount billingAccount = billingAccountService.findByCode(orderLineDTO.masterAccountNumber);
                if (billingAccount == null) {
                    rejectOrder("The billing account is not found");
                }
                // if user account doesn't exist, create new one with code = Service_Id
                if (userAccount == null && !orderRejected && (!(orderLineDTO.serviceId.contains("00-") && "M".equalsIgnoreCase(orderLineDTO.billAction)))) {
                    userAccount = new UserAccount();
                    userAccount.setCode(orderLineDTO.serviceId);
                    // Construct masterAccountNumber if does not exist
                    userAccountService.createUserAccount(billingAccount, userAccount);
                    createdUsersAccount.add(userAccount);
                }

                // checking customer hierarchy
                checkCustomerHierarchy(orderLineDTO);

                Map<String, String> catalog = getCatalog(orderLineDTO, offerTemplate);
                if (catalog == null) {
                    rejectOrder("Bill code " + orderLineDTO.billCode + " not found in catalog for date " + orderLineDTO.billEffectDate);
                    continue;
                }
                orderLineDTO.billType = getBillCodeField(catalog, orderLineDTO, Catalog.SERVICE_TYPE);
            }
            // store order items and their attributes in adequate CFs
            orderItemsCF.put(orderLineDTO.itemId.toString(), orderLineDTO.getDetails());
            for (AttributeDTO attributeDTO : orderLineDTO.attributes) {
                orderItemAttributesCF.put(orderLineDTO.itemId + "|" + attributeDTO.code, attributeDTO.getDetails());
            }
        }
        // create the order with related CFs
        if (reprocessOrder == null) {
            order.setCode(orderDTO.orderId.toString());
            order.setReceivedFromApp(orderDTO.orderingSystem);
            order.setStatus(OrderStatusEnum.PENDING);
            order.setOrderItems(orderItems);
            orderService.create(order);
            order.getCfValuesNullSafe().setValue("ORDER_MESSAGE", orderMessage);
            order.getCfValuesNullSafe().setValue("ORDER_DETAILS", orderDTO.getOrderDetails());
            order.getCfValuesNullSafe().setValue("ORDER_ITEMS", orderItemsCF);
            order.getCfValuesNullSafe().setValue("ORDER_ITEM_ATTRIBUTES", orderItemAttributesCF);
        }
        orderService.update(order);
        return order;
    }

    /**
     * @param orderStatus
     */
    private void rejectOrder(String orderStatus) {
        if (orderRejected == Boolean.FALSE) {
            orderRejected = Boolean.TRUE;
            this.orderStatus = orderStatus;
        }

    }

    /**
     * Execute created order by activating/changing/ceasing services in subscription
     *
     * @param orderDTO Current Formatted Order
     * @param order Current Order
     * @return Executed Order
     * @throws Exception App Exception
     */

    private void executeOrder(OrderDTO orderDTO) throws Exception {
        // Don't execute order if there is at least one error or the order is on hold and not released.
        if (orderRejected || (StringUtils.isBlank(releaseOrder) && isHeldOrder(orderDTO))) {
            return;
        }

        // For each order line, execute the service (activate/change/cease)
        for (OrderLineDTO orderLineDTO : orderDTO.orderLines) {
            if (orderLineDTO.offerName == null) {
                orderLineDTO.offerName = getOfferName(orderLineDTO);
            }
            switch (orderLineDTO.billAction) {
            case "P":
                activateService(orderLineDTO, orderDTO);
                break;
            case "C":
                if ("TOS".equalsIgnoreCase(orderLineDTO.billActionQualifier)) {
                    suspendService(orderLineDTO, orderDTO);
                    break;
                }
                terminateService(orderLineDTO, orderDTO);
                break;
            case "CN":
                changeNumber(orderLineDTO, orderDTO);
                break;
            case "CU":
                changeUAN(orderLineDTO, orderDTO);
                break;
            case "COS":// Change
            case "CNS":
                changeService(orderLineDTO, orderDTO);
                break;
            case "TOS":// Transfer
            case "TNS":
                transferService(orderLineDTO, orderDTO);
                break;
            case "M": //
            case "MC":
                applyOneShotCharge(orderLineDTO, orderDTO);
                break;
            case "SN": // Swap
                swapNumber(orderLineDTO, orderDTO);
                break;
            case "CH": // Change COS
                changeCos(orderLineDTO, orderDTO);
                changeSLA(orderLineDTO);
                break;
            case "PR": // Remark bill item
                addRemark(orderLineDTO, orderDTO);
                break;
            }
        }
    }

    /**
     * Change cos.
     *
     * @param orderLineDTO the order line DTO
     * @param orderDTO the order DTO
     * @throws Exception the exception
     */
    private void changeCos(OrderLineDTO orderLineDTO, OrderDTO orderDTO) throws Exception {
        LOGGER.info("############### Changing COS .....");
        Subscription subscription = subscriptionService.findByCode(orderLineDTO.serviceId);
        if (subscription == null) {
            rejectOrder("The subscription " + orderLineDTO.serviceId + " is not found");
            return;
        }

        String classOfService = null;
        String af = null;
        String ef = null;
        final String cosString = "CLASS_OF_SERVICE";
        final String afString = "AF";
        final String efString = "EF";
        // get COS attributes for current order line
        for (AttributeDTO attributeDTO : orderLineDTO.attributes) {
            if (cosString.equalsIgnoreCase(attributeDTO.code))
                classOfService = attributeDTO.value;
            if (afString.equalsIgnoreCase(attributeDTO.code))
                af = attributeDTO.value;
            if (efString.equalsIgnoreCase(attributeDTO.code))
                ef = attributeDTO.value;
        }

        // updating Class of service, EF and AF
        String oldCos = null, oldAF = null, oldEF = null;
        String oldCharge = null, newCharge = null;
        for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
            if (serviceInstance.getStatus().equals(InstanceStatusEnum.ACTIVE) && orderLineDTO.billCode.equalsIgnoreCase(serviceInstance.getCode())) {
                Map<String, String> cf = (Map<String, String>) serviceInstance.getCfValuesNullSafe().getValue("CF_BILL_ATTRIBUTE");
                Map<String, String> oldCf = (Map<String, String>) serviceInstance.getCfValuesNullSafe().getValue("CF_OLD_BILL_ATTRIBUTE");
                if (oldCf == null) {
                    oldCf = new HashMap<>();
                }
                if (cf == null) {
                    cf = new HashMap<>();
                }
                String timestamp = getDate(new Date());
                String oldEndDate = DateUtils.formatDateWithPattern(addSecondsToDate(getDate(orderLineDTO.billEffectDate), -1), "dd/MM/yyyy HH:mm:ss");
                if (classOfService != null) {
                    if (cf.get(cosString) != null) {
                        oldCos = cf.get(cosString).split(Pattern.quote("|"))[0];
                        String oldEffectDate = cf.get(cosString).split(Pattern.quote("|"))[2];
                        oldCf.put(timestamp + "|" + cosString, oldCos + "|" + oldEffectDate + "|" + oldEndDate + "|" + null);
                    }
                    cf.put(cosString, classOfService + "|" + null + "|" + orderLineDTO.billEffectDate);
                }

                if (af != null) {
                    if (cf.get(afString) != null) {
                        oldAF = cf.get(afString).split(Pattern.quote("|"))[0];
                        String oldEffectDate = cf.get(afString).split(Pattern.quote("|"))[2];
                        oldCf.put(timestamp + "|" + afString, oldAF + "|" + oldEffectDate + "|" + oldEndDate + "|" + null);
                    }
                    cf.put(afString, af + "|" + null + "|" + orderLineDTO.billEffectDate);
                }

                if (ef != null) {
                    if (cf.get(efString) != null) {
                        oldEF = cf.get(efString).split(Pattern.quote("|"))[0];
                        String oldEffectDate = cf.get(efString).split(Pattern.quote("|"))[2];
                        oldCf.put(timestamp + "|" + efString, oldEF + "|" + oldEffectDate + "|" + oldEndDate + "|" + null);
                    }
                    cf.put(efString, ef + "|" + null + "|" + orderLineDTO.billEffectDate);
                }

                serviceInstance.getCfValuesNullSafe().setValue("CF_OLD_BILL_ATTRIBUTE", oldCf);
                serviceInstance.getCfValuesNullSafe().setValue("CF_BILL_ATTRIBUTE", cf);
                serviceInstanceService.update(serviceInstance);

                OfferTemplate offerTemplate = offerTemplateService.findByCode(orderLineDTO.offerName);
                if (offerTemplate != null) {
                    Map<String, String> cosTarifTable = (Map<String, String>) offerTemplate.getCfValuesNullSafe().getValue("CLASS_OF_SERVICE",
                        getDate(orderLineDTO.billEffectDate));
                    if (cosTarifTable != null) {
                        if (oldCos != null && oldAF != null && oldEF != null) {
                            oldCharge = cosTarifTable.get(orderLineDTO.billCode + "|" + oldCos + "|" + oldAF + "|" + oldEF);
                        }
                        newCharge = cosTarifTable.get(orderLineDTO.billCode + "|" + classOfService + "|" + af + "|" + ef);
                        LOGGER.info("####### Old charge " + oldCharge);
                        LOGGER.info("####### new charge " + newCharge);
                        if (oldCharge == newCharge) { // charges do not need to be (re)calculated. No action is
                            // necessary
                            return;
                        }
                        OneShotChargeTemplate chargeTemplate = chargeTemplateService.findByCode(MISC_CHARGE_TEMPLATE_CODE);
                        if (chargeTemplate == null) {
                            rejectOrder("The one-shot charge " + orderLineDTO.billCode + " is not found");
                            return;
                        }
                        if (oldCharge != null) {
                            oneShotChargeInstanceService.oneShotChargeApplication(subscription, chargeTemplate, null, getDate(orderLineDTO.billEffectDate),
                                new BigDecimal(oldCharge), null, null, null, null, null, orderLineDTO.operatorOrderNumber, true);
                            LOGGER.info("Old charge applied!");
                        }
                        if (newCharge != null) {
                            oneShotChargeInstanceService.oneShotChargeApplication(subscription, chargeTemplate, null, getDate(orderLineDTO.billEffectDate),
                                new BigDecimal(newCharge), null, null, null, null, null, orderLineDTO.operatorOrderNumber, true);
                            LOGGER.info("New charge applied!");
                        }

                    }
                }

            }
        }

    }

    /**
     * Get formatted date from XML Order
     *
     * @param date date attribute
     * @return Formatted date
     * @throws ParseException Parsing date exception
     */
    private Date getDate(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        return format.parse(date);
    }

    private Date getFormattedDate(String date) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.parse(date);
    }

    /**
     * Change UAN
     *
     * @param orderLineDTO Order Line
     * @param orderDTO Current Order
     * @throws Exception App Exception
     */
    private void changeUAN(OrderLineDTO orderLineDTO, OrderDTO orderDTO) throws Exception {
        Subscription subscription = subscriptionService.findByCode(orderLineDTO.serviceId);
        if (subscription != null) {

            Date billEffectDate = DateUtils.parseDateWithPattern(orderLineDTO.billEffectDate, DATE_TIME_PATTERN);

            if (subscription.getCfValuesNullSafe() != null) {
                Map<String, List<CustomFieldValue>> valuesByCode = subscription.getCfValuesNullSafe().getValuesByCode();
                if (valuesByCode != null && valuesByCode.get("UAN") != null) {
                    List<CustomFieldValue> customFieldValueList = valuesByCode.get("UAN");
                    for (CustomFieldValue customFieldValue : customFieldValueList) {
                        if (customFieldValue != null && customFieldValue.getPeriod() != null && customFieldValue.getPeriod().getTo() == null) {
                            customFieldValue.getPeriod().setTo(addSecondsToDate(billEffectDate, -1));
                        }
                    }
                }
            }

            DatePeriod datePeriod = new DatePeriod(billEffectDate, null);
            subscription.getCfValuesNullSafe().setValue("UAN", datePeriod, null, orderLineDTO.uan);
            subscriptionService.update(subscription);
        }
    }

    /**
     * Getting service instance from bill code
     *
     * @param sub subscription
     * @param billCode bill code
     * @return service instance
     */
    private ServiceInstance getServiceInstance(Subscription sub, String billCode) {
        for (ServiceInstance serviceInstance : sub.getServiceInstances()) {
            if (serviceInstance.getStatus().equals(InstanceStatusEnum.ACTIVE) && serviceInstance.getCode().equalsIgnoreCase(billCode)) {
                return serviceInstance;
            }
        }
        return null;
    }

    /**
     * Change SLA
     *
     * @param orderLineDTO Order Line
     * @param ServiceInstance Service Instance
     * @throws Exception App Exception
     */
    private void changeSLA(OrderLineDTO orderLineDTO) throws Exception {
        LOGGER.info("oooooooooooooooooooo Changing SLA oooooooooooooooooooooooooooooo");
        LOGGER.info("oo 1- getting subscription " + orderLineDTO.serviceId);
        Subscription subscription = subscriptionService.findByCode(orderLineDTO.serviceId);
        if (subscription == null) {
            rejectOrder("The subscription " + orderLineDTO.serviceId + " is not found");
            return;
        }
        LOGGER.info("oo 2- getting service instance " + orderLineDTO.billCode);
        ServiceInstance serviceInstance = getServiceInstance(subscription, orderLineDTO.billCode);
        if (serviceInstance == null) {
            return;
        }
        LOGGER.info("oo 3- updating SLA History ");
        Map<String, List<CustomFieldValue>> valuesByCode = serviceInstance.getCfValuesNullSafe().getValuesByCode();
        if (valuesByCode != null && valuesByCode.get("SLA_HISTORY") != null) {
            List<CustomFieldValue> customFieldValueList = valuesByCode.get("SLA_HISTORY");
            for (CustomFieldValue customFieldValue : customFieldValueList) {
                if (customFieldValue != null && customFieldValue.getPeriod() != null && customFieldValue.getPeriod().getTo() == null) {
                    customFieldValue.getPeriod().setTo(addSecondsToDate(getDate(orderLineDTO.billEffectDate), -1));
                }
            }
        }
        serviceInstance.getCfValuesNullSafe().setValue("SLA_HISTORY", new DatePeriod(getDate(orderLineDTO.billEffectDate), null), null, orderLineDTO.getAttributeValue("SLA"));

        LOGGER.info("oo 4- updating SLA CF Value ");
        serviceInstance.getCfValuesNullSafe().setValue("SLA", orderLineDTO.getAttributeValue("SLA"));

        serviceInstanceService.update(serviceInstance);
        LOGGER.info("oooooooooooooooooooooooooooooooooooooooooooooooooooooooooo");
    }

    /**
     * Change Number
     *
     * @param orderLineDTO Order line
     * @param orderDTO Current Order
     * @throws Exception App Exception
     */
    private void changeNumber(OrderLineDTO orderLineDTO, OrderDTO orderDTO) throws Exception {

        if ("ALL".equalsIgnoreCase(orderLineDTO.billCode)) {
            LOGGER.info("Changing Tel Number ==> ALL ...");

            if (orderLineDTO.refServiceId == null || "".equals(orderLineDTO.refServiceId)) {
                orderRejected = true;
                orderStatus = "Missing Ref_Service_Id";
                return;
            }
            List<Subscription> refSubscriptions = findSubscriptionsByServiceIdLike(orderLineDTO.refServiceId);
            if (refSubscriptions != null && !refSubscriptions.isEmpty()) {
                orderRejected = true;
                orderStatus = "refServiceId is already assigned to the subscription " + refSubscriptions.get(0).getCode();
                return;
            }
            List<Subscription> subscriptionsToModify = findSubscriptionsByServiceIdLike(orderLineDTO.originalServiceId);
            if (subscriptionsToModify != null) {
                for (Subscription subscriptionToModify : subscriptionsToModify) {
                    if (subscriptionToModify.getStatus().equals(SubscriptionStatusEnum.RESILIATED) || subscriptionToModify.getStatus().equals(SubscriptionStatusEnum.CANCELED)) {
                        continue;
                    }
                    Map<String, Object> cfMap = new HashMap<>();
                    cfMap.put("CHANGE_TEL", DateUtils.formatDateWithPattern(new Date(), "dd/MM/yyyy HH:mm:ss") + "|" + orderLineDTO.billEffectDate + "|"
                            + subscriptionToModify.getCode() + "|" + subscriptionToModify.getCode().replace(orderLineDTO.serviceId, orderLineDTO.refServiceId) + "|" + "DONE");
                    DatePeriod datePeriod = new DatePeriod(getDate(orderLineDTO.billEffectDate), null);
                    subscriptionToModify.getCfValuesNullSafe().setValue("CHANGE_TEL", datePeriod, 0, cfMap);
                    subscriptionToModify.setCode(subscriptionToModify.getCode().replace(orderLineDTO.serviceId, orderLineDTO.refServiceId));
                    // Update user account
                    UserAccount userAccount = subscriptionToModify.getUserAccount();
                    userAccount.setCode(userAccount.getCode().replace(orderLineDTO.serviceId, orderLineDTO.refServiceId));
                    userAccountService.update(userAccount);
                    List<Access> newAccessPoints = new ArrayList<>();
                    for (Access accessPoint : subscriptionToModify.getAccessPoints()) {
                        if (accessPoint.getAccessUserId().endsWith(orderLineDTO.serviceId)) {
                            // accessPoint
                            // .setAccessUserId(accessPoint.getAccessUserId().replace(oldTelNumber, newTelNumber));
                            accessPoint.setEndDate(DateUtils.setDateToEndOfDay(DateUtils.addDaysToDate(getDate(orderLineDTO.billEffectDate), -1)));
                            accessService.update(accessPoint);
                            // add access point
                            Access newAccessPoint = new Access();
                            newAccessPoint.setAccessUserId(accessPoint.getAccessUserId().replace(orderLineDTO.serviceId, orderLineDTO.refServiceId));
                            newAccessPoint.setStartDate(DateUtils.setDateToStartOfDay(getDate(orderLineDTO.billEffectDate)));
                            newAccessPoint.setSubscription(subscriptionToModify);
                            newAccessPoints.add(newAccessPoint);
                        }
                    }
                    subscriptionToModify.getAccessPoints().addAll(newAccessPoints);
                    subscriptionService.update(subscriptionToModify);
                }
            }
        } else if (AncillaryServiceEnum.MSN.name().equalsIgnoreCase(orderLineDTO.billCode) || orderLineDTO.billCode.startsWith(AncillaryServiceEnum.DDI.name())) { // look in
            // ancillary
            // services MSN &
            // DDIXX

            String rangeStart = null;
            String rangeEnd = null;
            String newRangeStart = null;
            String newRangeEnd = null;

            Map<String, String> attributes = getRangesAttributes(orderLineDTO);
            if (attributes != null) {
                rangeStart = attributes.get("RANGE_START");
                rangeEnd = attributes.get("RANGE_END");
                newRangeStart = attributes.get("NEW_RANGE_START");
                newRangeEnd = attributes.get("NEW_RANGE_END");

                if (!isValidRanges(newRangeStart, newRangeEnd, orderLineDTO.originalServiceId)) {
                    return;
                }
            }
            // updating ranges for adequate Service_Ids
            boolean serviceFound = false;
            List<Subscription> subscriptions = subscriptionService.findByCodeLike(orderLineDTO.serviceId);
            for (Subscription subscription : subscriptions) {
                for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
                    if (serviceInstance.getStatus().equals(InstanceStatusEnum.ACTIVE) && serviceInstance.getCode().equalsIgnoreCase(orderLineDTO.billCode)) {
                        if (serviceInstance.getCfValuesNullSafe().getValue("CF_BILL_ATTRIBUTE") != null) {
                            Map<String, String> cf = (Map<String, String>) serviceInstance.getCfValuesNullSafe().getValue("CF_BILL_ATTRIBUTE");
                            String[] splitedCF = cf.get("RANGE_START").split(Pattern.quote("|"));
                            String startDate = splitedCF.length > 2 ? splitedCF[2] : null;
                            String oldEndDate = DateUtils.formatDateWithPattern(addSecondsToDate(getDate(orderLineDTO.billEffectDate), -1), "dd/MM/yyyy HH:mm:ss");
                            String unit = splitedCF.length > 1 ? splitedCF[1] : null;
                            if (cf.get("RANGE_START") != null && cf.get("RANGE_END") != null && cf.get("RANGE_START").split("\\|")[0].equals(rangeStart)
                                    && cf.get("RANGE_END").split("\\|")[0].equals(rangeEnd)) {
                                Map<String, String> oldCf = (Map<String, String>) serviceInstance.getCfValuesNullSafe().getValue("CF_OLD_BILL_ATTRIBUTE");
                                if (oldCf == null)
                                    oldCf = new HashMap<>();
                                String timestamp = getDate(new Date());
                                oldCf.put(timestamp + "|RANGE_START", rangeStart + "|" + startDate + "|" + oldEndDate + "|" + unit);
                                oldCf.put(timestamp + "|RANGE_END", rangeEnd + "|" + startDate + "|" + oldEndDate + "|" + unit);

                                cf.put("RANGE_START", newRangeStart + "|" + unit + "|" + orderLineDTO.billEffectDate);
                                cf.put("RANGE_END", newRangeEnd + "|" + unit + "|" + orderLineDTO.billEffectDate);
                                serviceInstance.getCfValuesNullSafe().setValue("CF_OLD_BILL_ATTRIBUTE", oldCf);
                                serviceInstanceService.update(serviceInstance);
                                serviceFound = true;
                                break;
                            } else if (cf.get("RANGE_START") != null && cf.get("RANGE_END") == null && cf.get("RANGE_START").split("\\|")[0].equals(rangeStart)) {
                                Map<String, String> oldCf = (Map<String, String>) serviceInstance.getCfValuesNullSafe().getValue("CF_OLD_BILL_ATTRIBUTE");
                                if (oldCf == null)
                                    oldCf = new HashMap<>();
                                String timestamp = getDate(new Date());
                                // value|startDate|EndDate|unit
                                oldCf.put(timestamp + "|RANGE_START", rangeStart + "|" + startDate + "|" + oldEndDate + "|" + unit);
                                cf.put("RANGE_START", newRangeStart + "|" + unit + "|" + orderLineDTO.billEffectDate);
                                serviceInstance.getCfValuesNullSafe().setValue("CF_OLD_BILL_ATTRIBUTE", oldCf);
                                serviceInstanceService.update(serviceInstance);
                                serviceFound = true;
                                break;
                            }
                        }
                    }
                }
            }
            if (serviceFound == false) {
                rejectOrder("Ancillary service with right Range Numbers not found");
                return;
            }
        } else {
            rejectOrder("invalid bill code for Action CN");
            return;
        }
    }

    public static Date addSecondsToDate(Date date, Integer seconds) {
        Date result = null;

        if (date != null) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.SECOND, seconds);
            result = calendar.getTime();
        }

        return result;
    }

    /**
     * Swap Number
     *
     * @param orderLineDTO Order line
     * @param orderDTO Current Order
     * @throws Exception App Exception
     */
    private void swapNumber(OrderLineDTO orderLineDTO, OrderDTO orderDTO) throws Exception {
        if ("ALL".equalsIgnoreCase(orderLineDTO.billCode)) {
            LOGGER.info("Swapping Tel Number ==> ALL ...");
            if (orderLineDTO.refServiceId == null || "".equals(orderLineDTO.refServiceId)) {
                rejectOrder("Missing Ref_Service_Id");
                return;
            }
            List<Subscription> refSubscriptionsToSwap = findSubscriptionsByServiceIdLike(orderLineDTO.refServiceId);
            LOGGER.info("####refSubscriptionsToSwap" + refSubscriptionsToSwap);

            if (refSubscriptionsToSwap != null) {
                for (Subscription refSubscriptionToSwap : refSubscriptionsToSwap) {
                    if (refSubscriptionToSwap.getStatus() != SubscriptionStatusEnum.ACTIVE) {
                        rejectOrder("The subscription" + refSubscriptionToSwap.getCode() + "is not active");
                        return;
                    }

                }
            }

            if (refSubscriptionsToSwap == null || refSubscriptionsToSwap.isEmpty()) {
                rejectOrder("Any subscription is found for " + orderLineDTO.refServiceId);
                return;
            }

            List<Subscription> subscriptionsToSwap = findSubscriptionsByServiceIdLike(orderLineDTO.originalServiceId);
            LOGGER.info("####subscriptionsToSwap" + subscriptionsToSwap);

            if (subscriptionsToSwap != null) {
                for (Subscription subscriptionToSwap : subscriptionsToSwap) {
                    if (subscriptionToSwap.getStatus() != SubscriptionStatusEnum.ACTIVE) {
                        rejectOrder("The subscription" + subscriptionToSwap.getCode() + "is not active");
                        return;
                    }
                }

            }

            if (refSubscriptionsToSwap != null) {
                for (Subscription refSubscriptionToSwap : refSubscriptionsToSwap) {

                    String refSubscriptionToSwapCode = refSubscriptionToSwap.getCode();
                    String interTelNumber = "OLD" + refSubscriptionToSwapCode;
                    refSubscriptionToSwap.setCode(interTelNumber);
                    UserAccount userAccount = refSubscriptionToSwap.getUserAccount();
                    String interUA = "OLD" + userAccount.getCode();
                    userAccount.setCode(interUA);
                    userAccountService.update(userAccount);
                    subscriptionService.update(refSubscriptionToSwap);

                }
            }

            if (subscriptionsToSwap != null) {
                for (Subscription subscriptionToSwap : subscriptionsToSwap) {

                    String subscriptionToSwapCode = subscriptionToSwap.getCode();

                    Map<String, Object> cfMap = new HashMap<>();
                    cfMap.put("SWAP_TEL", DateUtils.formatDateWithPattern(new Date(), "dd/MM/yyyy HH:mm:ss") + "|" + orderLineDTO.billEffectDate + "|" + subscriptionToSwapCode
                            + "|" + subscriptionToSwap.getCode().replace(orderLineDTO.serviceId, orderLineDTO.refServiceId) + "|" + "DONE");

                    DatePeriod datePeriod = new DatePeriod(getDate(orderLineDTO.billEffectDate), null);
                    subscriptionToSwap.getCfValuesNullSafe().setValue("SWAP_TEL", datePeriod, 0, cfMap);

                    // swap user account
                    UserAccount userAccount = subscriptionToSwap.getUserAccount();
                    userAccount.setCode(userAccount.getCode().replace(orderLineDTO.serviceId, orderLineDTO.refServiceId));
                    userAccountService.update(userAccount);
                    subscriptionToSwap.setUserAccount(userAccount);
                    subscriptionService.update(subscriptionToSwap);

                    subscriptionToSwap.setCode(subscriptionToSwap.getCode().replace(orderLineDTO.serviceId, orderLineDTO.refServiceId));
                    subscriptionService.update(subscriptionToSwap);

                    List<Access> newAccessPoints = new ArrayList<>();
                    for (Access accessPoint : subscriptionToSwap.getAccessPoints()) {
                        if (accessPoint.getAccessUserId().endsWith(orderLineDTO.serviceId)) {
                            accessPoint.setEndDate(DateUtils.setDateToEndOfDay(DateUtils.addDaysToDate(getDate(orderLineDTO.billEffectDate), -1)));
                            accessService.update(accessPoint);
                        }
                    }
                    // add access point

                    Access newAccessPoint = new Access();
                    newAccessPoint.setAccessUserId(subscriptionToSwap.getCode().replace(orderLineDTO.serviceId, orderLineDTO.refServiceId));
                    newAccessPoint.setStartDate(DateUtils.setDateToStartOfDay(getDate(orderLineDTO.billEffectDate)));
                    newAccessPoint.setSubscription(subscriptionToSwap);
                    newAccessPoints.add(newAccessPoint);

                    subscriptionToSwap.getAccessPoints().addAll(newAccessPoints);
                    subscriptionService.update(subscriptionToSwap);

                }

            }

            if (refSubscriptionsToSwap != null) {
                for (Subscription refSubscriptionToSwap : refSubscriptionsToSwap) {

                    String refSubscriptionToSwapCode = refSubscriptionToSwap.getCode();

                    UserAccount userAccount = refSubscriptionToSwap.getUserAccount();
                    userAccount.setCode(userAccount.getCode().replace(orderLineDTO.refServiceId, orderLineDTO.serviceId));
                    userAccount.setCode(userAccount.getCode().replace("OLD", ""));
                    userAccountService.update(userAccount);

                    refSubscriptionToSwap.setCode(refSubscriptionToSwap.getCode().replace(orderLineDTO.refServiceId, orderLineDTO.serviceId));
                    refSubscriptionToSwap.setCode(refSubscriptionToSwap.getCode().replace("OLD", ""));
                    subscriptionService.update(refSubscriptionToSwap);

                    Map<String, Object> cfMap = new HashMap<>();

                    cfMap.put("SWAP_TEL",
                        DateUtils.formatDateWithPattern(new Date(), "dd/MM/yyyy HH:mm:ss") + "|" + orderLineDTO.billEffectDate + "|"
                                + refSubscriptionToSwap.getCode().replace(orderLineDTO.serviceId, orderLineDTO.refServiceId) + "|"
                                + refSubscriptionToSwap.getCode().replace(orderLineDTO.refServiceId, orderLineDTO.serviceId) + "|" + "DONE");

                    DatePeriod datePeriod = new DatePeriod(getDate(orderLineDTO.billEffectDate), null);
                    refSubscriptionToSwap.getCfValuesNullSafe().setValue("SWAP_TEL", datePeriod, 0, cfMap);

                    List<Access> newAccessPoints = new ArrayList<>();
                    for (Access accessPoint : refSubscriptionToSwap.getAccessPoints()) {
                        if (accessPoint.getAccessUserId().endsWith(orderLineDTO.refServiceId)) {
                            accessPoint.setEndDate(DateUtils.setDateToEndOfDay(DateUtils.addDaysToDate(getDate(orderLineDTO.billEffectDate), -1)));
                            accessService.update(accessPoint);
                        }
                    }
                    // add access point

                    Access newAccessPoint = new Access();
                    newAccessPoint.setAccessUserId(refSubscriptionToSwap.getCode().replace(orderLineDTO.refServiceId, orderLineDTO.serviceId));
                    newAccessPoint.setStartDate(DateUtils.setDateToStartOfDay(getDate(orderLineDTO.billEffectDate)));
                    newAccessPoint.setSubscription(refSubscriptionToSwap);
                    newAccessPoints.add(newAccessPoint);

                    refSubscriptionToSwap.getAccessPoints().addAll(newAccessPoints);
                    subscriptionService.update(refSubscriptionToSwap);

                }

            }

        }
    }

    /**
     * Convert Date Object to a Date String
     *
     * @param date
     * @return Parsed Date
     */
    public String getDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        return format.format(date);
    }

    public String getFormattedDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(date);
    }

    /**
     * Checks if is held order.
     *
     * @param orderDto the order dto
     * @return true, if is held order
     */
    private boolean isHeldOrder(OrderDTO orderDto) {
        List<Order> orders;
        for (OrderLineDTO orderLineDTO : orderDto.orderLines) {
            orders = findPendingRejectedWaitingOrders(orderDto.orderId, orderLineDTO.originalServiceId);
            if (orders != null && !orders.isEmpty()) {
                orderDto.status = OrderStatusEnum.HELD;
                orderStatus = "The service Id " + orderLineDTO.serviceId + " is already exists against an order that is in Error Management : " + orders.get(0);
                return true;
            }
        }
        return false;
    }

    @SuppressWarnings("unchecked")
    public List<Order> findPendingRejectedWaitingOrders(String orderId, String serviceId) throws BusinessException {
        String query = "SELECT code FROM ord_order where code <> '" + orderId + "' AND status in ('PENDING','REJECTED','WAITING')" + " AND cf_values like '%<Service_Id>"
                + serviceId + "</Service_Id>%'";
        return orderService.getEntityManager().createNativeQuery(query).getResultList();
    }

    /**
     * Find subscription by service id like.
     *
     * @param serviceId the service id
     * @return the list
     */
    @SuppressWarnings("unchecked")
    private List<Subscription> findSubscriptionsByServiceIdLike(String serviceId) {
        QueryBuilder queryBuilder = new QueryBuilder(Subscription.class, "a", null);
        if (IEnable.class.isAssignableFrom(Subscription.class)) {
            queryBuilder.addBooleanCriterion("disabled", false);
        }
        queryBuilder.addCriterion("code", "like", "%" + serviceId, true);
        return queryBuilder.getQuery(subscriptionService.getEntityManager()).getResultList();
    }

    /**
     * applying oneShot charge
     *
     * @param orderLineDTO order line
     * @param orderDTO The order
     * @throws Exception App Exception
     */
    private void applyOneShotCharge(OrderLineDTO orderLineDTO, OrderDTO orderDTO) throws Exception {
        LOGGER.info("Applying oneShot charge...");

        String subscriber = orderLineDTO.serviceId;

        // MISC Order using the 00 Range
        if (orderLineDTO.serviceId.contains("00-")) {
            subscriber = "MISC_" + orderLineDTO.masterAccountNumber;
        }
        LOGGER.info("Subscription to be treated : " + subscriber);

        Subscription subscription = subscriptionService.findByCode(subscriber);
        if (subscription == null) {
            rejectOrder("The subscription " + subscriber + " is not found");
            return;
        }

        if (StringUtils.isBlank(orderLineDTO.billCode)) {
            rejectOrder("The bill code is required");
            return;
        }

        applyMiscOneShotCharge(orderLineDTO, orderDTO, subscription);

        LOGGER.info("End applying oneShot charge.");
    }

    /**
     * apply MiscOneShotCharge
     *
     * @param orderLineDTO order line
     * @param orderDTO The order
     * @param subscription The subscription
     * @throws Exception The exception
     */
    private void applyMiscOneShotCharge(OrderLineDTO orderLineDTO, OrderDTO orderDTO, Subscription subscription) throws Exception {
        LOGGER.info("applyMiscOneShotCharge...");
        OfferTemplate offerTemplate = offerTemplateService.findByCode(MISC_OFFER_TEMPLATE_CODE);
        if (offerTemplate == null) {
            rejectOrder("The offer template MISC_WB is not found");
            return;
        }

        Map<String, String> tariffTable = getCatalog(orderLineDTO, offerTemplate);
        if (tariffTable == null) {
            rejectOrder("Tariffs are not found for date " + orderLineDTO.billEffectDate);
            return;
        }

        String billCodeInfo = tariffTable.get(orderLineDTO.billCode);
        if (StringUtils.isBlank(billCodeInfo)) {
            rejectOrder("The service " + orderLineDTO.billCode + " is not found");
            return;
        }

        OneShotChargeTemplate chargetemplate = chargeTemplateService.findByCode(MISC_CHARGE_TEMPLATE_CODE);
        if (chargetemplate == null) {
            rejectOrder("The one-shot charge " + orderLineDTO.billCode + " is not found");
            return;
        }

        String chargeDescription = billCodeInfo.split("\\|")[0];
        BigDecimal chargePrice = getOneShotChargePrice(orderLineDTO, billCodeInfo);
        if (chargePrice == null) {
            return;
        }

        OneShotChargeInstance oneShotChargeInstance = oneShotChargeInstanceService.oneShotChargeApplication(subscription, chargetemplate, null,
            getDate(orderLineDTO.billEffectDate), chargePrice, null, null, null, null, null, orderLineDTO.operatorOrderNumber, true);

        // TODO : to force the subscription to active status (check this point with product owner)
        if (subscription.getStatus() == SubscriptionStatusEnum.CREATED) {
            subscription.setStatus(SubscriptionStatusEnum.ACTIVE);
        }

        oneShotChargeInstance.setCode(orderLineDTO.billCode);
        if (!StringUtils.isBlank(chargeDescription)) {
            oneShotChargeInstance.setDescription(chargeDescription);
        }
        oneShotChargeInstanceService.update(oneShotChargeInstance);
    }

    /**
     * get one-Shot charge price
     *
     * @param orderLineDTO order line
     * @param billCodeInfo The bill code info
     * @return the one-Shot charge price
     */
    private BigDecimal getOneShotChargePrice(OrderLineDTO orderLineDTO, String billCodeInfo) throws Exception {

        if ("MC".equalsIgnoreCase(orderLineDTO.billAction)) {
            boolean isChargeFound = false;
            Double chargePrice = null;
            for (AttributeDTO attributeDTO : orderLineDTO.attributes) {
                if (attributeDTO.code.equalsIgnoreCase("Charge")) {
                    isChargeFound = true;
                    try {
                        chargePrice = new Double(attributeDTO.value);
                    } catch (Exception ex) {
                        chargePrice = null;
                    }
                    break;
                }
            }
            if (!isChargeFound) {
                rejectOrder("No Charge Attribute found");
                return null;
            }
            if (chargePrice == null) {
                rejectOrder("No price found for a misc charge");
                return null;
            }
            return new BigDecimal(chargePrice);
        } else {
            String exchangeClass = getExchangeClass(orderLineDTO);
            Double oneShotTariff = (Double) getTariffInfo(orderLineDTO, "O", exchangeClass).get(1);
            return new BigDecimal(oneShotTariff);
        }
    }

    private Map<String, String> getNewTariff(Customer customer, Date subscriptionDate, OfferTemplate offerTemplate) {
        // check STANDARD_CUSTOMER CF
        Map<String, String> tariffs = null;
        if (customer != null && "false".equals(customer.getCfValuesNullSafe().getValue("STANDARD_CUSTOMER").toString())) {
            // get Tariff from customer level
            tariffs = (Map<String, String>) customer.getCfValuesNullSafe().getValue("NEW_TARIFF");
        } else {
            // Standard
            tariffs = (Map<String, String>) offerTemplate.getCfValuesNullSafe().getValue("NEW_TARIFF");
        }
        return tariffs;
    }

    private List<Object> getTariffInfo(OrderLineDTO orderLineDTO, String chargeType, String exchangeClass) throws Exception {

        Date subscriptionDate = getDate(orderLineDTO.billEffectDate);
        String billCode = orderLineDTO.billCode;
        String actionQualifier = orderLineDTO.billActionQualifier;
        String density = orderLineDTO.getAttributeValue("DENSITY");

        String region = orderLineDTO.getAttributeValue("REGION");
        String sla = orderLineDTO.getAttributeValue("SLA");
        String classService = orderLineDTO.getAttributeValue("CLASS_SERVICE");
        String ef = orderLineDTO.getAttributeValue("EF");
        String af = orderLineDTO.getAttributeValue("AF");
        List<Object> result = new ArrayList<>();

        try {
            Customer customer = getCustomerByOperatorId(orderLineDTO.operatorId);
            OfferTemplate offer = offerTemplateService.findByCode(orderLineDTO.offerName);
            Map<String, String> tariffTable = getNewTariff(customer, getDate(orderLineDTO.billEffectDate), offer);
            StringBuilder tariffKey = new StringBuilder();
            tariffKey.append(billCode).append(SEPARATOR).append(chargeType).append(SEPARATOR).append(actionQualifier).append(SEPARATOR).append(density).append(SEPARATOR)
                .append(region).append(SEPARATOR).append(sla).append(SEPARATOR).append(classService).append(SEPARATOR).append(ef).append(SEPARATOR).append(af).append(SEPARATOR)
                .append(exchangeClass).append(SEPARATOR);
            String newTariffKey = tariffKey.toString().replaceAll(Pattern.quote("||"), "|null|").replaceAll(Pattern.quote("||"), "|null|");
            Date nearDate = new GregorianCalendar(1800, 01, 07).getTime();
            Map<String, String> newTariffTable = new HashMap<>();
            for (Entry<String, String> entry : tariffTable.entrySet()) {
                String key = entry.getKey().replaceAll(Pattern.quote("||"), "|null|");
                key = key.replaceAll(Pattern.quote("||"), "|null|");
                newTariffTable.put(key, entry.getValue());
                if (key.startsWith(newTariffKey)) {
                    Date startDate = getFormattedDate(key.replace(newTariffKey, ""));
                    if (startDate.equals(subscriptionDate)) {
                        nearDate = startDate;
                        break;
                    } else if (startDate.before(subscriptionDate) && startDate.after(nearDate)) {
                        nearDate = startDate;
                    }
                }
            }
            newTariffKey = newTariffKey + getFormattedDate(nearDate); 
            result.add(newTariffTable.get(newTariffKey).split(Pattern.quote(SEPARATOR))[1]);
            result.add(new Double(newTariffTable.get(newTariffKey).split(Pattern.quote(SEPARATOR))[2]));
            result.add(newTariffTable.get(newTariffKey).split(Pattern.quote(SEPARATOR))[3]);
            return result;
        } catch (Exception ex) {
            LOGGER.info("Tariff not found for " + billCode + " with charge type " + chargeType + " in date " + orderLineDTO.billEffectDate);
            return null;
        }

    }

    /**
     * Transfer service.
     *
     * @param orderLineDTO the order line DTO
     * @param orderDTO the order DTO
     * @throws Exception the exception
     */
    private void transferService(OrderLineDTO orderLineDTO, OrderDTO orderDTO) throws Exception {
        LOGGER.info("transfering service..." + orderLineDTO.getDetails());
        if (orderRejected) {
            return;
        }
        if ("TOS".equalsIgnoreCase(orderLineDTO.billAction)) {
            Subscription subscription = subscriptionService.findByCode(orderLineDTO.serviceId);
            if (subscription == null) {
                rejectOrder("The subscription " + orderLineDTO.serviceId + " is not found");
            }
            if (subscription.getStatus().equals(SubscriptionStatusEnum.RESILIATED) || subscription.getStatus().equals(SubscriptionStatusEnum.CANCELED)) {
                rejectOrder("The Subscrition " + subscription.getCode() + " has status " + subscription.getStatus() + ", it can not be transfered");
                return;
            }
            SubscriptionTerminationReason terminationReason = subscriptionTerminationReasonService.findByCodeReason(TERMINATION_REASON_NONE);
            for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
                if (orderLineDTO.billCode.equals(serviceInstance.getCode())) {
                    // Min Term charges and cease charge should NOT been calculated
                    LOGGER.info("Transfer - terminating old Main service : " + serviceInstance.getCode());
                    // Saving reference to Service Id in ceased service
                    if (orderLineDTO.xRefServiceId != null) {
                        serviceInstance.getCfValuesNullSafe().setValue("XREF_SERVICE_ID", orderLineDTO.xRefServiceId);
                    }
                    if (!serviceInstance.getStatus().equals(InstanceStatusEnum.TERMINATED)) {
                        serviceInstanceService.terminateService(serviceInstance, getDate(orderLineDTO.billEffectDate), terminationReason, orderLineDTO.operatorOrderNumber);
                    }
                    LOGGER.info("Transfer - Main service ceased : " + serviceInstance.getCode());
                    break;
                }
            }

            // Add OLD_ prefix to subscription code in order to can create the new one with same code (for transfer)
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
            String oldPrefix = "OLD" + sdf.format(new Date().getTime());
            UserAccount userAccount = userAccountService.findByCode(orderLineDTO.serviceId);
            userAccountService.userAccountTermination(userAccount, getDate(orderLineDTO.billEffectDate), terminationReason);
            userAccount.setCode(oldPrefix + "_" + userAccount.getCode());
            userAccountService.update(userAccount);
            LOGGER.info("Transfer - Subscription terminated : " + subscription.getCode());

            subscription.setCode(oldPrefix + "_" + subscription.getCode());
            subscriptionService.update(subscription);
        } else if ("TNS".equalsIgnoreCase(orderLineDTO.billAction)) {
            Subscription newSubscription = subscriptionService.findByCode(orderLineDTO.serviceId);
            UserAccount userAccount = userAccountService.findByCode(orderLineDTO.serviceId);
            if (userAccount == null) {
                // create UserAccount from billing Account
                BillingAccount billingAccount = billingAccountService.findByCode(orderLineDTO.masterAccountNumber);
                if (billingAccount == null) {
                    rejectOrder("The billing account is not found");
                }
                userAccount = new UserAccount();
                userAccount.setCode(orderLineDTO.serviceId);
                userAccountService.createUserAccount(billingAccount, userAccount);
                createdUsersAccount.add(userAccount);
            }
            if (newSubscription != null) {
                rejectOrder("The subscription " + orderLineDTO.serviceId + " already exists");
                return;
            }
            activateService(orderLineDTO, orderDTO);
            LOGGER.info("Transfer - Main service activated : " + orderLineDTO.billCode);
        }
    }

    /**
     * Change Service from Order Line
     *
     * @param orderLineDTO Order Line
     * @param orderDTO Order
     * @throws Exception App Exception
     */
    private void changeService(OrderLineDTO orderLineDTO, OrderDTO orderDTO) throws Exception {
        LOGGER.info("Changing service...");
        Map<String, String> cf = (Map<String, String>) providerService.getProvider().getCfValuesNullSafe().getValue("MIGRATION_MATRIX");
        if (cf == null) {
            throw new Exception("Migration Matrix is not found");
        }

        // get migration code from change matrix
        String from = null;
        String to = null;

        String serviceId = orderLineDTO.serviceId;
        LOGGER.info("serviceId ****** " + serviceId);
        if (serviceId != null && serviceId.contains("_")) {
            String[] splitedServiceId = serviceId.split("_");
            serviceId = splitedServiceId.length > 1 ? splitedServiceId[1] : null;
        }

        if (serviceId != null && changeMap.get(serviceId) != null) {
            int size = changeMap.get(serviceId).size();
            from = size > 0 ? changeMap.get(serviceId).get(0) : null;
            to = size > 1 ? changeMap.get(serviceId).get(1) : null;
        }

        String migrationCode = null;

        if (from != null && to != null && !from.isEmpty() && !to.isEmpty()) {
            migrationCode = getProductMigrationRules(from, to, cf);
        }
        if (migrationCode == null) {
            migrationCode = "*|*|null";
        }

        LOGGER.info("#######migrationCode########" + migrationCode);
        String changeData = cf.get(migrationCode);

        if (changeData == null) {
            rejectOrder("Migration Matrix lacks CHANGE data");
            return;
            // throw new Exception("Migration Matrix lacks CHANGE data");
        }
        Boolean applyMinTermOnPrevious = ("N".equalsIgnoreCase(changeData.split("\\|")[1])) ? false : true;
        Boolean applyConnectionChargeOnNew = ("N".equalsIgnoreCase(changeData.split("\\|")[2])) ? false : true;
        Boolean resetEngagementOnNew = ("N".equalsIgnoreCase(changeData.split("\\|")[3])) ? false : true;

        int count = changeData.length() - changeData.replace("|", "").length();
        LOGGER.info("#####changeData######" + changeData);
        LOGGER.info("#####count######" + count);
        String serviceToApply = null;
        if (count > 3 && changeData.split(Pattern.quote("|")).length == 5) {
            serviceToApply = changeData.split(Pattern.quote("|"))[4];
        }

        LOGGER.info("applyMinTermOnPrevious:" + applyMinTermOnPrevious + "-applyConnectionChargeOnNew" + applyConnectionChargeOnNew + "-resetEngagementOnNew:"
                + resetEngagementOnNew + "-serviceToApply:" + serviceToApply);
        if ("COS".equalsIgnoreCase(orderLineDTO.billAction)) {
            Subscription subscription = subscriptionService.findByCode(orderLineDTO.serviceId);
            if (subscription == null) {
                return;
            }
            if (subscription.getStatus() == SubscriptionStatusEnum.RESILIATED || subscription.getStatus() == SubscriptionStatusEnum.CANCELED) {
                LOGGER.info("Subscription " + subscription.getCode() + " is already terminated");
                return;
            }
            for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
                if (orderLineDTO.billCode.equals(serviceInstance.getCode())) {
                    // Saving reference to Service Id in ceased service
                    if (orderLineDTO.xRefServiceId != null) {
                        serviceInstance.getCfValuesNullSafe().setValue("XREF_SERVICE_ID", orderLineDTO.xRefServiceId);
                    }
                    SubscriptionTerminationReason terminationReason = subscriptionTerminationReasonService.findByCodeReason(TERMINATION_REASON_MAIN);
                    if (applyMinTermOnPrevious == false) {
                        terminationReason = subscriptionTerminationReasonService.findByCodeReason(TERMINATION_REASON_TOS);
                    }
                    if (!InstanceStatusEnum.TERMINATED.equals(serviceInstance.getStatus())) {
                        serviceInstanceService.terminateService(serviceInstance, getDate(orderLineDTO.billEffectDate), terminationReason, orderLineDTO.operatorOrderNumber);
                        LOGGER.info("Change - Main service ceased : " + serviceInstance.getCode());
                        subscriptionService.terminateSubscription(subscription, getDate(orderLineDTO.billEffectDate), terminationReason, orderLineDTO.operatorOrderNumber);
                        List<AccessDTO> accessDtoList = new ArrayList<>();
                        for (Access a : subscription.getAccessPoints()) {
                            AccessDTO access = new AccessDTO();
                            access.accessUserId = a.getAccessUserId();
                            access.endDate = a.getEndDate();
                            accessDtoList.add(access);
                        }
                        // Add OLD_ prefix to subscription code in order to can create the new one with same code
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
                        String oldPrefix = "OLD" + sdf.format(new Date().getTime());
                        UserAccount userAccount = userAccountService.findByCode(orderLineDTO.serviceId);
                        userAccountService.userAccountTermination(userAccount, getDate(orderLineDTO.billEffectDate), terminationReason);
                        userAccount.setCode(oldPrefix + "_" + userAccount.getCode());
                        userAccountService.update(userAccount);
                        LOGGER.info("Subscription terminated : " + subscription.getCode());

                        for (AccessDTO accessDto : accessDtoList) {
                            for (Access access : subscription.getAccessPoints()) {
                                if (access.getAccessUserId().equals(accessDto.accessUserId)) {
                                    if (accessDto.endDate != null) {
                                        access.setEndDate(accessDto.endDate);
                                        accessService.update(access);
                                    }
                                }
                            }
                        }

                        subscription.setCode(oldPrefix + "_" + subscription.getCode());
                        subscriptionService.update(subscription);
                        break;
                    }
                } else {
                    if (!InstanceStatusEnum.TERMINATED.equals(serviceInstance.getStatus())) {
                        serviceInstanceService.terminateService(serviceInstance, getDate(orderLineDTO.billEffectDate),
                            subscriptionTerminationReasonService.findByCodeReason(TERMINATION_REASON_ANCILLARY), orderLineDTO.operatorOrderNumber);
                    }
                }
            }
        } else if ("CNS".equalsIgnoreCase(orderLineDTO.billAction)) {
            Subscription subscription = subscriptionService.findByCode(orderLineDTO.serviceId);
            // check if we have to transfer subscription if different billing account
            if (subscription != null && !orderLineDTO.masterAccountNumber.equals(subscription.getUserAccount().getBillingAccount().getCode())) {
                List<AccessDTO> accessDtoList = new ArrayList<>();
                for (Access a : subscription.getAccessPoints()) {
                    AccessDTO access = new AccessDTO();
                    access.accessUserId = a.getAccessUserId();
                    access.endDate = a.getEndDate();
                    accessDtoList.add(access);
                }
                SubscriptionTerminationReason terminationReason = subscriptionTerminationReasonService.findByCodeReason(TERMINATION_REASON_NONE);
                // Add OLD_ prefix to subscription code in order to can create the new one with same code (for transfer)
                SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
                String oldPrefix = "OLD" + sdf.format(new Date().getTime());
                UserAccount userAccount = userAccountService.findByCode(orderLineDTO.serviceId);
                userAccountService.userAccountTermination(userAccount, getDate(orderLineDTO.billEffectDate), terminationReason);
                userAccount.setCode(oldPrefix + "_" + userAccount.getCode());
                userAccountService.update(userAccount);
                for (AccessDTO accessDto : accessDtoList) {
                    for (Access access : subscription.getAccessPoints()) {
                        if (access.getAccessUserId().equals(accessDto.accessUserId)) {
                            if (accessDto.endDate != null) {
                                access.setEndDate(accessDto.endDate);
                                accessService.update(access);
                            }
                        }
                    }
                }
                subscription.setCode(oldPrefix + "_" + subscription.getCode());
                subscriptionService.update(subscription);
            }
            activateService(orderLineDTO, orderDTO, applyConnectionChargeOnNew, resetEngagementOnNew, serviceToApply);
            LOGGER.info("Change - Main service activated : " + orderLineDTO.billCode);
        }
    }

    private String getBillCodeField(Map<String, String> catalog, OrderLineDTO orderLineDTO, Catalog catalogField) {
        if (catalog == null)
            return null;
        String billCodeInfo = getBillCodeInfo(catalog, orderLineDTO, null);
        if (billCodeInfo == null)
            return null;
        while (billCodeInfo.contains("||")) {
            billCodeInfo = billCodeInfo.replaceAll(Pattern.quote("||"), "\\| \\|");
        }
        String billCodeField = billCodeInfo.split(Pattern.quote("|"))[catalogField.getPosition()];
        return (billCodeField == null) ? billCodeField : billCodeField.trim();
    }

    /**
     * Get Bill Code Info
     *
     * @param tariffTable Catalog
     * @param orderLineDTO Order Line
     * @param chargeType Charge Type
     * @return Bill code info in catalog
     */
    private String getBillCodeInfo(Map<String, String> catalog, OrderLineDTO orderLineDTO, String chargeType) {
        String billCodeInfo = null;
        if (chargeType == null) {
            billCodeInfo = catalog.get(orderLineDTO.billCode + "|R");
            billCodeInfo = (billCodeInfo == null) ? catalog.get(orderLineDTO.billCode + "|O") : billCodeInfo;
        } else {
            billCodeInfo = catalog.get(orderLineDTO.billCode + "|" + chargeType);
        }
        return billCodeInfo;
    }

    /**
     * Terminate the service from Order Line
     *
     * @param orderLineDTO Order Line
     * @param orderDTO Order
     * @throws Exception App Exception
     */
    private void terminateService(OrderLineDTO orderLineDTO, OrderDTO orderDTO) throws Exception {
        LOGGER.info("Terminating service...");
        Boolean mainServiceFound = false;
        Map<String, String> catalog = null;
        String billCodeInfo = null;

        OfferTemplate offerTemplate = offerTemplateService.findByCode(orderLineDTO.offerName);
        if (offerTemplate != null) {
            catalog = getCatalog(orderLineDTO, offerTemplate);
        }
        if (catalog != null) {
            billCodeInfo = getBillCodeInfo(catalog, orderLineDTO, null);
        }
        // Check if service is not found
        if (billCodeInfo == null) {
            rejectOrder("The service " + orderLineDTO.billCode + " is not found");
            return;
        }

        // Check Service Type (Main, Event, Ancillary)
        Subscription subscription = subscriptionService.findByCode(orderLineDTO.serviceId);
        if (subscription == null || subscription.getStatus() == SubscriptionStatusEnum.RESILIATED) {
            dataDiscrepancies += "The subscription " + orderLineDTO.serviceId + " is not found or already ceased. \n";
            return;
        }

        List<AccessDTO> accessDtoList = new ArrayList<>();
        for (Access a : subscription.getAccessPoints()) {
            AccessDTO access = new AccessDTO();
            access.accessUserId = a.getAccessUserId();
            access.endDate = a.getEndDate();
            accessDtoList.add(access);
        }

        SubscriptionTerminationReason terminationReasonMain = subscriptionTerminationReasonService.findByCodeReason(TERMINATION_REASON_MAIN);
        SubscriptionTerminationReason terminationReasonAncillary = subscriptionTerminationReasonService.findByCodeReason(TERMINATION_REASON_ANCILLARY);

        String serviceType = getBillCodeField(catalog, orderLineDTO, Catalog.SERVICE_TYPE);

        // Set the service quantity
        BigDecimal quantity = BigDecimal.ZERO;
        quantity = setServiceQuantity(orderLineDTO, billCodeInfo, quantity);
        if (quantity == null) {
            return;
        }
        Date terminationDate = getDate(orderLineDTO.billEffectDate);
        if ("C".equalsIgnoreCase(orderLineDTO.billAction) && "FAILED_INSTALL".equals(orderLineDTO.billActionQualifier)) {
            // #140 to rebate the fully amount
            terminationDate = subscription.getSubscriptionDate();
            orderLineDTO.billEffectDate = DateUtils.formatDateWithPattern(subscription.getSubscriptionDate(), DATE_TIME_PATTERN);
        }
        if (ServiceTypeEnum.MAIN.getLabel().equalsIgnoreCase(serviceType)) { // if main service then terminate all services
            // check existence of main service
            for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
                if (serviceInstance != null) {
                    if (serviceInstance.getStatus().equals(InstanceStatusEnum.TERMINATED)) {
                        if (orderLineDTO.billCode.equals(serviceInstance.getCode())) {
                            dataDiscrepancies += "The subscriber service (bill code) being ceased should be active against the subscription. \n";
                            mainServiceFound = true;
                        }
                        continue;
                    }
                    if (orderLineDTO.billCode.equals(serviceInstance.getCode())) {
                        quantity = updateMainService(orderLineDTO, orderDTO, serviceInstance, billCodeInfo, quantity);
                        if (quantity == null) {
                            return;
                        }
                        mainServiceFound = true;
                    }
                }
            }
            if (mainServiceFound == false) {
                rejectOrder("The service " + orderLineDTO.billCode + " is not found in subscription");
                return;
            }
            for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
                if (serviceInstance.getStatus().equals(InstanceStatusEnum.TERMINATED)) {
                    continue;
                }
                if (serviceInstance.getCfValuesNullSafe().getValue("SERVICE_TYPE") != null) {
                    String currentServiceType = serviceInstance.getCfValuesNullSafe().getValue("SERVICE_TYPE").toString();
                    if (ServiceTypeEnum.MAIN.getLabel().equalsIgnoreCase(currentServiceType)) {
                        serviceInstanceService.terminateService(serviceInstance, terminationDate, terminationReasonMain, orderLineDTO.operatorOrderNumber);
                        LOGGER.info("Main service terminated : " + serviceInstance.getCode());
                    } else {
                        // Active ancillary services should not exist against a subscriber when a request to cease the primary service is being processed
                        if (InstanceStatusEnum.ACTIVE == serviceInstance.getStatus()) {
                            dataDiscrepancies += "The Cease request is for a Primary Service and active ancillary services exist against the subscription. \n ";
                        }
                        serviceInstanceService.terminateService(serviceInstance, terminationDate, terminationReasonAncillary, orderLineDTO.operatorOrderNumber);
                        LOGGER.info("Ancillary service terminated : " + serviceInstance.getCode());
                    }
                }
            }
            subscriptionService.terminateSubscription(subscription, terminationDate, terminationReasonMain, orderLineDTO.operatorOrderNumber);

            // Add OLD_ prefix to subscription code in order to can create the new one with same code
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
            String oldPrefix = "OLD" + sdf.format(new Date().getTime());
            UserAccount userAccount = userAccountService.findByCode(orderLineDTO.serviceId);
            userAccountService.userAccountTermination(userAccount, terminationDate, terminationReasonMain);
            userAccount.setCode(oldPrefix + "_" + userAccount.getCode());
            userAccountService.update(userAccount);
            LOGGER.info("Subscription terminated : " + subscription.getCode());

            for (AccessDTO accessDto : accessDtoList) {
                for (Access access : subscription.getAccessPoints()) {
                    if (access.getAccessUserId().equals(accessDto.accessUserId)) {
                        if (accessDto.endDate != null) {
                            access.setEndDate(accessDto.endDate);
                            accessService.update(access);
                        }
                    }
                }
            }

            subscription.setCode(oldPrefix + "_" + subscription.getCode());
            subscriptionService.update(subscription);

            // PO2-140 : if Action_Qualifier of 'FAILED_INSTALL', Rebate all oneShotCharge.
            if ("C".equalsIgnoreCase(orderLineDTO.billAction) && "FAILED_INSTALL".equals(orderLineDTO.billActionQualifier)) {
                List<WalletOperation> walletOperations = subscription.getUserAccount().getWallet().getOperations();
                if (walletOperations != null && walletOperations.size() > 0) {
                    OneShotChargeTemplate chargetemplate;
                    for (WalletOperation walletOperation : walletOperations) {
                        ChargeInstance chargeInstance = walletOperation.getChargeInstance();
                        if (!(chargeInstance instanceof RecurringChargeInstance)) {
                            chargetemplate = chargeTemplateService.findById(chargeInstance.getChargeTemplate().getId());
                            BigDecimal woQuantity = walletOperation.getQuantity() != null ? walletOperation.getQuantity().negate() : null;
                            BigDecimal woAmount = walletOperation.getAmountWithoutTax();
                            if (woAmount != null && woQuantity != null && !BigDecimal.ZERO.equals(woQuantity)) {
                                woAmount = woAmount.divide(woQuantity).negate();
                            }
                            OneShotChargeInstance oneShotCharge = oneShotChargeInstanceService.oneShotChargeApplication(subscription, chargetemplate, null, terminationDate,
                                woAmount, null, woQuantity, chargeInstance.getCode().replace("_NRC", ""), "" + woAmount, chargeInstance.getCriteria3(),
                                chargeInstance.getOrderNumber(), false);
                            oneShotCharge.setStatus(InstanceStatusEnum.CLOSED);
                            oneShotCharge.setDescription(chargeInstance.getDescription());
                            oneShotChargeInstanceService.update(oneShotCharge);
                        }
                    }
                }
            }
        } else { // if ancillary service.
            terminateAncillaryService(orderLineDTO, orderDTO, subscription, terminationReasonAncillary, billCodeInfo, quantity);
        }
    }

    /**
     * Terminate the ancillary service from Order Line
     *
     * @param orderLineDTO the order line DTO
     * @param orderDTO the order DTO
     * @param subscription the subscription
     * @param terminationReason the termination reason
     * @param billCodeInfo the bill code info
     * @param quantity the quantity
     * @throws ParseException the parse exception
     * @throws BusinessException the business exception
     */
    private void terminateAncillaryService(OrderLineDTO orderLineDTO, OrderDTO orderDTO, Subscription subscription, SubscriptionTerminationReason terminationReason,
            String billCodeInfo, BigDecimal quantity) throws ParseException, BusinessException {
        Boolean ancillaryServiceFound = false;

        // Tha map which allows to store for each each service its new quantity.
        Map<ServiceInstance, BigDecimal> serviceInstanceQuantity = new HashMap<>();

        if (orderLineDTO != null && orderLineDTO.billCode != null && subscription != null) {

            // If MSN & DDIXX ancillary services
            if (orderLineDTO.billCode.equalsIgnoreCase(AncillaryServiceEnum.MSN.name()) || orderLineDTO.billCode.startsWith(AncillaryServiceEnum.DDI.name())) {

                Map<String, String> ranges = getOrderLineRanges(orderLineDTO);
                if (ranges == null) {
                    return;
                }

                for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
                    if (serviceInstance != null) {
                        if (InstanceStatusEnum.TERMINATED.equals(serviceInstance.getStatus())) {
                            if (orderLineDTO.billCode.equals(serviceInstance.getCode())) {

                                if (orderLineDTO.billCode.equalsIgnoreCase(AncillaryServiceEnum.MSN.name())) {
                                    Map<String, String> cf = (Map<String, String>) serviceInstance.getCfValuesNullSafe().getValue("CF_OLD_BILL_ATTRIBUTE");

                                    if (cf != null) {

                                        if (!cf.containsValue(ranges.get("RANGE_START"))) {
                                            continue;
                                        }
                                    }
                                }

                                dataDiscrepancies += "The subscriber service (bill code) being ceased should be active against the subscription. \n";
                            }
                            continue;
                        }
                        if (orderLineDTO.billCode.equalsIgnoreCase(serviceInstance.getCode())) {
                            if (!hasSameRanges(serviceInstance, ranges, orderLineDTO.billCode)) {
                                continue;
                            }

                            Map<String, String> oldCf = (Map<String, String>) serviceInstance.getCfValuesNullSafe().getValue("CF_OLD_BILL_ATTRIBUTE");
                            if (oldCf == null) {
                                oldCf = new HashMap<>();
                            }
                            if (AncillaryServiceEnum.DDI.name().equals(serviceInstance.getCode())) {
                                Map<String, String> cf = (Map<String, String>) serviceInstance.getCfValuesNullSafe().getValue("CF_BILL_ATTRIBUTE");
                                if (cf != null && cf.get("RANGE_START") != null && cf.get("RANGE_END") != null
                                        && cf.get("RANGE_START").split("\\|")[0].equals(ranges.get("RANGE_START"))
                                        && cf.get("RANGE_END").split("\\|")[0].equals(ranges.get("RANGE_END"))) {

                                    String timestamp = getDate(new Date());
                                    String[] splitedCF = cf.get("RANGE_START").split(Pattern.quote("|"));
                                    String startDate = splitedCF.length > 2 ? splitedCF[2] : null;
                                    String unit = splitedCF.length > 1 ? splitedCF[1] : null;
                                    // value|startDate|EndDate|unit
                                    oldCf.put(timestamp + "|RANGE_START", ranges.get("RANGE_START") + "|" + startDate + "|" + orderLineDTO.billEffectDate + "|" + unit);
                                    oldCf.put(timestamp + "|RANGE_END", ranges.get("RANGE_END") + "|" + startDate + "|" + orderLineDTO.billEffectDate + "|" + unit);
                                    cf.remove("RANGE_START");
                                    cf.remove("RANGE_END");
                                }
                            } else {
                                Map<String, String> cf = (Map<String, String>) serviceInstance.getCfValuesNullSafe().getValue("CF_BILL_ATTRIBUTE");
                                if (cf != null && cf.get("RANGE_START") != null && cf.get("RANGE_START").split("\\|")[0].equals(ranges.get("RANGE_START"))) {
                                    String timestamp = getDate(new Date());
                                    String[] splitedCF = cf.get("RANGE_START").split(Pattern.quote("|"));
                                    String startDate = splitedCF.length > 2 ? splitedCF[2] : null;
                                    String unit = splitedCF.length > 1 ? splitedCF[1] : null;
                                    // value|startDate|EndDate|unit
                                    oldCf.put(timestamp + "|RANGE_START", ranges.get("RANGE_START") + "|" + startDate + "|" + orderLineDTO.billEffectDate + "|" + unit);
                                    cf.remove("RANGE_START");
                                    if (cf.get("RANGE_END") != null && cf.get("RANGE_END").split("\\|")[0].equals(ranges.get("RANGE_END"))) {
                                        oldCf.put(timestamp + "|RANGE_END", ranges.get("RANGE_END") + "|" + startDate + "|" + orderLineDTO.billEffectDate + "|" + unit);
                                        cf.remove("RANGE_END");
                                    }
                                }
                            }
                            serviceInstance.getCfValuesNullSafe().setValue("CF_OLD_BILL_ATTRIBUTE", oldCf);
                            serviceInstanceService.update(serviceInstance);

                            // AC4 : For Cease bill items, if the quantity against the service/bill item is reduced to zero, the service is ceased
                            updateAncillaryService(orderLineDTO, orderDTO, terminationReason, serviceInstance, serviceInstanceQuantity, quantity);
                            LOGGER.info("Ancillary service terminated : " + serviceInstance.getCode());
                            ancillaryServiceFound = true;
                        }
                    }
                }
            } else {

                // validate and calculate the quantity to be reduced
                if (!validateAndCalculateQuantity(orderLineDTO, subscription, serviceInstanceQuantity, billCodeInfo, quantity)) {
                    return;
                }
                for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
                    if (serviceInstance != null) {
                        if (serviceInstance.getStatus().equals(InstanceStatusEnum.TERMINATED)) {
                            continue;
                        }
                        // Terminate all service instances.
                        if (orderLineDTO.billCode.equals(serviceInstance.getCode())) {
                            // AC4 : For Cease bill items, if the quantity against the service/bill item is reduced to zero, the service is ceased
                            updateAncillaryService(orderLineDTO, orderDTO, terminationReason, serviceInstance, serviceInstanceQuantity, quantity);
                            ancillaryServiceFound = true;
                        }
                    }
                }
            }
        }
        if (ancillaryServiceFound == false) {
            // An ancillary service should exist against an active subscriber when a Cease request is received.
            dataDiscrepancies += "The service " + orderLineDTO.billCode + " is not found in subscription or already ceased. \n";
        }
    }

    /**
     * Get the map of order line ranges.
     *
     * @param orderLineDTO the order line DTO
     * @return the map of order line ranges.
     */
    private Map<String, String> getOrderLineRanges(OrderLineDTO orderLineDTO) {
        Map<String, String> ranges = new HashMap<>();

        if (orderLineDTO == null || orderLineDTO.billCode == null || orderLineDTO.attributes == null) {
            rejectOrder("The order line or its attributes are required");
            return null;
        }

        // get range attributes for current order line
        for (AttributeDTO attributeDTO : orderLineDTO.attributes) {
            if (attributeDTO != null) {
                if ("RANGE_START".equalsIgnoreCase(attributeDTO.code)) {
                    ranges.put("RANGE_START", attributeDTO.value);
                }
                if ("RANGE_END".equalsIgnoreCase(attributeDTO.code)) {
                    ranges.put("RANGE_END", attributeDTO.value);
                }
                if (ranges.size() == 2) {
                    break;
                }
            }
        }

        if (AncillaryServiceEnum.DDI.name().equalsIgnoreCase(orderLineDTO.billCode)) {
            if (ranges.isEmpty() || !ranges.containsKey("RANGE_START") || !ranges.containsKey("RANGE_END")) {
                rejectOrder("The start and end range for the service : " + orderLineDTO.billCode + " are required");
                return null;
            }
        } else if (AncillaryServiceEnum.MSN.name().equalsIgnoreCase(orderLineDTO.billCode)) {
            if (!ranges.containsKey("RANGE_START")) {
                rejectOrder("The start range for the service : " + orderLineDTO.billCode + " is required");
                return null;
            }
        }
        return ranges;
    }

    /**
     * Suspend the service from Order Line
     *
     * @param orderLineDTO Order Line
     * @param orderDTO Order
     * @throws Exception App Exception
     */
    private void suspendService(OrderLineDTO orderLineDTO, OrderDTO orderDTO) throws Exception {
        LOGGER.info("Suspending service...");
        OfferTemplate offerTemplate = offerTemplateService.findByCode(orderLineDTO.offerName);
        Map<String, String> catalog = null;
        String billCodeInfo = catalog.get(orderLineDTO.billCode);

        if (offerTemplate != null) {
            catalog = getCatalog(orderLineDTO, offerTemplate);
        }
        if (catalog != null) {
            billCodeInfo = getBillCodeInfo(catalog, orderLineDTO, null);
        }

        // Check if service is not found
        if (billCodeInfo == null) {
            rejectOrder("The service " + orderLineDTO.billCode + " is not found");
            return;
        }

        // Check Service Type (Main, Event, Ancillary)
        Subscription subscription = subscriptionService.findByCode(orderLineDTO.serviceId);
        if (subscription == null) {
            rejectOrder("The subscription " + orderLineDTO.serviceId + " is not found");
        }
        List<AccessDTO> accessDtoList = new ArrayList<>();
        for (Access a : subscription.getAccessPoints()) {
            AccessDTO access = new AccessDTO();
            access.accessUserId = a.getAccessUserId();
            access.endDate = a.getEndDate();
            accessDtoList.add(access);
        }
        SubscriptionTerminationReason terminationReason = subscriptionTerminationReasonService.findByCodeReason(TERMINATION_REASON_TOS);

        String serviceType = getBillCodeField(catalog, orderLineDTO, Catalog.SERVICE_TYPE);

        if (ServiceTypeEnum.MAIN.getLabel().equalsIgnoreCase(serviceType)) { // if main service then terminate all services
            for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
                if (serviceInstance.getStatus().equals(InstanceStatusEnum.TERMINATED)) {
                    continue;
                }

                serviceInstanceService.terminateService(serviceInstance, getDate(orderLineDTO.billEffectDate), terminationReason, orderLineDTO.operatorOrderNumber);
                LOGGER.info("service terminated : " + serviceInstance.getCode());

            }
            subscriptionService.terminateSubscription(subscription, getDate(orderLineDTO.billEffectDate), terminationReason, orderLineDTO.operatorOrderNumber);
            // Add OLD_ prefix to subscription code in order to can create the new one with same code
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");
            String oldPrefix = "OLD" + sdf.format(new Date().getTime());
            UserAccount userAccount = userAccountService.findByCode(orderLineDTO.serviceId);
            userAccountService.userAccountTermination(userAccount, getDate(orderLineDTO.billEffectDate), terminationReason);
            userAccount.setCode(oldPrefix + "_" + userAccount.getCode());
            userAccountService.update(userAccount);
            LOGGER.info("Subscription terminated : " + subscription.getCode());

            for (AccessDTO accessDto : accessDtoList) {
                for (Access access : subscription.getAccessPoints()) {
                    if (access.getAccessUserId().equals(accessDto.accessUserId)) {
                        if (accessDto.endDate != null) {
                            access.setEndDate(accessDto.endDate);
                            accessService.update(access);
                        }
                    }
                }
            }

            subscription.setCode(oldPrefix + "_" + subscription.getCode());
            subscriptionService.update(subscription);
        } else {
            rejectOrder("The service " + orderLineDTO.billCode + " is not Main to be suspended");
            return;
        }
    }

    /**
     * Get days difference between two dates
     *
     * @param d1
     * @param d2
     * @return
     */
    public static long getDifferenceDays(Date d1, Date d2) {
        long diff = d2.getTime() - d1.getTime();
        return TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);
    }

    /**
     * Activate Service from Order Line
     *
     * @param orderLineDTO Order Line
     * @param orderDTO Order
     * @throws Exception App Exception
     */
    private void activateService(OrderLineDTO orderLineDTO, OrderDTO orderDTO) throws Exception {
        activateService(orderLineDTO, orderDTO, true, true, null);
    }

    /**
     * Activate the service from Order Line with CHANGE options
     *
     * @param orderLineDTO Order Line
     * @param orderDTO Order
     * @throws Exception App Exception
     */
    private void activateService(OrderLineDTO orderLineDTO, OrderDTO orderDTO, Boolean applyConnectionChargeOnNew, Boolean resetEngagementOnNew, String serviceToApply)
            throws Exception {
        LOGGER.info("Activating service " + orderLineDTO.serviceId + " billCode: " + orderLineDTO.billCode + " in offer " + orderLineDTO.offerName);
        OfferTemplate offerTemplate = offerTemplateService.findByCode(orderLineDTO.offerName);

        ServiceTemplate serviceTemplate = serviceTemplateService.findByCode(orderLineDTO.billCode);
        // check if catalog table exists
        if (serviceTemplate == null) {
            rejectOrder("Service is not found : " + orderLineDTO.billCode);
            return;
        }


        // PO2-528 : Validation on Provide order to ensure service_id not in use on DDI/MSN Ancillary Service
        if (!isValidOnMsnDdiRanges(orderLineDTO.serviceId, null)) {
            orderRejected = Boolean.TRUE;
            this.orderStatus = "ServiceId " + orderLineDTO.serviceId + " already in use on DDI/MSN Ancillary Service or an active subscription";
            return;
        }

        String exchangeClass = getExchangeClass(orderLineDTO);

        // Check Service Type (Main, Event, Ancillary)
        Subscription subscription = subscriptionService.findByCode(orderLineDTO.serviceId);
        String serviceType = "Main";
        String serviceName = null;

        if (getTariffInfo(orderLineDTO, "O", exchangeClass) == null || getTariffInfo(orderLineDTO, "R", exchangeClass) == null) {
                orderRejected = true;
                orderStatus = "Tariff is not found for " + orderLineDTO.billCode;
                return;
            }

        // Set the service quantity
        BigDecimal quantity = BigDecimal.ZERO;
        quantity = setServiceQuantity(orderLineDTO, billCodeInfo, quantity);
        if (quantity == null) {
            return;
        }

        // Create subscription if doesn't exist
        if (subscription == null) {
            subscription = new Subscription();
            subscription.setCode(orderLineDTO.serviceId);
            subscription.setSeller(sellerService.findByCode("OPENEIR"));
            LOGGER.info("User Account : " + orderLineDTO.serviceId);
            UserAccount userAccount = userAccountService.findByCode(orderLineDTO.serviceId);
            if (userAccount == null) {
                LOGGER.info("User account does not exist, it will be created");
                userAccount = new UserAccount();
                userAccount.setCode(orderLineDTO.serviceId);
                // Construct masterAccountNumber if does not exist
                LOGGER.info("orderLineDTO.serviceId : " + orderLineDTO.serviceId);
                BillingAccount billingAccount = billingAccountService.findByCode(orderLineDTO.masterAccountNumber);
                if (billingAccount == null) {
                    rejectOrder("The billing account is not found");
                    return;
                }
                userAccountService.createUserAccount(billingAccount, userAccount);
                createdUsersAccount.add(userAccount);
            }
            subscription.setUserAccount(userAccount);
            subscription.setOffer(offerTemplate);
            subscription.setSubscriptionDate(getDate(orderLineDTO.billEffectDate));
            subscription.setStatusDate(getDate(orderLineDTO.billEffectDate));
            DatePeriod datePeriod = new DatePeriod(getDate(orderLineDTO.billEffectDate), null);
            subscription.getCfValuesNullSafe().setValue("UAN", datePeriod, null, orderLineDTO.uan);
            subscriptionService.create(subscription);
            createdSubscriptions.add(subscription);
            // add access point
            Access accessPoint = new Access();
            accessPoint.setAccessUserId(orderLineDTO.serviceId);
            accessPoint.setStartDate(getDate(orderLineDTO.billEffectDate));
            accessPoint.setSubscription(subscription);
            subscription.getAccessPoints().add(accessPoint);
            subscriptionService.update(subscription);

        } else if (ServiceTypeEnum.MAIN.getLabel().equalsIgnoreCase(serviceType)) { // Check if subscription is cancelled or terminated or main service already activated
            if (subscription.getStatus() == SubscriptionStatusEnum.RESILIATED || subscription.getStatus() == SubscriptionStatusEnum.CANCELED) {
                rejectOrder("Subscription " + subscription.getCode() + " has status " + subscription.getStatus());
                return;
            }
            for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
                if (serviceInstance.getStatus().equals(InstanceStatusEnum.TERMINATED)) {
                    continue;
                }
                // check if main service already activated
                if (serviceInstance.getCode().equals(orderLineDTO.billCode)) {

                    if (serviceInstance.getStatus() == InstanceStatusEnum.ACTIVE) {
                        quantity = setServiceQuantity(orderLineDTO, serviceInstance, billCodeInfo, quantity, null);
                        if (quantity != null) {
                            updateWalletOperations(serviceInstance, orderLineDTO, quantity);
                            serviceInstance.setQuantity(quantity);
                            serviceInstanceService.update(serviceInstance);
                        }
                        return;
                    }

                    rejectOrder("Service " + orderLineDTO.billCode + " already exists");
                    return;
                }
                // Check if other main service already exists
                OrderLineDTO orderLine = new OrderLineDTO();
                orderLine.billCode = serviceInstance.getCode();
                String type = getBillCodeField(catalog, orderLine, Catalog.SERVICE_TYPE);
                if (ServiceTypeEnum.MAIN.getLabel().equalsIgnoreCase(type)) {
                    rejectOrder("Main Service already exists in subscription");
                    return;
                }
            }
        }
        // check if there is a main service when activating an ancillary service
        if (ServiceTypeEnum.ANCILLARY.getLabel().equalsIgnoreCase(serviceType)) {
            resetEngagementOnNew = false;
            boolean mainServiceExists = false;
            for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
                if (serviceInstance.getStatus().equals(InstanceStatusEnum.TERMINATED)) {
                    continue;
                }
                String serviceInstanceInfo = catalog.get(serviceInstance.getCode() + "|O");
                if (serviceInstanceInfo == null) {
                    serviceInstanceInfo = catalog.get(serviceInstance.getCode() + "|R");
                }
                if (serviceInstanceInfo != null && serviceInstanceInfo.split("\\|")[Catalog.SERVICE_TYPE.getPosition()].equalsIgnoreCase(ServiceTypeEnum.MAIN.getLabel())) {
                    mainServiceExists = true;
                }
                if (serviceInstance.getCode().equals(orderLineDTO.billCode) && serviceInstance.getStatus() == InstanceStatusEnum.ACTIVE) {
                    quantity = setServiceQuantity(orderLineDTO, serviceInstance, billCodeInfo, quantity, null);
                    if (quantity == null) {
                        return;
                    }
                }
            }
            if (mainServiceExists == false) {
                orderStatus = "Main service is not found";
                orderRejected = true;
                return;
            }

            if (AncillaryServiceEnum.MSN.name().equalsIgnoreCase(orderLineDTO.billCode) || orderLineDTO.billCode.startsWith(AncillaryServiceEnum.DDI.name())) { // look in ancillary
                // services MSN &
                // DDIXX

                String rangeStart = null;
                String rangeEnd = null;

                Map<String, String> attributes = getRangesAttributes(orderLineDTO);
                if (attributes != null && attributes.size() > 0) {
                    rangeStart = attributes.get("RANGE_START");
                    rangeEnd = attributes.get("RANGE_END");

                    if (!isValidRanges(rangeStart, rangeEnd, orderLineDTO.originalServiceId)) {
                        return;
                    }
                }
            }

        }

        // Create Service Instance with right data
        ServiceInstance serviceInstance = new ServiceInstance();
        serviceInstance.setVersion(2);
        ServiceTemplate serviceTemplate = serviceTemplateService.findByCode(serviceName);
        serviceInstance.setCode(orderLineDTO.billCode);
        serviceInstance.setOrderNumber(orderLineDTO.operatorOrderNumber);
        serviceInstance.setDescription(billCodeInfo.split("\\|")[Catalog.DESCRIPTION.getPosition()]);
        serviceInstance.setQuantity(new BigDecimal(orderLineDTO.billQuantity));
        serviceInstance.setServiceTemplate(serviceTemplate);
        serviceInstance.setSubscription(subscription);
        serviceInstance.setSubscriptionDate(getDate(orderLineDTO.billEffectDate));
        serviceInstance.getCfValuesNullSafe().setValue("CF_BILL_CODE", orderLineDTO.billCode);
        serviceInstance.getCfValuesNullSafe().setValue("CF_BILL_DESCRIPTION", billCodeInfo.split("\\|")[Catalog.DESCRIPTION.getPosition()]);
        serviceInstance.getCfValuesNullSafe().setValue("ACTION_QUALIFIER", orderLineDTO.billActionQualifier);
        serviceInstance.getCfValuesNullSafe().setValue("SERVICE_TYPE", serviceType);
        serviceInstance.getCfValuesNullSafe().setValue("SLA", orderLineDTO.getAttributeValue("SLA"));
        serviceInstance.getCfValuesNullSafe().setValue("SLA_HISTORY", new DatePeriod(getDate(orderLineDTO.billEffectDate), null), 1, orderLineDTO.getAttributeValue("SLA"));
        serviceInstance.getCfValuesNullSafe().setValue("CYCLE_FORWARD", getBillCodeField(catalog, orderLineDTO, Catalog.CYCLE_FORWARD));
        serviceInstance.getCfValuesNullSafe().setValue("PERIODICITY", getBillCodeField(catalog, orderLineDTO, Catalog.PERIODICITY));
        serviceInstance.getCfValuesNullSafe().setValue("SPECIAL_CONNECT", orderLineDTO.specialConnect);
        serviceInstance.getCfValuesNullSafe().setValue("SPECIAL_RENTAL", orderLineDTO.specialRental);
        if (!StringUtils.isBlank(orderLineDTO.getAttributeValue("EXCHANGE"))) {
            serviceInstance.getCfValuesNullSafe().setValue("EXCHANGE_CLASS", exchangeClass);
        }
        // Saving reference to Service Id in provided service
        if (("TNS".equalsIgnoreCase(orderLineDTO.billAction) || "CNS".equalsIgnoreCase(orderLineDTO.billAction)) && orderLineDTO.xRefServiceId != null) {
            serviceInstance.getCfValuesNullSafe().setValue("XREF_SERVICE_ID", orderLineDTO.xRefServiceId);
        }
        // calculate agreement date if exists
        if (resetEngagementOnNew == true) {
            Integer agreementDuration = getAgreementDuration(getDate(orderLineDTO.billEffectDate), orderLineDTO.operatorId, orderLineDTO.offerName);
            if (agreementDuration != null) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(getDate(orderLineDTO.billEffectDate));
                calendar.add(Calendar.MONTH, agreementDuration);
                serviceInstance.setEndAgreementDate(calendar.getTime());
            }
        }

        // filling Bill attributes in service instance level
        Map<String, String> attributes = new HashMap<>();
        for (AttributeDTO attribute : orderLineDTO.attributes) {
            attributes.put(attribute.code, attribute.value + "|" + attribute.unit + "|" + orderLineDTO.billEffectDate);
        }
        if (!attributes.isEmpty()) {
            serviceInstance.getCfValuesNullSafe().setValue("CF_BILL_ATTRIBUTE", attributes);
        }
        serviceInstanceService.create(serviceInstance);

        // activate service
        serviceInstanceService.serviceInstanciation(serviceInstance);
        subscription.getServiceInstances().add(serviceInstance);
        serviceInstanceService.serviceActivation(serviceInstance, null, null);

        if (serviceToApply != null) {
            applyServiceToApply(orderLineDTO, serviceToApply, subscription);
        }

        // renaming charge instance with bill code name + suffix
        for (OneShotChargeInstance oci : serviceInstance.getSubscriptionChargeInstances()) {
            oci.setCode(orderLineDTO.billCode + "_NRC");
        }
        for (RecurringChargeInstance rci : serviceInstance.getRecurringChargeInstances()) {
            rci.setCode(orderLineDTO.billCode + "_RC");
        }

    }

    /**
     * Update wallet operations.
     *
     * @param serviceInstance the service instance
     * @param orderLineDTO the order line DTO
     * @param quantity the quantity
     * @throws ParseException the parse exception
     */
    private void updateWalletOperations(ServiceInstance serviceInstance, OrderLineDTO orderLineDTO, BigDecimal quantity) throws ParseException {
        Date effectDate = getDate(orderLineDTO.billEffectDate);
        if (serviceInstance != null) {
            List<ChargeInstance> chargeInstances = serviceInstance.getChargeInstances();
            if (chargeInstances != null && !chargeInstances.isEmpty()) {
                BigDecimal walletPeriode;
                BigDecimal lastQtyPeriode;
                BigDecimal newQtyPeriode;
                BigDecimal newQuantity;
                for (ChargeInstance chargeInstance : chargeInstances) {
                    List<WalletOperation> walletOperations = chargeInstance.getWalletOperations();
                    Set<WalletOperation> walletOperationIds = new HashSet<>();
                    if (walletOperations != null && !walletOperations.isEmpty()) {
                        for (WalletOperation wo : walletOperations) {
                            if (effectDate != null && wo.getStartDate() != null && wo.getEndDate() != null) {
                                if (effectDate.compareTo(wo.getStartDate()) <= 0) {
                                    wo.setQuantity(quantity);
                                    walletOperationIds.add(wo);
                                    // wo.setAmountWithoutTax(wo.getUnitAmountWithoutTax().multiply(quantity));
                                } else if (effectDate.before(wo.getEndDate()) && effectDate.after(wo.getStartDate())) {
                                    walletPeriode = new BigDecimal(DateUtils.daysBetween(wo.getStartDate(), wo.getEndDate()));
                                    lastQtyPeriode = new BigDecimal(DateUtils.daysBetween(wo.getStartDate(), effectDate));
                                    newQtyPeriode = new BigDecimal(DateUtils.daysBetween(effectDate, wo.getEndDate()));
                                    log.info("walletPeriode " + walletPeriode);
                                    log.info("lastQtyPeriode " + lastQtyPeriode);
                                    log.info("newQtyPeriode " + newQtyPeriode);
                                    newQuantity = ((wo.getQuantity().multiply(lastQtyPeriode)).add(quantity.multiply(newQtyPeriode))).divide(walletPeriode, 4,
                                        RoundingMode.HALF_EVEN);
                                    log.info("newQuantity " + newQuantity);
                                    wo.setQuantity(newQuantity);
                                    walletOperationIds.add(wo);
                                }
                            }
                        }
                        for (WalletOperation walletOperation : walletOperationIds) {
                            executeRatingScript(walletOperation);
                        }
                    }
                }
            }
        }
    }

    /**
     * Execute rating script.
     *
     * @param walletOperation the wallet operation
     * @throws RatingScriptExecutionErrorException the rating script execution error exception
     */
    private void executeRatingScript(WalletOperation walletOperation) throws RatingScriptExecutionErrorException {
        String scriptInstanceCode = "com.eir.script.OrderRatingScript";
        try {
            log.debug("Will execute priceplan script " + scriptInstanceCode);
            scriptInstanceService.executeCached(walletOperation, scriptInstanceCode, null);

        } catch (BusinessException e) {
            throw new RatingScriptExecutionErrorException("Failed when run script " + scriptInstanceCode + ", info " + e.getMessage(), e);
        }
    }

    /**
     * Get the exchange class
     *
     * @param orderLineDTO Order line DTO
     * @return the exchange class
     * @throws ParseException the parse exception
     */
    private String getExchangeClass(OrderLineDTO orderLineDTO) throws ParseException {
        String exchangeClass = null;
        String exchangeCode = orderLineDTO.getAttributeValue("EXCHANGE");
        if (!StringUtils.isBlank(exchangeCode)) {
            Map<String, String> exchangeDetails = (Map<String, String>) customFieldInstanceService.getCFValueByClosestMatch(providerService.getProvider(), "EXCHANGE_DETAILS",
                getDate(orderLineDTO.billEffectDate), exchangeCode);
            if (exchangeDetails != null) {
                exchangeClass = exchangeDetails.get("Exchange_Class");
            }
        }
        return exchangeClass;
    }

    /**
     * Apply service to apply.
     *
     * @param orderLineDTO the order line DTO
     * @param serviceToApply the service to apply
     * @param subscription the subscription
     * @throws ParseException the parse exception
     */
    private void applyServiceToApply(OrderLineDTO orderLineDTO, String serviceToApply, Subscription subscription) throws ParseException {
        OfferTemplate miscOfferTemplate = offerTemplateService.findByCode(MISC_OFFER_TEMPLATE_CODE);
        if (miscOfferTemplate == null) {
            rejectOrder("The offer template MISC_WB is not found");
            return;
        }

        Map<String, String> miscTariffTable = getCatalog(orderLineDTO, miscOfferTemplate);
        if (miscTariffTable == null) {
            rejectOrder("Tariffs are not found for date " + orderLineDTO.billEffectDate);
            return;
        }

        String miscBillCodeInfo = miscTariffTable.get(serviceToApply);
        if (StringUtils.isBlank(miscBillCodeInfo)) {
            rejectOrder("The service " + serviceToApply + " is not found");
            return;
        }

        OneShotChargeTemplate chargetemplate = chargeTemplateService.findByCode(MISC_CHARGE_TEMPLATE_CODE);
        if (chargetemplate == null) {
            rejectOrder("The one-shot charge " + MISC_CHARGE_TEMPLATE_CODE + " is not found");
            return;
        }
        while (miscBillCodeInfo.contains("||")) {
            miscBillCodeInfo = miscBillCodeInfo.replaceAll(Pattern.quote("||"), "\\| \\|");
        }
        String oneShotTariff = miscBillCodeInfo.split(Pattern.quote("|"))[3];
        BigDecimal amount = (oneShotTariff != null && isValidDouble(oneShotTariff)) ? new BigDecimal(oneShotTariff) : null;
        oneShotChargeInstanceService.oneShotChargeApplication(subscription, chargetemplate, null, getDate(orderLineDTO.billEffectDate), amount, null, null, serviceToApply, "0",
            null, orderLineDTO.operatorOrderNumber, true);
    }

    /**
     * Get Service_Id with prefix attached
     *
     * @param orderLineDTO Order LineSubs
     * @return Prefixed Service Id
     */
    private String getServiceIdWithPrefix(OrderLineDTO orderLineDTO) {
        try {
            if ("MC".equalsIgnoreCase(orderLineDTO.billAction)) {
                return orderLineDTO.serviceId;
            }
            // get Prefix from catalog
            String subscriptionPrefix = null;
            CustomFieldValue cfv = serviceTemplateService.findByCode(orderLineDTO.billCode).getCfValuesNullSafe().getCfValue(SUBSCRIPTION_PREFIX);
            if(cfv != null) {
                subscriptionPrefix = cfv.getStringValue();
            }
            if (subscriptionPrefix == null || subscriptionPrefix.isEmpty()) {
                return orderLineDTO.serviceId;
            } else {
                return subscriptionPrefix.replace("_", "") + "_" + orderLineDTO.serviceId;
            }
        } catch (Exception ex) {
            return null;
        }
    }


    /**
     * Gets the master account number.
     *
     * @param orderLineDTO the order line DTO
     * @param offerTemplate the offer template
     * @return the master account number
     * @throws ParseException the parse exception
     */
    private String getMasterAccountNumber(OrderLineDTO orderLineDTO, OfferTemplate offerTemplate) throws ParseException {
        // Master Account Guiding is only for provide orders
        if (!"P".equalsIgnoreCase(orderLineDTO.billAction) || ("P".equalsIgnoreCase(orderLineDTO.billAction) && !orderLineDTO.isMain)) {
            String subscriptionCode = getServiceIdWithPrefix(orderLineDTO);
            if (subscriptionService.findByCode(subscriptionCode) != null) {
                return subscriptionService.findByCode(subscriptionCode).getUserAccount().getBillingAccount().getCode();
            }
        }
        return getProductSetPrefixCode(orderLineDTO, offerTemplate);
    }

    /**
     * Gets the product set prefix.
     *
     * @param orderLineDTO the order line DTO
     * @param offerTemplate the offer template
     * @return the product set prefix
     * @throws ParseException the parse exception
     */
    private String getProductSetPrefixCode(OrderLineDTO orderLineDTO, OfferTemplate offerTemplate) throws ParseException {
        // get Product set from catalog
        if (offerTemplate == null) {
            rejectOrder("The offer template is not found for bill code " + orderLineDTO.billCode);
            return null;
        }
        Map<String, String> catalog = (Map<String, String>) offerTemplate.getCfValuesNullSafe().getValue("NEW_CATALOG", getDate(orderLineDTO.billEffectDate));
        String productSet = getBillCodeField(catalog, orderLineDTO, Catalog.PRODUCT_SET);
        if (productSet == null || productSet.isEmpty()) {
            return null;
        }
        return productSet.trim().replace("_", "") + "_" + orderLineDTO.operatorId;
    }

    /**
     * Get ProductSet with prefix code attached
     *
     * @param orderLineDTO Order Line
     * @param offerTemplate Offer Template
     * @return ProductSet with prefix code
     */
    private String getMigrationCode(OrderLineDTO orderLineDTO, OfferTemplate offerTemplate) throws ParseException {
        // get Product set from catalog
        Map<String, String> catalog = (Map<String, String>) offerTemplate.getCfValuesNullSafe().getValue("NEW_CATALOG", getDate(orderLineDTO.billEffectDate));
        return getBillCodeField(catalog, orderLineDTO, Catalog.MIGRATION_CODE);

    }

    /**
     * Get engagement duration for a customer & offer.
     *
     * @param effectDate Effect Date
     * @param customer Customer
     * @param offer Offer
     * @return Engagement duration
     */
    private Integer getAgreementDuration(Date effectDate, String customer, String offer) throws ParseException {
        Map<String, String> cf = (Map<String, String>) providerService.getProvider().getCfValuesNullSafe().getValue("ENGAGEMENT_DURATION");
        if (cf == null) {
            return null;
        }
        String startDate = null;
        String endDate = null;
        for (Entry<String, String> entry : cf.entrySet()) {
            if (entry.getKey().startsWith(customer + "|" + offer + "|")) {
                String[] splitedKey = entry.getKey().split(Pattern.quote("|"));
                startDate = splitedKey.length > 2 ? splitedKey[2] : null;
                endDate = splitedKey.length > 3 ? splitedKey[3] : null;
                if (startDate == null || "null".equals(startDate)) {
                    continue;
                } else if (endDate == null || "null".equals(endDate)) {
                    if (getFormattedDate(startDate).before(effectDate)) {
                        return new Integer(entry.getValue());
                    }
                } else {
                    if (getFormattedDate(startDate).before(effectDate) && getFormattedDate(endDate).after(effectDate)) {
                        return new Integer(entry.getValue());
                    }
                }
            }
            if (entry.getKey().startsWith("*|" + offer + "|")) {
                startDate = entry.getKey().split(Pattern.quote("|"))[2];
                endDate = entry.getKey().split(Pattern.quote("|"))[3];

                Date start = DateUtils.parseDateWithPattern(startDate, DATE_TIME_PATTERN);
                Date end = DateUtils.parseDateWithPattern(endDate, DATE_TIME_PATTERN);

                if (start == null) {
                    continue;
                } else if (end == null) {
                    if (start.before(effectDate)) {
                        return new Integer(entry.getValue());
                    }
                } else {
                    if (start.before(effectDate) && end.after(effectDate)) {
                        return new Integer(entry.getValue());
                    }
                }
            }

        }

        return null;
    }

    /**
     * Get service Type
     *
     * @param orderLineDTO orderLine
     * @return Service Type
     * @throws Exception App Exception
     */
    private String getServiceType(OrderLineDTO orderLineDTO) throws Exception {
        if (orderLineDTO.offerName == null) {
            orderLineDTO.offerName = getOfferName(orderLineDTO);
            if (orderLineDTO.offerName == null) {
                return null;
            }
        }
        Map<String, String> catalog = getCatalog(orderLineDTO, offerTemplateService.findByCode(orderLineDTO.offerName));

        // check if catalog table exists
        if (catalog == null) {
            return null;
        }

        String billCodeInfo = getBillCodeInfo(catalog, orderLineDTO, null);

        // Check if service is not found
        if (billCodeInfo == null) {
            return null;
        }

        // Check Service Type (Main, Event, Ancillary)
        String serviceType = billCodeInfo.split("\\|")[Catalog.SERVICE_TYPE.getPosition()];
        return serviceType;

    }

    /**
     * Parse incoming order from XML
     *
     * @return Formatted Order
     * @throws Exception App Exception
     */
    private OrderDTO parseOrder() throws Exception {
        // parse received order Message
        OrderDTO order = new OrderDTO();
        AttributeDTO attribute;
        OrderLineDTO orderLine;
        Document doc = DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(new InputSource(new StringReader(orderMessage)));
        doc.getDocumentElement().normalize();
        final Element racine = doc.getDocumentElement();
        // parsing and validating order fields
        order.orderingSystem = validateField("Ordering_System", format(racine.getElementsByTagName("Ordering_System")), FieldType.TEXT, 5, true);
        order.orderId = validateField("Order_Id", format(racine.getElementsByTagName("Order_Id")), FieldType.TEXT, 15, true);
        order.refOrderId = validateField("Ref_Order_Id", format(racine.getElementsByTagName("Ref_Order_Id")), FieldType.TEXT, 15, false);
        order.transactionId = validateField("Transaction_Id", format(racine.getElementsByTagName("Transaction_Id")), FieldType.TEXT, 15, true);

        // parsing and validating order items fields
        for (int i = 0; i < racine.getElementsByTagName("Bill_Item").getLength(); i++) {
            Element orderItem = (Element) racine.getElementsByTagName("Bill_Item").item(i);
            // parsing and validating order attribute fields
            orderLine = new OrderLineDTO();
            orderLine.itemId = validateField("Item_Id", format(orderItem.getElementsByTagName("Item_Id")), FieldType.TEXT, 50, true);
            orderLine.operatorId = validateField("Operator_Id", format(orderItem.getElementsByTagName("Operator_Id")), FieldType.TEXT, 10, true);
            orderLine.itemComponentId = validateField("Item_Component_Id", format(orderItem.getElementsByTagName("Item_Component_Id")), FieldType.TEXT, 15, false);
            orderLine.itemVersion = validateField("Item_Version", format(orderItem.getElementsByTagName("Item_Version")), FieldType.INT, 3, false);
            orderLine.itemType = validateField("Item_Type", format(orderItem.getElementsByTagName("Item_Type")), FieldType.TEXT, 50, false);
            orderLine.agentCode = validateField("Agent_Code", format(orderItem.getElementsByTagName("Agent_Code")), FieldType.TEXT, 8, false);
            orderLine.uan = validateField("UAN", format(orderItem.getElementsByTagName("UAN")), FieldType.TEXT, 20, false);
            orderLine.masterAccountNumber = validateField("Master_Account_Number", format(orderItem.getElementsByTagName("Master_Account_Number")), FieldType.TEXT, 20, false);
            orderLine.serviceId = validateField("Service_Id", format(orderItem.getElementsByTagName("Service_Id")), FieldType.TEXT, 30, false);
            orderLine.originalServiceId = orderLine.serviceId;
            orderLine.refServiceId = validateField("Ref_Service_Id", format(orderItem.getElementsByTagName("Ref_Service_Id")), FieldType.TEXT, 30, false);
            orderLine.xRefServiceId = validateField("Xref_Service_Id", format(orderItem.getElementsByTagName("Xref_Service_Id")), FieldType.TEXT, 30, false);
            orderLine.billCode = validateField("Bill_Code", format(orderItem.getElementsByTagName("Bill_Code")), FieldType.TEXT, 100, true);
            orderLine.billAction = validateField("Action", format(orderItem.getElementsByTagName("Action")), FieldType.TEXT, 10, true);
            orderLine.billActionQualifier = validateField("Action_Qualifier", format(orderItem.getElementsByTagName("Action_Qualifier")), FieldType.TEXT, 50, false);
            orderLine.billQuantity = validateField("Quantity", format(orderItem.getElementsByTagName("Quantity")), FieldType.INT, 5, true);
            orderLine.billEffectDate = validateField("Effect_Date", format(orderItem.getElementsByTagName("Effect_Date")), FieldType.DATE, 19, true);
            orderLine.specialConnect = validateField("Special_Connect", format(orderItem.getElementsByTagName("Special_Connect")), FieldType.DOUBLE, 20, false);
            orderLine.specialRental = validateField("Special_Rental", format(orderItem.getElementsByTagName("Special_Rental")), FieldType.DOUBLE, 20, false);
            orderLine.operatorOrderNumber = validateField("Operator_Order_Number", format(orderItem.getElementsByTagName("Operator_Order_Number")), FieldType.TEXT, 20, false);

            orderLine.isMain = ServiceTypeEnum.MAIN.getLabel().equalsIgnoreCase(getServiceType(orderLine));

            if (orderLine.offerName == null) {
                orderLine.offerName = getOfferName(orderLine);
            }
            if (orderLine.masterAccountNumber == null && orderLine.offerName != null) {
                orderLine.masterAccountNumber = getMasterAccountNumber(orderLine, offerTemplateService.findByCode(orderLine.offerName));
            }

            for (int k = 0; k < orderItem.getElementsByTagName("Attribute").getLength(); k++) {
                Element attributeItem = (Element) orderItem.getElementsByTagName("Attribute").item(k);
                attribute = new AttributeDTO();
                attribute.code = validateField("Code", format(attributeItem.getElementsByTagName("Code")), FieldType.TEXT, 50, true);
                attribute.value = validateField("Value", format(attributeItem.getElementsByTagName("Value")), FieldType.TEXT, 50, true);
                attribute.unit = validateField("Unit", format(attributeItem.getElementsByTagName("Unit")), FieldType.TEXT, 10, false);
                attribute.componentId = validateField("Component_Id", format(attributeItem.getElementsByTagName("Component_Id")), FieldType.TEXT, 15, false);
                attribute.attributeType = validateField("Attribute_Type", format(attributeItem.getElementsByTagName("Attribute_Type")), FieldType.TEXT, 50, false);
                orderLine.attributes.add(attribute);
            }
            order.orderLines.add(orderLine);

        }
        return order;
    }

    /**
     * Get Catalog
     *
     * @param orderLineDTO Order line
     * @param offerTemplate Offer Template
     * @return Map of Catalog
     * @throws ParseException Exception
     */
    private Map<String, String> getCatalog(OrderLineDTO orderLineDTO, OfferTemplate offerTemplate) throws ParseException {
        if (offerTemplate == null) {
            return null;
        }
        return (Map<String, String>) offerTemplate.getCfValuesNullSafe().getValue("NEW_CATALOG", getDate(orderLineDTO.billEffectDate));
    }

    /**
     * Format XML Element By tag name
     *
     * @param elementsByTagName Node List for element by tag name
     * @return Formatted Text content
     */
    private String format(NodeList elementsByTagName) {
        if (elementsByTagName.item(0) == null) {
            return null;
        }
        return elementsByTagName.item(0).getTextContent();
    }

    /**
     * Validate field received in XML Order
     *
     * @param fieldName Field Name
     * @param fieldValue Field Value
     * @param fieldType Field Type
     * @param fieldSize Field Size
     * @param mandatory Is Field mandatory
     * @return Field Value
     * @throws Exception App Exception
     */
    public <T> T validateField(String fieldName, String fieldValue, FieldType fieldType, Integer fieldSize, Boolean mandatory) throws Exception {
        // Check required parameter
        if (mandatory && StringUtils.isBlank(fieldValue)) {
            errorCode = MISSING_PARAMETER;
            message = "The following parameter is required : " + fieldName;
            throw new Exception(message);
        } else if (!mandatory && StringUtils.isBlank(fieldValue)) {
            return null;
        }
        // Check value is valid depending on type
        if ((fieldType.equals(FieldType.INT) && !isValidInteger(fieldValue)) || (fieldType.equals(FieldType.DOUBLE) && !isValidDouble(fieldValue))) {
            errorCode = INVALID_VALUE;
            message = "The following parameter contains invalid value : " + fieldName;
            throw new Exception(message);
        }
        // Check field size depending on field specified length
        if ((fieldType.equals(FieldType.TEXT) && fieldValue.length() > fieldSize) || (fieldType.equals(FieldType.INT) && fieldValue.length() > fieldSize)
                || (fieldType.equals(FieldType.DOUBLE) && fieldValue.length() > fieldSize) || (fieldType.equals(FieldType.DATE) && !isValidDate(fieldValue))) {
            errorCode = INVALID_SIZE;
            message = "The following parameter has invalid size : " + fieldName;
            throw new Exception(message);
        }
        // return field value according to its type
        switch (fieldType) {
        case TEXT:
            return (T) fieldValue;
        case INT:
            return (T) new Long(fieldValue);
        case DOUBLE:
            return (T) new Double(fieldValue);
        case DATE:
            return (T) fieldValue;
        }
        return null;
    }

    /**
     * Find pending rejected waiting orders.
     *
     * @return the list
     */
    @SuppressWarnings("unchecked")
    public List<Order> findPendingRejectedWaitingOrders() {
        Query query = orderService.getEntityManager().createQuery("from " + Order.class.getName() + " o where o.status in :orderStatus ");
        query.setParameter("orderStatus", Arrays.asList(OrderStatusEnum.PENDING, OrderStatusEnum.REJECTED, OrderStatusEnum.IN_PROGRESS));// TODO change in_progress by waiting
        return query.getResultList();
    }

    /**
     * Check if date is valid (pattern : dd/MM/yyyy HH:mm:ss)
     *
     * @param date date
     * @return is date valid
     */
    private static Boolean isValidDate(String date) {
        try {
            new SimpleDateFormat("dd/MM/yyyy HH:mm:ss").parse(date);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    /**
     * Check if text is an integer
     *
     * @param integer integer attribute
     * @return is valid integer
     */
    private static Boolean isValidInteger(String integer) {
        try {
            Integer.parseInt(integer);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    /**
     * Check if text is a double
     *
     * @param number double attribute
     * @return is valid double
     */
    private static Boolean isValidDouble(String number) {
        try {
            Double.parseDouble(number);
        } catch (Exception ex) {
            return false;
        }
        return true;
    }

    private String getOfferName(OrderLineDTO orderLineDTO) {
        String offerName = null;
        String query = "SELECT code FROM cat_offer_template where cf_values like '%" + orderLineDTO.billCode + "%' and code not in ('MISC_WB','OF_EIR_TEMPLATE','NGA_FTH')";
        List<String> offerNames = offerTemplateService.getEntityManager().createNativeQuery(query).getResultList();
        if (offerNames != null && !offerNames.isEmpty()) {
            offerName = offerNames.get(0);
            OfferTemplate offer = offerTemplateService.findByCode(offerName);
            if (offer.getOfferServiceTemplates() == null || offer.getOfferServiceTemplates().isEmpty()) {
                rejectOrder("The offer " + offerName + " doesn't contain generic services");
            }
        }
        return offerName;
    }

    /**
     * Add Remark
     *
     * @param orderLineDTO the order line DTO
     * @param orderDTO the order DTO
     */
    private void addRemark(OrderLineDTO orderLineDTO, OrderDTO orderDTO) throws BusinessException {
        LOGGER.info("############### Start storing the 'Remark' attribute values .....");
        if (!OrderLineBillCodeEnum.RMK.name().equalsIgnoreCase(orderLineDTO.billCode)) {
            rejectOrder("The bill code should be set to" + OrderLineBillCodeEnum.RMK.name());
        }

        Subscription subscription = subscriptionService.findByCode(orderLineDTO.serviceId);
        if (subscription == null) {
            rejectOrder("The subscription " + orderLineDTO.serviceId + " is not found");
            return;
        }

        Map<String, String> remark = (Map<String, String>) subscription.getCfValuesNullSafe().getValue("REMARK");
        if (remark == null) {
            remark = new HashMap<>();
        }

        String remarkId = orderLineDTO.getAttributeValue(remarkAttributeNamesEnum.REMARK_ID.name());
        String elementType = orderLineDTO.getAttributeValue(remarkAttributeNamesEnum.ELEMENT_TYPE.name());
        String elementId = orderLineDTO.getAttributeValue(remarkAttributeNamesEnum.ELEMENT_ID.name());
        String description = orderLineDTO.getAttributeValue(remarkAttributeNamesEnum.DESCRIPTION.name());

        remark.put(orderLineDTO.itemId + "|" + orderLineDTO.itemComponentId,
            orderDTO.orderId + "|" + orderLineDTO.billEffectDate + "|" + (!StringUtils.isBlank(remarkId) ? remarkId : "") + "|"
                    + (!StringUtils.isBlank(elementType) ? elementType : "") + "|" + (!StringUtils.isBlank(elementId) ? elementId : "") + "|"
                    + (!StringUtils.isBlank(description) ? description : ""));
        subscription.getCfValuesNullSafe().setValue("REMARK", remark);
        subscriptionService.update(subscription);

        LOGGER.info("############### End storing the 'Remark' attribute values .....");
    }

    /**
     * get range Start
     *
     * @param serviceInstanceCode the service instance code
     * @return rangeStart
     */
    private Integer getRangeStart(Long serviceInstanceId) {
        Integer rangeStart = null;
        ServiceInstance serviceInstance = serviceInstanceService.findById(serviceInstanceId);
        if (serviceInstance.getCfValuesNullSafe().getValue("CF_BILL_ATTRIBUTE") != null) {
            Map<String, String> cf = (Map<String, String>) serviceInstance.getCfValuesNullSafe().getValue("CF_BILL_ATTRIBUTE");

            if (cf != null && cf.get("RANGE_START") != null) {
                try {
                    rangeStart = Integer.valueOf(cf.get("RANGE_START").split("\\|")[0].replaceAll("-", ""));
                } catch (NumberFormatException nfe) {
                }
            }

        }
        return rangeStart;
    }

    /**
     * get range numbers
     *
     * @param serviceInstanceCode the service instance code
     * @return map of range numbers
     */
    private Map<Integer, Integer> getRangeNumbers(String serviceInstanceCode) {
        Map<Integer, Integer> rangeNumbers = new HashMap<>();

        List<ServiceInstance> serviceInstances = serviceInstanceService.findByCodeLike(serviceInstanceCode);
        for (ServiceInstance serviceInstance : serviceInstances) {
            if (serviceInstance.getCfValuesNullSafe().getValue("CF_BILL_ATTRIBUTE") != null) {
                Map<String, String> cf = (Map<String, String>) serviceInstance.getCfValuesNullSafe().getValue("CF_BILL_ATTRIBUTE");
                if (cf.get("RANGE_START") != null || cf.get("RANGE_END") != null) {

                    Integer rangeStart = null;
                    try {
                        rangeStart = Integer.valueOf(cf.get("RANGE_START").split("\\|")[0].replaceAll("-", ""));
                    } catch (Exception nfe) {
                        rangeStart = null;
                    }

                    Integer rangeEnd = null;
                    try {
                        rangeEnd = Integer.valueOf(cf.get("RANGE_END").split("\\|")[0].replaceAll("-", ""));
                    } catch (Exception nfe) {
                        rangeEnd = null;
                    }

                    rangeStart = (rangeStart != null) ? rangeStart : rangeEnd;
                    rangeEnd = (rangeEnd != null) ? rangeEnd : rangeStart;

                    if (rangeStart != null && rangeEnd != null) {
                        rangeNumbers.put(Integer.valueOf(rangeStart), Integer.valueOf(rangeEnd));
                    }
                }
            }
        }
        return rangeNumbers;
    }

    /**
     * Check if new ranges are valid against the existing ones
     *
     * @param existingRangeStart the existing range start
     * @param existingRangeEnd the existing range end
     * @param newRangeStart the new range start
     * @param newRangeEnd the new range end
     * @return true if new ranges are valid against the existing ones
     */
    private boolean isValidRanges(Integer existingRangeStart, Integer existingRangeEnd, Integer newRangeStart, Integer newRangeEnd) {
        return !((newRangeStart != null && newRangeStart >= existingRangeStart && newRangeStart <= existingRangeEnd)
                || (newRangeEnd != null && newRangeEnd >= existingRangeStart && newRangeEnd <= existingRangeEnd));
    }

    /**
     * Check if new ranges are valid against the existing ones
     *
     * @param rangeStart the range start
     * @param rangeEnd the range end
     * @return true if new ranges are valid against the existing ones
     */
    private boolean isValidRanges(String rangeStart, String rangeEnd, String originalServiceId) {
        if (!StringUtils.isBlank(rangeStart) || !StringUtils.isBlank(rangeEnd)) {

            if (!isValidOnMsnDdiRanges(rangeStart, rangeEnd)) {
                return false;
            }
            // check if new range not already taken
            List<Subscription> subs = subscriptionService.findByCodeLike(rangeStart);

            if (subs != null && !subs.isEmpty() && !originalServiceId.equalsIgnoreCase(rangeStart)) {
                LOGGER.info(rangeStart + " already exist or interfers with existing subscription" + subs.get(0));
                rejectOrder("RANGE_START or RANGE_END already exists or interfers with existing subscription" + subs.get(0));
                return false;

            }

        }

        return true;

    }

    private boolean isValidOnMsnDdiRanges(String rangeStart, String rangeEnd) {

        if (!StringUtils.isBlank(rangeStart) || !StringUtils.isBlank(rangeEnd)) {
            Map<Integer, Integer> rangeNumbers = new HashMap<>();

            rangeNumbers.putAll(getRangeNumbers(AncillaryServiceEnum.MSN.name()));
            rangeNumbers.putAll(getRangeNumbers(AncillaryServiceEnum.DDI.name()));

            Integer newRangeStart = null;
            if (!StringUtils.isBlank(rangeStart)) {
                try {
                    newRangeStart = Integer.valueOf(rangeStart.replaceAll("-", ""));
                } catch (NumberFormatException nfe) {
                }
            }

            Integer newRangeEnd = null;
            if (!StringUtils.isBlank(rangeEnd)) {
                try {
                    newRangeEnd = Integer.valueOf(rangeEnd.replaceAll("-", ""));
                } catch (NumberFormatException nfe) {
                }
            }

            // validating range numbers
            for (Entry<Integer, Integer> entry : rangeNumbers.entrySet()) {
                if (!isValidRanges(entry.getKey(), entry.getValue(), newRangeStart, newRangeEnd)) {
                    String usedRanges = "";
                    usedRanges += newRangeStart != null ? newRangeStart + " " : "";
                    usedRanges += newRangeEnd != null ? newRangeEnd + " " : "";
                    log.error(usedRanges + "already in use at ranges: " + entry.getKey() + "  " + entry.getValue());
                    rejectOrder("NEW_RANGE_START and NEW_RANGE_END already exist or interfers with existing one");
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Set the service quantity
     *
     * @param orderLineDTO The Order Line
     * @param billCodeInfo The bill code info
     * @param quantity The quantity
     * @return The service quantity
     */
    private BigDecimal setServiceQuantity(OrderLineDTO orderLineDTO, String billCodeInfo, BigDecimal quantity) {
        // Get the max quantity for the current service.
        String maxQuantity = billCodeInfo.split("\\|")[Catalog.MAX_QUANTITY.getPosition()];
        if (!isValidDouble(maxQuantity)) {
            rejectOrder("The max quantity of service : " + orderLineDTO.billCode + " is invalid");
            return null;
        }
        if (orderLineDTO.billQuantity != null) {
            BigDecimal billQuantity = new BigDecimal(orderLineDTO.billQuantity);
            if (billQuantity.compareTo(BigDecimal.ZERO) < 0) {
                rejectOrder("the service quantity must be greater than 0");
                return null;
            }
            if (billQuantity.compareTo(new BigDecimal(maxQuantity)) > 0) {
                rejectOrder("the quantity exceeds the maximum quantity allowed for the service :" + orderLineDTO.billCode);
                return null;
            }
            quantity = quantity.add(billQuantity);
        } else {
            rejectOrder("The quantity for the service : " + orderLineDTO.billCode + " is invalid");
            return null;
        }
        return quantity;
    }

    /**
     * Set the service quantity
     *
     * @param orderLineDTO The Order Line
     * @param serviceInstance The service instance.
     * @param billCodeInfo The bill code info
     * @param quantity The quantity
     * @return The service quantity
     */
    private BigDecimal setServiceQuantity(OrderLineDTO orderLineDTO, ServiceInstance serviceInstance, String billCodeInfo, BigDecimal quantity,
            Map<ServiceInstance, BigDecimal> serviceInstanceQuantity) {

        // Get the max quantity for the current service.
        String maxQuantity = billCodeInfo.split("\\|")[Catalog.MAX_QUANTITY.getPosition()];
        if (!isValidDouble(maxQuantity)) {
            rejectOrder("The max quantity of service : " + orderLineDTO.billCode + " is invalid");
            return null;
        }

        if (quantity != null && serviceInstance.getQuantity() != null) {

            switch (orderLineDTO.billAction) {
            case "P":
            case "CNS":
            case "TNS":
                quantity = quantity.add(serviceInstance.getQuantity());
                break;
            case "C":
            case "COS":
            case "TOS":
                if (serviceInstance.getQuantity().compareTo(quantity) <= 0) {
                    if (serviceInstance.getQuantity().compareTo(quantity) < 0) {
                        dataDiscrepancies += "The quantity on the bill code should not be greater than the quantity on the active service. \n";
                        quantity = serviceInstance.getQuantity();
                    } else {
                        quantity = BigDecimal.ZERO;
                    }
                    if (serviceInstanceQuantity != null) {
                        serviceInstanceQuantity.put(serviceInstance, quantity);
                    }
                } else {
                    if (serviceInstanceQuantity != null) {
                        serviceInstanceQuantity.put(serviceInstance, serviceInstance.getQuantity().subtract(quantity));
                    }
                    quantity = serviceInstance.getQuantity().subtract(quantity);
                }
                break;
            }
        }
        if (new BigDecimal(maxQuantity).compareTo(quantity) < 0) {
            rejectOrder("the quantity supplied plus the existing quantity exceeds the maximum quantity allowed for the service :" + orderLineDTO.billCode);
            return null;
        }
        return quantity;
    }

    private BigDecimal updateMainService(OrderLineDTO orderLineDTO, OrderDTO orderDTO, ServiceInstance serviceInstance, String billCodeInfo, BigDecimal quantity)
            throws BusinessException, ParseException {
        if (serviceInstance.getStatus() == InstanceStatusEnum.ACTIVE && serviceInstance.getQuantity().compareTo(quantity) >= 0) {
            quantity = setServiceQuantity(orderLineDTO, serviceInstance, billCodeInfo, quantity, null);
            if (quantity != null) {
                if (quantity.compareTo(BigDecimal.ZERO) > 0) {
                    updateWalletOperations(serviceInstance, orderLineDTO, quantity);
                }
                serviceInstance.setQuantity(quantity);
                serviceInstanceService.update(serviceInstance);
            }
            if (quantity == null || serviceInstance.getQuantity().compareTo(BigDecimal.ZERO) > 0) {
                return null;
            }
        }
        return quantity;
    }

    /**
     * Update the ancillary service
     *
     * @param orderLineDTO the order line DTO
     * @param orderDTO the order DTO
     * @param terminationReason the termination reason
     * @param serviceInstance the service instance
     * @param serviceInstanceQuantity the service instance quantity map
     * @throws ParseException the parse exception
     * @throws BusinessException the business exception
     */
    private void updateAncillaryService(OrderLineDTO orderLineDTO, OrderDTO orderDTO, SubscriptionTerminationReason terminationReason, ServiceInstance serviceInstance,
            Map<ServiceInstance, BigDecimal> serviceInstanceQuantity, BigDecimal quantity) throws ParseException, BusinessException {
        if (orderLineDTO.billCode.equalsIgnoreCase(AncillaryServiceEnum.MSN.name()) || orderLineDTO.billCode.startsWith(AncillaryServiceEnum.DDI.name())) {
            serviceInstanceService.terminateService(serviceInstance, getDate(orderLineDTO.billEffectDate), terminationReason,
                orderDTO != null ? orderLineDTO.operatorOrderNumber : null);
            LOGGER.info("Ancillary service terminated : " + serviceInstance.getCode());
        }
        if (serviceInstanceQuantity.get(serviceInstance) != null) {
            if (serviceInstance.getQuantity().compareTo(quantity) <= 0)
                if (serviceInstance.getQuantity().compareTo(quantity) == 0) {
                    serviceInstance.setQuantity(BigDecimal.ZERO);
                    serviceInstanceService.update(serviceInstance);
                }
            serviceInstanceService.terminateService(serviceInstance, getDate(orderLineDTO.billEffectDate), terminationReason,
                orderDTO != null ? orderLineDTO.operatorOrderNumber : null);
            LOGGER.info("Ancillary service terminated : " + serviceInstance.getCode());
        } else if (serviceInstance.getQuantity().compareTo(quantity) > 0) {
            updateWalletOperations(serviceInstance, orderLineDTO, serviceInstanceQuantity.get(serviceInstance));
            serviceInstance.setQuantity(serviceInstanceQuantity.get(serviceInstance));
            serviceInstanceService.update(serviceInstance);
        }

    }

    /**
     * Validate bill item quantity and calculate the service quantity.
     *
     * @param orderLineDTO The order line dTO
     * @param subscription The subscription
     * @param serviceInstanceQuantity The service instance quantity map
     * @param billCodeInfo The bill code info
     * @param quantity The bill item quantity
     * @return
     */
    private boolean validateAndCalculateQuantity(OrderLineDTO orderLineDTO, Subscription subscription, Map<ServiceInstance, BigDecimal> serviceInstanceQuantity,
            String billCodeInfo, BigDecimal quantity) {

        for (ServiceInstance serviceInstance : subscription.getServiceInstances()) {
            if (serviceInstance != null) {
                if (orderLineDTO.billCode.equals(serviceInstance.getCode()) && serviceInstance.getStatus() == InstanceStatusEnum.ACTIVE) {
                    serviceInstanceQuantity.put(serviceInstance, serviceInstance.getQuantity());
                    quantity = setServiceQuantity(orderLineDTO, serviceInstance, billCodeInfo, quantity, serviceInstanceQuantity);
                    if (quantity == null) {
                        return false;
                    }
                    if (serviceInstance.getQuantity().compareTo(quantity) == 0) {
                        break;
                    }
                }
            }
        }

        return true;
    }

    /**
     * Check is the service instance has a same ranges as order line xml
     *
     * @param serviceInstance The service instance
     * @param ranges The order line ranges
     * @param billCode
     * @return True is is the service instance has a same ranges as order line xml.
     */
    private boolean hasSameRanges(ServiceInstance serviceInstance, Map<String, String> ranges, String billCode) {
        Map<String, String> cf = (Map<String, String>) serviceInstance.getCfValuesNullSafe().getValue("CF_BILL_ATTRIBUTE");
        if (cf != null) {
            if (billCode.equalsIgnoreCase(AncillaryServiceEnum.MSN.name()) && cf.get("RANGE_START").split("\\|")[0].equals(ranges.get("RANGE_START"))) {
                return true;
            }
            if (cf.get("RANGE_START") != null && cf.get("RANGE_END") != null && cf.get("RANGE_START").split("\\|")[0].equals(ranges.get("RANGE_START"))
                    && cf.get("RANGE_END").split("\\|")[0].equals(ranges.get("RANGE_END"))) {
                return true;
            }
        }
        return false;
    }

    /**
     * Get the order attributes into map
     *
     * @param orderLineDTO the order line DTO
     * @return the order attributes into map
     */
    private Map<String, String> getRangesAttributes(OrderLineDTO orderLineDTO) {
        Map<String, String> attributes = null;
        if (orderLineDTO != null && orderLineDTO.attributes != null) {
            attributes = new HashMap<>();
            for (AttributeDTO attributeDTO : orderLineDTO.attributes) {
                if (attributeDTO.code.equalsIgnoreCase("RANGE_START")) {
                    attributes.put("RANGE_START", attributeDTO.value);
                }
                if (attributeDTO.code.equalsIgnoreCase("RANGE_END")) {
                    attributes.put("RANGE_END", attributeDTO.value);
                }
                if (attributeDTO.code.equalsIgnoreCase("NEW_RANGE_START")) {
                    attributes.put("NEW_RANGE_START", attributeDTO.value);
                }
                if (attributeDTO.code.equalsIgnoreCase("NEW_RANGE_END")) {
                    attributes.put("NEW_RANGE_END", attributeDTO.value);
                }
            }
        }
        return attributes;
    }

    /**
     * Get the child element
     *
     * @param parent the parent element
     * @param name the element name
     * @return the child element
     */
    private Element getChild(Element parent, String name) {
        for (Node child = parent.getFirstChild(); child != null; child = child.getNextSibling()) {
            if (child instanceof Element && name.equals(child.getNodeName()))
                return (Element) child;
        }
        return null;
    }

    /**
     * Get orderLine DTO by item id
     *
     * @param orderDTO the order DTO
     * @param itemId the item id
     * @return the orderLine DTO
     */
    private OrderLineDTO getOrderLine(OrderDTO orderDTO, String itemId) {
        if (orderDTO != null && orderDTO.orderLines != null && !orderDTO.orderLines.isEmpty()) {
            for (OrderLineDTO orderLineDTO : orderDTO.orderLines) {
                if (orderLineDTO != null && !StringUtils.isBlank(orderLineDTO.itemId) && orderLineDTO.itemId.equalsIgnoreCase(itemId)) {
                    return orderLineDTO;
                }
            }
        }
        return null;
    }

    /**
     * Add element to parent
     *
     * @param document the root DOM document
     * @param parent the parent element
     * @param name the element name
     * @param value the element value
     */
    private void addElement(Document document, Element parent, String name, String value) {
        if (!StringUtils.isBlank(value)) {
            Element element = getChild(parent, name);
            if (element == null) {
                createElement(document, parent, name, value);
            }
        }
    }

    /**
     * Create element
     *
     * @param document the root DOM document
     * @param parent the parent element
     * @param name the element name
     * @param value the element value
     */
    private void createElement(Document document, Element parent, String name, String value) {
        if (!StringUtils.isBlank(value)) {
            Element status = document.createElement(name);
            status.appendChild(document.createTextNode(value));
            parent.appendChild(status);
        }
    }

    /**
     * Get string from document
     *
     * @param document the root DOM document
     * @return the string from document
     * @throws TransformerException the transformer exception
     */
    public static String getStringFromDocument(Document document) throws TransformerException {
        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        Source source = new DOMSource(document);
        StringWriter writer = new StringWriter();
        transformer.transform(source, new StreamResult(writer));
        return writer.toString();
    }

    /**
     * Parse incoming order from XML
     *
     * @return Formatted Order
     * @throws Exception App Exception
     */
    private void updateXmlOrderCF(Order order, OrderDTO orderDTO) throws Exception {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(new InputSource(new StringReader(orderMessage)));

        Element root = document.getDocumentElement();

        // append a status element
        /*
         * if (order.getStatus() != null) { addElement(document, root, "Status", order.getStatus().toString()); }
         */

        // append a statusMessage element
        addElement(document, root, "Status_Message", order.getStatusMessage());

        // parsing and validating order items fields
        for (int i = 0; i < root.getElementsByTagName("Bill_Item").getLength(); i++) {
            Element orderItem = (Element) root.getElementsByTagName("Bill_Item").item(i);
            // parsing and updating order attribute fields

            Element itemIdElement = getChild(orderItem, "Item_Id");
            OrderLineDTO orderLineDTO = getOrderLine(orderDTO, itemIdElement.getTextContent());

            // add Master_Account_Number element if not exist.
            addElement(document, orderItem, "Master_Account_Number", orderLineDTO.masterAccountNumber);

            // add Service_Id element if not exist.
            addElement(document, orderItem, "Service_Id", orderLineDTO.serviceId);
        }
        order.getCfValuesNullSafe().setValue("ORDER_MESSAGE", getStringFromDocument(document));
    }

    /**
     * ancillary service Enum
     */
    public enum AncillaryServiceEnum {
        MSN, DDI
    }

    /**
     * Order line bill action Enum
     */
    public enum OrderLineBillActionEnum {
        TOS, TNS, COS, CNS, C, P, CU, CN, SN, M, MC, PR
    }

    /**
     * Order line bill code Enum
     */
    public enum OrderLineBillCodeEnum {
        RMK
    }

    /**
     * service type Enum
     */
    public enum ServiceTypeEnum {
        MAIN("Main"), ANCILLARY("Ancillary");

        private String label;

        ServiceTypeEnum(String label) {
            this.label = label;
        }

        public String getLabel() {
            return this.label;
        }
    }

    /**
     * Remark attribute names Enum
     */
    public enum remarkAttributeNamesEnum {
        Item_Id, Item_Component_Id, Order_Id, Effective_Date, REMARK_ID, ELEMENT_TYPE, ELEMENT_ID, DESCRIPTION
    }
}

/**
 * Formatted Order Object
 *
 * @author Mohammed Amine TAZI
 */
class OrderDTO {
    private static final String SEPARATOR = "|";
    String orderingSystem;
    String orderId;
    String refOrderId;
    String transactionId;
    OrderStatusEnum status;
    List<OrderLineDTO> orderLines = new ArrayList<>();

    // order details text included in CF : ORDER_DETAILS
    Map<String, String> getOrderDetails() {
        Map<String, String> orderDetails = new HashMap<>();
        orderDetails.put("" + orderId, orderingSystem + SEPARATOR + refOrderId + SEPARATOR + transactionId);
        return orderDetails;
    }
}

/**
 * Formatted Order Line Object
 *
 * @author Mohammed Amine TAZI
 */
class OrderLineDTO {
    private static final String SEPARATOR = "|";
    String itemId;
    String operatorId;
    String itemComponentId;
    String operatorOrderNumber;
    Long itemVersion;
    String itemType;
    String agentCode;
    String uan;
    String masterAccountNumber;
    String serviceId;
    String refServiceId;
    String xRefServiceId;
    String billCode;
    String billAction;
    String billActionQualifier;
    Long billQuantity;
    String billEffectDate;
    String billType;
    String offerName;
    Double specialConnect;
    Double specialRental;
    boolean isMain;
    String originalServiceId;
    List<AttributeDTO> attributes = new ArrayList<>();

    String getDetails() {
        StringBuilder sb = new StringBuilder();
        String action = getOriginalOrderAction();
        sb.append(operatorId).append(SEPARATOR).append(itemComponentId).append(SEPARATOR).append(itemVersion).append(SEPARATOR).append(itemType).append(SEPARATOR).append(agentCode)
            .append(SEPARATOR).append(uan).append(SEPARATOR).append(masterAccountNumber).append(SEPARATOR).append(serviceId).append(SEPARATOR).append(refServiceId)
            .append(SEPARATOR).append(xRefServiceId).append(SEPARATOR).append(billCode).append(SEPARATOR).append(action).append(SEPARATOR).append(billActionQualifier)
            .append(SEPARATOR).append(billQuantity).append(SEPARATOR).append(billEffectDate).append(SEPARATOR).append(billType).append(SEPARATOR).append(offerName)
            .append(SEPARATOR).append(operatorOrderNumber).append(SEPARATOR);
        return sb.toString();
    }

    String getOriginalOrderAction() {
        String action = billAction;
        if ("COS".equalsIgnoreCase(action) || "TOS".equalsIgnoreCase(action) || "PNS".equalsIgnoreCase(action)) {
            action = "C";
        }
        if ("CNS".equalsIgnoreCase(action) || "TNS".equalsIgnoreCase(action)) {
            action = "P";
        }
        return action;
    }

    public String getAttributeValue(String attribute) {
        for (AttributeDTO attributeDTO : attributes) {
            if (attributeDTO.code.equalsIgnoreCase(attribute)) {
                return attributeDTO.value;
            }
        }
        return "";
    }

}

/**
 * Formatted Order Line Attributes Object
 *
 * @author Mohammed Amine TAZI
 */
class AttributeDTO {
    private static final String SEPARATOR = "|";
    String code;
    String value;
    String unit;
    String componentId;
    String attributeType;

    String getDetails() {
        StringBuilder sb = new StringBuilder();
        sb.append(value).append(SEPARATOR).append(unit).append(SEPARATOR).append(componentId).append(SEPARATOR).append(attributeType).append(SEPARATOR);
        return sb.toString();
    }
}

/**
 *
 *
 *
 */
class AccessDTO {

    String accessUserId;
    Date endDate;

}

/**
 * Field Type enumeration
 *
 * @author Mohammed Amine TAZI
 */
enum FieldType {
    TEXT, INT, DOUBLE, DATE
}

enum Catalog {
    DESCRIPTION(0), SERVICE_TYPE(1), PERIODICITY(2), CYCLE_FORWARD(3), MAX_QUANTITY(4), INV_CAT(5), TAX_CLASS(6), PRODUCT_SET(7), MIGRATION_CODE(8), SUBSCRIBER_PREFIX(9);

    int position;

    Catalog(Integer position) {
        this.position = position;
    }

    int getPosition() {
        return position;
    }
}