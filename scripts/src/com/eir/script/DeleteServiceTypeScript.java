package com.eir.script;

import java.util.List;
import java.util.Map;

import org.meveo.admin.exception.BusinessException;
import org.meveo.service.billing.impl.BillingAccountService;
import org.meveo.service.script.Script;

/**
 * Delete ServiceType Script
 * 
 * @author hznibar
 *
 */
public class DeleteServiceTypeScript extends Script {

//    private static final String SERVICE_TYPE = "Service_Type";
	/** The Constant serialVersionUID. */
	private static final long serialVersionUID = 1L;
	// API Services
	private BillingAccountService billingAccountService = (BillingAccountService) getServiceInterface(
			BillingAccountService.class.getSimpleName());

	@Override
	public void execute(Map<String, Object> initContext) throws BusinessException {
		log.info("#####################Starting of script DeleteServiceTypeScript");
		try {
			String serviceType = (String) initContext.get("CODE");
			List<String> bas = getBillingAccountByServiceType(serviceType);
			if (bas != null && bas.size() > 0) {
				throw new BusinessException(
						serviceType + ": This service type may not be deleted as it is assigned to a billing account.");
			}
		} catch (Exception e) {
			throw new BusinessException(e.getMessage());

		}
	}

	/**
	 * Gets the billing account by service type.
	 *
	 * @param serviceType the service type
	 * @return the billing account by service type
	 */
	@SuppressWarnings("unchecked")
	private List<String> getBillingAccountByServiceType(String serviceType) {
		String query = "SELECT code FROM account_entity where account_type = 'ACCT_BA' and cf_values like '%SERVICE_TYPE%"
				+ serviceType + "%'";
		List<String> bas = billingAccountService.getEntityManager().createNativeQuery(query).getResultList();
		return bas;
	}
}