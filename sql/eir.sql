
-- Extra indexes

CREATE INDEX eir_or_tariff_plan_map_index ON eir_or_tariff_plan_map (offer_id);
CREATE INDEX eir_or_tariff_index ON eir_or_tariff (tariff_plan_id, offer_id, service_id, lower(charge_type), priority);
CREATE INDEX eir_or_flat_discount_index on eir_or_flat_discount (customer_account, lower(charge_type));

CREATE INDEX eir_u_traffic_class_index on eir_u_traffic_class (lower(fee_code), lower(service_category), lower(call_type));
CREATE INDEX eir_u_tariff_plan_map_index on eir_u_tariff_plan_map (customer_id);
CREATE INDEX eir_u_time_period_index on eir_u_time_period (time_band);
CREATE INDEX eir_u_tariff_index on eir_u_tariff(rate_plan_id, traffic_class_id, time_period_id);

CREATE INDEX billing_service_instance_range_index on billing_service_instance 
(((cf_values::JSON->'RANGE_START'->0 ->>'long')::BIGINT), ((cf_values::JSON->'RANGE_END'->0 ->>'long')::BIGINT));

CREATE INDEX medina_access_eir_index on medina_access (regexp_replace(regexp_replace(acces_user_id, '^[0]', '', 'g'), '-', '', 'g'));

CREATE INDEX billing_subscription_uan_index on billing_subscription ((cf_values::JSON->'UAN'->0 ->>'string'::varchar));

CREATE INDEX billing_subscription_service_id_index on billing_subscription (((cf_values::JSON->'SERVICE_ID'->0 ->>'long')::BIGINT));

CREATE INDEX ord_order_service_id_index ON ord_order using gin ((cf_values::JSONB->'SERVICE_IDS'->0->'listString' ) jsonb_ops_custom);

CREATE INDEX ord_order_held_by_index on ord_order ((cf_values::JSON->'HELD_BY'->0->>'string'));

CREATE INDEX billing_subscription_original_code_index on billing_subscription ((cf_values::JSON->'ORIGINAL_CODE'->0->>'string'));

CREATE INDEX ord_order_ref_id_index on ord_order ((cf_values::JSON->'ORDER_REF_ID'->0 ->>'string'::varchar));

CREATE INDEX billing_wallet_operation_charge_instance_index ON billing_wallet_operation (charge_instance_id);

CREATE INDEX eir_sub_endpoint_sub_index ON eir_sub_endpoint (subscription_id);